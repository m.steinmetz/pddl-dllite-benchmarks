(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag152-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag139-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag126-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag104-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag46-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag33-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F10)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F6)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(Flag180-)
(Flag172-)
(Flag163-)
(Flag150-)
(Flag137-)
(Flag124-)
(Flag115-)
(Flag94-)
(Flag92-)
(Flag90-)
(Flag78-)
(Flag69-)
(Flag57-)
(Flag44-)
(Flag31-)
(Flag22-)
(Flag13-)
(BOARDED-P1)
(SERVED-P3)
(BOARDED-P2)
(BOARDED-P4)
(BOARDED-P7)
(SERVED-P2)
(SERVED-P4)
(SERVED-P5)
(BOARDED-P0)
(SERVED-P7)
(BOARDED-P5)
(SERVED-P0)
(SERVED-P1)
(BOARDED-P3)
(SERVED-P6)
(Flag153-)
(Flag151-)
(Flag140-)
(Flag138-)
(Flag127-)
(Flag125-)
(Flag105-)
(Flag103-)
(Flag102-)
(Flag93-)
(Flag91-)
(Flag47-)
(Flag45-)
(Flag34-)
(Flag32-)
(Flag1-)
(BOARDED-P6)
(NOT-BOARDED-P6)
(NOT-SERVED-P6)
(NOT-BOARDED-P3)
(NOT-BOARDED-P5)
(NOT-SERVED-P0)
(NOT-SERVED-P1)
(NOT-BOARDED-P0)
(NOT-SERVED-P7)
(NOT-SERVED-P4)
(NOT-SERVED-P5)
(NOT-BOARDED-P7)
(NOT-SERVED-P2)
(NOT-BOARDED-P4)
(NOT-BOARDED-P2)
(NOT-BOARDED-P1)
(NOT-SERVED-P3)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F6)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F10)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag33-)(and (Flag32-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag46-)(and (Flag45-)))

(:derived (Flag48-)(and (Flag47-)))

(:derived (Flag92-)(and (Flag91-)))

(:derived (Flag94-)(and (Flag93-)))

(:derived (Flag104-)(and (Flag103-)))

(:derived (Flag106-)(and (Flag105-)))

(:derived (Flag126-)(and (Flag125-)))

(:derived (Flag128-)(and (Flag127-)))

(:derived (Flag139-)(and (Flag138-)))

(:derived (Flag141-)(and (Flag140-)))

(:derived (Flag152-)(and (Flag151-)))

(:derived (Flag154-)(and (Flag153-)))

(:action STOP-F7
:parameters ()
:precondition
(and
(Flag102-)
)
:effect
(and
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
)
)
(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)))

(:derived (Flag32-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag34-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag45-)(and (NOT-BOARDED-P7)(SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag47-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag91-)(and (NOT-BOARDED-P6)(SERVED-P6)))

(:derived (Flag93-)(and (SERVED-P6)(NOT-BOARDED-P6)))

(:derived (Flag102-)(and (LIFT-AT-F7)(Flag92-)(Flag94-)(Flag95-)(Flag96-)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(NOT-BOARDED-P1)(Flag101-)))

(:derived (Flag103-)(and (SERVED-P7)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)))

(:derived (Flag105-)(and (NOT-BOARDED-P7)(SERVED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)))

(:derived (Flag125-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(SERVED-P4)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag127-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag138-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P2)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag140-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P2)(NOT-BOARDED-P1)))

(:derived (Flag151-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P2)(SERVED-P1)(NOT-BOARDED-P1)))

(:derived (Flag153-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(SERVED-P1)))

(:action STOP-F15
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
)
)
(:action STOP-F14
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
)
)
(:action STOP-F13
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag44-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag57-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag69-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag78-)
)
:effect
(and
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag90-)
)
:effect
(and
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag115-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag124-)
)
:effect
(and
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag137-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag150-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag163-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag172-)
)
:effect
(and
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag180-)
)
:effect
(and
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:derived (Flag3-)(and (Flag2-)))

(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag13-)(and (LIFT-AT-F15)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)))

(:derived (Flag22-)(and (LIFT-AT-F14)(Flag3-)(Flag4-)(Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)(Flag20-)(Flag21-)))

(:derived (Flag31-)(and (LIFT-AT-F13)(Flag23-)(Flag24-)(Flag25-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)))

(:derived (Flag44-)(and (LIFT-AT-F12)(Flag33-)(Flag35-)(Flag36-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)))

(:derived (Flag57-)(and (LIFT-AT-F11)(Flag46-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)))

(:derived (Flag59-)(and (Flag58-)))

(:derived (Flag60-)(and (Flag58-)))

(:derived (Flag69-)(and (LIFT-AT-F10)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)))

(:derived (Flag78-)(and (LIFT-AT-F9)(Flag3-)(Flag4-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)))

(:derived (Flag80-)(and (Flag79-)))

(:derived (Flag81-)(and (Flag79-)))

(:derived (Flag90-)(and (LIFT-AT-F8)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)(Flag85-)(Flag86-)(Flag87-)(Flag88-)(Flag89-)))

(:derived (Flag92-)(and (Flag2-)))

(:derived (Flag94-)(and (Flag2-)))

(:derived (Flag115-)(and (LIFT-AT-F6)(Flag104-)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)))

(:derived (Flag124-)(and (LIFT-AT-F5)(Flag3-)(Flag4-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)(Flag122-)(Flag123-)))

(:derived (Flag137-)(and (LIFT-AT-F4)(Flag126-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)))

(:derived (Flag150-)(and (LIFT-AT-F3)(Flag139-)(Flag141-)(Flag142-)(Flag143-)(Flag144-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)))

(:derived (Flag163-)(and (LIFT-AT-F2)(Flag152-)(Flag154-)(Flag155-)(Flag156-)(Flag157-)(Flag158-)(Flag159-)(Flag160-)(Flag161-)(Flag162-)))

(:derived (Flag172-)(and (LIFT-AT-F1)(Flag3-)(Flag4-)(Flag164-)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)))

(:derived (Flag180-)(and (LIFT-AT-F0)(Flag3-)(Flag4-)(Flag173-)(NOT-BOARDED-P6)(Flag174-)(Flag175-)(Flag176-)(Flag177-)(Flag178-)(Flag179-)))

(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F6)
)
:effect
(and
(NO-ACCESS-P6-F6)
(not (NOT-NO-ACCESS-P6-F6))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F10)
)
:effect
(and
(NO-ACCESS-P3-F10)
(not (NOT-NO-ACCESS-P3-F10))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag3-)(and (NOT-BOARDED-P6)))

(:derived (Flag4-)(and (NOT-BOARDED-P6)))

(:derived (Flag5-)(and (NOT-BOARDED-P7)))

(:derived (Flag5-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag6-)(and (NOT-BOARDED-P6)))

(:derived (Flag6-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag7-)(and (NOT-BOARDED-P5)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag8-)(and (NOT-BOARDED-P4)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag9-)(and (NOT-BOARDED-P3)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag10-)(and (NOT-BOARDED-P2)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag11-)(and (NOT-BOARDED-P1)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag12-)(and (NOT-BOARDED-P0)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag14-)(and (NOT-BOARDED-P7)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag15-)(and (NOT-BOARDED-P6)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag16-)(and (NOT-BOARDED-P5)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag17-)(and (NOT-BOARDED-P4)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag18-)(and (NOT-BOARDED-P3)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag19-)(and (NOT-BOARDED-P2)))

(:derived (Flag19-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag20-)(and (NOT-BOARDED-P1)))

(:derived (Flag20-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag21-)(and (NOT-BOARDED-P0)))

(:derived (Flag21-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag23-)(and (NOT-BOARDED-P7)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag24-)(and (NOT-BOARDED-P6)))

(:derived (Flag24-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag25-)(and (NOT-BOARDED-P5)))

(:derived (Flag25-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag26-)(and (NOT-BOARDED-P4)))

(:derived (Flag26-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag27-)(and (NOT-BOARDED-P3)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag28-)(and (NOT-BOARDED-P2)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag29-)(and (NOT-BOARDED-P1)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag30-)(and (NOT-BOARDED-P0)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag33-)(and (NOT-BOARDED-P6)))

(:derived (Flag35-)(and (NOT-BOARDED-P6)))

(:derived (Flag36-)(and (NOT-BOARDED-P7)))

(:derived (Flag36-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag37-)(and (NOT-BOARDED-P6)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag38-)(and (NOT-BOARDED-P5)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag39-)(and (NOT-BOARDED-P4)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag40-)(and (NOT-BOARDED-P3)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag41-)(and (NOT-BOARDED-P2)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag42-)(and (NOT-BOARDED-P1)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag43-)(and (NOT-BOARDED-P0)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag46-)(and (NOT-BOARDED-P6)))

(:derived (Flag48-)(and (NOT-BOARDED-P6)))

(:derived (Flag49-)(and (NOT-BOARDED-P7)))

(:derived (Flag49-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag50-)(and (NOT-BOARDED-P6)))

(:derived (Flag50-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag51-)(and (NOT-BOARDED-P5)))

(:derived (Flag51-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag52-)(and (NOT-BOARDED-P4)))

(:derived (Flag52-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag53-)(and (NOT-BOARDED-P3)))

(:derived (Flag53-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag54-)(and (NOT-BOARDED-P2)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag55-)(and (NOT-BOARDED-P1)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag56-)(and (NOT-BOARDED-P0)))

(:derived (Flag56-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag58-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag59-)(and (NOT-BOARDED-P6)))

(:derived (Flag60-)(and (NOT-BOARDED-P6)))

(:derived (Flag61-)(and (NOT-BOARDED-P7)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag62-)(and (NOT-BOARDED-P6)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag63-)(and (NOT-BOARDED-P5)))

(:derived (Flag63-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag64-)(and (NOT-BOARDED-P4)))

(:derived (Flag64-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag65-)(and (NOT-BOARDED-P3)))

(:derived (Flag65-)(and (NOT-NO-ACCESS-P3-F10)))

(:derived (Flag66-)(and (NOT-BOARDED-P2)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag67-)(and (NOT-BOARDED-P1)))

(:derived (Flag67-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag68-)(and (NOT-BOARDED-P0)))

(:derived (Flag68-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag70-)(and (NOT-BOARDED-P7)))

(:derived (Flag70-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag71-)(and (NOT-BOARDED-P6)))

(:derived (Flag71-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag72-)(and (NOT-BOARDED-P5)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag73-)(and (NOT-BOARDED-P4)))

(:derived (Flag73-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag74-)(and (NOT-BOARDED-P3)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag75-)(and (NOT-BOARDED-P2)))

(:derived (Flag75-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag76-)(and (NOT-BOARDED-P1)))

(:derived (Flag76-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag77-)(and (NOT-BOARDED-P0)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag79-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag80-)(and (NOT-BOARDED-P6)))

(:derived (Flag81-)(and (NOT-BOARDED-P6)))

(:derived (Flag82-)(and (NOT-BOARDED-P7)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag83-)(and (NOT-BOARDED-P6)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag84-)(and (NOT-BOARDED-P5)))

(:derived (Flag84-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag85-)(and (NOT-BOARDED-P4)))

(:derived (Flag85-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag86-)(and (NOT-BOARDED-P3)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag87-)(and (NOT-BOARDED-P2)))

(:derived (Flag87-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag88-)(and (NOT-BOARDED-P1)))

(:derived (Flag88-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag89-)(and (NOT-BOARDED-P0)))

(:derived (Flag89-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag95-)(and (NOT-BOARDED-P7)))

(:derived (Flag95-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag96-)(and (NOT-BOARDED-P6)))

(:derived (Flag96-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag97-)(and (NOT-BOARDED-P5)))

(:derived (Flag97-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag98-)(and (NOT-BOARDED-P4)))

(:derived (Flag98-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag99-)(and (NOT-BOARDED-P3)))

(:derived (Flag99-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag100-)(and (NOT-BOARDED-P2)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag101-)(and (NOT-BOARDED-P0)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag104-)(and (NOT-BOARDED-P6)))

(:derived (Flag106-)(and (NOT-BOARDED-P6)))

(:derived (Flag107-)(and (NOT-BOARDED-P7)))

(:derived (Flag107-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag108-)(and (NOT-BOARDED-P6)))

(:derived (Flag108-)(and (NOT-NO-ACCESS-P6-F6)))

(:derived (Flag109-)(and (NOT-BOARDED-P5)))

(:derived (Flag109-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag110-)(and (NOT-BOARDED-P4)))

(:derived (Flag110-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag111-)(and (NOT-BOARDED-P3)))

(:derived (Flag111-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag112-)(and (NOT-BOARDED-P2)))

(:derived (Flag112-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag113-)(and (NOT-BOARDED-P1)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag114-)(and (NOT-BOARDED-P0)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag116-)(and (NOT-BOARDED-P7)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag117-)(and (NOT-BOARDED-P6)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag118-)(and (NOT-BOARDED-P5)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag119-)(and (NOT-BOARDED-P4)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag120-)(and (NOT-BOARDED-P3)))

(:derived (Flag120-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag121-)(and (NOT-BOARDED-P2)))

(:derived (Flag121-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag122-)(and (NOT-BOARDED-P1)))

(:derived (Flag122-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag123-)(and (NOT-BOARDED-P0)))

(:derived (Flag123-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag126-)(and (NOT-BOARDED-P6)))

(:derived (Flag128-)(and (NOT-BOARDED-P6)))

(:derived (Flag129-)(and (NOT-BOARDED-P7)))

(:derived (Flag129-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag130-)(and (NOT-BOARDED-P6)))

(:derived (Flag130-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag131-)(and (NOT-BOARDED-P5)))

(:derived (Flag131-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag132-)(and (NOT-BOARDED-P4)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag133-)(and (NOT-BOARDED-P3)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag134-)(and (NOT-BOARDED-P2)))

(:derived (Flag134-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag135-)(and (NOT-BOARDED-P1)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag136-)(and (NOT-BOARDED-P0)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag139-)(and (NOT-BOARDED-P6)))

(:derived (Flag141-)(and (NOT-BOARDED-P6)))

(:derived (Flag142-)(and (NOT-BOARDED-P7)))

(:derived (Flag142-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag143-)(and (NOT-BOARDED-P6)))

(:derived (Flag143-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag144-)(and (NOT-BOARDED-P5)))

(:derived (Flag144-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag145-)(and (NOT-BOARDED-P4)))

(:derived (Flag145-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag146-)(and (NOT-BOARDED-P3)))

(:derived (Flag146-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag147-)(and (NOT-BOARDED-P2)))

(:derived (Flag147-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag148-)(and (NOT-BOARDED-P1)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag149-)(and (NOT-BOARDED-P0)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag152-)(and (NOT-BOARDED-P6)))

(:derived (Flag154-)(and (NOT-BOARDED-P6)))

(:derived (Flag155-)(and (NOT-BOARDED-P7)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag156-)(and (NOT-BOARDED-P6)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag157-)(and (NOT-BOARDED-P5)))

(:derived (Flag157-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag158-)(and (NOT-BOARDED-P4)))

(:derived (Flag158-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag159-)(and (NOT-BOARDED-P3)))

(:derived (Flag159-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag160-)(and (NOT-BOARDED-P2)))

(:derived (Flag160-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag161-)(and (NOT-BOARDED-P1)))

(:derived (Flag161-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag162-)(and (NOT-BOARDED-P0)))

(:derived (Flag162-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag164-)(and (NOT-BOARDED-P7)))

(:derived (Flag164-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag165-)(and (NOT-BOARDED-P6)))

(:derived (Flag165-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag166-)(and (NOT-BOARDED-P5)))

(:derived (Flag166-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag167-)(and (NOT-BOARDED-P4)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag168-)(and (NOT-BOARDED-P3)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag169-)(and (NOT-BOARDED-P2)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag170-)(and (NOT-BOARDED-P1)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag171-)(and (NOT-BOARDED-P0)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag173-)(and (NOT-BOARDED-P7)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag174-)(and (NOT-BOARDED-P5)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag175-)(and (NOT-BOARDED-P4)))

(:derived (Flag175-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag176-)(and (NOT-BOARDED-P3)))

(:derived (Flag176-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag177-)(and (NOT-BOARDED-P2)))

(:derived (Flag177-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag178-)(and (NOT-BOARDED-P1)))

(:derived (Flag178-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag179-)(and (NOT-BOARDED-P0)))

(:derived (Flag179-)(and (NOT-NO-ACCESS-P0-F0)))

)
