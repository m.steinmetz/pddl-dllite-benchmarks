(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag373-)
(Flag372-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag355-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag236-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag108-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag90-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag72-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag54-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag20-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag3-)
(Flag2-)
(LIFT-AT-F23)
(LIFT-AT-F22)
(LIFT-AT-F21)
(LIFT-AT-F20)
(LIFT-AT-F19)
(LIFT-AT-F18)
(LIFT-AT-F17)
(LIFT-AT-F16)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P0-F16)
(NO-ACCESS-P0-F17)
(NO-ACCESS-P0-F18)
(NO-ACCESS-P0-F19)
(NO-ACCESS-P0-F20)
(NO-ACCESS-P0-F21)
(NO-ACCESS-P0-F22)
(NO-ACCESS-P0-F23)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P1-F16)
(NO-ACCESS-P1-F17)
(NO-ACCESS-P1-F18)
(NO-ACCESS-P1-F19)
(NO-ACCESS-P1-F20)
(NO-ACCESS-P1-F21)
(NO-ACCESS-P1-F22)
(NO-ACCESS-P1-F23)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P2-F16)
(NO-ACCESS-P2-F17)
(NO-ACCESS-P2-F18)
(NO-ACCESS-P2-F19)
(NO-ACCESS-P2-F20)
(NO-ACCESS-P2-F21)
(NO-ACCESS-P2-F23)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F10)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P3-F16)
(NO-ACCESS-P3-F18)
(NO-ACCESS-P3-F19)
(NO-ACCESS-P3-F20)
(NO-ACCESS-P3-F21)
(NO-ACCESS-P3-F22)
(NO-ACCESS-P3-F23)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P4-F16)
(NO-ACCESS-P4-F18)
(NO-ACCESS-P4-F20)
(NO-ACCESS-P4-F21)
(NO-ACCESS-P4-F22)
(NO-ACCESS-P4-F23)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P5-F16)
(NO-ACCESS-P5-F17)
(NO-ACCESS-P5-F18)
(NO-ACCESS-P5-F19)
(NO-ACCESS-P5-F20)
(NO-ACCESS-P5-F21)
(NO-ACCESS-P5-F22)
(NO-ACCESS-P5-F23)
(NO-ACCESS-P6-F0)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F6)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P6-F16)
(NO-ACCESS-P6-F17)
(NO-ACCESS-P6-F18)
(NO-ACCESS-P6-F19)
(NO-ACCESS-P6-F20)
(NO-ACCESS-P6-F21)
(NO-ACCESS-P6-F22)
(NO-ACCESS-P6-F23)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(NO-ACCESS-P7-F16)
(NO-ACCESS-P7-F17)
(NO-ACCESS-P7-F18)
(NO-ACCESS-P7-F19)
(NO-ACCESS-P7-F20)
(NO-ACCESS-P7-F21)
(NO-ACCESS-P7-F22)
(NO-ACCESS-P7-F23)
(NO-ACCESS-P8-F0)
(NO-ACCESS-P8-F1)
(NO-ACCESS-P8-F2)
(NO-ACCESS-P8-F3)
(NO-ACCESS-P8-F4)
(NO-ACCESS-P8-F5)
(NO-ACCESS-P8-F6)
(NO-ACCESS-P8-F8)
(NO-ACCESS-P8-F9)
(NO-ACCESS-P8-F10)
(NO-ACCESS-P8-F11)
(NO-ACCESS-P8-F12)
(NO-ACCESS-P8-F13)
(NO-ACCESS-P8-F14)
(NO-ACCESS-P8-F15)
(NO-ACCESS-P8-F16)
(NO-ACCESS-P8-F17)
(NO-ACCESS-P8-F18)
(NO-ACCESS-P8-F19)
(NO-ACCESS-P8-F20)
(NO-ACCESS-P8-F21)
(NO-ACCESS-P8-F22)
(NO-ACCESS-P8-F23)
(NO-ACCESS-P9-F0)
(NO-ACCESS-P9-F1)
(NO-ACCESS-P9-F2)
(NO-ACCESS-P9-F3)
(NO-ACCESS-P9-F4)
(NO-ACCESS-P9-F5)
(NO-ACCESS-P9-F6)
(NO-ACCESS-P9-F7)
(NO-ACCESS-P9-F9)
(NO-ACCESS-P9-F10)
(NO-ACCESS-P9-F11)
(NO-ACCESS-P9-F12)
(NO-ACCESS-P9-F13)
(NO-ACCESS-P9-F14)
(NO-ACCESS-P9-F15)
(NO-ACCESS-P9-F16)
(NO-ACCESS-P9-F17)
(NO-ACCESS-P9-F18)
(NO-ACCESS-P9-F19)
(NO-ACCESS-P9-F20)
(NO-ACCESS-P9-F21)
(NO-ACCESS-P9-F22)
(NO-ACCESS-P9-F23)
(NO-ACCESS-P10-F0)
(NO-ACCESS-P10-F1)
(NO-ACCESS-P10-F2)
(NO-ACCESS-P10-F3)
(NO-ACCESS-P10-F4)
(NO-ACCESS-P10-F5)
(NO-ACCESS-P10-F6)
(NO-ACCESS-P10-F7)
(NO-ACCESS-P10-F8)
(NO-ACCESS-P10-F9)
(NO-ACCESS-P10-F10)
(NO-ACCESS-P10-F11)
(NO-ACCESS-P10-F12)
(NO-ACCESS-P10-F13)
(NO-ACCESS-P10-F14)
(NO-ACCESS-P10-F15)
(NO-ACCESS-P10-F16)
(NO-ACCESS-P10-F17)
(NO-ACCESS-P10-F18)
(NO-ACCESS-P10-F19)
(NO-ACCESS-P10-F20)
(NO-ACCESS-P10-F21)
(NO-ACCESS-P10-F22)
(NO-ACCESS-P10-F23)
(NO-ACCESS-P11-F0)
(NO-ACCESS-P11-F1)
(NO-ACCESS-P11-F2)
(NO-ACCESS-P11-F3)
(NO-ACCESS-P11-F4)
(NO-ACCESS-P11-F5)
(NO-ACCESS-P11-F6)
(NO-ACCESS-P11-F7)
(NO-ACCESS-P11-F8)
(NO-ACCESS-P11-F9)
(NO-ACCESS-P11-F10)
(NO-ACCESS-P11-F11)
(NO-ACCESS-P11-F12)
(NO-ACCESS-P11-F13)
(NO-ACCESS-P11-F14)
(NO-ACCESS-P11-F15)
(NO-ACCESS-P11-F16)
(NO-ACCESS-P11-F17)
(NO-ACCESS-P11-F18)
(NO-ACCESS-P11-F19)
(NO-ACCESS-P11-F20)
(NO-ACCESS-P11-F21)
(NO-ACCESS-P11-F22)
(NO-ACCESS-P11-F23)
(Flag357-)
(Flag356-)
(Flag300-)
(Flag298-)
(Flag296-)
(Flag256-)
(Flag254-)
(Flag238-)
(Flag237-)
(Flag222-)
(Flag220-)
(Flag205-)
(Flag203-)
(Flag161-)
(Flag159-)
(Flag143-)
(Flag141-)
(Flag126-)
(Flag124-)
(Flag110-)
(Flag109-)
(Flag93-)
(Flag91-)
(Flag75-)
(Flag73-)
(Flag70-)
(Flag38-)
(Flag36-)
(Flag22-)
(Flag21-)
(Flag5-)
(Flag4-)
(SERVED-P0)
(BOARDED-P11)
(SERVED-P7)
(SERVED-P10)
(Flag383-)
(Flag371-)
(Flag354-)
(Flag341-)
(Flag328-)
(Flag302-)
(Flag280-)
(Flag268-)
(Flag252-)
(Flag235-)
(Flag218-)
(Flag201-)
(Flag188-)
(Flag175-)
(Flag160-)
(Flag158-)
(Flag157-)
(Flag139-)
(Flag122-)
(Flag107-)
(Flag92-)
(Flag89-)
(Flag88-)
(Flag74-)
(Flag71-)
(Flag52-)
(Flag34-)
(Flag19-)
(SERVED-P2)
(SERVED-P6)
(BOARDED-P6)
(SERVED-P11)
(BOARDED-P4)
(BOARDED-P8)
(BOARDED-P7)
(BOARDED-P2)
(BOARDED-P3)
(BOARDED-P0)
(BOARDED-P1)
(SERVED-P8)
(BOARDED-P10)
(SERVED-P3)
(BOARDED-P5)
(SERVED-P4)
(SERVED-P9)
(SERVED-P5)
(Flag315-)
(Flag301-)
(Flag299-)
(Flag297-)
(Flag255-)
(Flag253-)
(Flag221-)
(Flag219-)
(Flag204-)
(Flag202-)
(Flag142-)
(Flag140-)
(Flag125-)
(Flag123-)
(Flag55-)
(Flag53-)
(Flag37-)
(Flag35-)
(BOARDED-P9)
(SERVED-P1)
(Flag1-)
(NOT-BOARDED-P9)
(NOT-SERVED-P1)
(NOT-SERVED-P5)
(NOT-SERVED-P9)
(NOT-BOARDED-P5)
(NOT-SERVED-P4)
(NOT-BOARDED-P10)
(NOT-SERVED-P3)
(NOT-BOARDED-P0)
(NOT-BOARDED-P1)
(NOT-SERVED-P8)
(NOT-BOARDED-P3)
(NOT-BOARDED-P2)
(NOT-BOARDED-P7)
(NOT-BOARDED-P8)
(NOT-BOARDED-P4)
(NOT-SERVED-P11)
(NOT-BOARDED-P6)
(NOT-SERVED-P2)
(NOT-SERVED-P6)
(NOT-BOARDED-P11)
(NOT-SERVED-P7)
(NOT-SERVED-P10)
(NOT-SERVED-P0)
(NOT-NO-ACCESS-P11-F23)
(NOT-NO-ACCESS-P11-F22)
(NOT-NO-ACCESS-P11-F21)
(NOT-NO-ACCESS-P11-F20)
(NOT-NO-ACCESS-P11-F19)
(NOT-NO-ACCESS-P11-F18)
(NOT-NO-ACCESS-P11-F17)
(NOT-NO-ACCESS-P11-F16)
(NOT-NO-ACCESS-P11-F15)
(NOT-NO-ACCESS-P11-F14)
(NOT-NO-ACCESS-P11-F13)
(NOT-NO-ACCESS-P11-F12)
(NOT-NO-ACCESS-P11-F11)
(NOT-NO-ACCESS-P11-F10)
(NOT-NO-ACCESS-P11-F9)
(NOT-NO-ACCESS-P11-F8)
(NOT-NO-ACCESS-P11-F7)
(NOT-NO-ACCESS-P11-F6)
(NOT-NO-ACCESS-P11-F5)
(NOT-NO-ACCESS-P11-F4)
(NOT-NO-ACCESS-P11-F3)
(NOT-NO-ACCESS-P11-F2)
(NOT-NO-ACCESS-P11-F1)
(NOT-NO-ACCESS-P11-F0)
(NOT-NO-ACCESS-P10-F23)
(NOT-NO-ACCESS-P10-F22)
(NOT-NO-ACCESS-P10-F21)
(NOT-NO-ACCESS-P10-F20)
(NOT-NO-ACCESS-P10-F19)
(NOT-NO-ACCESS-P10-F18)
(NOT-NO-ACCESS-P10-F17)
(NOT-NO-ACCESS-P10-F16)
(NOT-NO-ACCESS-P10-F15)
(NOT-NO-ACCESS-P10-F14)
(NOT-NO-ACCESS-P10-F13)
(NOT-NO-ACCESS-P10-F12)
(NOT-NO-ACCESS-P10-F11)
(NOT-NO-ACCESS-P10-F10)
(NOT-NO-ACCESS-P10-F9)
(NOT-NO-ACCESS-P10-F8)
(NOT-NO-ACCESS-P10-F7)
(NOT-NO-ACCESS-P10-F6)
(NOT-NO-ACCESS-P10-F5)
(NOT-NO-ACCESS-P10-F4)
(NOT-NO-ACCESS-P10-F3)
(NOT-NO-ACCESS-P10-F2)
(NOT-NO-ACCESS-P10-F1)
(NOT-NO-ACCESS-P10-F0)
(NOT-NO-ACCESS-P9-F23)
(NOT-NO-ACCESS-P9-F22)
(NOT-NO-ACCESS-P9-F21)
(NOT-NO-ACCESS-P9-F20)
(NOT-NO-ACCESS-P9-F19)
(NOT-NO-ACCESS-P9-F18)
(NOT-NO-ACCESS-P9-F17)
(NOT-NO-ACCESS-P9-F16)
(NOT-NO-ACCESS-P9-F15)
(NOT-NO-ACCESS-P9-F14)
(NOT-NO-ACCESS-P9-F13)
(NOT-NO-ACCESS-P9-F12)
(NOT-NO-ACCESS-P9-F11)
(NOT-NO-ACCESS-P9-F10)
(NOT-NO-ACCESS-P9-F9)
(NOT-NO-ACCESS-P9-F7)
(NOT-NO-ACCESS-P9-F6)
(NOT-NO-ACCESS-P9-F5)
(NOT-NO-ACCESS-P9-F4)
(NOT-NO-ACCESS-P9-F3)
(NOT-NO-ACCESS-P9-F2)
(NOT-NO-ACCESS-P9-F1)
(NOT-NO-ACCESS-P9-F0)
(NOT-NO-ACCESS-P8-F23)
(NOT-NO-ACCESS-P8-F22)
(NOT-NO-ACCESS-P8-F21)
(NOT-NO-ACCESS-P8-F20)
(NOT-NO-ACCESS-P8-F19)
(NOT-NO-ACCESS-P8-F18)
(NOT-NO-ACCESS-P8-F17)
(NOT-NO-ACCESS-P8-F16)
(NOT-NO-ACCESS-P8-F15)
(NOT-NO-ACCESS-P8-F14)
(NOT-NO-ACCESS-P8-F13)
(NOT-NO-ACCESS-P8-F12)
(NOT-NO-ACCESS-P8-F11)
(NOT-NO-ACCESS-P8-F10)
(NOT-NO-ACCESS-P8-F9)
(NOT-NO-ACCESS-P8-F8)
(NOT-NO-ACCESS-P8-F6)
(NOT-NO-ACCESS-P8-F5)
(NOT-NO-ACCESS-P8-F4)
(NOT-NO-ACCESS-P8-F3)
(NOT-NO-ACCESS-P8-F2)
(NOT-NO-ACCESS-P8-F1)
(NOT-NO-ACCESS-P8-F0)
(NOT-NO-ACCESS-P7-F23)
(NOT-NO-ACCESS-P7-F22)
(NOT-NO-ACCESS-P7-F21)
(NOT-NO-ACCESS-P7-F20)
(NOT-NO-ACCESS-P7-F19)
(NOT-NO-ACCESS-P7-F18)
(NOT-NO-ACCESS-P7-F17)
(NOT-NO-ACCESS-P7-F16)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F23)
(NOT-NO-ACCESS-P6-F22)
(NOT-NO-ACCESS-P6-F21)
(NOT-NO-ACCESS-P6-F20)
(NOT-NO-ACCESS-P6-F19)
(NOT-NO-ACCESS-P6-F18)
(NOT-NO-ACCESS-P6-F17)
(NOT-NO-ACCESS-P6-F16)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F6)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P6-F0)
(NOT-NO-ACCESS-P5-F23)
(NOT-NO-ACCESS-P5-F22)
(NOT-NO-ACCESS-P5-F21)
(NOT-NO-ACCESS-P5-F20)
(NOT-NO-ACCESS-P5-F19)
(NOT-NO-ACCESS-P5-F18)
(NOT-NO-ACCESS-P5-F17)
(NOT-NO-ACCESS-P5-F16)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F23)
(NOT-NO-ACCESS-P4-F22)
(NOT-NO-ACCESS-P4-F21)
(NOT-NO-ACCESS-P4-F20)
(NOT-NO-ACCESS-P4-F18)
(NOT-NO-ACCESS-P4-F16)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F23)
(NOT-NO-ACCESS-P3-F22)
(NOT-NO-ACCESS-P3-F21)
(NOT-NO-ACCESS-P3-F20)
(NOT-NO-ACCESS-P3-F19)
(NOT-NO-ACCESS-P3-F18)
(NOT-NO-ACCESS-P3-F16)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F10)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F23)
(NOT-NO-ACCESS-P2-F21)
(NOT-NO-ACCESS-P2-F20)
(NOT-NO-ACCESS-P2-F19)
(NOT-NO-ACCESS-P2-F18)
(NOT-NO-ACCESS-P2-F17)
(NOT-NO-ACCESS-P2-F16)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F23)
(NOT-NO-ACCESS-P1-F22)
(NOT-NO-ACCESS-P1-F21)
(NOT-NO-ACCESS-P1-F20)
(NOT-NO-ACCESS-P1-F19)
(NOT-NO-ACCESS-P1-F18)
(NOT-NO-ACCESS-P1-F17)
(NOT-NO-ACCESS-P1-F16)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P0-F23)
(NOT-NO-ACCESS-P0-F22)
(NOT-NO-ACCESS-P0-F21)
(NOT-NO-ACCESS-P0-F20)
(NOT-NO-ACCESS-P0-F19)
(NOT-NO-ACCESS-P0-F18)
(NOT-NO-ACCESS-P0-F17)
(NOT-NO-ACCESS-P0-F16)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)(SERVED-P8)(SERVED-P9)(SERVED-P10)(SERVED-P11)))

(:derived (Flag36-)(and (Flag35-)))

(:derived (Flag38-)(and (Flag37-)))

(:derived (Flag54-)(and (Flag53-)))

(:derived (Flag56-)(and (Flag55-)))

(:derived (Flag124-)(and (Flag123-)))

(:derived (Flag126-)(and (Flag125-)))

(:derived (Flag141-)(and (Flag140-)))

(:derived (Flag143-)(and (Flag142-)))

(:derived (Flag203-)(and (Flag202-)))

(:derived (Flag205-)(and (Flag204-)))

(:derived (Flag220-)(and (Flag219-)))

(:derived (Flag222-)(and (Flag221-)))

(:derived (Flag254-)(and (Flag253-)))

(:derived (Flag256-)(and (Flag255-)))

(:derived (Flag298-)(and (Flag297-)))

(:derived (Flag300-)(and (Flag299-)))

(:derived (Flag302-)(and (Flag301-)))

(:action STOP-F5
:parameters ()
:precondition
(and
(Flag315-)
)
:effect
(and
(when
(and
(NOT-SERVED-P9)
)
(and
(BOARDED-P9)
(not (NOT-BOARDED-P9))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:derived (Flag6-)(and (BOARDED-P10)))

(:derived (Flag6-)(and (BOARDED-P7)))

(:derived (Flag6-)(and (BOARDED-P2)))

(:derived (Flag6-)(and (BOARDED-P1)))

(:derived (Flag6-)(and (BOARDED-P0)))

(:derived (Flag35-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag37-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag39-)(and (BOARDED-P10)))

(:derived (Flag39-)(and (BOARDED-P7)))

(:derived (Flag39-)(and (BOARDED-P5)))

(:derived (Flag39-)(and (BOARDED-P2)))

(:derived (Flag39-)(and (BOARDED-P1)))

(:derived (Flag39-)(and (BOARDED-P0)))

(:derived (Flag53-)(and (SERVED-P11)(NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag55-)(and (NOT-BOARDED-P11)(SERVED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag57-)(and (BOARDED-P5)))

(:derived (Flag57-)(and (BOARDED-P2)))

(:derived (Flag57-)(and (BOARDED-P1)))

(:derived (Flag57-)(and (BOARDED-P0)))

(:derived (Flag73-)(and (Flag71-)))

(:derived (Flag75-)(and (Flag74-)))

(:derived (Flag76-)(and (BOARDED-P10)))

(:derived (Flag76-)(and (BOARDED-P7)))

(:derived (Flag76-)(and (BOARDED-P5)))

(:derived (Flag76-)(and (BOARDED-P2)))

(:derived (Flag76-)(and (BOARDED-P1)))

(:derived (Flag76-)(and (BOARDED-P0)))

(:derived (Flag91-)(and (Flag89-)))

(:derived (Flag93-)(and (Flag92-)))

(:derived (Flag94-)(and (BOARDED-P10)))

(:derived (Flag94-)(and (BOARDED-P7)))

(:derived (Flag94-)(and (BOARDED-P5)))

(:derived (Flag94-)(and (BOARDED-P2)))

(:derived (Flag94-)(and (BOARDED-P1)))

(:derived (Flag94-)(and (BOARDED-P0)))

(:derived (Flag111-)(and (BOARDED-P10)))

(:derived (Flag111-)(and (BOARDED-P7)))

(:derived (Flag111-)(and (BOARDED-P5)))

(:derived (Flag111-)(and (BOARDED-P2)))

(:derived (Flag111-)(and (BOARDED-P1)))

(:derived (Flag111-)(and (BOARDED-P0)))

(:derived (Flag123-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag125-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P2)))

(:derived (Flag140-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P2)(NOT-BOARDED-P2)))

(:derived (Flag142-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P2)))

(:derived (Flag144-)(and (BOARDED-P10)))

(:derived (Flag144-)(and (BOARDED-P7)))

(:derived (Flag144-)(and (BOARDED-P5)))

(:derived (Flag144-)(and (BOARDED-P2)))

(:derived (Flag144-)(and (BOARDED-P1)))

(:derived (Flag144-)(and (BOARDED-P0)))

(:derived (Flag159-)(and (Flag158-)))

(:derived (Flag161-)(and (Flag160-)))

(:derived (Flag162-)(and (BOARDED-P10)))

(:derived (Flag162-)(and (BOARDED-P7)))

(:derived (Flag162-)(and (BOARDED-P5)))

(:derived (Flag162-)(and (BOARDED-P2)))

(:derived (Flag162-)(and (BOARDED-P1)))

(:derived (Flag162-)(and (BOARDED-P0)))

(:derived (Flag202-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(SERVED-P8)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag204-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag219-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P4)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag221-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag239-)(and (BOARDED-P10)))

(:derived (Flag239-)(and (BOARDED-P7)))

(:derived (Flag239-)(and (BOARDED-P5)))

(:derived (Flag239-)(and (BOARDED-P2)))

(:derived (Flag239-)(and (BOARDED-P1)))

(:derived (Flag239-)(and (BOARDED-P0)))

(:derived (Flag253-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(SERVED-P6)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag255-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(SERVED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag283-)(and (BOARDED-P10)))

(:derived (Flag283-)(and (BOARDED-P7)))

(:derived (Flag283-)(and (BOARDED-P5)))

(:derived (Flag283-)(and (BOARDED-P2)))

(:derived (Flag283-)(and (BOARDED-P1)))

(:derived (Flag297-)(and (NOT-BOARDED-P11)(SERVED-P9)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag299-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(SERVED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag301-)(and (NOT-BOARDED-P9)(SERVED-P9)))

(:derived (Flag302-)(and (BOARDED-P10)))

(:derived (Flag302-)(and (BOARDED-P7)))

(:derived (Flag302-)(and (BOARDED-P5)))

(:derived (Flag302-)(and (BOARDED-P2)))

(:derived (Flag302-)(and (BOARDED-P0)))

(:derived (Flag315-)(and (LIFT-AT-F5)(Flag298-)(Flag300-)(Flag302-)(Flag303-)(Flag304-)(Flag305-)(Flag306-)(Flag307-)(Flag308-)(Flag309-)(Flag310-)(Flag311-)(Flag312-)(Flag313-)(Flag314-)))

(:derived (Flag358-)(and (BOARDED-P10)))

(:derived (Flag358-)(and (BOARDED-P7)))

(:derived (Flag358-)(and (BOARDED-P5)))

(:derived (Flag358-)(and (BOARDED-P1)))

(:derived (Flag358-)(and (BOARDED-P0)))

(:action STOP-F23
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F22
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(when
(and
(BOARDED-P9)
)
(and
(NOT-BOARDED-P9)
(SERVED-P9)
(not (BOARDED-P9))
(not (NOT-SERVED-P9))
)
)
)
)
(:action STOP-F21
:parameters ()
:precondition
(and
(Flag52-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action STOP-F19
:parameters ()
:precondition
(and
(Flag88-)
)
:effect
(and
(when
(and
(NOT-SERVED-P10)
)
(and
(BOARDED-P10)
(not (NOT-BOARDED-P10))
)
)
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F18
:parameters ()
:precondition
(and
(Flag107-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(when
(and
(BOARDED-P8)
)
(and
(NOT-BOARDED-P8)
(SERVED-P8)
(not (BOARDED-P8))
(not (NOT-SERVED-P8))
)
)
)
)
(:action STOP-F17
:parameters ()
:precondition
(and
(Flag122-)
)
:effect
(and
)
)
(:action STOP-F16
:parameters ()
:precondition
(and
(Flag139-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
)
)
(:action STOP-F15
:parameters ()
:precondition
(and
(Flag157-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F14
:parameters ()
:precondition
(and
(Flag175-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
)
)
(:action STOP-F13
:parameters ()
:precondition
(and
(Flag188-)
)
:effect
(and
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag201-)
)
:effect
(and
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag218-)
)
:effect
(and
(when
(and
(NOT-SERVED-P8)
)
(and
(BOARDED-P8)
(not (NOT-BOARDED-P8))
)
)
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag235-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag252-)
)
:effect
(and
(when
(and
(BOARDED-P11)
)
(and
(NOT-BOARDED-P11)
(SERVED-P11)
(not (BOARDED-P11))
(not (NOT-SERVED-P11))
)
)
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag268-)
)
:effect
(and
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag280-)
)
:effect
(and
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag328-)
)
:effect
(and
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag341-)
)
:effect
(and
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag354-)
)
:effect
(and
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag371-)
)
:effect
(and
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag383-)
)
:effect
(and
)
)
(:derived (Flag6-)(and (BOARDED-P11)))

(:derived (Flag19-)(and (LIFT-AT-F23)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)))

(:derived (Flag34-)(and (LIFT-AT-F22)(Flag21-)(Flag22-)(Flag23-)(Flag24-)(Flag25-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(NOT-BOARDED-P2)(Flag32-)(Flag33-)))

(:derived (Flag39-)(and (BOARDED-P11)))

(:derived (Flag52-)(and (LIFT-AT-F21)(Flag36-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)))

(:derived (Flag57-)(and (BOARDED-P11)))

(:derived (Flag71-)(and (NOT-BOARDED-P10)(SERVED-P10)(NOT-BOARDED-P0)))

(:derived (Flag74-)(and (SERVED-P10)(NOT-BOARDED-P10)(NOT-BOARDED-P0)))

(:derived (Flag76-)(and (BOARDED-P11)))

(:derived (Flag88-)(and (LIFT-AT-F19)(Flag73-)(Flag75-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(NOT-BOARDED-P4)(Flag84-)(Flag85-)(Flag86-)(Flag87-)))

(:derived (Flag89-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag92-)(and (NOT-BOARDED-P10)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag94-)(and (BOARDED-P11)))

(:derived (Flag107-)(and (LIFT-AT-F18)(Flag91-)(Flag93-)(Flag94-)(Flag95-)(Flag96-)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)))

(:derived (Flag111-)(and (BOARDED-P11)))

(:derived (Flag122-)(and (LIFT-AT-F17)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(Flag119-)(Flag120-)(Flag121-)))

(:derived (Flag139-)(and (LIFT-AT-F16)(Flag124-)(Flag126-)(Flag111-)(Flag127-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)))

(:derived (Flag144-)(and (BOARDED-P11)))

(:derived (Flag157-)(and (LIFT-AT-F15)(Flag141-)(Flag143-)(Flag144-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)(Flag150-)(Flag151-)(Flag152-)(Flag153-)(Flag154-)(Flag155-)(Flag156-)))

(:derived (Flag158-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P7)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag160-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(SERVED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag162-)(and (BOARDED-P11)))

(:derived (Flag175-)(and (LIFT-AT-F14)(Flag159-)(Flag161-)(Flag162-)(Flag163-)(Flag164-)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)))

(:derived (Flag188-)(and (LIFT-AT-F13)(Flag109-)(Flag110-)(Flag111-)(Flag176-)(Flag177-)(Flag178-)(Flag179-)(Flag180-)(Flag181-)(Flag182-)(Flag183-)(Flag184-)(Flag185-)(Flag186-)(Flag187-)))

(:derived (Flag201-)(and (LIFT-AT-F12)(Flag109-)(Flag110-)(Flag111-)(Flag189-)(Flag190-)(Flag191-)(Flag192-)(Flag193-)(Flag194-)(Flag195-)(Flag196-)(Flag197-)(Flag198-)(Flag199-)(Flag200-)))

(:derived (Flag218-)(and (LIFT-AT-F11)(Flag203-)(Flag205-)(Flag111-)(Flag206-)(Flag207-)(Flag208-)(Flag209-)(Flag210-)(Flag211-)(Flag212-)(Flag213-)(Flag214-)(Flag215-)(Flag216-)(Flag217-)))

(:derived (Flag235-)(and (LIFT-AT-F10)(Flag220-)(Flag222-)(Flag111-)(Flag223-)(Flag224-)(Flag225-)(Flag226-)(Flag227-)(Flag228-)(Flag229-)(Flag230-)(Flag231-)(Flag232-)(Flag233-)(Flag234-)))

(:derived (Flag252-)(and (LIFT-AT-F9)(Flag237-)(Flag238-)(Flag239-)(Flag240-)(Flag241-)(Flag242-)(Flag243-)(Flag244-)(Flag245-)(Flag246-)(Flag247-)(Flag248-)(Flag249-)(Flag250-)(Flag251-)))

(:derived (Flag268-)(and (LIFT-AT-F8)(Flag254-)(Flag256-)(Flag111-)(Flag257-)(Flag258-)(NOT-BOARDED-P9)(Flag259-)(Flag260-)(Flag261-)(Flag262-)(Flag263-)(Flag264-)(Flag265-)(Flag266-)(Flag267-)))

(:derived (Flag280-)(and (LIFT-AT-F7)(Flag109-)(Flag110-)(Flag111-)(Flag269-)(Flag270-)(Flag271-)(NOT-BOARDED-P8)(Flag272-)(Flag273-)(Flag274-)(Flag275-)(Flag276-)(Flag277-)(Flag278-)(Flag279-)))

(:derived (Flag283-)(and (BOARDED-P11)))

(:derived (Flag302-)(and (BOARDED-P11)))

(:derived (Flag328-)(and (LIFT-AT-F4)(Flag109-)(Flag110-)(Flag111-)(Flag316-)(Flag317-)(Flag318-)(Flag319-)(Flag320-)(Flag321-)(Flag322-)(Flag323-)(Flag324-)(Flag325-)(Flag326-)(Flag327-)))

(:derived (Flag341-)(and (LIFT-AT-F3)(Flag109-)(Flag110-)(Flag111-)(Flag329-)(Flag330-)(Flag331-)(Flag332-)(Flag333-)(Flag334-)(Flag335-)(Flag336-)(Flag337-)(Flag338-)(Flag339-)(Flag340-)))

(:derived (Flag354-)(and (LIFT-AT-F2)(Flag109-)(Flag110-)(Flag111-)(Flag342-)(Flag343-)(Flag344-)(Flag345-)(Flag346-)(Flag347-)(Flag348-)(Flag349-)(Flag350-)(Flag351-)(Flag352-)(Flag353-)))

(:derived (Flag358-)(and (BOARDED-P11)))

(:derived (Flag371-)(and (LIFT-AT-F1)(Flag356-)(Flag357-)(Flag358-)(Flag359-)(Flag360-)(Flag361-)(Flag362-)(Flag363-)(Flag364-)(Flag365-)(Flag366-)(Flag367-)(Flag368-)(Flag369-)(Flag370-)))

(:derived (Flag383-)(and (LIFT-AT-F0)(Flag109-)(Flag110-)(Flag111-)(Flag372-)(Flag373-)(Flag374-)(Flag375-)(Flag376-)(Flag377-)(Flag378-)(Flag379-)(Flag380-)(Flag381-)(NOT-BOARDED-P1)(Flag382-)))

(:action STOP-F20
:parameters ()
:precondition
(and
(Flag70-)
)
:effect
(and
(when
(and
(NOT-SERVED-P11)
)
(and
(BOARDED-P11)
(not (NOT-BOARDED-P11))
)
)
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
(when
(and
(BOARDED-P10)
)
(and
(NOT-BOARDED-P10)
(SERVED-P10)
(not (BOARDED-P10))
(not (NOT-SERVED-P10))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag296-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F16
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F17
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F18
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F19
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F20
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F21
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F22
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F23
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F16
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F17
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F18
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F19
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F20
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F21
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F22
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F23
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F16
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F17
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F18
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F19
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F20
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F21
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F22
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F23
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F16
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F17
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F18
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F19
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F20
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F21
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F22
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F23
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F16
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F17
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F18
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F19
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F20
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F21
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F22
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F23
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F16
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F17
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F18
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F19
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F20
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F21
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F22
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F23
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F16
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F17
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F18
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F19
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F20
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F21
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F22
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F23
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F16
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F17
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F18
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F19
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F20
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F21
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F22
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F23
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F16
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F17
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F18
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F19
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F20
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F21
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F22
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F23
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F16
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F17
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F18
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F19
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F20
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F21
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F22
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F23
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F16
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F17
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F18
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F19
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F20
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F21
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F22
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F23
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F16
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F17
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F18
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F19
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F20
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F21
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F22
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F23
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F16
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F17
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F18
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F19
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F20
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F21
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F22
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F23
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F16
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F17
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F18
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F19
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F20
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F21
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F22
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F23
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F14))
)
)
(:action UP-F15-F16
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F17
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F18
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F19
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F20
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F21
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F22
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F23
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F15))
)
)
(:action UP-F16-F17
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F18
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F19
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F20
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F21
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F22
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F23
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F16))
)
)
(:action UP-F17-F18
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F19
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F20
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F21
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F22
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F23
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F17))
)
)
(:action UP-F18-F19
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F20
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F21
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F22
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F23
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F18))
)
)
(:action UP-F19-F20
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F21
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F22
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F23
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F19))
)
)
(:action UP-F20-F21
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F22
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F23
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F20))
)
)
(:action UP-F21-F22
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F21))
)
)
(:action UP-F21-F23
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F21))
)
)
(:action UP-F22-F23
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F22))
)
)
(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag2-)))

(:derived (Flag21-)(and (Flag2-)))

(:derived (Flag21-)(and (Flag20-)))

(:derived (Flag22-)(and (Flag20-)))

(:derived (Flag22-)(and (Flag2-)))

(:derived (Flag36-)(and (Flag2-)))

(:derived (Flag38-)(and (Flag2-)))

(:derived (Flag70-)(and (LIFT-AT-F20)(Flag54-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)))

(:derived (Flag73-)(and (Flag72-)))

(:derived (Flag75-)(and (Flag72-)))

(:derived (Flag91-)(and (Flag90-)))

(:derived (Flag93-)(and (Flag90-)))

(:derived (Flag109-)(and (Flag2-)))

(:derived (Flag109-)(and (Flag108-)))

(:derived (Flag110-)(and (Flag108-)))

(:derived (Flag110-)(and (Flag2-)))

(:derived (Flag124-)(and (Flag2-)))

(:derived (Flag126-)(and (Flag2-)))

(:derived (Flag141-)(and (Flag2-)))

(:derived (Flag143-)(and (Flag2-)))

(:derived (Flag159-)(and (Flag2-)))

(:derived (Flag161-)(and (Flag2-)))

(:derived (Flag203-)(and (Flag2-)))

(:derived (Flag205-)(and (Flag2-)))

(:derived (Flag220-)(and (Flag2-)))

(:derived (Flag222-)(and (Flag2-)))

(:derived (Flag237-)(and (Flag2-)))

(:derived (Flag237-)(and (Flag236-)))

(:derived (Flag238-)(and (Flag236-)))

(:derived (Flag238-)(and (Flag2-)))

(:derived (Flag254-)(and (Flag2-)))

(:derived (Flag256-)(and (Flag2-)))

(:derived (Flag281-)(and (Flag108-)))

(:derived (Flag282-)(and (Flag108-)))

(:derived (Flag296-)(and (LIFT-AT-F6)(Flag281-)(Flag282-)(Flag283-)(Flag284-)(Flag285-)(Flag286-)(Flag287-)(Flag288-)(Flag289-)(Flag290-)(Flag291-)(Flag292-)(Flag293-)(Flag294-)(Flag295-)))

(:derived (Flag298-)(and (Flag2-)))

(:derived (Flag300-)(and (Flag2-)))

(:derived (Flag356-)(and (Flag2-)))

(:derived (Flag356-)(and (Flag355-)))

(:derived (Flag357-)(and (Flag355-)))

(:derived (Flag357-)(and (Flag2-)))

(:action BLOCK-ACCESS-P11-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F23)
)
:effect
(and
(NO-ACCESS-P11-F23)
(not (NOT-NO-ACCESS-P11-F23))
)
)
(:action BLOCK-ACCESS-P11-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F22)
)
:effect
(and
(NO-ACCESS-P11-F22)
(not (NOT-NO-ACCESS-P11-F22))
)
)
(:action BLOCK-ACCESS-P11-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F21)
)
:effect
(and
(NO-ACCESS-P11-F21)
(not (NOT-NO-ACCESS-P11-F21))
)
)
(:action BLOCK-ACCESS-P11-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F20)
)
:effect
(and
(NO-ACCESS-P11-F20)
(not (NOT-NO-ACCESS-P11-F20))
)
)
(:action BLOCK-ACCESS-P11-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F19)
)
:effect
(and
(NO-ACCESS-P11-F19)
(not (NOT-NO-ACCESS-P11-F19))
)
)
(:action BLOCK-ACCESS-P11-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F18)
)
:effect
(and
(NO-ACCESS-P11-F18)
(not (NOT-NO-ACCESS-P11-F18))
)
)
(:action BLOCK-ACCESS-P11-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F17)
)
:effect
(and
(NO-ACCESS-P11-F17)
(not (NOT-NO-ACCESS-P11-F17))
)
)
(:action BLOCK-ACCESS-P11-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F16)
)
:effect
(and
(NO-ACCESS-P11-F16)
(not (NOT-NO-ACCESS-P11-F16))
)
)
(:action BLOCK-ACCESS-P11-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F15)
)
:effect
(and
(NO-ACCESS-P11-F15)
(not (NOT-NO-ACCESS-P11-F15))
)
)
(:action BLOCK-ACCESS-P11-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F14)
)
:effect
(and
(NO-ACCESS-P11-F14)
(not (NOT-NO-ACCESS-P11-F14))
)
)
(:action BLOCK-ACCESS-P11-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F13)
)
:effect
(and
(NO-ACCESS-P11-F13)
(not (NOT-NO-ACCESS-P11-F13))
)
)
(:action BLOCK-ACCESS-P11-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F12)
)
:effect
(and
(NO-ACCESS-P11-F12)
(not (NOT-NO-ACCESS-P11-F12))
)
)
(:action BLOCK-ACCESS-P11-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F11)
)
:effect
(and
(NO-ACCESS-P11-F11)
(not (NOT-NO-ACCESS-P11-F11))
)
)
(:action BLOCK-ACCESS-P11-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F10)
)
:effect
(and
(NO-ACCESS-P11-F10)
(not (NOT-NO-ACCESS-P11-F10))
)
)
(:action BLOCK-ACCESS-P11-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F9)
)
:effect
(and
(NO-ACCESS-P11-F9)
(not (NOT-NO-ACCESS-P11-F9))
)
)
(:action BLOCK-ACCESS-P11-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F8)
)
:effect
(and
(NO-ACCESS-P11-F8)
(not (NOT-NO-ACCESS-P11-F8))
)
)
(:action BLOCK-ACCESS-P11-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F7)
)
:effect
(and
(NO-ACCESS-P11-F7)
(not (NOT-NO-ACCESS-P11-F7))
)
)
(:action BLOCK-ACCESS-P11-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F6)
)
:effect
(and
(NO-ACCESS-P11-F6)
(not (NOT-NO-ACCESS-P11-F6))
)
)
(:action BLOCK-ACCESS-P11-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F5)
)
:effect
(and
(NO-ACCESS-P11-F5)
(not (NOT-NO-ACCESS-P11-F5))
)
)
(:action BLOCK-ACCESS-P11-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F4)
)
:effect
(and
(NO-ACCESS-P11-F4)
(not (NOT-NO-ACCESS-P11-F4))
)
)
(:action BLOCK-ACCESS-P11-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F3)
)
:effect
(and
(NO-ACCESS-P11-F3)
(not (NOT-NO-ACCESS-P11-F3))
)
)
(:action BLOCK-ACCESS-P11-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F2)
)
:effect
(and
(NO-ACCESS-P11-F2)
(not (NOT-NO-ACCESS-P11-F2))
)
)
(:action BLOCK-ACCESS-P11-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F1)
)
:effect
(and
(NO-ACCESS-P11-F1)
(not (NOT-NO-ACCESS-P11-F1))
)
)
(:action BLOCK-ACCESS-P11-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F0)
)
:effect
(and
(NO-ACCESS-P11-F0)
(not (NOT-NO-ACCESS-P11-F0))
)
)
(:action BLOCK-ACCESS-P10-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F23)
)
:effect
(and
(NO-ACCESS-P10-F23)
(not (NOT-NO-ACCESS-P10-F23))
)
)
(:action BLOCK-ACCESS-P10-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F22)
)
:effect
(and
(NO-ACCESS-P10-F22)
(not (NOT-NO-ACCESS-P10-F22))
)
)
(:action BLOCK-ACCESS-P10-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F21)
)
:effect
(and
(NO-ACCESS-P10-F21)
(not (NOT-NO-ACCESS-P10-F21))
)
)
(:action BLOCK-ACCESS-P10-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F20)
)
:effect
(and
(NO-ACCESS-P10-F20)
(not (NOT-NO-ACCESS-P10-F20))
)
)
(:action BLOCK-ACCESS-P10-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F19)
)
:effect
(and
(NO-ACCESS-P10-F19)
(not (NOT-NO-ACCESS-P10-F19))
)
)
(:action BLOCK-ACCESS-P10-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F18)
)
:effect
(and
(NO-ACCESS-P10-F18)
(not (NOT-NO-ACCESS-P10-F18))
)
)
(:action BLOCK-ACCESS-P10-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F17)
)
:effect
(and
(NO-ACCESS-P10-F17)
(not (NOT-NO-ACCESS-P10-F17))
)
)
(:action BLOCK-ACCESS-P10-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F16)
)
:effect
(and
(NO-ACCESS-P10-F16)
(not (NOT-NO-ACCESS-P10-F16))
)
)
(:action BLOCK-ACCESS-P10-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F15)
)
:effect
(and
(NO-ACCESS-P10-F15)
(not (NOT-NO-ACCESS-P10-F15))
)
)
(:action BLOCK-ACCESS-P10-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F14)
)
:effect
(and
(NO-ACCESS-P10-F14)
(not (NOT-NO-ACCESS-P10-F14))
)
)
(:action BLOCK-ACCESS-P10-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F13)
)
:effect
(and
(NO-ACCESS-P10-F13)
(not (NOT-NO-ACCESS-P10-F13))
)
)
(:action BLOCK-ACCESS-P10-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F12)
)
:effect
(and
(NO-ACCESS-P10-F12)
(not (NOT-NO-ACCESS-P10-F12))
)
)
(:action BLOCK-ACCESS-P10-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F11)
)
:effect
(and
(NO-ACCESS-P10-F11)
(not (NOT-NO-ACCESS-P10-F11))
)
)
(:action BLOCK-ACCESS-P10-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F10)
)
:effect
(and
(NO-ACCESS-P10-F10)
(not (NOT-NO-ACCESS-P10-F10))
)
)
(:action BLOCK-ACCESS-P10-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F9)
)
:effect
(and
(NO-ACCESS-P10-F9)
(not (NOT-NO-ACCESS-P10-F9))
)
)
(:action BLOCK-ACCESS-P10-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F8)
)
:effect
(and
(NO-ACCESS-P10-F8)
(not (NOT-NO-ACCESS-P10-F8))
)
)
(:action BLOCK-ACCESS-P10-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F7)
)
:effect
(and
(NO-ACCESS-P10-F7)
(not (NOT-NO-ACCESS-P10-F7))
)
)
(:action BLOCK-ACCESS-P10-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F6)
)
:effect
(and
(NO-ACCESS-P10-F6)
(not (NOT-NO-ACCESS-P10-F6))
)
)
(:action BLOCK-ACCESS-P10-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F5)
)
:effect
(and
(NO-ACCESS-P10-F5)
(not (NOT-NO-ACCESS-P10-F5))
)
)
(:action BLOCK-ACCESS-P10-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F4)
)
:effect
(and
(NO-ACCESS-P10-F4)
(not (NOT-NO-ACCESS-P10-F4))
)
)
(:action BLOCK-ACCESS-P10-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F3)
)
:effect
(and
(NO-ACCESS-P10-F3)
(not (NOT-NO-ACCESS-P10-F3))
)
)
(:action BLOCK-ACCESS-P10-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F2)
)
:effect
(and
(NO-ACCESS-P10-F2)
(not (NOT-NO-ACCESS-P10-F2))
)
)
(:action BLOCK-ACCESS-P10-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F1)
)
:effect
(and
(NO-ACCESS-P10-F1)
(not (NOT-NO-ACCESS-P10-F1))
)
)
(:action BLOCK-ACCESS-P10-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F0)
)
:effect
(and
(NO-ACCESS-P10-F0)
(not (NOT-NO-ACCESS-P10-F0))
)
)
(:action BLOCK-ACCESS-P9-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F23)
)
:effect
(and
(NO-ACCESS-P9-F23)
(not (NOT-NO-ACCESS-P9-F23))
)
)
(:action BLOCK-ACCESS-P9-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F22)
)
:effect
(and
(NO-ACCESS-P9-F22)
(not (NOT-NO-ACCESS-P9-F22))
)
)
(:action BLOCK-ACCESS-P9-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F21)
)
:effect
(and
(NO-ACCESS-P9-F21)
(not (NOT-NO-ACCESS-P9-F21))
)
)
(:action BLOCK-ACCESS-P9-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F20)
)
:effect
(and
(NO-ACCESS-P9-F20)
(not (NOT-NO-ACCESS-P9-F20))
)
)
(:action BLOCK-ACCESS-P9-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F19)
)
:effect
(and
(NO-ACCESS-P9-F19)
(not (NOT-NO-ACCESS-P9-F19))
)
)
(:action BLOCK-ACCESS-P9-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F18)
)
:effect
(and
(NO-ACCESS-P9-F18)
(not (NOT-NO-ACCESS-P9-F18))
)
)
(:action BLOCK-ACCESS-P9-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F17)
)
:effect
(and
(NO-ACCESS-P9-F17)
(not (NOT-NO-ACCESS-P9-F17))
)
)
(:action BLOCK-ACCESS-P9-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F16)
)
:effect
(and
(NO-ACCESS-P9-F16)
(not (NOT-NO-ACCESS-P9-F16))
)
)
(:action BLOCK-ACCESS-P9-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F15)
)
:effect
(and
(NO-ACCESS-P9-F15)
(not (NOT-NO-ACCESS-P9-F15))
)
)
(:action BLOCK-ACCESS-P9-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F14)
)
:effect
(and
(NO-ACCESS-P9-F14)
(not (NOT-NO-ACCESS-P9-F14))
)
)
(:action BLOCK-ACCESS-P9-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F13)
)
:effect
(and
(NO-ACCESS-P9-F13)
(not (NOT-NO-ACCESS-P9-F13))
)
)
(:action BLOCK-ACCESS-P9-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F12)
)
:effect
(and
(NO-ACCESS-P9-F12)
(not (NOT-NO-ACCESS-P9-F12))
)
)
(:action BLOCK-ACCESS-P9-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F11)
)
:effect
(and
(NO-ACCESS-P9-F11)
(not (NOT-NO-ACCESS-P9-F11))
)
)
(:action BLOCK-ACCESS-P9-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F10)
)
:effect
(and
(NO-ACCESS-P9-F10)
(not (NOT-NO-ACCESS-P9-F10))
)
)
(:action BLOCK-ACCESS-P9-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F9)
)
:effect
(and
(NO-ACCESS-P9-F9)
(not (NOT-NO-ACCESS-P9-F9))
)
)
(:action BLOCK-ACCESS-P9-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F7)
)
:effect
(and
(NO-ACCESS-P9-F7)
(not (NOT-NO-ACCESS-P9-F7))
)
)
(:action BLOCK-ACCESS-P9-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F6)
)
:effect
(and
(NO-ACCESS-P9-F6)
(not (NOT-NO-ACCESS-P9-F6))
)
)
(:action BLOCK-ACCESS-P9-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F5)
)
:effect
(and
(NO-ACCESS-P9-F5)
(not (NOT-NO-ACCESS-P9-F5))
)
)
(:action BLOCK-ACCESS-P9-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F4)
)
:effect
(and
(NO-ACCESS-P9-F4)
(not (NOT-NO-ACCESS-P9-F4))
)
)
(:action BLOCK-ACCESS-P9-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F3)
)
:effect
(and
(NO-ACCESS-P9-F3)
(not (NOT-NO-ACCESS-P9-F3))
)
)
(:action BLOCK-ACCESS-P9-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F2)
)
:effect
(and
(NO-ACCESS-P9-F2)
(not (NOT-NO-ACCESS-P9-F2))
)
)
(:action BLOCK-ACCESS-P9-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F1)
)
:effect
(and
(NO-ACCESS-P9-F1)
(not (NOT-NO-ACCESS-P9-F1))
)
)
(:action BLOCK-ACCESS-P9-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F0)
)
:effect
(and
(NO-ACCESS-P9-F0)
(not (NOT-NO-ACCESS-P9-F0))
)
)
(:action BLOCK-ACCESS-P8-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F23)
)
:effect
(and
(NO-ACCESS-P8-F23)
(not (NOT-NO-ACCESS-P8-F23))
)
)
(:action BLOCK-ACCESS-P8-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F22)
)
:effect
(and
(NO-ACCESS-P8-F22)
(not (NOT-NO-ACCESS-P8-F22))
)
)
(:action BLOCK-ACCESS-P8-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F21)
)
:effect
(and
(NO-ACCESS-P8-F21)
(not (NOT-NO-ACCESS-P8-F21))
)
)
(:action BLOCK-ACCESS-P8-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F20)
)
:effect
(and
(NO-ACCESS-P8-F20)
(not (NOT-NO-ACCESS-P8-F20))
)
)
(:action BLOCK-ACCESS-P8-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F19)
)
:effect
(and
(NO-ACCESS-P8-F19)
(not (NOT-NO-ACCESS-P8-F19))
)
)
(:action BLOCK-ACCESS-P8-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F18)
)
:effect
(and
(NO-ACCESS-P8-F18)
(not (NOT-NO-ACCESS-P8-F18))
)
)
(:action BLOCK-ACCESS-P8-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F17)
)
:effect
(and
(NO-ACCESS-P8-F17)
(not (NOT-NO-ACCESS-P8-F17))
)
)
(:action BLOCK-ACCESS-P8-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F16)
)
:effect
(and
(NO-ACCESS-P8-F16)
(not (NOT-NO-ACCESS-P8-F16))
)
)
(:action BLOCK-ACCESS-P8-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F15)
)
:effect
(and
(NO-ACCESS-P8-F15)
(not (NOT-NO-ACCESS-P8-F15))
)
)
(:action BLOCK-ACCESS-P8-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F14)
)
:effect
(and
(NO-ACCESS-P8-F14)
(not (NOT-NO-ACCESS-P8-F14))
)
)
(:action BLOCK-ACCESS-P8-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F13)
)
:effect
(and
(NO-ACCESS-P8-F13)
(not (NOT-NO-ACCESS-P8-F13))
)
)
(:action BLOCK-ACCESS-P8-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F12)
)
:effect
(and
(NO-ACCESS-P8-F12)
(not (NOT-NO-ACCESS-P8-F12))
)
)
(:action BLOCK-ACCESS-P8-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F11)
)
:effect
(and
(NO-ACCESS-P8-F11)
(not (NOT-NO-ACCESS-P8-F11))
)
)
(:action BLOCK-ACCESS-P8-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F10)
)
:effect
(and
(NO-ACCESS-P8-F10)
(not (NOT-NO-ACCESS-P8-F10))
)
)
(:action BLOCK-ACCESS-P8-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F9)
)
:effect
(and
(NO-ACCESS-P8-F9)
(not (NOT-NO-ACCESS-P8-F9))
)
)
(:action BLOCK-ACCESS-P8-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F8)
)
:effect
(and
(NO-ACCESS-P8-F8)
(not (NOT-NO-ACCESS-P8-F8))
)
)
(:action BLOCK-ACCESS-P8-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F6)
)
:effect
(and
(NO-ACCESS-P8-F6)
(not (NOT-NO-ACCESS-P8-F6))
)
)
(:action BLOCK-ACCESS-P8-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F5)
)
:effect
(and
(NO-ACCESS-P8-F5)
(not (NOT-NO-ACCESS-P8-F5))
)
)
(:action BLOCK-ACCESS-P8-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F4)
)
:effect
(and
(NO-ACCESS-P8-F4)
(not (NOT-NO-ACCESS-P8-F4))
)
)
(:action BLOCK-ACCESS-P8-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F3)
)
:effect
(and
(NO-ACCESS-P8-F3)
(not (NOT-NO-ACCESS-P8-F3))
)
)
(:action BLOCK-ACCESS-P8-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F2)
)
:effect
(and
(NO-ACCESS-P8-F2)
(not (NOT-NO-ACCESS-P8-F2))
)
)
(:action BLOCK-ACCESS-P8-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F1)
)
:effect
(and
(NO-ACCESS-P8-F1)
(not (NOT-NO-ACCESS-P8-F1))
)
)
(:action BLOCK-ACCESS-P8-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F0)
)
:effect
(and
(NO-ACCESS-P8-F0)
(not (NOT-NO-ACCESS-P8-F0))
)
)
(:action BLOCK-ACCESS-P7-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F23)
)
:effect
(and
(NO-ACCESS-P7-F23)
(not (NOT-NO-ACCESS-P7-F23))
)
)
(:action BLOCK-ACCESS-P7-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F22)
)
:effect
(and
(NO-ACCESS-P7-F22)
(not (NOT-NO-ACCESS-P7-F22))
)
)
(:action BLOCK-ACCESS-P7-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F21)
)
:effect
(and
(NO-ACCESS-P7-F21)
(not (NOT-NO-ACCESS-P7-F21))
)
)
(:action BLOCK-ACCESS-P7-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F20)
)
:effect
(and
(NO-ACCESS-P7-F20)
(not (NOT-NO-ACCESS-P7-F20))
)
)
(:action BLOCK-ACCESS-P7-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F19)
)
:effect
(and
(NO-ACCESS-P7-F19)
(not (NOT-NO-ACCESS-P7-F19))
)
)
(:action BLOCK-ACCESS-P7-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F18)
)
:effect
(and
(NO-ACCESS-P7-F18)
(not (NOT-NO-ACCESS-P7-F18))
)
)
(:action BLOCK-ACCESS-P7-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F17)
)
:effect
(and
(NO-ACCESS-P7-F17)
(not (NOT-NO-ACCESS-P7-F17))
)
)
(:action BLOCK-ACCESS-P7-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F16)
)
:effect
(and
(NO-ACCESS-P7-F16)
(not (NOT-NO-ACCESS-P7-F16))
)
)
(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F23)
)
:effect
(and
(NO-ACCESS-P6-F23)
(not (NOT-NO-ACCESS-P6-F23))
)
)
(:action BLOCK-ACCESS-P6-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F22)
)
:effect
(and
(NO-ACCESS-P6-F22)
(not (NOT-NO-ACCESS-P6-F22))
)
)
(:action BLOCK-ACCESS-P6-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F21)
)
:effect
(and
(NO-ACCESS-P6-F21)
(not (NOT-NO-ACCESS-P6-F21))
)
)
(:action BLOCK-ACCESS-P6-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F20)
)
:effect
(and
(NO-ACCESS-P6-F20)
(not (NOT-NO-ACCESS-P6-F20))
)
)
(:action BLOCK-ACCESS-P6-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F19)
)
:effect
(and
(NO-ACCESS-P6-F19)
(not (NOT-NO-ACCESS-P6-F19))
)
)
(:action BLOCK-ACCESS-P6-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F18)
)
:effect
(and
(NO-ACCESS-P6-F18)
(not (NOT-NO-ACCESS-P6-F18))
)
)
(:action BLOCK-ACCESS-P6-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F17)
)
:effect
(and
(NO-ACCESS-P6-F17)
(not (NOT-NO-ACCESS-P6-F17))
)
)
(:action BLOCK-ACCESS-P6-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F16)
)
:effect
(and
(NO-ACCESS-P6-F16)
(not (NOT-NO-ACCESS-P6-F16))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F6)
)
:effect
(and
(NO-ACCESS-P6-F6)
(not (NOT-NO-ACCESS-P6-F6))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P6-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F0)
)
:effect
(and
(NO-ACCESS-P6-F0)
(not (NOT-NO-ACCESS-P6-F0))
)
)
(:action BLOCK-ACCESS-P5-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F23)
)
:effect
(and
(NO-ACCESS-P5-F23)
(not (NOT-NO-ACCESS-P5-F23))
)
)
(:action BLOCK-ACCESS-P5-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F22)
)
:effect
(and
(NO-ACCESS-P5-F22)
(not (NOT-NO-ACCESS-P5-F22))
)
)
(:action BLOCK-ACCESS-P5-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F21)
)
:effect
(and
(NO-ACCESS-P5-F21)
(not (NOT-NO-ACCESS-P5-F21))
)
)
(:action BLOCK-ACCESS-P5-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F20)
)
:effect
(and
(NO-ACCESS-P5-F20)
(not (NOT-NO-ACCESS-P5-F20))
)
)
(:action BLOCK-ACCESS-P5-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F19)
)
:effect
(and
(NO-ACCESS-P5-F19)
(not (NOT-NO-ACCESS-P5-F19))
)
)
(:action BLOCK-ACCESS-P5-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F18)
)
:effect
(and
(NO-ACCESS-P5-F18)
(not (NOT-NO-ACCESS-P5-F18))
)
)
(:action BLOCK-ACCESS-P5-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F17)
)
:effect
(and
(NO-ACCESS-P5-F17)
(not (NOT-NO-ACCESS-P5-F17))
)
)
(:action BLOCK-ACCESS-P5-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F16)
)
:effect
(and
(NO-ACCESS-P5-F16)
(not (NOT-NO-ACCESS-P5-F16))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F23)
)
:effect
(and
(NO-ACCESS-P4-F23)
(not (NOT-NO-ACCESS-P4-F23))
)
)
(:action BLOCK-ACCESS-P4-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F22)
)
:effect
(and
(NO-ACCESS-P4-F22)
(not (NOT-NO-ACCESS-P4-F22))
)
)
(:action BLOCK-ACCESS-P4-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F21)
)
:effect
(and
(NO-ACCESS-P4-F21)
(not (NOT-NO-ACCESS-P4-F21))
)
)
(:action BLOCK-ACCESS-P4-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F20)
)
:effect
(and
(NO-ACCESS-P4-F20)
(not (NOT-NO-ACCESS-P4-F20))
)
)
(:action BLOCK-ACCESS-P4-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F18)
)
:effect
(and
(NO-ACCESS-P4-F18)
(not (NOT-NO-ACCESS-P4-F18))
)
)
(:action BLOCK-ACCESS-P4-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F16)
)
:effect
(and
(NO-ACCESS-P4-F16)
(not (NOT-NO-ACCESS-P4-F16))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F23)
)
:effect
(and
(NO-ACCESS-P3-F23)
(not (NOT-NO-ACCESS-P3-F23))
)
)
(:action BLOCK-ACCESS-P3-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F22)
)
:effect
(and
(NO-ACCESS-P3-F22)
(not (NOT-NO-ACCESS-P3-F22))
)
)
(:action BLOCK-ACCESS-P3-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F21)
)
:effect
(and
(NO-ACCESS-P3-F21)
(not (NOT-NO-ACCESS-P3-F21))
)
)
(:action BLOCK-ACCESS-P3-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F20)
)
:effect
(and
(NO-ACCESS-P3-F20)
(not (NOT-NO-ACCESS-P3-F20))
)
)
(:action BLOCK-ACCESS-P3-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F19)
)
:effect
(and
(NO-ACCESS-P3-F19)
(not (NOT-NO-ACCESS-P3-F19))
)
)
(:action BLOCK-ACCESS-P3-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F18)
)
:effect
(and
(NO-ACCESS-P3-F18)
(not (NOT-NO-ACCESS-P3-F18))
)
)
(:action BLOCK-ACCESS-P3-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F16)
)
:effect
(and
(NO-ACCESS-P3-F16)
(not (NOT-NO-ACCESS-P3-F16))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F10)
)
:effect
(and
(NO-ACCESS-P3-F10)
(not (NOT-NO-ACCESS-P3-F10))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F23)
)
:effect
(and
(NO-ACCESS-P2-F23)
(not (NOT-NO-ACCESS-P2-F23))
)
)
(:action BLOCK-ACCESS-P2-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F21)
)
:effect
(and
(NO-ACCESS-P2-F21)
(not (NOT-NO-ACCESS-P2-F21))
)
)
(:action BLOCK-ACCESS-P2-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F20)
)
:effect
(and
(NO-ACCESS-P2-F20)
(not (NOT-NO-ACCESS-P2-F20))
)
)
(:action BLOCK-ACCESS-P2-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F19)
)
:effect
(and
(NO-ACCESS-P2-F19)
(not (NOT-NO-ACCESS-P2-F19))
)
)
(:action BLOCK-ACCESS-P2-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F18)
)
:effect
(and
(NO-ACCESS-P2-F18)
(not (NOT-NO-ACCESS-P2-F18))
)
)
(:action BLOCK-ACCESS-P2-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F17)
)
:effect
(and
(NO-ACCESS-P2-F17)
(not (NOT-NO-ACCESS-P2-F17))
)
)
(:action BLOCK-ACCESS-P2-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F16)
)
:effect
(and
(NO-ACCESS-P2-F16)
(not (NOT-NO-ACCESS-P2-F16))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F23)
)
:effect
(and
(NO-ACCESS-P1-F23)
(not (NOT-NO-ACCESS-P1-F23))
)
)
(:action BLOCK-ACCESS-P1-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F22)
)
:effect
(and
(NO-ACCESS-P1-F22)
(not (NOT-NO-ACCESS-P1-F22))
)
)
(:action BLOCK-ACCESS-P1-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F21)
)
:effect
(and
(NO-ACCESS-P1-F21)
(not (NOT-NO-ACCESS-P1-F21))
)
)
(:action BLOCK-ACCESS-P1-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F20)
)
:effect
(and
(NO-ACCESS-P1-F20)
(not (NOT-NO-ACCESS-P1-F20))
)
)
(:action BLOCK-ACCESS-P1-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F19)
)
:effect
(and
(NO-ACCESS-P1-F19)
(not (NOT-NO-ACCESS-P1-F19))
)
)
(:action BLOCK-ACCESS-P1-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F18)
)
:effect
(and
(NO-ACCESS-P1-F18)
(not (NOT-NO-ACCESS-P1-F18))
)
)
(:action BLOCK-ACCESS-P1-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F17)
)
:effect
(and
(NO-ACCESS-P1-F17)
(not (NOT-NO-ACCESS-P1-F17))
)
)
(:action BLOCK-ACCESS-P1-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F16)
)
:effect
(and
(NO-ACCESS-P1-F16)
(not (NOT-NO-ACCESS-P1-F16))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P0-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F23)
)
:effect
(and
(NO-ACCESS-P0-F23)
(not (NOT-NO-ACCESS-P0-F23))
)
)
(:action BLOCK-ACCESS-P0-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F22)
)
:effect
(and
(NO-ACCESS-P0-F22)
(not (NOT-NO-ACCESS-P0-F22))
)
)
(:action BLOCK-ACCESS-P0-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F21)
)
:effect
(and
(NO-ACCESS-P0-F21)
(not (NOT-NO-ACCESS-P0-F21))
)
)
(:action BLOCK-ACCESS-P0-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F20)
)
:effect
(and
(NO-ACCESS-P0-F20)
(not (NOT-NO-ACCESS-P0-F20))
)
)
(:action BLOCK-ACCESS-P0-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F19)
)
:effect
(and
(NO-ACCESS-P0-F19)
(not (NOT-NO-ACCESS-P0-F19))
)
)
(:action BLOCK-ACCESS-P0-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F18)
)
:effect
(and
(NO-ACCESS-P0-F18)
(not (NOT-NO-ACCESS-P0-F18))
)
)
(:action BLOCK-ACCESS-P0-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F17)
)
:effect
(and
(NO-ACCESS-P0-F17)
(not (NOT-NO-ACCESS-P0-F17))
)
)
(:action BLOCK-ACCESS-P0-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F16)
)
:effect
(and
(NO-ACCESS-P0-F16)
(not (NOT-NO-ACCESS-P0-F16))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F16-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F17-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F18-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F19-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F20-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F21-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F22-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F23-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P7)
(NOT-BOARDED-P10)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F23))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F16
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F17
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F18
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F19
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F20
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F21
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F22
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F23
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P0)))

(:derived (Flag3-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag6-)(and (NOT-BOARDED-P9)))

(:derived (Flag7-)(and (NOT-BOARDED-P11)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P11-F23)))

(:derived (Flag8-)(and (NOT-BOARDED-P10)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P10-F23)))

(:derived (Flag9-)(and (NOT-BOARDED-P9)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P9-F23)))

(:derived (Flag10-)(and (NOT-BOARDED-P8)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P8-F23)))

(:derived (Flag11-)(and (NOT-BOARDED-P7)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P7-F23)))

(:derived (Flag12-)(and (NOT-BOARDED-P6)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P6-F23)))

(:derived (Flag13-)(and (NOT-BOARDED-P5)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P5-F23)))

(:derived (Flag14-)(and (NOT-BOARDED-P4)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P4-F23)))

(:derived (Flag15-)(and (NOT-BOARDED-P3)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P3-F23)))

(:derived (Flag16-)(and (NOT-BOARDED-P2)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P2-F23)))

(:derived (Flag17-)(and (NOT-BOARDED-P1)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P1-F23)))

(:derived (Flag18-)(and (NOT-BOARDED-P0)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P0-F23)))

(:derived (Flag20-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag23-)(and (NOT-BOARDED-P11)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P11-F22)))

(:derived (Flag24-)(and (NOT-BOARDED-P10)))

(:derived (Flag24-)(and (NOT-NO-ACCESS-P10-F22)))

(:derived (Flag25-)(and (NOT-BOARDED-P9)))

(:derived (Flag25-)(and (NOT-NO-ACCESS-P9-F22)))

(:derived (Flag26-)(and (NOT-BOARDED-P8)))

(:derived (Flag26-)(and (NOT-NO-ACCESS-P8-F22)))

(:derived (Flag27-)(and (NOT-BOARDED-P7)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P7-F22)))

(:derived (Flag28-)(and (NOT-BOARDED-P6)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P6-F22)))

(:derived (Flag29-)(and (NOT-BOARDED-P5)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P5-F22)))

(:derived (Flag30-)(and (NOT-BOARDED-P4)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P4-F22)))

(:derived (Flag31-)(and (NOT-BOARDED-P3)))

(:derived (Flag31-)(and (NOT-NO-ACCESS-P3-F22)))

(:derived (Flag32-)(and (NOT-BOARDED-P1)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P1-F22)))

(:derived (Flag33-)(and (NOT-BOARDED-P0)))

(:derived (Flag33-)(and (NOT-NO-ACCESS-P0-F22)))

(:derived (Flag39-)(and (NOT-BOARDED-P9)))

(:derived (Flag39-)(and (NOT-SERVED-P5)))

(:derived (Flag40-)(and (NOT-BOARDED-P11)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P11-F21)))

(:derived (Flag41-)(and (NOT-BOARDED-P10)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P10-F21)))

(:derived (Flag42-)(and (NOT-BOARDED-P9)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P9-F21)))

(:derived (Flag43-)(and (NOT-BOARDED-P8)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P8-F21)))

(:derived (Flag44-)(and (NOT-BOARDED-P7)))

(:derived (Flag44-)(and (NOT-NO-ACCESS-P7-F21)))

(:derived (Flag45-)(and (NOT-BOARDED-P6)))

(:derived (Flag45-)(and (NOT-NO-ACCESS-P6-F21)))

(:derived (Flag46-)(and (NOT-BOARDED-P5)))

(:derived (Flag46-)(and (NOT-NO-ACCESS-P5-F21)))

(:derived (Flag47-)(and (NOT-BOARDED-P4)))

(:derived (Flag47-)(and (NOT-NO-ACCESS-P4-F21)))

(:derived (Flag48-)(and (NOT-BOARDED-P3)))

(:derived (Flag48-)(and (NOT-NO-ACCESS-P3-F21)))

(:derived (Flag49-)(and (NOT-BOARDED-P2)))

(:derived (Flag49-)(and (NOT-NO-ACCESS-P2-F21)))

(:derived (Flag50-)(and (NOT-BOARDED-P1)))

(:derived (Flag50-)(and (NOT-NO-ACCESS-P1-F21)))

(:derived (Flag51-)(and (NOT-BOARDED-P0)))

(:derived (Flag51-)(and (NOT-NO-ACCESS-P0-F21)))

(:derived (Flag54-)(and (NOT-BOARDED-P0)))

(:derived (Flag56-)(and (NOT-BOARDED-P0)))

(:derived (Flag57-)(and (NOT-BOARDED-P9)))

(:derived (Flag57-)(and (NOT-SERVED-P11)))

(:derived (Flag58-)(and (NOT-BOARDED-P11)))

(:derived (Flag58-)(and (NOT-NO-ACCESS-P11-F20)))

(:derived (Flag59-)(and (NOT-BOARDED-P10)))

(:derived (Flag59-)(and (NOT-NO-ACCESS-P10-F20)))

(:derived (Flag60-)(and (NOT-BOARDED-P9)))

(:derived (Flag60-)(and (NOT-NO-ACCESS-P9-F20)))

(:derived (Flag61-)(and (NOT-BOARDED-P8)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P8-F20)))

(:derived (Flag62-)(and (NOT-BOARDED-P7)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P7-F20)))

(:derived (Flag63-)(and (NOT-BOARDED-P6)))

(:derived (Flag63-)(and (NOT-NO-ACCESS-P6-F20)))

(:derived (Flag64-)(and (NOT-BOARDED-P5)))

(:derived (Flag64-)(and (NOT-NO-ACCESS-P5-F20)))

(:derived (Flag65-)(and (NOT-BOARDED-P4)))

(:derived (Flag65-)(and (NOT-NO-ACCESS-P4-F20)))

(:derived (Flag66-)(and (NOT-BOARDED-P3)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P3-F20)))

(:derived (Flag67-)(and (NOT-BOARDED-P2)))

(:derived (Flag67-)(and (NOT-NO-ACCESS-P2-F20)))

(:derived (Flag68-)(and (NOT-BOARDED-P1)))

(:derived (Flag68-)(and (NOT-NO-ACCESS-P1-F20)))

(:derived (Flag69-)(and (NOT-BOARDED-P0)))

(:derived (Flag69-)(and (NOT-NO-ACCESS-P0-F20)))

(:derived (Flag72-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P2)))

(:derived (Flag76-)(and (NOT-BOARDED-P9)))

(:derived (Flag76-)(and (NOT-SERVED-P10)))

(:derived (Flag77-)(and (NOT-BOARDED-P11)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P11-F19)))

(:derived (Flag78-)(and (NOT-BOARDED-P10)))

(:derived (Flag78-)(and (NOT-NO-ACCESS-P10-F19)))

(:derived (Flag79-)(and (NOT-BOARDED-P9)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P9-F19)))

(:derived (Flag80-)(and (NOT-BOARDED-P8)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P8-F19)))

(:derived (Flag81-)(and (NOT-BOARDED-P7)))

(:derived (Flag81-)(and (NOT-NO-ACCESS-P7-F19)))

(:derived (Flag82-)(and (NOT-BOARDED-P6)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P6-F19)))

(:derived (Flag83-)(and (NOT-BOARDED-P5)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P5-F19)))

(:derived (Flag84-)(and (NOT-BOARDED-P3)))

(:derived (Flag84-)(and (NOT-NO-ACCESS-P3-F19)))

(:derived (Flag85-)(and (NOT-BOARDED-P2)))

(:derived (Flag85-)(and (NOT-NO-ACCESS-P2-F19)))

(:derived (Flag86-)(and (NOT-BOARDED-P1)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P1-F19)))

(:derived (Flag87-)(and (NOT-BOARDED-P0)))

(:derived (Flag87-)(and (NOT-NO-ACCESS-P0-F19)))

(:derived (Flag90-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag94-)(and (NOT-BOARDED-P9)))

(:derived (Flag94-)(and (NOT-SERVED-P1)))

(:derived (Flag94-)(and (NOT-SERVED-P0)))

(:derived (Flag95-)(and (NOT-BOARDED-P11)))

(:derived (Flag95-)(and (NOT-NO-ACCESS-P11-F18)))

(:derived (Flag96-)(and (NOT-BOARDED-P10)))

(:derived (Flag96-)(and (NOT-NO-ACCESS-P10-F18)))

(:derived (Flag97-)(and (NOT-BOARDED-P9)))

(:derived (Flag97-)(and (NOT-NO-ACCESS-P9-F18)))

(:derived (Flag98-)(and (NOT-BOARDED-P8)))

(:derived (Flag98-)(and (NOT-NO-ACCESS-P8-F18)))

(:derived (Flag99-)(and (NOT-BOARDED-P7)))

(:derived (Flag99-)(and (NOT-NO-ACCESS-P7-F18)))

(:derived (Flag100-)(and (NOT-BOARDED-P6)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P6-F18)))

(:derived (Flag101-)(and (NOT-BOARDED-P5)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P5-F18)))

(:derived (Flag102-)(and (NOT-BOARDED-P4)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P4-F18)))

(:derived (Flag103-)(and (NOT-BOARDED-P3)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P3-F18)))

(:derived (Flag104-)(and (NOT-BOARDED-P2)))

(:derived (Flag104-)(and (NOT-NO-ACCESS-P2-F18)))

(:derived (Flag105-)(and (NOT-BOARDED-P1)))

(:derived (Flag105-)(and (NOT-NO-ACCESS-P1-F18)))

(:derived (Flag106-)(and (NOT-BOARDED-P0)))

(:derived (Flag106-)(and (NOT-NO-ACCESS-P0-F18)))

(:derived (Flag108-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag111-)(and (NOT-BOARDED-P9)))

(:derived (Flag112-)(and (NOT-BOARDED-P11)))

(:derived (Flag112-)(and (NOT-NO-ACCESS-P11-F17)))

(:derived (Flag113-)(and (NOT-BOARDED-P10)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P10-F17)))

(:derived (Flag114-)(and (NOT-BOARDED-P9)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P9-F17)))

(:derived (Flag115-)(and (NOT-BOARDED-P8)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P8-F17)))

(:derived (Flag116-)(and (NOT-BOARDED-P7)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P7-F17)))

(:derived (Flag117-)(and (NOT-BOARDED-P6)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P6-F17)))

(:derived (Flag118-)(and (NOT-BOARDED-P5)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P5-F17)))

(:derived (Flag119-)(and (NOT-BOARDED-P2)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P2-F17)))

(:derived (Flag120-)(and (NOT-BOARDED-P1)))

(:derived (Flag120-)(and (NOT-NO-ACCESS-P1-F17)))

(:derived (Flag121-)(and (NOT-BOARDED-P0)))

(:derived (Flag121-)(and (NOT-NO-ACCESS-P0-F17)))

(:derived (Flag127-)(and (NOT-BOARDED-P11)))

(:derived (Flag127-)(and (NOT-NO-ACCESS-P11-F16)))

(:derived (Flag128-)(and (NOT-BOARDED-P10)))

(:derived (Flag128-)(and (NOT-NO-ACCESS-P10-F16)))

(:derived (Flag129-)(and (NOT-BOARDED-P9)))

(:derived (Flag129-)(and (NOT-NO-ACCESS-P9-F16)))

(:derived (Flag130-)(and (NOT-BOARDED-P8)))

(:derived (Flag130-)(and (NOT-NO-ACCESS-P8-F16)))

(:derived (Flag131-)(and (NOT-BOARDED-P7)))

(:derived (Flag131-)(and (NOT-NO-ACCESS-P7-F16)))

(:derived (Flag132-)(and (NOT-BOARDED-P6)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P6-F16)))

(:derived (Flag133-)(and (NOT-BOARDED-P5)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P5-F16)))

(:derived (Flag134-)(and (NOT-BOARDED-P4)))

(:derived (Flag134-)(and (NOT-NO-ACCESS-P4-F16)))

(:derived (Flag135-)(and (NOT-BOARDED-P3)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P3-F16)))

(:derived (Flag136-)(and (NOT-BOARDED-P2)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P2-F16)))

(:derived (Flag137-)(and (NOT-BOARDED-P1)))

(:derived (Flag137-)(and (NOT-NO-ACCESS-P1-F16)))

(:derived (Flag138-)(and (NOT-BOARDED-P0)))

(:derived (Flag138-)(and (NOT-NO-ACCESS-P0-F16)))

(:derived (Flag144-)(and (NOT-BOARDED-P9)))

(:derived (Flag144-)(and (NOT-SERVED-P2)))

(:derived (Flag145-)(and (NOT-BOARDED-P11)))

(:derived (Flag145-)(and (NOT-NO-ACCESS-P11-F15)))

(:derived (Flag146-)(and (NOT-BOARDED-P10)))

(:derived (Flag146-)(and (NOT-NO-ACCESS-P10-F15)))

(:derived (Flag147-)(and (NOT-BOARDED-P9)))

(:derived (Flag147-)(and (NOT-NO-ACCESS-P9-F15)))

(:derived (Flag148-)(and (NOT-BOARDED-P8)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P8-F15)))

(:derived (Flag149-)(and (NOT-BOARDED-P7)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag150-)(and (NOT-BOARDED-P6)))

(:derived (Flag150-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag151-)(and (NOT-BOARDED-P5)))

(:derived (Flag151-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag152-)(and (NOT-BOARDED-P4)))

(:derived (Flag152-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag153-)(and (NOT-BOARDED-P3)))

(:derived (Flag153-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag154-)(and (NOT-BOARDED-P2)))

(:derived (Flag154-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag155-)(and (NOT-BOARDED-P1)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag156-)(and (NOT-BOARDED-P0)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag162-)(and (NOT-BOARDED-P9)))

(:derived (Flag162-)(and (NOT-SERVED-P7)))

(:derived (Flag163-)(and (NOT-BOARDED-P11)))

(:derived (Flag163-)(and (NOT-NO-ACCESS-P11-F14)))

(:derived (Flag164-)(and (NOT-BOARDED-P10)))

(:derived (Flag164-)(and (NOT-NO-ACCESS-P10-F14)))

(:derived (Flag165-)(and (NOT-BOARDED-P9)))

(:derived (Flag165-)(and (NOT-NO-ACCESS-P9-F14)))

(:derived (Flag166-)(and (NOT-BOARDED-P8)))

(:derived (Flag166-)(and (NOT-NO-ACCESS-P8-F14)))

(:derived (Flag167-)(and (NOT-BOARDED-P7)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag168-)(and (NOT-BOARDED-P6)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag169-)(and (NOT-BOARDED-P5)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag170-)(and (NOT-BOARDED-P4)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag171-)(and (NOT-BOARDED-P3)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag172-)(and (NOT-BOARDED-P2)))

(:derived (Flag172-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag173-)(and (NOT-BOARDED-P1)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag174-)(and (NOT-BOARDED-P0)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag176-)(and (NOT-BOARDED-P11)))

(:derived (Flag176-)(and (NOT-NO-ACCESS-P11-F13)))

(:derived (Flag177-)(and (NOT-BOARDED-P10)))

(:derived (Flag177-)(and (NOT-NO-ACCESS-P10-F13)))

(:derived (Flag178-)(and (NOT-BOARDED-P9)))

(:derived (Flag178-)(and (NOT-NO-ACCESS-P9-F13)))

(:derived (Flag179-)(and (NOT-BOARDED-P8)))

(:derived (Flag179-)(and (NOT-NO-ACCESS-P8-F13)))

(:derived (Flag180-)(and (NOT-BOARDED-P7)))

(:derived (Flag180-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag181-)(and (NOT-BOARDED-P6)))

(:derived (Flag181-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag182-)(and (NOT-BOARDED-P5)))

(:derived (Flag182-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag183-)(and (NOT-BOARDED-P4)))

(:derived (Flag183-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag184-)(and (NOT-BOARDED-P3)))

(:derived (Flag184-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag185-)(and (NOT-BOARDED-P2)))

(:derived (Flag185-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag186-)(and (NOT-BOARDED-P1)))

(:derived (Flag186-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag187-)(and (NOT-BOARDED-P0)))

(:derived (Flag187-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag189-)(and (NOT-BOARDED-P11)))

(:derived (Flag189-)(and (NOT-NO-ACCESS-P11-F12)))

(:derived (Flag190-)(and (NOT-BOARDED-P10)))

(:derived (Flag190-)(and (NOT-NO-ACCESS-P10-F12)))

(:derived (Flag191-)(and (NOT-BOARDED-P9)))

(:derived (Flag191-)(and (NOT-NO-ACCESS-P9-F12)))

(:derived (Flag192-)(and (NOT-BOARDED-P8)))

(:derived (Flag192-)(and (NOT-NO-ACCESS-P8-F12)))

(:derived (Flag193-)(and (NOT-BOARDED-P7)))

(:derived (Flag193-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag194-)(and (NOT-BOARDED-P6)))

(:derived (Flag194-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag195-)(and (NOT-BOARDED-P5)))

(:derived (Flag195-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag196-)(and (NOT-BOARDED-P4)))

(:derived (Flag196-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag197-)(and (NOT-BOARDED-P3)))

(:derived (Flag197-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag198-)(and (NOT-BOARDED-P2)))

(:derived (Flag198-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag199-)(and (NOT-BOARDED-P1)))

(:derived (Flag199-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag200-)(and (NOT-BOARDED-P0)))

(:derived (Flag200-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag206-)(and (NOT-BOARDED-P11)))

(:derived (Flag206-)(and (NOT-NO-ACCESS-P11-F11)))

(:derived (Flag207-)(and (NOT-BOARDED-P10)))

(:derived (Flag207-)(and (NOT-NO-ACCESS-P10-F11)))

(:derived (Flag208-)(and (NOT-BOARDED-P9)))

(:derived (Flag208-)(and (NOT-NO-ACCESS-P9-F11)))

(:derived (Flag209-)(and (NOT-BOARDED-P8)))

(:derived (Flag209-)(and (NOT-NO-ACCESS-P8-F11)))

(:derived (Flag210-)(and (NOT-BOARDED-P7)))

(:derived (Flag210-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag211-)(and (NOT-BOARDED-P6)))

(:derived (Flag211-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag212-)(and (NOT-BOARDED-P5)))

(:derived (Flag212-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag213-)(and (NOT-BOARDED-P4)))

(:derived (Flag213-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag214-)(and (NOT-BOARDED-P3)))

(:derived (Flag214-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag215-)(and (NOT-BOARDED-P2)))

(:derived (Flag215-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag216-)(and (NOT-BOARDED-P1)))

(:derived (Flag216-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag217-)(and (NOT-BOARDED-P0)))

(:derived (Flag217-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag223-)(and (NOT-BOARDED-P11)))

(:derived (Flag223-)(and (NOT-NO-ACCESS-P11-F10)))

(:derived (Flag224-)(and (NOT-BOARDED-P10)))

(:derived (Flag224-)(and (NOT-NO-ACCESS-P10-F10)))

(:derived (Flag225-)(and (NOT-BOARDED-P9)))

(:derived (Flag225-)(and (NOT-NO-ACCESS-P9-F10)))

(:derived (Flag226-)(and (NOT-BOARDED-P8)))

(:derived (Flag226-)(and (NOT-NO-ACCESS-P8-F10)))

(:derived (Flag227-)(and (NOT-BOARDED-P7)))

(:derived (Flag227-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag228-)(and (NOT-BOARDED-P6)))

(:derived (Flag228-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag229-)(and (NOT-BOARDED-P5)))

(:derived (Flag229-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag230-)(and (NOT-BOARDED-P4)))

(:derived (Flag230-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag231-)(and (NOT-BOARDED-P3)))

(:derived (Flag231-)(and (NOT-NO-ACCESS-P3-F10)))

(:derived (Flag232-)(and (NOT-BOARDED-P2)))

(:derived (Flag232-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag233-)(and (NOT-BOARDED-P1)))

(:derived (Flag233-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag234-)(and (NOT-BOARDED-P0)))

(:derived (Flag234-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag236-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)))

(:derived (Flag239-)(and (NOT-BOARDED-P9)))

(:derived (Flag240-)(and (NOT-BOARDED-P11)))

(:derived (Flag240-)(and (NOT-NO-ACCESS-P11-F9)))

(:derived (Flag241-)(and (NOT-BOARDED-P10)))

(:derived (Flag241-)(and (NOT-NO-ACCESS-P10-F9)))

(:derived (Flag242-)(and (NOT-BOARDED-P9)))

(:derived (Flag242-)(and (NOT-NO-ACCESS-P9-F9)))

(:derived (Flag243-)(and (NOT-BOARDED-P8)))

(:derived (Flag243-)(and (NOT-NO-ACCESS-P8-F9)))

(:derived (Flag244-)(and (NOT-BOARDED-P7)))

(:derived (Flag244-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag245-)(and (NOT-BOARDED-P6)))

(:derived (Flag245-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag246-)(and (NOT-BOARDED-P5)))

(:derived (Flag246-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag247-)(and (NOT-BOARDED-P4)))

(:derived (Flag247-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag248-)(and (NOT-BOARDED-P3)))

(:derived (Flag248-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag249-)(and (NOT-BOARDED-P2)))

(:derived (Flag249-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag250-)(and (NOT-BOARDED-P1)))

(:derived (Flag250-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag251-)(and (NOT-BOARDED-P0)))

(:derived (Flag251-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag257-)(and (NOT-BOARDED-P11)))

(:derived (Flag257-)(and (NOT-NO-ACCESS-P11-F8)))

(:derived (Flag258-)(and (NOT-BOARDED-P10)))

(:derived (Flag258-)(and (NOT-NO-ACCESS-P10-F8)))

(:derived (Flag259-)(and (NOT-BOARDED-P8)))

(:derived (Flag259-)(and (NOT-NO-ACCESS-P8-F8)))

(:derived (Flag260-)(and (NOT-BOARDED-P7)))

(:derived (Flag260-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag261-)(and (NOT-BOARDED-P6)))

(:derived (Flag261-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag262-)(and (NOT-BOARDED-P5)))

(:derived (Flag262-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag263-)(and (NOT-BOARDED-P4)))

(:derived (Flag263-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag264-)(and (NOT-BOARDED-P3)))

(:derived (Flag264-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag265-)(and (NOT-BOARDED-P2)))

(:derived (Flag265-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag266-)(and (NOT-BOARDED-P1)))

(:derived (Flag266-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag267-)(and (NOT-BOARDED-P0)))

(:derived (Flag267-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag269-)(and (NOT-BOARDED-P11)))

(:derived (Flag269-)(and (NOT-NO-ACCESS-P11-F7)))

(:derived (Flag270-)(and (NOT-BOARDED-P10)))

(:derived (Flag270-)(and (NOT-NO-ACCESS-P10-F7)))

(:derived (Flag271-)(and (NOT-BOARDED-P9)))

(:derived (Flag271-)(and (NOT-NO-ACCESS-P9-F7)))

(:derived (Flag272-)(and (NOT-BOARDED-P7)))

(:derived (Flag272-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag273-)(and (NOT-BOARDED-P6)))

(:derived (Flag273-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag274-)(and (NOT-BOARDED-P5)))

(:derived (Flag274-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag275-)(and (NOT-BOARDED-P4)))

(:derived (Flag275-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag276-)(and (NOT-BOARDED-P3)))

(:derived (Flag276-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag277-)(and (NOT-BOARDED-P2)))

(:derived (Flag277-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag278-)(and (NOT-BOARDED-P1)))

(:derived (Flag278-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag279-)(and (NOT-BOARDED-P0)))

(:derived (Flag279-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag281-)(and (NOT-BOARDED-P10)))

(:derived (Flag282-)(and (NOT-BOARDED-P10)))

(:derived (Flag283-)(and (NOT-BOARDED-P9)))

(:derived (Flag284-)(and (NOT-BOARDED-P11)))

(:derived (Flag284-)(and (NOT-NO-ACCESS-P11-F6)))

(:derived (Flag285-)(and (NOT-BOARDED-P10)))

(:derived (Flag285-)(and (NOT-NO-ACCESS-P10-F6)))

(:derived (Flag286-)(and (NOT-BOARDED-P9)))

(:derived (Flag286-)(and (NOT-NO-ACCESS-P9-F6)))

(:derived (Flag287-)(and (NOT-BOARDED-P8)))

(:derived (Flag287-)(and (NOT-NO-ACCESS-P8-F6)))

(:derived (Flag288-)(and (NOT-BOARDED-P7)))

(:derived (Flag288-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag289-)(and (NOT-BOARDED-P6)))

(:derived (Flag289-)(and (NOT-NO-ACCESS-P6-F6)))

(:derived (Flag290-)(and (NOT-BOARDED-P5)))

(:derived (Flag290-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag291-)(and (NOT-BOARDED-P4)))

(:derived (Flag291-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag292-)(and (NOT-BOARDED-P3)))

(:derived (Flag292-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag293-)(and (NOT-BOARDED-P2)))

(:derived (Flag293-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag294-)(and (NOT-BOARDED-P1)))

(:derived (Flag294-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag295-)(and (NOT-BOARDED-P0)))

(:derived (Flag295-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag303-)(and (NOT-BOARDED-P11)))

(:derived (Flag303-)(and (NOT-NO-ACCESS-P11-F5)))

(:derived (Flag304-)(and (NOT-BOARDED-P10)))

(:derived (Flag304-)(and (NOT-NO-ACCESS-P10-F5)))

(:derived (Flag305-)(and (NOT-BOARDED-P9)))

(:derived (Flag305-)(and (NOT-NO-ACCESS-P9-F5)))

(:derived (Flag306-)(and (NOT-BOARDED-P8)))

(:derived (Flag306-)(and (NOT-NO-ACCESS-P8-F5)))

(:derived (Flag307-)(and (NOT-BOARDED-P7)))

(:derived (Flag307-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag308-)(and (NOT-BOARDED-P6)))

(:derived (Flag308-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag309-)(and (NOT-BOARDED-P5)))

(:derived (Flag309-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag310-)(and (NOT-BOARDED-P4)))

(:derived (Flag310-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag311-)(and (NOT-BOARDED-P3)))

(:derived (Flag311-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag312-)(and (NOT-BOARDED-P2)))

(:derived (Flag312-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag313-)(and (NOT-BOARDED-P1)))

(:derived (Flag313-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag314-)(and (NOT-BOARDED-P0)))

(:derived (Flag314-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag316-)(and (NOT-BOARDED-P11)))

(:derived (Flag316-)(and (NOT-NO-ACCESS-P11-F4)))

(:derived (Flag317-)(and (NOT-BOARDED-P10)))

(:derived (Flag317-)(and (NOT-NO-ACCESS-P10-F4)))

(:derived (Flag318-)(and (NOT-BOARDED-P9)))

(:derived (Flag318-)(and (NOT-NO-ACCESS-P9-F4)))

(:derived (Flag319-)(and (NOT-BOARDED-P8)))

(:derived (Flag319-)(and (NOT-NO-ACCESS-P8-F4)))

(:derived (Flag320-)(and (NOT-BOARDED-P7)))

(:derived (Flag320-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag321-)(and (NOT-BOARDED-P6)))

(:derived (Flag321-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag322-)(and (NOT-BOARDED-P5)))

(:derived (Flag322-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag323-)(and (NOT-BOARDED-P4)))

(:derived (Flag323-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag324-)(and (NOT-BOARDED-P3)))

(:derived (Flag324-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag325-)(and (NOT-BOARDED-P2)))

(:derived (Flag325-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag326-)(and (NOT-BOARDED-P1)))

(:derived (Flag326-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag327-)(and (NOT-BOARDED-P0)))

(:derived (Flag327-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag329-)(and (NOT-BOARDED-P11)))

(:derived (Flag329-)(and (NOT-NO-ACCESS-P11-F3)))

(:derived (Flag330-)(and (NOT-BOARDED-P10)))

(:derived (Flag330-)(and (NOT-NO-ACCESS-P10-F3)))

(:derived (Flag331-)(and (NOT-BOARDED-P9)))

(:derived (Flag331-)(and (NOT-NO-ACCESS-P9-F3)))

(:derived (Flag332-)(and (NOT-BOARDED-P8)))

(:derived (Flag332-)(and (NOT-NO-ACCESS-P8-F3)))

(:derived (Flag333-)(and (NOT-BOARDED-P7)))

(:derived (Flag333-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag334-)(and (NOT-BOARDED-P6)))

(:derived (Flag334-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag335-)(and (NOT-BOARDED-P5)))

(:derived (Flag335-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag336-)(and (NOT-BOARDED-P4)))

(:derived (Flag336-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag337-)(and (NOT-BOARDED-P3)))

(:derived (Flag337-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag338-)(and (NOT-BOARDED-P2)))

(:derived (Flag338-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag339-)(and (NOT-BOARDED-P1)))

(:derived (Flag339-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag340-)(and (NOT-BOARDED-P0)))

(:derived (Flag340-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag342-)(and (NOT-BOARDED-P11)))

(:derived (Flag342-)(and (NOT-NO-ACCESS-P11-F2)))

(:derived (Flag343-)(and (NOT-BOARDED-P10)))

(:derived (Flag343-)(and (NOT-NO-ACCESS-P10-F2)))

(:derived (Flag344-)(and (NOT-BOARDED-P9)))

(:derived (Flag344-)(and (NOT-NO-ACCESS-P9-F2)))

(:derived (Flag345-)(and (NOT-BOARDED-P8)))

(:derived (Flag345-)(and (NOT-NO-ACCESS-P8-F2)))

(:derived (Flag346-)(and (NOT-BOARDED-P7)))

(:derived (Flag346-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag347-)(and (NOT-BOARDED-P6)))

(:derived (Flag347-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag348-)(and (NOT-BOARDED-P5)))

(:derived (Flag348-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag349-)(and (NOT-BOARDED-P4)))

(:derived (Flag349-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag350-)(and (NOT-BOARDED-P3)))

(:derived (Flag350-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag351-)(and (NOT-BOARDED-P2)))

(:derived (Flag351-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag352-)(and (NOT-BOARDED-P1)))

(:derived (Flag352-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag353-)(and (NOT-BOARDED-P0)))

(:derived (Flag353-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag355-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)))

(:derived (Flag358-)(and (NOT-BOARDED-P9)))

(:derived (Flag359-)(and (NOT-BOARDED-P11)))

(:derived (Flag359-)(and (NOT-NO-ACCESS-P11-F1)))

(:derived (Flag360-)(and (NOT-BOARDED-P10)))

(:derived (Flag360-)(and (NOT-NO-ACCESS-P10-F1)))

(:derived (Flag361-)(and (NOT-BOARDED-P9)))

(:derived (Flag361-)(and (NOT-NO-ACCESS-P9-F1)))

(:derived (Flag362-)(and (NOT-BOARDED-P8)))

(:derived (Flag362-)(and (NOT-NO-ACCESS-P8-F1)))

(:derived (Flag363-)(and (NOT-BOARDED-P7)))

(:derived (Flag363-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag364-)(and (NOT-BOARDED-P6)))

(:derived (Flag364-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag365-)(and (NOT-BOARDED-P5)))

(:derived (Flag365-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag366-)(and (NOT-BOARDED-P4)))

(:derived (Flag366-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag367-)(and (NOT-BOARDED-P3)))

(:derived (Flag367-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag368-)(and (NOT-BOARDED-P2)))

(:derived (Flag368-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag369-)(and (NOT-BOARDED-P1)))

(:derived (Flag369-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag370-)(and (NOT-BOARDED-P0)))

(:derived (Flag370-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag372-)(and (NOT-BOARDED-P11)))

(:derived (Flag372-)(and (NOT-NO-ACCESS-P11-F0)))

(:derived (Flag373-)(and (NOT-BOARDED-P10)))

(:derived (Flag373-)(and (NOT-NO-ACCESS-P10-F0)))

(:derived (Flag374-)(and (NOT-BOARDED-P9)))

(:derived (Flag374-)(and (NOT-NO-ACCESS-P9-F0)))

(:derived (Flag375-)(and (NOT-BOARDED-P8)))

(:derived (Flag375-)(and (NOT-NO-ACCESS-P8-F0)))

(:derived (Flag376-)(and (NOT-BOARDED-P7)))

(:derived (Flag376-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag377-)(and (NOT-BOARDED-P6)))

(:derived (Flag377-)(and (NOT-NO-ACCESS-P6-F0)))

(:derived (Flag378-)(and (NOT-BOARDED-P5)))

(:derived (Flag378-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag379-)(and (NOT-BOARDED-P4)))

(:derived (Flag379-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag380-)(and (NOT-BOARDED-P3)))

(:derived (Flag380-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag381-)(and (NOT-BOARDED-P2)))

(:derived (Flag381-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag382-)(and (NOT-BOARDED-P0)))

(:derived (Flag382-)(and (NOT-NO-ACCESS-P0-F0)))

)
