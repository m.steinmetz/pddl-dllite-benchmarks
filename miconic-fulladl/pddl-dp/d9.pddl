(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag202-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag188-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag143-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag110-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag26-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(LIFT-AT-F17)
(LIFT-AT-F16)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P0-F16)
(NO-ACCESS-P0-F17)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P1-F16)
(NO-ACCESS-P1-F17)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P2-F16)
(NO-ACCESS-P2-F17)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F10)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P3-F16)
(NO-ACCESS-P3-F17)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P4-F16)
(NO-ACCESS-P4-F17)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P5-F16)
(NO-ACCESS-P5-F17)
(NO-ACCESS-P6-F0)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F6)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P6-F16)
(NO-ACCESS-P6-F17)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(NO-ACCESS-P7-F16)
(NO-ACCESS-P7-F17)
(NO-ACCESS-P8-F0)
(NO-ACCESS-P8-F1)
(NO-ACCESS-P8-F2)
(NO-ACCESS-P8-F3)
(NO-ACCESS-P8-F4)
(NO-ACCESS-P8-F5)
(NO-ACCESS-P8-F6)
(NO-ACCESS-P8-F7)
(NO-ACCESS-P8-F8)
(NO-ACCESS-P8-F9)
(NO-ACCESS-P8-F10)
(NO-ACCESS-P8-F11)
(NO-ACCESS-P8-F12)
(NO-ACCESS-P8-F13)
(NO-ACCESS-P8-F14)
(NO-ACCESS-P8-F15)
(NO-ACCESS-P8-F16)
(NO-ACCESS-P8-F17)
(Flag214-)
(Flag200-)
(Flag186-)
(Flag176-)
(Flag163-)
(Flag154-)
(Flag141-)
(Flag131-)
(Flag122-)
(Flag108-)
(Flag95-)
(Flag85-)
(Flag75-)
(Flag52-)
(Flag50-)
(Flag48-)
(Flag38-)
(Flag24-)
(Flag14-)
(BOARDED-P4)
(BOARDED-P3)
(BOARDED-P6)
(SERVED-P5)
(BOARDED-P5)
(SERVED-P3)
(BOARDED-P0)
(BOARDED-P2)
(SERVED-P1)
(SERVED-P8)
(SERVED-P4)
(BOARDED-P1)
(BOARDED-P8)
(SERVED-P0)
(SERVED-P6)
(SERVED-P7)
(Flag203-)
(Flag201-)
(Flag189-)
(Flag187-)
(Flag144-)
(Flag142-)
(Flag111-)
(Flag109-)
(Flag62-)
(Flag51-)
(Flag49-)
(Flag27-)
(Flag25-)
(BOARDED-P7)
(SERVED-P2)
(Flag1-)
(NOT-BOARDED-P7)
(NOT-SERVED-P2)
(NOT-SERVED-P6)
(NOT-SERVED-P7)
(NOT-BOARDED-P1)
(NOT-BOARDED-P8)
(NOT-SERVED-P0)
(NOT-SERVED-P4)
(NOT-SERVED-P8)
(NOT-BOARDED-P0)
(NOT-BOARDED-P2)
(NOT-SERVED-P1)
(NOT-BOARDED-P5)
(NOT-SERVED-P3)
(NOT-SERVED-P5)
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(NOT-BOARDED-P4)
(NOT-NO-ACCESS-P8-F17)
(NOT-NO-ACCESS-P8-F16)
(NOT-NO-ACCESS-P8-F15)
(NOT-NO-ACCESS-P8-F14)
(NOT-NO-ACCESS-P8-F13)
(NOT-NO-ACCESS-P8-F12)
(NOT-NO-ACCESS-P8-F11)
(NOT-NO-ACCESS-P8-F10)
(NOT-NO-ACCESS-P8-F9)
(NOT-NO-ACCESS-P8-F8)
(NOT-NO-ACCESS-P8-F7)
(NOT-NO-ACCESS-P8-F6)
(NOT-NO-ACCESS-P8-F5)
(NOT-NO-ACCESS-P8-F4)
(NOT-NO-ACCESS-P8-F3)
(NOT-NO-ACCESS-P8-F2)
(NOT-NO-ACCESS-P8-F1)
(NOT-NO-ACCESS-P8-F0)
(NOT-NO-ACCESS-P7-F17)
(NOT-NO-ACCESS-P7-F16)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F17)
(NOT-NO-ACCESS-P6-F16)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F6)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P6-F0)
(NOT-NO-ACCESS-P5-F17)
(NOT-NO-ACCESS-P5-F16)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F17)
(NOT-NO-ACCESS-P4-F16)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F17)
(NOT-NO-ACCESS-P3-F16)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F10)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F17)
(NOT-NO-ACCESS-P2-F16)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F17)
(NOT-NO-ACCESS-P1-F16)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F17)
(NOT-NO-ACCESS-P0-F16)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)(SERVED-P8)))

(:derived (Flag26-)(and (Flag25-)))

(:derived (Flag28-)(and (Flag27-)))

(:derived (Flag50-)(and (Flag49-)))

(:derived (Flag52-)(and (Flag51-)))

(:derived (Flag110-)(and (Flag109-)))

(:derived (Flag112-)(and (Flag111-)))

(:derived (Flag143-)(and (Flag142-)))

(:derived (Flag145-)(and (Flag144-)))

(:derived (Flag188-)(and (Flag187-)))

(:derived (Flag190-)(and (Flag189-)))

(:derived (Flag202-)(and (Flag201-)))

(:derived (Flag204-)(and (Flag203-)))

(:action STOP-F13
:parameters ()
:precondition
(and
(Flag62-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:derived (Flag25-)(and (SERVED-P8)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P1)(NOT-BOARDED-P1)))

(:derived (Flag27-)(and (NOT-BOARDED-P8)(SERVED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(SERVED-P1)))

(:derived (Flag49-)(and (NOT-BOARDED-P7)(SERVED-P7)))

(:derived (Flag51-)(and (SERVED-P7)(NOT-BOARDED-P7)))

(:derived (Flag62-)(and (LIFT-AT-F13)(Flag50-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)))

(:derived (Flag109-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag111-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag142-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag144-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag187-)(and (NOT-BOARDED-P8)(SERVED-P6)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag189-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(SERVED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag201-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P4)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag203-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:action STOP-F17
:parameters ()
:precondition
(and
(Flag14-)
)
:effect
(and
)
)
(:action STOP-F16
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
)
)
(:action STOP-F15
:parameters ()
:precondition
(and
(Flag38-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(when
(and
(NOT-SERVED-P8)
)
(and
(BOARDED-P8)
(not (NOT-BOARDED-P8))
)
)
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action STOP-F14
:parameters ()
:precondition
(and
(Flag48-)
)
:effect
(and
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag75-)
)
:effect
(and
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag85-)
)
:effect
(and
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag95-)
)
:effect
(and
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag108-)
)
:effect
(and
(when
(and
(BOARDED-P8)
)
(and
(NOT-BOARDED-P8)
(SERVED-P8)
(not (BOARDED-P8))
(not (NOT-SERVED-P8))
)
)
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag122-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag131-)
)
:effect
(and
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag141-)
)
:effect
(and
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag154-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag163-)
)
:effect
(and
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag176-)
)
:effect
(and
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag186-)
)
:effect
(and
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag200-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag214-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F14))
)
)
(:action UP-F15-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F15))
)
)
(:action UP-F16-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F16))
)
)
(:derived (Flag3-)(and (Flag2-)))

(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag14-)(and (LIFT-AT-F17)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)))

(:derived (Flag24-)(and (LIFT-AT-F16)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)(Flag20-)(Flag21-)(Flag22-)(Flag23-)))

(:derived (Flag38-)(and (LIFT-AT-F15)(Flag26-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(Flag32-)(Flag33-)(Flag34-)(Flag35-)(Flag36-)(Flag37-)))

(:derived (Flag48-)(and (LIFT-AT-F14)(Flag3-)(Flag4-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)))

(:derived (Flag50-)(and (Flag2-)))

(:derived (Flag52-)(and (Flag2-)))

(:derived (Flag64-)(and (Flag63-)))

(:derived (Flag65-)(and (Flag63-)))

(:derived (Flag75-)(and (LIFT-AT-F12)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)))

(:derived (Flag85-)(and (LIFT-AT-F11)(Flag3-)(Flag4-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)))

(:derived (Flag95-)(and (LIFT-AT-F10)(Flag3-)(Flag4-)(Flag86-)(Flag87-)(Flag88-)(Flag89-)(Flag90-)(Flag91-)(Flag92-)(Flag93-)(Flag94-)))

(:derived (Flag97-)(and (Flag96-)))

(:derived (Flag98-)(and (Flag96-)))

(:derived (Flag108-)(and (LIFT-AT-F9)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)(Flag107-)))

(:derived (Flag122-)(and (LIFT-AT-F8)(Flag110-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)))

(:derived (Flag131-)(and (LIFT-AT-F7)(Flag3-)(Flag4-)(Flag123-)(Flag124-)(Flag125-)(Flag126-)(NOT-BOARDED-P4)(Flag127-)(Flag128-)(Flag129-)(Flag130-)))

(:derived (Flag141-)(and (LIFT-AT-F6)(Flag3-)(Flag4-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)(Flag139-)(Flag140-)))

(:derived (Flag154-)(and (LIFT-AT-F5)(Flag143-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)(NOT-BOARDED-P4)(Flag150-)(Flag151-)(Flag152-)(Flag153-)))

(:derived (Flag163-)(and (LIFT-AT-F4)(Flag3-)(Flag4-)(Flag155-)(Flag156-)(Flag157-)(NOT-BOARDED-P5)(Flag158-)(Flag159-)(Flag160-)(Flag161-)(Flag162-)))

(:derived (Flag165-)(and (Flag164-)))

(:derived (Flag166-)(and (Flag164-)))

(:derived (Flag176-)(and (LIFT-AT-F3)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)(Flag175-)))

(:derived (Flag186-)(and (LIFT-AT-F2)(Flag3-)(Flag4-)(Flag177-)(Flag178-)(Flag179-)(Flag180-)(Flag181-)(Flag182-)(Flag183-)(Flag184-)(Flag185-)))

(:derived (Flag200-)(and (LIFT-AT-F1)(Flag188-)(Flag190-)(Flag191-)(Flag192-)(Flag193-)(Flag194-)(Flag195-)(Flag196-)(Flag197-)(Flag198-)(Flag199-)))

(:derived (Flag214-)(and (LIFT-AT-F0)(Flag202-)(Flag204-)(Flag205-)(Flag206-)(Flag207-)(Flag208-)(Flag209-)(Flag210-)(Flag211-)(Flag212-)(Flag213-)))

(:action BLOCK-ACCESS-P8-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F17)
)
:effect
(and
(NO-ACCESS-P8-F17)
(not (NOT-NO-ACCESS-P8-F17))
)
)
(:action BLOCK-ACCESS-P8-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F16)
)
:effect
(and
(NO-ACCESS-P8-F16)
(not (NOT-NO-ACCESS-P8-F16))
)
)
(:action BLOCK-ACCESS-P8-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F15)
)
:effect
(and
(NO-ACCESS-P8-F15)
(not (NOT-NO-ACCESS-P8-F15))
)
)
(:action BLOCK-ACCESS-P8-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F14)
)
:effect
(and
(NO-ACCESS-P8-F14)
(not (NOT-NO-ACCESS-P8-F14))
)
)
(:action BLOCK-ACCESS-P8-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F13)
)
:effect
(and
(NO-ACCESS-P8-F13)
(not (NOT-NO-ACCESS-P8-F13))
)
)
(:action BLOCK-ACCESS-P8-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F12)
)
:effect
(and
(NO-ACCESS-P8-F12)
(not (NOT-NO-ACCESS-P8-F12))
)
)
(:action BLOCK-ACCESS-P8-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F11)
)
:effect
(and
(NO-ACCESS-P8-F11)
(not (NOT-NO-ACCESS-P8-F11))
)
)
(:action BLOCK-ACCESS-P8-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F10)
)
:effect
(and
(NO-ACCESS-P8-F10)
(not (NOT-NO-ACCESS-P8-F10))
)
)
(:action BLOCK-ACCESS-P8-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F9)
)
:effect
(and
(NO-ACCESS-P8-F9)
(not (NOT-NO-ACCESS-P8-F9))
)
)
(:action BLOCK-ACCESS-P8-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F8)
)
:effect
(and
(NO-ACCESS-P8-F8)
(not (NOT-NO-ACCESS-P8-F8))
)
)
(:action BLOCK-ACCESS-P8-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F7)
)
:effect
(and
(NO-ACCESS-P8-F7)
(not (NOT-NO-ACCESS-P8-F7))
)
)
(:action BLOCK-ACCESS-P8-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F6)
)
:effect
(and
(NO-ACCESS-P8-F6)
(not (NOT-NO-ACCESS-P8-F6))
)
)
(:action BLOCK-ACCESS-P8-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F5)
)
:effect
(and
(NO-ACCESS-P8-F5)
(not (NOT-NO-ACCESS-P8-F5))
)
)
(:action BLOCK-ACCESS-P8-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F4)
)
:effect
(and
(NO-ACCESS-P8-F4)
(not (NOT-NO-ACCESS-P8-F4))
)
)
(:action BLOCK-ACCESS-P8-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F3)
)
:effect
(and
(NO-ACCESS-P8-F3)
(not (NOT-NO-ACCESS-P8-F3))
)
)
(:action BLOCK-ACCESS-P8-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F2)
)
:effect
(and
(NO-ACCESS-P8-F2)
(not (NOT-NO-ACCESS-P8-F2))
)
)
(:action BLOCK-ACCESS-P8-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F1)
)
:effect
(and
(NO-ACCESS-P8-F1)
(not (NOT-NO-ACCESS-P8-F1))
)
)
(:action BLOCK-ACCESS-P8-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F0)
)
:effect
(and
(NO-ACCESS-P8-F0)
(not (NOT-NO-ACCESS-P8-F0))
)
)
(:action BLOCK-ACCESS-P7-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F17)
)
:effect
(and
(NO-ACCESS-P7-F17)
(not (NOT-NO-ACCESS-P7-F17))
)
)
(:action BLOCK-ACCESS-P7-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F16)
)
:effect
(and
(NO-ACCESS-P7-F16)
(not (NOT-NO-ACCESS-P7-F16))
)
)
(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F17)
)
:effect
(and
(NO-ACCESS-P6-F17)
(not (NOT-NO-ACCESS-P6-F17))
)
)
(:action BLOCK-ACCESS-P6-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F16)
)
:effect
(and
(NO-ACCESS-P6-F16)
(not (NOT-NO-ACCESS-P6-F16))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F6)
)
:effect
(and
(NO-ACCESS-P6-F6)
(not (NOT-NO-ACCESS-P6-F6))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P6-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F0)
)
:effect
(and
(NO-ACCESS-P6-F0)
(not (NOT-NO-ACCESS-P6-F0))
)
)
(:action BLOCK-ACCESS-P5-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F17)
)
:effect
(and
(NO-ACCESS-P5-F17)
(not (NOT-NO-ACCESS-P5-F17))
)
)
(:action BLOCK-ACCESS-P5-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F16)
)
:effect
(and
(NO-ACCESS-P5-F16)
(not (NOT-NO-ACCESS-P5-F16))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F17)
)
:effect
(and
(NO-ACCESS-P4-F17)
(not (NOT-NO-ACCESS-P4-F17))
)
)
(:action BLOCK-ACCESS-P4-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F16)
)
:effect
(and
(NO-ACCESS-P4-F16)
(not (NOT-NO-ACCESS-P4-F16))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F17)
)
:effect
(and
(NO-ACCESS-P3-F17)
(not (NOT-NO-ACCESS-P3-F17))
)
)
(:action BLOCK-ACCESS-P3-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F16)
)
:effect
(and
(NO-ACCESS-P3-F16)
(not (NOT-NO-ACCESS-P3-F16))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F10)
)
:effect
(and
(NO-ACCESS-P3-F10)
(not (NOT-NO-ACCESS-P3-F10))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F17)
)
:effect
(and
(NO-ACCESS-P2-F17)
(not (NOT-NO-ACCESS-P2-F17))
)
)
(:action BLOCK-ACCESS-P2-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F16)
)
:effect
(and
(NO-ACCESS-P2-F16)
(not (NOT-NO-ACCESS-P2-F16))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F17)
)
:effect
(and
(NO-ACCESS-P1-F17)
(not (NOT-NO-ACCESS-P1-F17))
)
)
(:action BLOCK-ACCESS-P1-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F16)
)
:effect
(and
(NO-ACCESS-P1-F16)
(not (NOT-NO-ACCESS-P1-F16))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F17)
)
:effect
(and
(NO-ACCESS-P0-F17)
(not (NOT-NO-ACCESS-P0-F17))
)
)
(:action BLOCK-ACCESS-P0-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F16)
)
:effect
(and
(NO-ACCESS-P0-F16)
(not (NOT-NO-ACCESS-P0-F16))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F0
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F0
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F1
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F1
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F2
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F2
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F3
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F3
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F4
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F4
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F5
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F5
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F6
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F6
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F7
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F7
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F8
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F8
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F9
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F9
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F10
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F10
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F11
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F11
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F12
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F12
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F13
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F13
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F14
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F14
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F16-F15
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F15
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F17-F16
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F17))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag3-)(and (NOT-BOARDED-P7)))

(:derived (Flag4-)(and (NOT-BOARDED-P7)))

(:derived (Flag5-)(and (NOT-BOARDED-P8)))

(:derived (Flag5-)(and (NOT-NO-ACCESS-P8-F17)))

(:derived (Flag6-)(and (NOT-BOARDED-P7)))

(:derived (Flag6-)(and (NOT-NO-ACCESS-P7-F17)))

(:derived (Flag7-)(and (NOT-BOARDED-P6)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P6-F17)))

(:derived (Flag8-)(and (NOT-BOARDED-P5)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P5-F17)))

(:derived (Flag9-)(and (NOT-BOARDED-P4)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P4-F17)))

(:derived (Flag10-)(and (NOT-BOARDED-P3)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P3-F17)))

(:derived (Flag11-)(and (NOT-BOARDED-P2)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P2-F17)))

(:derived (Flag12-)(and (NOT-BOARDED-P1)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P1-F17)))

(:derived (Flag13-)(and (NOT-BOARDED-P0)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P0-F17)))

(:derived (Flag15-)(and (NOT-BOARDED-P8)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P8-F16)))

(:derived (Flag16-)(and (NOT-BOARDED-P7)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P7-F16)))

(:derived (Flag17-)(and (NOT-BOARDED-P6)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P6-F16)))

(:derived (Flag18-)(and (NOT-BOARDED-P5)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P5-F16)))

(:derived (Flag19-)(and (NOT-BOARDED-P4)))

(:derived (Flag19-)(and (NOT-NO-ACCESS-P4-F16)))

(:derived (Flag20-)(and (NOT-BOARDED-P3)))

(:derived (Flag20-)(and (NOT-NO-ACCESS-P3-F16)))

(:derived (Flag21-)(and (NOT-BOARDED-P2)))

(:derived (Flag21-)(and (NOT-NO-ACCESS-P2-F16)))

(:derived (Flag22-)(and (NOT-BOARDED-P1)))

(:derived (Flag22-)(and (NOT-NO-ACCESS-P1-F16)))

(:derived (Flag23-)(and (NOT-BOARDED-P0)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P0-F16)))

(:derived (Flag26-)(and (NOT-BOARDED-P7)))

(:derived (Flag28-)(and (NOT-BOARDED-P7)))

(:derived (Flag29-)(and (NOT-BOARDED-P8)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P8-F15)))

(:derived (Flag30-)(and (NOT-BOARDED-P7)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag31-)(and (NOT-BOARDED-P6)))

(:derived (Flag31-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag32-)(and (NOT-BOARDED-P5)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag33-)(and (NOT-BOARDED-P4)))

(:derived (Flag33-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag34-)(and (NOT-BOARDED-P3)))

(:derived (Flag34-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag35-)(and (NOT-BOARDED-P2)))

(:derived (Flag35-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag36-)(and (NOT-BOARDED-P1)))

(:derived (Flag36-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag37-)(and (NOT-BOARDED-P0)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag39-)(and (NOT-BOARDED-P8)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P8-F14)))

(:derived (Flag40-)(and (NOT-BOARDED-P7)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag41-)(and (NOT-BOARDED-P6)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag42-)(and (NOT-BOARDED-P5)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag43-)(and (NOT-BOARDED-P4)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag44-)(and (NOT-BOARDED-P3)))

(:derived (Flag44-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag45-)(and (NOT-BOARDED-P2)))

(:derived (Flag45-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag46-)(and (NOT-BOARDED-P1)))

(:derived (Flag46-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag47-)(and (NOT-BOARDED-P0)))

(:derived (Flag47-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag53-)(and (NOT-BOARDED-P8)))

(:derived (Flag53-)(and (NOT-NO-ACCESS-P8-F13)))

(:derived (Flag54-)(and (NOT-BOARDED-P7)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag55-)(and (NOT-BOARDED-P6)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag56-)(and (NOT-BOARDED-P5)))

(:derived (Flag56-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag57-)(and (NOT-BOARDED-P4)))

(:derived (Flag57-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag58-)(and (NOT-BOARDED-P3)))

(:derived (Flag58-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag59-)(and (NOT-BOARDED-P2)))

(:derived (Flag59-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag60-)(and (NOT-BOARDED-P1)))

(:derived (Flag60-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag61-)(and (NOT-BOARDED-P0)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag63-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag64-)(and (NOT-BOARDED-P7)))

(:derived (Flag65-)(and (NOT-BOARDED-P7)))

(:derived (Flag66-)(and (NOT-BOARDED-P8)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P8-F12)))

(:derived (Flag67-)(and (NOT-BOARDED-P7)))

(:derived (Flag67-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag68-)(and (NOT-BOARDED-P6)))

(:derived (Flag68-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag69-)(and (NOT-BOARDED-P5)))

(:derived (Flag69-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag70-)(and (NOT-BOARDED-P4)))

(:derived (Flag70-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag71-)(and (NOT-BOARDED-P3)))

(:derived (Flag71-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag72-)(and (NOT-BOARDED-P2)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag73-)(and (NOT-BOARDED-P1)))

(:derived (Flag73-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag74-)(and (NOT-BOARDED-P0)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag76-)(and (NOT-BOARDED-P8)))

(:derived (Flag76-)(and (NOT-NO-ACCESS-P8-F11)))

(:derived (Flag77-)(and (NOT-BOARDED-P7)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag78-)(and (NOT-BOARDED-P6)))

(:derived (Flag78-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag79-)(and (NOT-BOARDED-P5)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag80-)(and (NOT-BOARDED-P4)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag81-)(and (NOT-BOARDED-P3)))

(:derived (Flag81-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag82-)(and (NOT-BOARDED-P2)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag83-)(and (NOT-BOARDED-P1)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag84-)(and (NOT-BOARDED-P0)))

(:derived (Flag84-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag86-)(and (NOT-BOARDED-P8)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P8-F10)))

(:derived (Flag87-)(and (NOT-BOARDED-P7)))

(:derived (Flag87-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag88-)(and (NOT-BOARDED-P6)))

(:derived (Flag88-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag89-)(and (NOT-BOARDED-P5)))

(:derived (Flag89-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag90-)(and (NOT-BOARDED-P4)))

(:derived (Flag90-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag91-)(and (NOT-BOARDED-P3)))

(:derived (Flag91-)(and (NOT-NO-ACCESS-P3-F10)))

(:derived (Flag92-)(and (NOT-BOARDED-P2)))

(:derived (Flag92-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag93-)(and (NOT-BOARDED-P1)))

(:derived (Flag93-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag94-)(and (NOT-BOARDED-P0)))

(:derived (Flag94-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag96-)(and (NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag97-)(and (NOT-BOARDED-P7)))

(:derived (Flag98-)(and (NOT-BOARDED-P7)))

(:derived (Flag99-)(and (NOT-BOARDED-P8)))

(:derived (Flag99-)(and (NOT-NO-ACCESS-P8-F9)))

(:derived (Flag100-)(and (NOT-BOARDED-P7)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag101-)(and (NOT-BOARDED-P6)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag102-)(and (NOT-BOARDED-P5)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag103-)(and (NOT-BOARDED-P4)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag104-)(and (NOT-BOARDED-P3)))

(:derived (Flag104-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag105-)(and (NOT-BOARDED-P2)))

(:derived (Flag105-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag106-)(and (NOT-BOARDED-P1)))

(:derived (Flag106-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag107-)(and (NOT-BOARDED-P0)))

(:derived (Flag107-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag110-)(and (NOT-BOARDED-P7)))

(:derived (Flag112-)(and (NOT-BOARDED-P7)))

(:derived (Flag113-)(and (NOT-BOARDED-P8)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P8-F8)))

(:derived (Flag114-)(and (NOT-BOARDED-P7)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag115-)(and (NOT-BOARDED-P6)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag116-)(and (NOT-BOARDED-P5)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag117-)(and (NOT-BOARDED-P4)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag118-)(and (NOT-BOARDED-P3)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag119-)(and (NOT-BOARDED-P2)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag120-)(and (NOT-BOARDED-P1)))

(:derived (Flag120-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag121-)(and (NOT-BOARDED-P0)))

(:derived (Flag121-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag123-)(and (NOT-BOARDED-P8)))

(:derived (Flag123-)(and (NOT-NO-ACCESS-P8-F7)))

(:derived (Flag124-)(and (NOT-BOARDED-P7)))

(:derived (Flag124-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag125-)(and (NOT-BOARDED-P6)))

(:derived (Flag125-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag126-)(and (NOT-BOARDED-P5)))

(:derived (Flag126-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag127-)(and (NOT-BOARDED-P3)))

(:derived (Flag127-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag128-)(and (NOT-BOARDED-P2)))

(:derived (Flag128-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag129-)(and (NOT-BOARDED-P1)))

(:derived (Flag129-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag130-)(and (NOT-BOARDED-P0)))

(:derived (Flag130-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag132-)(and (NOT-BOARDED-P8)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P8-F6)))

(:derived (Flag133-)(and (NOT-BOARDED-P7)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag134-)(and (NOT-BOARDED-P6)))

(:derived (Flag134-)(and (NOT-NO-ACCESS-P6-F6)))

(:derived (Flag135-)(and (NOT-BOARDED-P5)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag136-)(and (NOT-BOARDED-P4)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag137-)(and (NOT-BOARDED-P3)))

(:derived (Flag137-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag138-)(and (NOT-BOARDED-P2)))

(:derived (Flag138-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag139-)(and (NOT-BOARDED-P1)))

(:derived (Flag139-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag140-)(and (NOT-BOARDED-P0)))

(:derived (Flag140-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag143-)(and (NOT-BOARDED-P7)))

(:derived (Flag145-)(and (NOT-BOARDED-P7)))

(:derived (Flag146-)(and (NOT-BOARDED-P8)))

(:derived (Flag146-)(and (NOT-NO-ACCESS-P8-F5)))

(:derived (Flag147-)(and (NOT-BOARDED-P7)))

(:derived (Flag147-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag148-)(and (NOT-BOARDED-P6)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag149-)(and (NOT-BOARDED-P5)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag150-)(and (NOT-BOARDED-P3)))

(:derived (Flag150-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag151-)(and (NOT-BOARDED-P2)))

(:derived (Flag151-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag152-)(and (NOT-BOARDED-P1)))

(:derived (Flag152-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag153-)(and (NOT-BOARDED-P0)))

(:derived (Flag153-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag155-)(and (NOT-BOARDED-P8)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P8-F4)))

(:derived (Flag156-)(and (NOT-BOARDED-P7)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag157-)(and (NOT-BOARDED-P6)))

(:derived (Flag157-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag158-)(and (NOT-BOARDED-P4)))

(:derived (Flag158-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag159-)(and (NOT-BOARDED-P3)))

(:derived (Flag159-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag160-)(and (NOT-BOARDED-P2)))

(:derived (Flag160-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag161-)(and (NOT-BOARDED-P1)))

(:derived (Flag161-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag162-)(and (NOT-BOARDED-P0)))

(:derived (Flag162-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag164-)(and (NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag165-)(and (NOT-BOARDED-P7)))

(:derived (Flag166-)(and (NOT-BOARDED-P7)))

(:derived (Flag167-)(and (NOT-BOARDED-P8)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P8-F3)))

(:derived (Flag168-)(and (NOT-BOARDED-P7)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag169-)(and (NOT-BOARDED-P6)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag170-)(and (NOT-BOARDED-P5)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag171-)(and (NOT-BOARDED-P4)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag172-)(and (NOT-BOARDED-P3)))

(:derived (Flag172-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag173-)(and (NOT-BOARDED-P2)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag174-)(and (NOT-BOARDED-P1)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag175-)(and (NOT-BOARDED-P0)))

(:derived (Flag175-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag177-)(and (NOT-BOARDED-P8)))

(:derived (Flag177-)(and (NOT-NO-ACCESS-P8-F2)))

(:derived (Flag178-)(and (NOT-BOARDED-P7)))

(:derived (Flag178-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag179-)(and (NOT-BOARDED-P6)))

(:derived (Flag179-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag180-)(and (NOT-BOARDED-P5)))

(:derived (Flag180-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag181-)(and (NOT-BOARDED-P4)))

(:derived (Flag181-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag182-)(and (NOT-BOARDED-P3)))

(:derived (Flag182-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag183-)(and (NOT-BOARDED-P2)))

(:derived (Flag183-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag184-)(and (NOT-BOARDED-P1)))

(:derived (Flag184-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag185-)(and (NOT-BOARDED-P0)))

(:derived (Flag185-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag188-)(and (NOT-BOARDED-P7)))

(:derived (Flag190-)(and (NOT-BOARDED-P7)))

(:derived (Flag191-)(and (NOT-BOARDED-P8)))

(:derived (Flag191-)(and (NOT-NO-ACCESS-P8-F1)))

(:derived (Flag192-)(and (NOT-BOARDED-P7)))

(:derived (Flag192-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag193-)(and (NOT-BOARDED-P6)))

(:derived (Flag193-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag194-)(and (NOT-BOARDED-P5)))

(:derived (Flag194-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag195-)(and (NOT-BOARDED-P4)))

(:derived (Flag195-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag196-)(and (NOT-BOARDED-P3)))

(:derived (Flag196-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag197-)(and (NOT-BOARDED-P2)))

(:derived (Flag197-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag198-)(and (NOT-BOARDED-P1)))

(:derived (Flag198-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag199-)(and (NOT-BOARDED-P0)))

(:derived (Flag199-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag202-)(and (NOT-BOARDED-P7)))

(:derived (Flag204-)(and (NOT-BOARDED-P7)))

(:derived (Flag205-)(and (NOT-BOARDED-P8)))

(:derived (Flag205-)(and (NOT-NO-ACCESS-P8-F0)))

(:derived (Flag206-)(and (NOT-BOARDED-P7)))

(:derived (Flag206-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag207-)(and (NOT-BOARDED-P6)))

(:derived (Flag207-)(and (NOT-NO-ACCESS-P6-F0)))

(:derived (Flag208-)(and (NOT-BOARDED-P5)))

(:derived (Flag208-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag209-)(and (NOT-BOARDED-P4)))

(:derived (Flag209-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag210-)(and (NOT-BOARDED-P3)))

(:derived (Flag210-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag211-)(and (NOT-BOARDED-P2)))

(:derived (Flag211-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag212-)(and (NOT-BOARDED-P1)))

(:derived (Flag212-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag213-)(and (NOT-BOARDED-P0)))

(:derived (Flag213-)(and (NOT-NO-ACCESS-P0-F0)))

)
