(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag86-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag69-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag24-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag3-)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F10)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(Flag109-)
(Flag107-)
(Flag105-)
(Flag95-)
(Flag84-)
(Flag78-)
(Flag67-)
(Flag60-)
(Flag53-)
(Flag43-)
(Flag33-)
(Flag22-)
(Flag12-)
(SERVED-P0)
(BOARDED-P1)
(BOARDED-P2)
(BOARDED-P5)
(SERVED-P4)
(SERVED-P1)
(SERVED-P2)
(BOARDED-P0)
(SERVED-P3)
(BOARDED-P3)
(SERVED-P5)
(Flag116-)
(Flag108-)
(Flag106-)
(Flag87-)
(Flag85-)
(Flag70-)
(Flag68-)
(Flag25-)
(Flag23-)
(Flag4-)
(Flag2-)
(Flag1-)
(BOARDED-P4)
(NOT-BOARDED-P4)
(NOT-BOARDED-P3)
(NOT-SERVED-P5)
(NOT-SERVED-P3)
(NOT-BOARDED-P0)
(NOT-SERVED-P1)
(NOT-SERVED-P2)
(NOT-SERVED-P4)
(NOT-BOARDED-P5)
(NOT-BOARDED-P1)
(NOT-BOARDED-P2)
(NOT-SERVED-P0)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F10)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag3-)(and (Flag2-)))

(:derived (Flag5-)(and (Flag4-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag26-)(and (Flag25-)))

(:derived (Flag69-)(and (Flag68-)))

(:derived (Flag71-)(and (Flag70-)))

(:derived (Flag86-)(and (Flag85-)))

(:derived (Flag88-)(and (Flag87-)))

(:derived (Flag107-)(and (Flag106-)))

(:derived (Flag109-)(and (Flag108-)))

(:action STOP-F0
:parameters ()
:precondition
(and
(Flag116-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
)
)
(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)))

(:derived (Flag2-)(and (SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag4-)(and (NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag23-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag25-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag68-)(and (SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag70-)(and (NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag85-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(SERVED-P1)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag87-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(SERVED-P1)(NOT-BOARDED-P0)))

(:derived (Flag106-)(and (NOT-BOARDED-P4)(SERVED-P4)))

(:derived (Flag108-)(and (SERVED-P4)(NOT-BOARDED-P4)))

(:derived (Flag116-)(and (LIFT-AT-F0)(Flag107-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)))

(:action STOP-F11
:parameters ()
:precondition
(and
(Flag12-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag43-)
)
:effect
(and
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag53-)
)
:effect
(and
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag60-)
)
:effect
(and
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag67-)
)
:effect
(and
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag78-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag84-)
)
:effect
(and
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag95-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag105-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:derived (Flag12-)(and (LIFT-AT-F11)(Flag3-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)))

(:derived (Flag14-)(and (Flag13-)))

(:derived (Flag15-)(and (Flag13-)))

(:derived (Flag22-)(and (LIFT-AT-F10)(Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)(Flag20-)(Flag21-)))

(:derived (Flag33-)(and (LIFT-AT-F9)(Flag24-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(Flag32-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag36-)(and (Flag34-)))

(:derived (Flag43-)(and (LIFT-AT-F8)(Flag35-)(Flag36-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)))

(:derived (Flag45-)(and (Flag44-)))

(:derived (Flag46-)(and (Flag44-)))

(:derived (Flag53-)(and (LIFT-AT-F7)(Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)))

(:derived (Flag60-)(and (LIFT-AT-F6)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)))

(:derived (Flag67-)(and (LIFT-AT-F5)(Flag35-)(Flag36-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)))

(:derived (Flag78-)(and (LIFT-AT-F4)(Flag69-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)))

(:derived (Flag84-)(and (LIFT-AT-F3)(Flag35-)(Flag36-)(Flag79-)(Flag80-)(Flag81-)(NOT-BOARDED-P2)(Flag82-)(Flag83-)))

(:derived (Flag95-)(and (LIFT-AT-F2)(Flag86-)(Flag88-)(Flag89-)(Flag90-)(Flag91-)(Flag92-)(Flag93-)(Flag94-)))

(:derived (Flag97-)(and (Flag96-)))

(:derived (Flag98-)(and (Flag96-)))

(:derived (Flag105-)(and (LIFT-AT-F1)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)))

(:derived (Flag107-)(and (Flag34-)))

(:derived (Flag109-)(and (Flag34-)))

(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F10)
)
:effect
(and
(NO-ACCESS-P3-F10)
(not (NOT-NO-ACCESS-P3-F10))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:derived (Flag3-)(and (NOT-BOARDED-P4)))

(:derived (Flag5-)(and (NOT-BOARDED-P4)))

(:derived (Flag6-)(and (NOT-BOARDED-P5)))

(:derived (Flag6-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag7-)(and (NOT-BOARDED-P4)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag8-)(and (NOT-BOARDED-P3)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag9-)(and (NOT-BOARDED-P2)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag10-)(and (NOT-BOARDED-P1)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag11-)(and (NOT-BOARDED-P0)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag13-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag14-)(and (NOT-BOARDED-P4)))

(:derived (Flag15-)(and (NOT-BOARDED-P4)))

(:derived (Flag16-)(and (NOT-BOARDED-P5)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag17-)(and (NOT-BOARDED-P4)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag18-)(and (NOT-BOARDED-P3)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P3-F10)))

(:derived (Flag19-)(and (NOT-BOARDED-P2)))

(:derived (Flag19-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag20-)(and (NOT-BOARDED-P1)))

(:derived (Flag20-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag21-)(and (NOT-BOARDED-P0)))

(:derived (Flag21-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag24-)(and (NOT-BOARDED-P4)))

(:derived (Flag26-)(and (NOT-BOARDED-P4)))

(:derived (Flag27-)(and (NOT-BOARDED-P5)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag28-)(and (NOT-BOARDED-P4)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag29-)(and (NOT-BOARDED-P3)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag30-)(and (NOT-BOARDED-P2)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag31-)(and (NOT-BOARDED-P1)))

(:derived (Flag31-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag32-)(and (NOT-BOARDED-P0)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag34-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag35-)(and (NOT-BOARDED-P4)))

(:derived (Flag36-)(and (NOT-BOARDED-P4)))

(:derived (Flag37-)(and (NOT-BOARDED-P5)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag38-)(and (NOT-BOARDED-P4)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag39-)(and (NOT-BOARDED-P3)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag40-)(and (NOT-BOARDED-P2)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag41-)(and (NOT-BOARDED-P1)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag42-)(and (NOT-BOARDED-P0)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag44-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P0)))

(:derived (Flag45-)(and (NOT-BOARDED-P4)))

(:derived (Flag46-)(and (NOT-BOARDED-P4)))

(:derived (Flag47-)(and (NOT-BOARDED-P5)))

(:derived (Flag47-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag48-)(and (NOT-BOARDED-P4)))

(:derived (Flag48-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag49-)(and (NOT-BOARDED-P3)))

(:derived (Flag49-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag50-)(and (NOT-BOARDED-P2)))

(:derived (Flag50-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag51-)(and (NOT-BOARDED-P1)))

(:derived (Flag51-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag52-)(and (NOT-BOARDED-P0)))

(:derived (Flag52-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag54-)(and (NOT-BOARDED-P5)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag55-)(and (NOT-BOARDED-P4)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag56-)(and (NOT-BOARDED-P3)))

(:derived (Flag56-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag57-)(and (NOT-BOARDED-P2)))

(:derived (Flag57-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag58-)(and (NOT-BOARDED-P1)))

(:derived (Flag58-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag59-)(and (NOT-BOARDED-P0)))

(:derived (Flag59-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag61-)(and (NOT-BOARDED-P5)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag62-)(and (NOT-BOARDED-P4)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag63-)(and (NOT-BOARDED-P3)))

(:derived (Flag63-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag64-)(and (NOT-BOARDED-P2)))

(:derived (Flag64-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag65-)(and (NOT-BOARDED-P1)))

(:derived (Flag65-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag66-)(and (NOT-BOARDED-P0)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag69-)(and (NOT-BOARDED-P4)))

(:derived (Flag71-)(and (NOT-BOARDED-P4)))

(:derived (Flag72-)(and (NOT-BOARDED-P5)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag73-)(and (NOT-BOARDED-P4)))

(:derived (Flag73-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag74-)(and (NOT-BOARDED-P3)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag75-)(and (NOT-BOARDED-P2)))

(:derived (Flag75-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag76-)(and (NOT-BOARDED-P1)))

(:derived (Flag76-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag77-)(and (NOT-BOARDED-P0)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag79-)(and (NOT-BOARDED-P5)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag80-)(and (NOT-BOARDED-P4)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag81-)(and (NOT-BOARDED-P3)))

(:derived (Flag81-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag82-)(and (NOT-BOARDED-P1)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag83-)(and (NOT-BOARDED-P0)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag86-)(and (NOT-BOARDED-P4)))

(:derived (Flag88-)(and (NOT-BOARDED-P4)))

(:derived (Flag89-)(and (NOT-BOARDED-P5)))

(:derived (Flag89-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag90-)(and (NOT-BOARDED-P4)))

(:derived (Flag90-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag91-)(and (NOT-BOARDED-P3)))

(:derived (Flag91-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag92-)(and (NOT-BOARDED-P2)))

(:derived (Flag92-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag93-)(and (NOT-BOARDED-P1)))

(:derived (Flag93-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag94-)(and (NOT-BOARDED-P0)))

(:derived (Flag94-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag96-)(and (NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)))

(:derived (Flag97-)(and (NOT-BOARDED-P4)))

(:derived (Flag98-)(and (NOT-BOARDED-P4)))

(:derived (Flag99-)(and (NOT-BOARDED-P5)))

(:derived (Flag99-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag100-)(and (NOT-BOARDED-P4)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag101-)(and (NOT-BOARDED-P3)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag102-)(and (NOT-BOARDED-P2)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag103-)(and (NOT-BOARDED-P1)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag104-)(and (NOT-BOARDED-P0)))

(:derived (Flag104-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag110-)(and (NOT-BOARDED-P5)))

(:derived (Flag110-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag111-)(and (NOT-BOARDED-P4)))

(:derived (Flag111-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag112-)(and (NOT-BOARDED-P3)))

(:derived (Flag112-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag113-)(and (NOT-BOARDED-P2)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag114-)(and (NOT-BOARDED-P1)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag115-)(and (NOT-BOARDED-P0)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P0-F0)))

)
