(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag225-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag177-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag105-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag49-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag34-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag19-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag2-)
(LIFT-AT-F21)
(LIFT-AT-F20)
(LIFT-AT-F19)
(LIFT-AT-F18)
(LIFT-AT-F17)
(LIFT-AT-F16)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P0-F16)
(NO-ACCESS-P0-F17)
(NO-ACCESS-P0-F18)
(NO-ACCESS-P0-F19)
(NO-ACCESS-P0-F20)
(NO-ACCESS-P0-F21)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P1-F16)
(NO-ACCESS-P1-F17)
(NO-ACCESS-P1-F18)
(NO-ACCESS-P1-F19)
(NO-ACCESS-P1-F20)
(NO-ACCESS-P1-F21)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P2-F16)
(NO-ACCESS-P2-F17)
(NO-ACCESS-P2-F18)
(NO-ACCESS-P2-F19)
(NO-ACCESS-P2-F20)
(NO-ACCESS-P2-F21)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F10)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P3-F16)
(NO-ACCESS-P3-F17)
(NO-ACCESS-P3-F18)
(NO-ACCESS-P3-F19)
(NO-ACCESS-P3-F20)
(NO-ACCESS-P3-F21)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P4-F16)
(NO-ACCESS-P4-F17)
(NO-ACCESS-P4-F18)
(NO-ACCESS-P4-F19)
(NO-ACCESS-P4-F20)
(NO-ACCESS-P4-F21)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P5-F16)
(NO-ACCESS-P5-F17)
(NO-ACCESS-P5-F18)
(NO-ACCESS-P5-F19)
(NO-ACCESS-P5-F20)
(NO-ACCESS-P6-F0)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P6-F16)
(NO-ACCESS-P6-F17)
(NO-ACCESS-P6-F18)
(NO-ACCESS-P6-F19)
(NO-ACCESS-P6-F20)
(NO-ACCESS-P6-F21)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(NO-ACCESS-P7-F16)
(NO-ACCESS-P7-F17)
(NO-ACCESS-P7-F18)
(NO-ACCESS-P7-F19)
(NO-ACCESS-P7-F21)
(NO-ACCESS-P8-F0)
(NO-ACCESS-P8-F1)
(NO-ACCESS-P8-F2)
(NO-ACCESS-P8-F3)
(NO-ACCESS-P8-F4)
(NO-ACCESS-P8-F5)
(NO-ACCESS-P8-F6)
(NO-ACCESS-P8-F7)
(NO-ACCESS-P8-F8)
(NO-ACCESS-P8-F9)
(NO-ACCESS-P8-F10)
(NO-ACCESS-P8-F11)
(NO-ACCESS-P8-F12)
(NO-ACCESS-P8-F13)
(NO-ACCESS-P8-F14)
(NO-ACCESS-P8-F15)
(NO-ACCESS-P8-F16)
(NO-ACCESS-P8-F17)
(NO-ACCESS-P8-F18)
(NO-ACCESS-P8-F19)
(NO-ACCESS-P8-F20)
(NO-ACCESS-P8-F21)
(NO-ACCESS-P9-F0)
(NO-ACCESS-P9-F1)
(NO-ACCESS-P9-F2)
(NO-ACCESS-P9-F3)
(NO-ACCESS-P9-F4)
(NO-ACCESS-P9-F5)
(NO-ACCESS-P9-F6)
(NO-ACCESS-P9-F7)
(NO-ACCESS-P9-F8)
(NO-ACCESS-P9-F9)
(NO-ACCESS-P9-F10)
(NO-ACCESS-P9-F11)
(NO-ACCESS-P9-F12)
(NO-ACCESS-P9-F13)
(NO-ACCESS-P9-F14)
(NO-ACCESS-P9-F15)
(NO-ACCESS-P9-F16)
(NO-ACCESS-P9-F17)
(NO-ACCESS-P9-F18)
(NO-ACCESS-P9-F19)
(NO-ACCESS-P9-F20)
(NO-ACCESS-P9-F21)
(NO-ACCESS-P10-F0)
(NO-ACCESS-P10-F1)
(NO-ACCESS-P10-F2)
(NO-ACCESS-P10-F3)
(NO-ACCESS-P10-F4)
(NO-ACCESS-P10-F5)
(NO-ACCESS-P10-F6)
(NO-ACCESS-P10-F7)
(NO-ACCESS-P10-F8)
(NO-ACCESS-P10-F9)
(NO-ACCESS-P10-F10)
(NO-ACCESS-P10-F11)
(NO-ACCESS-P10-F12)
(NO-ACCESS-P10-F13)
(NO-ACCESS-P10-F14)
(NO-ACCESS-P10-F15)
(NO-ACCESS-P10-F16)
(NO-ACCESS-P10-F17)
(NO-ACCESS-P10-F18)
(NO-ACCESS-P10-F19)
(NO-ACCESS-P10-F20)
(NO-ACCESS-P10-F21)
(Flag307-)
(Flag305-)
(Flag303-)
(Flag253-)
(Flag251-)
(Flag227-)
(Flag226-)
(Flag212-)
(Flag210-)
(Flag196-)
(Flag194-)
(Flag179-)
(Flag178-)
(Flag162-)
(Flag160-)
(Flag134-)
(Flag107-)
(Flag106-)
(Flag92-)
(Flag90-)
(Flag51-)
(Flag50-)
(Flag36-)
(Flag35-)
(Flag21-)
(Flag20-)
(Flag6-)
(Flag4-)
(SERVED-P7)
(SERVED-P5)
(Flag320-)
(Flag289-)
(Flag277-)
(Flag265-)
(Flag249-)
(Flag238-)
(Flag224-)
(Flag208-)
(Flag195-)
(Flag193-)
(Flag192-)
(Flag176-)
(Flag158-)
(Flag146-)
(Flag120-)
(Flag104-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag76-)
(Flag64-)
(Flag48-)
(Flag33-)
(Flag18-)
(BOARDED-P0)
(BOARDED-P8)
(BOARDED-P1)
(BOARDED-P9)
(SERVED-P10)
(BOARDED-P4)
(BOARDED-P10)
(SERVED-P6)
(BOARDED-P5)
(SERVED-P4)
(BOARDED-P3)
(BOARDED-P6)
(SERVED-P3)
(BOARDED-P7)
(SERVED-P2)
(SERVED-P9)
(BOARDED-P2)
(SERVED-P0)
(SERVED-P1)
(SERVED-P8)
(Flag306-)
(Flag304-)
(Flag252-)
(Flag250-)
(Flag211-)
(Flag209-)
(Flag163-)
(Flag161-)
(Flag159-)
(Flag5-)
(Flag3-)
(Flag1-)
(NOT-BOARDED-P2)
(NOT-SERVED-P0)
(NOT-SERVED-P1)
(NOT-SERVED-P8)
(NOT-SERVED-P9)
(NOT-SERVED-P2)
(NOT-BOARDED-P7)
(NOT-SERVED-P3)
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(NOT-SERVED-P4)
(NOT-BOARDED-P5)
(NOT-BOARDED-P4)
(NOT-BOARDED-P10)
(NOT-SERVED-P6)
(NOT-SERVED-P10)
(NOT-BOARDED-P9)
(NOT-BOARDED-P1)
(NOT-BOARDED-P0)
(NOT-BOARDED-P8)
(NOT-SERVED-P5)
(NOT-SERVED-P7)
(NOT-NO-ACCESS-P10-F21)
(NOT-NO-ACCESS-P10-F20)
(NOT-NO-ACCESS-P10-F19)
(NOT-NO-ACCESS-P10-F18)
(NOT-NO-ACCESS-P10-F17)
(NOT-NO-ACCESS-P10-F16)
(NOT-NO-ACCESS-P10-F15)
(NOT-NO-ACCESS-P10-F14)
(NOT-NO-ACCESS-P10-F13)
(NOT-NO-ACCESS-P10-F12)
(NOT-NO-ACCESS-P10-F11)
(NOT-NO-ACCESS-P10-F10)
(NOT-NO-ACCESS-P10-F9)
(NOT-NO-ACCESS-P10-F8)
(NOT-NO-ACCESS-P10-F7)
(NOT-NO-ACCESS-P10-F6)
(NOT-NO-ACCESS-P10-F5)
(NOT-NO-ACCESS-P10-F4)
(NOT-NO-ACCESS-P10-F3)
(NOT-NO-ACCESS-P10-F2)
(NOT-NO-ACCESS-P10-F1)
(NOT-NO-ACCESS-P10-F0)
(NOT-NO-ACCESS-P9-F21)
(NOT-NO-ACCESS-P9-F20)
(NOT-NO-ACCESS-P9-F19)
(NOT-NO-ACCESS-P9-F18)
(NOT-NO-ACCESS-P9-F17)
(NOT-NO-ACCESS-P9-F16)
(NOT-NO-ACCESS-P9-F15)
(NOT-NO-ACCESS-P9-F14)
(NOT-NO-ACCESS-P9-F13)
(NOT-NO-ACCESS-P9-F12)
(NOT-NO-ACCESS-P9-F11)
(NOT-NO-ACCESS-P9-F10)
(NOT-NO-ACCESS-P9-F9)
(NOT-NO-ACCESS-P9-F8)
(NOT-NO-ACCESS-P9-F7)
(NOT-NO-ACCESS-P9-F6)
(NOT-NO-ACCESS-P9-F5)
(NOT-NO-ACCESS-P9-F4)
(NOT-NO-ACCESS-P9-F3)
(NOT-NO-ACCESS-P9-F2)
(NOT-NO-ACCESS-P9-F1)
(NOT-NO-ACCESS-P9-F0)
(NOT-NO-ACCESS-P8-F21)
(NOT-NO-ACCESS-P8-F20)
(NOT-NO-ACCESS-P8-F19)
(NOT-NO-ACCESS-P8-F18)
(NOT-NO-ACCESS-P8-F17)
(NOT-NO-ACCESS-P8-F16)
(NOT-NO-ACCESS-P8-F15)
(NOT-NO-ACCESS-P8-F14)
(NOT-NO-ACCESS-P8-F13)
(NOT-NO-ACCESS-P8-F12)
(NOT-NO-ACCESS-P8-F11)
(NOT-NO-ACCESS-P8-F10)
(NOT-NO-ACCESS-P8-F9)
(NOT-NO-ACCESS-P8-F8)
(NOT-NO-ACCESS-P8-F7)
(NOT-NO-ACCESS-P8-F6)
(NOT-NO-ACCESS-P8-F5)
(NOT-NO-ACCESS-P8-F4)
(NOT-NO-ACCESS-P8-F3)
(NOT-NO-ACCESS-P8-F2)
(NOT-NO-ACCESS-P8-F1)
(NOT-NO-ACCESS-P8-F0)
(NOT-NO-ACCESS-P7-F21)
(NOT-NO-ACCESS-P7-F19)
(NOT-NO-ACCESS-P7-F18)
(NOT-NO-ACCESS-P7-F17)
(NOT-NO-ACCESS-P7-F16)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F21)
(NOT-NO-ACCESS-P6-F20)
(NOT-NO-ACCESS-P6-F19)
(NOT-NO-ACCESS-P6-F18)
(NOT-NO-ACCESS-P6-F17)
(NOT-NO-ACCESS-P6-F16)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P6-F0)
(NOT-NO-ACCESS-P5-F20)
(NOT-NO-ACCESS-P5-F19)
(NOT-NO-ACCESS-P5-F18)
(NOT-NO-ACCESS-P5-F17)
(NOT-NO-ACCESS-P5-F16)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F21)
(NOT-NO-ACCESS-P4-F20)
(NOT-NO-ACCESS-P4-F19)
(NOT-NO-ACCESS-P4-F18)
(NOT-NO-ACCESS-P4-F17)
(NOT-NO-ACCESS-P4-F16)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F21)
(NOT-NO-ACCESS-P3-F20)
(NOT-NO-ACCESS-P3-F19)
(NOT-NO-ACCESS-P3-F18)
(NOT-NO-ACCESS-P3-F17)
(NOT-NO-ACCESS-P3-F16)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F10)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F21)
(NOT-NO-ACCESS-P2-F20)
(NOT-NO-ACCESS-P2-F19)
(NOT-NO-ACCESS-P2-F18)
(NOT-NO-ACCESS-P2-F17)
(NOT-NO-ACCESS-P2-F16)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F21)
(NOT-NO-ACCESS-P1-F20)
(NOT-NO-ACCESS-P1-F19)
(NOT-NO-ACCESS-P1-F18)
(NOT-NO-ACCESS-P1-F17)
(NOT-NO-ACCESS-P1-F16)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F21)
(NOT-NO-ACCESS-P0-F20)
(NOT-NO-ACCESS-P0-F19)
(NOT-NO-ACCESS-P0-F18)
(NOT-NO-ACCESS-P0-F17)
(NOT-NO-ACCESS-P0-F16)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag6-)(and (Flag5-)))

(:derived (Flag160-)(and (Flag159-)))

(:derived (Flag162-)(and (Flag161-)))

(:derived (Flag164-)(and (Flag163-)))

(:derived (Flag210-)(and (Flag209-)))

(:derived (Flag212-)(and (Flag211-)))

(:derived (Flag251-)(and (Flag250-)))

(:derived (Flag253-)(and (Flag252-)))

(:derived (Flag305-)(and (Flag304-)))

(:derived (Flag307-)(and (Flag306-)))

(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)(SERVED-P8)(SERVED-P9)(SERVED-P10)))

(:derived (Flag3-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P2)(NOT-BOARDED-P2)))

(:derived (Flag5-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P2)))

(:derived (Flag7-)(and (BOARDED-P4)))

(:derived (Flag7-)(and (BOARDED-P3)))

(:derived (Flag7-)(and (BOARDED-P2)))

(:derived (Flag22-)(and (BOARDED-P8)))

(:derived (Flag22-)(and (BOARDED-P4)))

(:derived (Flag22-)(and (BOARDED-P3)))

(:derived (Flag22-)(and (BOARDED-P2)))

(:derived (Flag22-)(and (BOARDED-P1)))

(:derived (Flag22-)(and (BOARDED-P0)))

(:derived (Flag52-)(and (BOARDED-P8)))

(:derived (Flag52-)(and (BOARDED-P4)))

(:derived (Flag52-)(and (BOARDED-P3)))

(:derived (Flag52-)(and (BOARDED-P1)))

(:derived (Flag52-)(and (BOARDED-P0)))

(:derived (Flag90-)(and (Flag89-)))

(:derived (Flag92-)(and (Flag91-)))

(:derived (Flag108-)(and (BOARDED-P8)))

(:derived (Flag108-)(and (BOARDED-P4)))

(:derived (Flag108-)(and (BOARDED-P2)))

(:derived (Flag108-)(and (BOARDED-P1)))

(:derived (Flag108-)(and (BOARDED-P0)))

(:derived (Flag159-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P6)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag161-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(SERVED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag163-)(and (NOT-BOARDED-P6)(SERVED-P6)))

(:derived (Flag164-)(and (BOARDED-P8)))

(:derived (Flag164-)(and (BOARDED-P4)))

(:derived (Flag164-)(and (BOARDED-P3)))

(:derived (Flag164-)(and (BOARDED-P2)))

(:derived (Flag164-)(and (BOARDED-P1)))

(:derived (Flag164-)(and (BOARDED-P0)))

(:derived (Flag180-)(and (BOARDED-P8)))

(:derived (Flag180-)(and (BOARDED-P3)))

(:derived (Flag180-)(and (BOARDED-P2)))

(:derived (Flag180-)(and (BOARDED-P1)))

(:derived (Flag180-)(and (BOARDED-P0)))

(:derived (Flag194-)(and (Flag193-)))

(:derived (Flag196-)(and (Flag195-)))

(:derived (Flag209-)(and (SERVED-P10)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P4)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag211-)(and (NOT-BOARDED-P10)(SERVED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P4)(SERVED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag250-)(and (NOT-BOARDED-P10)(SERVED-P9)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag252-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(SERVED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag266-)(and (BOARDED-P8)))

(:derived (Flag266-)(and (BOARDED-P4)))

(:derived (Flag266-)(and (BOARDED-P3)))

(:derived (Flag266-)(and (BOARDED-P2)))

(:derived (Flag266-)(and (BOARDED-P1)))

(:derived (Flag266-)(and (BOARDED-P0)))

(:derived (Flag304-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(SERVED-P8)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag306-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag308-)(and (BOARDED-P8)))

(:derived (Flag308-)(and (BOARDED-P4)))

(:derived (Flag308-)(and (BOARDED-P3)))

(:derived (Flag308-)(and (BOARDED-P2)))

(:derived (Flag308-)(and (BOARDED-P1)))

(:derived (Flag308-)(and (BOARDED-P0)))

(:action STOP-F21
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
(when
(and
(BOARDED-P8)
)
(and
(NOT-BOARDED-P8)
(SERVED-P8)
(not (BOARDED-P8))
(not (NOT-SERVED-P8))
)
)
)
)
(:action STOP-F20
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(when
(and
(BOARDED-P9)
)
(and
(NOT-BOARDED-P9)
(SERVED-P9)
(not (BOARDED-P9))
(not (NOT-SERVED-P9))
)
)
)
)
(:action STOP-F19
:parameters ()
:precondition
(and
(Flag48-)
)
:effect
(and
)
)
(:action STOP-F18
:parameters ()
:precondition
(and
(Flag64-)
)
:effect
(and
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F17
:parameters ()
:precondition
(and
(Flag76-)
)
:effect
(and
)
)
(:action STOP-F16
:parameters ()
:precondition
(and
(Flag88-)
)
:effect
(and
)
)
(:action STOP-F15
:parameters ()
:precondition
(and
(Flag104-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
)
)
(:action STOP-F14
:parameters ()
:precondition
(and
(Flag120-)
)
:effect
(and
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag146-)
)
:effect
(and
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag158-)
)
:effect
(and
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag176-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag192-)
)
:effect
(and
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag208-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag224-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
(when
(and
(NOT-SERVED-P10)
)
(and
(BOARDED-P10)
(not (NOT-BOARDED-P10))
)
)
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag238-)
)
:effect
(and
(when
(and
(BOARDED-P10)
)
(and
(NOT-BOARDED-P10)
(SERVED-P10)
(not (BOARDED-P10))
(not (NOT-SERVED-P10))
)
)
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag249-)
)
:effect
(and
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag265-)
)
:effect
(and
(when
(and
(NOT-SERVED-P9)
)
(and
(BOARDED-P9)
(not (NOT-BOARDED-P9))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag277-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag289-)
)
:effect
(and
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag320-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(NOT-SERVED-P8)
)
(and
(BOARDED-P8)
(not (NOT-BOARDED-P8))
)
)
)
)
(:derived (Flag18-)(and (LIFT-AT-F21)(Flag4-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(NOT-BOARDED-P5)(Flag13-)(Flag14-)(Flag15-)(Flag16-)(Flag17-)))

(:derived (Flag33-)(and (LIFT-AT-F20)(Flag20-)(Flag21-)(Flag22-)(Flag23-)(Flag24-)(Flag25-)(NOT-BOARDED-P7)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(Flag32-)))

(:derived (Flag48-)(and (LIFT-AT-F19)(Flag35-)(Flag36-)(Flag22-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)))

(:derived (Flag64-)(and (LIFT-AT-F18)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)))

(:derived (Flag76-)(and (LIFT-AT-F17)(Flag35-)(Flag36-)(Flag22-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)))

(:derived (Flag88-)(and (LIFT-AT-F16)(Flag35-)(Flag36-)(Flag22-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)(Flag85-)(Flag86-)(Flag87-)))

(:derived (Flag89-)(and (NOT-BOARDED-P7)(SERVED-P7)(NOT-BOARDED-P5)))

(:derived (Flag91-)(and (SERVED-P7)(NOT-BOARDED-P7)(NOT-BOARDED-P5)))

(:derived (Flag104-)(and (LIFT-AT-F15)(Flag90-)(Flag92-)(Flag22-)(Flag93-)(Flag94-)(Flag95-)(Flag96-)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)))

(:derived (Flag120-)(and (LIFT-AT-F14)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)))

(:derived (Flag146-)(and (LIFT-AT-F12)(Flag35-)(Flag36-)(Flag22-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)(Flag139-)(Flag140-)(Flag141-)(Flag142-)(Flag143-)(Flag144-)(Flag145-)))

(:derived (Flag158-)(and (LIFT-AT-F11)(Flag35-)(Flag36-)(Flag22-)(Flag147-)(Flag148-)(Flag149-)(Flag150-)(Flag151-)(Flag152-)(Flag153-)(Flag154-)(Flag155-)(Flag156-)(Flag157-)))

(:derived (Flag176-)(and (LIFT-AT-F10)(Flag160-)(Flag162-)(Flag164-)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)(Flag175-)))

(:derived (Flag192-)(and (LIFT-AT-F9)(Flag178-)(Flag179-)(Flag180-)(Flag181-)(Flag182-)(Flag183-)(Flag184-)(Flag185-)(Flag186-)(Flag187-)(Flag188-)(Flag189-)(Flag190-)(Flag191-)))

(:derived (Flag193-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)(SERVED-P5)))

(:derived (Flag195-)(and (NOT-BOARDED-P7)(SERVED-P5)(NOT-BOARDED-P5)))

(:derived (Flag208-)(and (LIFT-AT-F8)(Flag194-)(Flag196-)(Flag22-)(Flag197-)(Flag198-)(Flag199-)(Flag200-)(Flag201-)(Flag202-)(Flag203-)(Flag204-)(Flag205-)(Flag206-)(Flag207-)))

(:derived (Flag224-)(and (LIFT-AT-F7)(Flag210-)(Flag212-)(Flag213-)(Flag214-)(Flag215-)(Flag216-)(Flag217-)(Flag218-)(Flag219-)(Flag220-)(Flag221-)(Flag222-)(Flag223-)))

(:derived (Flag238-)(and (LIFT-AT-F6)(Flag226-)(Flag227-)(Flag22-)(Flag228-)(Flag229-)(Flag230-)(Flag231-)(NOT-BOARDED-P6)(Flag232-)(Flag233-)(Flag234-)(Flag235-)(Flag236-)(Flag237-)))

(:derived (Flag249-)(and (LIFT-AT-F5)(Flag35-)(Flag36-)(Flag22-)(Flag239-)(Flag240-)(Flag241-)(Flag242-)(Flag243-)(Flag244-)(Flag245-)(Flag246-)(NOT-BOARDED-P2)(Flag247-)(Flag248-)))

(:derived (Flag265-)(and (LIFT-AT-F4)(Flag251-)(Flag253-)(Flag22-)(Flag254-)(Flag255-)(Flag256-)(Flag257-)(Flag258-)(Flag259-)(Flag260-)(Flag261-)(Flag262-)(Flag263-)(Flag264-)))

(:derived (Flag277-)(and (LIFT-AT-F3)(Flag35-)(Flag36-)(Flag266-)(Flag267-)(Flag268-)(Flag269-)(Flag270-)(Flag271-)(Flag272-)(Flag273-)(Flag274-)(NOT-BOARDED-P2)(Flag275-)(Flag276-)))

(:derived (Flag289-)(and (LIFT-AT-F2)(Flag35-)(Flag36-)(Flag22-)(Flag278-)(Flag279-)(Flag280-)(Flag281-)(Flag282-)(Flag283-)(Flag284-)(Flag285-)(Flag286-)(Flag287-)(Flag288-)))

(:derived (Flag320-)(and (LIFT-AT-F0)(Flag305-)(Flag307-)(Flag308-)(Flag309-)(Flag310-)(Flag311-)(Flag312-)(Flag313-)(Flag314-)(Flag315-)(Flag316-)(Flag317-)(Flag318-)(Flag319-)))

(:action STOP-F13
:parameters ()
:precondition
(and
(Flag134-)
)
:effect
(and
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag303-)
)
:effect
(and
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F14))
)
)
(:action UP-F15-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F15))
)
)
(:action UP-F16-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F16))
)
)
(:action UP-F17-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F17))
)
)
(:action UP-F18-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F18))
)
)
(:action UP-F19-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F19))
)
)
(:action UP-F20-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F20))
)
)
(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag6-)(and (Flag2-)))

(:derived (Flag20-)(and (Flag2-)))

(:derived (Flag20-)(and (Flag19-)))

(:derived (Flag21-)(and (Flag19-)))

(:derived (Flag21-)(and (Flag2-)))

(:derived (Flag35-)(and (Flag2-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag36-)(and (Flag34-)))

(:derived (Flag36-)(and (Flag2-)))

(:derived (Flag50-)(and (Flag2-)))

(:derived (Flag50-)(and (Flag49-)))

(:derived (Flag51-)(and (Flag49-)))

(:derived (Flag51-)(and (Flag2-)))

(:derived (Flag90-)(and (Flag34-)))

(:derived (Flag92-)(and (Flag34-)))

(:derived (Flag106-)(and (Flag2-)))

(:derived (Flag106-)(and (Flag105-)))

(:derived (Flag107-)(and (Flag105-)))

(:derived (Flag107-)(and (Flag2-)))

(:derived (Flag121-)(and (Flag34-)))

(:derived (Flag122-)(and (Flag34-)))

(:derived (Flag134-)(and (LIFT-AT-F13)(Flag121-)(Flag122-)(Flag22-)(Flag123-)(Flag124-)(Flag125-)(Flag126-)(Flag127-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)))

(:derived (Flag160-)(and (Flag2-)))

(:derived (Flag162-)(and (Flag2-)))

(:derived (Flag178-)(and (Flag2-)))

(:derived (Flag178-)(and (Flag177-)))

(:derived (Flag179-)(and (Flag177-)))

(:derived (Flag179-)(and (Flag2-)))

(:derived (Flag194-)(and (Flag34-)))

(:derived (Flag196-)(and (Flag34-)))

(:derived (Flag210-)(and (Flag2-)))

(:derived (Flag212-)(and (Flag2-)))

(:derived (Flag226-)(and (Flag2-)))

(:derived (Flag226-)(and (Flag225-)))

(:derived (Flag227-)(and (Flag225-)))

(:derived (Flag227-)(and (Flag2-)))

(:derived (Flag251-)(and (Flag2-)))

(:derived (Flag253-)(and (Flag2-)))

(:derived (Flag290-)(and (Flag34-)))

(:derived (Flag291-)(and (Flag34-)))

(:derived (Flag303-)(and (LIFT-AT-F1)(Flag290-)(Flag291-)(Flag22-)(Flag292-)(Flag293-)(Flag294-)(Flag295-)(Flag296-)(Flag297-)(Flag298-)(Flag299-)(Flag300-)(Flag301-)(Flag302-)))

(:derived (Flag305-)(and (Flag2-)))

(:derived (Flag307-)(and (Flag2-)))

(:action BLOCK-ACCESS-P10-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F21)
)
:effect
(and
(NO-ACCESS-P10-F21)
(not (NOT-NO-ACCESS-P10-F21))
)
)
(:action BLOCK-ACCESS-P10-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F20)
)
:effect
(and
(NO-ACCESS-P10-F20)
(not (NOT-NO-ACCESS-P10-F20))
)
)
(:action BLOCK-ACCESS-P10-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F19)
)
:effect
(and
(NO-ACCESS-P10-F19)
(not (NOT-NO-ACCESS-P10-F19))
)
)
(:action BLOCK-ACCESS-P10-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F18)
)
:effect
(and
(NO-ACCESS-P10-F18)
(not (NOT-NO-ACCESS-P10-F18))
)
)
(:action BLOCK-ACCESS-P10-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F17)
)
:effect
(and
(NO-ACCESS-P10-F17)
(not (NOT-NO-ACCESS-P10-F17))
)
)
(:action BLOCK-ACCESS-P10-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F16)
)
:effect
(and
(NO-ACCESS-P10-F16)
(not (NOT-NO-ACCESS-P10-F16))
)
)
(:action BLOCK-ACCESS-P10-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F15)
)
:effect
(and
(NO-ACCESS-P10-F15)
(not (NOT-NO-ACCESS-P10-F15))
)
)
(:action BLOCK-ACCESS-P10-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F14)
)
:effect
(and
(NO-ACCESS-P10-F14)
(not (NOT-NO-ACCESS-P10-F14))
)
)
(:action BLOCK-ACCESS-P10-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F13)
)
:effect
(and
(NO-ACCESS-P10-F13)
(not (NOT-NO-ACCESS-P10-F13))
)
)
(:action BLOCK-ACCESS-P10-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F12)
)
:effect
(and
(NO-ACCESS-P10-F12)
(not (NOT-NO-ACCESS-P10-F12))
)
)
(:action BLOCK-ACCESS-P10-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F11)
)
:effect
(and
(NO-ACCESS-P10-F11)
(not (NOT-NO-ACCESS-P10-F11))
)
)
(:action BLOCK-ACCESS-P10-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F10)
)
:effect
(and
(NO-ACCESS-P10-F10)
(not (NOT-NO-ACCESS-P10-F10))
)
)
(:action BLOCK-ACCESS-P10-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F9)
)
:effect
(and
(NO-ACCESS-P10-F9)
(not (NOT-NO-ACCESS-P10-F9))
)
)
(:action BLOCK-ACCESS-P10-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F8)
)
:effect
(and
(NO-ACCESS-P10-F8)
(not (NOT-NO-ACCESS-P10-F8))
)
)
(:action BLOCK-ACCESS-P10-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F7)
)
:effect
(and
(NO-ACCESS-P10-F7)
(not (NOT-NO-ACCESS-P10-F7))
)
)
(:action BLOCK-ACCESS-P10-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F6)
)
:effect
(and
(NO-ACCESS-P10-F6)
(not (NOT-NO-ACCESS-P10-F6))
)
)
(:action BLOCK-ACCESS-P10-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F5)
)
:effect
(and
(NO-ACCESS-P10-F5)
(not (NOT-NO-ACCESS-P10-F5))
)
)
(:action BLOCK-ACCESS-P10-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F4)
)
:effect
(and
(NO-ACCESS-P10-F4)
(not (NOT-NO-ACCESS-P10-F4))
)
)
(:action BLOCK-ACCESS-P10-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F3)
)
:effect
(and
(NO-ACCESS-P10-F3)
(not (NOT-NO-ACCESS-P10-F3))
)
)
(:action BLOCK-ACCESS-P10-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F2)
)
:effect
(and
(NO-ACCESS-P10-F2)
(not (NOT-NO-ACCESS-P10-F2))
)
)
(:action BLOCK-ACCESS-P10-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F1)
)
:effect
(and
(NO-ACCESS-P10-F1)
(not (NOT-NO-ACCESS-P10-F1))
)
)
(:action BLOCK-ACCESS-P10-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F0)
)
:effect
(and
(NO-ACCESS-P10-F0)
(not (NOT-NO-ACCESS-P10-F0))
)
)
(:action BLOCK-ACCESS-P9-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F21)
)
:effect
(and
(NO-ACCESS-P9-F21)
(not (NOT-NO-ACCESS-P9-F21))
)
)
(:action BLOCK-ACCESS-P9-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F20)
)
:effect
(and
(NO-ACCESS-P9-F20)
(not (NOT-NO-ACCESS-P9-F20))
)
)
(:action BLOCK-ACCESS-P9-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F19)
)
:effect
(and
(NO-ACCESS-P9-F19)
(not (NOT-NO-ACCESS-P9-F19))
)
)
(:action BLOCK-ACCESS-P9-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F18)
)
:effect
(and
(NO-ACCESS-P9-F18)
(not (NOT-NO-ACCESS-P9-F18))
)
)
(:action BLOCK-ACCESS-P9-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F17)
)
:effect
(and
(NO-ACCESS-P9-F17)
(not (NOT-NO-ACCESS-P9-F17))
)
)
(:action BLOCK-ACCESS-P9-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F16)
)
:effect
(and
(NO-ACCESS-P9-F16)
(not (NOT-NO-ACCESS-P9-F16))
)
)
(:action BLOCK-ACCESS-P9-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F15)
)
:effect
(and
(NO-ACCESS-P9-F15)
(not (NOT-NO-ACCESS-P9-F15))
)
)
(:action BLOCK-ACCESS-P9-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F14)
)
:effect
(and
(NO-ACCESS-P9-F14)
(not (NOT-NO-ACCESS-P9-F14))
)
)
(:action BLOCK-ACCESS-P9-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F13)
)
:effect
(and
(NO-ACCESS-P9-F13)
(not (NOT-NO-ACCESS-P9-F13))
)
)
(:action BLOCK-ACCESS-P9-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F12)
)
:effect
(and
(NO-ACCESS-P9-F12)
(not (NOT-NO-ACCESS-P9-F12))
)
)
(:action BLOCK-ACCESS-P9-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F11)
)
:effect
(and
(NO-ACCESS-P9-F11)
(not (NOT-NO-ACCESS-P9-F11))
)
)
(:action BLOCK-ACCESS-P9-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F10)
)
:effect
(and
(NO-ACCESS-P9-F10)
(not (NOT-NO-ACCESS-P9-F10))
)
)
(:action BLOCK-ACCESS-P9-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F9)
)
:effect
(and
(NO-ACCESS-P9-F9)
(not (NOT-NO-ACCESS-P9-F9))
)
)
(:action BLOCK-ACCESS-P9-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F8)
)
:effect
(and
(NO-ACCESS-P9-F8)
(not (NOT-NO-ACCESS-P9-F8))
)
)
(:action BLOCK-ACCESS-P9-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F7)
)
:effect
(and
(NO-ACCESS-P9-F7)
(not (NOT-NO-ACCESS-P9-F7))
)
)
(:action BLOCK-ACCESS-P9-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F6)
)
:effect
(and
(NO-ACCESS-P9-F6)
(not (NOT-NO-ACCESS-P9-F6))
)
)
(:action BLOCK-ACCESS-P9-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F5)
)
:effect
(and
(NO-ACCESS-P9-F5)
(not (NOT-NO-ACCESS-P9-F5))
)
)
(:action BLOCK-ACCESS-P9-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F4)
)
:effect
(and
(NO-ACCESS-P9-F4)
(not (NOT-NO-ACCESS-P9-F4))
)
)
(:action BLOCK-ACCESS-P9-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F3)
)
:effect
(and
(NO-ACCESS-P9-F3)
(not (NOT-NO-ACCESS-P9-F3))
)
)
(:action BLOCK-ACCESS-P9-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F2)
)
:effect
(and
(NO-ACCESS-P9-F2)
(not (NOT-NO-ACCESS-P9-F2))
)
)
(:action BLOCK-ACCESS-P9-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F1)
)
:effect
(and
(NO-ACCESS-P9-F1)
(not (NOT-NO-ACCESS-P9-F1))
)
)
(:action BLOCK-ACCESS-P9-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F0)
)
:effect
(and
(NO-ACCESS-P9-F0)
(not (NOT-NO-ACCESS-P9-F0))
)
)
(:action BLOCK-ACCESS-P8-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F21)
)
:effect
(and
(NO-ACCESS-P8-F21)
(not (NOT-NO-ACCESS-P8-F21))
)
)
(:action BLOCK-ACCESS-P8-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F20)
)
:effect
(and
(NO-ACCESS-P8-F20)
(not (NOT-NO-ACCESS-P8-F20))
)
)
(:action BLOCK-ACCESS-P8-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F19)
)
:effect
(and
(NO-ACCESS-P8-F19)
(not (NOT-NO-ACCESS-P8-F19))
)
)
(:action BLOCK-ACCESS-P8-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F18)
)
:effect
(and
(NO-ACCESS-P8-F18)
(not (NOT-NO-ACCESS-P8-F18))
)
)
(:action BLOCK-ACCESS-P8-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F17)
)
:effect
(and
(NO-ACCESS-P8-F17)
(not (NOT-NO-ACCESS-P8-F17))
)
)
(:action BLOCK-ACCESS-P8-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F16)
)
:effect
(and
(NO-ACCESS-P8-F16)
(not (NOT-NO-ACCESS-P8-F16))
)
)
(:action BLOCK-ACCESS-P8-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F15)
)
:effect
(and
(NO-ACCESS-P8-F15)
(not (NOT-NO-ACCESS-P8-F15))
)
)
(:action BLOCK-ACCESS-P8-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F14)
)
:effect
(and
(NO-ACCESS-P8-F14)
(not (NOT-NO-ACCESS-P8-F14))
)
)
(:action BLOCK-ACCESS-P8-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F13)
)
:effect
(and
(NO-ACCESS-P8-F13)
(not (NOT-NO-ACCESS-P8-F13))
)
)
(:action BLOCK-ACCESS-P8-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F12)
)
:effect
(and
(NO-ACCESS-P8-F12)
(not (NOT-NO-ACCESS-P8-F12))
)
)
(:action BLOCK-ACCESS-P8-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F11)
)
:effect
(and
(NO-ACCESS-P8-F11)
(not (NOT-NO-ACCESS-P8-F11))
)
)
(:action BLOCK-ACCESS-P8-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F10)
)
:effect
(and
(NO-ACCESS-P8-F10)
(not (NOT-NO-ACCESS-P8-F10))
)
)
(:action BLOCK-ACCESS-P8-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F9)
)
:effect
(and
(NO-ACCESS-P8-F9)
(not (NOT-NO-ACCESS-P8-F9))
)
)
(:action BLOCK-ACCESS-P8-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F8)
)
:effect
(and
(NO-ACCESS-P8-F8)
(not (NOT-NO-ACCESS-P8-F8))
)
)
(:action BLOCK-ACCESS-P8-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F7)
)
:effect
(and
(NO-ACCESS-P8-F7)
(not (NOT-NO-ACCESS-P8-F7))
)
)
(:action BLOCK-ACCESS-P8-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F6)
)
:effect
(and
(NO-ACCESS-P8-F6)
(not (NOT-NO-ACCESS-P8-F6))
)
)
(:action BLOCK-ACCESS-P8-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F5)
)
:effect
(and
(NO-ACCESS-P8-F5)
(not (NOT-NO-ACCESS-P8-F5))
)
)
(:action BLOCK-ACCESS-P8-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F4)
)
:effect
(and
(NO-ACCESS-P8-F4)
(not (NOT-NO-ACCESS-P8-F4))
)
)
(:action BLOCK-ACCESS-P8-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F3)
)
:effect
(and
(NO-ACCESS-P8-F3)
(not (NOT-NO-ACCESS-P8-F3))
)
)
(:action BLOCK-ACCESS-P8-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F2)
)
:effect
(and
(NO-ACCESS-P8-F2)
(not (NOT-NO-ACCESS-P8-F2))
)
)
(:action BLOCK-ACCESS-P8-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F1)
)
:effect
(and
(NO-ACCESS-P8-F1)
(not (NOT-NO-ACCESS-P8-F1))
)
)
(:action BLOCK-ACCESS-P8-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F0)
)
:effect
(and
(NO-ACCESS-P8-F0)
(not (NOT-NO-ACCESS-P8-F0))
)
)
(:action BLOCK-ACCESS-P7-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F21)
)
:effect
(and
(NO-ACCESS-P7-F21)
(not (NOT-NO-ACCESS-P7-F21))
)
)
(:action BLOCK-ACCESS-P7-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F19)
)
:effect
(and
(NO-ACCESS-P7-F19)
(not (NOT-NO-ACCESS-P7-F19))
)
)
(:action BLOCK-ACCESS-P7-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F18)
)
:effect
(and
(NO-ACCESS-P7-F18)
(not (NOT-NO-ACCESS-P7-F18))
)
)
(:action BLOCK-ACCESS-P7-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F17)
)
:effect
(and
(NO-ACCESS-P7-F17)
(not (NOT-NO-ACCESS-P7-F17))
)
)
(:action BLOCK-ACCESS-P7-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F16)
)
:effect
(and
(NO-ACCESS-P7-F16)
(not (NOT-NO-ACCESS-P7-F16))
)
)
(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F21)
)
:effect
(and
(NO-ACCESS-P6-F21)
(not (NOT-NO-ACCESS-P6-F21))
)
)
(:action BLOCK-ACCESS-P6-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F20)
)
:effect
(and
(NO-ACCESS-P6-F20)
(not (NOT-NO-ACCESS-P6-F20))
)
)
(:action BLOCK-ACCESS-P6-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F19)
)
:effect
(and
(NO-ACCESS-P6-F19)
(not (NOT-NO-ACCESS-P6-F19))
)
)
(:action BLOCK-ACCESS-P6-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F18)
)
:effect
(and
(NO-ACCESS-P6-F18)
(not (NOT-NO-ACCESS-P6-F18))
)
)
(:action BLOCK-ACCESS-P6-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F17)
)
:effect
(and
(NO-ACCESS-P6-F17)
(not (NOT-NO-ACCESS-P6-F17))
)
)
(:action BLOCK-ACCESS-P6-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F16)
)
:effect
(and
(NO-ACCESS-P6-F16)
(not (NOT-NO-ACCESS-P6-F16))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P6-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F0)
)
:effect
(and
(NO-ACCESS-P6-F0)
(not (NOT-NO-ACCESS-P6-F0))
)
)
(:action BLOCK-ACCESS-P5-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F20)
)
:effect
(and
(NO-ACCESS-P5-F20)
(not (NOT-NO-ACCESS-P5-F20))
)
)
(:action BLOCK-ACCESS-P5-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F19)
)
:effect
(and
(NO-ACCESS-P5-F19)
(not (NOT-NO-ACCESS-P5-F19))
)
)
(:action BLOCK-ACCESS-P5-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F18)
)
:effect
(and
(NO-ACCESS-P5-F18)
(not (NOT-NO-ACCESS-P5-F18))
)
)
(:action BLOCK-ACCESS-P5-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F17)
)
:effect
(and
(NO-ACCESS-P5-F17)
(not (NOT-NO-ACCESS-P5-F17))
)
)
(:action BLOCK-ACCESS-P5-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F16)
)
:effect
(and
(NO-ACCESS-P5-F16)
(not (NOT-NO-ACCESS-P5-F16))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F21)
)
:effect
(and
(NO-ACCESS-P4-F21)
(not (NOT-NO-ACCESS-P4-F21))
)
)
(:action BLOCK-ACCESS-P4-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F20)
)
:effect
(and
(NO-ACCESS-P4-F20)
(not (NOT-NO-ACCESS-P4-F20))
)
)
(:action BLOCK-ACCESS-P4-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F19)
)
:effect
(and
(NO-ACCESS-P4-F19)
(not (NOT-NO-ACCESS-P4-F19))
)
)
(:action BLOCK-ACCESS-P4-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F18)
)
:effect
(and
(NO-ACCESS-P4-F18)
(not (NOT-NO-ACCESS-P4-F18))
)
)
(:action BLOCK-ACCESS-P4-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F17)
)
:effect
(and
(NO-ACCESS-P4-F17)
(not (NOT-NO-ACCESS-P4-F17))
)
)
(:action BLOCK-ACCESS-P4-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F16)
)
:effect
(and
(NO-ACCESS-P4-F16)
(not (NOT-NO-ACCESS-P4-F16))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F21)
)
:effect
(and
(NO-ACCESS-P3-F21)
(not (NOT-NO-ACCESS-P3-F21))
)
)
(:action BLOCK-ACCESS-P3-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F20)
)
:effect
(and
(NO-ACCESS-P3-F20)
(not (NOT-NO-ACCESS-P3-F20))
)
)
(:action BLOCK-ACCESS-P3-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F19)
)
:effect
(and
(NO-ACCESS-P3-F19)
(not (NOT-NO-ACCESS-P3-F19))
)
)
(:action BLOCK-ACCESS-P3-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F18)
)
:effect
(and
(NO-ACCESS-P3-F18)
(not (NOT-NO-ACCESS-P3-F18))
)
)
(:action BLOCK-ACCESS-P3-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F17)
)
:effect
(and
(NO-ACCESS-P3-F17)
(not (NOT-NO-ACCESS-P3-F17))
)
)
(:action BLOCK-ACCESS-P3-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F16)
)
:effect
(and
(NO-ACCESS-P3-F16)
(not (NOT-NO-ACCESS-P3-F16))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F10)
)
:effect
(and
(NO-ACCESS-P3-F10)
(not (NOT-NO-ACCESS-P3-F10))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F21)
)
:effect
(and
(NO-ACCESS-P2-F21)
(not (NOT-NO-ACCESS-P2-F21))
)
)
(:action BLOCK-ACCESS-P2-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F20)
)
:effect
(and
(NO-ACCESS-P2-F20)
(not (NOT-NO-ACCESS-P2-F20))
)
)
(:action BLOCK-ACCESS-P2-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F19)
)
:effect
(and
(NO-ACCESS-P2-F19)
(not (NOT-NO-ACCESS-P2-F19))
)
)
(:action BLOCK-ACCESS-P2-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F18)
)
:effect
(and
(NO-ACCESS-P2-F18)
(not (NOT-NO-ACCESS-P2-F18))
)
)
(:action BLOCK-ACCESS-P2-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F17)
)
:effect
(and
(NO-ACCESS-P2-F17)
(not (NOT-NO-ACCESS-P2-F17))
)
)
(:action BLOCK-ACCESS-P2-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F16)
)
:effect
(and
(NO-ACCESS-P2-F16)
(not (NOT-NO-ACCESS-P2-F16))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F21)
)
:effect
(and
(NO-ACCESS-P1-F21)
(not (NOT-NO-ACCESS-P1-F21))
)
)
(:action BLOCK-ACCESS-P1-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F20)
)
:effect
(and
(NO-ACCESS-P1-F20)
(not (NOT-NO-ACCESS-P1-F20))
)
)
(:action BLOCK-ACCESS-P1-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F19)
)
:effect
(and
(NO-ACCESS-P1-F19)
(not (NOT-NO-ACCESS-P1-F19))
)
)
(:action BLOCK-ACCESS-P1-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F18)
)
:effect
(and
(NO-ACCESS-P1-F18)
(not (NOT-NO-ACCESS-P1-F18))
)
)
(:action BLOCK-ACCESS-P1-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F17)
)
:effect
(and
(NO-ACCESS-P1-F17)
(not (NOT-NO-ACCESS-P1-F17))
)
)
(:action BLOCK-ACCESS-P1-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F16)
)
:effect
(and
(NO-ACCESS-P1-F16)
(not (NOT-NO-ACCESS-P1-F16))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F21)
)
:effect
(and
(NO-ACCESS-P0-F21)
(not (NOT-NO-ACCESS-P0-F21))
)
)
(:action BLOCK-ACCESS-P0-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F20)
)
:effect
(and
(NO-ACCESS-P0-F20)
(not (NOT-NO-ACCESS-P0-F20))
)
)
(:action BLOCK-ACCESS-P0-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F19)
)
:effect
(and
(NO-ACCESS-P0-F19)
(not (NOT-NO-ACCESS-P0-F19))
)
)
(:action BLOCK-ACCESS-P0-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F18)
)
:effect
(and
(NO-ACCESS-P0-F18)
(not (NOT-NO-ACCESS-P0-F18))
)
)
(:action BLOCK-ACCESS-P0-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F17)
)
:effect
(and
(NO-ACCESS-P0-F17)
(not (NOT-NO-ACCESS-P0-F17))
)
)
(:action BLOCK-ACCESS-P0-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F16)
)
:effect
(and
(NO-ACCESS-P0-F16)
(not (NOT-NO-ACCESS-P0-F16))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F0
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F0
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F0
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F0
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F0
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F0
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F1
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F1
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F1
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F1
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F1
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F1
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F2
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F2
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F2
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F2
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F2
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F2
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F3
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F3
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F3
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F3
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F3
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F3
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F4
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F4
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F4
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F4
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F4
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F4
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F5
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F5
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F5
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F5
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F5
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F5
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F6
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F6
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F6
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F6
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F6
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F6
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F7
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F7
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F7
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F7
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F7
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F7
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F8
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F8
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F8
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F8
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F8
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F8
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F9
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F9
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F9
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F9
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F9
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F9
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F10
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F10
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F10
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F10
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F10
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F10
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F11
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F11
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F11
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F11
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F11
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F11
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F12
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F12
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F12
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F12
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F12
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F12
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F13
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F13
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F13
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F13
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F13
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F13
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F14
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F14
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F14
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F14
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F14
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F14
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F16-F15
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F15
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F15
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F15
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F15
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F15
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F17-F16
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F16
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F16
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F16
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F16
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F18-F17
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F17
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F17
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F17
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F19-F18
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F18
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F18
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F20-F19
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F19
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F21-F20
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F21))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P6)
(NOT-BOARDED-P10)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P5)))

(:derived (Flag7-)(and (NOT-BOARDED-P6)))

(:derived (Flag7-)(and (NOT-SERVED-P2)))

(:derived (Flag8-)(and (NOT-BOARDED-P10)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P10-F21)))

(:derived (Flag9-)(and (NOT-BOARDED-P9)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P9-F21)))

(:derived (Flag10-)(and (NOT-BOARDED-P8)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P8-F21)))

(:derived (Flag11-)(and (NOT-BOARDED-P7)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P7-F21)))

(:derived (Flag12-)(and (NOT-BOARDED-P6)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P6-F21)))

(:derived (Flag13-)(and (NOT-BOARDED-P4)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P4-F21)))

(:derived (Flag14-)(and (NOT-BOARDED-P3)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P3-F21)))

(:derived (Flag15-)(and (NOT-BOARDED-P2)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P2-F21)))

(:derived (Flag16-)(and (NOT-BOARDED-P1)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P1-F21)))

(:derived (Flag17-)(and (NOT-BOARDED-P0)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P0-F21)))

(:derived (Flag19-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag22-)(and (NOT-BOARDED-P6)))

(:derived (Flag23-)(and (NOT-BOARDED-P10)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P10-F20)))

(:derived (Flag24-)(and (NOT-BOARDED-P9)))

(:derived (Flag24-)(and (NOT-NO-ACCESS-P9-F20)))

(:derived (Flag25-)(and (NOT-BOARDED-P8)))

(:derived (Flag25-)(and (NOT-NO-ACCESS-P8-F20)))

(:derived (Flag26-)(and (NOT-BOARDED-P6)))

(:derived (Flag26-)(and (NOT-NO-ACCESS-P6-F20)))

(:derived (Flag27-)(and (NOT-BOARDED-P5)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P5-F20)))

(:derived (Flag28-)(and (NOT-BOARDED-P4)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P4-F20)))

(:derived (Flag29-)(and (NOT-BOARDED-P3)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P3-F20)))

(:derived (Flag30-)(and (NOT-BOARDED-P2)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P2-F20)))

(:derived (Flag31-)(and (NOT-BOARDED-P1)))

(:derived (Flag31-)(and (NOT-NO-ACCESS-P1-F20)))

(:derived (Flag32-)(and (NOT-BOARDED-P0)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P0-F20)))

(:derived (Flag34-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag37-)(and (NOT-BOARDED-P10)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P10-F19)))

(:derived (Flag38-)(and (NOT-BOARDED-P9)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P9-F19)))

(:derived (Flag39-)(and (NOT-BOARDED-P8)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P8-F19)))

(:derived (Flag40-)(and (NOT-BOARDED-P7)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P7-F19)))

(:derived (Flag41-)(and (NOT-BOARDED-P6)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P6-F19)))

(:derived (Flag42-)(and (NOT-BOARDED-P5)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P5-F19)))

(:derived (Flag43-)(and (NOT-BOARDED-P4)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P4-F19)))

(:derived (Flag44-)(and (NOT-BOARDED-P3)))

(:derived (Flag44-)(and (NOT-NO-ACCESS-P3-F19)))

(:derived (Flag45-)(and (NOT-BOARDED-P2)))

(:derived (Flag45-)(and (NOT-NO-ACCESS-P2-F19)))

(:derived (Flag46-)(and (NOT-BOARDED-P1)))

(:derived (Flag46-)(and (NOT-NO-ACCESS-P1-F19)))

(:derived (Flag47-)(and (NOT-BOARDED-P0)))

(:derived (Flag47-)(and (NOT-NO-ACCESS-P0-F19)))

(:derived (Flag49-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P0)))

(:derived (Flag52-)(and (NOT-BOARDED-P6)))

(:derived (Flag53-)(and (NOT-BOARDED-P10)))

(:derived (Flag53-)(and (NOT-NO-ACCESS-P10-F18)))

(:derived (Flag54-)(and (NOT-BOARDED-P9)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P9-F18)))

(:derived (Flag55-)(and (NOT-BOARDED-P8)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P8-F18)))

(:derived (Flag56-)(and (NOT-BOARDED-P7)))

(:derived (Flag56-)(and (NOT-NO-ACCESS-P7-F18)))

(:derived (Flag57-)(and (NOT-BOARDED-P6)))

(:derived (Flag57-)(and (NOT-NO-ACCESS-P6-F18)))

(:derived (Flag58-)(and (NOT-BOARDED-P5)))

(:derived (Flag58-)(and (NOT-NO-ACCESS-P5-F18)))

(:derived (Flag59-)(and (NOT-BOARDED-P4)))

(:derived (Flag59-)(and (NOT-NO-ACCESS-P4-F18)))

(:derived (Flag60-)(and (NOT-BOARDED-P3)))

(:derived (Flag60-)(and (NOT-NO-ACCESS-P3-F18)))

(:derived (Flag61-)(and (NOT-BOARDED-P2)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P2-F18)))

(:derived (Flag62-)(and (NOT-BOARDED-P1)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P1-F18)))

(:derived (Flag63-)(and (NOT-BOARDED-P0)))

(:derived (Flag63-)(and (NOT-NO-ACCESS-P0-F18)))

(:derived (Flag65-)(and (NOT-BOARDED-P10)))

(:derived (Flag65-)(and (NOT-NO-ACCESS-P10-F17)))

(:derived (Flag66-)(and (NOT-BOARDED-P9)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P9-F17)))

(:derived (Flag67-)(and (NOT-BOARDED-P8)))

(:derived (Flag67-)(and (NOT-NO-ACCESS-P8-F17)))

(:derived (Flag68-)(and (NOT-BOARDED-P7)))

(:derived (Flag68-)(and (NOT-NO-ACCESS-P7-F17)))

(:derived (Flag69-)(and (NOT-BOARDED-P6)))

(:derived (Flag69-)(and (NOT-NO-ACCESS-P6-F17)))

(:derived (Flag70-)(and (NOT-BOARDED-P5)))

(:derived (Flag70-)(and (NOT-NO-ACCESS-P5-F17)))

(:derived (Flag71-)(and (NOT-BOARDED-P4)))

(:derived (Flag71-)(and (NOT-NO-ACCESS-P4-F17)))

(:derived (Flag72-)(and (NOT-BOARDED-P3)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P3-F17)))

(:derived (Flag73-)(and (NOT-BOARDED-P2)))

(:derived (Flag73-)(and (NOT-NO-ACCESS-P2-F17)))

(:derived (Flag74-)(and (NOT-BOARDED-P1)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P1-F17)))

(:derived (Flag75-)(and (NOT-BOARDED-P0)))

(:derived (Flag75-)(and (NOT-NO-ACCESS-P0-F17)))

(:derived (Flag77-)(and (NOT-BOARDED-P10)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P10-F16)))

(:derived (Flag78-)(and (NOT-BOARDED-P9)))

(:derived (Flag78-)(and (NOT-NO-ACCESS-P9-F16)))

(:derived (Flag79-)(and (NOT-BOARDED-P8)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P8-F16)))

(:derived (Flag80-)(and (NOT-BOARDED-P7)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P7-F16)))

(:derived (Flag81-)(and (NOT-BOARDED-P6)))

(:derived (Flag81-)(and (NOT-NO-ACCESS-P6-F16)))

(:derived (Flag82-)(and (NOT-BOARDED-P5)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P5-F16)))

(:derived (Flag83-)(and (NOT-BOARDED-P4)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P4-F16)))

(:derived (Flag84-)(and (NOT-BOARDED-P3)))

(:derived (Flag84-)(and (NOT-NO-ACCESS-P3-F16)))

(:derived (Flag85-)(and (NOT-BOARDED-P2)))

(:derived (Flag85-)(and (NOT-NO-ACCESS-P2-F16)))

(:derived (Flag86-)(and (NOT-BOARDED-P1)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P1-F16)))

(:derived (Flag87-)(and (NOT-BOARDED-P0)))

(:derived (Flag87-)(and (NOT-NO-ACCESS-P0-F16)))

(:derived (Flag93-)(and (NOT-BOARDED-P10)))

(:derived (Flag93-)(and (NOT-NO-ACCESS-P10-F15)))

(:derived (Flag94-)(and (NOT-BOARDED-P9)))

(:derived (Flag94-)(and (NOT-NO-ACCESS-P9-F15)))

(:derived (Flag95-)(and (NOT-BOARDED-P8)))

(:derived (Flag95-)(and (NOT-NO-ACCESS-P8-F15)))

(:derived (Flag96-)(and (NOT-BOARDED-P7)))

(:derived (Flag96-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag97-)(and (NOT-BOARDED-P6)))

(:derived (Flag97-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag98-)(and (NOT-BOARDED-P5)))

(:derived (Flag98-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag99-)(and (NOT-BOARDED-P4)))

(:derived (Flag99-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag100-)(and (NOT-BOARDED-P3)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag101-)(and (NOT-BOARDED-P2)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag102-)(and (NOT-BOARDED-P1)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag103-)(and (NOT-BOARDED-P0)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag105-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag108-)(and (NOT-BOARDED-P6)))

(:derived (Flag109-)(and (NOT-BOARDED-P10)))

(:derived (Flag109-)(and (NOT-NO-ACCESS-P10-F14)))

(:derived (Flag110-)(and (NOT-BOARDED-P9)))

(:derived (Flag110-)(and (NOT-NO-ACCESS-P9-F14)))

(:derived (Flag111-)(and (NOT-BOARDED-P8)))

(:derived (Flag111-)(and (NOT-NO-ACCESS-P8-F14)))

(:derived (Flag112-)(and (NOT-BOARDED-P7)))

(:derived (Flag112-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag113-)(and (NOT-BOARDED-P6)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag114-)(and (NOT-BOARDED-P5)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag115-)(and (NOT-BOARDED-P4)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag116-)(and (NOT-BOARDED-P3)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag117-)(and (NOT-BOARDED-P2)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag118-)(and (NOT-BOARDED-P1)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag119-)(and (NOT-BOARDED-P0)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag121-)(and (NOT-BOARDED-P7)))

(:derived (Flag122-)(and (NOT-BOARDED-P7)))

(:derived (Flag123-)(and (NOT-BOARDED-P10)))

(:derived (Flag123-)(and (NOT-NO-ACCESS-P10-F13)))

(:derived (Flag124-)(and (NOT-BOARDED-P9)))

(:derived (Flag124-)(and (NOT-NO-ACCESS-P9-F13)))

(:derived (Flag125-)(and (NOT-BOARDED-P8)))

(:derived (Flag125-)(and (NOT-NO-ACCESS-P8-F13)))

(:derived (Flag126-)(and (NOT-BOARDED-P7)))

(:derived (Flag126-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag127-)(and (NOT-BOARDED-P6)))

(:derived (Flag127-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag128-)(and (NOT-BOARDED-P5)))

(:derived (Flag128-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag129-)(and (NOT-BOARDED-P4)))

(:derived (Flag129-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag130-)(and (NOT-BOARDED-P3)))

(:derived (Flag130-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag131-)(and (NOT-BOARDED-P2)))

(:derived (Flag131-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag132-)(and (NOT-BOARDED-P1)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag133-)(and (NOT-BOARDED-P0)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag135-)(and (NOT-BOARDED-P10)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P10-F12)))

(:derived (Flag136-)(and (NOT-BOARDED-P9)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P9-F12)))

(:derived (Flag137-)(and (NOT-BOARDED-P8)))

(:derived (Flag137-)(and (NOT-NO-ACCESS-P8-F12)))

(:derived (Flag138-)(and (NOT-BOARDED-P7)))

(:derived (Flag138-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag139-)(and (NOT-BOARDED-P6)))

(:derived (Flag139-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag140-)(and (NOT-BOARDED-P5)))

(:derived (Flag140-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag141-)(and (NOT-BOARDED-P4)))

(:derived (Flag141-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag142-)(and (NOT-BOARDED-P3)))

(:derived (Flag142-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag143-)(and (NOT-BOARDED-P2)))

(:derived (Flag143-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag144-)(and (NOT-BOARDED-P1)))

(:derived (Flag144-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag145-)(and (NOT-BOARDED-P0)))

(:derived (Flag145-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag147-)(and (NOT-BOARDED-P10)))

(:derived (Flag147-)(and (NOT-NO-ACCESS-P10-F11)))

(:derived (Flag148-)(and (NOT-BOARDED-P9)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P9-F11)))

(:derived (Flag149-)(and (NOT-BOARDED-P8)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P8-F11)))

(:derived (Flag150-)(and (NOT-BOARDED-P7)))

(:derived (Flag150-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag151-)(and (NOT-BOARDED-P6)))

(:derived (Flag151-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag152-)(and (NOT-BOARDED-P5)))

(:derived (Flag152-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag153-)(and (NOT-BOARDED-P4)))

(:derived (Flag153-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag154-)(and (NOT-BOARDED-P3)))

(:derived (Flag154-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag155-)(and (NOT-BOARDED-P2)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag156-)(and (NOT-BOARDED-P1)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag157-)(and (NOT-BOARDED-P0)))

(:derived (Flag157-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag164-)(and (NOT-SERVED-P3)))

(:derived (Flag165-)(and (NOT-BOARDED-P10)))

(:derived (Flag165-)(and (NOT-NO-ACCESS-P10-F10)))

(:derived (Flag166-)(and (NOT-BOARDED-P9)))

(:derived (Flag166-)(and (NOT-NO-ACCESS-P9-F10)))

(:derived (Flag167-)(and (NOT-BOARDED-P8)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P8-F10)))

(:derived (Flag168-)(and (NOT-BOARDED-P7)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag169-)(and (NOT-BOARDED-P6)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag170-)(and (NOT-BOARDED-P5)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag171-)(and (NOT-BOARDED-P4)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag172-)(and (NOT-BOARDED-P3)))

(:derived (Flag172-)(and (NOT-NO-ACCESS-P3-F10)))

(:derived (Flag173-)(and (NOT-BOARDED-P2)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag174-)(and (NOT-BOARDED-P1)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag175-)(and (NOT-BOARDED-P0)))

(:derived (Flag175-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag177-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag180-)(and (NOT-BOARDED-P6)))

(:derived (Flag181-)(and (NOT-BOARDED-P10)))

(:derived (Flag181-)(and (NOT-NO-ACCESS-P10-F9)))

(:derived (Flag182-)(and (NOT-BOARDED-P9)))

(:derived (Flag182-)(and (NOT-NO-ACCESS-P9-F9)))

(:derived (Flag183-)(and (NOT-BOARDED-P8)))

(:derived (Flag183-)(and (NOT-NO-ACCESS-P8-F9)))

(:derived (Flag184-)(and (NOT-BOARDED-P7)))

(:derived (Flag184-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag185-)(and (NOT-BOARDED-P6)))

(:derived (Flag185-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag186-)(and (NOT-BOARDED-P5)))

(:derived (Flag186-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag187-)(and (NOT-BOARDED-P4)))

(:derived (Flag187-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag188-)(and (NOT-BOARDED-P3)))

(:derived (Flag188-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag189-)(and (NOT-BOARDED-P2)))

(:derived (Flag189-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag190-)(and (NOT-BOARDED-P1)))

(:derived (Flag190-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag191-)(and (NOT-BOARDED-P0)))

(:derived (Flag191-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag197-)(and (NOT-BOARDED-P10)))

(:derived (Flag197-)(and (NOT-NO-ACCESS-P10-F8)))

(:derived (Flag198-)(and (NOT-BOARDED-P9)))

(:derived (Flag198-)(and (NOT-NO-ACCESS-P9-F8)))

(:derived (Flag199-)(and (NOT-BOARDED-P8)))

(:derived (Flag199-)(and (NOT-NO-ACCESS-P8-F8)))

(:derived (Flag200-)(and (NOT-BOARDED-P7)))

(:derived (Flag200-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag201-)(and (NOT-BOARDED-P6)))

(:derived (Flag201-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag202-)(and (NOT-BOARDED-P5)))

(:derived (Flag202-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag203-)(and (NOT-BOARDED-P4)))

(:derived (Flag203-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag204-)(and (NOT-BOARDED-P3)))

(:derived (Flag204-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag205-)(and (NOT-BOARDED-P2)))

(:derived (Flag205-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag206-)(and (NOT-BOARDED-P1)))

(:derived (Flag206-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag207-)(and (NOT-BOARDED-P0)))

(:derived (Flag207-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag213-)(and (NOT-BOARDED-P10)))

(:derived (Flag213-)(and (NOT-NO-ACCESS-P10-F7)))

(:derived (Flag214-)(and (NOT-BOARDED-P9)))

(:derived (Flag214-)(and (NOT-NO-ACCESS-P9-F7)))

(:derived (Flag215-)(and (NOT-BOARDED-P8)))

(:derived (Flag215-)(and (NOT-NO-ACCESS-P8-F7)))

(:derived (Flag216-)(and (NOT-BOARDED-P7)))

(:derived (Flag216-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag217-)(and (NOT-BOARDED-P6)))

(:derived (Flag217-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag218-)(and (NOT-BOARDED-P5)))

(:derived (Flag218-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag219-)(and (NOT-BOARDED-P4)))

(:derived (Flag219-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag220-)(and (NOT-BOARDED-P3)))

(:derived (Flag220-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag221-)(and (NOT-BOARDED-P2)))

(:derived (Flag221-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag222-)(and (NOT-BOARDED-P1)))

(:derived (Flag222-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag223-)(and (NOT-BOARDED-P0)))

(:derived (Flag223-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag225-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag228-)(and (NOT-BOARDED-P10)))

(:derived (Flag228-)(and (NOT-NO-ACCESS-P10-F6)))

(:derived (Flag229-)(and (NOT-BOARDED-P9)))

(:derived (Flag229-)(and (NOT-NO-ACCESS-P9-F6)))

(:derived (Flag230-)(and (NOT-BOARDED-P8)))

(:derived (Flag230-)(and (NOT-NO-ACCESS-P8-F6)))

(:derived (Flag231-)(and (NOT-BOARDED-P7)))

(:derived (Flag231-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag232-)(and (NOT-BOARDED-P5)))

(:derived (Flag232-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag233-)(and (NOT-BOARDED-P4)))

(:derived (Flag233-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag234-)(and (NOT-BOARDED-P3)))

(:derived (Flag234-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag235-)(and (NOT-BOARDED-P2)))

(:derived (Flag235-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag236-)(and (NOT-BOARDED-P1)))

(:derived (Flag236-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag237-)(and (NOT-BOARDED-P0)))

(:derived (Flag237-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag239-)(and (NOT-BOARDED-P10)))

(:derived (Flag239-)(and (NOT-NO-ACCESS-P10-F5)))

(:derived (Flag240-)(and (NOT-BOARDED-P9)))

(:derived (Flag240-)(and (NOT-NO-ACCESS-P9-F5)))

(:derived (Flag241-)(and (NOT-BOARDED-P8)))

(:derived (Flag241-)(and (NOT-NO-ACCESS-P8-F5)))

(:derived (Flag242-)(and (NOT-BOARDED-P7)))

(:derived (Flag242-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag243-)(and (NOT-BOARDED-P6)))

(:derived (Flag243-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag244-)(and (NOT-BOARDED-P5)))

(:derived (Flag244-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag245-)(and (NOT-BOARDED-P4)))

(:derived (Flag245-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag246-)(and (NOT-BOARDED-P3)))

(:derived (Flag246-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag247-)(and (NOT-BOARDED-P1)))

(:derived (Flag247-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag248-)(and (NOT-BOARDED-P0)))

(:derived (Flag248-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag254-)(and (NOT-BOARDED-P10)))

(:derived (Flag254-)(and (NOT-NO-ACCESS-P10-F4)))

(:derived (Flag255-)(and (NOT-BOARDED-P9)))

(:derived (Flag255-)(and (NOT-NO-ACCESS-P9-F4)))

(:derived (Flag256-)(and (NOT-BOARDED-P8)))

(:derived (Flag256-)(and (NOT-NO-ACCESS-P8-F4)))

(:derived (Flag257-)(and (NOT-BOARDED-P7)))

(:derived (Flag257-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag258-)(and (NOT-BOARDED-P6)))

(:derived (Flag258-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag259-)(and (NOT-BOARDED-P5)))

(:derived (Flag259-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag260-)(and (NOT-BOARDED-P4)))

(:derived (Flag260-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag261-)(and (NOT-BOARDED-P3)))

(:derived (Flag261-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag262-)(and (NOT-BOARDED-P2)))

(:derived (Flag262-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag263-)(and (NOT-BOARDED-P1)))

(:derived (Flag263-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag264-)(and (NOT-BOARDED-P0)))

(:derived (Flag264-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag266-)(and (NOT-BOARDED-P6)))

(:derived (Flag266-)(and (NOT-SERVED-P1)))

(:derived (Flag267-)(and (NOT-BOARDED-P10)))

(:derived (Flag267-)(and (NOT-NO-ACCESS-P10-F3)))

(:derived (Flag268-)(and (NOT-BOARDED-P9)))

(:derived (Flag268-)(and (NOT-NO-ACCESS-P9-F3)))

(:derived (Flag269-)(and (NOT-BOARDED-P8)))

(:derived (Flag269-)(and (NOT-NO-ACCESS-P8-F3)))

(:derived (Flag270-)(and (NOT-BOARDED-P7)))

(:derived (Flag270-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag271-)(and (NOT-BOARDED-P6)))

(:derived (Flag271-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag272-)(and (NOT-BOARDED-P5)))

(:derived (Flag272-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag273-)(and (NOT-BOARDED-P4)))

(:derived (Flag273-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag274-)(and (NOT-BOARDED-P3)))

(:derived (Flag274-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag275-)(and (NOT-BOARDED-P1)))

(:derived (Flag275-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag276-)(and (NOT-BOARDED-P0)))

(:derived (Flag276-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag278-)(and (NOT-BOARDED-P10)))

(:derived (Flag278-)(and (NOT-NO-ACCESS-P10-F2)))

(:derived (Flag279-)(and (NOT-BOARDED-P9)))

(:derived (Flag279-)(and (NOT-NO-ACCESS-P9-F2)))

(:derived (Flag280-)(and (NOT-BOARDED-P8)))

(:derived (Flag280-)(and (NOT-NO-ACCESS-P8-F2)))

(:derived (Flag281-)(and (NOT-BOARDED-P7)))

(:derived (Flag281-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag282-)(and (NOT-BOARDED-P6)))

(:derived (Flag282-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag283-)(and (NOT-BOARDED-P5)))

(:derived (Flag283-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag284-)(and (NOT-BOARDED-P4)))

(:derived (Flag284-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag285-)(and (NOT-BOARDED-P3)))

(:derived (Flag285-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag286-)(and (NOT-BOARDED-P2)))

(:derived (Flag286-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag287-)(and (NOT-BOARDED-P1)))

(:derived (Flag287-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag288-)(and (NOT-BOARDED-P0)))

(:derived (Flag288-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag290-)(and (NOT-BOARDED-P5)))

(:derived (Flag291-)(and (NOT-BOARDED-P5)))

(:derived (Flag292-)(and (NOT-BOARDED-P10)))

(:derived (Flag292-)(and (NOT-NO-ACCESS-P10-F1)))

(:derived (Flag293-)(and (NOT-BOARDED-P9)))

(:derived (Flag293-)(and (NOT-NO-ACCESS-P9-F1)))

(:derived (Flag294-)(and (NOT-BOARDED-P8)))

(:derived (Flag294-)(and (NOT-NO-ACCESS-P8-F1)))

(:derived (Flag295-)(and (NOT-BOARDED-P7)))

(:derived (Flag295-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag296-)(and (NOT-BOARDED-P6)))

(:derived (Flag296-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag297-)(and (NOT-BOARDED-P5)))

(:derived (Flag297-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag298-)(and (NOT-BOARDED-P4)))

(:derived (Flag298-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag299-)(and (NOT-BOARDED-P3)))

(:derived (Flag299-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag300-)(and (NOT-BOARDED-P2)))

(:derived (Flag300-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag301-)(and (NOT-BOARDED-P1)))

(:derived (Flag301-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag302-)(and (NOT-BOARDED-P0)))

(:derived (Flag302-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag308-)(and (NOT-BOARDED-P6)))

(:derived (Flag308-)(and (NOT-SERVED-P8)))

(:derived (Flag308-)(and (NOT-SERVED-P0)))

(:derived (Flag309-)(and (NOT-BOARDED-P10)))

(:derived (Flag309-)(and (NOT-NO-ACCESS-P10-F0)))

(:derived (Flag310-)(and (NOT-BOARDED-P9)))

(:derived (Flag310-)(and (NOT-NO-ACCESS-P9-F0)))

(:derived (Flag311-)(and (NOT-BOARDED-P8)))

(:derived (Flag311-)(and (NOT-NO-ACCESS-P8-F0)))

(:derived (Flag312-)(and (NOT-BOARDED-P7)))

(:derived (Flag312-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag313-)(and (NOT-BOARDED-P6)))

(:derived (Flag313-)(and (NOT-NO-ACCESS-P6-F0)))

(:derived (Flag314-)(and (NOT-BOARDED-P5)))

(:derived (Flag314-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag315-)(and (NOT-BOARDED-P4)))

(:derived (Flag315-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag316-)(and (NOT-BOARDED-P3)))

(:derived (Flag316-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag317-)(and (NOT-BOARDED-P2)))

(:derived (Flag317-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag318-)(and (NOT-BOARDED-P1)))

(:derived (Flag318-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag319-)(and (NOT-BOARDED-P0)))

(:derived (Flag319-)(and (NOT-NO-ACCESS-P0-F0)))

)
