(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(Flag41-)
(Flag36-)
(Flag31-)
(Flag26-)
(Flag21-)
(Flag16-)
(Flag11-)
(Flag6-)
(BOARDED-P1)
(BOARDED-P2)
(BOARDED-P3)
(SERVED-P1)
(SERVED-P3)
(SERVED-P0)
(BOARDED-P0)
(SERVED-P2)
(Flag1-)
(NOT-BOARDED-P0)
(NOT-SERVED-P2)
(NOT-SERVED-P0)
(NOT-SERVED-P3)
(NOT-SERVED-P1)
(NOT-BOARDED-P3)
(NOT-BOARDED-P1)
(NOT-BOARDED-P2)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)))

(:action STOP-F7
:parameters ()
:precondition
(and
(Flag6-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag16-)
)
:effect
(and
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag21-)
)
:effect
(and
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag26-)
)
:effect
(and
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag41-)
)
:effect
(and
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:derived (Flag6-)(and (LIFT-AT-F7)(Flag2-)(Flag3-)(Flag4-)(Flag5-)))

(:derived (Flag11-)(and (LIFT-AT-F6)(Flag7-)(Flag8-)(Flag9-)(Flag10-)))

(:derived (Flag16-)(and (LIFT-AT-F5)(Flag12-)(Flag13-)(Flag14-)(Flag15-)))

(:derived (Flag21-)(and (LIFT-AT-F4)(Flag17-)(Flag18-)(Flag19-)(Flag20-)))

(:derived (Flag26-)(and (LIFT-AT-F3)(Flag22-)(Flag23-)(Flag24-)(Flag25-)))

(:derived (Flag31-)(and (LIFT-AT-F2)(Flag27-)(Flag28-)(Flag29-)(Flag30-)))

(:derived (Flag36-)(and (LIFT-AT-F1)(Flag32-)(Flag33-)(Flag34-)(Flag35-)))

(:derived (Flag41-)(and (LIFT-AT-F0)(Flag37-)(Flag38-)(Flag39-)(Flag40-)))

(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P3)))

(:derived (Flag2-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag3-)(and (NOT-BOARDED-P2)))

(:derived (Flag3-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag4-)(and (NOT-BOARDED-P1)))

(:derived (Flag4-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag5-)(and (NOT-BOARDED-P0)))

(:derived (Flag5-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag7-)(and (NOT-BOARDED-P3)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag8-)(and (NOT-BOARDED-P2)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag9-)(and (NOT-BOARDED-P1)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag10-)(and (NOT-BOARDED-P0)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag12-)(and (NOT-BOARDED-P3)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag13-)(and (NOT-BOARDED-P2)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag14-)(and (NOT-BOARDED-P1)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag15-)(and (NOT-BOARDED-P0)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag17-)(and (NOT-BOARDED-P3)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag18-)(and (NOT-BOARDED-P2)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag19-)(and (NOT-BOARDED-P1)))

(:derived (Flag19-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag20-)(and (NOT-BOARDED-P0)))

(:derived (Flag20-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag22-)(and (NOT-BOARDED-P3)))

(:derived (Flag22-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag23-)(and (NOT-BOARDED-P2)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag24-)(and (NOT-BOARDED-P1)))

(:derived (Flag24-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag25-)(and (NOT-BOARDED-P0)))

(:derived (Flag25-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag27-)(and (NOT-BOARDED-P3)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag28-)(and (NOT-BOARDED-P2)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag29-)(and (NOT-BOARDED-P1)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag30-)(and (NOT-BOARDED-P0)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag32-)(and (NOT-BOARDED-P3)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag33-)(and (NOT-BOARDED-P2)))

(:derived (Flag33-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag34-)(and (NOT-BOARDED-P1)))

(:derived (Flag34-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag35-)(and (NOT-BOARDED-P0)))

(:derived (Flag35-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag37-)(and (NOT-BOARDED-P3)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag38-)(and (NOT-BOARDED-P2)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag39-)(and (NOT-BOARDED-P1)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag40-)(and (NOT-BOARDED-P0)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P0-F0)))

)
