(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag124-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag98-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag82-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag65-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag18-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag3-)
(Flag2-)
(LIFT-AT-F19)
(LIFT-AT-F18)
(LIFT-AT-F17)
(LIFT-AT-F16)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P0-F16)
(NO-ACCESS-P0-F17)
(NO-ACCESS-P0-F18)
(NO-ACCESS-P0-F19)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P1-F16)
(NO-ACCESS-P1-F17)
(NO-ACCESS-P1-F18)
(NO-ACCESS-P1-F19)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P2-F16)
(NO-ACCESS-P2-F17)
(NO-ACCESS-P2-F18)
(NO-ACCESS-P2-F19)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P3-F16)
(NO-ACCESS-P3-F17)
(NO-ACCESS-P3-F18)
(NO-ACCESS-P3-F19)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P4-F16)
(NO-ACCESS-P4-F17)
(NO-ACCESS-P4-F18)
(NO-ACCESS-P4-F19)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P5-F16)
(NO-ACCESS-P5-F17)
(NO-ACCESS-P5-F18)
(NO-ACCESS-P5-F19)
(NO-ACCESS-P6-F0)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F6)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P6-F16)
(NO-ACCESS-P6-F17)
(NO-ACCESS-P6-F18)
(NO-ACCESS-P6-F19)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(NO-ACCESS-P7-F16)
(NO-ACCESS-P7-F17)
(NO-ACCESS-P7-F18)
(NO-ACCESS-P7-F19)
(NO-ACCESS-P8-F0)
(NO-ACCESS-P8-F1)
(NO-ACCESS-P8-F2)
(NO-ACCESS-P8-F3)
(NO-ACCESS-P8-F4)
(NO-ACCESS-P8-F5)
(NO-ACCESS-P8-F6)
(NO-ACCESS-P8-F7)
(NO-ACCESS-P8-F8)
(NO-ACCESS-P8-F9)
(NO-ACCESS-P8-F10)
(NO-ACCESS-P8-F11)
(NO-ACCESS-P8-F12)
(NO-ACCESS-P8-F13)
(NO-ACCESS-P8-F14)
(NO-ACCESS-P8-F15)
(NO-ACCESS-P8-F16)
(NO-ACCESS-P8-F17)
(NO-ACCESS-P8-F18)
(NO-ACCESS-P8-F19)
(NO-ACCESS-P9-F0)
(NO-ACCESS-P9-F1)
(NO-ACCESS-P9-F2)
(NO-ACCESS-P9-F3)
(NO-ACCESS-P9-F4)
(NO-ACCESS-P9-F5)
(NO-ACCESS-P9-F6)
(NO-ACCESS-P9-F7)
(NO-ACCESS-P9-F8)
(NO-ACCESS-P9-F9)
(NO-ACCESS-P9-F10)
(NO-ACCESS-P9-F11)
(NO-ACCESS-P9-F12)
(NO-ACCESS-P9-F13)
(NO-ACCESS-P9-F14)
(NO-ACCESS-P9-F15)
(NO-ACCESS-P9-F16)
(NO-ACCESS-P9-F17)
(NO-ACCESS-P9-F18)
(NO-ACCESS-P9-F19)
(Flag275-)
(Flag273-)
(Flag260-)
(Flag225-)
(Flag223-)
(Flag211-)
(Flag209-)
(Flag195-)
(Flag193-)
(Flag179-)
(Flag177-)
(Flag142-)
(Flag140-)
(Flag126-)
(Flag125-)
(Flag112-)
(Flag84-)
(Flag83-)
(Flag68-)
(Flag66-)
(Flag51-)
(Flag49-)
(Flag35-)
(Flag33-)
(Flag20-)
(Flag19-)
(Flag5-)
(Flag4-)
(SERVED-P5)
(SERVED-P7)
(BOARDED-P5)
(SERVED-P2)
(Flag286-)
(Flag271-)
(Flag246-)
(Flag236-)
(Flag221-)
(Flag207-)
(Flag191-)
(Flag175-)
(Flag164-)
(Flag153-)
(Flag141-)
(Flag139-)
(Flag138-)
(Flag123-)
(Flag99-)
(Flag97-)
(Flag96-)
(Flag70-)
(Flag69-)
(Flag67-)
(Flag64-)
(Flag63-)
(Flag47-)
(Flag31-)
(Flag17-)
(BOARDED-P4)
(BOARDED-P1)
(BOARDED-P8)
(BOARDED-P3)
(BOARDED-P9)
(BOARDED-P2)
(SERVED-P3)
(SERVED-P8)
(SERVED-P9)
(BOARDED-P6)
(SERVED-P4)
(BOARDED-P0)
(SERVED-P6)
(SERVED-P0)
(Flag274-)
(Flag272-)
(Flag210-)
(Flag208-)
(Flag194-)
(Flag192-)
(Flag178-)
(Flag176-)
(Flag81-)
(Flag50-)
(Flag48-)
(Flag34-)
(Flag32-)
(BOARDED-P7)
(SERVED-P1)
(Flag224-)
(Flag222-)
(Flag1-)
(NOT-BOARDED-P7)
(NOT-SERVED-P1)
(NOT-SERVED-P0)
(NOT-BOARDED-P0)
(NOT-SERVED-P6)
(NOT-BOARDED-P6)
(NOT-SERVED-P4)
(NOT-SERVED-P8)
(NOT-SERVED-P9)
(NOT-SERVED-P3)
(NOT-BOARDED-P2)
(NOT-BOARDED-P9)
(NOT-BOARDED-P3)
(NOT-BOARDED-P8)
(NOT-BOARDED-P1)
(NOT-BOARDED-P4)
(NOT-BOARDED-P5)
(NOT-SERVED-P2)
(NOT-SERVED-P5)
(NOT-SERVED-P7)
(NOT-NO-ACCESS-P9-F19)
(NOT-NO-ACCESS-P9-F18)
(NOT-NO-ACCESS-P9-F17)
(NOT-NO-ACCESS-P9-F16)
(NOT-NO-ACCESS-P9-F15)
(NOT-NO-ACCESS-P9-F14)
(NOT-NO-ACCESS-P9-F13)
(NOT-NO-ACCESS-P9-F12)
(NOT-NO-ACCESS-P9-F11)
(NOT-NO-ACCESS-P9-F10)
(NOT-NO-ACCESS-P9-F9)
(NOT-NO-ACCESS-P9-F8)
(NOT-NO-ACCESS-P9-F7)
(NOT-NO-ACCESS-P9-F6)
(NOT-NO-ACCESS-P9-F5)
(NOT-NO-ACCESS-P9-F4)
(NOT-NO-ACCESS-P9-F3)
(NOT-NO-ACCESS-P9-F2)
(NOT-NO-ACCESS-P9-F1)
(NOT-NO-ACCESS-P9-F0)
(NOT-NO-ACCESS-P8-F19)
(NOT-NO-ACCESS-P8-F18)
(NOT-NO-ACCESS-P8-F17)
(NOT-NO-ACCESS-P8-F16)
(NOT-NO-ACCESS-P8-F15)
(NOT-NO-ACCESS-P8-F14)
(NOT-NO-ACCESS-P8-F13)
(NOT-NO-ACCESS-P8-F12)
(NOT-NO-ACCESS-P8-F11)
(NOT-NO-ACCESS-P8-F10)
(NOT-NO-ACCESS-P8-F9)
(NOT-NO-ACCESS-P8-F8)
(NOT-NO-ACCESS-P8-F7)
(NOT-NO-ACCESS-P8-F6)
(NOT-NO-ACCESS-P8-F5)
(NOT-NO-ACCESS-P8-F4)
(NOT-NO-ACCESS-P8-F3)
(NOT-NO-ACCESS-P8-F2)
(NOT-NO-ACCESS-P8-F1)
(NOT-NO-ACCESS-P8-F0)
(NOT-NO-ACCESS-P7-F19)
(NOT-NO-ACCESS-P7-F18)
(NOT-NO-ACCESS-P7-F17)
(NOT-NO-ACCESS-P7-F16)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F19)
(NOT-NO-ACCESS-P6-F18)
(NOT-NO-ACCESS-P6-F17)
(NOT-NO-ACCESS-P6-F16)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F6)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P6-F0)
(NOT-NO-ACCESS-P5-F19)
(NOT-NO-ACCESS-P5-F18)
(NOT-NO-ACCESS-P5-F17)
(NOT-NO-ACCESS-P5-F16)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F19)
(NOT-NO-ACCESS-P4-F18)
(NOT-NO-ACCESS-P4-F17)
(NOT-NO-ACCESS-P4-F16)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F19)
(NOT-NO-ACCESS-P3-F18)
(NOT-NO-ACCESS-P3-F17)
(NOT-NO-ACCESS-P3-F16)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F19)
(NOT-NO-ACCESS-P2-F18)
(NOT-NO-ACCESS-P2-F17)
(NOT-NO-ACCESS-P2-F16)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F19)
(NOT-NO-ACCESS-P1-F18)
(NOT-NO-ACCESS-P1-F17)
(NOT-NO-ACCESS-P1-F16)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F19)
(NOT-NO-ACCESS-P0-F18)
(NOT-NO-ACCESS-P0-F17)
(NOT-NO-ACCESS-P0-F16)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:derived (Flag223-)(and (Flag222-)))

(:derived (Flag225-)(and (Flag224-)))

(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)(SERVED-P8)(SERVED-P9)))

(:derived (Flag33-)(and (Flag32-)))

(:derived (Flag35-)(and (Flag34-)))

(:derived (Flag49-)(and (Flag48-)))

(:derived (Flag51-)(and (Flag50-)))

(:derived (Flag177-)(and (Flag176-)))

(:derived (Flag179-)(and (Flag178-)))

(:derived (Flag193-)(and (Flag192-)))

(:derived (Flag195-)(and (Flag194-)))

(:derived (Flag209-)(and (Flag208-)))

(:derived (Flag211-)(and (Flag210-)))

(:derived (Flag222-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P1)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag224-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(SERVED-P1)(NOT-BOARDED-P0)))

(:derived (Flag273-)(and (Flag272-)))

(:derived (Flag275-)(and (Flag274-)))

(:action STOP-F15
:parameters ()
:precondition
(and
(Flag81-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:derived (Flag6-)(and (BOARDED-P9)))

(:derived (Flag6-)(and (BOARDED-P6)))

(:derived (Flag6-)(and (BOARDED-P3)))

(:derived (Flag6-)(and (BOARDED-P2)))

(:derived (Flag6-)(and (BOARDED-P1)))

(:derived (Flag32-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag34-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag36-)(and (BOARDED-P9)))

(:derived (Flag36-)(and (BOARDED-P3)))

(:derived (Flag36-)(and (BOARDED-P2)))

(:derived (Flag36-)(and (BOARDED-P1)))

(:derived (Flag48-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P6)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag50-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(SERVED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag52-)(and (BOARDED-P9)))

(:derived (Flag52-)(and (BOARDED-P6)))

(:derived (Flag52-)(and (BOARDED-P3)))

(:derived (Flag52-)(and (BOARDED-P2)))

(:derived (Flag52-)(and (BOARDED-P1)))

(:derived (Flag66-)(and (Flag64-)))

(:derived (Flag68-)(and (Flag67-)))

(:derived (Flag70-)(and (Flag69-)))

(:derived (Flag70-)(and (BOARDED-P9)))

(:derived (Flag70-)(and (BOARDED-P6)))

(:derived (Flag70-)(and (BOARDED-P3)))

(:derived (Flag70-)(and (BOARDED-P2)))

(:derived (Flag81-)(and (LIFT-AT-F15)(Flag66-)(Flag68-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)))

(:derived (Flag85-)(and (BOARDED-P6)))

(:derived (Flag85-)(and (BOARDED-P3)))

(:derived (Flag85-)(and (BOARDED-P2)))

(:derived (Flag85-)(and (BOARDED-P1)))

(:derived (Flag98-)(and (Flag97-)))

(:derived (Flag100-)(and (Flag99-)))

(:derived (Flag101-)(and (BOARDED-P9)))

(:derived (Flag101-)(and (BOARDED-P6)))

(:derived (Flag101-)(and (BOARDED-P3)))

(:derived (Flag101-)(and (BOARDED-P1)))

(:derived (Flag127-)(and (BOARDED-P9)))

(:derived (Flag127-)(and (BOARDED-P6)))

(:derived (Flag127-)(and (BOARDED-P2)))

(:derived (Flag127-)(and (BOARDED-P1)))

(:derived (Flag140-)(and (Flag139-)))

(:derived (Flag142-)(and (Flag141-)))

(:derived (Flag143-)(and (BOARDED-P9)))

(:derived (Flag143-)(and (BOARDED-P6)))

(:derived (Flag143-)(and (BOARDED-P3)))

(:derived (Flag143-)(and (BOARDED-P2)))

(:derived (Flag143-)(and (BOARDED-P1)))

(:derived (Flag176-)(and (SERVED-P9)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag178-)(and (NOT-BOARDED-P9)(SERVED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag180-)(and (BOARDED-P9)))

(:derived (Flag180-)(and (BOARDED-P6)))

(:derived (Flag180-)(and (BOARDED-P3)))

(:derived (Flag180-)(and (BOARDED-P2)))

(:derived (Flag180-)(and (BOARDED-P1)))

(:derived (Flag192-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag194-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag196-)(and (BOARDED-P9)))

(:derived (Flag196-)(and (BOARDED-P6)))

(:derived (Flag196-)(and (BOARDED-P3)))

(:derived (Flag196-)(and (BOARDED-P2)))

(:derived (Flag196-)(and (BOARDED-P1)))

(:derived (Flag208-)(and (NOT-BOARDED-P9)(SERVED-P8)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag210-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag226-)(and (BOARDED-P9)))

(:derived (Flag226-)(and (BOARDED-P6)))

(:derived (Flag226-)(and (BOARDED-P3)))

(:derived (Flag226-)(and (BOARDED-P2)))

(:derived (Flag226-)(and (BOARDED-P1)))

(:derived (Flag272-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P4)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag274-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(SERVED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:action STOP-F19
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
)
)
(:action STOP-F18
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action STOP-F17
:parameters ()
:precondition
(and
(Flag47-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
)
)
(:action STOP-F16
:parameters ()
:precondition
(and
(Flag63-)
)
:effect
(and
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action STOP-F14
:parameters ()
:precondition
(and
(Flag96-)
)
:effect
(and
(when
(and
(BOARDED-P8)
)
(and
(NOT-BOARDED-P8)
(SERVED-P8)
(not (BOARDED-P8))
(not (NOT-SERVED-P8))
)
)
(when
(and
(BOARDED-P9)
)
(and
(NOT-BOARDED-P9)
(SERVED-P9)
(not (BOARDED-P9))
(not (NOT-SERVED-P9))
)
)
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag123-)
)
:effect
(and
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag138-)
)
:effect
(and
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag153-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag164-)
)
:effect
(and
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag175-)
)
:effect
(and
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag191-)
)
:effect
(and
(when
(and
(NOT-SERVED-P9)
)
(and
(BOARDED-P9)
(not (NOT-BOARDED-P9))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag207-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag221-)
)
:effect
(and
(when
(and
(NOT-SERVED-P8)
)
(and
(BOARDED-P8)
(not (NOT-BOARDED-P8))
)
)
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag236-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag246-)
)
:effect
(and
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag271-)
)
:effect
(and
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag286-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
)
)
(:derived (Flag6-)(and (BOARDED-P5)))

(:derived (Flag17-)(and (LIFT-AT-F19)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(Flag16-)))

(:derived (Flag31-)(and (LIFT-AT-F18)(Flag19-)(Flag20-)(Flag6-)(Flag21-)(Flag22-)(Flag23-)(Flag24-)(Flag25-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)))

(:derived (Flag36-)(and (BOARDED-P5)))

(:derived (Flag47-)(and (LIFT-AT-F17)(Flag33-)(Flag35-)(Flag36-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)))

(:derived (Flag52-)(and (BOARDED-P5)))

(:derived (Flag63-)(and (LIFT-AT-F16)(Flag49-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)))

(:derived (Flag64-)(and (NOT-BOARDED-P7)(SERVED-P7)(NOT-BOARDED-P2)))

(:derived (Flag67-)(and (SERVED-P7)(NOT-BOARDED-P7)(NOT-BOARDED-P2)))

(:derived (Flag69-)(and (NOT-BOARDED-P7)(SERVED-P7)))

(:derived (Flag70-)(and (BOARDED-P5)))

(:derived (Flag85-)(and (BOARDED-P5)))

(:derived (Flag96-)(and (LIFT-AT-F14)(Flag83-)(Flag84-)(Flag85-)(Flag86-)(Flag87-)(Flag88-)(Flag89-)(Flag90-)(Flag91-)(Flag92-)(Flag93-)(Flag94-)(Flag95-)))

(:derived (Flag97-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag99-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag101-)(and (BOARDED-P5)))

(:derived (Flag123-)(and (LIFT-AT-F12)(Flag4-)(Flag5-)(Flag6-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)(Flag122-)))

(:derived (Flag127-)(and (BOARDED-P5)))

(:derived (Flag138-)(and (LIFT-AT-F11)(Flag125-)(Flag126-)(Flag127-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)))

(:derived (Flag139-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P2)(SERVED-P2)))

(:derived (Flag141-)(and (NOT-BOARDED-P7)(SERVED-P2)(NOT-BOARDED-P2)))

(:derived (Flag143-)(and (BOARDED-P5)))

(:derived (Flag153-)(and (LIFT-AT-F10)(Flag140-)(Flag142-)(Flag143-)(Flag144-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)(NOT-BOARDED-P3)(Flag150-)(Flag151-)(Flag152-)))

(:derived (Flag164-)(and (LIFT-AT-F9)(Flag4-)(Flag5-)(Flag6-)(Flag154-)(Flag155-)(Flag156-)(Flag157-)(Flag158-)(Flag159-)(Flag160-)(Flag161-)(Flag162-)(Flag163-)))

(:derived (Flag175-)(and (LIFT-AT-F8)(Flag4-)(Flag5-)(Flag6-)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)))

(:derived (Flag180-)(and (BOARDED-P5)))

(:derived (Flag191-)(and (LIFT-AT-F7)(Flag177-)(Flag179-)(Flag180-)(Flag181-)(Flag182-)(Flag183-)(Flag184-)(Flag185-)(Flag186-)(Flag187-)(Flag188-)(Flag189-)(Flag190-)))

(:derived (Flag196-)(and (BOARDED-P5)))

(:derived (Flag207-)(and (LIFT-AT-F6)(Flag193-)(Flag195-)(Flag196-)(Flag197-)(Flag198-)(Flag199-)(Flag200-)(Flag201-)(Flag202-)(Flag203-)(Flag204-)(Flag205-)(Flag206-)))

(:derived (Flag221-)(and (LIFT-AT-F5)(Flag209-)(Flag211-)(Flag6-)(Flag212-)(Flag213-)(Flag214-)(Flag215-)(Flag216-)(Flag217-)(NOT-BOARDED-P3)(Flag218-)(Flag219-)(Flag220-)))

(:derived (Flag226-)(and (BOARDED-P5)))

(:derived (Flag236-)(and (LIFT-AT-F4)(Flag223-)(Flag225-)(Flag226-)(Flag227-)(Flag228-)(Flag229-)(Flag230-)(Flag231-)(NOT-BOARDED-P4)(Flag232-)(Flag233-)(Flag234-)(Flag235-)))

(:derived (Flag246-)(and (LIFT-AT-F3)(Flag4-)(Flag5-)(Flag6-)(Flag237-)(Flag238-)(Flag239-)(Flag240-)(Flag241-)(Flag242-)(NOT-BOARDED-P3)(Flag243-)(Flag244-)(Flag245-)))

(:derived (Flag271-)(and (LIFT-AT-F1)(Flag4-)(Flag5-)(Flag6-)(Flag261-)(Flag262-)(Flag263-)(Flag264-)(Flag265-)(Flag266-)(Flag267-)(Flag268-)(Flag269-)(Flag270-)))

(:derived (Flag286-)(and (LIFT-AT-F0)(Flag273-)(Flag275-)(Flag6-)(Flag276-)(Flag277-)(Flag278-)(Flag279-)(Flag280-)(Flag281-)(Flag282-)(Flag283-)(Flag284-)(Flag285-)))

(:action STOP-F13
:parameters ()
:precondition
(and
(Flag112-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag260-)
)
:effect
(and
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F16
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F17
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F18
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F19
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F16
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F17
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F18
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F19
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F16
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F17
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F18
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F19
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F16
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F17
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F18
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F19
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F16
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F17
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F18
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F19
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F16
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F17
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F18
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F19
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F16
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F17
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F18
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F19
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F16
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F17
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F18
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F19
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F16
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F17
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F18
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F19
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F16
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F17
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F18
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F19
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F16
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F17
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F18
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F19
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F16
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F17
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F18
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F19
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F16
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F17
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F18
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F19
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F16
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F17
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F18
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F19
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F14))
)
)
(:action UP-F15-F16
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F17
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F18
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F19
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F15))
)
)
(:action UP-F16-F17
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F18
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F19
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F16))
)
)
(:action UP-F17-F18
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F19
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F17))
)
)
(:action UP-F18-F19
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F18))
)
)
(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag2-)))

(:derived (Flag19-)(and (Flag2-)))

(:derived (Flag19-)(and (Flag18-)))

(:derived (Flag20-)(and (Flag18-)))

(:derived (Flag20-)(and (Flag2-)))

(:derived (Flag33-)(and (Flag2-)))

(:derived (Flag35-)(and (Flag2-)))

(:derived (Flag49-)(and (Flag2-)))

(:derived (Flag51-)(and (Flag2-)))

(:derived (Flag66-)(and (Flag65-)))

(:derived (Flag68-)(and (Flag65-)))

(:derived (Flag83-)(and (Flag2-)))

(:derived (Flag83-)(and (Flag82-)))

(:derived (Flag84-)(and (Flag82-)))

(:derived (Flag84-)(and (Flag2-)))

(:derived (Flag112-)(and (LIFT-AT-F13)(Flag98-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)))

(:derived (Flag125-)(and (Flag2-)))

(:derived (Flag125-)(and (Flag124-)))

(:derived (Flag126-)(and (Flag124-)))

(:derived (Flag126-)(and (Flag2-)))

(:derived (Flag140-)(and (Flag3-)))

(:derived (Flag142-)(and (Flag3-)))

(:derived (Flag177-)(and (Flag2-)))

(:derived (Flag179-)(and (Flag2-)))

(:derived (Flag193-)(and (Flag2-)))

(:derived (Flag195-)(and (Flag2-)))

(:derived (Flag209-)(and (Flag2-)))

(:derived (Flag211-)(and (Flag2-)))

(:derived (Flag223-)(and (Flag2-)))

(:derived (Flag225-)(and (Flag2-)))

(:derived (Flag248-)(and (Flag247-)))

(:derived (Flag249-)(and (Flag247-)))

(:derived (Flag260-)(and (LIFT-AT-F2)(Flag248-)(Flag249-)(Flag250-)(Flag251-)(Flag252-)(Flag253-)(Flag254-)(Flag255-)(Flag256-)(Flag257-)(Flag258-)(Flag259-)))

(:derived (Flag273-)(and (Flag2-)))

(:derived (Flag275-)(and (Flag2-)))

(:action BLOCK-ACCESS-P9-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F19)
)
:effect
(and
(NO-ACCESS-P9-F19)
(not (NOT-NO-ACCESS-P9-F19))
)
)
(:action BLOCK-ACCESS-P9-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F18)
)
:effect
(and
(NO-ACCESS-P9-F18)
(not (NOT-NO-ACCESS-P9-F18))
)
)
(:action BLOCK-ACCESS-P9-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F17)
)
:effect
(and
(NO-ACCESS-P9-F17)
(not (NOT-NO-ACCESS-P9-F17))
)
)
(:action BLOCK-ACCESS-P9-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F16)
)
:effect
(and
(NO-ACCESS-P9-F16)
(not (NOT-NO-ACCESS-P9-F16))
)
)
(:action BLOCK-ACCESS-P9-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F15)
)
:effect
(and
(NO-ACCESS-P9-F15)
(not (NOT-NO-ACCESS-P9-F15))
)
)
(:action BLOCK-ACCESS-P9-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F14)
)
:effect
(and
(NO-ACCESS-P9-F14)
(not (NOT-NO-ACCESS-P9-F14))
)
)
(:action BLOCK-ACCESS-P9-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F13)
)
:effect
(and
(NO-ACCESS-P9-F13)
(not (NOT-NO-ACCESS-P9-F13))
)
)
(:action BLOCK-ACCESS-P9-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F12)
)
:effect
(and
(NO-ACCESS-P9-F12)
(not (NOT-NO-ACCESS-P9-F12))
)
)
(:action BLOCK-ACCESS-P9-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F11)
)
:effect
(and
(NO-ACCESS-P9-F11)
(not (NOT-NO-ACCESS-P9-F11))
)
)
(:action BLOCK-ACCESS-P9-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F10)
)
:effect
(and
(NO-ACCESS-P9-F10)
(not (NOT-NO-ACCESS-P9-F10))
)
)
(:action BLOCK-ACCESS-P9-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F9)
)
:effect
(and
(NO-ACCESS-P9-F9)
(not (NOT-NO-ACCESS-P9-F9))
)
)
(:action BLOCK-ACCESS-P9-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F8)
)
:effect
(and
(NO-ACCESS-P9-F8)
(not (NOT-NO-ACCESS-P9-F8))
)
)
(:action BLOCK-ACCESS-P9-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F7)
)
:effect
(and
(NO-ACCESS-P9-F7)
(not (NOT-NO-ACCESS-P9-F7))
)
)
(:action BLOCK-ACCESS-P9-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F6)
)
:effect
(and
(NO-ACCESS-P9-F6)
(not (NOT-NO-ACCESS-P9-F6))
)
)
(:action BLOCK-ACCESS-P9-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F5)
)
:effect
(and
(NO-ACCESS-P9-F5)
(not (NOT-NO-ACCESS-P9-F5))
)
)
(:action BLOCK-ACCESS-P9-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F4)
)
:effect
(and
(NO-ACCESS-P9-F4)
(not (NOT-NO-ACCESS-P9-F4))
)
)
(:action BLOCK-ACCESS-P9-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F3)
)
:effect
(and
(NO-ACCESS-P9-F3)
(not (NOT-NO-ACCESS-P9-F3))
)
)
(:action BLOCK-ACCESS-P9-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F2)
)
:effect
(and
(NO-ACCESS-P9-F2)
(not (NOT-NO-ACCESS-P9-F2))
)
)
(:action BLOCK-ACCESS-P9-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F1)
)
:effect
(and
(NO-ACCESS-P9-F1)
(not (NOT-NO-ACCESS-P9-F1))
)
)
(:action BLOCK-ACCESS-P9-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F0)
)
:effect
(and
(NO-ACCESS-P9-F0)
(not (NOT-NO-ACCESS-P9-F0))
)
)
(:action BLOCK-ACCESS-P8-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F19)
)
:effect
(and
(NO-ACCESS-P8-F19)
(not (NOT-NO-ACCESS-P8-F19))
)
)
(:action BLOCK-ACCESS-P8-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F18)
)
:effect
(and
(NO-ACCESS-P8-F18)
(not (NOT-NO-ACCESS-P8-F18))
)
)
(:action BLOCK-ACCESS-P8-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F17)
)
:effect
(and
(NO-ACCESS-P8-F17)
(not (NOT-NO-ACCESS-P8-F17))
)
)
(:action BLOCK-ACCESS-P8-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F16)
)
:effect
(and
(NO-ACCESS-P8-F16)
(not (NOT-NO-ACCESS-P8-F16))
)
)
(:action BLOCK-ACCESS-P8-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F15)
)
:effect
(and
(NO-ACCESS-P8-F15)
(not (NOT-NO-ACCESS-P8-F15))
)
)
(:action BLOCK-ACCESS-P8-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F14)
)
:effect
(and
(NO-ACCESS-P8-F14)
(not (NOT-NO-ACCESS-P8-F14))
)
)
(:action BLOCK-ACCESS-P8-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F13)
)
:effect
(and
(NO-ACCESS-P8-F13)
(not (NOT-NO-ACCESS-P8-F13))
)
)
(:action BLOCK-ACCESS-P8-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F12)
)
:effect
(and
(NO-ACCESS-P8-F12)
(not (NOT-NO-ACCESS-P8-F12))
)
)
(:action BLOCK-ACCESS-P8-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F11)
)
:effect
(and
(NO-ACCESS-P8-F11)
(not (NOT-NO-ACCESS-P8-F11))
)
)
(:action BLOCK-ACCESS-P8-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F10)
)
:effect
(and
(NO-ACCESS-P8-F10)
(not (NOT-NO-ACCESS-P8-F10))
)
)
(:action BLOCK-ACCESS-P8-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F9)
)
:effect
(and
(NO-ACCESS-P8-F9)
(not (NOT-NO-ACCESS-P8-F9))
)
)
(:action BLOCK-ACCESS-P8-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F8)
)
:effect
(and
(NO-ACCESS-P8-F8)
(not (NOT-NO-ACCESS-P8-F8))
)
)
(:action BLOCK-ACCESS-P8-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F7)
)
:effect
(and
(NO-ACCESS-P8-F7)
(not (NOT-NO-ACCESS-P8-F7))
)
)
(:action BLOCK-ACCESS-P8-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F6)
)
:effect
(and
(NO-ACCESS-P8-F6)
(not (NOT-NO-ACCESS-P8-F6))
)
)
(:action BLOCK-ACCESS-P8-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F5)
)
:effect
(and
(NO-ACCESS-P8-F5)
(not (NOT-NO-ACCESS-P8-F5))
)
)
(:action BLOCK-ACCESS-P8-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F4)
)
:effect
(and
(NO-ACCESS-P8-F4)
(not (NOT-NO-ACCESS-P8-F4))
)
)
(:action BLOCK-ACCESS-P8-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F3)
)
:effect
(and
(NO-ACCESS-P8-F3)
(not (NOT-NO-ACCESS-P8-F3))
)
)
(:action BLOCK-ACCESS-P8-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F2)
)
:effect
(and
(NO-ACCESS-P8-F2)
(not (NOT-NO-ACCESS-P8-F2))
)
)
(:action BLOCK-ACCESS-P8-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F1)
)
:effect
(and
(NO-ACCESS-P8-F1)
(not (NOT-NO-ACCESS-P8-F1))
)
)
(:action BLOCK-ACCESS-P8-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F0)
)
:effect
(and
(NO-ACCESS-P8-F0)
(not (NOT-NO-ACCESS-P8-F0))
)
)
(:action BLOCK-ACCESS-P7-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F19)
)
:effect
(and
(NO-ACCESS-P7-F19)
(not (NOT-NO-ACCESS-P7-F19))
)
)
(:action BLOCK-ACCESS-P7-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F18)
)
:effect
(and
(NO-ACCESS-P7-F18)
(not (NOT-NO-ACCESS-P7-F18))
)
)
(:action BLOCK-ACCESS-P7-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F17)
)
:effect
(and
(NO-ACCESS-P7-F17)
(not (NOT-NO-ACCESS-P7-F17))
)
)
(:action BLOCK-ACCESS-P7-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F16)
)
:effect
(and
(NO-ACCESS-P7-F16)
(not (NOT-NO-ACCESS-P7-F16))
)
)
(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F19)
)
:effect
(and
(NO-ACCESS-P6-F19)
(not (NOT-NO-ACCESS-P6-F19))
)
)
(:action BLOCK-ACCESS-P6-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F18)
)
:effect
(and
(NO-ACCESS-P6-F18)
(not (NOT-NO-ACCESS-P6-F18))
)
)
(:action BLOCK-ACCESS-P6-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F17)
)
:effect
(and
(NO-ACCESS-P6-F17)
(not (NOT-NO-ACCESS-P6-F17))
)
)
(:action BLOCK-ACCESS-P6-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F16)
)
:effect
(and
(NO-ACCESS-P6-F16)
(not (NOT-NO-ACCESS-P6-F16))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F6)
)
:effect
(and
(NO-ACCESS-P6-F6)
(not (NOT-NO-ACCESS-P6-F6))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P6-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F0)
)
:effect
(and
(NO-ACCESS-P6-F0)
(not (NOT-NO-ACCESS-P6-F0))
)
)
(:action BLOCK-ACCESS-P5-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F19)
)
:effect
(and
(NO-ACCESS-P5-F19)
(not (NOT-NO-ACCESS-P5-F19))
)
)
(:action BLOCK-ACCESS-P5-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F18)
)
:effect
(and
(NO-ACCESS-P5-F18)
(not (NOT-NO-ACCESS-P5-F18))
)
)
(:action BLOCK-ACCESS-P5-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F17)
)
:effect
(and
(NO-ACCESS-P5-F17)
(not (NOT-NO-ACCESS-P5-F17))
)
)
(:action BLOCK-ACCESS-P5-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F16)
)
:effect
(and
(NO-ACCESS-P5-F16)
(not (NOT-NO-ACCESS-P5-F16))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F19)
)
:effect
(and
(NO-ACCESS-P4-F19)
(not (NOT-NO-ACCESS-P4-F19))
)
)
(:action BLOCK-ACCESS-P4-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F18)
)
:effect
(and
(NO-ACCESS-P4-F18)
(not (NOT-NO-ACCESS-P4-F18))
)
)
(:action BLOCK-ACCESS-P4-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F17)
)
:effect
(and
(NO-ACCESS-P4-F17)
(not (NOT-NO-ACCESS-P4-F17))
)
)
(:action BLOCK-ACCESS-P4-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F16)
)
:effect
(and
(NO-ACCESS-P4-F16)
(not (NOT-NO-ACCESS-P4-F16))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F19)
)
:effect
(and
(NO-ACCESS-P3-F19)
(not (NOT-NO-ACCESS-P3-F19))
)
)
(:action BLOCK-ACCESS-P3-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F18)
)
:effect
(and
(NO-ACCESS-P3-F18)
(not (NOT-NO-ACCESS-P3-F18))
)
)
(:action BLOCK-ACCESS-P3-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F17)
)
:effect
(and
(NO-ACCESS-P3-F17)
(not (NOT-NO-ACCESS-P3-F17))
)
)
(:action BLOCK-ACCESS-P3-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F16)
)
:effect
(and
(NO-ACCESS-P3-F16)
(not (NOT-NO-ACCESS-P3-F16))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F19)
)
:effect
(and
(NO-ACCESS-P2-F19)
(not (NOT-NO-ACCESS-P2-F19))
)
)
(:action BLOCK-ACCESS-P2-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F18)
)
:effect
(and
(NO-ACCESS-P2-F18)
(not (NOT-NO-ACCESS-P2-F18))
)
)
(:action BLOCK-ACCESS-P2-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F17)
)
:effect
(and
(NO-ACCESS-P2-F17)
(not (NOT-NO-ACCESS-P2-F17))
)
)
(:action BLOCK-ACCESS-P2-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F16)
)
:effect
(and
(NO-ACCESS-P2-F16)
(not (NOT-NO-ACCESS-P2-F16))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F19)
)
:effect
(and
(NO-ACCESS-P1-F19)
(not (NOT-NO-ACCESS-P1-F19))
)
)
(:action BLOCK-ACCESS-P1-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F18)
)
:effect
(and
(NO-ACCESS-P1-F18)
(not (NOT-NO-ACCESS-P1-F18))
)
)
(:action BLOCK-ACCESS-P1-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F17)
)
:effect
(and
(NO-ACCESS-P1-F17)
(not (NOT-NO-ACCESS-P1-F17))
)
)
(:action BLOCK-ACCESS-P1-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F16)
)
:effect
(and
(NO-ACCESS-P1-F16)
(not (NOT-NO-ACCESS-P1-F16))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F19)
)
:effect
(and
(NO-ACCESS-P0-F19)
(not (NOT-NO-ACCESS-P0-F19))
)
)
(:action BLOCK-ACCESS-P0-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F18)
)
:effect
(and
(NO-ACCESS-P0-F18)
(not (NOT-NO-ACCESS-P0-F18))
)
)
(:action BLOCK-ACCESS-P0-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F17)
)
:effect
(and
(NO-ACCESS-P0-F17)
(not (NOT-NO-ACCESS-P0-F17))
)
)
(:action BLOCK-ACCESS-P0-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F16)
)
:effect
(and
(NO-ACCESS-P0-F16)
(not (NOT-NO-ACCESS-P0-F16))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F0
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F16-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F17-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F18-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F19-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P3)
(NOT-BOARDED-P6)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F19))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F16
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F17
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F18
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F19
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P7)(NOT-BOARDED-P2)))

(:derived (Flag3-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag6-)(and (NOT-BOARDED-P7)))

(:derived (Flag7-)(and (NOT-BOARDED-P9)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P9-F19)))

(:derived (Flag8-)(and (NOT-BOARDED-P8)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P8-F19)))

(:derived (Flag9-)(and (NOT-BOARDED-P7)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P7-F19)))

(:derived (Flag10-)(and (NOT-BOARDED-P6)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P6-F19)))

(:derived (Flag11-)(and (NOT-BOARDED-P5)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P5-F19)))

(:derived (Flag12-)(and (NOT-BOARDED-P4)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P4-F19)))

(:derived (Flag13-)(and (NOT-BOARDED-P3)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P3-F19)))

(:derived (Flag14-)(and (NOT-BOARDED-P2)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P2-F19)))

(:derived (Flag15-)(and (NOT-BOARDED-P1)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P1-F19)))

(:derived (Flag16-)(and (NOT-BOARDED-P0)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P0-F19)))

(:derived (Flag18-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)))

(:derived (Flag21-)(and (NOT-BOARDED-P9)))

(:derived (Flag21-)(and (NOT-NO-ACCESS-P9-F18)))

(:derived (Flag22-)(and (NOT-BOARDED-P8)))

(:derived (Flag22-)(and (NOT-NO-ACCESS-P8-F18)))

(:derived (Flag23-)(and (NOT-BOARDED-P7)))

(:derived (Flag23-)(and (NOT-NO-ACCESS-P7-F18)))

(:derived (Flag24-)(and (NOT-BOARDED-P6)))

(:derived (Flag24-)(and (NOT-NO-ACCESS-P6-F18)))

(:derived (Flag25-)(and (NOT-BOARDED-P5)))

(:derived (Flag25-)(and (NOT-NO-ACCESS-P5-F18)))

(:derived (Flag26-)(and (NOT-BOARDED-P4)))

(:derived (Flag26-)(and (NOT-NO-ACCESS-P4-F18)))

(:derived (Flag27-)(and (NOT-BOARDED-P3)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P3-F18)))

(:derived (Flag28-)(and (NOT-BOARDED-P2)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P2-F18)))

(:derived (Flag29-)(and (NOT-BOARDED-P1)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P1-F18)))

(:derived (Flag30-)(and (NOT-BOARDED-P0)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P0-F18)))

(:derived (Flag36-)(and (NOT-BOARDED-P7)))

(:derived (Flag37-)(and (NOT-BOARDED-P9)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P9-F17)))

(:derived (Flag38-)(and (NOT-BOARDED-P8)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P8-F17)))

(:derived (Flag39-)(and (NOT-BOARDED-P7)))

(:derived (Flag39-)(and (NOT-NO-ACCESS-P7-F17)))

(:derived (Flag40-)(and (NOT-BOARDED-P6)))

(:derived (Flag40-)(and (NOT-NO-ACCESS-P6-F17)))

(:derived (Flag41-)(and (NOT-BOARDED-P5)))

(:derived (Flag41-)(and (NOT-NO-ACCESS-P5-F17)))

(:derived (Flag42-)(and (NOT-BOARDED-P4)))

(:derived (Flag42-)(and (NOT-NO-ACCESS-P4-F17)))

(:derived (Flag43-)(and (NOT-BOARDED-P3)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P3-F17)))

(:derived (Flag44-)(and (NOT-BOARDED-P2)))

(:derived (Flag44-)(and (NOT-NO-ACCESS-P2-F17)))

(:derived (Flag45-)(and (NOT-BOARDED-P1)))

(:derived (Flag45-)(and (NOT-NO-ACCESS-P1-F17)))

(:derived (Flag46-)(and (NOT-BOARDED-P0)))

(:derived (Flag46-)(and (NOT-NO-ACCESS-P0-F17)))

(:derived (Flag52-)(and (NOT-BOARDED-P7)))

(:derived (Flag52-)(and (NOT-SERVED-P6)))

(:derived (Flag53-)(and (NOT-BOARDED-P9)))

(:derived (Flag53-)(and (NOT-NO-ACCESS-P9-F16)))

(:derived (Flag54-)(and (NOT-BOARDED-P8)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P8-F16)))

(:derived (Flag55-)(and (NOT-BOARDED-P7)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P7-F16)))

(:derived (Flag56-)(and (NOT-BOARDED-P6)))

(:derived (Flag56-)(and (NOT-NO-ACCESS-P6-F16)))

(:derived (Flag57-)(and (NOT-BOARDED-P5)))

(:derived (Flag57-)(and (NOT-NO-ACCESS-P5-F16)))

(:derived (Flag58-)(and (NOT-BOARDED-P4)))

(:derived (Flag58-)(and (NOT-NO-ACCESS-P4-F16)))

(:derived (Flag59-)(and (NOT-BOARDED-P3)))

(:derived (Flag59-)(and (NOT-NO-ACCESS-P3-F16)))

(:derived (Flag60-)(and (NOT-BOARDED-P2)))

(:derived (Flag60-)(and (NOT-NO-ACCESS-P2-F16)))

(:derived (Flag61-)(and (NOT-BOARDED-P1)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P1-F16)))

(:derived (Flag62-)(and (NOT-BOARDED-P0)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P0-F16)))

(:derived (Flag65-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P0)))

(:derived (Flag71-)(and (NOT-BOARDED-P9)))

(:derived (Flag71-)(and (NOT-NO-ACCESS-P9-F15)))

(:derived (Flag72-)(and (NOT-BOARDED-P8)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P8-F15)))

(:derived (Flag73-)(and (NOT-BOARDED-P7)))

(:derived (Flag73-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag74-)(and (NOT-BOARDED-P6)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag75-)(and (NOT-BOARDED-P5)))

(:derived (Flag75-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag76-)(and (NOT-BOARDED-P4)))

(:derived (Flag76-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag77-)(and (NOT-BOARDED-P3)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag78-)(and (NOT-BOARDED-P2)))

(:derived (Flag78-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag79-)(and (NOT-BOARDED-P1)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag80-)(and (NOT-BOARDED-P0)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag82-)(and (NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag85-)(and (NOT-BOARDED-P7)))

(:derived (Flag86-)(and (NOT-BOARDED-P9)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P9-F14)))

(:derived (Flag87-)(and (NOT-BOARDED-P8)))

(:derived (Flag87-)(and (NOT-NO-ACCESS-P8-F14)))

(:derived (Flag88-)(and (NOT-BOARDED-P7)))

(:derived (Flag88-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag89-)(and (NOT-BOARDED-P6)))

(:derived (Flag89-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag90-)(and (NOT-BOARDED-P5)))

(:derived (Flag90-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag91-)(and (NOT-BOARDED-P4)))

(:derived (Flag91-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag92-)(and (NOT-BOARDED-P3)))

(:derived (Flag92-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag93-)(and (NOT-BOARDED-P2)))

(:derived (Flag93-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag94-)(and (NOT-BOARDED-P1)))

(:derived (Flag94-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag95-)(and (NOT-BOARDED-P0)))

(:derived (Flag95-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag98-)(and (NOT-BOARDED-P7)))

(:derived (Flag100-)(and (NOT-BOARDED-P7)))

(:derived (Flag101-)(and (NOT-BOARDED-P7)))

(:derived (Flag101-)(and (NOT-SERVED-P5)))

(:derived (Flag102-)(and (NOT-BOARDED-P9)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P9-F13)))

(:derived (Flag103-)(and (NOT-BOARDED-P8)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P8-F13)))

(:derived (Flag104-)(and (NOT-BOARDED-P7)))

(:derived (Flag104-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag105-)(and (NOT-BOARDED-P6)))

(:derived (Flag105-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag106-)(and (NOT-BOARDED-P5)))

(:derived (Flag106-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag107-)(and (NOT-BOARDED-P4)))

(:derived (Flag107-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag108-)(and (NOT-BOARDED-P3)))

(:derived (Flag108-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag109-)(and (NOT-BOARDED-P2)))

(:derived (Flag109-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag110-)(and (NOT-BOARDED-P1)))

(:derived (Flag110-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag111-)(and (NOT-BOARDED-P0)))

(:derived (Flag111-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag113-)(and (NOT-BOARDED-P9)))

(:derived (Flag113-)(and (NOT-NO-ACCESS-P9-F12)))

(:derived (Flag114-)(and (NOT-BOARDED-P8)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P8-F12)))

(:derived (Flag115-)(and (NOT-BOARDED-P7)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag116-)(and (NOT-BOARDED-P6)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag117-)(and (NOT-BOARDED-P5)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag118-)(and (NOT-BOARDED-P4)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag119-)(and (NOT-BOARDED-P3)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag120-)(and (NOT-BOARDED-P2)))

(:derived (Flag120-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag121-)(and (NOT-BOARDED-P1)))

(:derived (Flag121-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag122-)(and (NOT-BOARDED-P0)))

(:derived (Flag122-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag124-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P5)(NOT-BOARDED-P4)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag127-)(and (NOT-BOARDED-P7)))

(:derived (Flag128-)(and (NOT-BOARDED-P9)))

(:derived (Flag128-)(and (NOT-NO-ACCESS-P9-F11)))

(:derived (Flag129-)(and (NOT-BOARDED-P8)))

(:derived (Flag129-)(and (NOT-NO-ACCESS-P8-F11)))

(:derived (Flag130-)(and (NOT-BOARDED-P7)))

(:derived (Flag130-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag131-)(and (NOT-BOARDED-P6)))

(:derived (Flag131-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag132-)(and (NOT-BOARDED-P5)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag133-)(and (NOT-BOARDED-P4)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag134-)(and (NOT-BOARDED-P3)))

(:derived (Flag134-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag135-)(and (NOT-BOARDED-P2)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag136-)(and (NOT-BOARDED-P1)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag137-)(and (NOT-BOARDED-P0)))

(:derived (Flag137-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag143-)(and (NOT-BOARDED-P7)))

(:derived (Flag143-)(and (NOT-SERVED-P2)))

(:derived (Flag144-)(and (NOT-BOARDED-P9)))

(:derived (Flag144-)(and (NOT-NO-ACCESS-P9-F10)))

(:derived (Flag145-)(and (NOT-BOARDED-P8)))

(:derived (Flag145-)(and (NOT-NO-ACCESS-P8-F10)))

(:derived (Flag146-)(and (NOT-BOARDED-P7)))

(:derived (Flag146-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag147-)(and (NOT-BOARDED-P6)))

(:derived (Flag147-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag148-)(and (NOT-BOARDED-P5)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag149-)(and (NOT-BOARDED-P4)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag150-)(and (NOT-BOARDED-P2)))

(:derived (Flag150-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag151-)(and (NOT-BOARDED-P1)))

(:derived (Flag151-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag152-)(and (NOT-BOARDED-P0)))

(:derived (Flag152-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag154-)(and (NOT-BOARDED-P9)))

(:derived (Flag154-)(and (NOT-NO-ACCESS-P9-F9)))

(:derived (Flag155-)(and (NOT-BOARDED-P8)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P8-F9)))

(:derived (Flag156-)(and (NOT-BOARDED-P7)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag157-)(and (NOT-BOARDED-P6)))

(:derived (Flag157-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag158-)(and (NOT-BOARDED-P5)))

(:derived (Flag158-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag159-)(and (NOT-BOARDED-P4)))

(:derived (Flag159-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag160-)(and (NOT-BOARDED-P3)))

(:derived (Flag160-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag161-)(and (NOT-BOARDED-P2)))

(:derived (Flag161-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag162-)(and (NOT-BOARDED-P1)))

(:derived (Flag162-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag163-)(and (NOT-BOARDED-P0)))

(:derived (Flag163-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag165-)(and (NOT-BOARDED-P9)))

(:derived (Flag165-)(and (NOT-NO-ACCESS-P9-F8)))

(:derived (Flag166-)(and (NOT-BOARDED-P8)))

(:derived (Flag166-)(and (NOT-NO-ACCESS-P8-F8)))

(:derived (Flag167-)(and (NOT-BOARDED-P7)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag168-)(and (NOT-BOARDED-P6)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag169-)(and (NOT-BOARDED-P5)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag170-)(and (NOT-BOARDED-P4)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag171-)(and (NOT-BOARDED-P3)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag172-)(and (NOT-BOARDED-P2)))

(:derived (Flag172-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag173-)(and (NOT-BOARDED-P1)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag174-)(and (NOT-BOARDED-P0)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag180-)(and (NOT-BOARDED-P7)))

(:derived (Flag180-)(and (NOT-SERVED-P9)))

(:derived (Flag181-)(and (NOT-BOARDED-P9)))

(:derived (Flag181-)(and (NOT-NO-ACCESS-P9-F7)))

(:derived (Flag182-)(and (NOT-BOARDED-P8)))

(:derived (Flag182-)(and (NOT-NO-ACCESS-P8-F7)))

(:derived (Flag183-)(and (NOT-BOARDED-P7)))

(:derived (Flag183-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag184-)(and (NOT-BOARDED-P6)))

(:derived (Flag184-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag185-)(and (NOT-BOARDED-P5)))

(:derived (Flag185-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag186-)(and (NOT-BOARDED-P4)))

(:derived (Flag186-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag187-)(and (NOT-BOARDED-P3)))

(:derived (Flag187-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag188-)(and (NOT-BOARDED-P2)))

(:derived (Flag188-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag189-)(and (NOT-BOARDED-P1)))

(:derived (Flag189-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag190-)(and (NOT-BOARDED-P0)))

(:derived (Flag190-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag196-)(and (NOT-BOARDED-P7)))

(:derived (Flag196-)(and (NOT-SERVED-P3)))

(:derived (Flag197-)(and (NOT-BOARDED-P9)))

(:derived (Flag197-)(and (NOT-NO-ACCESS-P9-F6)))

(:derived (Flag198-)(and (NOT-BOARDED-P8)))

(:derived (Flag198-)(and (NOT-NO-ACCESS-P8-F6)))

(:derived (Flag199-)(and (NOT-BOARDED-P7)))

(:derived (Flag199-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag200-)(and (NOT-BOARDED-P6)))

(:derived (Flag200-)(and (NOT-NO-ACCESS-P6-F6)))

(:derived (Flag201-)(and (NOT-BOARDED-P5)))

(:derived (Flag201-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag202-)(and (NOT-BOARDED-P4)))

(:derived (Flag202-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag203-)(and (NOT-BOARDED-P3)))

(:derived (Flag203-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag204-)(and (NOT-BOARDED-P2)))

(:derived (Flag204-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag205-)(and (NOT-BOARDED-P1)))

(:derived (Flag205-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag206-)(and (NOT-BOARDED-P0)))

(:derived (Flag206-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag212-)(and (NOT-BOARDED-P9)))

(:derived (Flag212-)(and (NOT-NO-ACCESS-P9-F5)))

(:derived (Flag213-)(and (NOT-BOARDED-P8)))

(:derived (Flag213-)(and (NOT-NO-ACCESS-P8-F5)))

(:derived (Flag214-)(and (NOT-BOARDED-P7)))

(:derived (Flag214-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag215-)(and (NOT-BOARDED-P6)))

(:derived (Flag215-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag216-)(and (NOT-BOARDED-P5)))

(:derived (Flag216-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag217-)(and (NOT-BOARDED-P4)))

(:derived (Flag217-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag218-)(and (NOT-BOARDED-P2)))

(:derived (Flag218-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag219-)(and (NOT-BOARDED-P1)))

(:derived (Flag219-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag220-)(and (NOT-BOARDED-P0)))

(:derived (Flag220-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag226-)(and (NOT-BOARDED-P7)))

(:derived (Flag226-)(and (NOT-SERVED-P1)))

(:derived (Flag227-)(and (NOT-BOARDED-P9)))

(:derived (Flag227-)(and (NOT-NO-ACCESS-P9-F4)))

(:derived (Flag228-)(and (NOT-BOARDED-P8)))

(:derived (Flag228-)(and (NOT-NO-ACCESS-P8-F4)))

(:derived (Flag229-)(and (NOT-BOARDED-P7)))

(:derived (Flag229-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag230-)(and (NOT-BOARDED-P6)))

(:derived (Flag230-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag231-)(and (NOT-BOARDED-P5)))

(:derived (Flag231-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag232-)(and (NOT-BOARDED-P3)))

(:derived (Flag232-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag233-)(and (NOT-BOARDED-P2)))

(:derived (Flag233-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag234-)(and (NOT-BOARDED-P1)))

(:derived (Flag234-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag235-)(and (NOT-BOARDED-P0)))

(:derived (Flag235-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag237-)(and (NOT-BOARDED-P9)))

(:derived (Flag237-)(and (NOT-NO-ACCESS-P9-F3)))

(:derived (Flag238-)(and (NOT-BOARDED-P8)))

(:derived (Flag238-)(and (NOT-NO-ACCESS-P8-F3)))

(:derived (Flag239-)(and (NOT-BOARDED-P7)))

(:derived (Flag239-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag240-)(and (NOT-BOARDED-P6)))

(:derived (Flag240-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag241-)(and (NOT-BOARDED-P5)))

(:derived (Flag241-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag242-)(and (NOT-BOARDED-P4)))

(:derived (Flag242-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag243-)(and (NOT-BOARDED-P2)))

(:derived (Flag243-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag244-)(and (NOT-BOARDED-P1)))

(:derived (Flag244-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag245-)(and (NOT-BOARDED-P0)))

(:derived (Flag245-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag247-)(and (NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P6)(NOT-BOARDED-P4)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag248-)(and (NOT-BOARDED-P2)))

(:derived (Flag249-)(and (NOT-BOARDED-P2)))

(:derived (Flag250-)(and (NOT-BOARDED-P9)))

(:derived (Flag250-)(and (NOT-NO-ACCESS-P9-F2)))

(:derived (Flag251-)(and (NOT-BOARDED-P8)))

(:derived (Flag251-)(and (NOT-NO-ACCESS-P8-F2)))

(:derived (Flag252-)(and (NOT-BOARDED-P7)))

(:derived (Flag252-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag253-)(and (NOT-BOARDED-P6)))

(:derived (Flag253-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag254-)(and (NOT-BOARDED-P5)))

(:derived (Flag254-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag255-)(and (NOT-BOARDED-P4)))

(:derived (Flag255-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag256-)(and (NOT-BOARDED-P3)))

(:derived (Flag256-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag257-)(and (NOT-BOARDED-P2)))

(:derived (Flag257-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag258-)(and (NOT-BOARDED-P1)))

(:derived (Flag258-)(and (NOT-NO-ACCESS-P1-F2)))

(:derived (Flag259-)(and (NOT-BOARDED-P0)))

(:derived (Flag259-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag261-)(and (NOT-BOARDED-P9)))

(:derived (Flag261-)(and (NOT-NO-ACCESS-P9-F1)))

(:derived (Flag262-)(and (NOT-BOARDED-P8)))

(:derived (Flag262-)(and (NOT-NO-ACCESS-P8-F1)))

(:derived (Flag263-)(and (NOT-BOARDED-P7)))

(:derived (Flag263-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag264-)(and (NOT-BOARDED-P6)))

(:derived (Flag264-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag265-)(and (NOT-BOARDED-P5)))

(:derived (Flag265-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag266-)(and (NOT-BOARDED-P4)))

(:derived (Flag266-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag267-)(and (NOT-BOARDED-P3)))

(:derived (Flag267-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag268-)(and (NOT-BOARDED-P2)))

(:derived (Flag268-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag269-)(and (NOT-BOARDED-P1)))

(:derived (Flag269-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag270-)(and (NOT-BOARDED-P0)))

(:derived (Flag270-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag276-)(and (NOT-BOARDED-P9)))

(:derived (Flag276-)(and (NOT-NO-ACCESS-P9-F0)))

(:derived (Flag277-)(and (NOT-BOARDED-P8)))

(:derived (Flag277-)(and (NOT-NO-ACCESS-P8-F0)))

(:derived (Flag278-)(and (NOT-BOARDED-P7)))

(:derived (Flag278-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag279-)(and (NOT-BOARDED-P6)))

(:derived (Flag279-)(and (NOT-NO-ACCESS-P6-F0)))

(:derived (Flag280-)(and (NOT-BOARDED-P5)))

(:derived (Flag280-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag281-)(and (NOT-BOARDED-P4)))

(:derived (Flag281-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag282-)(and (NOT-BOARDED-P3)))

(:derived (Flag282-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag283-)(and (NOT-BOARDED-P2)))

(:derived (Flag283-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag284-)(and (NOT-BOARDED-P1)))

(:derived (Flag284-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag285-)(and (NOT-BOARDED-P0)))

(:derived (Flag285-)(and (NOT-NO-ACCESS-P0-F0)))

)
