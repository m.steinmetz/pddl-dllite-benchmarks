(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag443-)
(Flag442-)
(Flag441-)
(Flag440-)
(Flag439-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag428-)
(Flag426-)
(Flag425-)
(Flag424-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag413-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag373-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag316-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag232-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag181-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag145-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag57-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag40-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag21-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag3-)
(Flag2-)
(LIFT-AT-F25)
(LIFT-AT-F24)
(LIFT-AT-F23)
(LIFT-AT-F22)
(LIFT-AT-F21)
(LIFT-AT-F20)
(LIFT-AT-F19)
(LIFT-AT-F18)
(LIFT-AT-F17)
(LIFT-AT-F16)
(LIFT-AT-F15)
(LIFT-AT-F14)
(LIFT-AT-F13)
(LIFT-AT-F12)
(LIFT-AT-F11)
(LIFT-AT-F10)
(LIFT-AT-F9)
(LIFT-AT-F8)
(LIFT-AT-F7)
(LIFT-AT-F6)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P0-F6)
(NO-ACCESS-P0-F7)
(NO-ACCESS-P0-F8)
(NO-ACCESS-P0-F9)
(NO-ACCESS-P0-F10)
(NO-ACCESS-P0-F11)
(NO-ACCESS-P0-F12)
(NO-ACCESS-P0-F13)
(NO-ACCESS-P0-F14)
(NO-ACCESS-P0-F15)
(NO-ACCESS-P0-F16)
(NO-ACCESS-P0-F17)
(NO-ACCESS-P0-F18)
(NO-ACCESS-P0-F19)
(NO-ACCESS-P0-F20)
(NO-ACCESS-P0-F21)
(NO-ACCESS-P0-F22)
(NO-ACCESS-P0-F23)
(NO-ACCESS-P0-F24)
(NO-ACCESS-P0-F25)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P1-F6)
(NO-ACCESS-P1-F7)
(NO-ACCESS-P1-F8)
(NO-ACCESS-P1-F9)
(NO-ACCESS-P1-F10)
(NO-ACCESS-P1-F11)
(NO-ACCESS-P1-F12)
(NO-ACCESS-P1-F13)
(NO-ACCESS-P1-F14)
(NO-ACCESS-P1-F15)
(NO-ACCESS-P1-F16)
(NO-ACCESS-P1-F17)
(NO-ACCESS-P1-F18)
(NO-ACCESS-P1-F19)
(NO-ACCESS-P1-F20)
(NO-ACCESS-P1-F21)
(NO-ACCESS-P1-F22)
(NO-ACCESS-P1-F23)
(NO-ACCESS-P1-F24)
(NO-ACCESS-P1-F25)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(NO-ACCESS-P2-F6)
(NO-ACCESS-P2-F7)
(NO-ACCESS-P2-F8)
(NO-ACCESS-P2-F9)
(NO-ACCESS-P2-F10)
(NO-ACCESS-P2-F11)
(NO-ACCESS-P2-F12)
(NO-ACCESS-P2-F13)
(NO-ACCESS-P2-F14)
(NO-ACCESS-P2-F15)
(NO-ACCESS-P2-F16)
(NO-ACCESS-P2-F17)
(NO-ACCESS-P2-F19)
(NO-ACCESS-P2-F21)
(NO-ACCESS-P2-F22)
(NO-ACCESS-P2-F23)
(NO-ACCESS-P2-F24)
(NO-ACCESS-P3-F0)
(NO-ACCESS-P3-F1)
(NO-ACCESS-P3-F2)
(NO-ACCESS-P3-F3)
(NO-ACCESS-P3-F4)
(NO-ACCESS-P3-F5)
(NO-ACCESS-P3-F6)
(NO-ACCESS-P3-F7)
(NO-ACCESS-P3-F8)
(NO-ACCESS-P3-F9)
(NO-ACCESS-P3-F11)
(NO-ACCESS-P3-F12)
(NO-ACCESS-P3-F13)
(NO-ACCESS-P3-F14)
(NO-ACCESS-P3-F15)
(NO-ACCESS-P3-F16)
(NO-ACCESS-P3-F18)
(NO-ACCESS-P3-F19)
(NO-ACCESS-P3-F20)
(NO-ACCESS-P3-F21)
(NO-ACCESS-P3-F22)
(NO-ACCESS-P3-F23)
(NO-ACCESS-P3-F24)
(NO-ACCESS-P3-F25)
(NO-ACCESS-P4-F0)
(NO-ACCESS-P4-F1)
(NO-ACCESS-P4-F2)
(NO-ACCESS-P4-F3)
(NO-ACCESS-P4-F4)
(NO-ACCESS-P4-F5)
(NO-ACCESS-P4-F6)
(NO-ACCESS-P4-F7)
(NO-ACCESS-P4-F8)
(NO-ACCESS-P4-F9)
(NO-ACCESS-P4-F10)
(NO-ACCESS-P4-F11)
(NO-ACCESS-P4-F12)
(NO-ACCESS-P4-F13)
(NO-ACCESS-P4-F14)
(NO-ACCESS-P4-F15)
(NO-ACCESS-P4-F16)
(NO-ACCESS-P4-F17)
(NO-ACCESS-P4-F18)
(NO-ACCESS-P4-F19)
(NO-ACCESS-P4-F20)
(NO-ACCESS-P4-F21)
(NO-ACCESS-P4-F22)
(NO-ACCESS-P4-F23)
(NO-ACCESS-P4-F24)
(NO-ACCESS-P4-F25)
(NO-ACCESS-P5-F0)
(NO-ACCESS-P5-F1)
(NO-ACCESS-P5-F2)
(NO-ACCESS-P5-F3)
(NO-ACCESS-P5-F4)
(NO-ACCESS-P5-F5)
(NO-ACCESS-P5-F6)
(NO-ACCESS-P5-F7)
(NO-ACCESS-P5-F8)
(NO-ACCESS-P5-F9)
(NO-ACCESS-P5-F10)
(NO-ACCESS-P5-F11)
(NO-ACCESS-P5-F12)
(NO-ACCESS-P5-F13)
(NO-ACCESS-P5-F14)
(NO-ACCESS-P5-F15)
(NO-ACCESS-P5-F16)
(NO-ACCESS-P5-F17)
(NO-ACCESS-P5-F18)
(NO-ACCESS-P5-F19)
(NO-ACCESS-P5-F20)
(NO-ACCESS-P5-F21)
(NO-ACCESS-P5-F22)
(NO-ACCESS-P5-F23)
(NO-ACCESS-P5-F24)
(NO-ACCESS-P5-F25)
(NO-ACCESS-P6-F0)
(NO-ACCESS-P6-F1)
(NO-ACCESS-P6-F2)
(NO-ACCESS-P6-F3)
(NO-ACCESS-P6-F4)
(NO-ACCESS-P6-F5)
(NO-ACCESS-P6-F6)
(NO-ACCESS-P6-F7)
(NO-ACCESS-P6-F8)
(NO-ACCESS-P6-F9)
(NO-ACCESS-P6-F10)
(NO-ACCESS-P6-F11)
(NO-ACCESS-P6-F12)
(NO-ACCESS-P6-F13)
(NO-ACCESS-P6-F14)
(NO-ACCESS-P6-F15)
(NO-ACCESS-P6-F16)
(NO-ACCESS-P6-F17)
(NO-ACCESS-P6-F18)
(NO-ACCESS-P6-F19)
(NO-ACCESS-P6-F20)
(NO-ACCESS-P6-F21)
(NO-ACCESS-P6-F22)
(NO-ACCESS-P6-F23)
(NO-ACCESS-P6-F24)
(NO-ACCESS-P6-F25)
(NO-ACCESS-P7-F0)
(NO-ACCESS-P7-F1)
(NO-ACCESS-P7-F2)
(NO-ACCESS-P7-F3)
(NO-ACCESS-P7-F4)
(NO-ACCESS-P7-F5)
(NO-ACCESS-P7-F6)
(NO-ACCESS-P7-F7)
(NO-ACCESS-P7-F8)
(NO-ACCESS-P7-F9)
(NO-ACCESS-P7-F10)
(NO-ACCESS-P7-F11)
(NO-ACCESS-P7-F12)
(NO-ACCESS-P7-F13)
(NO-ACCESS-P7-F14)
(NO-ACCESS-P7-F15)
(NO-ACCESS-P7-F16)
(NO-ACCESS-P7-F17)
(NO-ACCESS-P7-F18)
(NO-ACCESS-P7-F19)
(NO-ACCESS-P7-F21)
(NO-ACCESS-P7-F22)
(NO-ACCESS-P7-F23)
(NO-ACCESS-P7-F24)
(NO-ACCESS-P7-F25)
(NO-ACCESS-P8-F0)
(NO-ACCESS-P8-F1)
(NO-ACCESS-P8-F2)
(NO-ACCESS-P8-F3)
(NO-ACCESS-P8-F4)
(NO-ACCESS-P8-F5)
(NO-ACCESS-P8-F6)
(NO-ACCESS-P8-F7)
(NO-ACCESS-P8-F8)
(NO-ACCESS-P8-F9)
(NO-ACCESS-P8-F10)
(NO-ACCESS-P8-F11)
(NO-ACCESS-P8-F12)
(NO-ACCESS-P8-F13)
(NO-ACCESS-P8-F14)
(NO-ACCESS-P8-F15)
(NO-ACCESS-P8-F16)
(NO-ACCESS-P8-F17)
(NO-ACCESS-P8-F18)
(NO-ACCESS-P8-F19)
(NO-ACCESS-P8-F20)
(NO-ACCESS-P8-F21)
(NO-ACCESS-P8-F22)
(NO-ACCESS-P8-F23)
(NO-ACCESS-P8-F24)
(NO-ACCESS-P8-F25)
(NO-ACCESS-P9-F0)
(NO-ACCESS-P9-F1)
(NO-ACCESS-P9-F2)
(NO-ACCESS-P9-F3)
(NO-ACCESS-P9-F4)
(NO-ACCESS-P9-F5)
(NO-ACCESS-P9-F6)
(NO-ACCESS-P9-F7)
(NO-ACCESS-P9-F8)
(NO-ACCESS-P9-F9)
(NO-ACCESS-P9-F10)
(NO-ACCESS-P9-F11)
(NO-ACCESS-P9-F12)
(NO-ACCESS-P9-F13)
(NO-ACCESS-P9-F14)
(NO-ACCESS-P9-F15)
(NO-ACCESS-P9-F16)
(NO-ACCESS-P9-F17)
(NO-ACCESS-P9-F18)
(NO-ACCESS-P9-F19)
(NO-ACCESS-P9-F20)
(NO-ACCESS-P9-F21)
(NO-ACCESS-P9-F22)
(NO-ACCESS-P9-F23)
(NO-ACCESS-P9-F24)
(NO-ACCESS-P9-F25)
(NO-ACCESS-P10-F0)
(NO-ACCESS-P10-F1)
(NO-ACCESS-P10-F2)
(NO-ACCESS-P10-F3)
(NO-ACCESS-P10-F4)
(NO-ACCESS-P10-F5)
(NO-ACCESS-P10-F6)
(NO-ACCESS-P10-F7)
(NO-ACCESS-P10-F8)
(NO-ACCESS-P10-F9)
(NO-ACCESS-P10-F10)
(NO-ACCESS-P10-F11)
(NO-ACCESS-P10-F12)
(NO-ACCESS-P10-F13)
(NO-ACCESS-P10-F14)
(NO-ACCESS-P10-F15)
(NO-ACCESS-P10-F16)
(NO-ACCESS-P10-F17)
(NO-ACCESS-P10-F18)
(NO-ACCESS-P10-F19)
(NO-ACCESS-P10-F20)
(NO-ACCESS-P10-F21)
(NO-ACCESS-P10-F22)
(NO-ACCESS-P10-F23)
(NO-ACCESS-P10-F24)
(NO-ACCESS-P10-F25)
(NO-ACCESS-P11-F0)
(NO-ACCESS-P11-F1)
(NO-ACCESS-P11-F2)
(NO-ACCESS-P11-F3)
(NO-ACCESS-P11-F4)
(NO-ACCESS-P11-F5)
(NO-ACCESS-P11-F6)
(NO-ACCESS-P11-F7)
(NO-ACCESS-P11-F8)
(NO-ACCESS-P11-F9)
(NO-ACCESS-P11-F10)
(NO-ACCESS-P11-F11)
(NO-ACCESS-P11-F12)
(NO-ACCESS-P11-F13)
(NO-ACCESS-P11-F14)
(NO-ACCESS-P11-F15)
(NO-ACCESS-P11-F16)
(NO-ACCESS-P11-F17)
(NO-ACCESS-P11-F18)
(NO-ACCESS-P11-F19)
(NO-ACCESS-P11-F20)
(NO-ACCESS-P11-F21)
(NO-ACCESS-P11-F22)
(NO-ACCESS-P11-F23)
(NO-ACCESS-P11-F24)
(NO-ACCESS-P11-F25)
(NO-ACCESS-P12-F0)
(NO-ACCESS-P12-F1)
(NO-ACCESS-P12-F2)
(NO-ACCESS-P12-F3)
(NO-ACCESS-P12-F4)
(NO-ACCESS-P12-F5)
(NO-ACCESS-P12-F6)
(NO-ACCESS-P12-F7)
(NO-ACCESS-P12-F8)
(NO-ACCESS-P12-F9)
(NO-ACCESS-P12-F10)
(NO-ACCESS-P12-F11)
(NO-ACCESS-P12-F12)
(NO-ACCESS-P12-F13)
(NO-ACCESS-P12-F14)
(NO-ACCESS-P12-F15)
(NO-ACCESS-P12-F16)
(NO-ACCESS-P12-F17)
(NO-ACCESS-P12-F18)
(NO-ACCESS-P12-F19)
(NO-ACCESS-P12-F20)
(NO-ACCESS-P12-F21)
(NO-ACCESS-P12-F22)
(NO-ACCESS-P12-F23)
(NO-ACCESS-P12-F24)
(NO-ACCESS-P12-F25)
(Flag430-)
(Flag429-)
(Flag412-)
(Flag410-)
(Flag394-)
(Flag392-)
(Flag375-)
(Flag374-)
(Flag357-)
(Flag355-)
(Flag337-)
(Flag335-)
(Flag333-)
(Flag299-)
(Flag297-)
(Flag253-)
(Flag251-)
(Flag234-)
(Flag233-)
(Flag202-)
(Flag200-)
(Flag198-)
(Flag165-)
(Flag163-)
(Flag147-)
(Flag146-)
(Flag130-)
(Flag128-)
(Flag59-)
(Flag58-)
(Flag42-)
(Flag41-)
(Flag24-)
(Flag22-)
(Flag5-)
(Flag4-)
(BOARDED-P5)
(SERVED-P4)
(BOARDED-P8)
(SERVED-P12)
(Flag444-)
(Flag427-)
(Flag408-)
(Flag390-)
(Flag372-)
(Flag314-)
(Flag298-)
(Flag296-)
(Flag295-)
(Flag281-)
(Flag267-)
(Flag249-)
(Flag231-)
(Flag217-)
(Flag179-)
(Flag161-)
(Flag144-)
(Flag126-)
(Flag113-)
(Flag99-)
(Flag87-)
(Flag73-)
(Flag56-)
(Flag39-)
(Flag23-)
(Flag20-)
(Flag19-)
(SERVED-P9)
(BOARDED-P1)
(BOARDED-P3)
(SERVED-P0)
(SERVED-P3)
(BOARDED-P9)
(SERVED-P2)
(BOARDED-P12)
(BOARDED-P7)
(SERVED-P7)
(BOARDED-P2)
(BOARDED-P10)
(SERVED-P6)
(SERVED-P10)
(BOARDED-P0)
(SERVED-P8)
(SERVED-P11)
(BOARDED-P6)
(SERVED-P5)
(BOARDED-P4)
(SERVED-P1)
(Flag411-)
(Flag409-)
(Flag393-)
(Flag391-)
(Flag356-)
(Flag354-)
(Flag339-)
(Flag338-)
(Flag336-)
(Flag334-)
(Flag317-)
(Flag315-)
(Flag252-)
(Flag250-)
(Flag201-)
(Flag199-)
(Flag182-)
(Flag180-)
(Flag164-)
(Flag162-)
(Flag129-)
(Flag127-)
(Flag1-)
(Flag353-)
(BOARDED-P11)
(NOT-BOARDED-P11)
(NOT-BOARDED-P4)
(NOT-SERVED-P1)
(NOT-BOARDED-P6)
(NOT-SERVED-P5)
(NOT-SERVED-P11)
(NOT-BOARDED-P0)
(NOT-SERVED-P8)
(NOT-SERVED-P10)
(NOT-BOARDED-P10)
(NOT-SERVED-P6)
(NOT-BOARDED-P2)
(NOT-SERVED-P7)
(NOT-BOARDED-P7)
(NOT-BOARDED-P12)
(NOT-BOARDED-P9)
(NOT-SERVED-P2)
(NOT-SERVED-P3)
(NOT-BOARDED-P3)
(NOT-SERVED-P0)
(NOT-BOARDED-P1)
(NOT-SERVED-P9)
(NOT-BOARDED-P8)
(NOT-SERVED-P12)
(NOT-BOARDED-P5)
(NOT-SERVED-P4)
(NOT-NO-ACCESS-P12-F25)
(NOT-NO-ACCESS-P12-F24)
(NOT-NO-ACCESS-P12-F23)
(NOT-NO-ACCESS-P12-F22)
(NOT-NO-ACCESS-P12-F21)
(NOT-NO-ACCESS-P12-F20)
(NOT-NO-ACCESS-P12-F19)
(NOT-NO-ACCESS-P12-F18)
(NOT-NO-ACCESS-P12-F17)
(NOT-NO-ACCESS-P12-F16)
(NOT-NO-ACCESS-P12-F15)
(NOT-NO-ACCESS-P12-F14)
(NOT-NO-ACCESS-P12-F13)
(NOT-NO-ACCESS-P12-F12)
(NOT-NO-ACCESS-P12-F11)
(NOT-NO-ACCESS-P12-F10)
(NOT-NO-ACCESS-P12-F9)
(NOT-NO-ACCESS-P12-F8)
(NOT-NO-ACCESS-P12-F7)
(NOT-NO-ACCESS-P12-F6)
(NOT-NO-ACCESS-P12-F5)
(NOT-NO-ACCESS-P12-F4)
(NOT-NO-ACCESS-P12-F3)
(NOT-NO-ACCESS-P12-F2)
(NOT-NO-ACCESS-P12-F1)
(NOT-NO-ACCESS-P12-F0)
(NOT-NO-ACCESS-P11-F25)
(NOT-NO-ACCESS-P11-F24)
(NOT-NO-ACCESS-P11-F23)
(NOT-NO-ACCESS-P11-F22)
(NOT-NO-ACCESS-P11-F21)
(NOT-NO-ACCESS-P11-F20)
(NOT-NO-ACCESS-P11-F19)
(NOT-NO-ACCESS-P11-F18)
(NOT-NO-ACCESS-P11-F17)
(NOT-NO-ACCESS-P11-F16)
(NOT-NO-ACCESS-P11-F15)
(NOT-NO-ACCESS-P11-F14)
(NOT-NO-ACCESS-P11-F13)
(NOT-NO-ACCESS-P11-F12)
(NOT-NO-ACCESS-P11-F11)
(NOT-NO-ACCESS-P11-F10)
(NOT-NO-ACCESS-P11-F9)
(NOT-NO-ACCESS-P11-F8)
(NOT-NO-ACCESS-P11-F7)
(NOT-NO-ACCESS-P11-F6)
(NOT-NO-ACCESS-P11-F5)
(NOT-NO-ACCESS-P11-F4)
(NOT-NO-ACCESS-P11-F3)
(NOT-NO-ACCESS-P11-F2)
(NOT-NO-ACCESS-P11-F1)
(NOT-NO-ACCESS-P11-F0)
(NOT-NO-ACCESS-P10-F25)
(NOT-NO-ACCESS-P10-F24)
(NOT-NO-ACCESS-P10-F23)
(NOT-NO-ACCESS-P10-F22)
(NOT-NO-ACCESS-P10-F21)
(NOT-NO-ACCESS-P10-F20)
(NOT-NO-ACCESS-P10-F19)
(NOT-NO-ACCESS-P10-F18)
(NOT-NO-ACCESS-P10-F17)
(NOT-NO-ACCESS-P10-F16)
(NOT-NO-ACCESS-P10-F15)
(NOT-NO-ACCESS-P10-F14)
(NOT-NO-ACCESS-P10-F13)
(NOT-NO-ACCESS-P10-F12)
(NOT-NO-ACCESS-P10-F11)
(NOT-NO-ACCESS-P10-F10)
(NOT-NO-ACCESS-P10-F9)
(NOT-NO-ACCESS-P10-F8)
(NOT-NO-ACCESS-P10-F7)
(NOT-NO-ACCESS-P10-F6)
(NOT-NO-ACCESS-P10-F5)
(NOT-NO-ACCESS-P10-F4)
(NOT-NO-ACCESS-P10-F3)
(NOT-NO-ACCESS-P10-F2)
(NOT-NO-ACCESS-P10-F1)
(NOT-NO-ACCESS-P10-F0)
(NOT-NO-ACCESS-P9-F25)
(NOT-NO-ACCESS-P9-F24)
(NOT-NO-ACCESS-P9-F23)
(NOT-NO-ACCESS-P9-F22)
(NOT-NO-ACCESS-P9-F21)
(NOT-NO-ACCESS-P9-F20)
(NOT-NO-ACCESS-P9-F19)
(NOT-NO-ACCESS-P9-F18)
(NOT-NO-ACCESS-P9-F17)
(NOT-NO-ACCESS-P9-F16)
(NOT-NO-ACCESS-P9-F15)
(NOT-NO-ACCESS-P9-F14)
(NOT-NO-ACCESS-P9-F13)
(NOT-NO-ACCESS-P9-F12)
(NOT-NO-ACCESS-P9-F11)
(NOT-NO-ACCESS-P9-F10)
(NOT-NO-ACCESS-P9-F9)
(NOT-NO-ACCESS-P9-F8)
(NOT-NO-ACCESS-P9-F7)
(NOT-NO-ACCESS-P9-F6)
(NOT-NO-ACCESS-P9-F5)
(NOT-NO-ACCESS-P9-F4)
(NOT-NO-ACCESS-P9-F3)
(NOT-NO-ACCESS-P9-F2)
(NOT-NO-ACCESS-P9-F1)
(NOT-NO-ACCESS-P9-F0)
(NOT-NO-ACCESS-P8-F25)
(NOT-NO-ACCESS-P8-F24)
(NOT-NO-ACCESS-P8-F23)
(NOT-NO-ACCESS-P8-F22)
(NOT-NO-ACCESS-P8-F21)
(NOT-NO-ACCESS-P8-F20)
(NOT-NO-ACCESS-P8-F19)
(NOT-NO-ACCESS-P8-F18)
(NOT-NO-ACCESS-P8-F17)
(NOT-NO-ACCESS-P8-F16)
(NOT-NO-ACCESS-P8-F15)
(NOT-NO-ACCESS-P8-F14)
(NOT-NO-ACCESS-P8-F13)
(NOT-NO-ACCESS-P8-F12)
(NOT-NO-ACCESS-P8-F11)
(NOT-NO-ACCESS-P8-F10)
(NOT-NO-ACCESS-P8-F9)
(NOT-NO-ACCESS-P8-F8)
(NOT-NO-ACCESS-P8-F7)
(NOT-NO-ACCESS-P8-F6)
(NOT-NO-ACCESS-P8-F5)
(NOT-NO-ACCESS-P8-F4)
(NOT-NO-ACCESS-P8-F3)
(NOT-NO-ACCESS-P8-F2)
(NOT-NO-ACCESS-P8-F1)
(NOT-NO-ACCESS-P8-F0)
(NOT-NO-ACCESS-P7-F25)
(NOT-NO-ACCESS-P7-F24)
(NOT-NO-ACCESS-P7-F23)
(NOT-NO-ACCESS-P7-F22)
(NOT-NO-ACCESS-P7-F21)
(NOT-NO-ACCESS-P7-F19)
(NOT-NO-ACCESS-P7-F18)
(NOT-NO-ACCESS-P7-F17)
(NOT-NO-ACCESS-P7-F16)
(NOT-NO-ACCESS-P7-F15)
(NOT-NO-ACCESS-P7-F14)
(NOT-NO-ACCESS-P7-F13)
(NOT-NO-ACCESS-P7-F12)
(NOT-NO-ACCESS-P7-F11)
(NOT-NO-ACCESS-P7-F10)
(NOT-NO-ACCESS-P7-F9)
(NOT-NO-ACCESS-P7-F8)
(NOT-NO-ACCESS-P7-F7)
(NOT-NO-ACCESS-P7-F6)
(NOT-NO-ACCESS-P7-F5)
(NOT-NO-ACCESS-P7-F4)
(NOT-NO-ACCESS-P7-F3)
(NOT-NO-ACCESS-P7-F2)
(NOT-NO-ACCESS-P7-F1)
(NOT-NO-ACCESS-P7-F0)
(NOT-NO-ACCESS-P6-F25)
(NOT-NO-ACCESS-P6-F24)
(NOT-NO-ACCESS-P6-F23)
(NOT-NO-ACCESS-P6-F22)
(NOT-NO-ACCESS-P6-F21)
(NOT-NO-ACCESS-P6-F20)
(NOT-NO-ACCESS-P6-F19)
(NOT-NO-ACCESS-P6-F18)
(NOT-NO-ACCESS-P6-F17)
(NOT-NO-ACCESS-P6-F16)
(NOT-NO-ACCESS-P6-F15)
(NOT-NO-ACCESS-P6-F14)
(NOT-NO-ACCESS-P6-F13)
(NOT-NO-ACCESS-P6-F12)
(NOT-NO-ACCESS-P6-F11)
(NOT-NO-ACCESS-P6-F10)
(NOT-NO-ACCESS-P6-F9)
(NOT-NO-ACCESS-P6-F8)
(NOT-NO-ACCESS-P6-F7)
(NOT-NO-ACCESS-P6-F6)
(NOT-NO-ACCESS-P6-F5)
(NOT-NO-ACCESS-P6-F4)
(NOT-NO-ACCESS-P6-F3)
(NOT-NO-ACCESS-P6-F2)
(NOT-NO-ACCESS-P6-F1)
(NOT-NO-ACCESS-P6-F0)
(NOT-NO-ACCESS-P5-F25)
(NOT-NO-ACCESS-P5-F24)
(NOT-NO-ACCESS-P5-F23)
(NOT-NO-ACCESS-P5-F22)
(NOT-NO-ACCESS-P5-F21)
(NOT-NO-ACCESS-P5-F20)
(NOT-NO-ACCESS-P5-F19)
(NOT-NO-ACCESS-P5-F18)
(NOT-NO-ACCESS-P5-F17)
(NOT-NO-ACCESS-P5-F16)
(NOT-NO-ACCESS-P5-F15)
(NOT-NO-ACCESS-P5-F14)
(NOT-NO-ACCESS-P5-F13)
(NOT-NO-ACCESS-P5-F12)
(NOT-NO-ACCESS-P5-F11)
(NOT-NO-ACCESS-P5-F10)
(NOT-NO-ACCESS-P5-F9)
(NOT-NO-ACCESS-P5-F8)
(NOT-NO-ACCESS-P5-F7)
(NOT-NO-ACCESS-P5-F6)
(NOT-NO-ACCESS-P5-F5)
(NOT-NO-ACCESS-P5-F4)
(NOT-NO-ACCESS-P5-F3)
(NOT-NO-ACCESS-P5-F2)
(NOT-NO-ACCESS-P5-F1)
(NOT-NO-ACCESS-P5-F0)
(NOT-NO-ACCESS-P4-F25)
(NOT-NO-ACCESS-P4-F24)
(NOT-NO-ACCESS-P4-F23)
(NOT-NO-ACCESS-P4-F22)
(NOT-NO-ACCESS-P4-F21)
(NOT-NO-ACCESS-P4-F20)
(NOT-NO-ACCESS-P4-F19)
(NOT-NO-ACCESS-P4-F18)
(NOT-NO-ACCESS-P4-F17)
(NOT-NO-ACCESS-P4-F16)
(NOT-NO-ACCESS-P4-F15)
(NOT-NO-ACCESS-P4-F14)
(NOT-NO-ACCESS-P4-F13)
(NOT-NO-ACCESS-P4-F12)
(NOT-NO-ACCESS-P4-F11)
(NOT-NO-ACCESS-P4-F10)
(NOT-NO-ACCESS-P4-F9)
(NOT-NO-ACCESS-P4-F8)
(NOT-NO-ACCESS-P4-F7)
(NOT-NO-ACCESS-P4-F6)
(NOT-NO-ACCESS-P4-F5)
(NOT-NO-ACCESS-P4-F4)
(NOT-NO-ACCESS-P4-F3)
(NOT-NO-ACCESS-P4-F2)
(NOT-NO-ACCESS-P4-F1)
(NOT-NO-ACCESS-P4-F0)
(NOT-NO-ACCESS-P3-F25)
(NOT-NO-ACCESS-P3-F24)
(NOT-NO-ACCESS-P3-F23)
(NOT-NO-ACCESS-P3-F22)
(NOT-NO-ACCESS-P3-F21)
(NOT-NO-ACCESS-P3-F20)
(NOT-NO-ACCESS-P3-F19)
(NOT-NO-ACCESS-P3-F18)
(NOT-NO-ACCESS-P3-F16)
(NOT-NO-ACCESS-P3-F15)
(NOT-NO-ACCESS-P3-F14)
(NOT-NO-ACCESS-P3-F13)
(NOT-NO-ACCESS-P3-F12)
(NOT-NO-ACCESS-P3-F11)
(NOT-NO-ACCESS-P3-F9)
(NOT-NO-ACCESS-P3-F8)
(NOT-NO-ACCESS-P3-F7)
(NOT-NO-ACCESS-P3-F6)
(NOT-NO-ACCESS-P3-F5)
(NOT-NO-ACCESS-P3-F4)
(NOT-NO-ACCESS-P3-F3)
(NOT-NO-ACCESS-P3-F2)
(NOT-NO-ACCESS-P3-F1)
(NOT-NO-ACCESS-P3-F0)
(NOT-NO-ACCESS-P2-F24)
(NOT-NO-ACCESS-P2-F23)
(NOT-NO-ACCESS-P2-F22)
(NOT-NO-ACCESS-P2-F21)
(NOT-NO-ACCESS-P2-F19)
(NOT-NO-ACCESS-P2-F17)
(NOT-NO-ACCESS-P2-F16)
(NOT-NO-ACCESS-P2-F15)
(NOT-NO-ACCESS-P2-F14)
(NOT-NO-ACCESS-P2-F13)
(NOT-NO-ACCESS-P2-F12)
(NOT-NO-ACCESS-P2-F11)
(NOT-NO-ACCESS-P2-F10)
(NOT-NO-ACCESS-P2-F9)
(NOT-NO-ACCESS-P2-F8)
(NOT-NO-ACCESS-P2-F7)
(NOT-NO-ACCESS-P2-F6)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F25)
(NOT-NO-ACCESS-P1-F24)
(NOT-NO-ACCESS-P1-F23)
(NOT-NO-ACCESS-P1-F22)
(NOT-NO-ACCESS-P1-F21)
(NOT-NO-ACCESS-P1-F20)
(NOT-NO-ACCESS-P1-F19)
(NOT-NO-ACCESS-P1-F18)
(NOT-NO-ACCESS-P1-F17)
(NOT-NO-ACCESS-P1-F16)
(NOT-NO-ACCESS-P1-F15)
(NOT-NO-ACCESS-P1-F14)
(NOT-NO-ACCESS-P1-F13)
(NOT-NO-ACCESS-P1-F12)
(NOT-NO-ACCESS-P1-F11)
(NOT-NO-ACCESS-P1-F10)
(NOT-NO-ACCESS-P1-F9)
(NOT-NO-ACCESS-P1-F8)
(NOT-NO-ACCESS-P1-F7)
(NOT-NO-ACCESS-P1-F6)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F25)
(NOT-NO-ACCESS-P0-F24)
(NOT-NO-ACCESS-P0-F23)
(NOT-NO-ACCESS-P0-F22)
(NOT-NO-ACCESS-P0-F21)
(NOT-NO-ACCESS-P0-F20)
(NOT-NO-ACCESS-P0-F19)
(NOT-NO-ACCESS-P0-F18)
(NOT-NO-ACCESS-P0-F17)
(NOT-NO-ACCESS-P0-F16)
(NOT-NO-ACCESS-P0-F15)
(NOT-NO-ACCESS-P0-F14)
(NOT-NO-ACCESS-P0-F13)
(NOT-NO-ACCESS-P0-F12)
(NOT-NO-ACCESS-P0-F11)
(NOT-NO-ACCESS-P0-F10)
(NOT-NO-ACCESS-P0-F9)
(NOT-NO-ACCESS-P0-F8)
(NOT-NO-ACCESS-P0-F7)
(NOT-NO-ACCESS-P0-F6)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag353-)
)
:effect
(and
(when
(and
(NOT-SERVED-P11)
)
(and
(BOARDED-P11)
(not (NOT-BOARDED-P11))
)
)
)
)
(:derived (Flag128-)(and (Flag127-)))

(:derived (Flag130-)(and (Flag129-)))

(:derived (Flag163-)(and (Flag162-)))

(:derived (Flag165-)(and (Flag164-)))

(:derived (Flag181-)(and (Flag180-)))

(:derived (Flag183-)(and (Flag182-)))

(:derived (Flag200-)(and (Flag199-)))

(:derived (Flag202-)(and (Flag201-)))

(:derived (Flag251-)(and (Flag250-)))

(:derived (Flag253-)(and (Flag252-)))

(:derived (Flag316-)(and (Flag315-)))

(:derived (Flag318-)(and (Flag317-)))

(:derived (Flag335-)(and (Flag334-)))

(:derived (Flag337-)(and (Flag336-)))

(:derived (Flag339-)(and (Flag338-)))

(:derived (Flag353-)(and (LIFT-AT-F5)(Flag335-)(Flag337-)(Flag339-)(Flag340-)(Flag341-)(Flag342-)(Flag343-)(Flag344-)(Flag345-)(Flag346-)(Flag347-)(Flag348-)(Flag349-)(Flag350-)(Flag351-)(Flag352-)))

(:derived (Flag355-)(and (Flag354-)))

(:derived (Flag357-)(and (Flag356-)))

(:derived (Flag392-)(and (Flag391-)))

(:derived (Flag394-)(and (Flag393-)))

(:derived (Flag410-)(and (Flag409-)))

(:derived (Flag412-)(and (Flag411-)))

(:derived (Flag1-)(and (SERVED-P0)(SERVED-P1)(SERVED-P2)(SERVED-P3)(SERVED-P4)(SERVED-P5)(SERVED-P6)(SERVED-P7)(SERVED-P8)(SERVED-P9)(SERVED-P10)(SERVED-P11)(SERVED-P12)))

(:derived (Flag6-)(and (BOARDED-P12)))

(:derived (Flag6-)(and (BOARDED-P7)))

(:derived (Flag6-)(and (BOARDED-P4)))

(:derived (Flag6-)(and (BOARDED-P3)))

(:derived (Flag6-)(and (BOARDED-P2)))

(:derived (Flag6-)(and (BOARDED-P1)))

(:derived (Flag6-)(and (BOARDED-P0)))

(:derived (Flag22-)(and (Flag20-)))

(:derived (Flag24-)(and (Flag23-)))

(:derived (Flag25-)(and (BOARDED-P12)))

(:derived (Flag25-)(and (BOARDED-P7)))

(:derived (Flag25-)(and (BOARDED-P4)))

(:derived (Flag25-)(and (BOARDED-P3)))

(:derived (Flag25-)(and (BOARDED-P2)))

(:derived (Flag25-)(and (BOARDED-P0)))

(:derived (Flag127-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(SERVED-P0)(NOT-BOARDED-P0)))

(:derived (Flag129-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)(SERVED-P0)))

(:derived (Flag131-)(and (BOARDED-P12)))

(:derived (Flag131-)(and (BOARDED-P7)))

(:derived (Flag131-)(and (BOARDED-P4)))

(:derived (Flag131-)(and (BOARDED-P3)))

(:derived (Flag131-)(and (BOARDED-P2)))

(:derived (Flag131-)(and (BOARDED-P1)))

(:derived (Flag131-)(and (BOARDED-P0)))

(:derived (Flag162-)(and (NOT-BOARDED-P11)(SERVED-P10)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag164-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(SERVED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag180-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(SERVED-P8)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag182-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag184-)(and (BOARDED-P7)))

(:derived (Flag184-)(and (BOARDED-P4)))

(:derived (Flag184-)(and (BOARDED-P3)))

(:derived (Flag184-)(and (BOARDED-P2)))

(:derived (Flag184-)(and (BOARDED-P1)))

(:derived (Flag184-)(and (BOARDED-P0)))

(:derived (Flag199-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(SERVED-P2)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag201-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag203-)(and (BOARDED-P12)))

(:derived (Flag203-)(and (BOARDED-P7)))

(:derived (Flag203-)(and (BOARDED-P4)))

(:derived (Flag203-)(and (BOARDED-P3)))

(:derived (Flag203-)(and (BOARDED-P2)))

(:derived (Flag203-)(and (BOARDED-P1)))

(:derived (Flag203-)(and (BOARDED-P0)))

(:derived (Flag235-)(and (BOARDED-P12)))

(:derived (Flag235-)(and (BOARDED-P4)))

(:derived (Flag235-)(and (BOARDED-P3)))

(:derived (Flag235-)(and (BOARDED-P2)))

(:derived (Flag235-)(and (BOARDED-P1)))

(:derived (Flag235-)(and (BOARDED-P0)))

(:derived (Flag250-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(SERVED-P7)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag252-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(SERVED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag254-)(and (BOARDED-P12)))

(:derived (Flag254-)(and (BOARDED-P7)))

(:derived (Flag254-)(and (BOARDED-P4)))

(:derived (Flag254-)(and (BOARDED-P3)))

(:derived (Flag254-)(and (BOARDED-P2)))

(:derived (Flag254-)(and (BOARDED-P1)))

(:derived (Flag254-)(and (BOARDED-P0)))

(:derived (Flag297-)(and (Flag296-)))

(:derived (Flag299-)(and (Flag298-)))

(:derived (Flag300-)(and (BOARDED-P12)))

(:derived (Flag300-)(and (BOARDED-P7)))

(:derived (Flag300-)(and (BOARDED-P4)))

(:derived (Flag300-)(and (BOARDED-P3)))

(:derived (Flag300-)(and (BOARDED-P2)))

(:derived (Flag300-)(and (BOARDED-P1)))

(:derived (Flag300-)(and (BOARDED-P0)))

(:derived (Flag315-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(SERVED-P5)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag317-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(SERVED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag319-)(and (BOARDED-P12)))

(:derived (Flag319-)(and (BOARDED-P7)))

(:derived (Flag319-)(and (BOARDED-P3)))

(:derived (Flag319-)(and (BOARDED-P2)))

(:derived (Flag319-)(and (BOARDED-P1)))

(:derived (Flag319-)(and (BOARDED-P0)))

(:derived (Flag334-)(and (SERVED-P11)(NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag336-)(and (NOT-BOARDED-P11)(SERVED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag338-)(and (NOT-BOARDED-P11)(SERVED-P11)))

(:derived (Flag339-)(and (BOARDED-P12)))

(:derived (Flag339-)(and (BOARDED-P7)))

(:derived (Flag339-)(and (BOARDED-P4)))

(:derived (Flag339-)(and (BOARDED-P3)))

(:derived (Flag339-)(and (BOARDED-P2)))

(:derived (Flag339-)(and (BOARDED-P1)))

(:derived (Flag339-)(and (BOARDED-P0)))

(:derived (Flag354-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(SERVED-P9)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag356-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(SERVED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag358-)(and (BOARDED-P12)))

(:derived (Flag358-)(and (BOARDED-P7)))

(:derived (Flag358-)(and (BOARDED-P4)))

(:derived (Flag358-)(and (BOARDED-P3)))

(:derived (Flag358-)(and (BOARDED-P1)))

(:derived (Flag358-)(and (BOARDED-P0)))

(:derived (Flag376-)(and (BOARDED-P12)))

(:derived (Flag376-)(and (BOARDED-P7)))

(:derived (Flag376-)(and (BOARDED-P4)))

(:derived (Flag376-)(and (BOARDED-P2)))

(:derived (Flag376-)(and (BOARDED-P1)))

(:derived (Flag376-)(and (BOARDED-P0)))

(:derived (Flag391-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(SERVED-P3)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag393-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(SERVED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)))

(:derived (Flag395-)(and (BOARDED-P12)))

(:derived (Flag395-)(and (BOARDED-P7)))

(:derived (Flag395-)(and (BOARDED-P4)))

(:derived (Flag395-)(and (BOARDED-P3)))

(:derived (Flag395-)(and (BOARDED-P2)))

(:derived (Flag395-)(and (BOARDED-P1)))

(:derived (Flag409-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(SERVED-P1)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag411-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(SERVED-P1)(NOT-BOARDED-P0)))

(:derived (Flag413-)(and (BOARDED-P12)))

(:derived (Flag413-)(and (BOARDED-P7)))

(:derived (Flag413-)(and (BOARDED-P4)))

(:derived (Flag413-)(and (BOARDED-P3)))

(:derived (Flag413-)(and (BOARDED-P2)))

(:derived (Flag413-)(and (BOARDED-P1)))

(:derived (Flag413-)(and (BOARDED-P0)))

(:action STOP-F25
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
)
)
(:action STOP-F24
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(when
(and
(NOT-SERVED-P4)
)
(and
(BOARDED-P4)
(not (NOT-BOARDED-P4))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
)
)
(:action STOP-F23
:parameters ()
:precondition
(and
(Flag56-)
)
:effect
(and
(when
(and
(NOT-SERVED-P6)
)
(and
(BOARDED-P6)
(not (NOT-BOARDED-P6))
)
)
(when
(and
(BOARDED-P5)
)
(and
(NOT-BOARDED-P5)
(SERVED-P5)
(not (BOARDED-P5))
(not (NOT-SERVED-P5))
)
)
)
)
(:action STOP-F22
:parameters ()
:precondition
(and
(Flag73-)
)
:effect
(and
(when
(and
(BOARDED-P11)
)
(and
(NOT-BOARDED-P11)
(SERVED-P11)
(not (BOARDED-P11))
(not (NOT-SERVED-P11))
)
)
)
)
(:action STOP-F21
:parameters ()
:precondition
(and
(Flag87-)
)
:effect
(and
)
)
(:action STOP-F20
:parameters ()
:precondition
(and
(Flag99-)
)
:effect
(and
)
)
(:action STOP-F19
:parameters ()
:precondition
(and
(Flag113-)
)
:effect
(and
)
)
(:action STOP-F18
:parameters ()
:precondition
(and
(Flag126-)
)
:effect
(and
)
)
(:action STOP-F17
:parameters ()
:precondition
(and
(Flag144-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P8)
)
(and
(NOT-BOARDED-P8)
(SERVED-P8)
(not (BOARDED-P8))
(not (NOT-SERVED-P8))
)
)
)
)
(:action STOP-F16
:parameters ()
:precondition
(and
(Flag161-)
)
:effect
(and
(when
(and
(BOARDED-P10)
)
(and
(NOT-BOARDED-P10)
(SERVED-P10)
(not (BOARDED-P10))
(not (NOT-SERVED-P10))
)
)
)
)
(:action STOP-F15
:parameters ()
:precondition
(and
(Flag179-)
)
:effect
(and
(when
(and
(NOT-SERVED-P10)
)
(and
(BOARDED-P10)
(not (NOT-BOARDED-P10))
)
)
(when
(and
(BOARDED-P6)
)
(and
(NOT-BOARDED-P6)
(SERVED-P6)
(not (BOARDED-P6))
(not (NOT-SERVED-P6))
)
)
)
)
(:action STOP-F13
:parameters ()
:precondition
(and
(Flag217-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
)
)
(:action STOP-F12
:parameters ()
:precondition
(and
(Flag231-)
)
:effect
(and
)
)
(:action STOP-F11
:parameters ()
:precondition
(and
(Flag249-)
)
:effect
(and
(when
(and
(BOARDED-P7)
)
(and
(NOT-BOARDED-P7)
(SERVED-P7)
(not (BOARDED-P7))
(not (NOT-SERVED-P7))
)
)
)
)
(:action STOP-F10
:parameters ()
:precondition
(and
(Flag267-)
)
:effect
(and
(when
(and
(NOT-SERVED-P7)
)
(and
(BOARDED-P7)
(not (NOT-BOARDED-P7))
)
)
)
)
(:action STOP-F9
:parameters ()
:precondition
(and
(Flag281-)
)
:effect
(and
)
)
(:action STOP-F8
:parameters ()
:precondition
(and
(Flag295-)
)
:effect
(and
)
)
(:action STOP-F7
:parameters ()
:precondition
(and
(Flag314-)
)
:effect
(and
(when
(and
(NOT-SERVED-P12)
)
(and
(BOARDED-P12)
(not (NOT-BOARDED-P12))
)
)
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag372-)
)
:effect
(and
(when
(and
(NOT-SERVED-P9)
)
(and
(BOARDED-P9)
(not (NOT-BOARDED-P9))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag390-)
)
:effect
(and
(when
(and
(BOARDED-P3)
)
(and
(NOT-BOARDED-P3)
(SERVED-P3)
(not (BOARDED-P3))
(not (NOT-SERVED-P3))
)
)
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag408-)
)
:effect
(and
(when
(and
(NOT-SERVED-P3)
)
(and
(BOARDED-P3)
(not (NOT-BOARDED-P3))
)
)
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag427-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag444-)
)
:effect
(and
(when
(and
(BOARDED-P9)
)
(and
(NOT-BOARDED-P9)
(SERVED-P9)
(not (BOARDED-P9))
(not (NOT-SERVED-P9))
)
)
)
)
(:derived (Flag19-)(and (LIFT-AT-F25)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(Flag16-)(NOT-BOARDED-P2)(Flag17-)(Flag18-)))

(:derived (Flag20-)(and (NOT-BOARDED-P12)(NOT-BOARDED-P4)(SERVED-P4)))

(:derived (Flag23-)(and (NOT-BOARDED-P12)(SERVED-P4)(NOT-BOARDED-P4)))

(:derived (Flag39-)(and (LIFT-AT-F24)(Flag22-)(Flag24-)(Flag25-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(Flag32-)(Flag33-)(Flag34-)(Flag35-)(Flag36-)(Flag37-)(Flag38-)))

(:derived (Flag56-)(and (LIFT-AT-F23)(Flag41-)(Flag42-)(Flag6-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)))

(:derived (Flag73-)(and (LIFT-AT-F22)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)(Flag70-)(Flag71-)(Flag72-)))

(:derived (Flag87-)(and (LIFT-AT-F21)(Flag4-)(Flag5-)(Flag6-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)(Flag85-)(Flag86-)))

(:derived (Flag99-)(and (LIFT-AT-F20)(Flag4-)(Flag5-)(Flag6-)(Flag88-)(Flag89-)(Flag90-)(Flag91-)(Flag92-)(NOT-BOARDED-P7)(Flag93-)(Flag94-)(Flag95-)(Flag96-)(NOT-BOARDED-P2)(Flag97-)(Flag98-)))

(:derived (Flag113-)(and (LIFT-AT-F19)(Flag4-)(Flag5-)(Flag6-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)))

(:derived (Flag126-)(and (LIFT-AT-F18)(Flag4-)(Flag5-)(Flag6-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)(Flag122-)(Flag123-)(NOT-BOARDED-P2)(Flag124-)(Flag125-)))

(:derived (Flag144-)(and (LIFT-AT-F17)(Flag128-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)(Flag139-)(Flag140-)(NOT-BOARDED-P3)(Flag141-)(Flag142-)(Flag143-)))

(:derived (Flag161-)(and (LIFT-AT-F16)(Flag146-)(Flag147-)(Flag6-)(Flag148-)(Flag149-)(Flag150-)(Flag151-)(Flag152-)(Flag153-)(Flag154-)(Flag155-)(Flag156-)(Flag157-)(Flag158-)(Flag159-)(Flag160-)))

(:derived (Flag179-)(and (LIFT-AT-F15)(Flag163-)(Flag165-)(Flag6-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)(Flag175-)(Flag176-)(Flag177-)(Flag178-)))

(:derived (Flag217-)(and (LIFT-AT-F13)(Flag200-)(Flag202-)(Flag203-)(Flag204-)(Flag205-)(Flag206-)(Flag207-)(Flag208-)(Flag209-)(Flag210-)(Flag211-)(Flag212-)(Flag213-)(Flag214-)(Flag215-)(Flag216-)))

(:derived (Flag231-)(and (LIFT-AT-F12)(Flag4-)(Flag5-)(Flag6-)(Flag218-)(Flag219-)(Flag220-)(Flag221-)(Flag222-)(Flag223-)(Flag224-)(Flag225-)(Flag226-)(Flag227-)(Flag228-)(Flag229-)(Flag230-)))

(:derived (Flag249-)(and (LIFT-AT-F11)(Flag233-)(Flag234-)(Flag235-)(Flag236-)(Flag237-)(Flag238-)(Flag239-)(Flag240-)(Flag241-)(Flag242-)(Flag243-)(Flag244-)(Flag245-)(Flag246-)(Flag247-)(Flag248-)))

(:derived (Flag267-)(and (LIFT-AT-F10)(Flag251-)(Flag253-)(Flag254-)(Flag255-)(Flag256-)(Flag257-)(Flag258-)(Flag259-)(Flag260-)(Flag261-)(Flag262-)(Flag263-)(NOT-BOARDED-P3)(Flag264-)(Flag265-)(Flag266-)))

(:derived (Flag281-)(and (LIFT-AT-F9)(Flag4-)(Flag5-)(Flag6-)(Flag268-)(Flag269-)(Flag270-)(Flag271-)(Flag272-)(Flag273-)(Flag274-)(Flag275-)(Flag276-)(Flag277-)(Flag278-)(Flag279-)(Flag280-)))

(:derived (Flag295-)(and (LIFT-AT-F8)(Flag4-)(Flag5-)(Flag6-)(Flag282-)(Flag283-)(Flag284-)(Flag285-)(Flag286-)(Flag287-)(Flag288-)(Flag289-)(Flag290-)(Flag291-)(Flag292-)(Flag293-)(Flag294-)))

(:derived (Flag296-)(and (NOT-BOARDED-P12)(SERVED-P12)(NOT-BOARDED-P4)))

(:derived (Flag298-)(and (SERVED-P12)(NOT-BOARDED-P12)(NOT-BOARDED-P4)))

(:derived (Flag314-)(and (LIFT-AT-F7)(Flag297-)(Flag299-)(Flag300-)(Flag301-)(Flag302-)(Flag303-)(Flag304-)(Flag305-)(Flag306-)(Flag307-)(Flag308-)(Flag309-)(Flag310-)(Flag311-)(Flag312-)(Flag313-)))

(:derived (Flag372-)(and (LIFT-AT-F4)(Flag355-)(Flag357-)(Flag358-)(Flag359-)(Flag360-)(Flag361-)(Flag362-)(Flag363-)(Flag364-)(Flag365-)(Flag366-)(Flag367-)(Flag368-)(Flag369-)(Flag370-)(Flag371-)))

(:derived (Flag390-)(and (LIFT-AT-F3)(Flag374-)(Flag375-)(Flag376-)(Flag377-)(Flag378-)(Flag379-)(Flag380-)(Flag381-)(Flag382-)(Flag383-)(Flag384-)(Flag385-)(Flag386-)(Flag387-)(Flag388-)(Flag389-)))

(:derived (Flag408-)(and (LIFT-AT-F2)(Flag392-)(Flag394-)(Flag395-)(Flag396-)(Flag397-)(Flag398-)(Flag399-)(Flag400-)(Flag401-)(Flag402-)(Flag403-)(Flag404-)(Flag405-)(Flag406-)(NOT-BOARDED-P1)(Flag407-)))

(:derived (Flag427-)(and (LIFT-AT-F1)(Flag410-)(Flag412-)(Flag413-)(Flag414-)(Flag415-)(Flag416-)(Flag417-)(Flag418-)(Flag419-)(Flag420-)(Flag421-)(Flag422-)(Flag423-)(Flag424-)(Flag425-)(Flag426-)))

(:derived (Flag444-)(and (LIFT-AT-F0)(Flag429-)(Flag430-)(Flag6-)(Flag431-)(Flag432-)(Flag433-)(Flag434-)(Flag435-)(Flag436-)(Flag437-)(Flag438-)(Flag439-)(Flag440-)(Flag441-)(Flag442-)(Flag443-)))

(:action STOP-F14
:parameters ()
:precondition
(and
(Flag198-)
)
:effect
(and
(when
(and
(NOT-SERVED-P8)
)
(and
(BOARDED-P8)
(not (NOT-BOARDED-P8))
)
)
(when
(and
(BOARDED-P12)
)
(and
(NOT-BOARDED-P12)
(SERVED-P12)
(not (BOARDED-P12))
(not (NOT-SERVED-P12))
)
)
)
)
(:action STOP-F6
:parameters ()
:precondition
(and
(Flag333-)
)
:effect
(and
(when
(and
(NOT-SERVED-P5)
)
(and
(BOARDED-P5)
(not (NOT-BOARDED-P5))
)
)
(when
(and
(BOARDED-P4)
)
(and
(NOT-BOARDED-P4)
(SERVED-P4)
(not (BOARDED-P4))
(not (NOT-SERVED-P4))
)
)
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F1))
)
)
(:action UP-F1-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F1))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F2))
)
)
(:action UP-F2-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F2))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F3))
)
)
(:action UP-F3-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F3))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F4))
)
)
(:action UP-F4-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F4))
)
)
(:action UP-F5-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F5))
)
)
(:action UP-F5-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F5))
)
)
(:action UP-F6-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F6))
)
)
(:action UP-F6-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F6))
)
)
(:action UP-F7-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F7))
)
)
(:action UP-F7-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F7))
)
)
(:action UP-F8-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F8))
)
)
(:action UP-F8-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F8))
)
)
(:action UP-F9-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F9))
)
)
(:action UP-F9-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F9))
)
)
(:action UP-F10-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F10))
)
)
(:action UP-F10-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F10))
)
)
(:action UP-F11-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F11))
)
)
(:action UP-F11-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F11))
)
)
(:action UP-F12-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F12))
)
)
(:action UP-F12-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F12))
)
)
(:action UP-F13-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F13))
)
)
(:action UP-F13-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F13))
)
)
(:action UP-F14-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F14))
)
)
(:action UP-F14-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F14))
)
)
(:action UP-F15-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F15))
)
)
(:action UP-F15-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F15))
)
)
(:action UP-F16-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F16))
)
)
(:action UP-F16-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F16))
)
)
(:action UP-F17-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F17))
)
)
(:action UP-F17-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F17))
)
)
(:action UP-F18-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F18))
)
)
(:action UP-F18-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F18))
)
)
(:action UP-F19-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F19))
)
)
(:action UP-F19-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F19))
)
)
(:action UP-F20-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F20))
)
)
(:action UP-F20-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F20))
)
)
(:action UP-F21-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F21))
)
)
(:action UP-F21-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F21))
)
)
(:action UP-F21-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F21))
)
)
(:action UP-F21-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F21))
)
)
(:action UP-F22-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F22))
)
)
(:action UP-F22-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F22))
)
)
(:action UP-F22-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F22))
)
)
(:action UP-F23-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F23))
)
)
(:action UP-F23-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F23))
)
)
(:action UP-F24-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F24))
)
)
(:derived (Flag4-)(and (Flag2-)))

(:derived (Flag4-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag3-)))

(:derived (Flag5-)(and (Flag2-)))

(:derived (Flag22-)(and (Flag21-)))

(:derived (Flag24-)(and (Flag21-)))

(:derived (Flag41-)(and (Flag2-)))

(:derived (Flag41-)(and (Flag40-)))

(:derived (Flag42-)(and (Flag40-)))

(:derived (Flag42-)(and (Flag2-)))

(:derived (Flag58-)(and (Flag2-)))

(:derived (Flag58-)(and (Flag57-)))

(:derived (Flag59-)(and (Flag57-)))

(:derived (Flag59-)(and (Flag2-)))

(:derived (Flag128-)(and (Flag2-)))

(:derived (Flag130-)(and (Flag2-)))

(:derived (Flag146-)(and (Flag2-)))

(:derived (Flag146-)(and (Flag145-)))

(:derived (Flag147-)(and (Flag145-)))

(:derived (Flag147-)(and (Flag2-)))

(:derived (Flag163-)(and (Flag2-)))

(:derived (Flag165-)(and (Flag2-)))

(:derived (Flag198-)(and (LIFT-AT-F14)(Flag181-)(Flag183-)(Flag184-)(Flag185-)(Flag186-)(Flag187-)(Flag188-)(Flag189-)(Flag190-)(Flag191-)(Flag192-)(Flag193-)(Flag194-)(Flag195-)(Flag196-)(Flag197-)))

(:derived (Flag200-)(and (Flag2-)))

(:derived (Flag202-)(and (Flag2-)))

(:derived (Flag233-)(and (Flag2-)))

(:derived (Flag233-)(and (Flag232-)))

(:derived (Flag234-)(and (Flag232-)))

(:derived (Flag234-)(and (Flag2-)))

(:derived (Flag251-)(and (Flag2-)))

(:derived (Flag253-)(and (Flag2-)))

(:derived (Flag297-)(and (Flag3-)))

(:derived (Flag299-)(and (Flag3-)))

(:derived (Flag333-)(and (LIFT-AT-F6)(Flag316-)(Flag318-)(Flag319-)(Flag320-)(Flag321-)(Flag322-)(Flag323-)(Flag324-)(Flag325-)(Flag326-)(Flag327-)(Flag328-)(Flag329-)(Flag330-)(Flag331-)(Flag332-)))

(:derived (Flag335-)(and (Flag2-)))

(:derived (Flag337-)(and (Flag2-)))

(:derived (Flag355-)(and (Flag2-)))

(:derived (Flag357-)(and (Flag2-)))

(:derived (Flag374-)(and (Flag2-)))

(:derived (Flag374-)(and (Flag373-)))

(:derived (Flag375-)(and (Flag373-)))

(:derived (Flag375-)(and (Flag2-)))

(:derived (Flag392-)(and (Flag2-)))

(:derived (Flag394-)(and (Flag2-)))

(:derived (Flag410-)(and (Flag2-)))

(:derived (Flag412-)(and (Flag2-)))

(:derived (Flag429-)(and (Flag2-)))

(:derived (Flag429-)(and (Flag428-)))

(:derived (Flag430-)(and (Flag428-)))

(:derived (Flag430-)(and (Flag2-)))

(:action BLOCK-ACCESS-P12-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F25)
)
:effect
(and
(NO-ACCESS-P12-F25)
(not (NOT-NO-ACCESS-P12-F25))
)
)
(:action BLOCK-ACCESS-P12-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F24)
)
:effect
(and
(NO-ACCESS-P12-F24)
(not (NOT-NO-ACCESS-P12-F24))
)
)
(:action BLOCK-ACCESS-P12-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F23)
)
:effect
(and
(NO-ACCESS-P12-F23)
(not (NOT-NO-ACCESS-P12-F23))
)
)
(:action BLOCK-ACCESS-P12-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F22)
)
:effect
(and
(NO-ACCESS-P12-F22)
(not (NOT-NO-ACCESS-P12-F22))
)
)
(:action BLOCK-ACCESS-P12-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F21)
)
:effect
(and
(NO-ACCESS-P12-F21)
(not (NOT-NO-ACCESS-P12-F21))
)
)
(:action BLOCK-ACCESS-P12-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F20)
)
:effect
(and
(NO-ACCESS-P12-F20)
(not (NOT-NO-ACCESS-P12-F20))
)
)
(:action BLOCK-ACCESS-P12-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F19)
)
:effect
(and
(NO-ACCESS-P12-F19)
(not (NOT-NO-ACCESS-P12-F19))
)
)
(:action BLOCK-ACCESS-P12-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F18)
)
:effect
(and
(NO-ACCESS-P12-F18)
(not (NOT-NO-ACCESS-P12-F18))
)
)
(:action BLOCK-ACCESS-P12-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F17)
)
:effect
(and
(NO-ACCESS-P12-F17)
(not (NOT-NO-ACCESS-P12-F17))
)
)
(:action BLOCK-ACCESS-P12-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F16)
)
:effect
(and
(NO-ACCESS-P12-F16)
(not (NOT-NO-ACCESS-P12-F16))
)
)
(:action BLOCK-ACCESS-P12-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F15)
)
:effect
(and
(NO-ACCESS-P12-F15)
(not (NOT-NO-ACCESS-P12-F15))
)
)
(:action BLOCK-ACCESS-P12-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F14)
)
:effect
(and
(NO-ACCESS-P12-F14)
(not (NOT-NO-ACCESS-P12-F14))
)
)
(:action BLOCK-ACCESS-P12-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F13)
)
:effect
(and
(NO-ACCESS-P12-F13)
(not (NOT-NO-ACCESS-P12-F13))
)
)
(:action BLOCK-ACCESS-P12-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F12)
)
:effect
(and
(NO-ACCESS-P12-F12)
(not (NOT-NO-ACCESS-P12-F12))
)
)
(:action BLOCK-ACCESS-P12-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F11)
)
:effect
(and
(NO-ACCESS-P12-F11)
(not (NOT-NO-ACCESS-P12-F11))
)
)
(:action BLOCK-ACCESS-P12-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F10)
)
:effect
(and
(NO-ACCESS-P12-F10)
(not (NOT-NO-ACCESS-P12-F10))
)
)
(:action BLOCK-ACCESS-P12-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F9)
)
:effect
(and
(NO-ACCESS-P12-F9)
(not (NOT-NO-ACCESS-P12-F9))
)
)
(:action BLOCK-ACCESS-P12-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F8)
)
:effect
(and
(NO-ACCESS-P12-F8)
(not (NOT-NO-ACCESS-P12-F8))
)
)
(:action BLOCK-ACCESS-P12-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F7)
)
:effect
(and
(NO-ACCESS-P12-F7)
(not (NOT-NO-ACCESS-P12-F7))
)
)
(:action BLOCK-ACCESS-P12-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F6)
)
:effect
(and
(NO-ACCESS-P12-F6)
(not (NOT-NO-ACCESS-P12-F6))
)
)
(:action BLOCK-ACCESS-P12-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F5)
)
:effect
(and
(NO-ACCESS-P12-F5)
(not (NOT-NO-ACCESS-P12-F5))
)
)
(:action BLOCK-ACCESS-P12-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F4)
)
:effect
(and
(NO-ACCESS-P12-F4)
(not (NOT-NO-ACCESS-P12-F4))
)
)
(:action BLOCK-ACCESS-P12-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F3)
)
:effect
(and
(NO-ACCESS-P12-F3)
(not (NOT-NO-ACCESS-P12-F3))
)
)
(:action BLOCK-ACCESS-P12-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F2)
)
:effect
(and
(NO-ACCESS-P12-F2)
(not (NOT-NO-ACCESS-P12-F2))
)
)
(:action BLOCK-ACCESS-P12-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F1)
)
:effect
(and
(NO-ACCESS-P12-F1)
(not (NOT-NO-ACCESS-P12-F1))
)
)
(:action BLOCK-ACCESS-P12-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P12-F0)
)
:effect
(and
(NO-ACCESS-P12-F0)
(not (NOT-NO-ACCESS-P12-F0))
)
)
(:action BLOCK-ACCESS-P11-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F25)
)
:effect
(and
(NO-ACCESS-P11-F25)
(not (NOT-NO-ACCESS-P11-F25))
)
)
(:action BLOCK-ACCESS-P11-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F24)
)
:effect
(and
(NO-ACCESS-P11-F24)
(not (NOT-NO-ACCESS-P11-F24))
)
)
(:action BLOCK-ACCESS-P11-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F23)
)
:effect
(and
(NO-ACCESS-P11-F23)
(not (NOT-NO-ACCESS-P11-F23))
)
)
(:action BLOCK-ACCESS-P11-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F22)
)
:effect
(and
(NO-ACCESS-P11-F22)
(not (NOT-NO-ACCESS-P11-F22))
)
)
(:action BLOCK-ACCESS-P11-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F21)
)
:effect
(and
(NO-ACCESS-P11-F21)
(not (NOT-NO-ACCESS-P11-F21))
)
)
(:action BLOCK-ACCESS-P11-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F20)
)
:effect
(and
(NO-ACCESS-P11-F20)
(not (NOT-NO-ACCESS-P11-F20))
)
)
(:action BLOCK-ACCESS-P11-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F19)
)
:effect
(and
(NO-ACCESS-P11-F19)
(not (NOT-NO-ACCESS-P11-F19))
)
)
(:action BLOCK-ACCESS-P11-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F18)
)
:effect
(and
(NO-ACCESS-P11-F18)
(not (NOT-NO-ACCESS-P11-F18))
)
)
(:action BLOCK-ACCESS-P11-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F17)
)
:effect
(and
(NO-ACCESS-P11-F17)
(not (NOT-NO-ACCESS-P11-F17))
)
)
(:action BLOCK-ACCESS-P11-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F16)
)
:effect
(and
(NO-ACCESS-P11-F16)
(not (NOT-NO-ACCESS-P11-F16))
)
)
(:action BLOCK-ACCESS-P11-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F15)
)
:effect
(and
(NO-ACCESS-P11-F15)
(not (NOT-NO-ACCESS-P11-F15))
)
)
(:action BLOCK-ACCESS-P11-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F14)
)
:effect
(and
(NO-ACCESS-P11-F14)
(not (NOT-NO-ACCESS-P11-F14))
)
)
(:action BLOCK-ACCESS-P11-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F13)
)
:effect
(and
(NO-ACCESS-P11-F13)
(not (NOT-NO-ACCESS-P11-F13))
)
)
(:action BLOCK-ACCESS-P11-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F12)
)
:effect
(and
(NO-ACCESS-P11-F12)
(not (NOT-NO-ACCESS-P11-F12))
)
)
(:action BLOCK-ACCESS-P11-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F11)
)
:effect
(and
(NO-ACCESS-P11-F11)
(not (NOT-NO-ACCESS-P11-F11))
)
)
(:action BLOCK-ACCESS-P11-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F10)
)
:effect
(and
(NO-ACCESS-P11-F10)
(not (NOT-NO-ACCESS-P11-F10))
)
)
(:action BLOCK-ACCESS-P11-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F9)
)
:effect
(and
(NO-ACCESS-P11-F9)
(not (NOT-NO-ACCESS-P11-F9))
)
)
(:action BLOCK-ACCESS-P11-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F8)
)
:effect
(and
(NO-ACCESS-P11-F8)
(not (NOT-NO-ACCESS-P11-F8))
)
)
(:action BLOCK-ACCESS-P11-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F7)
)
:effect
(and
(NO-ACCESS-P11-F7)
(not (NOT-NO-ACCESS-P11-F7))
)
)
(:action BLOCK-ACCESS-P11-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F6)
)
:effect
(and
(NO-ACCESS-P11-F6)
(not (NOT-NO-ACCESS-P11-F6))
)
)
(:action BLOCK-ACCESS-P11-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F5)
)
:effect
(and
(NO-ACCESS-P11-F5)
(not (NOT-NO-ACCESS-P11-F5))
)
)
(:action BLOCK-ACCESS-P11-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F4)
)
:effect
(and
(NO-ACCESS-P11-F4)
(not (NOT-NO-ACCESS-P11-F4))
)
)
(:action BLOCK-ACCESS-P11-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F3)
)
:effect
(and
(NO-ACCESS-P11-F3)
(not (NOT-NO-ACCESS-P11-F3))
)
)
(:action BLOCK-ACCESS-P11-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F2)
)
:effect
(and
(NO-ACCESS-P11-F2)
(not (NOT-NO-ACCESS-P11-F2))
)
)
(:action BLOCK-ACCESS-P11-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F1)
)
:effect
(and
(NO-ACCESS-P11-F1)
(not (NOT-NO-ACCESS-P11-F1))
)
)
(:action BLOCK-ACCESS-P11-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P11-F0)
)
:effect
(and
(NO-ACCESS-P11-F0)
(not (NOT-NO-ACCESS-P11-F0))
)
)
(:action BLOCK-ACCESS-P10-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F25)
)
:effect
(and
(NO-ACCESS-P10-F25)
(not (NOT-NO-ACCESS-P10-F25))
)
)
(:action BLOCK-ACCESS-P10-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F24)
)
:effect
(and
(NO-ACCESS-P10-F24)
(not (NOT-NO-ACCESS-P10-F24))
)
)
(:action BLOCK-ACCESS-P10-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F23)
)
:effect
(and
(NO-ACCESS-P10-F23)
(not (NOT-NO-ACCESS-P10-F23))
)
)
(:action BLOCK-ACCESS-P10-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F22)
)
:effect
(and
(NO-ACCESS-P10-F22)
(not (NOT-NO-ACCESS-P10-F22))
)
)
(:action BLOCK-ACCESS-P10-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F21)
)
:effect
(and
(NO-ACCESS-P10-F21)
(not (NOT-NO-ACCESS-P10-F21))
)
)
(:action BLOCK-ACCESS-P10-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F20)
)
:effect
(and
(NO-ACCESS-P10-F20)
(not (NOT-NO-ACCESS-P10-F20))
)
)
(:action BLOCK-ACCESS-P10-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F19)
)
:effect
(and
(NO-ACCESS-P10-F19)
(not (NOT-NO-ACCESS-P10-F19))
)
)
(:action BLOCK-ACCESS-P10-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F18)
)
:effect
(and
(NO-ACCESS-P10-F18)
(not (NOT-NO-ACCESS-P10-F18))
)
)
(:action BLOCK-ACCESS-P10-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F17)
)
:effect
(and
(NO-ACCESS-P10-F17)
(not (NOT-NO-ACCESS-P10-F17))
)
)
(:action BLOCK-ACCESS-P10-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F16)
)
:effect
(and
(NO-ACCESS-P10-F16)
(not (NOT-NO-ACCESS-P10-F16))
)
)
(:action BLOCK-ACCESS-P10-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F15)
)
:effect
(and
(NO-ACCESS-P10-F15)
(not (NOT-NO-ACCESS-P10-F15))
)
)
(:action BLOCK-ACCESS-P10-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F14)
)
:effect
(and
(NO-ACCESS-P10-F14)
(not (NOT-NO-ACCESS-P10-F14))
)
)
(:action BLOCK-ACCESS-P10-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F13)
)
:effect
(and
(NO-ACCESS-P10-F13)
(not (NOT-NO-ACCESS-P10-F13))
)
)
(:action BLOCK-ACCESS-P10-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F12)
)
:effect
(and
(NO-ACCESS-P10-F12)
(not (NOT-NO-ACCESS-P10-F12))
)
)
(:action BLOCK-ACCESS-P10-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F11)
)
:effect
(and
(NO-ACCESS-P10-F11)
(not (NOT-NO-ACCESS-P10-F11))
)
)
(:action BLOCK-ACCESS-P10-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F10)
)
:effect
(and
(NO-ACCESS-P10-F10)
(not (NOT-NO-ACCESS-P10-F10))
)
)
(:action BLOCK-ACCESS-P10-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F9)
)
:effect
(and
(NO-ACCESS-P10-F9)
(not (NOT-NO-ACCESS-P10-F9))
)
)
(:action BLOCK-ACCESS-P10-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F8)
)
:effect
(and
(NO-ACCESS-P10-F8)
(not (NOT-NO-ACCESS-P10-F8))
)
)
(:action BLOCK-ACCESS-P10-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F7)
)
:effect
(and
(NO-ACCESS-P10-F7)
(not (NOT-NO-ACCESS-P10-F7))
)
)
(:action BLOCK-ACCESS-P10-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F6)
)
:effect
(and
(NO-ACCESS-P10-F6)
(not (NOT-NO-ACCESS-P10-F6))
)
)
(:action BLOCK-ACCESS-P10-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F5)
)
:effect
(and
(NO-ACCESS-P10-F5)
(not (NOT-NO-ACCESS-P10-F5))
)
)
(:action BLOCK-ACCESS-P10-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F4)
)
:effect
(and
(NO-ACCESS-P10-F4)
(not (NOT-NO-ACCESS-P10-F4))
)
)
(:action BLOCK-ACCESS-P10-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F3)
)
:effect
(and
(NO-ACCESS-P10-F3)
(not (NOT-NO-ACCESS-P10-F3))
)
)
(:action BLOCK-ACCESS-P10-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F2)
)
:effect
(and
(NO-ACCESS-P10-F2)
(not (NOT-NO-ACCESS-P10-F2))
)
)
(:action BLOCK-ACCESS-P10-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F1)
)
:effect
(and
(NO-ACCESS-P10-F1)
(not (NOT-NO-ACCESS-P10-F1))
)
)
(:action BLOCK-ACCESS-P10-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P10-F0)
)
:effect
(and
(NO-ACCESS-P10-F0)
(not (NOT-NO-ACCESS-P10-F0))
)
)
(:action BLOCK-ACCESS-P9-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F25)
)
:effect
(and
(NO-ACCESS-P9-F25)
(not (NOT-NO-ACCESS-P9-F25))
)
)
(:action BLOCK-ACCESS-P9-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F24)
)
:effect
(and
(NO-ACCESS-P9-F24)
(not (NOT-NO-ACCESS-P9-F24))
)
)
(:action BLOCK-ACCESS-P9-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F23)
)
:effect
(and
(NO-ACCESS-P9-F23)
(not (NOT-NO-ACCESS-P9-F23))
)
)
(:action BLOCK-ACCESS-P9-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F22)
)
:effect
(and
(NO-ACCESS-P9-F22)
(not (NOT-NO-ACCESS-P9-F22))
)
)
(:action BLOCK-ACCESS-P9-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F21)
)
:effect
(and
(NO-ACCESS-P9-F21)
(not (NOT-NO-ACCESS-P9-F21))
)
)
(:action BLOCK-ACCESS-P9-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F20)
)
:effect
(and
(NO-ACCESS-P9-F20)
(not (NOT-NO-ACCESS-P9-F20))
)
)
(:action BLOCK-ACCESS-P9-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F19)
)
:effect
(and
(NO-ACCESS-P9-F19)
(not (NOT-NO-ACCESS-P9-F19))
)
)
(:action BLOCK-ACCESS-P9-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F18)
)
:effect
(and
(NO-ACCESS-P9-F18)
(not (NOT-NO-ACCESS-P9-F18))
)
)
(:action BLOCK-ACCESS-P9-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F17)
)
:effect
(and
(NO-ACCESS-P9-F17)
(not (NOT-NO-ACCESS-P9-F17))
)
)
(:action BLOCK-ACCESS-P9-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F16)
)
:effect
(and
(NO-ACCESS-P9-F16)
(not (NOT-NO-ACCESS-P9-F16))
)
)
(:action BLOCK-ACCESS-P9-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F15)
)
:effect
(and
(NO-ACCESS-P9-F15)
(not (NOT-NO-ACCESS-P9-F15))
)
)
(:action BLOCK-ACCESS-P9-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F14)
)
:effect
(and
(NO-ACCESS-P9-F14)
(not (NOT-NO-ACCESS-P9-F14))
)
)
(:action BLOCK-ACCESS-P9-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F13)
)
:effect
(and
(NO-ACCESS-P9-F13)
(not (NOT-NO-ACCESS-P9-F13))
)
)
(:action BLOCK-ACCESS-P9-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F12)
)
:effect
(and
(NO-ACCESS-P9-F12)
(not (NOT-NO-ACCESS-P9-F12))
)
)
(:action BLOCK-ACCESS-P9-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F11)
)
:effect
(and
(NO-ACCESS-P9-F11)
(not (NOT-NO-ACCESS-P9-F11))
)
)
(:action BLOCK-ACCESS-P9-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F10)
)
:effect
(and
(NO-ACCESS-P9-F10)
(not (NOT-NO-ACCESS-P9-F10))
)
)
(:action BLOCK-ACCESS-P9-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F9)
)
:effect
(and
(NO-ACCESS-P9-F9)
(not (NOT-NO-ACCESS-P9-F9))
)
)
(:action BLOCK-ACCESS-P9-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F8)
)
:effect
(and
(NO-ACCESS-P9-F8)
(not (NOT-NO-ACCESS-P9-F8))
)
)
(:action BLOCK-ACCESS-P9-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F7)
)
:effect
(and
(NO-ACCESS-P9-F7)
(not (NOT-NO-ACCESS-P9-F7))
)
)
(:action BLOCK-ACCESS-P9-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F6)
)
:effect
(and
(NO-ACCESS-P9-F6)
(not (NOT-NO-ACCESS-P9-F6))
)
)
(:action BLOCK-ACCESS-P9-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F5)
)
:effect
(and
(NO-ACCESS-P9-F5)
(not (NOT-NO-ACCESS-P9-F5))
)
)
(:action BLOCK-ACCESS-P9-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F4)
)
:effect
(and
(NO-ACCESS-P9-F4)
(not (NOT-NO-ACCESS-P9-F4))
)
)
(:action BLOCK-ACCESS-P9-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F3)
)
:effect
(and
(NO-ACCESS-P9-F3)
(not (NOT-NO-ACCESS-P9-F3))
)
)
(:action BLOCK-ACCESS-P9-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F2)
)
:effect
(and
(NO-ACCESS-P9-F2)
(not (NOT-NO-ACCESS-P9-F2))
)
)
(:action BLOCK-ACCESS-P9-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F1)
)
:effect
(and
(NO-ACCESS-P9-F1)
(not (NOT-NO-ACCESS-P9-F1))
)
)
(:action BLOCK-ACCESS-P9-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P9-F0)
)
:effect
(and
(NO-ACCESS-P9-F0)
(not (NOT-NO-ACCESS-P9-F0))
)
)
(:action BLOCK-ACCESS-P8-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F25)
)
:effect
(and
(NO-ACCESS-P8-F25)
(not (NOT-NO-ACCESS-P8-F25))
)
)
(:action BLOCK-ACCESS-P8-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F24)
)
:effect
(and
(NO-ACCESS-P8-F24)
(not (NOT-NO-ACCESS-P8-F24))
)
)
(:action BLOCK-ACCESS-P8-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F23)
)
:effect
(and
(NO-ACCESS-P8-F23)
(not (NOT-NO-ACCESS-P8-F23))
)
)
(:action BLOCK-ACCESS-P8-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F22)
)
:effect
(and
(NO-ACCESS-P8-F22)
(not (NOT-NO-ACCESS-P8-F22))
)
)
(:action BLOCK-ACCESS-P8-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F21)
)
:effect
(and
(NO-ACCESS-P8-F21)
(not (NOT-NO-ACCESS-P8-F21))
)
)
(:action BLOCK-ACCESS-P8-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F20)
)
:effect
(and
(NO-ACCESS-P8-F20)
(not (NOT-NO-ACCESS-P8-F20))
)
)
(:action BLOCK-ACCESS-P8-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F19)
)
:effect
(and
(NO-ACCESS-P8-F19)
(not (NOT-NO-ACCESS-P8-F19))
)
)
(:action BLOCK-ACCESS-P8-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F18)
)
:effect
(and
(NO-ACCESS-P8-F18)
(not (NOT-NO-ACCESS-P8-F18))
)
)
(:action BLOCK-ACCESS-P8-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F17)
)
:effect
(and
(NO-ACCESS-P8-F17)
(not (NOT-NO-ACCESS-P8-F17))
)
)
(:action BLOCK-ACCESS-P8-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F16)
)
:effect
(and
(NO-ACCESS-P8-F16)
(not (NOT-NO-ACCESS-P8-F16))
)
)
(:action BLOCK-ACCESS-P8-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F15)
)
:effect
(and
(NO-ACCESS-P8-F15)
(not (NOT-NO-ACCESS-P8-F15))
)
)
(:action BLOCK-ACCESS-P8-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F14)
)
:effect
(and
(NO-ACCESS-P8-F14)
(not (NOT-NO-ACCESS-P8-F14))
)
)
(:action BLOCK-ACCESS-P8-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F13)
)
:effect
(and
(NO-ACCESS-P8-F13)
(not (NOT-NO-ACCESS-P8-F13))
)
)
(:action BLOCK-ACCESS-P8-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F12)
)
:effect
(and
(NO-ACCESS-P8-F12)
(not (NOT-NO-ACCESS-P8-F12))
)
)
(:action BLOCK-ACCESS-P8-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F11)
)
:effect
(and
(NO-ACCESS-P8-F11)
(not (NOT-NO-ACCESS-P8-F11))
)
)
(:action BLOCK-ACCESS-P8-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F10)
)
:effect
(and
(NO-ACCESS-P8-F10)
(not (NOT-NO-ACCESS-P8-F10))
)
)
(:action BLOCK-ACCESS-P8-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F9)
)
:effect
(and
(NO-ACCESS-P8-F9)
(not (NOT-NO-ACCESS-P8-F9))
)
)
(:action BLOCK-ACCESS-P8-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F8)
)
:effect
(and
(NO-ACCESS-P8-F8)
(not (NOT-NO-ACCESS-P8-F8))
)
)
(:action BLOCK-ACCESS-P8-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F7)
)
:effect
(and
(NO-ACCESS-P8-F7)
(not (NOT-NO-ACCESS-P8-F7))
)
)
(:action BLOCK-ACCESS-P8-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F6)
)
:effect
(and
(NO-ACCESS-P8-F6)
(not (NOT-NO-ACCESS-P8-F6))
)
)
(:action BLOCK-ACCESS-P8-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F5)
)
:effect
(and
(NO-ACCESS-P8-F5)
(not (NOT-NO-ACCESS-P8-F5))
)
)
(:action BLOCK-ACCESS-P8-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F4)
)
:effect
(and
(NO-ACCESS-P8-F4)
(not (NOT-NO-ACCESS-P8-F4))
)
)
(:action BLOCK-ACCESS-P8-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F3)
)
:effect
(and
(NO-ACCESS-P8-F3)
(not (NOT-NO-ACCESS-P8-F3))
)
)
(:action BLOCK-ACCESS-P8-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F2)
)
:effect
(and
(NO-ACCESS-P8-F2)
(not (NOT-NO-ACCESS-P8-F2))
)
)
(:action BLOCK-ACCESS-P8-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F1)
)
:effect
(and
(NO-ACCESS-P8-F1)
(not (NOT-NO-ACCESS-P8-F1))
)
)
(:action BLOCK-ACCESS-P8-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P8-F0)
)
:effect
(and
(NO-ACCESS-P8-F0)
(not (NOT-NO-ACCESS-P8-F0))
)
)
(:action BLOCK-ACCESS-P7-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F25)
)
:effect
(and
(NO-ACCESS-P7-F25)
(not (NOT-NO-ACCESS-P7-F25))
)
)
(:action BLOCK-ACCESS-P7-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F24)
)
:effect
(and
(NO-ACCESS-P7-F24)
(not (NOT-NO-ACCESS-P7-F24))
)
)
(:action BLOCK-ACCESS-P7-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F23)
)
:effect
(and
(NO-ACCESS-P7-F23)
(not (NOT-NO-ACCESS-P7-F23))
)
)
(:action BLOCK-ACCESS-P7-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F22)
)
:effect
(and
(NO-ACCESS-P7-F22)
(not (NOT-NO-ACCESS-P7-F22))
)
)
(:action BLOCK-ACCESS-P7-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F21)
)
:effect
(and
(NO-ACCESS-P7-F21)
(not (NOT-NO-ACCESS-P7-F21))
)
)
(:action BLOCK-ACCESS-P7-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F19)
)
:effect
(and
(NO-ACCESS-P7-F19)
(not (NOT-NO-ACCESS-P7-F19))
)
)
(:action BLOCK-ACCESS-P7-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F18)
)
:effect
(and
(NO-ACCESS-P7-F18)
(not (NOT-NO-ACCESS-P7-F18))
)
)
(:action BLOCK-ACCESS-P7-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F17)
)
:effect
(and
(NO-ACCESS-P7-F17)
(not (NOT-NO-ACCESS-P7-F17))
)
)
(:action BLOCK-ACCESS-P7-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F16)
)
:effect
(and
(NO-ACCESS-P7-F16)
(not (NOT-NO-ACCESS-P7-F16))
)
)
(:action BLOCK-ACCESS-P7-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F15)
)
:effect
(and
(NO-ACCESS-P7-F15)
(not (NOT-NO-ACCESS-P7-F15))
)
)
(:action BLOCK-ACCESS-P7-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F14)
)
:effect
(and
(NO-ACCESS-P7-F14)
(not (NOT-NO-ACCESS-P7-F14))
)
)
(:action BLOCK-ACCESS-P7-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F13)
)
:effect
(and
(NO-ACCESS-P7-F13)
(not (NOT-NO-ACCESS-P7-F13))
)
)
(:action BLOCK-ACCESS-P7-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F12)
)
:effect
(and
(NO-ACCESS-P7-F12)
(not (NOT-NO-ACCESS-P7-F12))
)
)
(:action BLOCK-ACCESS-P7-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F11)
)
:effect
(and
(NO-ACCESS-P7-F11)
(not (NOT-NO-ACCESS-P7-F11))
)
)
(:action BLOCK-ACCESS-P7-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F10)
)
:effect
(and
(NO-ACCESS-P7-F10)
(not (NOT-NO-ACCESS-P7-F10))
)
)
(:action BLOCK-ACCESS-P7-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F9)
)
:effect
(and
(NO-ACCESS-P7-F9)
(not (NOT-NO-ACCESS-P7-F9))
)
)
(:action BLOCK-ACCESS-P7-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F8)
)
:effect
(and
(NO-ACCESS-P7-F8)
(not (NOT-NO-ACCESS-P7-F8))
)
)
(:action BLOCK-ACCESS-P7-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F7)
)
:effect
(and
(NO-ACCESS-P7-F7)
(not (NOT-NO-ACCESS-P7-F7))
)
)
(:action BLOCK-ACCESS-P7-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F6)
)
:effect
(and
(NO-ACCESS-P7-F6)
(not (NOT-NO-ACCESS-P7-F6))
)
)
(:action BLOCK-ACCESS-P7-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F5)
)
:effect
(and
(NO-ACCESS-P7-F5)
(not (NOT-NO-ACCESS-P7-F5))
)
)
(:action BLOCK-ACCESS-P7-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F4)
)
:effect
(and
(NO-ACCESS-P7-F4)
(not (NOT-NO-ACCESS-P7-F4))
)
)
(:action BLOCK-ACCESS-P7-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F3)
)
:effect
(and
(NO-ACCESS-P7-F3)
(not (NOT-NO-ACCESS-P7-F3))
)
)
(:action BLOCK-ACCESS-P7-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F2)
)
:effect
(and
(NO-ACCESS-P7-F2)
(not (NOT-NO-ACCESS-P7-F2))
)
)
(:action BLOCK-ACCESS-P7-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F1)
)
:effect
(and
(NO-ACCESS-P7-F1)
(not (NOT-NO-ACCESS-P7-F1))
)
)
(:action BLOCK-ACCESS-P7-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P7-F0)
)
:effect
(and
(NO-ACCESS-P7-F0)
(not (NOT-NO-ACCESS-P7-F0))
)
)
(:action BLOCK-ACCESS-P6-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F25)
)
:effect
(and
(NO-ACCESS-P6-F25)
(not (NOT-NO-ACCESS-P6-F25))
)
)
(:action BLOCK-ACCESS-P6-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F24)
)
:effect
(and
(NO-ACCESS-P6-F24)
(not (NOT-NO-ACCESS-P6-F24))
)
)
(:action BLOCK-ACCESS-P6-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F23)
)
:effect
(and
(NO-ACCESS-P6-F23)
(not (NOT-NO-ACCESS-P6-F23))
)
)
(:action BLOCK-ACCESS-P6-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F22)
)
:effect
(and
(NO-ACCESS-P6-F22)
(not (NOT-NO-ACCESS-P6-F22))
)
)
(:action BLOCK-ACCESS-P6-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F21)
)
:effect
(and
(NO-ACCESS-P6-F21)
(not (NOT-NO-ACCESS-P6-F21))
)
)
(:action BLOCK-ACCESS-P6-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F20)
)
:effect
(and
(NO-ACCESS-P6-F20)
(not (NOT-NO-ACCESS-P6-F20))
)
)
(:action BLOCK-ACCESS-P6-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F19)
)
:effect
(and
(NO-ACCESS-P6-F19)
(not (NOT-NO-ACCESS-P6-F19))
)
)
(:action BLOCK-ACCESS-P6-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F18)
)
:effect
(and
(NO-ACCESS-P6-F18)
(not (NOT-NO-ACCESS-P6-F18))
)
)
(:action BLOCK-ACCESS-P6-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F17)
)
:effect
(and
(NO-ACCESS-P6-F17)
(not (NOT-NO-ACCESS-P6-F17))
)
)
(:action BLOCK-ACCESS-P6-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F16)
)
:effect
(and
(NO-ACCESS-P6-F16)
(not (NOT-NO-ACCESS-P6-F16))
)
)
(:action BLOCK-ACCESS-P6-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F15)
)
:effect
(and
(NO-ACCESS-P6-F15)
(not (NOT-NO-ACCESS-P6-F15))
)
)
(:action BLOCK-ACCESS-P6-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F14)
)
:effect
(and
(NO-ACCESS-P6-F14)
(not (NOT-NO-ACCESS-P6-F14))
)
)
(:action BLOCK-ACCESS-P6-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F13)
)
:effect
(and
(NO-ACCESS-P6-F13)
(not (NOT-NO-ACCESS-P6-F13))
)
)
(:action BLOCK-ACCESS-P6-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F12)
)
:effect
(and
(NO-ACCESS-P6-F12)
(not (NOT-NO-ACCESS-P6-F12))
)
)
(:action BLOCK-ACCESS-P6-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F11)
)
:effect
(and
(NO-ACCESS-P6-F11)
(not (NOT-NO-ACCESS-P6-F11))
)
)
(:action BLOCK-ACCESS-P6-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F10)
)
:effect
(and
(NO-ACCESS-P6-F10)
(not (NOT-NO-ACCESS-P6-F10))
)
)
(:action BLOCK-ACCESS-P6-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F9)
)
:effect
(and
(NO-ACCESS-P6-F9)
(not (NOT-NO-ACCESS-P6-F9))
)
)
(:action BLOCK-ACCESS-P6-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F8)
)
:effect
(and
(NO-ACCESS-P6-F8)
(not (NOT-NO-ACCESS-P6-F8))
)
)
(:action BLOCK-ACCESS-P6-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F7)
)
:effect
(and
(NO-ACCESS-P6-F7)
(not (NOT-NO-ACCESS-P6-F7))
)
)
(:action BLOCK-ACCESS-P6-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F6)
)
:effect
(and
(NO-ACCESS-P6-F6)
(not (NOT-NO-ACCESS-P6-F6))
)
)
(:action BLOCK-ACCESS-P6-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F5)
)
:effect
(and
(NO-ACCESS-P6-F5)
(not (NOT-NO-ACCESS-P6-F5))
)
)
(:action BLOCK-ACCESS-P6-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F4)
)
:effect
(and
(NO-ACCESS-P6-F4)
(not (NOT-NO-ACCESS-P6-F4))
)
)
(:action BLOCK-ACCESS-P6-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F3)
)
:effect
(and
(NO-ACCESS-P6-F3)
(not (NOT-NO-ACCESS-P6-F3))
)
)
(:action BLOCK-ACCESS-P6-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F2)
)
:effect
(and
(NO-ACCESS-P6-F2)
(not (NOT-NO-ACCESS-P6-F2))
)
)
(:action BLOCK-ACCESS-P6-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F1)
)
:effect
(and
(NO-ACCESS-P6-F1)
(not (NOT-NO-ACCESS-P6-F1))
)
)
(:action BLOCK-ACCESS-P6-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P6-F0)
)
:effect
(and
(NO-ACCESS-P6-F0)
(not (NOT-NO-ACCESS-P6-F0))
)
)
(:action BLOCK-ACCESS-P5-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F25)
)
:effect
(and
(NO-ACCESS-P5-F25)
(not (NOT-NO-ACCESS-P5-F25))
)
)
(:action BLOCK-ACCESS-P5-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F24)
)
:effect
(and
(NO-ACCESS-P5-F24)
(not (NOT-NO-ACCESS-P5-F24))
)
)
(:action BLOCK-ACCESS-P5-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F23)
)
:effect
(and
(NO-ACCESS-P5-F23)
(not (NOT-NO-ACCESS-P5-F23))
)
)
(:action BLOCK-ACCESS-P5-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F22)
)
:effect
(and
(NO-ACCESS-P5-F22)
(not (NOT-NO-ACCESS-P5-F22))
)
)
(:action BLOCK-ACCESS-P5-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F21)
)
:effect
(and
(NO-ACCESS-P5-F21)
(not (NOT-NO-ACCESS-P5-F21))
)
)
(:action BLOCK-ACCESS-P5-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F20)
)
:effect
(and
(NO-ACCESS-P5-F20)
(not (NOT-NO-ACCESS-P5-F20))
)
)
(:action BLOCK-ACCESS-P5-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F19)
)
:effect
(and
(NO-ACCESS-P5-F19)
(not (NOT-NO-ACCESS-P5-F19))
)
)
(:action BLOCK-ACCESS-P5-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F18)
)
:effect
(and
(NO-ACCESS-P5-F18)
(not (NOT-NO-ACCESS-P5-F18))
)
)
(:action BLOCK-ACCESS-P5-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F17)
)
:effect
(and
(NO-ACCESS-P5-F17)
(not (NOT-NO-ACCESS-P5-F17))
)
)
(:action BLOCK-ACCESS-P5-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F16)
)
:effect
(and
(NO-ACCESS-P5-F16)
(not (NOT-NO-ACCESS-P5-F16))
)
)
(:action BLOCK-ACCESS-P5-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F15)
)
:effect
(and
(NO-ACCESS-P5-F15)
(not (NOT-NO-ACCESS-P5-F15))
)
)
(:action BLOCK-ACCESS-P5-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F14)
)
:effect
(and
(NO-ACCESS-P5-F14)
(not (NOT-NO-ACCESS-P5-F14))
)
)
(:action BLOCK-ACCESS-P5-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F13)
)
:effect
(and
(NO-ACCESS-P5-F13)
(not (NOT-NO-ACCESS-P5-F13))
)
)
(:action BLOCK-ACCESS-P5-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F12)
)
:effect
(and
(NO-ACCESS-P5-F12)
(not (NOT-NO-ACCESS-P5-F12))
)
)
(:action BLOCK-ACCESS-P5-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F11)
)
:effect
(and
(NO-ACCESS-P5-F11)
(not (NOT-NO-ACCESS-P5-F11))
)
)
(:action BLOCK-ACCESS-P5-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F10)
)
:effect
(and
(NO-ACCESS-P5-F10)
(not (NOT-NO-ACCESS-P5-F10))
)
)
(:action BLOCK-ACCESS-P5-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F9)
)
:effect
(and
(NO-ACCESS-P5-F9)
(not (NOT-NO-ACCESS-P5-F9))
)
)
(:action BLOCK-ACCESS-P5-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F8)
)
:effect
(and
(NO-ACCESS-P5-F8)
(not (NOT-NO-ACCESS-P5-F8))
)
)
(:action BLOCK-ACCESS-P5-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F7)
)
:effect
(and
(NO-ACCESS-P5-F7)
(not (NOT-NO-ACCESS-P5-F7))
)
)
(:action BLOCK-ACCESS-P5-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F6)
)
:effect
(and
(NO-ACCESS-P5-F6)
(not (NOT-NO-ACCESS-P5-F6))
)
)
(:action BLOCK-ACCESS-P5-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F5)
)
:effect
(and
(NO-ACCESS-P5-F5)
(not (NOT-NO-ACCESS-P5-F5))
)
)
(:action BLOCK-ACCESS-P5-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F4)
)
:effect
(and
(NO-ACCESS-P5-F4)
(not (NOT-NO-ACCESS-P5-F4))
)
)
(:action BLOCK-ACCESS-P5-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F3)
)
:effect
(and
(NO-ACCESS-P5-F3)
(not (NOT-NO-ACCESS-P5-F3))
)
)
(:action BLOCK-ACCESS-P5-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F2)
)
:effect
(and
(NO-ACCESS-P5-F2)
(not (NOT-NO-ACCESS-P5-F2))
)
)
(:action BLOCK-ACCESS-P5-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F1)
)
:effect
(and
(NO-ACCESS-P5-F1)
(not (NOT-NO-ACCESS-P5-F1))
)
)
(:action BLOCK-ACCESS-P5-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P5-F0)
)
:effect
(and
(NO-ACCESS-P5-F0)
(not (NOT-NO-ACCESS-P5-F0))
)
)
(:action BLOCK-ACCESS-P4-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F25)
)
:effect
(and
(NO-ACCESS-P4-F25)
(not (NOT-NO-ACCESS-P4-F25))
)
)
(:action BLOCK-ACCESS-P4-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F24)
)
:effect
(and
(NO-ACCESS-P4-F24)
(not (NOT-NO-ACCESS-P4-F24))
)
)
(:action BLOCK-ACCESS-P4-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F23)
)
:effect
(and
(NO-ACCESS-P4-F23)
(not (NOT-NO-ACCESS-P4-F23))
)
)
(:action BLOCK-ACCESS-P4-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F22)
)
:effect
(and
(NO-ACCESS-P4-F22)
(not (NOT-NO-ACCESS-P4-F22))
)
)
(:action BLOCK-ACCESS-P4-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F21)
)
:effect
(and
(NO-ACCESS-P4-F21)
(not (NOT-NO-ACCESS-P4-F21))
)
)
(:action BLOCK-ACCESS-P4-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F20)
)
:effect
(and
(NO-ACCESS-P4-F20)
(not (NOT-NO-ACCESS-P4-F20))
)
)
(:action BLOCK-ACCESS-P4-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F19)
)
:effect
(and
(NO-ACCESS-P4-F19)
(not (NOT-NO-ACCESS-P4-F19))
)
)
(:action BLOCK-ACCESS-P4-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F18)
)
:effect
(and
(NO-ACCESS-P4-F18)
(not (NOT-NO-ACCESS-P4-F18))
)
)
(:action BLOCK-ACCESS-P4-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F17)
)
:effect
(and
(NO-ACCESS-P4-F17)
(not (NOT-NO-ACCESS-P4-F17))
)
)
(:action BLOCK-ACCESS-P4-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F16)
)
:effect
(and
(NO-ACCESS-P4-F16)
(not (NOT-NO-ACCESS-P4-F16))
)
)
(:action BLOCK-ACCESS-P4-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F15)
)
:effect
(and
(NO-ACCESS-P4-F15)
(not (NOT-NO-ACCESS-P4-F15))
)
)
(:action BLOCK-ACCESS-P4-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F14)
)
:effect
(and
(NO-ACCESS-P4-F14)
(not (NOT-NO-ACCESS-P4-F14))
)
)
(:action BLOCK-ACCESS-P4-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F13)
)
:effect
(and
(NO-ACCESS-P4-F13)
(not (NOT-NO-ACCESS-P4-F13))
)
)
(:action BLOCK-ACCESS-P4-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F12)
)
:effect
(and
(NO-ACCESS-P4-F12)
(not (NOT-NO-ACCESS-P4-F12))
)
)
(:action BLOCK-ACCESS-P4-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F11)
)
:effect
(and
(NO-ACCESS-P4-F11)
(not (NOT-NO-ACCESS-P4-F11))
)
)
(:action BLOCK-ACCESS-P4-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F10)
)
:effect
(and
(NO-ACCESS-P4-F10)
(not (NOT-NO-ACCESS-P4-F10))
)
)
(:action BLOCK-ACCESS-P4-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F9)
)
:effect
(and
(NO-ACCESS-P4-F9)
(not (NOT-NO-ACCESS-P4-F9))
)
)
(:action BLOCK-ACCESS-P4-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F8)
)
:effect
(and
(NO-ACCESS-P4-F8)
(not (NOT-NO-ACCESS-P4-F8))
)
)
(:action BLOCK-ACCESS-P4-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F7)
)
:effect
(and
(NO-ACCESS-P4-F7)
(not (NOT-NO-ACCESS-P4-F7))
)
)
(:action BLOCK-ACCESS-P4-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F6)
)
:effect
(and
(NO-ACCESS-P4-F6)
(not (NOT-NO-ACCESS-P4-F6))
)
)
(:action BLOCK-ACCESS-P4-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F5)
)
:effect
(and
(NO-ACCESS-P4-F5)
(not (NOT-NO-ACCESS-P4-F5))
)
)
(:action BLOCK-ACCESS-P4-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F4)
)
:effect
(and
(NO-ACCESS-P4-F4)
(not (NOT-NO-ACCESS-P4-F4))
)
)
(:action BLOCK-ACCESS-P4-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F3)
)
:effect
(and
(NO-ACCESS-P4-F3)
(not (NOT-NO-ACCESS-P4-F3))
)
)
(:action BLOCK-ACCESS-P4-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F2)
)
:effect
(and
(NO-ACCESS-P4-F2)
(not (NOT-NO-ACCESS-P4-F2))
)
)
(:action BLOCK-ACCESS-P4-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F1)
)
:effect
(and
(NO-ACCESS-P4-F1)
(not (NOT-NO-ACCESS-P4-F1))
)
)
(:action BLOCK-ACCESS-P4-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P4-F0)
)
:effect
(and
(NO-ACCESS-P4-F0)
(not (NOT-NO-ACCESS-P4-F0))
)
)
(:action BLOCK-ACCESS-P3-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F25)
)
:effect
(and
(NO-ACCESS-P3-F25)
(not (NOT-NO-ACCESS-P3-F25))
)
)
(:action BLOCK-ACCESS-P3-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F24)
)
:effect
(and
(NO-ACCESS-P3-F24)
(not (NOT-NO-ACCESS-P3-F24))
)
)
(:action BLOCK-ACCESS-P3-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F23)
)
:effect
(and
(NO-ACCESS-P3-F23)
(not (NOT-NO-ACCESS-P3-F23))
)
)
(:action BLOCK-ACCESS-P3-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F22)
)
:effect
(and
(NO-ACCESS-P3-F22)
(not (NOT-NO-ACCESS-P3-F22))
)
)
(:action BLOCK-ACCESS-P3-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F21)
)
:effect
(and
(NO-ACCESS-P3-F21)
(not (NOT-NO-ACCESS-P3-F21))
)
)
(:action BLOCK-ACCESS-P3-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F20)
)
:effect
(and
(NO-ACCESS-P3-F20)
(not (NOT-NO-ACCESS-P3-F20))
)
)
(:action BLOCK-ACCESS-P3-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F19)
)
:effect
(and
(NO-ACCESS-P3-F19)
(not (NOT-NO-ACCESS-P3-F19))
)
)
(:action BLOCK-ACCESS-P3-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F18)
)
:effect
(and
(NO-ACCESS-P3-F18)
(not (NOT-NO-ACCESS-P3-F18))
)
)
(:action BLOCK-ACCESS-P3-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F16)
)
:effect
(and
(NO-ACCESS-P3-F16)
(not (NOT-NO-ACCESS-P3-F16))
)
)
(:action BLOCK-ACCESS-P3-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F15)
)
:effect
(and
(NO-ACCESS-P3-F15)
(not (NOT-NO-ACCESS-P3-F15))
)
)
(:action BLOCK-ACCESS-P3-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F14)
)
:effect
(and
(NO-ACCESS-P3-F14)
(not (NOT-NO-ACCESS-P3-F14))
)
)
(:action BLOCK-ACCESS-P3-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F13)
)
:effect
(and
(NO-ACCESS-P3-F13)
(not (NOT-NO-ACCESS-P3-F13))
)
)
(:action BLOCK-ACCESS-P3-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F12)
)
:effect
(and
(NO-ACCESS-P3-F12)
(not (NOT-NO-ACCESS-P3-F12))
)
)
(:action BLOCK-ACCESS-P3-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F11)
)
:effect
(and
(NO-ACCESS-P3-F11)
(not (NOT-NO-ACCESS-P3-F11))
)
)
(:action BLOCK-ACCESS-P3-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F9)
)
:effect
(and
(NO-ACCESS-P3-F9)
(not (NOT-NO-ACCESS-P3-F9))
)
)
(:action BLOCK-ACCESS-P3-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F8)
)
:effect
(and
(NO-ACCESS-P3-F8)
(not (NOT-NO-ACCESS-P3-F8))
)
)
(:action BLOCK-ACCESS-P3-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F7)
)
:effect
(and
(NO-ACCESS-P3-F7)
(not (NOT-NO-ACCESS-P3-F7))
)
)
(:action BLOCK-ACCESS-P3-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F6)
)
:effect
(and
(NO-ACCESS-P3-F6)
(not (NOT-NO-ACCESS-P3-F6))
)
)
(:action BLOCK-ACCESS-P3-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F5)
)
:effect
(and
(NO-ACCESS-P3-F5)
(not (NOT-NO-ACCESS-P3-F5))
)
)
(:action BLOCK-ACCESS-P3-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F4)
)
:effect
(and
(NO-ACCESS-P3-F4)
(not (NOT-NO-ACCESS-P3-F4))
)
)
(:action BLOCK-ACCESS-P3-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F3)
)
:effect
(and
(NO-ACCESS-P3-F3)
(not (NOT-NO-ACCESS-P3-F3))
)
)
(:action BLOCK-ACCESS-P3-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F2)
)
:effect
(and
(NO-ACCESS-P3-F2)
(not (NOT-NO-ACCESS-P3-F2))
)
)
(:action BLOCK-ACCESS-P3-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F1)
)
:effect
(and
(NO-ACCESS-P3-F1)
(not (NOT-NO-ACCESS-P3-F1))
)
)
(:action BLOCK-ACCESS-P3-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P3-F0)
)
:effect
(and
(NO-ACCESS-P3-F0)
(not (NOT-NO-ACCESS-P3-F0))
)
)
(:action BLOCK-ACCESS-P2-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F24)
)
:effect
(and
(NO-ACCESS-P2-F24)
(not (NOT-NO-ACCESS-P2-F24))
)
)
(:action BLOCK-ACCESS-P2-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F23)
)
:effect
(and
(NO-ACCESS-P2-F23)
(not (NOT-NO-ACCESS-P2-F23))
)
)
(:action BLOCK-ACCESS-P2-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F22)
)
:effect
(and
(NO-ACCESS-P2-F22)
(not (NOT-NO-ACCESS-P2-F22))
)
)
(:action BLOCK-ACCESS-P2-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F21)
)
:effect
(and
(NO-ACCESS-P2-F21)
(not (NOT-NO-ACCESS-P2-F21))
)
)
(:action BLOCK-ACCESS-P2-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F19)
)
:effect
(and
(NO-ACCESS-P2-F19)
(not (NOT-NO-ACCESS-P2-F19))
)
)
(:action BLOCK-ACCESS-P2-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F17)
)
:effect
(and
(NO-ACCESS-P2-F17)
(not (NOT-NO-ACCESS-P2-F17))
)
)
(:action BLOCK-ACCESS-P2-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F16)
)
:effect
(and
(NO-ACCESS-P2-F16)
(not (NOT-NO-ACCESS-P2-F16))
)
)
(:action BLOCK-ACCESS-P2-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F15)
)
:effect
(and
(NO-ACCESS-P2-F15)
(not (NOT-NO-ACCESS-P2-F15))
)
)
(:action BLOCK-ACCESS-P2-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F14)
)
:effect
(and
(NO-ACCESS-P2-F14)
(not (NOT-NO-ACCESS-P2-F14))
)
)
(:action BLOCK-ACCESS-P2-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F13)
)
:effect
(and
(NO-ACCESS-P2-F13)
(not (NOT-NO-ACCESS-P2-F13))
)
)
(:action BLOCK-ACCESS-P2-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F12)
)
:effect
(and
(NO-ACCESS-P2-F12)
(not (NOT-NO-ACCESS-P2-F12))
)
)
(:action BLOCK-ACCESS-P2-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F11)
)
:effect
(and
(NO-ACCESS-P2-F11)
(not (NOT-NO-ACCESS-P2-F11))
)
)
(:action BLOCK-ACCESS-P2-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F10)
)
:effect
(and
(NO-ACCESS-P2-F10)
(not (NOT-NO-ACCESS-P2-F10))
)
)
(:action BLOCK-ACCESS-P2-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F9)
)
:effect
(and
(NO-ACCESS-P2-F9)
(not (NOT-NO-ACCESS-P2-F9))
)
)
(:action BLOCK-ACCESS-P2-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F8)
)
:effect
(and
(NO-ACCESS-P2-F8)
(not (NOT-NO-ACCESS-P2-F8))
)
)
(:action BLOCK-ACCESS-P2-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F7)
)
:effect
(and
(NO-ACCESS-P2-F7)
(not (NOT-NO-ACCESS-P2-F7))
)
)
(:action BLOCK-ACCESS-P2-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F6)
)
:effect
(and
(NO-ACCESS-P2-F6)
(not (NOT-NO-ACCESS-P2-F6))
)
)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
)
)
(:action BLOCK-ACCESS-P1-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F25)
)
:effect
(and
(NO-ACCESS-P1-F25)
(not (NOT-NO-ACCESS-P1-F25))
)
)
(:action BLOCK-ACCESS-P1-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F24)
)
:effect
(and
(NO-ACCESS-P1-F24)
(not (NOT-NO-ACCESS-P1-F24))
)
)
(:action BLOCK-ACCESS-P1-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F23)
)
:effect
(and
(NO-ACCESS-P1-F23)
(not (NOT-NO-ACCESS-P1-F23))
)
)
(:action BLOCK-ACCESS-P1-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F22)
)
:effect
(and
(NO-ACCESS-P1-F22)
(not (NOT-NO-ACCESS-P1-F22))
)
)
(:action BLOCK-ACCESS-P1-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F21)
)
:effect
(and
(NO-ACCESS-P1-F21)
(not (NOT-NO-ACCESS-P1-F21))
)
)
(:action BLOCK-ACCESS-P1-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F20)
)
:effect
(and
(NO-ACCESS-P1-F20)
(not (NOT-NO-ACCESS-P1-F20))
)
)
(:action BLOCK-ACCESS-P1-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F19)
)
:effect
(and
(NO-ACCESS-P1-F19)
(not (NOT-NO-ACCESS-P1-F19))
)
)
(:action BLOCK-ACCESS-P1-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F18)
)
:effect
(and
(NO-ACCESS-P1-F18)
(not (NOT-NO-ACCESS-P1-F18))
)
)
(:action BLOCK-ACCESS-P1-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F17)
)
:effect
(and
(NO-ACCESS-P1-F17)
(not (NOT-NO-ACCESS-P1-F17))
)
)
(:action BLOCK-ACCESS-P1-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F16)
)
:effect
(and
(NO-ACCESS-P1-F16)
(not (NOT-NO-ACCESS-P1-F16))
)
)
(:action BLOCK-ACCESS-P1-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F15)
)
:effect
(and
(NO-ACCESS-P1-F15)
(not (NOT-NO-ACCESS-P1-F15))
)
)
(:action BLOCK-ACCESS-P1-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F14)
)
:effect
(and
(NO-ACCESS-P1-F14)
(not (NOT-NO-ACCESS-P1-F14))
)
)
(:action BLOCK-ACCESS-P1-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F13)
)
:effect
(and
(NO-ACCESS-P1-F13)
(not (NOT-NO-ACCESS-P1-F13))
)
)
(:action BLOCK-ACCESS-P1-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F12)
)
:effect
(and
(NO-ACCESS-P1-F12)
(not (NOT-NO-ACCESS-P1-F12))
)
)
(:action BLOCK-ACCESS-P1-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F11)
)
:effect
(and
(NO-ACCESS-P1-F11)
(not (NOT-NO-ACCESS-P1-F11))
)
)
(:action BLOCK-ACCESS-P1-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F10)
)
:effect
(and
(NO-ACCESS-P1-F10)
(not (NOT-NO-ACCESS-P1-F10))
)
)
(:action BLOCK-ACCESS-P1-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F9)
)
:effect
(and
(NO-ACCESS-P1-F9)
(not (NOT-NO-ACCESS-P1-F9))
)
)
(:action BLOCK-ACCESS-P1-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F8)
)
:effect
(and
(NO-ACCESS-P1-F8)
(not (NOT-NO-ACCESS-P1-F8))
)
)
(:action BLOCK-ACCESS-P1-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F7)
)
:effect
(and
(NO-ACCESS-P1-F7)
(not (NOT-NO-ACCESS-P1-F7))
)
)
(:action BLOCK-ACCESS-P1-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F6)
)
:effect
(and
(NO-ACCESS-P1-F6)
(not (NOT-NO-ACCESS-P1-F6))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
)
)
(:action BLOCK-ACCESS-P0-F25
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F25)
)
:effect
(and
(NO-ACCESS-P0-F25)
(not (NOT-NO-ACCESS-P0-F25))
)
)
(:action BLOCK-ACCESS-P0-F24
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F24)
)
:effect
(and
(NO-ACCESS-P0-F24)
(not (NOT-NO-ACCESS-P0-F24))
)
)
(:action BLOCK-ACCESS-P0-F23
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F23)
)
:effect
(and
(NO-ACCESS-P0-F23)
(not (NOT-NO-ACCESS-P0-F23))
)
)
(:action BLOCK-ACCESS-P0-F22
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F22)
)
:effect
(and
(NO-ACCESS-P0-F22)
(not (NOT-NO-ACCESS-P0-F22))
)
)
(:action BLOCK-ACCESS-P0-F21
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F21)
)
:effect
(and
(NO-ACCESS-P0-F21)
(not (NOT-NO-ACCESS-P0-F21))
)
)
(:action BLOCK-ACCESS-P0-F20
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F20)
)
:effect
(and
(NO-ACCESS-P0-F20)
(not (NOT-NO-ACCESS-P0-F20))
)
)
(:action BLOCK-ACCESS-P0-F19
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F19)
)
:effect
(and
(NO-ACCESS-P0-F19)
(not (NOT-NO-ACCESS-P0-F19))
)
)
(:action BLOCK-ACCESS-P0-F18
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F18)
)
:effect
(and
(NO-ACCESS-P0-F18)
(not (NOT-NO-ACCESS-P0-F18))
)
)
(:action BLOCK-ACCESS-P0-F17
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F17)
)
:effect
(and
(NO-ACCESS-P0-F17)
(not (NOT-NO-ACCESS-P0-F17))
)
)
(:action BLOCK-ACCESS-P0-F16
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F16)
)
:effect
(and
(NO-ACCESS-P0-F16)
(not (NOT-NO-ACCESS-P0-F16))
)
)
(:action BLOCK-ACCESS-P0-F15
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F15)
)
:effect
(and
(NO-ACCESS-P0-F15)
(not (NOT-NO-ACCESS-P0-F15))
)
)
(:action BLOCK-ACCESS-P0-F14
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F14)
)
:effect
(and
(NO-ACCESS-P0-F14)
(not (NOT-NO-ACCESS-P0-F14))
)
)
(:action BLOCK-ACCESS-P0-F13
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F13)
)
:effect
(and
(NO-ACCESS-P0-F13)
(not (NOT-NO-ACCESS-P0-F13))
)
)
(:action BLOCK-ACCESS-P0-F12
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F12)
)
:effect
(and
(NO-ACCESS-P0-F12)
(not (NOT-NO-ACCESS-P0-F12))
)
)
(:action BLOCK-ACCESS-P0-F11
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F11)
)
:effect
(and
(NO-ACCESS-P0-F11)
(not (NOT-NO-ACCESS-P0-F11))
)
)
(:action BLOCK-ACCESS-P0-F10
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F10)
)
:effect
(and
(NO-ACCESS-P0-F10)
(not (NOT-NO-ACCESS-P0-F10))
)
)
(:action BLOCK-ACCESS-P0-F9
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F9)
)
:effect
(and
(NO-ACCESS-P0-F9)
(not (NOT-NO-ACCESS-P0-F9))
)
)
(:action BLOCK-ACCESS-P0-F8
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F8)
)
:effect
(and
(NO-ACCESS-P0-F8)
(not (NOT-NO-ACCESS-P0-F8))
)
)
(:action BLOCK-ACCESS-P0-F7
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F7)
)
:effect
(and
(NO-ACCESS-P0-F7)
(not (NOT-NO-ACCESS-P0-F7))
)
)
(:action BLOCK-ACCESS-P0-F6
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F6)
)
:effect
(and
(NO-ACCESS-P0-F6)
(not (NOT-NO-ACCESS-P0-F6))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F0
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F0
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F0
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F0
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F0
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F0
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F0
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F0
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F0
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F0
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F0
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F0
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F0
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F0
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F0
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F0
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F0
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F0
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F0
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F0
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F1
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F1
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F1
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F1
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F1
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F1
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F1
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F1
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F1
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F1
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F1
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F1
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F1
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F1
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F1
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F1
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F1
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F1
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F1
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F1
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F2
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F2
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F2
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F2
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F2
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F2
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F2
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F2
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F2
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F2
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F2
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F2
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F2
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F2
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F2
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F2
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F2
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F2
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F2
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F2
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F3
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F3
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F3
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F3
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F3
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F3
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F3
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F3
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F3
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F3
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F3
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F3
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F3
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F3
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F3
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F3
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F3
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F3
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F3
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F3
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
)
)
(:action DOWN-F6-F4
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F4
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F4
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F4
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F4
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F4
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F4
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F4
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F4
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F4
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F4
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F4
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F4
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F4
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F4
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F4
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F4
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F4
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F4
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F4
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F6-F5
:parameters ()
:precondition
(and
(LIFT-AT-F6)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F6))
)
)
(:action DOWN-F7-F5
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F5
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F5
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F5
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F5
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F5
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F5
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F5
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F5
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F5
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F5
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F5
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F5
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F5
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F5
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F5
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F5
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F5
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F5
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F7-F6
:parameters ()
:precondition
(and
(LIFT-AT-F7)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F7))
)
)
(:action DOWN-F8-F6
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F6
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F6
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F6
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F6
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F6
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F6
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F6
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F6
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F6
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F6
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F6
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F6
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F6
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F6
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F6
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F6
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F6
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F8-F7
:parameters ()
:precondition
(and
(LIFT-AT-F8)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F8))
)
)
(:action DOWN-F9-F7
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F7
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F7
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F7
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F7
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F7
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F7
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F7
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F7
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F7
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F7
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F7
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F7
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F7
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F7
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F7
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F7
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F9-F8
:parameters ()
:precondition
(and
(LIFT-AT-F9)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F9))
)
)
(:action DOWN-F10-F8
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F8
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F8
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F8
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F8
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F8
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F8
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F8
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F8
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F8
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F8
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F8
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F8
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F8
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F8
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F8
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F10-F9
:parameters ()
:precondition
(and
(LIFT-AT-F10)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F10))
)
)
(:action DOWN-F11-F9
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F9
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F9
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F9
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F9
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F9
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F9
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F9
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F9
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F9
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F9
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F9
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F9
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F9
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F9
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F11-F10
:parameters ()
:precondition
(and
(LIFT-AT-F11)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F11))
)
)
(:action DOWN-F12-F10
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F10
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F10
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F10
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F10
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F10
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F10
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F10
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F10
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F10
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F10
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F10
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F10
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F10
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F12-F11
:parameters ()
:precondition
(and
(LIFT-AT-F12)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F12))
)
)
(:action DOWN-F13-F11
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F11
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F11
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F11
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F11
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F11
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F11
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F11
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F11
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F11
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F11
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F11
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F11
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F13-F12
:parameters ()
:precondition
(and
(LIFT-AT-F13)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F13))
)
)
(:action DOWN-F14-F12
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F12
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F12
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F12
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F12
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F12
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F12
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F12
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F12
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F12
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F12
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F12
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F14-F13
:parameters ()
:precondition
(and
(LIFT-AT-F14)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F14))
)
)
(:action DOWN-F15-F13
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F13
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F13
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F13
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F13
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F13
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F13
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F13
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F13
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F13
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F13
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F15-F14
:parameters ()
:precondition
(and
(LIFT-AT-F15)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F15))
)
)
(:action DOWN-F16-F14
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F14
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F14
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F14
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F14
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F14
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F14
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F14
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F14
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F14
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F16-F15
:parameters ()
:precondition
(and
(LIFT-AT-F16)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F16))
)
)
(:action DOWN-F17-F15
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F15
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F15
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F15
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F15
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F15
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F15
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F15
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F15
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F17-F16
:parameters ()
:precondition
(and
(LIFT-AT-F17)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F17))
)
)
(:action DOWN-F18-F16
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F16
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F16
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F16
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F16
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F16
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F16
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F16
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F18-F17
:parameters ()
:precondition
(and
(LIFT-AT-F18)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F18))
)
)
(:action DOWN-F19-F17
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F17
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F17
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F17
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F17
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F17
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F17
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F19-F18
:parameters ()
:precondition
(and
(LIFT-AT-F19)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F19))
)
)
(:action DOWN-F20-F18
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F18
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F18
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F18
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F18
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F18
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F20-F19
:parameters ()
:precondition
(and
(LIFT-AT-F20)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F20))
)
)
(:action DOWN-F21-F19
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F19
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F19
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F19
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F19
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F21-F20
:parameters ()
:precondition
(and
(LIFT-AT-F21)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F21))
)
)
(:action DOWN-F22-F20
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F20
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F20
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F20
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F22-F21
:parameters ()
:precondition
(and
(LIFT-AT-F22)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F22))
)
)
(:action DOWN-F23-F21
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F21
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F21
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F23-F22
:parameters ()
:precondition
(and
(LIFT-AT-F23)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F23))
)
)
(:action DOWN-F24-F22
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F22
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F24-F23
:parameters ()
:precondition
(and
(LIFT-AT-F24)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F24))
)
)
(:action DOWN-F25-F23
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F25))
)
)
(:action DOWN-F25-F24
:parameters ()
:precondition
(and
(LIFT-AT-F25)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F25))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F6
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F6)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F7
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F7)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F8
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F8)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F9
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F9)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F10
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F10)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F11
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F11)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F12
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F12)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F13
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F13)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F14
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F14)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F15
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F15)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F16
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F16)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F17
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F17)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F18
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F18)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F19
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F19)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F20
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F20)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F21
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F21)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F22
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F22)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F23
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F23)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F24
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F24)
(not (LIFT-AT-F0))
)
)
(:action UP-F0-F25
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
(NOT-BOARDED-P9)
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F25)
(not (LIFT-AT-F0))
)
)
(:derived (Flag2-)(and (NOT-BOARDED-P12)(NOT-BOARDED-P4)))

(:derived (Flag3-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag6-)(and (NOT-BOARDED-P11)))

(:derived (Flag7-)(and (NOT-BOARDED-P12)))

(:derived (Flag7-)(and (NOT-NO-ACCESS-P12-F25)))

(:derived (Flag8-)(and (NOT-BOARDED-P11)))

(:derived (Flag8-)(and (NOT-NO-ACCESS-P11-F25)))

(:derived (Flag9-)(and (NOT-BOARDED-P10)))

(:derived (Flag9-)(and (NOT-NO-ACCESS-P10-F25)))

(:derived (Flag10-)(and (NOT-BOARDED-P9)))

(:derived (Flag10-)(and (NOT-NO-ACCESS-P9-F25)))

(:derived (Flag11-)(and (NOT-BOARDED-P8)))

(:derived (Flag11-)(and (NOT-NO-ACCESS-P8-F25)))

(:derived (Flag12-)(and (NOT-BOARDED-P7)))

(:derived (Flag12-)(and (NOT-NO-ACCESS-P7-F25)))

(:derived (Flag13-)(and (NOT-BOARDED-P6)))

(:derived (Flag13-)(and (NOT-NO-ACCESS-P6-F25)))

(:derived (Flag14-)(and (NOT-BOARDED-P5)))

(:derived (Flag14-)(and (NOT-NO-ACCESS-P5-F25)))

(:derived (Flag15-)(and (NOT-BOARDED-P4)))

(:derived (Flag15-)(and (NOT-NO-ACCESS-P4-F25)))

(:derived (Flag16-)(and (NOT-BOARDED-P3)))

(:derived (Flag16-)(and (NOT-NO-ACCESS-P3-F25)))

(:derived (Flag17-)(and (NOT-BOARDED-P1)))

(:derived (Flag17-)(and (NOT-NO-ACCESS-P1-F25)))

(:derived (Flag18-)(and (NOT-BOARDED-P0)))

(:derived (Flag18-)(and (NOT-NO-ACCESS-P0-F25)))

(:derived (Flag21-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P0)))

(:derived (Flag25-)(and (NOT-BOARDED-P11)))

(:derived (Flag25-)(and (NOT-SERVED-P4)))

(:derived (Flag26-)(and (NOT-BOARDED-P12)))

(:derived (Flag26-)(and (NOT-NO-ACCESS-P12-F24)))

(:derived (Flag27-)(and (NOT-BOARDED-P11)))

(:derived (Flag27-)(and (NOT-NO-ACCESS-P11-F24)))

(:derived (Flag28-)(and (NOT-BOARDED-P10)))

(:derived (Flag28-)(and (NOT-NO-ACCESS-P10-F24)))

(:derived (Flag29-)(and (NOT-BOARDED-P9)))

(:derived (Flag29-)(and (NOT-NO-ACCESS-P9-F24)))

(:derived (Flag30-)(and (NOT-BOARDED-P8)))

(:derived (Flag30-)(and (NOT-NO-ACCESS-P8-F24)))

(:derived (Flag31-)(and (NOT-BOARDED-P7)))

(:derived (Flag31-)(and (NOT-NO-ACCESS-P7-F24)))

(:derived (Flag32-)(and (NOT-BOARDED-P6)))

(:derived (Flag32-)(and (NOT-NO-ACCESS-P6-F24)))

(:derived (Flag33-)(and (NOT-BOARDED-P5)))

(:derived (Flag33-)(and (NOT-NO-ACCESS-P5-F24)))

(:derived (Flag34-)(and (NOT-BOARDED-P4)))

(:derived (Flag34-)(and (NOT-NO-ACCESS-P4-F24)))

(:derived (Flag35-)(and (NOT-BOARDED-P3)))

(:derived (Flag35-)(and (NOT-NO-ACCESS-P3-F24)))

(:derived (Flag36-)(and (NOT-BOARDED-P2)))

(:derived (Flag36-)(and (NOT-NO-ACCESS-P2-F24)))

(:derived (Flag37-)(and (NOT-BOARDED-P1)))

(:derived (Flag37-)(and (NOT-NO-ACCESS-P1-F24)))

(:derived (Flag38-)(and (NOT-BOARDED-P0)))

(:derived (Flag38-)(and (NOT-NO-ACCESS-P0-F24)))

(:derived (Flag40-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag43-)(and (NOT-BOARDED-P12)))

(:derived (Flag43-)(and (NOT-NO-ACCESS-P12-F23)))

(:derived (Flag44-)(and (NOT-BOARDED-P11)))

(:derived (Flag44-)(and (NOT-NO-ACCESS-P11-F23)))

(:derived (Flag45-)(and (NOT-BOARDED-P10)))

(:derived (Flag45-)(and (NOT-NO-ACCESS-P10-F23)))

(:derived (Flag46-)(and (NOT-BOARDED-P9)))

(:derived (Flag46-)(and (NOT-NO-ACCESS-P9-F23)))

(:derived (Flag47-)(and (NOT-BOARDED-P8)))

(:derived (Flag47-)(and (NOT-NO-ACCESS-P8-F23)))

(:derived (Flag48-)(and (NOT-BOARDED-P7)))

(:derived (Flag48-)(and (NOT-NO-ACCESS-P7-F23)))

(:derived (Flag49-)(and (NOT-BOARDED-P6)))

(:derived (Flag49-)(and (NOT-NO-ACCESS-P6-F23)))

(:derived (Flag50-)(and (NOT-BOARDED-P5)))

(:derived (Flag50-)(and (NOT-NO-ACCESS-P5-F23)))

(:derived (Flag51-)(and (NOT-BOARDED-P4)))

(:derived (Flag51-)(and (NOT-NO-ACCESS-P4-F23)))

(:derived (Flag52-)(and (NOT-BOARDED-P3)))

(:derived (Flag52-)(and (NOT-NO-ACCESS-P3-F23)))

(:derived (Flag53-)(and (NOT-BOARDED-P2)))

(:derived (Flag53-)(and (NOT-NO-ACCESS-P2-F23)))

(:derived (Flag54-)(and (NOT-BOARDED-P1)))

(:derived (Flag54-)(and (NOT-NO-ACCESS-P1-F23)))

(:derived (Flag55-)(and (NOT-BOARDED-P0)))

(:derived (Flag55-)(and (NOT-NO-ACCESS-P0-F23)))

(:derived (Flag57-)(and (NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag60-)(and (NOT-BOARDED-P12)))

(:derived (Flag60-)(and (NOT-NO-ACCESS-P12-F22)))

(:derived (Flag61-)(and (NOT-BOARDED-P11)))

(:derived (Flag61-)(and (NOT-NO-ACCESS-P11-F22)))

(:derived (Flag62-)(and (NOT-BOARDED-P10)))

(:derived (Flag62-)(and (NOT-NO-ACCESS-P10-F22)))

(:derived (Flag63-)(and (NOT-BOARDED-P9)))

(:derived (Flag63-)(and (NOT-NO-ACCESS-P9-F22)))

(:derived (Flag64-)(and (NOT-BOARDED-P8)))

(:derived (Flag64-)(and (NOT-NO-ACCESS-P8-F22)))

(:derived (Flag65-)(and (NOT-BOARDED-P7)))

(:derived (Flag65-)(and (NOT-NO-ACCESS-P7-F22)))

(:derived (Flag66-)(and (NOT-BOARDED-P6)))

(:derived (Flag66-)(and (NOT-NO-ACCESS-P6-F22)))

(:derived (Flag67-)(and (NOT-BOARDED-P5)))

(:derived (Flag67-)(and (NOT-NO-ACCESS-P5-F22)))

(:derived (Flag68-)(and (NOT-BOARDED-P4)))

(:derived (Flag68-)(and (NOT-NO-ACCESS-P4-F22)))

(:derived (Flag69-)(and (NOT-BOARDED-P3)))

(:derived (Flag69-)(and (NOT-NO-ACCESS-P3-F22)))

(:derived (Flag70-)(and (NOT-BOARDED-P2)))

(:derived (Flag70-)(and (NOT-NO-ACCESS-P2-F22)))

(:derived (Flag71-)(and (NOT-BOARDED-P1)))

(:derived (Flag71-)(and (NOT-NO-ACCESS-P1-F22)))

(:derived (Flag72-)(and (NOT-BOARDED-P0)))

(:derived (Flag72-)(and (NOT-NO-ACCESS-P0-F22)))

(:derived (Flag74-)(and (NOT-BOARDED-P12)))

(:derived (Flag74-)(and (NOT-NO-ACCESS-P12-F21)))

(:derived (Flag75-)(and (NOT-BOARDED-P11)))

(:derived (Flag75-)(and (NOT-NO-ACCESS-P11-F21)))

(:derived (Flag76-)(and (NOT-BOARDED-P10)))

(:derived (Flag76-)(and (NOT-NO-ACCESS-P10-F21)))

(:derived (Flag77-)(and (NOT-BOARDED-P9)))

(:derived (Flag77-)(and (NOT-NO-ACCESS-P9-F21)))

(:derived (Flag78-)(and (NOT-BOARDED-P8)))

(:derived (Flag78-)(and (NOT-NO-ACCESS-P8-F21)))

(:derived (Flag79-)(and (NOT-BOARDED-P7)))

(:derived (Flag79-)(and (NOT-NO-ACCESS-P7-F21)))

(:derived (Flag80-)(and (NOT-BOARDED-P6)))

(:derived (Flag80-)(and (NOT-NO-ACCESS-P6-F21)))

(:derived (Flag81-)(and (NOT-BOARDED-P5)))

(:derived (Flag81-)(and (NOT-NO-ACCESS-P5-F21)))

(:derived (Flag82-)(and (NOT-BOARDED-P4)))

(:derived (Flag82-)(and (NOT-NO-ACCESS-P4-F21)))

(:derived (Flag83-)(and (NOT-BOARDED-P3)))

(:derived (Flag83-)(and (NOT-NO-ACCESS-P3-F21)))

(:derived (Flag84-)(and (NOT-BOARDED-P2)))

(:derived (Flag84-)(and (NOT-NO-ACCESS-P2-F21)))

(:derived (Flag85-)(and (NOT-BOARDED-P1)))

(:derived (Flag85-)(and (NOT-NO-ACCESS-P1-F21)))

(:derived (Flag86-)(and (NOT-BOARDED-P0)))

(:derived (Flag86-)(and (NOT-NO-ACCESS-P0-F21)))

(:derived (Flag88-)(and (NOT-BOARDED-P12)))

(:derived (Flag88-)(and (NOT-NO-ACCESS-P12-F20)))

(:derived (Flag89-)(and (NOT-BOARDED-P11)))

(:derived (Flag89-)(and (NOT-NO-ACCESS-P11-F20)))

(:derived (Flag90-)(and (NOT-BOARDED-P10)))

(:derived (Flag90-)(and (NOT-NO-ACCESS-P10-F20)))

(:derived (Flag91-)(and (NOT-BOARDED-P9)))

(:derived (Flag91-)(and (NOT-NO-ACCESS-P9-F20)))

(:derived (Flag92-)(and (NOT-BOARDED-P8)))

(:derived (Flag92-)(and (NOT-NO-ACCESS-P8-F20)))

(:derived (Flag93-)(and (NOT-BOARDED-P6)))

(:derived (Flag93-)(and (NOT-NO-ACCESS-P6-F20)))

(:derived (Flag94-)(and (NOT-BOARDED-P5)))

(:derived (Flag94-)(and (NOT-NO-ACCESS-P5-F20)))

(:derived (Flag95-)(and (NOT-BOARDED-P4)))

(:derived (Flag95-)(and (NOT-NO-ACCESS-P4-F20)))

(:derived (Flag96-)(and (NOT-BOARDED-P3)))

(:derived (Flag96-)(and (NOT-NO-ACCESS-P3-F20)))

(:derived (Flag97-)(and (NOT-BOARDED-P1)))

(:derived (Flag97-)(and (NOT-NO-ACCESS-P1-F20)))

(:derived (Flag98-)(and (NOT-BOARDED-P0)))

(:derived (Flag98-)(and (NOT-NO-ACCESS-P0-F20)))

(:derived (Flag100-)(and (NOT-BOARDED-P12)))

(:derived (Flag100-)(and (NOT-NO-ACCESS-P12-F19)))

(:derived (Flag101-)(and (NOT-BOARDED-P11)))

(:derived (Flag101-)(and (NOT-NO-ACCESS-P11-F19)))

(:derived (Flag102-)(and (NOT-BOARDED-P10)))

(:derived (Flag102-)(and (NOT-NO-ACCESS-P10-F19)))

(:derived (Flag103-)(and (NOT-BOARDED-P9)))

(:derived (Flag103-)(and (NOT-NO-ACCESS-P9-F19)))

(:derived (Flag104-)(and (NOT-BOARDED-P8)))

(:derived (Flag104-)(and (NOT-NO-ACCESS-P8-F19)))

(:derived (Flag105-)(and (NOT-BOARDED-P7)))

(:derived (Flag105-)(and (NOT-NO-ACCESS-P7-F19)))

(:derived (Flag106-)(and (NOT-BOARDED-P6)))

(:derived (Flag106-)(and (NOT-NO-ACCESS-P6-F19)))

(:derived (Flag107-)(and (NOT-BOARDED-P5)))

(:derived (Flag107-)(and (NOT-NO-ACCESS-P5-F19)))

(:derived (Flag108-)(and (NOT-BOARDED-P4)))

(:derived (Flag108-)(and (NOT-NO-ACCESS-P4-F19)))

(:derived (Flag109-)(and (NOT-BOARDED-P3)))

(:derived (Flag109-)(and (NOT-NO-ACCESS-P3-F19)))

(:derived (Flag110-)(and (NOT-BOARDED-P2)))

(:derived (Flag110-)(and (NOT-NO-ACCESS-P2-F19)))

(:derived (Flag111-)(and (NOT-BOARDED-P1)))

(:derived (Flag111-)(and (NOT-NO-ACCESS-P1-F19)))

(:derived (Flag112-)(and (NOT-BOARDED-P0)))

(:derived (Flag112-)(and (NOT-NO-ACCESS-P0-F19)))

(:derived (Flag114-)(and (NOT-BOARDED-P12)))

(:derived (Flag114-)(and (NOT-NO-ACCESS-P12-F18)))

(:derived (Flag115-)(and (NOT-BOARDED-P11)))

(:derived (Flag115-)(and (NOT-NO-ACCESS-P11-F18)))

(:derived (Flag116-)(and (NOT-BOARDED-P10)))

(:derived (Flag116-)(and (NOT-NO-ACCESS-P10-F18)))

(:derived (Flag117-)(and (NOT-BOARDED-P9)))

(:derived (Flag117-)(and (NOT-NO-ACCESS-P9-F18)))

(:derived (Flag118-)(and (NOT-BOARDED-P8)))

(:derived (Flag118-)(and (NOT-NO-ACCESS-P8-F18)))

(:derived (Flag119-)(and (NOT-BOARDED-P7)))

(:derived (Flag119-)(and (NOT-NO-ACCESS-P7-F18)))

(:derived (Flag120-)(and (NOT-BOARDED-P6)))

(:derived (Flag120-)(and (NOT-NO-ACCESS-P6-F18)))

(:derived (Flag121-)(and (NOT-BOARDED-P5)))

(:derived (Flag121-)(and (NOT-NO-ACCESS-P5-F18)))

(:derived (Flag122-)(and (NOT-BOARDED-P4)))

(:derived (Flag122-)(and (NOT-NO-ACCESS-P4-F18)))

(:derived (Flag123-)(and (NOT-BOARDED-P3)))

(:derived (Flag123-)(and (NOT-NO-ACCESS-P3-F18)))

(:derived (Flag124-)(and (NOT-BOARDED-P1)))

(:derived (Flag124-)(and (NOT-NO-ACCESS-P1-F18)))

(:derived (Flag125-)(and (NOT-BOARDED-P0)))

(:derived (Flag125-)(and (NOT-NO-ACCESS-P0-F18)))

(:derived (Flag131-)(and (NOT-BOARDED-P11)))

(:derived (Flag131-)(and (NOT-SERVED-P0)))

(:derived (Flag132-)(and (NOT-BOARDED-P12)))

(:derived (Flag132-)(and (NOT-NO-ACCESS-P12-F17)))

(:derived (Flag133-)(and (NOT-BOARDED-P11)))

(:derived (Flag133-)(and (NOT-NO-ACCESS-P11-F17)))

(:derived (Flag134-)(and (NOT-BOARDED-P10)))

(:derived (Flag134-)(and (NOT-NO-ACCESS-P10-F17)))

(:derived (Flag135-)(and (NOT-BOARDED-P9)))

(:derived (Flag135-)(and (NOT-NO-ACCESS-P9-F17)))

(:derived (Flag136-)(and (NOT-BOARDED-P8)))

(:derived (Flag136-)(and (NOT-NO-ACCESS-P8-F17)))

(:derived (Flag137-)(and (NOT-BOARDED-P7)))

(:derived (Flag137-)(and (NOT-NO-ACCESS-P7-F17)))

(:derived (Flag138-)(and (NOT-BOARDED-P6)))

(:derived (Flag138-)(and (NOT-NO-ACCESS-P6-F17)))

(:derived (Flag139-)(and (NOT-BOARDED-P5)))

(:derived (Flag139-)(and (NOT-NO-ACCESS-P5-F17)))

(:derived (Flag140-)(and (NOT-BOARDED-P4)))

(:derived (Flag140-)(and (NOT-NO-ACCESS-P4-F17)))

(:derived (Flag141-)(and (NOT-BOARDED-P2)))

(:derived (Flag141-)(and (NOT-NO-ACCESS-P2-F17)))

(:derived (Flag142-)(and (NOT-BOARDED-P1)))

(:derived (Flag142-)(and (NOT-NO-ACCESS-P1-F17)))

(:derived (Flag143-)(and (NOT-BOARDED-P0)))

(:derived (Flag143-)(and (NOT-NO-ACCESS-P0-F17)))

(:derived (Flag145-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag148-)(and (NOT-BOARDED-P12)))

(:derived (Flag148-)(and (NOT-NO-ACCESS-P12-F16)))

(:derived (Flag149-)(and (NOT-BOARDED-P11)))

(:derived (Flag149-)(and (NOT-NO-ACCESS-P11-F16)))

(:derived (Flag150-)(and (NOT-BOARDED-P10)))

(:derived (Flag150-)(and (NOT-NO-ACCESS-P10-F16)))

(:derived (Flag151-)(and (NOT-BOARDED-P9)))

(:derived (Flag151-)(and (NOT-NO-ACCESS-P9-F16)))

(:derived (Flag152-)(and (NOT-BOARDED-P8)))

(:derived (Flag152-)(and (NOT-NO-ACCESS-P8-F16)))

(:derived (Flag153-)(and (NOT-BOARDED-P7)))

(:derived (Flag153-)(and (NOT-NO-ACCESS-P7-F16)))

(:derived (Flag154-)(and (NOT-BOARDED-P6)))

(:derived (Flag154-)(and (NOT-NO-ACCESS-P6-F16)))

(:derived (Flag155-)(and (NOT-BOARDED-P5)))

(:derived (Flag155-)(and (NOT-NO-ACCESS-P5-F16)))

(:derived (Flag156-)(and (NOT-BOARDED-P4)))

(:derived (Flag156-)(and (NOT-NO-ACCESS-P4-F16)))

(:derived (Flag157-)(and (NOT-BOARDED-P3)))

(:derived (Flag157-)(and (NOT-NO-ACCESS-P3-F16)))

(:derived (Flag158-)(and (NOT-BOARDED-P2)))

(:derived (Flag158-)(and (NOT-NO-ACCESS-P2-F16)))

(:derived (Flag159-)(and (NOT-BOARDED-P1)))

(:derived (Flag159-)(and (NOT-NO-ACCESS-P1-F16)))

(:derived (Flag160-)(and (NOT-BOARDED-P0)))

(:derived (Flag160-)(and (NOT-NO-ACCESS-P0-F16)))

(:derived (Flag166-)(and (NOT-BOARDED-P12)))

(:derived (Flag166-)(and (NOT-NO-ACCESS-P12-F15)))

(:derived (Flag167-)(and (NOT-BOARDED-P11)))

(:derived (Flag167-)(and (NOT-NO-ACCESS-P11-F15)))

(:derived (Flag168-)(and (NOT-BOARDED-P10)))

(:derived (Flag168-)(and (NOT-NO-ACCESS-P10-F15)))

(:derived (Flag169-)(and (NOT-BOARDED-P9)))

(:derived (Flag169-)(and (NOT-NO-ACCESS-P9-F15)))

(:derived (Flag170-)(and (NOT-BOARDED-P8)))

(:derived (Flag170-)(and (NOT-NO-ACCESS-P8-F15)))

(:derived (Flag171-)(and (NOT-BOARDED-P7)))

(:derived (Flag171-)(and (NOT-NO-ACCESS-P7-F15)))

(:derived (Flag172-)(and (NOT-BOARDED-P6)))

(:derived (Flag172-)(and (NOT-NO-ACCESS-P6-F15)))

(:derived (Flag173-)(and (NOT-BOARDED-P5)))

(:derived (Flag173-)(and (NOT-NO-ACCESS-P5-F15)))

(:derived (Flag174-)(and (NOT-BOARDED-P4)))

(:derived (Flag174-)(and (NOT-NO-ACCESS-P4-F15)))

(:derived (Flag175-)(and (NOT-BOARDED-P3)))

(:derived (Flag175-)(and (NOT-NO-ACCESS-P3-F15)))

(:derived (Flag176-)(and (NOT-BOARDED-P2)))

(:derived (Flag176-)(and (NOT-NO-ACCESS-P2-F15)))

(:derived (Flag177-)(and (NOT-BOARDED-P1)))

(:derived (Flag177-)(and (NOT-NO-ACCESS-P1-F15)))

(:derived (Flag178-)(and (NOT-BOARDED-P0)))

(:derived (Flag178-)(and (NOT-NO-ACCESS-P0-F15)))

(:derived (Flag181-)(and (NOT-BOARDED-P4)))

(:derived (Flag183-)(and (NOT-BOARDED-P4)))

(:derived (Flag184-)(and (NOT-BOARDED-P11)))

(:derived (Flag185-)(and (NOT-BOARDED-P12)))

(:derived (Flag185-)(and (NOT-NO-ACCESS-P12-F14)))

(:derived (Flag186-)(and (NOT-BOARDED-P11)))

(:derived (Flag186-)(and (NOT-NO-ACCESS-P11-F14)))

(:derived (Flag187-)(and (NOT-BOARDED-P10)))

(:derived (Flag187-)(and (NOT-NO-ACCESS-P10-F14)))

(:derived (Flag188-)(and (NOT-BOARDED-P9)))

(:derived (Flag188-)(and (NOT-NO-ACCESS-P9-F14)))

(:derived (Flag189-)(and (NOT-BOARDED-P8)))

(:derived (Flag189-)(and (NOT-NO-ACCESS-P8-F14)))

(:derived (Flag190-)(and (NOT-BOARDED-P7)))

(:derived (Flag190-)(and (NOT-NO-ACCESS-P7-F14)))

(:derived (Flag191-)(and (NOT-BOARDED-P6)))

(:derived (Flag191-)(and (NOT-NO-ACCESS-P6-F14)))

(:derived (Flag192-)(and (NOT-BOARDED-P5)))

(:derived (Flag192-)(and (NOT-NO-ACCESS-P5-F14)))

(:derived (Flag193-)(and (NOT-BOARDED-P4)))

(:derived (Flag193-)(and (NOT-NO-ACCESS-P4-F14)))

(:derived (Flag194-)(and (NOT-BOARDED-P3)))

(:derived (Flag194-)(and (NOT-NO-ACCESS-P3-F14)))

(:derived (Flag195-)(and (NOT-BOARDED-P2)))

(:derived (Flag195-)(and (NOT-NO-ACCESS-P2-F14)))

(:derived (Flag196-)(and (NOT-BOARDED-P1)))

(:derived (Flag196-)(and (NOT-NO-ACCESS-P1-F14)))

(:derived (Flag197-)(and (NOT-BOARDED-P0)))

(:derived (Flag197-)(and (NOT-NO-ACCESS-P0-F14)))

(:derived (Flag203-)(and (NOT-BOARDED-P11)))

(:derived (Flag203-)(and (NOT-SERVED-P2)))

(:derived (Flag204-)(and (NOT-BOARDED-P12)))

(:derived (Flag204-)(and (NOT-NO-ACCESS-P12-F13)))

(:derived (Flag205-)(and (NOT-BOARDED-P11)))

(:derived (Flag205-)(and (NOT-NO-ACCESS-P11-F13)))

(:derived (Flag206-)(and (NOT-BOARDED-P10)))

(:derived (Flag206-)(and (NOT-NO-ACCESS-P10-F13)))

(:derived (Flag207-)(and (NOT-BOARDED-P9)))

(:derived (Flag207-)(and (NOT-NO-ACCESS-P9-F13)))

(:derived (Flag208-)(and (NOT-BOARDED-P8)))

(:derived (Flag208-)(and (NOT-NO-ACCESS-P8-F13)))

(:derived (Flag209-)(and (NOT-BOARDED-P7)))

(:derived (Flag209-)(and (NOT-NO-ACCESS-P7-F13)))

(:derived (Flag210-)(and (NOT-BOARDED-P6)))

(:derived (Flag210-)(and (NOT-NO-ACCESS-P6-F13)))

(:derived (Flag211-)(and (NOT-BOARDED-P5)))

(:derived (Flag211-)(and (NOT-NO-ACCESS-P5-F13)))

(:derived (Flag212-)(and (NOT-BOARDED-P4)))

(:derived (Flag212-)(and (NOT-NO-ACCESS-P4-F13)))

(:derived (Flag213-)(and (NOT-BOARDED-P3)))

(:derived (Flag213-)(and (NOT-NO-ACCESS-P3-F13)))

(:derived (Flag214-)(and (NOT-BOARDED-P2)))

(:derived (Flag214-)(and (NOT-NO-ACCESS-P2-F13)))

(:derived (Flag215-)(and (NOT-BOARDED-P1)))

(:derived (Flag215-)(and (NOT-NO-ACCESS-P1-F13)))

(:derived (Flag216-)(and (NOT-BOARDED-P0)))

(:derived (Flag216-)(and (NOT-NO-ACCESS-P0-F13)))

(:derived (Flag218-)(and (NOT-BOARDED-P12)))

(:derived (Flag218-)(and (NOT-NO-ACCESS-P12-F12)))

(:derived (Flag219-)(and (NOT-BOARDED-P11)))

(:derived (Flag219-)(and (NOT-NO-ACCESS-P11-F12)))

(:derived (Flag220-)(and (NOT-BOARDED-P10)))

(:derived (Flag220-)(and (NOT-NO-ACCESS-P10-F12)))

(:derived (Flag221-)(and (NOT-BOARDED-P9)))

(:derived (Flag221-)(and (NOT-NO-ACCESS-P9-F12)))

(:derived (Flag222-)(and (NOT-BOARDED-P8)))

(:derived (Flag222-)(and (NOT-NO-ACCESS-P8-F12)))

(:derived (Flag223-)(and (NOT-BOARDED-P7)))

(:derived (Flag223-)(and (NOT-NO-ACCESS-P7-F12)))

(:derived (Flag224-)(and (NOT-BOARDED-P6)))

(:derived (Flag224-)(and (NOT-NO-ACCESS-P6-F12)))

(:derived (Flag225-)(and (NOT-BOARDED-P5)))

(:derived (Flag225-)(and (NOT-NO-ACCESS-P5-F12)))

(:derived (Flag226-)(and (NOT-BOARDED-P4)))

(:derived (Flag226-)(and (NOT-NO-ACCESS-P4-F12)))

(:derived (Flag227-)(and (NOT-BOARDED-P3)))

(:derived (Flag227-)(and (NOT-NO-ACCESS-P3-F12)))

(:derived (Flag228-)(and (NOT-BOARDED-P2)))

(:derived (Flag228-)(and (NOT-NO-ACCESS-P2-F12)))

(:derived (Flag229-)(and (NOT-BOARDED-P1)))

(:derived (Flag229-)(and (NOT-NO-ACCESS-P1-F12)))

(:derived (Flag230-)(and (NOT-BOARDED-P0)))

(:derived (Flag230-)(and (NOT-NO-ACCESS-P0-F12)))

(:derived (Flag232-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag235-)(and (NOT-BOARDED-P11)))

(:derived (Flag236-)(and (NOT-BOARDED-P12)))

(:derived (Flag236-)(and (NOT-NO-ACCESS-P12-F11)))

(:derived (Flag237-)(and (NOT-BOARDED-P11)))

(:derived (Flag237-)(and (NOT-NO-ACCESS-P11-F11)))

(:derived (Flag238-)(and (NOT-BOARDED-P10)))

(:derived (Flag238-)(and (NOT-NO-ACCESS-P10-F11)))

(:derived (Flag239-)(and (NOT-BOARDED-P9)))

(:derived (Flag239-)(and (NOT-NO-ACCESS-P9-F11)))

(:derived (Flag240-)(and (NOT-BOARDED-P8)))

(:derived (Flag240-)(and (NOT-NO-ACCESS-P8-F11)))

(:derived (Flag241-)(and (NOT-BOARDED-P7)))

(:derived (Flag241-)(and (NOT-NO-ACCESS-P7-F11)))

(:derived (Flag242-)(and (NOT-BOARDED-P6)))

(:derived (Flag242-)(and (NOT-NO-ACCESS-P6-F11)))

(:derived (Flag243-)(and (NOT-BOARDED-P5)))

(:derived (Flag243-)(and (NOT-NO-ACCESS-P5-F11)))

(:derived (Flag244-)(and (NOT-BOARDED-P4)))

(:derived (Flag244-)(and (NOT-NO-ACCESS-P4-F11)))

(:derived (Flag245-)(and (NOT-BOARDED-P3)))

(:derived (Flag245-)(and (NOT-NO-ACCESS-P3-F11)))

(:derived (Flag246-)(and (NOT-BOARDED-P2)))

(:derived (Flag246-)(and (NOT-NO-ACCESS-P2-F11)))

(:derived (Flag247-)(and (NOT-BOARDED-P1)))

(:derived (Flag247-)(and (NOT-NO-ACCESS-P1-F11)))

(:derived (Flag248-)(and (NOT-BOARDED-P0)))

(:derived (Flag248-)(and (NOT-NO-ACCESS-P0-F11)))

(:derived (Flag254-)(and (NOT-BOARDED-P11)))

(:derived (Flag254-)(and (NOT-SERVED-P7)))

(:derived (Flag255-)(and (NOT-BOARDED-P12)))

(:derived (Flag255-)(and (NOT-NO-ACCESS-P12-F10)))

(:derived (Flag256-)(and (NOT-BOARDED-P11)))

(:derived (Flag256-)(and (NOT-NO-ACCESS-P11-F10)))

(:derived (Flag257-)(and (NOT-BOARDED-P10)))

(:derived (Flag257-)(and (NOT-NO-ACCESS-P10-F10)))

(:derived (Flag258-)(and (NOT-BOARDED-P9)))

(:derived (Flag258-)(and (NOT-NO-ACCESS-P9-F10)))

(:derived (Flag259-)(and (NOT-BOARDED-P8)))

(:derived (Flag259-)(and (NOT-NO-ACCESS-P8-F10)))

(:derived (Flag260-)(and (NOT-BOARDED-P7)))

(:derived (Flag260-)(and (NOT-NO-ACCESS-P7-F10)))

(:derived (Flag261-)(and (NOT-BOARDED-P6)))

(:derived (Flag261-)(and (NOT-NO-ACCESS-P6-F10)))

(:derived (Flag262-)(and (NOT-BOARDED-P5)))

(:derived (Flag262-)(and (NOT-NO-ACCESS-P5-F10)))

(:derived (Flag263-)(and (NOT-BOARDED-P4)))

(:derived (Flag263-)(and (NOT-NO-ACCESS-P4-F10)))

(:derived (Flag264-)(and (NOT-BOARDED-P2)))

(:derived (Flag264-)(and (NOT-NO-ACCESS-P2-F10)))

(:derived (Flag265-)(and (NOT-BOARDED-P1)))

(:derived (Flag265-)(and (NOT-NO-ACCESS-P1-F10)))

(:derived (Flag266-)(and (NOT-BOARDED-P0)))

(:derived (Flag266-)(and (NOT-NO-ACCESS-P0-F10)))

(:derived (Flag268-)(and (NOT-BOARDED-P12)))

(:derived (Flag268-)(and (NOT-NO-ACCESS-P12-F9)))

(:derived (Flag269-)(and (NOT-BOARDED-P11)))

(:derived (Flag269-)(and (NOT-NO-ACCESS-P11-F9)))

(:derived (Flag270-)(and (NOT-BOARDED-P10)))

(:derived (Flag270-)(and (NOT-NO-ACCESS-P10-F9)))

(:derived (Flag271-)(and (NOT-BOARDED-P9)))

(:derived (Flag271-)(and (NOT-NO-ACCESS-P9-F9)))

(:derived (Flag272-)(and (NOT-BOARDED-P8)))

(:derived (Flag272-)(and (NOT-NO-ACCESS-P8-F9)))

(:derived (Flag273-)(and (NOT-BOARDED-P7)))

(:derived (Flag273-)(and (NOT-NO-ACCESS-P7-F9)))

(:derived (Flag274-)(and (NOT-BOARDED-P6)))

(:derived (Flag274-)(and (NOT-NO-ACCESS-P6-F9)))

(:derived (Flag275-)(and (NOT-BOARDED-P5)))

(:derived (Flag275-)(and (NOT-NO-ACCESS-P5-F9)))

(:derived (Flag276-)(and (NOT-BOARDED-P4)))

(:derived (Flag276-)(and (NOT-NO-ACCESS-P4-F9)))

(:derived (Flag277-)(and (NOT-BOARDED-P3)))

(:derived (Flag277-)(and (NOT-NO-ACCESS-P3-F9)))

(:derived (Flag278-)(and (NOT-BOARDED-P2)))

(:derived (Flag278-)(and (NOT-NO-ACCESS-P2-F9)))

(:derived (Flag279-)(and (NOT-BOARDED-P1)))

(:derived (Flag279-)(and (NOT-NO-ACCESS-P1-F9)))

(:derived (Flag280-)(and (NOT-BOARDED-P0)))

(:derived (Flag280-)(and (NOT-NO-ACCESS-P0-F9)))

(:derived (Flag282-)(and (NOT-BOARDED-P12)))

(:derived (Flag282-)(and (NOT-NO-ACCESS-P12-F8)))

(:derived (Flag283-)(and (NOT-BOARDED-P11)))

(:derived (Flag283-)(and (NOT-NO-ACCESS-P11-F8)))

(:derived (Flag284-)(and (NOT-BOARDED-P10)))

(:derived (Flag284-)(and (NOT-NO-ACCESS-P10-F8)))

(:derived (Flag285-)(and (NOT-BOARDED-P9)))

(:derived (Flag285-)(and (NOT-NO-ACCESS-P9-F8)))

(:derived (Flag286-)(and (NOT-BOARDED-P8)))

(:derived (Flag286-)(and (NOT-NO-ACCESS-P8-F8)))

(:derived (Flag287-)(and (NOT-BOARDED-P7)))

(:derived (Flag287-)(and (NOT-NO-ACCESS-P7-F8)))

(:derived (Flag288-)(and (NOT-BOARDED-P6)))

(:derived (Flag288-)(and (NOT-NO-ACCESS-P6-F8)))

(:derived (Flag289-)(and (NOT-BOARDED-P5)))

(:derived (Flag289-)(and (NOT-NO-ACCESS-P5-F8)))

(:derived (Flag290-)(and (NOT-BOARDED-P4)))

(:derived (Flag290-)(and (NOT-NO-ACCESS-P4-F8)))

(:derived (Flag291-)(and (NOT-BOARDED-P3)))

(:derived (Flag291-)(and (NOT-NO-ACCESS-P3-F8)))

(:derived (Flag292-)(and (NOT-BOARDED-P2)))

(:derived (Flag292-)(and (NOT-NO-ACCESS-P2-F8)))

(:derived (Flag293-)(and (NOT-BOARDED-P1)))

(:derived (Flag293-)(and (NOT-NO-ACCESS-P1-F8)))

(:derived (Flag294-)(and (NOT-BOARDED-P0)))

(:derived (Flag294-)(and (NOT-NO-ACCESS-P0-F8)))

(:derived (Flag300-)(and (NOT-BOARDED-P11)))

(:derived (Flag300-)(and (NOT-SERVED-P12)))

(:derived (Flag301-)(and (NOT-BOARDED-P12)))

(:derived (Flag301-)(and (NOT-NO-ACCESS-P12-F7)))

(:derived (Flag302-)(and (NOT-BOARDED-P11)))

(:derived (Flag302-)(and (NOT-NO-ACCESS-P11-F7)))

(:derived (Flag303-)(and (NOT-BOARDED-P10)))

(:derived (Flag303-)(and (NOT-NO-ACCESS-P10-F7)))

(:derived (Flag304-)(and (NOT-BOARDED-P9)))

(:derived (Flag304-)(and (NOT-NO-ACCESS-P9-F7)))

(:derived (Flag305-)(and (NOT-BOARDED-P8)))

(:derived (Flag305-)(and (NOT-NO-ACCESS-P8-F7)))

(:derived (Flag306-)(and (NOT-BOARDED-P7)))

(:derived (Flag306-)(and (NOT-NO-ACCESS-P7-F7)))

(:derived (Flag307-)(and (NOT-BOARDED-P6)))

(:derived (Flag307-)(and (NOT-NO-ACCESS-P6-F7)))

(:derived (Flag308-)(and (NOT-BOARDED-P5)))

(:derived (Flag308-)(and (NOT-NO-ACCESS-P5-F7)))

(:derived (Flag309-)(and (NOT-BOARDED-P4)))

(:derived (Flag309-)(and (NOT-NO-ACCESS-P4-F7)))

(:derived (Flag310-)(and (NOT-BOARDED-P3)))

(:derived (Flag310-)(and (NOT-NO-ACCESS-P3-F7)))

(:derived (Flag311-)(and (NOT-BOARDED-P2)))

(:derived (Flag311-)(and (NOT-NO-ACCESS-P2-F7)))

(:derived (Flag312-)(and (NOT-BOARDED-P1)))

(:derived (Flag312-)(and (NOT-NO-ACCESS-P1-F7)))

(:derived (Flag313-)(and (NOT-BOARDED-P0)))

(:derived (Flag313-)(and (NOT-NO-ACCESS-P0-F7)))

(:derived (Flag316-)(and (NOT-BOARDED-P12)))

(:derived (Flag318-)(and (NOT-BOARDED-P12)))

(:derived (Flag319-)(and (NOT-BOARDED-P11)))

(:derived (Flag320-)(and (NOT-BOARDED-P12)))

(:derived (Flag320-)(and (NOT-NO-ACCESS-P12-F6)))

(:derived (Flag321-)(and (NOT-BOARDED-P11)))

(:derived (Flag321-)(and (NOT-NO-ACCESS-P11-F6)))

(:derived (Flag322-)(and (NOT-BOARDED-P10)))

(:derived (Flag322-)(and (NOT-NO-ACCESS-P10-F6)))

(:derived (Flag323-)(and (NOT-BOARDED-P9)))

(:derived (Flag323-)(and (NOT-NO-ACCESS-P9-F6)))

(:derived (Flag324-)(and (NOT-BOARDED-P8)))

(:derived (Flag324-)(and (NOT-NO-ACCESS-P8-F6)))

(:derived (Flag325-)(and (NOT-BOARDED-P7)))

(:derived (Flag325-)(and (NOT-NO-ACCESS-P7-F6)))

(:derived (Flag326-)(and (NOT-BOARDED-P6)))

(:derived (Flag326-)(and (NOT-NO-ACCESS-P6-F6)))

(:derived (Flag327-)(and (NOT-BOARDED-P5)))

(:derived (Flag327-)(and (NOT-NO-ACCESS-P5-F6)))

(:derived (Flag328-)(and (NOT-BOARDED-P4)))

(:derived (Flag328-)(and (NOT-NO-ACCESS-P4-F6)))

(:derived (Flag329-)(and (NOT-BOARDED-P3)))

(:derived (Flag329-)(and (NOT-NO-ACCESS-P3-F6)))

(:derived (Flag330-)(and (NOT-BOARDED-P2)))

(:derived (Flag330-)(and (NOT-NO-ACCESS-P2-F6)))

(:derived (Flag331-)(and (NOT-BOARDED-P1)))

(:derived (Flag331-)(and (NOT-NO-ACCESS-P1-F6)))

(:derived (Flag332-)(and (NOT-BOARDED-P0)))

(:derived (Flag332-)(and (NOT-NO-ACCESS-P0-F6)))

(:derived (Flag340-)(and (NOT-BOARDED-P12)))

(:derived (Flag340-)(and (NOT-NO-ACCESS-P12-F5)))

(:derived (Flag341-)(and (NOT-BOARDED-P11)))

(:derived (Flag341-)(and (NOT-NO-ACCESS-P11-F5)))

(:derived (Flag342-)(and (NOT-BOARDED-P10)))

(:derived (Flag342-)(and (NOT-NO-ACCESS-P10-F5)))

(:derived (Flag343-)(and (NOT-BOARDED-P9)))

(:derived (Flag343-)(and (NOT-NO-ACCESS-P9-F5)))

(:derived (Flag344-)(and (NOT-BOARDED-P8)))

(:derived (Flag344-)(and (NOT-NO-ACCESS-P8-F5)))

(:derived (Flag345-)(and (NOT-BOARDED-P7)))

(:derived (Flag345-)(and (NOT-NO-ACCESS-P7-F5)))

(:derived (Flag346-)(and (NOT-BOARDED-P6)))

(:derived (Flag346-)(and (NOT-NO-ACCESS-P6-F5)))

(:derived (Flag347-)(and (NOT-BOARDED-P5)))

(:derived (Flag347-)(and (NOT-NO-ACCESS-P5-F5)))

(:derived (Flag348-)(and (NOT-BOARDED-P4)))

(:derived (Flag348-)(and (NOT-NO-ACCESS-P4-F5)))

(:derived (Flag349-)(and (NOT-BOARDED-P3)))

(:derived (Flag349-)(and (NOT-NO-ACCESS-P3-F5)))

(:derived (Flag350-)(and (NOT-BOARDED-P2)))

(:derived (Flag350-)(and (NOT-NO-ACCESS-P2-F5)))

(:derived (Flag351-)(and (NOT-BOARDED-P1)))

(:derived (Flag351-)(and (NOT-NO-ACCESS-P1-F5)))

(:derived (Flag352-)(and (NOT-BOARDED-P0)))

(:derived (Flag352-)(and (NOT-NO-ACCESS-P0-F5)))

(:derived (Flag358-)(and (NOT-BOARDED-P11)))

(:derived (Flag359-)(and (NOT-BOARDED-P12)))

(:derived (Flag359-)(and (NOT-NO-ACCESS-P12-F4)))

(:derived (Flag360-)(and (NOT-BOARDED-P11)))

(:derived (Flag360-)(and (NOT-NO-ACCESS-P11-F4)))

(:derived (Flag361-)(and (NOT-BOARDED-P10)))

(:derived (Flag361-)(and (NOT-NO-ACCESS-P10-F4)))

(:derived (Flag362-)(and (NOT-BOARDED-P9)))

(:derived (Flag362-)(and (NOT-NO-ACCESS-P9-F4)))

(:derived (Flag363-)(and (NOT-BOARDED-P8)))

(:derived (Flag363-)(and (NOT-NO-ACCESS-P8-F4)))

(:derived (Flag364-)(and (NOT-BOARDED-P7)))

(:derived (Flag364-)(and (NOT-NO-ACCESS-P7-F4)))

(:derived (Flag365-)(and (NOT-BOARDED-P6)))

(:derived (Flag365-)(and (NOT-NO-ACCESS-P6-F4)))

(:derived (Flag366-)(and (NOT-BOARDED-P5)))

(:derived (Flag366-)(and (NOT-NO-ACCESS-P5-F4)))

(:derived (Flag367-)(and (NOT-BOARDED-P4)))

(:derived (Flag367-)(and (NOT-NO-ACCESS-P4-F4)))

(:derived (Flag368-)(and (NOT-BOARDED-P3)))

(:derived (Flag368-)(and (NOT-NO-ACCESS-P3-F4)))

(:derived (Flag369-)(and (NOT-BOARDED-P2)))

(:derived (Flag369-)(and (NOT-NO-ACCESS-P2-F4)))

(:derived (Flag370-)(and (NOT-BOARDED-P1)))

(:derived (Flag370-)(and (NOT-NO-ACCESS-P1-F4)))

(:derived (Flag371-)(and (NOT-BOARDED-P0)))

(:derived (Flag371-)(and (NOT-NO-ACCESS-P0-F4)))

(:derived (Flag373-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P9)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag376-)(and (NOT-BOARDED-P11)))

(:derived (Flag377-)(and (NOT-BOARDED-P12)))

(:derived (Flag377-)(and (NOT-NO-ACCESS-P12-F3)))

(:derived (Flag378-)(and (NOT-BOARDED-P11)))

(:derived (Flag378-)(and (NOT-NO-ACCESS-P11-F3)))

(:derived (Flag379-)(and (NOT-BOARDED-P10)))

(:derived (Flag379-)(and (NOT-NO-ACCESS-P10-F3)))

(:derived (Flag380-)(and (NOT-BOARDED-P9)))

(:derived (Flag380-)(and (NOT-NO-ACCESS-P9-F3)))

(:derived (Flag381-)(and (NOT-BOARDED-P8)))

(:derived (Flag381-)(and (NOT-NO-ACCESS-P8-F3)))

(:derived (Flag382-)(and (NOT-BOARDED-P7)))

(:derived (Flag382-)(and (NOT-NO-ACCESS-P7-F3)))

(:derived (Flag383-)(and (NOT-BOARDED-P6)))

(:derived (Flag383-)(and (NOT-NO-ACCESS-P6-F3)))

(:derived (Flag384-)(and (NOT-BOARDED-P5)))

(:derived (Flag384-)(and (NOT-NO-ACCESS-P5-F3)))

(:derived (Flag385-)(and (NOT-BOARDED-P4)))

(:derived (Flag385-)(and (NOT-NO-ACCESS-P4-F3)))

(:derived (Flag386-)(and (NOT-BOARDED-P3)))

(:derived (Flag386-)(and (NOT-NO-ACCESS-P3-F3)))

(:derived (Flag387-)(and (NOT-BOARDED-P2)))

(:derived (Flag387-)(and (NOT-NO-ACCESS-P2-F3)))

(:derived (Flag388-)(and (NOT-BOARDED-P1)))

(:derived (Flag388-)(and (NOT-NO-ACCESS-P1-F3)))

(:derived (Flag389-)(and (NOT-BOARDED-P0)))

(:derived (Flag389-)(and (NOT-NO-ACCESS-P0-F3)))

(:derived (Flag395-)(and (NOT-BOARDED-P11)))

(:derived (Flag395-)(and (NOT-SERVED-P3)))

(:derived (Flag396-)(and (NOT-BOARDED-P12)))

(:derived (Flag396-)(and (NOT-NO-ACCESS-P12-F2)))

(:derived (Flag397-)(and (NOT-BOARDED-P11)))

(:derived (Flag397-)(and (NOT-NO-ACCESS-P11-F2)))

(:derived (Flag398-)(and (NOT-BOARDED-P10)))

(:derived (Flag398-)(and (NOT-NO-ACCESS-P10-F2)))

(:derived (Flag399-)(and (NOT-BOARDED-P9)))

(:derived (Flag399-)(and (NOT-NO-ACCESS-P9-F2)))

(:derived (Flag400-)(and (NOT-BOARDED-P8)))

(:derived (Flag400-)(and (NOT-NO-ACCESS-P8-F2)))

(:derived (Flag401-)(and (NOT-BOARDED-P7)))

(:derived (Flag401-)(and (NOT-NO-ACCESS-P7-F2)))

(:derived (Flag402-)(and (NOT-BOARDED-P6)))

(:derived (Flag402-)(and (NOT-NO-ACCESS-P6-F2)))

(:derived (Flag403-)(and (NOT-BOARDED-P5)))

(:derived (Flag403-)(and (NOT-NO-ACCESS-P5-F2)))

(:derived (Flag404-)(and (NOT-BOARDED-P4)))

(:derived (Flag404-)(and (NOT-NO-ACCESS-P4-F2)))

(:derived (Flag405-)(and (NOT-BOARDED-P3)))

(:derived (Flag405-)(and (NOT-NO-ACCESS-P3-F2)))

(:derived (Flag406-)(and (NOT-BOARDED-P2)))

(:derived (Flag406-)(and (NOT-NO-ACCESS-P2-F2)))

(:derived (Flag407-)(and (NOT-BOARDED-P0)))

(:derived (Flag407-)(and (NOT-NO-ACCESS-P0-F2)))

(:derived (Flag413-)(and (NOT-BOARDED-P11)))

(:derived (Flag413-)(and (NOT-SERVED-P1)))

(:derived (Flag414-)(and (NOT-BOARDED-P12)))

(:derived (Flag414-)(and (NOT-NO-ACCESS-P12-F1)))

(:derived (Flag415-)(and (NOT-BOARDED-P11)))

(:derived (Flag415-)(and (NOT-NO-ACCESS-P11-F1)))

(:derived (Flag416-)(and (NOT-BOARDED-P10)))

(:derived (Flag416-)(and (NOT-NO-ACCESS-P10-F1)))

(:derived (Flag417-)(and (NOT-BOARDED-P9)))

(:derived (Flag417-)(and (NOT-NO-ACCESS-P9-F1)))

(:derived (Flag418-)(and (NOT-BOARDED-P8)))

(:derived (Flag418-)(and (NOT-NO-ACCESS-P8-F1)))

(:derived (Flag419-)(and (NOT-BOARDED-P7)))

(:derived (Flag419-)(and (NOT-NO-ACCESS-P7-F1)))

(:derived (Flag420-)(and (NOT-BOARDED-P6)))

(:derived (Flag420-)(and (NOT-NO-ACCESS-P6-F1)))

(:derived (Flag421-)(and (NOT-BOARDED-P5)))

(:derived (Flag421-)(and (NOT-NO-ACCESS-P5-F1)))

(:derived (Flag422-)(and (NOT-BOARDED-P4)))

(:derived (Flag422-)(and (NOT-NO-ACCESS-P4-F1)))

(:derived (Flag423-)(and (NOT-BOARDED-P3)))

(:derived (Flag423-)(and (NOT-NO-ACCESS-P3-F1)))

(:derived (Flag424-)(and (NOT-BOARDED-P2)))

(:derived (Flag424-)(and (NOT-NO-ACCESS-P2-F1)))

(:derived (Flag425-)(and (NOT-BOARDED-P1)))

(:derived (Flag425-)(and (NOT-NO-ACCESS-P1-F1)))

(:derived (Flag426-)(and (NOT-BOARDED-P0)))

(:derived (Flag426-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag428-)(and (NOT-BOARDED-P11)(NOT-BOARDED-P10)(NOT-BOARDED-P8)(NOT-BOARDED-P7)(NOT-BOARDED-P5)(NOT-BOARDED-P3)(NOT-BOARDED-P2)(NOT-BOARDED-P1)(NOT-BOARDED-P0)))

(:derived (Flag431-)(and (NOT-BOARDED-P12)))

(:derived (Flag431-)(and (NOT-NO-ACCESS-P12-F0)))

(:derived (Flag432-)(and (NOT-BOARDED-P11)))

(:derived (Flag432-)(and (NOT-NO-ACCESS-P11-F0)))

(:derived (Flag433-)(and (NOT-BOARDED-P10)))

(:derived (Flag433-)(and (NOT-NO-ACCESS-P10-F0)))

(:derived (Flag434-)(and (NOT-BOARDED-P9)))

(:derived (Flag434-)(and (NOT-NO-ACCESS-P9-F0)))

(:derived (Flag435-)(and (NOT-BOARDED-P8)))

(:derived (Flag435-)(and (NOT-NO-ACCESS-P8-F0)))

(:derived (Flag436-)(and (NOT-BOARDED-P7)))

(:derived (Flag436-)(and (NOT-NO-ACCESS-P7-F0)))

(:derived (Flag437-)(and (NOT-BOARDED-P6)))

(:derived (Flag437-)(and (NOT-NO-ACCESS-P6-F0)))

(:derived (Flag438-)(and (NOT-BOARDED-P5)))

(:derived (Flag438-)(and (NOT-NO-ACCESS-P5-F0)))

(:derived (Flag439-)(and (NOT-BOARDED-P4)))

(:derived (Flag439-)(and (NOT-NO-ACCESS-P4-F0)))

(:derived (Flag440-)(and (NOT-BOARDED-P3)))

(:derived (Flag440-)(and (NOT-NO-ACCESS-P3-F0)))

(:derived (Flag441-)(and (NOT-BOARDED-P2)))

(:derived (Flag441-)(and (NOT-NO-ACCESS-P2-F0)))

(:derived (Flag442-)(and (NOT-BOARDED-P1)))

(:derived (Flag442-)(and (NOT-NO-ACCESS-P1-F0)))

(:derived (Flag443-)(and (NOT-BOARDED-P0)))

(:derived (Flag443-)(and (NOT-NO-ACCESS-P0-F0)))

)
