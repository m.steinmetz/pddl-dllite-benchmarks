(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag3-)
(Flag1-)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(Flag4-)
(Flag2-)
(SERVED-P0)
(BOARDED-P0)
(NOT-BOARDED-P0)
(NOT-SERVED-P0)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag2-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag4-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
)
)
(:derived (Flag2-)(and (LIFT-AT-F1)(Flag1-)))

(:derived (Flag4-)(and (LIFT-AT-F0)(Flag3-)))

(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
)
)
(:derived (Flag1-)(and (NOT-BOARDED-P0)))

(:derived (Flag1-)(and (NOT-NO-ACCESS-P0-F1)))

(:derived (Flag3-)(and (NOT-BOARDED-P0)))

(:derived (Flag3-)(and (NOT-NO-ACCESS-P0-F0)))

)
