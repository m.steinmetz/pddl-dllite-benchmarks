(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag3-)
(Flag1-)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(Flag4-)
(Flag2-)
(SERVED-P0)
(BOARDED-P0)
(NOT-BOARDED-P0)
(NOT-SERVED-P0)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
(Flag3prime-)
(Flag1prime-)
(Flag4prime-)
(Flag2prime-)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action Flag2Action
:parameters ()
:precondition
(and
(LIFT-AT-F1)
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F1))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(LIFT-AT-F0)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag4prime-))
(not (Flag2prime-))
)
)
(:action Flag1Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F1))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F0))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F0))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
)
