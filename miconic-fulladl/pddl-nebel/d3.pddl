(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag24-)
(Flag23-)
(Flag22-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag4-)
(Flag3-)
(Flag2-)
(LIFT-AT-F5)
(LIFT-AT-F4)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P0-F4)
(NO-ACCESS-P0-F5)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(NO-ACCESS-P1-F4)
(NO-ACCESS-P1-F5)
(NO-ACCESS-P2-F0)
(NO-ACCESS-P2-F1)
(NO-ACCESS-P2-F2)
(NO-ACCESS-P2-F3)
(NO-ACCESS-P2-F4)
(NO-ACCESS-P2-F5)
(Flag25-)
(Flag21-)
(Flag17-)
(Flag13-)
(Flag9-)
(Flag5-)
(BOARDED-P0)
(SERVED-P1)
(SERVED-P2)
(BOARDED-P1)
(SERVED-P0)
(BOARDED-P2)
(Flag1-)
(NOT-BOARDED-P2)
(NOT-SERVED-P0)
(NOT-BOARDED-P1)
(NOT-BOARDED-P0)
(NOT-SERVED-P1)
(NOT-SERVED-P2)
(NOT-NO-ACCESS-P2-F5)
(NOT-NO-ACCESS-P2-F4)
(NOT-NO-ACCESS-P2-F3)
(NOT-NO-ACCESS-P2-F2)
(NOT-NO-ACCESS-P2-F1)
(NOT-NO-ACCESS-P2-F0)
(NOT-NO-ACCESS-P1-F5)
(NOT-NO-ACCESS-P1-F4)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F5)
(NOT-NO-ACCESS-P0-F4)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag25prime-)
(Flag21prime-)
(Flag17prime-)
(Flag13prime-)
(Flag9prime-)
(Flag5prime-)
(Flag1prime-)
)
(:action Flag1Action
:parameters ()
:precondition
(and
(SERVED-P0)
(SERVED-P1)
(SERVED-P2)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (SERVED-P0))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (SERVED-P1))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-2
:parameters ()
:precondition
(and
(not (SERVED-P2))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action STOP-F5
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P2)
)
(and
(BOARDED-P2)
(not (NOT-BOARDED-P2))
)
)
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action STOP-F4
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
(when
(and
(BOARDED-P2)
)
(and
(NOT-BOARDED-P2)
(SERVED-P2)
(not (BOARDED-P2))
(not (NOT-SERVED-P2))
)
)
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F4
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F5
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F2-F4
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F2-F5
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F3-F4
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F3-F5
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F4-F5
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action Flag5Action
:parameters ()
:precondition
(and
(LIFT-AT-F5)
(Flag2-)
(Flag2prime-)
(Flag3-)
(Flag3prime-)
(Flag4-)
(Flag4prime-)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Prim5Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F5))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-1
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-3
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(LIFT-AT-F4)
(Flag6-)
(Flag6prime-)
(Flag7-)
(Flag7prime-)
(Flag8-)
(Flag8prime-)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F4))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-2
:parameters ()
:precondition
(and
(not (Flag7-))
(Flag7prime-)
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(LIFT-AT-F3)
(Flag10-)
(Flag10prime-)
(Flag11-)
(Flag11prime-)
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F3))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-2
:parameters ()
:precondition
(and
(not (Flag11-))
(Flag11prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-3
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(LIFT-AT-F2)
(Flag14-)
(Flag14prime-)
(Flag15-)
(Flag15prime-)
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F2))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-2
:parameters ()
:precondition
(and
(not (Flag15-))
(Flag15prime-)
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-3
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(LIFT-AT-F1)
(Flag18-)
(Flag18prime-)
(Flag19-)
(Flag19prime-)
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F1))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-2
:parameters ()
:precondition
(and
(not (Flag19-))
(Flag19prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-3
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(LIFT-AT-F0)
(Flag22-)
(Flag22prime-)
(Flag23-)
(Flag23prime-)
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-2
:parameters ()
:precondition
(and
(not (Flag23-))
(Flag23prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-3
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action BLOCK-ACCESS-P2-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(NO-ACCESS-P2-F5)
(not (NOT-NO-ACCESS-P2-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P2-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(NO-ACCESS-P2-F4)
(not (NOT-NO-ACCESS-P2-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P2-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(NO-ACCESS-P2-F3)
(not (NOT-NO-ACCESS-P2-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P2-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(NO-ACCESS-P2-F2)
(not (NOT-NO-ACCESS-P2-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P2-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(NO-ACCESS-P2-F1)
(not (NOT-NO-ACCESS-P2-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P2-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(NO-ACCESS-P2-F0)
(not (NOT-NO-ACCESS-P2-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(NO-ACCESS-P1-F5)
(not (NOT-NO-ACCESS-P1-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(NO-ACCESS-P1-F4)
(not (NOT-NO-ACCESS-P1-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F5
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(NO-ACCESS-P0-F5)
(not (NOT-NO-ACCESS-P0-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F4
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(NO-ACCESS-P0-F4)
(not (NOT-NO-ACCESS-P0-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F4-F0
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F5-F0
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F4-F1
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F5-F1
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F4-F2
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F5-F2
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F4-F3
:parameters ()
:precondition
(and
(LIFT-AT-F4)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F4))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F5-F3
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F5-F4
:parameters ()
:precondition
(and
(LIFT-AT-F5)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F5))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F4
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F4)
(not (LIFT-AT-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F5
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F5)
(not (LIFT-AT-F0))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag9prime-))
(not (Flag5prime-))
(not (Flag1prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F5)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F5)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F5)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F5))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F5))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F5))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F4)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F4)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F4))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F4))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F4))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F3)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim10Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F3))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F3))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F3))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F2)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F2))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F2))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F2))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F1)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F1))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F1))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F1))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P2)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P2-F0)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P2))
(not (NOT-NO-ACCESS-P2-F0))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F0))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F0))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F0))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
)
