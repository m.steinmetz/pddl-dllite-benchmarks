(define (domain grounded-SIMPLE-ADL-MICONIC)
(:requirements
:strips
)
(:predicates
(Flag12-)
(Flag11-)
(Flag9-)
(Flag8-)
(Flag6-)
(Flag5-)
(Flag3-)
(Flag2-)
(LIFT-AT-F3)
(LIFT-AT-F2)
(LIFT-AT-F1)
(NO-ACCESS-P0-F0)
(NO-ACCESS-P0-F1)
(NO-ACCESS-P0-F2)
(NO-ACCESS-P0-F3)
(NO-ACCESS-P1-F0)
(NO-ACCESS-P1-F1)
(NO-ACCESS-P1-F2)
(NO-ACCESS-P1-F3)
(Flag13-)
(Flag10-)
(Flag7-)
(Flag4-)
(BOARDED-P1)
(SERVED-P0)
(BOARDED-P0)
(SERVED-P1)
(Flag1-)
(NOT-BOARDED-P0)
(NOT-SERVED-P1)
(NOT-SERVED-P0)
(NOT-BOARDED-P1)
(NOT-NO-ACCESS-P1-F3)
(NOT-NO-ACCESS-P1-F2)
(NOT-NO-ACCESS-P1-F1)
(NOT-NO-ACCESS-P1-F0)
(NOT-NO-ACCESS-P0-F3)
(NOT-NO-ACCESS-P0-F2)
(NOT-NO-ACCESS-P0-F1)
(NOT-NO-ACCESS-P0-F0)
(LIFT-AT-F0)
(Flag12prime-)
(Flag11prime-)
(Flag9prime-)
(Flag8prime-)
(Flag6prime-)
(Flag5prime-)
(Flag3prime-)
(Flag2prime-)
(Flag13prime-)
(Flag10prime-)
(Flag7prime-)
(Flag4prime-)
(Flag1prime-)
)
(:action Flag1Action
:parameters ()
:precondition
(and
(SERVED-P0)
(SERVED-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (SERVED-P0))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (SERVED-P1))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action STOP-F3
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P0)
)
(and
(BOARDED-P0)
(not (NOT-BOARDED-P0))
)
)
(when
(and
(BOARDED-P1)
)
(and
(NOT-BOARDED-P1)
(SERVED-P1)
(not (BOARDED-P1))
(not (NOT-SERVED-P1))
)
)
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action STOP-F2
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(when
(and
(BOARDED-P0)
)
(and
(NOT-BOARDED-P0)
(SERVED-P0)
(not (BOARDED-P0))
(not (NOT-SERVED-P0))
)
)
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action STOP-F1
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(when
(and
(NOT-SERVED-P1)
)
(and
(BOARDED-P1)
(not (NOT-BOARDED-P1))
)
)
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action STOP-F0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F2
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F1))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F1-F3
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F1))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F2-F3
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F2))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action Flag4Action
:parameters ()
:precondition
(and
(LIFT-AT-F3)
(Flag2-)
(Flag2prime-)
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F3))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Flag7Action
:parameters ()
:precondition
(and
(LIFT-AT-F2)
(Flag5-)
(Flag5prime-)
(Flag6-)
(Flag6prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F2))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-1
:parameters ()
:precondition
(and
(not (Flag5-))
(Flag5prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(LIFT-AT-F1)
(Flag8-)
(Flag8prime-)
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F1))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-2
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(LIFT-AT-F0)
(Flag11-)
(Flag11prime-)
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (Flag11-))
(Flag11prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-2
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action BLOCK-ACCESS-P1-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(NO-ACCESS-P1-F3)
(not (NOT-NO-ACCESS-P1-F3))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(NO-ACCESS-P1-F2)
(not (NOT-NO-ACCESS-P1-F2))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(NO-ACCESS-P1-F1)
(not (NOT-NO-ACCESS-P1-F1))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P1-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(NO-ACCESS-P1-F0)
(not (NOT-NO-ACCESS-P1-F0))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F3
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(NO-ACCESS-P0-F3)
(not (NOT-NO-ACCESS-P0-F3))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F2
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(NO-ACCESS-P0-F2)
(not (NOT-NO-ACCESS-P0-F2))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(NO-ACCESS-P0-F1)
(not (NOT-NO-ACCESS-P0-F1))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action BLOCK-ACCESS-P0-F0
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(NO-ACCESS-P0-F0)
(not (NOT-NO-ACCESS-P0-F0))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F1-F0
:parameters ()
:precondition
(and
(LIFT-AT-F1)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F1))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F2-F0
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F2))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F0
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F0)
(not (LIFT-AT-F3))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F2-F1
:parameters ()
:precondition
(and
(LIFT-AT-F2)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F2))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F1
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F3))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action DOWN-F3-F2
:parameters ()
:precondition
(and
(LIFT-AT-F3)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F3))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F1
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F1)
(not (LIFT-AT-F0))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F2
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F2)
(not (LIFT-AT-F0))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action UP-F0-F3
:parameters ()
:precondition
(and
(LIFT-AT-F0)
)
:effect
(and
(LIFT-AT-F3)
(not (LIFT-AT-F0))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag4prime-))
(not (Flag1prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F3)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F3)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F3))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F3))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F2)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F2))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F2))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F1)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F1)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F1))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F1))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag11Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P1)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag11Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P1-F0)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(NOT-BOARDED-P0)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(NOT-NO-ACCESS-P0-F0)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim11Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P1))
(not (NOT-NO-ACCESS-P1-F0))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (NOT-BOARDED-P0))
(not (NOT-NO-ACCESS-P0-F0))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (LIFT-AT-F0))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
)
