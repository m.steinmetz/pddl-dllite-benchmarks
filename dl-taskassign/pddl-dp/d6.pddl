(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag37-)
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-B)
(ELECTRONICSAGENT-E)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(SPECIFICATIONSAGENT-E)
(CODINGAGENT-A)
(CODINGAGENT-B)
(CODINGAGENT-E)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TESTINGAGENT-E)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(DESIGNAGENT-E)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-E)
(INFORMATICENGINEER-D)
(INFORMATICENGINEER-F)
(TASKAGENT-A)
(TASKAGENT-B)
(TASKAGENT-E)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(SOFTWAREAGENT-E)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-F)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(MATERIALSAGENT-E)
(Flag281-)
(Flag160-)
(Flag38-)
(Flag35-)
(Flag33-)
(ERROR-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag39-)
(Flag36-)
(Flag34-)
(Flag31-)
(ELECTRONICSAGENT-C)
(ELECTRONICSAGENT-D)
(ELECTRONICSAGENT-F)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-D)
(SPECIFICATIONSAGENT-F)
(CODINGAGENT-C)
(CODINGAGENT-D)
(CODINGAGENT-F)
(TESTINGAGENT-C)
(TESTINGAGENT-D)
(TESTINGAGENT-F)
(DESIGNAGENT-C)
(DESIGNAGENT-D)
(DESIGNAGENT-F)
(TASKAGENT-C)
(TASKAGENT-D)
(TASKAGENT-F)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-D)
(SOFTWAREAGENT-F)
(MATERIALSAGENT-C)
(MATERIALSAGENT-D)
(MATERIALSAGENT-F)
(Flag32-)
(NOT-CHECKCONSISTENCY-)
(NOT-CODINGAGENT-F)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-F)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICSAGENT-F)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-C)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-F)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-E)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-F)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-E)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-CODINGAGENT-E)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-E)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ELECTRONICSAGENT-E)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
)
(:derived (Flag32-)(and (Flag31-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag33-)(and (MATERIALSAGENT-F)))

(:derived (Flag33-)(and (SPECIFICATIONSAGENT-F)))

(:derived (Flag33-)(and (ELECTRONICSAGENT-F)))

(:derived (Flag33-)(and (CODINGAGENT-F)))

(:derived (Flag33-)(and (DESIGNAGENT-F)))

(:derived (Flag33-)(and (TESTINGAGENT-F)))

(:derived (Flag33-)(and (TASKAGENT-F)))

(:derived (Flag33-)(and (SOFTWAREAGENT-F)))

(:derived (Flag35-)(and (MATERIALSAGENT-D)))

(:derived (Flag35-)(and (SPECIFICATIONSAGENT-D)))

(:derived (Flag35-)(and (ELECTRONICSAGENT-D)))

(:derived (Flag35-)(and (CODINGAGENT-D)))

(:derived (Flag35-)(and (DESIGNAGENT-D)))

(:derived (Flag35-)(and (TESTINGAGENT-D)))

(:derived (Flag35-)(and (TASKAGENT-D)))

(:derived (Flag35-)(and (SOFTWAREAGENT-D)))

(:derived (Flag38-)(and (MATERIALSAGENT-C)))

(:derived (Flag38-)(and (SPECIFICATIONSAGENT-C)))

(:derived (Flag38-)(and (ELECTRONICSAGENT-C)))

(:derived (Flag38-)(and (CODINGAGENT-C)))

(:derived (Flag38-)(and (DESIGNAGENT-C)))

(:derived (Flag38-)(and (TESTINGAGENT-C)))

(:derived (Flag38-)(and (TASKAGENT-C)))

(:derived (Flag38-)(and (SOFTWAREAGENT-C)))

(:action ASSIGNMATERIALSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(MATERIALSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(SOFTWAREAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(TASKAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(DESIGNAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(TESTINGAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(CODINGAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(SPECIFICATIONSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(ELECTRONICSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag31-)(and (Flag1-)))

(:derived (Flag31-)(and (Flag2-)))

(:derived (Flag31-)(and (Flag3-)))

(:derived (Flag31-)(and (Flag4-)))

(:derived (Flag31-)(and (Flag5-)))

(:derived (Flag31-)(and (Flag6-)))

(:derived (Flag31-)(and (Flag7-)))

(:derived (Flag31-)(and (Flag8-)))

(:derived (Flag31-)(and (Flag9-)))

(:derived (Flag31-)(and (Flag10-)))

(:derived (Flag31-)(and (Flag11-)))

(:derived (Flag31-)(and (Flag12-)))

(:derived (Flag31-)(and (Flag13-)))

(:derived (Flag31-)(and (Flag14-)))

(:derived (Flag31-)(and (Flag15-)))

(:derived (Flag31-)(and (Flag16-)))

(:derived (Flag31-)(and (Flag17-)))

(:derived (Flag31-)(and (Flag18-)))

(:derived (Flag31-)(and (Flag19-)))

(:derived (Flag31-)(and (Flag20-)))

(:derived (Flag31-)(and (Flag21-)))

(:derived (Flag31-)(and (Flag22-)))

(:derived (Flag31-)(and (Flag23-)))

(:derived (Flag31-)(and (Flag24-)))

(:derived (Flag31-)(and (Flag25-)))

(:derived (Flag31-)(and (Flag26-)))

(:derived (Flag31-)(and (Flag27-)))

(:derived (Flag31-)(and (Flag28-)))

(:derived (Flag31-)(and (Flag29-)))

(:derived (Flag31-)(and (Flag30-)))

(:derived (Flag34-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag33-)))

(:derived (Flag36-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag35-)))

(:derived (Flag39-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag38-)))

(:derived (Flag1-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-A)))

(:derived (Flag2-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-A)))

(:derived (Flag3-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-A)))

(:derived (Flag4-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-A)))

(:derived (Flag5-)(and (ELECTRONICENGINEER-F)(ELECTRONICENGINEER-A)))

(:derived (Flag6-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-C)))

(:derived (Flag7-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-C)))

(:derived (Flag8-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-C)))

(:derived (Flag9-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-C)))

(:derived (Flag10-)(and (ELECTRONICENGINEER-F)(ELECTRONICENGINEER-C)))

(:derived (Flag11-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-B)))

(:derived (Flag12-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-B)))

(:derived (Flag13-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-B)))

(:derived (Flag14-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-B)))

(:derived (Flag15-)(and (ELECTRONICENGINEER-F)(ELECTRONICENGINEER-B)))

(:derived (Flag16-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-E)))

(:derived (Flag17-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-E)))

(:derived (Flag18-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-E)))

(:derived (Flag19-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-E)))

(:derived (Flag20-)(and (ELECTRONICENGINEER-F)(ELECTRONICENGINEER-E)))

(:derived (Flag21-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-D)))

(:derived (Flag22-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-D)))

(:derived (Flag23-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-D)))

(:derived (Flag24-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-D)))

(:derived (Flag25-)(and (ELECTRONICENGINEER-F)(ELECTRONICENGINEER-D)))

(:derived (Flag26-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-F)))

(:derived (Flag27-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-F)))

(:derived (Flag28-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-F)))

(:derived (Flag29-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-F)))

(:derived (Flag30-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-F)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-E)
(ELECTRONICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-F)
(ELECTRONICENGINEER-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(CODINGAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(CODINGAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-E)
(CODINGAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-F)
(CODINGAGENT-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-E)
(INFORMATICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-F)
(INFORMATICENGINEER-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag33-)(and (INFORMATICENGINEER-F)))

(:derived (Flag33-)(and (ELECTRONICENGINEER-F)))

(:derived (Flag35-)(and (INFORMATICENGINEER-D)))

(:derived (Flag35-)(and (ELECTRONICENGINEER-D)))

(:derived (Flag38-)(and (INFORMATICENGINEER-C)))

(:derived (Flag38-)(and (ELECTRONICENGINEER-C)))

(:derived (Flag160-)(and (Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)(Flag85-)(Flag86-)(Flag87-)(Flag88-)(Flag89-)(Flag90-)(Flag91-)(Flag92-)(Flag93-)(Flag94-)(Flag95-)(Flag96-)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)(Flag122-)(Flag123-)(Flag124-)(Flag125-)(Flag126-)(Flag127-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)(Flag139-)(Flag140-)(Flag141-)(Flag142-)(Flag143-)(Flag144-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)(Flag150-)(Flag151-)(Flag152-)(Flag153-)(Flag154-)(Flag155-)(Flag156-)(Flag157-)(Flag158-)(Flag159-)))

(:derived (Flag281-)(and (Flag161-)(Flag162-)(Flag163-)(Flag164-)(Flag165-)(Flag166-)(Flag167-)(Flag168-)(Flag169-)(Flag170-)(Flag171-)(Flag172-)(Flag173-)(Flag174-)(Flag175-)(Flag176-)(Flag177-)(Flag178-)(Flag179-)(Flag180-)(Flag181-)(Flag182-)(Flag183-)(Flag184-)(Flag185-)(Flag186-)(Flag187-)(Flag188-)(Flag189-)(Flag190-)(Flag191-)(Flag192-)(Flag193-)(Flag194-)(Flag195-)(Flag196-)(Flag197-)(Flag198-)(Flag199-)(Flag200-)(Flag201-)(Flag202-)(Flag203-)(Flag204-)(Flag205-)(Flag206-)(Flag207-)(Flag208-)(Flag209-)(Flag210-)(Flag211-)(Flag212-)(Flag213-)(Flag214-)(Flag215-)(Flag216-)(Flag217-)(Flag218-)(Flag219-)(Flag220-)(Flag221-)(Flag222-)(Flag223-)(Flag224-)(Flag225-)(Flag226-)(Flag227-)(Flag228-)(Flag229-)(Flag230-)(Flag231-)(Flag232-)(Flag233-)(Flag234-)(Flag235-)(Flag236-)(Flag237-)(Flag238-)(Flag239-)(Flag240-)(Flag241-)(Flag242-)(Flag243-)(Flag244-)(Flag245-)(Flag246-)(Flag247-)(Flag248-)(Flag249-)(Flag250-)(Flag251-)(Flag252-)(Flag253-)(Flag254-)(Flag255-)(Flag256-)(Flag257-)(Flag258-)(Flag259-)(Flag260-)(Flag261-)(Flag262-)(Flag263-)(Flag264-)(Flag265-)(Flag266-)(Flag267-)(Flag268-)(Flag269-)(Flag270-)(Flag271-)(Flag272-)(Flag273-)(Flag274-)(Flag275-)(Flag276-)(Flag277-)(Flag278-)(Flag279-)(Flag280-)))

(:action ASSIGNMATERIALSAGENT-E
:parameters ()
:precondition
(and
(Flag37-)
)
:effect
(and
(MATERIALSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-F)
(not (NOT-ELECTRONICENGINEER-F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-E)
(not (NOT-ELECTRONICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-F)
(not (NOT-INFORMATICENGINEER-F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-E)
(not (NOT-INFORMATICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag37-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag90-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag90-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag90-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag91-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag91-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag91-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag92-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag92-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag92-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag93-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag93-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag93-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag94-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag94-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag94-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag95-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag95-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag95-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag96-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag96-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag96-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag97-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag97-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag97-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag98-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag98-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag98-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag99-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag99-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag99-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag100-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag100-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag100-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag101-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag101-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag101-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag102-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag102-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag102-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag103-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag103-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag103-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag104-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag104-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag104-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag105-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag105-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag105-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag106-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag106-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag106-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag107-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag107-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag107-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag108-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag108-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag108-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag109-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag109-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag109-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag110-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag110-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag110-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag111-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag111-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag111-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag112-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag112-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag112-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag113-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag113-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag113-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag114-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag114-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag114-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag115-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag115-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag115-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag116-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag116-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag116-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag117-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag117-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag117-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag118-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag118-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag118-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag119-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag119-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag119-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag120-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag120-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag120-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag121-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag121-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag121-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag122-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag122-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag122-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag123-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag123-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag123-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag124-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag124-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag124-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag125-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag125-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag125-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag126-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag126-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag126-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag127-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag127-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag127-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag128-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag128-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag128-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag129-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag129-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag129-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag130-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag130-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag130-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag131-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag131-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag131-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag132-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag132-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag132-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag133-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag133-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag133-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag134-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag134-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag134-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag135-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag135-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag135-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag136-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag136-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag136-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag137-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag137-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag137-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag138-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag138-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag138-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag139-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag139-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag139-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag140-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag140-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag140-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag141-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag141-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag141-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag142-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag142-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag142-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag143-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag143-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag143-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag144-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag144-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag144-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag145-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag145-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag145-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag146-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag146-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag146-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag147-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag147-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag147-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag148-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag148-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag148-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag149-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag149-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag149-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag150-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag150-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag150-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag151-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag151-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag151-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag152-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag152-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag152-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag153-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag153-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag153-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag154-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag154-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag154-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag155-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag155-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag155-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag156-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag156-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag156-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag157-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag157-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag157-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag158-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag158-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag158-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag159-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag159-)(and (NOT-INFORMATICENGINEER-F)))

(:derived (Flag159-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag161-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag161-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag161-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag162-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag162-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag162-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag163-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag163-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag163-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag164-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag164-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag164-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag165-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag165-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag165-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag166-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag166-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag166-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag167-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag167-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag167-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag168-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag168-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag168-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag169-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag169-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag169-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag170-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag170-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag170-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag171-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag171-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag171-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag172-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag172-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag172-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag173-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag173-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag173-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag174-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag174-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag174-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag175-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag175-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag175-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag176-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag176-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag176-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag177-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag177-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag177-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag178-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag178-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag178-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag179-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag179-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag179-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag180-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag180-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag180-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag181-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag181-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag181-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag182-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag182-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag182-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag183-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag183-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag183-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag184-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag184-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag184-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag185-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag185-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag185-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag186-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag186-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag186-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag187-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag187-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag187-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag188-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag188-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag188-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag189-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag189-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag189-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag190-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag190-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag190-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag191-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag191-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag191-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag192-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag192-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag192-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag193-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag193-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag193-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag194-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag194-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag194-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag195-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag195-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag195-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag196-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag196-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag196-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag197-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag197-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag197-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag198-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag198-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag198-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag199-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag199-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag199-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag200-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag200-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag200-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag201-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag201-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag201-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag202-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag202-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag202-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag203-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag203-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag203-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag204-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag204-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag204-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag205-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag205-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag205-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag206-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag206-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag206-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag207-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag207-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag207-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag208-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag208-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag208-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag209-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag209-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag209-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag210-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag210-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag210-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag211-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag211-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag211-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag212-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag212-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag212-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag213-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag213-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag213-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag214-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag214-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag214-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag215-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag215-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag215-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag216-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag216-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag216-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag217-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag217-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag217-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag218-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag218-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag218-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag219-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag219-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag219-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag220-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag220-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag220-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag221-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag221-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag221-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag222-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag222-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag222-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag223-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag223-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag223-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag224-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag224-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag224-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag225-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag225-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag225-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag226-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag226-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag226-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag227-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag227-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag227-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag228-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag228-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag228-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag229-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag229-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag229-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag230-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag230-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag230-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag231-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag231-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag231-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag232-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag232-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag232-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag233-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag233-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag233-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag234-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag234-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag234-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag235-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag235-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag235-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag236-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag236-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag236-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag237-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag237-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag237-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag238-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag238-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag238-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag239-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag239-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag239-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag240-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag240-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag240-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag241-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag241-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag241-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag242-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag242-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag242-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag243-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag243-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag243-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag244-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag244-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag244-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag245-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag245-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag245-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag246-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag246-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag246-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag247-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag247-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag247-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag248-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag248-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag248-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag249-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag249-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag249-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag250-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag250-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag250-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag251-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag251-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag251-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag252-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag252-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag252-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag253-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag253-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag253-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag254-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag254-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag254-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag255-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag255-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag255-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag256-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag256-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag256-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag257-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag257-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag257-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag258-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag258-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag258-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag259-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag259-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag259-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag260-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag260-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag260-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag261-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag261-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag261-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag262-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag262-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag262-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag263-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag263-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag263-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag264-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag264-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag264-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag265-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag265-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag265-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag266-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag266-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag266-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag267-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag267-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag267-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag268-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag268-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag268-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag269-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag269-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag269-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag270-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag270-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag270-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag271-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag271-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag271-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag272-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag272-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag272-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag273-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag273-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag273-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag274-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag274-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag274-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag275-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag275-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag275-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag276-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag276-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag276-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag277-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag277-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag277-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag278-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag278-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag278-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag279-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag279-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag279-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag280-)(and (NOT-ELECTRONICENGINEER-F)))

(:derived (Flag280-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag280-)(and (NOT-ELECTRONICENGINEER-E)))

)
