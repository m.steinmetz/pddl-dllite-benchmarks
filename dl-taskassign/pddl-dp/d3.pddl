(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag11-)
(ELECTRONICENGINEER-A)
(CHECKCONSISTENCY-)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(SPECIFICATIONSAGENT-C)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(MATERIALSAGENT-C)
(TESTINGAGENT-C)
(TASKAGENT-C)
(CODINGAGENT-C)
(DESIGNAGENT-C)
(ELECTRONICSAGENT-C)
(SOFTWAREAGENT-C)
(Flag27-)
(Flag20-)
(Flag12-)
(Flag9-)
(ERROR-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag13-)
(Flag10-)
(Flag7-)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TASKAGENT-A)
(TASKAGENT-B)
(CODINGAGENT-A)
(CODINGAGENT-B)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(ELECTRONICSAGENT-A)
(ELECTRONICSAGENT-B)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(Flag8-)
(NOT-CHECKCONSISTENCY-)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ERROR-)
(NOT-ELECTRONICSAGENT-C)
(NOT-CODINGAGENT-C)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
)
(:derived (Flag8-)(and (Flag7-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag9-)(and (MATERIALSAGENT-B)))

(:derived (Flag9-)(and (TASKAGENT-B)))

(:derived (Flag9-)(and (SPECIFICATIONSAGENT-B)))

(:derived (Flag9-)(and (ELECTRONICSAGENT-B)))

(:derived (Flag9-)(and (CODINGAGENT-B)))

(:derived (Flag9-)(and (DESIGNAGENT-B)))

(:derived (Flag9-)(and (TESTINGAGENT-B)))

(:derived (Flag9-)(and (SOFTWAREAGENT-B)))

(:derived (Flag12-)(and (MATERIALSAGENT-A)))

(:derived (Flag12-)(and (TASKAGENT-A)))

(:derived (Flag12-)(and (SPECIFICATIONSAGENT-A)))

(:derived (Flag12-)(and (ELECTRONICSAGENT-A)))

(:derived (Flag12-)(and (CODINGAGENT-A)))

(:derived (Flag12-)(and (DESIGNAGENT-A)))

(:derived (Flag12-)(and (TESTINGAGENT-A)))

(:derived (Flag12-)(and (SOFTWAREAGENT-A)))

(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag7-)(and (Flag1-)))

(:derived (Flag7-)(and (Flag2-)))

(:derived (Flag7-)(and (Flag3-)))

(:derived (Flag7-)(and (Flag4-)))

(:derived (Flag7-)(and (Flag5-)))

(:derived (Flag7-)(and (Flag6-)))

(:derived (Flag10-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag9-)))

(:derived (Flag13-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag12-)))

(:derived (Flag1-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-A)))

(:derived (Flag2-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-A)))

(:derived (Flag3-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-C)))

(:derived (Flag4-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-C)))

(:derived (Flag5-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-B)))

(:derived (Flag6-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-B)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(SPECIFICATIONSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag9-)(and (INFORMATICENGINEER-B)))

(:derived (Flag9-)(and (ELECTRONICENGINEER-B)))

(:derived (Flag12-)(and (INFORMATICENGINEER-A)))

(:derived (Flag12-)(and (ELECTRONICENGINEER-A)))

(:derived (Flag20-)(and (Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)))

(:derived (Flag27-)(and (Flag21-)(Flag22-)(Flag23-)(Flag24-)(Flag25-)(Flag26-)))

(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag11-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag14-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag14-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag14-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag15-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag15-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag15-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag16-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag16-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag16-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag17-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag17-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag17-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag18-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag18-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag18-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag19-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag19-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag19-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-C)))

)
