(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag15-)
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-D)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-D)
(DESIGNAGENT-A)
(DESIGNAGENT-D)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-D)
(TESTINGAGENT-A)
(TESTINGAGENT-D)
(MATERIALSAGENT-A)
(MATERIALSAGENT-D)
(CODINGAGENT-A)
(CODINGAGENT-D)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-D)
(TASKAGENT-A)
(TASKAGENT-D)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-D)
(Flag69-)
(Flag44-)
(Flag18-)
(Flag16-)
(ERROR-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag19-)
(Flag17-)
(Flag13-)
(ELECTRONICSAGENT-C)
(ELECTRONICSAGENT-B)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-B)
(DESIGNAGENT-C)
(DESIGNAGENT-B)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-B)
(TESTINGAGENT-C)
(TESTINGAGENT-B)
(MATERIALSAGENT-C)
(MATERIALSAGENT-B)
(CODINGAGENT-C)
(CODINGAGENT-B)
(TASKAGENT-C)
(TASKAGENT-B)
(Flag14-)
(NOT-CHECKCONSISTENCY-)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-SOFTWAREAGENT-B)
(NOT-SOFTWAREAGENT-C)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-C)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-SOFTWAREAGENT-D)
(NOT-SOFTWAREAGENT-A)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-A)
)
(:derived (Flag14-)(and (Flag13-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag16-)(and (MATERIALSAGENT-B)))

(:derived (Flag16-)(and (SPECIFICATIONSAGENT-B)))

(:derived (Flag16-)(and (ELECTRONICSAGENT-B)))

(:derived (Flag16-)(and (CODINGAGENT-B)))

(:derived (Flag16-)(and (DESIGNAGENT-B)))

(:derived (Flag16-)(and (TESTINGAGENT-B)))

(:derived (Flag16-)(and (TASKAGENT-B)))

(:derived (Flag16-)(and (SOFTWAREAGENT-B)))

(:derived (Flag18-)(and (MATERIALSAGENT-C)))

(:derived (Flag18-)(and (SPECIFICATIONSAGENT-C)))

(:derived (Flag18-)(and (ELECTRONICSAGENT-C)))

(:derived (Flag18-)(and (CODINGAGENT-C)))

(:derived (Flag18-)(and (DESIGNAGENT-C)))

(:derived (Flag18-)(and (TESTINGAGENT-C)))

(:derived (Flag18-)(and (TASKAGENT-C)))

(:derived (Flag18-)(and (SOFTWAREAGENT-C)))

(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag13-)(and (Flag1-)))

(:derived (Flag13-)(and (Flag2-)))

(:derived (Flag13-)(and (Flag3-)))

(:derived (Flag13-)(and (Flag4-)))

(:derived (Flag13-)(and (Flag5-)))

(:derived (Flag13-)(and (Flag6-)))

(:derived (Flag13-)(and (Flag7-)))

(:derived (Flag13-)(and (Flag8-)))

(:derived (Flag13-)(and (Flag9-)))

(:derived (Flag13-)(and (Flag10-)))

(:derived (Flag13-)(and (Flag11-)))

(:derived (Flag13-)(and (Flag12-)))

(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag16-)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag18-)))

(:derived (Flag1-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-A)))

(:derived (Flag2-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-A)))

(:derived (Flag3-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-A)))

(:derived (Flag4-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-C)))

(:derived (Flag5-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-C)))

(:derived (Flag6-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-C)))

(:derived (Flag7-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-B)))

(:derived (Flag8-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-B)))

(:derived (Flag9-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-B)))

(:derived (Flag10-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-D)))

(:derived (Flag11-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-D)))

(:derived (Flag12-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-D)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SOFTWAREAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(CODINGAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(CODINGAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag16-)(and (INFORMATICENGINEER-B)))

(:derived (Flag16-)(and (ELECTRONICENGINEER-B)))

(:derived (Flag18-)(and (INFORMATICENGINEER-C)))

(:derived (Flag18-)(and (ELECTRONICENGINEER-C)))

(:derived (Flag44-)(and (Flag20-)(Flag21-)(Flag22-)(Flag23-)(Flag24-)(Flag25-)(Flag26-)(Flag27-)(Flag28-)(Flag29-)(Flag30-)(Flag31-)(Flag32-)(Flag33-)(Flag34-)(Flag35-)(Flag36-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)))

(:derived (Flag69-)(and (Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)))

(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag20-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag20-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag20-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag21-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag22-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag23-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag24-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag25-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag26-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag27-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag27-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag27-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag28-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag28-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag28-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag29-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag29-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag29-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag45-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag45-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag45-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag46-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag46-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag46-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag47-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag47-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag47-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag48-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag48-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag48-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag49-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag49-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag49-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag50-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag50-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag50-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag51-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag51-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag51-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag52-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag52-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag52-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag53-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag53-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag53-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag54-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag54-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag54-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag55-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag55-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag55-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag56-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag56-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag56-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag57-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag57-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag57-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag58-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag58-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag58-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag59-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag59-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag59-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag60-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag60-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag60-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag61-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag61-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag61-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag62-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag62-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag62-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag63-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag63-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag63-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag64-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag64-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag64-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag65-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag65-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag65-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag66-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag66-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag66-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag67-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag67-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag67-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag68-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag68-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag68-)(and (NOT-ELECTRONICENGINEER-C)))

)
