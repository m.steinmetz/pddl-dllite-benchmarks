(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag23-)
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-D)
(DESIGNAGENT-C)
(DESIGNAGENT-D)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-D)
(MATERIALSAGENT-C)
(MATERIALSAGENT-D)
(TASKAGENT-C)
(TASKAGENT-D)
(CODINGAGENT-C)
(CODINGAGENT-D)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-D)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-E)
(INFORMATICENGINEER-D)
(TESTINGAGENT-C)
(TESTINGAGENT-D)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
(Flag151-)
(Flag90-)
(Flag28-)
(Flag26-)
(Flag24-)
(ERROR-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag21-)
(ELECTRONICSAGENT-A)
(ELECTRONICSAGENT-B)
(ELECTRONICSAGENT-E)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(DESIGNAGENT-E)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(SPECIFICATIONSAGENT-E)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(MATERIALSAGENT-E)
(TASKAGENT-A)
(TASKAGENT-B)
(TASKAGENT-E)
(CODINGAGENT-A)
(CODINGAGENT-B)
(CODINGAGENT-E)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(SOFTWAREAGENT-E)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TESTINGAGENT-E)
(Flag22-)
(NOT-CHECKCONSISTENCY-)
(NOT-SOFTWAREAGENT-E)
(NOT-SOFTWAREAGENT-B)
(NOT-SOFTWAREAGENT-A)
(NOT-CODINGAGENT-E)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-E)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ELECTRONICSAGENT-E)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-E)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-E)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-SOFTWAREAGENT-D)
(NOT-SOFTWAREAGENT-C)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-C)
)
(:derived (Flag22-)(and (Flag21-)(NOT-ERROR-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag24-)(and (MATERIALSAGENT-E)))

(:derived (Flag24-)(and (SPECIFICATIONSAGENT-E)))

(:derived (Flag24-)(and (ELECTRONICSAGENT-E)))

(:derived (Flag24-)(and (CODINGAGENT-E)))

(:derived (Flag24-)(and (DESIGNAGENT-E)))

(:derived (Flag24-)(and (TESTINGAGENT-E)))

(:derived (Flag24-)(and (TASKAGENT-E)))

(:derived (Flag24-)(and (SOFTWAREAGENT-E)))

(:derived (Flag26-)(and (MATERIALSAGENT-B)))

(:derived (Flag26-)(and (SPECIFICATIONSAGENT-B)))

(:derived (Flag26-)(and (ELECTRONICSAGENT-B)))

(:derived (Flag26-)(and (CODINGAGENT-B)))

(:derived (Flag26-)(and (DESIGNAGENT-B)))

(:derived (Flag26-)(and (TESTINGAGENT-B)))

(:derived (Flag26-)(and (TASKAGENT-B)))

(:derived (Flag26-)(and (SOFTWAREAGENT-B)))

(:derived (Flag28-)(and (MATERIALSAGENT-A)))

(:derived (Flag28-)(and (SPECIFICATIONSAGENT-A)))

(:derived (Flag28-)(and (ELECTRONICSAGENT-A)))

(:derived (Flag28-)(and (CODINGAGENT-A)))

(:derived (Flag28-)(and (DESIGNAGENT-A)))

(:derived (Flag28-)(and (TESTINGAGENT-A)))

(:derived (Flag28-)(and (TASKAGENT-A)))

(:derived (Flag28-)(and (SOFTWAREAGENT-A)))

(:action ASSIGNTESTINGAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(TESTINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(SOFTWAREAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CODINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(TASKAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(MATERIALSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(SPECIFICATIONSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(DESIGNAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(ELECTRONICSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag21-)(and (Flag1-)))

(:derived (Flag21-)(and (Flag2-)))

(:derived (Flag21-)(and (Flag3-)))

(:derived (Flag21-)(and (Flag4-)))

(:derived (Flag21-)(and (Flag5-)))

(:derived (Flag21-)(and (Flag6-)))

(:derived (Flag21-)(and (Flag7-)))

(:derived (Flag21-)(and (Flag8-)))

(:derived (Flag21-)(and (Flag9-)))

(:derived (Flag21-)(and (Flag10-)))

(:derived (Flag21-)(and (Flag11-)))

(:derived (Flag21-)(and (Flag12-)))

(:derived (Flag21-)(and (Flag13-)))

(:derived (Flag21-)(and (Flag14-)))

(:derived (Flag21-)(and (Flag15-)))

(:derived (Flag21-)(and (Flag16-)))

(:derived (Flag21-)(and (Flag17-)))

(:derived (Flag21-)(and (Flag18-)))

(:derived (Flag21-)(and (Flag19-)))

(:derived (Flag21-)(and (Flag20-)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag24-)))

(:derived (Flag27-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag26-)))

(:derived (Flag29-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)(Flag28-)))

(:derived (Flag1-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-A)))

(:derived (Flag2-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-A)))

(:derived (Flag3-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-A)))

(:derived (Flag4-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-A)))

(:derived (Flag5-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-C)))

(:derived (Flag6-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-C)))

(:derived (Flag7-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-C)))

(:derived (Flag8-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-C)))

(:derived (Flag9-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-B)))

(:derived (Flag10-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-B)))

(:derived (Flag11-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-B)))

(:derived (Flag12-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-B)))

(:derived (Flag13-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-E)))

(:derived (Flag14-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-E)))

(:derived (Flag15-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-E)))

(:derived (Flag16-)(and (ELECTRONICENGINEER-D)(ELECTRONICENGINEER-E)))

(:derived (Flag17-)(and (ELECTRONICENGINEER-A)(ELECTRONICENGINEER-D)))

(:derived (Flag18-)(and (ELECTRONICENGINEER-C)(ELECTRONICENGINEER-D)))

(:derived (Flag19-)(and (ELECTRONICENGINEER-B)(ELECTRONICENGINEER-D)))

(:derived (Flag20-)(and (ELECTRONICENGINEER-E)(ELECTRONICENGINEER-D)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(SPECIFICATIONSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-E)
(ELECTRONICSAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-E)
(INFORMATICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-E)
(ELECTRONICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SOFTWAREAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:derived (Flag24-)(and (INFORMATICENGINEER-E)))

(:derived (Flag24-)(and (ELECTRONICENGINEER-E)))

(:derived (Flag26-)(and (INFORMATICENGINEER-B)))

(:derived (Flag26-)(and (ELECTRONICENGINEER-B)))

(:derived (Flag28-)(and (INFORMATICENGINEER-A)))

(:derived (Flag28-)(and (ELECTRONICENGINEER-A)))

(:derived (Flag90-)(and (Flag30-)(Flag31-)(Flag32-)(Flag33-)(Flag34-)(Flag35-)(Flag36-)(Flag37-)(Flag38-)(Flag39-)(Flag40-)(Flag41-)(Flag42-)(Flag43-)(Flag44-)(Flag45-)(Flag46-)(Flag47-)(Flag48-)(Flag49-)(Flag50-)(Flag51-)(Flag52-)(Flag53-)(Flag54-)(Flag55-)(Flag56-)(Flag57-)(Flag58-)(Flag59-)(Flag60-)(Flag61-)(Flag62-)(Flag63-)(Flag64-)(Flag65-)(Flag66-)(Flag67-)(Flag68-)(Flag69-)(Flag70-)(Flag71-)(Flag72-)(Flag73-)(Flag74-)(Flag75-)(Flag76-)(Flag77-)(Flag78-)(Flag79-)(Flag80-)(Flag81-)(Flag82-)(Flag83-)(Flag84-)(Flag85-)(Flag86-)(Flag87-)(Flag88-)(Flag89-)))

(:derived (Flag151-)(and (Flag91-)(Flag92-)(Flag93-)(Flag94-)(Flag95-)(Flag96-)(Flag97-)(Flag98-)(Flag99-)(Flag100-)(Flag101-)(Flag102-)(Flag103-)(Flag104-)(Flag105-)(Flag106-)(Flag107-)(Flag108-)(Flag109-)(Flag110-)(Flag111-)(Flag112-)(Flag113-)(Flag114-)(Flag115-)(Flag116-)(Flag117-)(Flag118-)(Flag119-)(Flag120-)(Flag121-)(Flag122-)(Flag123-)(Flag124-)(Flag125-)(Flag126-)(Flag127-)(Flag128-)(Flag129-)(Flag130-)(Flag131-)(Flag132-)(Flag133-)(Flag134-)(Flag135-)(Flag136-)(Flag137-)(Flag138-)(Flag139-)(Flag140-)(Flag141-)(Flag142-)(Flag143-)(Flag144-)(Flag145-)(Flag146-)(Flag147-)(Flag148-)(Flag149-)(Flag150-)))

(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-E)
(not (NOT-ELECTRONICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-E)
(not (NOT-INFORMATICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(NOT-ERROR-)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag30-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag31-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag32-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag33-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag34-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag35-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag36-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag37-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag38-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag39-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag40-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag41-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag42-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag43-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag44-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag45-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag46-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag47-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag48-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag49-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag50-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag51-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag52-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag53-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag54-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag55-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag56-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag57-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag58-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag59-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag60-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag61-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag62-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag63-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag64-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag65-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag66-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag67-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag68-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag69-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag70-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag71-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag72-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag73-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag74-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag75-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag76-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag77-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag78-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag79-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag80-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag81-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag82-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag83-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag84-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag85-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag86-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag87-)(and (NOT-INFORMATICENGINEER-A)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag88-)(and (NOT-INFORMATICENGINEER-C)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-D)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-E)))

(:derived (Flag89-)(and (NOT-INFORMATICENGINEER-B)))

(:derived (Flag91-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag91-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag91-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag92-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag92-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag92-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag93-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag93-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag93-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag94-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag94-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag94-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag95-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag95-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag95-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag96-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag96-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag96-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag97-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag97-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag97-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag98-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag98-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag98-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag99-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag99-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag99-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag100-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag100-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag100-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag101-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag101-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag101-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag102-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag102-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag102-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag103-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag103-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag103-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag104-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag104-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag104-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag105-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag105-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag105-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag106-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag106-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag106-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag107-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag107-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag107-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag108-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag108-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag108-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag109-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag109-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag109-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag110-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag110-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag110-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag111-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag111-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag111-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag112-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag112-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag112-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag113-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag113-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag113-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag114-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag114-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag114-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag115-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag115-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag115-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag116-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag116-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag116-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag117-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag117-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag117-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag118-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag118-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag118-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag119-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag119-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag119-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag120-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag120-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag120-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag121-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag121-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag121-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag122-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag122-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag122-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag123-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag123-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag123-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag124-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag124-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag124-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag125-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag125-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag125-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag126-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag126-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag126-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag127-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag127-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag127-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag128-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag128-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag128-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag129-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag129-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag129-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag130-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag130-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag130-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag131-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag131-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag131-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag132-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag132-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag132-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag133-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag133-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag133-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag134-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag134-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag134-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag135-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag135-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag135-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag136-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag136-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag136-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag137-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag137-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag137-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag138-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag138-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag138-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag139-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag139-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag139-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag140-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag140-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag140-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag141-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag141-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag141-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag142-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag142-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag142-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag143-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag143-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag143-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag144-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag144-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag144-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag145-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag145-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag145-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag146-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag146-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag146-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag147-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag147-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag147-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag148-)(and (NOT-ELECTRONICENGINEER-A)))

(:derived (Flag148-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag148-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag149-)(and (NOT-ELECTRONICENGINEER-C)))

(:derived (Flag149-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag149-)(and (NOT-ELECTRONICENGINEER-E)))

(:derived (Flag150-)(and (NOT-ELECTRONICENGINEER-B)))

(:derived (Flag150-)(and (NOT-ELECTRONICENGINEER-D)))

(:derived (Flag150-)(and (NOT-ELECTRONICENGINEER-E)))

)
