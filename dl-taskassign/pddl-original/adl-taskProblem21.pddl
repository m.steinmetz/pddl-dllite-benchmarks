(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p s r u t 
  )
  (:init
    (Engineer k)
    (Developer h)
    (Engineer n)
    (Developer d)
    (Developer a)
    (Engineer f)
    (Developer t)
    (Developer o)
    (Designer i)
    (Engineer p)
    (Designer g)
    (Developer s)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)