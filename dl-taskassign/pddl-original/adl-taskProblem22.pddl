(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p s r u t v 
  )
  (:init
    (Engineer h)
    (Engineer i)
    (Engineer k)
    (Engineer o)
    (Designer t)
    (Designer v)
    (Engineer f)
    (Designer j)
    (Engineer p)
    (Designer e)
    (Engineer r)
    (Designer b)
    (Designer c)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)