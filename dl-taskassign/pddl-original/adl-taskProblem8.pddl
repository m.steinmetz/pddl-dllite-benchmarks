(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f h 
  )
  (:init
    (Engineer h)
    (Engineer a)
    (Designer f)
    (Engineer g)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (not
            (=?x ?y )
          )
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
        )
      )
    )
  )
)