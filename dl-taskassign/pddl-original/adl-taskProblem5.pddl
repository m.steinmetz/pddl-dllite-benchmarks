(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d 
  )
  (:init
    (Designer d)
    (Developer c)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)