(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l n 
  )
  (:init
    (Developer i)
    (Developer j)
    (Engineer b)
    (Engineer m)
    (Engineer g)
    (Designer n)
    (Designer h)
    (Designer c)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (not
            (=?x ?y )
          )
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
        )
      )
    )
  )
)