(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h j 
  )
  (:init
    (Engineer h)
    (Engineer a)
    (Developer f)
    (Developer i)
    (Designer j)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)