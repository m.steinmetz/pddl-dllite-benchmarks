(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n p 
  )
  (:init
    (Engineer j)
    (Developer h)
    (Engineer m)
    (Developer b)
    (Developer c)
    (Designer i)
    (Designer d)
    (Designer e)
    (Designer f)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)