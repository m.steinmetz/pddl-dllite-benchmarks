(define
  (domain taskAssigment)
  (:requirements :adl)
  (:predicates
    (ElectronicsAgent ?x )
    (Designer ?x )
    (SoftwareAgent ?x )
    (InformaticEngineer ?x )
    (MaterialsAgent ?x )
    (DesignAgent ?x )
    (TaskAgent ?x )
    (MaterialsEngineer ?x )
    (CodingAgent ?x )
    (TestingAgent ?x )
    (ElectronicEngineer ?x )
    (Employee ?x )
    (FullName ?x )
    (Engineer ?x )
    (SpecificationsAgent ?x )
    (Developer ?x )
    (hasPersonalInfo ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action assignSoftwareAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (SoftwareAgent ?x)
        )
      )
    )
  )
  (:action hireElectronicEng
    :parameters ( ?n )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (= a a)
    )
    :effect ( and
      (CheckConsistency)
      (when
        (not
          (exists ( ?x2 ?x3 ?x1 )
            (and
              (not
                (=?x2 ?x3 )
              )
              (ElectronicEngineer ?x1)
              (not
                (=?x1 ?x2 )
              )
              (ElectronicEngineer ?x3)
              (ElectronicEngineer ?x2)
              (not
                (=?x1 ?x3 )
              )
            )
          )
        )
        (and
          (ElectronicEngineer ?n)
        )
      )
    )
  )
  (:action assignSpecificationsAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (SpecificationsAgent ?x)
        )
      )
    )
  )
  (:action hireInformaticEng
    :parameters ( ?n )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (= a a)
    )
    :effect ( and
      (CheckConsistency)
      (when
        (not
          (exists ( ?x2 ?x3 ?x1 )
            (and
              (not
                (=?x1 ?x2 )
              )
              (not
                (=?x1 ?x3 )
              )
              (InformaticEngineer ?x1)
              (InformaticEngineer ?x2)
              (InformaticEngineer ?x3)
              (not
                (=?x2 ?x3 )
              )
            )
          )
        )
        (and
          (InformaticEngineer ?n)
        )
      )
    )
  )
  (:action assignMaterialsAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (MaterialsAgent ?x)
        )
      )
    )
  )
  (:action assignElectronicsAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (ElectronicsAgent ?x)
        )
      )
    )
  )
  (:action assignCodingAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (CodingAgent ?x)
        )
      )
    )
  )
  (:action removePersonalInfo
    :parameters ( ?x ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (hasPersonalInfo ?x ?y)
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (not (hasPersonalInfo ?x ?y))
        )
      )
    )
  )
  (:action assignTestingAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (TestingAgent ?x)
        )
      )
    )
  )
  (:action assignDesignAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (DesignAgent ?x)
        )
      )
    )
  )
  (:action assignTaskAgent
    :parameters ( ?x )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (MaterialsAgent ?x)
        (Developer ?x)
        (SpecificationsAgent ?x)
        (ElectronicsAgent ?x)
        (CodingAgent ?x)
        (DesignAgent ?x)
        (InformaticEngineer ?x)
        (Employee ?x)
        (TestingAgent ?x)
        (ElectronicEngineer ?x)
        (exists (?z_0 )
          (hasPersonalInfo ?x ?z_0)
        )
        (MaterialsEngineer ?x)
        (Designer ?x)
        (TaskAgent ?x)
        (Engineer ?x)
        (SoftwareAgent ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= a a)
        (and
          (TaskAgent ?x)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?z_0 ?x_6 )
            (and
              (Designer ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (FullName ?x_6)
              (MaterialsEngineer ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (hasPersonalInfo ?z_0 ?x_6)
              (MaterialsEngineer ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (ElectronicEngineer ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (SoftwareAgent ?x_1)
              (Designer ?x_1)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (MaterialsAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (FullName ?x_6)
              (DesignAgent ?x_6)
            )
          )
          (exists (?x_7 )
            (and
              (Developer ?x_7)
              (SpecificationsAgent ?x_7)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (InformaticEngineer ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (SoftwareAgent ?x_4)
              (MaterialsEngineer ?x_4)
            )
          )
          (exists (?x_1 )
            (and
              (CodingAgent ?x_1)
              (Designer ?x_1)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (DesignAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (Employee ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (SpecificationsAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (Developer ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (hasPersonalInfo ?x_6 ?z_0)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (TestingAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (FullName ?x_6)
              (TaskAgent ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (ElectronicsAgent ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?x_2 )
            (and
              (Designer ?x_2)
              (ElectronicsAgent ?x_2)
            )
          )
          (exists (?x_8 )
            (and
              (CodingAgent ?x_8)
              (ElectronicEngineer ?x_8)
            )
          )
          (exists (?x_6 )
            (and
              (CodingAgent ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (SoftwareAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (CodingAgent ?x_4)
              (MaterialsEngineer ?x_4)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (Developer ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (MaterialsAgent ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?x_4 )
            (and
              (SpecificationsAgent ?x_4)
              (MaterialsEngineer ?x_4)
            )
          )
          (exists (?x_6 )
            (and
              (Employee ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 ?z_1 )
            (and
              (hasPersonalInfo ?x_6 ?z_1)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (ElectronicsAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (InformaticEngineer ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (FullName ?x_6)
              (Engineer ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (FullName ?x_6)
              (TestingAgent ?x_6)
            )
          )
          (exists (?x_5 )
            (and
              (MaterialsEngineer ?x_5)
              (ElectronicsAgent ?x_5)
            )
          )
          (exists (?x_6 )
            (and
              (SoftwareAgent ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (CodingAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_0 )
            (and
              (InformaticEngineer ?x_0)
              (ElectronicEngineer ?x_0)
            )
          )
          (exists (?x_6 )
            (and
              (SpecificationsAgent ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?x_3 )
            (and
              (InformaticEngineer ?x_3)
              (ElectronicsAgent ?x_3)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (TaskAgent ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?x_6 )
            (and
              (ElectronicEngineer ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?z_0 ?x_6 )
            (and
              (Engineer ?x_6)
              (hasPersonalInfo ?z_0 ?x_6)
            )
          )
          (exists (?z_0 )
            (hasPersonalInfo ?z_0 ?z_0)
          )
          (exists (?x_6 )
            (and
              (Designer ?x_6)
              (FullName ?x_6)
            )
          )
          (exists (?x_1 )
            (and
              (SpecificationsAgent ?x_1)
              (Designer ?x_1)
            )
          )
        )
        (Error)
      )
    )
  )
)