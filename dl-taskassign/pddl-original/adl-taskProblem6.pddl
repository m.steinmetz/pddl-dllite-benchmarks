(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d f 
  )
  (:init
    (Engineer a)
    (Developer b)
    (Developer e)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (not
            (=?x ?y )
          )
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
        )
      )
    )
  )
)