(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b d 
  )
  (:init
    (Engineer d)
    (Designer a)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (not
            (=?x ?y )
          )
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
        )
      )
    )
  )
)