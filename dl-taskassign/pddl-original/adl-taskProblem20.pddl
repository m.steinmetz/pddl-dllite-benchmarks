(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p s r t 
  )
  (:init
    (Developer l)
    (Developer n)
    (Engineer k)
    (Developer i)
    (Engineer o)
    (Designer q)
    (Engineer f)
    (Designer h)
    (Developer s)
    (Developer r)
    (Engineer m)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (not
            (=?x ?y )
          )
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
        )
      )
    )
  )
)