(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f 
  )
  (:init
    (Designer f)
    (Developer a)
    (Designer b)
    (Developer c)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)