(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p s r 
  )
  (:init
    (Developer n)
    (Developer o)
    (Developer h)
    (Developer k)
    (Developer f)
    (Engineer d)
    (Developer c)
    (Designer m)
    (Engineer s)
    (Designer g)
    (Developer q)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)