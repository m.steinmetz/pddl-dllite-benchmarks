(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p 
  )
  (:init
    (Engineer h)
    (Engineer i)
    (Developer n)
    (Engineer k)
    (Developer j)
    (Engineer o)
    (Engineer b)
    (Designer q)
    (Engineer g)
    (Designer d)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)