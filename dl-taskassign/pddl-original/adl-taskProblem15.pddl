(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n 
  )
  (:init
    (Engineer k)
    (Engineer o)
    (Engineer d)
    (Engineer e)
    (Engineer g)
    (Designer j)
    (Designer f)
    (Designer b)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)