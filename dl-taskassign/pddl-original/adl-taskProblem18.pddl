(define (problem taskAssigment_problem )
  (:domain taskAssigment)
  (:objects
    a c b e d g f i h k j m l o n q p r 
  )
  (:init
    (Engineer h)
    (Developer m)
    (Engineer j)
    (Developer d)
    (Engineer c)
    (Developer n)
    (Engineer p)
    (Designer f)
    (Designer g)
    (Designer a)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?y ?x )
        (and
          (and
            (ElectronicEngineer ?y)
            (ElectronicEngineer ?x)
          )
          (not
            (=?x ?y )
          )
        )
      )
    )
  )
)