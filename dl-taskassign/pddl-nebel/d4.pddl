(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag15-)
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-D)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-D)
(DESIGNAGENT-A)
(DESIGNAGENT-D)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-D)
(TESTINGAGENT-A)
(TESTINGAGENT-D)
(MATERIALSAGENT-A)
(MATERIALSAGENT-D)
(CODINGAGENT-A)
(CODINGAGENT-D)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-D)
(TASKAGENT-A)
(TASKAGENT-D)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-D)
(Flag69-)
(Flag44-)
(Flag18-)
(Flag16-)
(ERROR-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag19-)
(Flag17-)
(Flag13-)
(ELECTRONICSAGENT-C)
(ELECTRONICSAGENT-B)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-B)
(DESIGNAGENT-C)
(DESIGNAGENT-B)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-B)
(TESTINGAGENT-C)
(TESTINGAGENT-B)
(MATERIALSAGENT-C)
(MATERIALSAGENT-B)
(CODINGAGENT-C)
(CODINGAGENT-B)
(TASKAGENT-C)
(TASKAGENT-B)
(Flag14-)
(NOT-CHECKCONSISTENCY-)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-SOFTWAREAGENT-B)
(NOT-SOFTWAREAGENT-C)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-C)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-SOFTWAREAGENT-D)
(NOT-SOFTWAREAGENT-A)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-A)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag15prime-)
(Flag69prime-)
(Flag44prime-)
(Flag18prime-)
(Flag16prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag1prime-)
(Flag19prime-)
(Flag17prime-)
(Flag13prime-)
(Flag14prime-)
)
(:action Flag14Action
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (Flag13-))
(Flag13prime-)
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-8
:parameters ()
:precondition
(and
(TASKAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-B))
(not (SPECIFICATIONSAGENT-B))
(not (ELECTRONICSAGENT-B))
(not (CODINGAGENT-B))
(not (DESIGNAGENT-B))
(not (INFORMATICENGINEER-B))
(not (TESTINGAGENT-B))
(not (ELECTRONICENGINEER-B))
(not (TASKAGENT-B))
(not (SOFTWAREAGENT-B))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-8
:parameters ()
:precondition
(and
(TASKAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-C))
(not (SPECIFICATIONSAGENT-C))
(not (ELECTRONICSAGENT-C))
(not (CODINGAGENT-C))
(not (DESIGNAGENT-C))
(not (INFORMATICENGINEER-C))
(not (TESTINGAGENT-C))
(not (ELECTRONICENGINEER-C))
(not (TASKAGENT-C))
(not (SOFTWAREAGENT-C))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action Flag13Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-1
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-2
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-3
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-4
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-5
:parameters ()
:precondition
(and
(Flag6-)
(Flag6prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-6
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-7
:parameters ()
:precondition
(and
(Flag8-)
(Flag8prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-8
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-9
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-10
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag13Action-11
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
(not (Flag2-))
(Flag2prime-)
(not (Flag3-))
(Flag3prime-)
(not (Flag4-))
(Flag4prime-)
(not (Flag5-))
(Flag5prime-)
(not (Flag6-))
(Flag6prime-)
(not (Flag7-))
(Flag7prime-)
(not (Flag8-))
(Flag8prime-)
(not (Flag9-))
(Flag9prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag11-))
(Flag11prime-)
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Prim17Action-2
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim19Action-2
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag8Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim5Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim6Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim7Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim12Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SOFTWAREAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(CODINGAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(CODINGAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action Flag16Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag44Action
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
(Flag21-)
(Flag21prime-)
(Flag22-)
(Flag22prime-)
(Flag23-)
(Flag23prime-)
(Flag24-)
(Flag24prime-)
(Flag25-)
(Flag25prime-)
(Flag26-)
(Flag26prime-)
(Flag27-)
(Flag27prime-)
(Flag28-)
(Flag28prime-)
(Flag29-)
(Flag29prime-)
(Flag30-)
(Flag30prime-)
(Flag31-)
(Flag31prime-)
(Flag32-)
(Flag32prime-)
(Flag33-)
(Flag33prime-)
(Flag34-)
(Flag34prime-)
(Flag35-)
(Flag35prime-)
(Flag36-)
(Flag36prime-)
(Flag37-)
(Flag37prime-)
(Flag38-)
(Flag38prime-)
(Flag39-)
(Flag39prime-)
(Flag40-)
(Flag40prime-)
(Flag41-)
(Flag41prime-)
(Flag42-)
(Flag42prime-)
(Flag43-)
(Flag43prime-)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (Flag21-))
(Flag21prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-3
:parameters ()
:precondition
(and
(not (Flag23-))
(Flag23prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-4
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-5
:parameters ()
:precondition
(and
(not (Flag25-))
(Flag25prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-6
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-7
:parameters ()
:precondition
(and
(not (Flag27-))
(Flag27prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-8
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-9
:parameters ()
:precondition
(and
(not (Flag29-))
(Flag29prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-10
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-11
:parameters ()
:precondition
(and
(not (Flag31-))
(Flag31prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-12
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-13
:parameters ()
:precondition
(and
(not (Flag33-))
(Flag33prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-14
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-15
:parameters ()
:precondition
(and
(not (Flag35-))
(Flag35prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-16
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-17
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-18
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-19
:parameters ()
:precondition
(and
(not (Flag39-))
(Flag39prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-20
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-21
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-22
:parameters ()
:precondition
(and
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-23
:parameters ()
:precondition
(and
(not (Flag43-))
(Flag43prime-)
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(Flag45-)
(Flag45prime-)
(Flag46-)
(Flag46prime-)
(Flag47-)
(Flag47prime-)
(Flag48-)
(Flag48prime-)
(Flag49-)
(Flag49prime-)
(Flag50-)
(Flag50prime-)
(Flag51-)
(Flag51prime-)
(Flag52-)
(Flag52prime-)
(Flag53-)
(Flag53prime-)
(Flag54-)
(Flag54prime-)
(Flag55-)
(Flag55prime-)
(Flag56-)
(Flag56prime-)
(Flag57-)
(Flag57prime-)
(Flag58-)
(Flag58prime-)
(Flag59-)
(Flag59prime-)
(Flag60-)
(Flag60prime-)
(Flag61-)
(Flag61prime-)
(Flag62-)
(Flag62prime-)
(Flag63-)
(Flag63prime-)
(Flag64-)
(Flag64prime-)
(Flag65-)
(Flag65prime-)
(Flag66-)
(Flag66prime-)
(Flag67-)
(Flag67prime-)
(Flag68-)
(Flag68prime-)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (Flag45-))
(Flag45prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (Flag46-))
(Flag46prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-3
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-4
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-5
:parameters ()
:precondition
(and
(not (Flag50-))
(Flag50prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-6
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-7
:parameters ()
:precondition
(and
(not (Flag52-))
(Flag52prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-8
:parameters ()
:precondition
(and
(not (Flag53-))
(Flag53prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-9
:parameters ()
:precondition
(and
(not (Flag54-))
(Flag54prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-10
:parameters ()
:precondition
(and
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-11
:parameters ()
:precondition
(and
(not (Flag56-))
(Flag56prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-12
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-13
:parameters ()
:precondition
(and
(not (Flag58-))
(Flag58prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-14
:parameters ()
:precondition
(and
(not (Flag59-))
(Flag59prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-15
:parameters ()
:precondition
(and
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-16
:parameters ()
:precondition
(and
(not (Flag61-))
(Flag61prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-17
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-18
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-19
:parameters ()
:precondition
(and
(not (Flag64-))
(Flag64prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-20
:parameters ()
:precondition
(and
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-21
:parameters ()
:precondition
(and
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-22
:parameters ()
:precondition
(and
(not (Flag67-))
(Flag67prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-23
:parameters ()
:precondition
(and
(not (Flag68-))
(Flag68prime-)
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
(Flag69prime-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag69prime-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag69prime-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag69prime-)
)
:effect
(and
(when
(and
(Flag69-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag44prime-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag44prime-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag44prime-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag44prime-)
)
:effect
(and
(when
(and
(Flag44-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag15prime-))
(not (Flag69prime-))
(not (Flag44prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag13prime-))
(not (Flag14prime-))
)
)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-2
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Flag15Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Prim15Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim15Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag20Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag27Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag29Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Prim20Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
)
