(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag11-)
(ELECTRONICENGINEER-A)
(CHECKCONSISTENCY-)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(SPECIFICATIONSAGENT-C)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(MATERIALSAGENT-C)
(TESTINGAGENT-C)
(TASKAGENT-C)
(CODINGAGENT-C)
(DESIGNAGENT-C)
(ELECTRONICSAGENT-C)
(SOFTWAREAGENT-C)
(Flag27-)
(Flag20-)
(Flag12-)
(Flag9-)
(ERROR-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag13-)
(Flag10-)
(Flag7-)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TASKAGENT-A)
(TASKAGENT-B)
(CODINGAGENT-A)
(CODINGAGENT-B)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(ELECTRONICSAGENT-A)
(ELECTRONICSAGENT-B)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(Flag8-)
(NOT-CHECKCONSISTENCY-)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ERROR-)
(NOT-ELECTRONICSAGENT-C)
(NOT-CODINGAGENT-C)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag11prime-)
(Flag27prime-)
(Flag20prime-)
(Flag12prime-)
(Flag9prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag1prime-)
(Flag13prime-)
(Flag10prime-)
(Flag7prime-)
(Flag8prime-)
)
(:action Flag8Action
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (Flag7-))
(Flag7prime-)
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(TASKAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(CODINGAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(DESIGNAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-7
:parameters ()
:precondition
(and
(TESTINGAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-B))
(not (TASKAGENT-B))
(not (SPECIFICATIONSAGENT-B))
(not (ELECTRONICSAGENT-B))
(not (CODINGAGENT-B))
(not (DESIGNAGENT-B))
(not (INFORMATICENGINEER-B))
(not (TESTINGAGENT-B))
(not (ELECTRONICENGINEER-B))
(not (SOFTWAREAGENT-B))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(TASKAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-2
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-3
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-4
:parameters ()
:precondition
(and
(CODINGAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-5
:parameters ()
:precondition
(and
(DESIGNAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-7
:parameters ()
:precondition
(and
(TESTINGAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-A))
(not (TASKAGENT-A))
(not (SPECIFICATIONSAGENT-A))
(not (ELECTRONICSAGENT-A))
(not (CODINGAGENT-A))
(not (DESIGNAGENT-A))
(not (INFORMATICENGINEER-A))
(not (TESTINGAGENT-A))
(not (ELECTRONICENGINEER-A))
(not (SOFTWAREAGENT-A))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-5
:parameters ()
:precondition
(and
(Flag6-)
(Flag6prime-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
(not (Flag2-))
(Flag2prime-)
(not (Flag3-))
(Flag3prime-)
(not (Flag4-))
(Flag4prime-)
(not (Flag5-))
(Flag5prime-)
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action-2
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action-2
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim5Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim6Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(SPECIFICATIONSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action Flag9Action-6
:parameters ()
:precondition
(and
(INFORMATICENGINEER-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-8
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag12Action-6
:parameters ()
:precondition
(and
(INFORMATICENGINEER-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag12Action-8
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
(Flag15-)
(Flag15prime-)
(Flag16-)
(Flag16prime-)
(Flag17-)
(Flag17prime-)
(Flag18-)
(Flag18prime-)
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (Flag15-))
(Flag15prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-2
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-3
:parameters ()
:precondition
(and
(not (Flag17-))
(Flag17prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-4
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-5
:parameters ()
:precondition
(and
(not (Flag19-))
(Flag19prime-)
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
(Flag22-)
(Flag22prime-)
(Flag23-)
(Flag23prime-)
(Flag24-)
(Flag24prime-)
(Flag25-)
(Flag25prime-)
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (Flag21-))
(Flag21prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-2
:parameters ()
:precondition
(and
(not (Flag23-))
(Flag23prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-3
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-4
:parameters ()
:precondition
(and
(not (Flag25-))
(Flag25prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-5
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag27prime-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag27prime-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag27prime-)
)
:effect
(and
(when
(and
(Flag27-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag20prime-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag20prime-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag20prime-)
)
:effect
(and
(when
(and
(Flag20-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag11prime-))
(not (Flag27prime-))
(not (Flag20prime-))
(not (Flag12prime-))
(not (Flag9prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag13prime-))
(not (Flag10prime-))
(not (Flag7prime-))
(not (Flag8prime-))
)
)
(:action Prim8Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-2
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag14Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag15Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag16Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag17Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag18Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag19Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Prim14Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim16Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag22Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag23Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag25Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag25Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
)
