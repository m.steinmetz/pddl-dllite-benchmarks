(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag37-)
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-B)
(ELECTRONICSAGENT-E)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(SPECIFICATIONSAGENT-E)
(CODINGAGENT-A)
(CODINGAGENT-B)
(CODINGAGENT-E)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TESTINGAGENT-E)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(DESIGNAGENT-E)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-E)
(INFORMATICENGINEER-D)
(INFORMATICENGINEER-F)
(TASKAGENT-A)
(TASKAGENT-B)
(TASKAGENT-E)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(SOFTWAREAGENT-E)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-F)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(MATERIALSAGENT-E)
(Flag281-)
(Flag160-)
(Flag38-)
(Flag35-)
(Flag33-)
(ERROR-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag39-)
(Flag36-)
(Flag34-)
(Flag31-)
(ELECTRONICSAGENT-C)
(ELECTRONICSAGENT-D)
(ELECTRONICSAGENT-F)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-D)
(SPECIFICATIONSAGENT-F)
(CODINGAGENT-C)
(CODINGAGENT-D)
(CODINGAGENT-F)
(TESTINGAGENT-C)
(TESTINGAGENT-D)
(TESTINGAGENT-F)
(DESIGNAGENT-C)
(DESIGNAGENT-D)
(DESIGNAGENT-F)
(TASKAGENT-C)
(TASKAGENT-D)
(TASKAGENT-F)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-D)
(SOFTWAREAGENT-F)
(MATERIALSAGENT-C)
(MATERIALSAGENT-D)
(MATERIALSAGENT-F)
(Flag32-)
(NOT-CHECKCONSISTENCY-)
(NOT-CODINGAGENT-F)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-F)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICSAGENT-F)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-C)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-F)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-E)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-F)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-E)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-CODINGAGENT-E)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-E)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ELECTRONICSAGENT-E)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
(Flag280prime-)
(Flag279prime-)
(Flag278prime-)
(Flag277prime-)
(Flag276prime-)
(Flag275prime-)
(Flag274prime-)
(Flag273prime-)
(Flag272prime-)
(Flag271prime-)
(Flag270prime-)
(Flag269prime-)
(Flag268prime-)
(Flag267prime-)
(Flag266prime-)
(Flag265prime-)
(Flag264prime-)
(Flag263prime-)
(Flag262prime-)
(Flag261prime-)
(Flag260prime-)
(Flag259prime-)
(Flag258prime-)
(Flag257prime-)
(Flag256prime-)
(Flag255prime-)
(Flag254prime-)
(Flag253prime-)
(Flag252prime-)
(Flag251prime-)
(Flag250prime-)
(Flag249prime-)
(Flag248prime-)
(Flag247prime-)
(Flag246prime-)
(Flag245prime-)
(Flag244prime-)
(Flag243prime-)
(Flag242prime-)
(Flag241prime-)
(Flag240prime-)
(Flag239prime-)
(Flag238prime-)
(Flag237prime-)
(Flag236prime-)
(Flag235prime-)
(Flag234prime-)
(Flag233prime-)
(Flag232prime-)
(Flag231prime-)
(Flag230prime-)
(Flag229prime-)
(Flag228prime-)
(Flag227prime-)
(Flag226prime-)
(Flag225prime-)
(Flag224prime-)
(Flag223prime-)
(Flag222prime-)
(Flag221prime-)
(Flag220prime-)
(Flag219prime-)
(Flag218prime-)
(Flag217prime-)
(Flag216prime-)
(Flag215prime-)
(Flag214prime-)
(Flag213prime-)
(Flag212prime-)
(Flag211prime-)
(Flag210prime-)
(Flag209prime-)
(Flag208prime-)
(Flag207prime-)
(Flag206prime-)
(Flag205prime-)
(Flag204prime-)
(Flag203prime-)
(Flag202prime-)
(Flag201prime-)
(Flag200prime-)
(Flag199prime-)
(Flag198prime-)
(Flag197prime-)
(Flag196prime-)
(Flag195prime-)
(Flag194prime-)
(Flag193prime-)
(Flag192prime-)
(Flag191prime-)
(Flag190prime-)
(Flag189prime-)
(Flag188prime-)
(Flag187prime-)
(Flag186prime-)
(Flag185prime-)
(Flag184prime-)
(Flag183prime-)
(Flag182prime-)
(Flag181prime-)
(Flag180prime-)
(Flag179prime-)
(Flag178prime-)
(Flag177prime-)
(Flag176prime-)
(Flag175prime-)
(Flag174prime-)
(Flag173prime-)
(Flag172prime-)
(Flag171prime-)
(Flag170prime-)
(Flag169prime-)
(Flag168prime-)
(Flag167prime-)
(Flag166prime-)
(Flag165prime-)
(Flag164prime-)
(Flag163prime-)
(Flag162prime-)
(Flag161prime-)
(Flag159prime-)
(Flag158prime-)
(Flag157prime-)
(Flag156prime-)
(Flag155prime-)
(Flag154prime-)
(Flag153prime-)
(Flag152prime-)
(Flag151prime-)
(Flag150prime-)
(Flag149prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag90prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag37prime-)
(Flag281prime-)
(Flag160prime-)
(Flag38prime-)
(Flag35prime-)
(Flag33prime-)
(Flag30prime-)
(Flag29prime-)
(Flag28prime-)
(Flag27prime-)
(Flag26prime-)
(Flag25prime-)
(Flag24prime-)
(Flag23prime-)
(Flag22prime-)
(Flag21prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag1prime-)
(Flag39prime-)
(Flag36prime-)
(Flag34prime-)
(Flag31prime-)
(Flag32prime-)
)
(:action Flag32Action
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (Flag31-))
(Flag31prime-)
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-8
:parameters ()
:precondition
(and
(TASKAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-F))
(not (SPECIFICATIONSAGENT-F))
(not (ELECTRONICSAGENT-F))
(not (CODINGAGENT-F))
(not (DESIGNAGENT-F))
(not (INFORMATICENGINEER-F))
(not (TESTINGAGENT-F))
(not (ELECTRONICENGINEER-F))
(not (TASKAGENT-F))
(not (SOFTWAREAGENT-F))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-8
:parameters ()
:precondition
(and
(TASKAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-D))
(not (SPECIFICATIONSAGENT-D))
(not (ELECTRONICSAGENT-D))
(not (CODINGAGENT-D))
(not (DESIGNAGENT-D))
(not (INFORMATICENGINEER-D))
(not (TESTINGAGENT-D))
(not (ELECTRONICENGINEER-D))
(not (TASKAGENT-D))
(not (SOFTWAREAGENT-D))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-8
:parameters ()
:precondition
(and
(TASKAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-C))
(not (SPECIFICATIONSAGENT-C))
(not (ELECTRONICSAGENT-C))
(not (CODINGAGENT-C))
(not (DESIGNAGENT-C))
(not (INFORMATICENGINEER-C))
(not (TESTINGAGENT-C))
(not (ELECTRONICENGINEER-C))
(not (TASKAGENT-C))
(not (SOFTWAREAGENT-C))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action ASSIGNMATERIALSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(MATERIALSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(SOFTWAREAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(TASKAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(DESIGNAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(TESTINGAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(CODINGAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-F
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(ELECTRONICSAGENT-F)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-3
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-4
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-5
:parameters ()
:precondition
(and
(Flag6-)
(Flag6prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-6
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-7
:parameters ()
:precondition
(and
(Flag8-)
(Flag8prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-8
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-9
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-10
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-11
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-12
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-13
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-14
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-15
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-16
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-17
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-18
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-19
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-20
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-21
:parameters ()
:precondition
(and
(Flag22-)
(Flag22prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-22
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-23
:parameters ()
:precondition
(and
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-24
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-25
:parameters ()
:precondition
(and
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-26
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-27
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-28
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-29
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
(not (Flag2-))
(Flag2prime-)
(not (Flag3-))
(Flag3prime-)
(not (Flag4-))
(Flag4prime-)
(not (Flag5-))
(Flag5prime-)
(not (Flag6-))
(Flag6prime-)
(not (Flag7-))
(Flag7prime-)
(not (Flag8-))
(Flag8prime-)
(not (Flag9-))
(Flag9prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag11-))
(Flag11prime-)
(not (Flag12-))
(Flag12prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag15-))
(Flag15prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
(not (Flag21-))
(Flag21prime-)
(not (Flag22-))
(Flag22prime-)
(not (Flag23-))
(Flag23prime-)
(not (Flag24-))
(Flag24prime-)
(not (Flag25-))
(Flag25prime-)
(not (Flag26-))
(Flag26prime-)
(not (Flag27-))
(Flag27prime-)
(not (Flag28-))
(Flag28prime-)
(not (Flag29-))
(Flag29prime-)
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Flag34Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action-2
:parameters ()
:precondition
(and
(not (Flag33-))
(Flag33prime-)
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action-2
:parameters ()
:precondition
(and
(not (Flag35-))
(Flag35prime-)
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag38-)
(Flag38prime-)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag8Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag14Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag21Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag22Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag24Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag26Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag28Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag30Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim5Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim6Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim7Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim12Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim15Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim21Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim21Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim24Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim24Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-F))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-E)
(ELECTRONICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-F)
(ELECTRONICENGINEER-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(CODINGAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(CODINGAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(CODINGAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-E)
(CODINGAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-F)
(CODINGAGENT-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-E)
(INFORMATICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-F)
(INFORMATICENGINEER-F)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action Flag33Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-F)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag38Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag160Action
:parameters ()
:precondition
(and
(Flag40-)
(Flag40prime-)
(Flag41-)
(Flag41prime-)
(Flag42-)
(Flag42prime-)
(Flag43-)
(Flag43prime-)
(Flag44-)
(Flag44prime-)
(Flag45-)
(Flag45prime-)
(Flag46-)
(Flag46prime-)
(Flag47-)
(Flag47prime-)
(Flag48-)
(Flag48prime-)
(Flag49-)
(Flag49prime-)
(Flag50-)
(Flag50prime-)
(Flag51-)
(Flag51prime-)
(Flag52-)
(Flag52prime-)
(Flag53-)
(Flag53prime-)
(Flag54-)
(Flag54prime-)
(Flag55-)
(Flag55prime-)
(Flag56-)
(Flag56prime-)
(Flag57-)
(Flag57prime-)
(Flag58-)
(Flag58prime-)
(Flag59-)
(Flag59prime-)
(Flag60-)
(Flag60prime-)
(Flag61-)
(Flag61prime-)
(Flag62-)
(Flag62prime-)
(Flag63-)
(Flag63prime-)
(Flag64-)
(Flag64prime-)
(Flag65-)
(Flag65prime-)
(Flag66-)
(Flag66prime-)
(Flag67-)
(Flag67prime-)
(Flag68-)
(Flag68prime-)
(Flag69-)
(Flag69prime-)
(Flag70-)
(Flag70prime-)
(Flag71-)
(Flag71prime-)
(Flag72-)
(Flag72prime-)
(Flag73-)
(Flag73prime-)
(Flag74-)
(Flag74prime-)
(Flag75-)
(Flag75prime-)
(Flag76-)
(Flag76prime-)
(Flag77-)
(Flag77prime-)
(Flag78-)
(Flag78prime-)
(Flag79-)
(Flag79prime-)
(Flag80-)
(Flag80prime-)
(Flag81-)
(Flag81prime-)
(Flag82-)
(Flag82prime-)
(Flag83-)
(Flag83prime-)
(Flag84-)
(Flag84prime-)
(Flag85-)
(Flag85prime-)
(Flag86-)
(Flag86prime-)
(Flag87-)
(Flag87prime-)
(Flag88-)
(Flag88prime-)
(Flag89-)
(Flag89prime-)
(Flag90-)
(Flag90prime-)
(Flag91-)
(Flag91prime-)
(Flag92-)
(Flag92prime-)
(Flag93-)
(Flag93prime-)
(Flag94-)
(Flag94prime-)
(Flag95-)
(Flag95prime-)
(Flag96-)
(Flag96prime-)
(Flag97-)
(Flag97prime-)
(Flag98-)
(Flag98prime-)
(Flag99-)
(Flag99prime-)
(Flag100-)
(Flag100prime-)
(Flag101-)
(Flag101prime-)
(Flag102-)
(Flag102prime-)
(Flag103-)
(Flag103prime-)
(Flag104-)
(Flag104prime-)
(Flag105-)
(Flag105prime-)
(Flag106-)
(Flag106prime-)
(Flag107-)
(Flag107prime-)
(Flag108-)
(Flag108prime-)
(Flag109-)
(Flag109prime-)
(Flag110-)
(Flag110prime-)
(Flag111-)
(Flag111prime-)
(Flag112-)
(Flag112prime-)
(Flag113-)
(Flag113prime-)
(Flag114-)
(Flag114prime-)
(Flag115-)
(Flag115prime-)
(Flag116-)
(Flag116prime-)
(Flag117-)
(Flag117prime-)
(Flag118-)
(Flag118prime-)
(Flag119-)
(Flag119prime-)
(Flag120-)
(Flag120prime-)
(Flag121-)
(Flag121prime-)
(Flag122-)
(Flag122prime-)
(Flag123-)
(Flag123prime-)
(Flag124-)
(Flag124prime-)
(Flag125-)
(Flag125prime-)
(Flag126-)
(Flag126prime-)
(Flag127-)
(Flag127prime-)
(Flag128-)
(Flag128prime-)
(Flag129-)
(Flag129prime-)
(Flag130-)
(Flag130prime-)
(Flag131-)
(Flag131prime-)
(Flag132-)
(Flag132prime-)
(Flag133-)
(Flag133prime-)
(Flag134-)
(Flag134prime-)
(Flag135-)
(Flag135prime-)
(Flag136-)
(Flag136prime-)
(Flag137-)
(Flag137prime-)
(Flag138-)
(Flag138prime-)
(Flag139-)
(Flag139prime-)
(Flag140-)
(Flag140prime-)
(Flag141-)
(Flag141prime-)
(Flag142-)
(Flag142prime-)
(Flag143-)
(Flag143prime-)
(Flag144-)
(Flag144prime-)
(Flag145-)
(Flag145prime-)
(Flag146-)
(Flag146prime-)
(Flag147-)
(Flag147prime-)
(Flag148-)
(Flag148prime-)
(Flag149-)
(Flag149prime-)
(Flag150-)
(Flag150prime-)
(Flag151-)
(Flag151prime-)
(Flag152-)
(Flag152prime-)
(Flag153-)
(Flag153prime-)
(Flag154-)
(Flag154prime-)
(Flag155-)
(Flag155prime-)
(Flag156-)
(Flag156prime-)
(Flag157-)
(Flag157prime-)
(Flag158-)
(Flag158prime-)
(Flag159-)
(Flag159prime-)
)
:effect
(and
(Flag160-)
(Flag160prime-)
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-2
:parameters ()
:precondition
(and
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (Flag43-))
(Flag43prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-4
:parameters ()
:precondition
(and
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-5
:parameters ()
:precondition
(and
(not (Flag45-))
(Flag45prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-6
:parameters ()
:precondition
(and
(not (Flag46-))
(Flag46prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-7
:parameters ()
:precondition
(and
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-8
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-9
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-10
:parameters ()
:precondition
(and
(not (Flag50-))
(Flag50prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-11
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-12
:parameters ()
:precondition
(and
(not (Flag52-))
(Flag52prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-13
:parameters ()
:precondition
(and
(not (Flag53-))
(Flag53prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-14
:parameters ()
:precondition
(and
(not (Flag54-))
(Flag54prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-15
:parameters ()
:precondition
(and
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-16
:parameters ()
:precondition
(and
(not (Flag56-))
(Flag56prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-17
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-18
:parameters ()
:precondition
(and
(not (Flag58-))
(Flag58prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-19
:parameters ()
:precondition
(and
(not (Flag59-))
(Flag59prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-20
:parameters ()
:precondition
(and
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-21
:parameters ()
:precondition
(and
(not (Flag61-))
(Flag61prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-22
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-23
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-24
:parameters ()
:precondition
(and
(not (Flag64-))
(Flag64prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-25
:parameters ()
:precondition
(and
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-26
:parameters ()
:precondition
(and
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-27
:parameters ()
:precondition
(and
(not (Flag67-))
(Flag67prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-28
:parameters ()
:precondition
(and
(not (Flag68-))
(Flag68prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-29
:parameters ()
:precondition
(and
(not (Flag69-))
(Flag69prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-30
:parameters ()
:precondition
(and
(not (Flag70-))
(Flag70prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-31
:parameters ()
:precondition
(and
(not (Flag71-))
(Flag71prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-32
:parameters ()
:precondition
(and
(not (Flag72-))
(Flag72prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-33
:parameters ()
:precondition
(and
(not (Flag73-))
(Flag73prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-34
:parameters ()
:precondition
(and
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-35
:parameters ()
:precondition
(and
(not (Flag75-))
(Flag75prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-36
:parameters ()
:precondition
(and
(not (Flag76-))
(Flag76prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-37
:parameters ()
:precondition
(and
(not (Flag77-))
(Flag77prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-38
:parameters ()
:precondition
(and
(not (Flag78-))
(Flag78prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-39
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-40
:parameters ()
:precondition
(and
(not (Flag80-))
(Flag80prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-41
:parameters ()
:precondition
(and
(not (Flag81-))
(Flag81prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-42
:parameters ()
:precondition
(and
(not (Flag82-))
(Flag82prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-43
:parameters ()
:precondition
(and
(not (Flag83-))
(Flag83prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-44
:parameters ()
:precondition
(and
(not (Flag84-))
(Flag84prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-45
:parameters ()
:precondition
(and
(not (Flag85-))
(Flag85prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-46
:parameters ()
:precondition
(and
(not (Flag86-))
(Flag86prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-47
:parameters ()
:precondition
(and
(not (Flag87-))
(Flag87prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-48
:parameters ()
:precondition
(and
(not (Flag88-))
(Flag88prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-49
:parameters ()
:precondition
(and
(not (Flag89-))
(Flag89prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-50
:parameters ()
:precondition
(and
(not (Flag90-))
(Flag90prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-51
:parameters ()
:precondition
(and
(not (Flag91-))
(Flag91prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-52
:parameters ()
:precondition
(and
(not (Flag92-))
(Flag92prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-53
:parameters ()
:precondition
(and
(not (Flag93-))
(Flag93prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-54
:parameters ()
:precondition
(and
(not (Flag94-))
(Flag94prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-55
:parameters ()
:precondition
(and
(not (Flag95-))
(Flag95prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-56
:parameters ()
:precondition
(and
(not (Flag96-))
(Flag96prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-57
:parameters ()
:precondition
(and
(not (Flag97-))
(Flag97prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-58
:parameters ()
:precondition
(and
(not (Flag98-))
(Flag98prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-59
:parameters ()
:precondition
(and
(not (Flag99-))
(Flag99prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-60
:parameters ()
:precondition
(and
(not (Flag100-))
(Flag100prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-61
:parameters ()
:precondition
(and
(not (Flag101-))
(Flag101prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-62
:parameters ()
:precondition
(and
(not (Flag102-))
(Flag102prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-63
:parameters ()
:precondition
(and
(not (Flag103-))
(Flag103prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-64
:parameters ()
:precondition
(and
(not (Flag104-))
(Flag104prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-65
:parameters ()
:precondition
(and
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-66
:parameters ()
:precondition
(and
(not (Flag106-))
(Flag106prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-67
:parameters ()
:precondition
(and
(not (Flag107-))
(Flag107prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-68
:parameters ()
:precondition
(and
(not (Flag108-))
(Flag108prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-69
:parameters ()
:precondition
(and
(not (Flag109-))
(Flag109prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-70
:parameters ()
:precondition
(and
(not (Flag110-))
(Flag110prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-71
:parameters ()
:precondition
(and
(not (Flag111-))
(Flag111prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-72
:parameters ()
:precondition
(and
(not (Flag112-))
(Flag112prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-73
:parameters ()
:precondition
(and
(not (Flag113-))
(Flag113prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-74
:parameters ()
:precondition
(and
(not (Flag114-))
(Flag114prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-75
:parameters ()
:precondition
(and
(not (Flag115-))
(Flag115prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-76
:parameters ()
:precondition
(and
(not (Flag116-))
(Flag116prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-77
:parameters ()
:precondition
(and
(not (Flag117-))
(Flag117prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-78
:parameters ()
:precondition
(and
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-79
:parameters ()
:precondition
(and
(not (Flag119-))
(Flag119prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-80
:parameters ()
:precondition
(and
(not (Flag120-))
(Flag120prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-81
:parameters ()
:precondition
(and
(not (Flag121-))
(Flag121prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-82
:parameters ()
:precondition
(and
(not (Flag122-))
(Flag122prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-83
:parameters ()
:precondition
(and
(not (Flag123-))
(Flag123prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-84
:parameters ()
:precondition
(and
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-85
:parameters ()
:precondition
(and
(not (Flag125-))
(Flag125prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-86
:parameters ()
:precondition
(and
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-87
:parameters ()
:precondition
(and
(not (Flag127-))
(Flag127prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-88
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-89
:parameters ()
:precondition
(and
(not (Flag129-))
(Flag129prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-90
:parameters ()
:precondition
(and
(not (Flag130-))
(Flag130prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-91
:parameters ()
:precondition
(and
(not (Flag131-))
(Flag131prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-92
:parameters ()
:precondition
(and
(not (Flag132-))
(Flag132prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-93
:parameters ()
:precondition
(and
(not (Flag133-))
(Flag133prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-94
:parameters ()
:precondition
(and
(not (Flag134-))
(Flag134prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-95
:parameters ()
:precondition
(and
(not (Flag135-))
(Flag135prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-96
:parameters ()
:precondition
(and
(not (Flag136-))
(Flag136prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-97
:parameters ()
:precondition
(and
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-98
:parameters ()
:precondition
(and
(not (Flag138-))
(Flag138prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-99
:parameters ()
:precondition
(and
(not (Flag139-))
(Flag139prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-100
:parameters ()
:precondition
(and
(not (Flag140-))
(Flag140prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-101
:parameters ()
:precondition
(and
(not (Flag141-))
(Flag141prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-102
:parameters ()
:precondition
(and
(not (Flag142-))
(Flag142prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-103
:parameters ()
:precondition
(and
(not (Flag143-))
(Flag143prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-104
:parameters ()
:precondition
(and
(not (Flag144-))
(Flag144prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-105
:parameters ()
:precondition
(and
(not (Flag145-))
(Flag145prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-106
:parameters ()
:precondition
(and
(not (Flag146-))
(Flag146prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-107
:parameters ()
:precondition
(and
(not (Flag147-))
(Flag147prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-108
:parameters ()
:precondition
(and
(not (Flag148-))
(Flag148prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-109
:parameters ()
:precondition
(and
(not (Flag149-))
(Flag149prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-110
:parameters ()
:precondition
(and
(not (Flag150-))
(Flag150prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-111
:parameters ()
:precondition
(and
(not (Flag151-))
(Flag151prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-112
:parameters ()
:precondition
(and
(not (Flag152-))
(Flag152prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-113
:parameters ()
:precondition
(and
(not (Flag153-))
(Flag153prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-114
:parameters ()
:precondition
(and
(not (Flag154-))
(Flag154prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-115
:parameters ()
:precondition
(and
(not (Flag155-))
(Flag155prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-116
:parameters ()
:precondition
(and
(not (Flag156-))
(Flag156prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-117
:parameters ()
:precondition
(and
(not (Flag157-))
(Flag157prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-118
:parameters ()
:precondition
(and
(not (Flag158-))
(Flag158prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Prim160Action-119
:parameters ()
:precondition
(and
(not (Flag159-))
(Flag159prime-)
)
:effect
(and
(Flag160prime-)
(not (Flag160-))
)

)
(:action Flag281Action
:parameters ()
:precondition
(and
(Flag161-)
(Flag161prime-)
(Flag162-)
(Flag162prime-)
(Flag163-)
(Flag163prime-)
(Flag164-)
(Flag164prime-)
(Flag165-)
(Flag165prime-)
(Flag166-)
(Flag166prime-)
(Flag167-)
(Flag167prime-)
(Flag168-)
(Flag168prime-)
(Flag169-)
(Flag169prime-)
(Flag170-)
(Flag170prime-)
(Flag171-)
(Flag171prime-)
(Flag172-)
(Flag172prime-)
(Flag173-)
(Flag173prime-)
(Flag174-)
(Flag174prime-)
(Flag175-)
(Flag175prime-)
(Flag176-)
(Flag176prime-)
(Flag177-)
(Flag177prime-)
(Flag178-)
(Flag178prime-)
(Flag179-)
(Flag179prime-)
(Flag180-)
(Flag180prime-)
(Flag181-)
(Flag181prime-)
(Flag182-)
(Flag182prime-)
(Flag183-)
(Flag183prime-)
(Flag184-)
(Flag184prime-)
(Flag185-)
(Flag185prime-)
(Flag186-)
(Flag186prime-)
(Flag187-)
(Flag187prime-)
(Flag188-)
(Flag188prime-)
(Flag189-)
(Flag189prime-)
(Flag190-)
(Flag190prime-)
(Flag191-)
(Flag191prime-)
(Flag192-)
(Flag192prime-)
(Flag193-)
(Flag193prime-)
(Flag194-)
(Flag194prime-)
(Flag195-)
(Flag195prime-)
(Flag196-)
(Flag196prime-)
(Flag197-)
(Flag197prime-)
(Flag198-)
(Flag198prime-)
(Flag199-)
(Flag199prime-)
(Flag200-)
(Flag200prime-)
(Flag201-)
(Flag201prime-)
(Flag202-)
(Flag202prime-)
(Flag203-)
(Flag203prime-)
(Flag204-)
(Flag204prime-)
(Flag205-)
(Flag205prime-)
(Flag206-)
(Flag206prime-)
(Flag207-)
(Flag207prime-)
(Flag208-)
(Flag208prime-)
(Flag209-)
(Flag209prime-)
(Flag210-)
(Flag210prime-)
(Flag211-)
(Flag211prime-)
(Flag212-)
(Flag212prime-)
(Flag213-)
(Flag213prime-)
(Flag214-)
(Flag214prime-)
(Flag215-)
(Flag215prime-)
(Flag216-)
(Flag216prime-)
(Flag217-)
(Flag217prime-)
(Flag218-)
(Flag218prime-)
(Flag219-)
(Flag219prime-)
(Flag220-)
(Flag220prime-)
(Flag221-)
(Flag221prime-)
(Flag222-)
(Flag222prime-)
(Flag223-)
(Flag223prime-)
(Flag224-)
(Flag224prime-)
(Flag225-)
(Flag225prime-)
(Flag226-)
(Flag226prime-)
(Flag227-)
(Flag227prime-)
(Flag228-)
(Flag228prime-)
(Flag229-)
(Flag229prime-)
(Flag230-)
(Flag230prime-)
(Flag231-)
(Flag231prime-)
(Flag232-)
(Flag232prime-)
(Flag233-)
(Flag233prime-)
(Flag234-)
(Flag234prime-)
(Flag235-)
(Flag235prime-)
(Flag236-)
(Flag236prime-)
(Flag237-)
(Flag237prime-)
(Flag238-)
(Flag238prime-)
(Flag239-)
(Flag239prime-)
(Flag240-)
(Flag240prime-)
(Flag241-)
(Flag241prime-)
(Flag242-)
(Flag242prime-)
(Flag243-)
(Flag243prime-)
(Flag244-)
(Flag244prime-)
(Flag245-)
(Flag245prime-)
(Flag246-)
(Flag246prime-)
(Flag247-)
(Flag247prime-)
(Flag248-)
(Flag248prime-)
(Flag249-)
(Flag249prime-)
(Flag250-)
(Flag250prime-)
(Flag251-)
(Flag251prime-)
(Flag252-)
(Flag252prime-)
(Flag253-)
(Flag253prime-)
(Flag254-)
(Flag254prime-)
(Flag255-)
(Flag255prime-)
(Flag256-)
(Flag256prime-)
(Flag257-)
(Flag257prime-)
(Flag258-)
(Flag258prime-)
(Flag259-)
(Flag259prime-)
(Flag260-)
(Flag260prime-)
(Flag261-)
(Flag261prime-)
(Flag262-)
(Flag262prime-)
(Flag263-)
(Flag263prime-)
(Flag264-)
(Flag264prime-)
(Flag265-)
(Flag265prime-)
(Flag266-)
(Flag266prime-)
(Flag267-)
(Flag267prime-)
(Flag268-)
(Flag268prime-)
(Flag269-)
(Flag269prime-)
(Flag270-)
(Flag270prime-)
(Flag271-)
(Flag271prime-)
(Flag272-)
(Flag272prime-)
(Flag273-)
(Flag273prime-)
(Flag274-)
(Flag274prime-)
(Flag275-)
(Flag275prime-)
(Flag276-)
(Flag276prime-)
(Flag277-)
(Flag277prime-)
(Flag278-)
(Flag278prime-)
(Flag279-)
(Flag279prime-)
(Flag280-)
(Flag280prime-)
)
:effect
(and
(Flag281-)
(Flag281prime-)
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (Flag161-))
(Flag161prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (Flag162-))
(Flag162prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (Flag163-))
(Flag163prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-3
:parameters ()
:precondition
(and
(not (Flag164-))
(Flag164prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-4
:parameters ()
:precondition
(and
(not (Flag165-))
(Flag165prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-5
:parameters ()
:precondition
(and
(not (Flag166-))
(Flag166prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-6
:parameters ()
:precondition
(and
(not (Flag167-))
(Flag167prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-7
:parameters ()
:precondition
(and
(not (Flag168-))
(Flag168prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-8
:parameters ()
:precondition
(and
(not (Flag169-))
(Flag169prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-9
:parameters ()
:precondition
(and
(not (Flag170-))
(Flag170prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-10
:parameters ()
:precondition
(and
(not (Flag171-))
(Flag171prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-11
:parameters ()
:precondition
(and
(not (Flag172-))
(Flag172prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-12
:parameters ()
:precondition
(and
(not (Flag173-))
(Flag173prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-13
:parameters ()
:precondition
(and
(not (Flag174-))
(Flag174prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-14
:parameters ()
:precondition
(and
(not (Flag175-))
(Flag175prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-15
:parameters ()
:precondition
(and
(not (Flag176-))
(Flag176prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-16
:parameters ()
:precondition
(and
(not (Flag177-))
(Flag177prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-17
:parameters ()
:precondition
(and
(not (Flag178-))
(Flag178prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-18
:parameters ()
:precondition
(and
(not (Flag179-))
(Flag179prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-19
:parameters ()
:precondition
(and
(not (Flag180-))
(Flag180prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-20
:parameters ()
:precondition
(and
(not (Flag181-))
(Flag181prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-21
:parameters ()
:precondition
(and
(not (Flag182-))
(Flag182prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-22
:parameters ()
:precondition
(and
(not (Flag183-))
(Flag183prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-23
:parameters ()
:precondition
(and
(not (Flag184-))
(Flag184prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-24
:parameters ()
:precondition
(and
(not (Flag185-))
(Flag185prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-25
:parameters ()
:precondition
(and
(not (Flag186-))
(Flag186prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-26
:parameters ()
:precondition
(and
(not (Flag187-))
(Flag187prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-27
:parameters ()
:precondition
(and
(not (Flag188-))
(Flag188prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-28
:parameters ()
:precondition
(and
(not (Flag189-))
(Flag189prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-29
:parameters ()
:precondition
(and
(not (Flag190-))
(Flag190prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-30
:parameters ()
:precondition
(and
(not (Flag191-))
(Flag191prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-31
:parameters ()
:precondition
(and
(not (Flag192-))
(Flag192prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-32
:parameters ()
:precondition
(and
(not (Flag193-))
(Flag193prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-33
:parameters ()
:precondition
(and
(not (Flag194-))
(Flag194prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-34
:parameters ()
:precondition
(and
(not (Flag195-))
(Flag195prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-35
:parameters ()
:precondition
(and
(not (Flag196-))
(Flag196prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-36
:parameters ()
:precondition
(and
(not (Flag197-))
(Flag197prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-37
:parameters ()
:precondition
(and
(not (Flag198-))
(Flag198prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-38
:parameters ()
:precondition
(and
(not (Flag199-))
(Flag199prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-39
:parameters ()
:precondition
(and
(not (Flag200-))
(Flag200prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-40
:parameters ()
:precondition
(and
(not (Flag201-))
(Flag201prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-41
:parameters ()
:precondition
(and
(not (Flag202-))
(Flag202prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-42
:parameters ()
:precondition
(and
(not (Flag203-))
(Flag203prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-43
:parameters ()
:precondition
(and
(not (Flag204-))
(Flag204prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-44
:parameters ()
:precondition
(and
(not (Flag205-))
(Flag205prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-45
:parameters ()
:precondition
(and
(not (Flag206-))
(Flag206prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-46
:parameters ()
:precondition
(and
(not (Flag207-))
(Flag207prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-47
:parameters ()
:precondition
(and
(not (Flag208-))
(Flag208prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-48
:parameters ()
:precondition
(and
(not (Flag209-))
(Flag209prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-49
:parameters ()
:precondition
(and
(not (Flag210-))
(Flag210prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-50
:parameters ()
:precondition
(and
(not (Flag211-))
(Flag211prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-51
:parameters ()
:precondition
(and
(not (Flag212-))
(Flag212prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-52
:parameters ()
:precondition
(and
(not (Flag213-))
(Flag213prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-53
:parameters ()
:precondition
(and
(not (Flag214-))
(Flag214prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-54
:parameters ()
:precondition
(and
(not (Flag215-))
(Flag215prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-55
:parameters ()
:precondition
(and
(not (Flag216-))
(Flag216prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-56
:parameters ()
:precondition
(and
(not (Flag217-))
(Flag217prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-57
:parameters ()
:precondition
(and
(not (Flag218-))
(Flag218prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-58
:parameters ()
:precondition
(and
(not (Flag219-))
(Flag219prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-59
:parameters ()
:precondition
(and
(not (Flag220-))
(Flag220prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-60
:parameters ()
:precondition
(and
(not (Flag221-))
(Flag221prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-61
:parameters ()
:precondition
(and
(not (Flag222-))
(Flag222prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-62
:parameters ()
:precondition
(and
(not (Flag223-))
(Flag223prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-63
:parameters ()
:precondition
(and
(not (Flag224-))
(Flag224prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-64
:parameters ()
:precondition
(and
(not (Flag225-))
(Flag225prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-65
:parameters ()
:precondition
(and
(not (Flag226-))
(Flag226prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-66
:parameters ()
:precondition
(and
(not (Flag227-))
(Flag227prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-67
:parameters ()
:precondition
(and
(not (Flag228-))
(Flag228prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-68
:parameters ()
:precondition
(and
(not (Flag229-))
(Flag229prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-69
:parameters ()
:precondition
(and
(not (Flag230-))
(Flag230prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-70
:parameters ()
:precondition
(and
(not (Flag231-))
(Flag231prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-71
:parameters ()
:precondition
(and
(not (Flag232-))
(Flag232prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-72
:parameters ()
:precondition
(and
(not (Flag233-))
(Flag233prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-73
:parameters ()
:precondition
(and
(not (Flag234-))
(Flag234prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-74
:parameters ()
:precondition
(and
(not (Flag235-))
(Flag235prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-75
:parameters ()
:precondition
(and
(not (Flag236-))
(Flag236prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-76
:parameters ()
:precondition
(and
(not (Flag237-))
(Flag237prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-77
:parameters ()
:precondition
(and
(not (Flag238-))
(Flag238prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-78
:parameters ()
:precondition
(and
(not (Flag239-))
(Flag239prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-79
:parameters ()
:precondition
(and
(not (Flag240-))
(Flag240prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-80
:parameters ()
:precondition
(and
(not (Flag241-))
(Flag241prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-81
:parameters ()
:precondition
(and
(not (Flag242-))
(Flag242prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-82
:parameters ()
:precondition
(and
(not (Flag243-))
(Flag243prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-83
:parameters ()
:precondition
(and
(not (Flag244-))
(Flag244prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-84
:parameters ()
:precondition
(and
(not (Flag245-))
(Flag245prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-85
:parameters ()
:precondition
(and
(not (Flag246-))
(Flag246prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-86
:parameters ()
:precondition
(and
(not (Flag247-))
(Flag247prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-87
:parameters ()
:precondition
(and
(not (Flag248-))
(Flag248prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-88
:parameters ()
:precondition
(and
(not (Flag249-))
(Flag249prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-89
:parameters ()
:precondition
(and
(not (Flag250-))
(Flag250prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-90
:parameters ()
:precondition
(and
(not (Flag251-))
(Flag251prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-91
:parameters ()
:precondition
(and
(not (Flag252-))
(Flag252prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-92
:parameters ()
:precondition
(and
(not (Flag253-))
(Flag253prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-93
:parameters ()
:precondition
(and
(not (Flag254-))
(Flag254prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-94
:parameters ()
:precondition
(and
(not (Flag255-))
(Flag255prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-95
:parameters ()
:precondition
(and
(not (Flag256-))
(Flag256prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-96
:parameters ()
:precondition
(and
(not (Flag257-))
(Flag257prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-97
:parameters ()
:precondition
(and
(not (Flag258-))
(Flag258prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-98
:parameters ()
:precondition
(and
(not (Flag259-))
(Flag259prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-99
:parameters ()
:precondition
(and
(not (Flag260-))
(Flag260prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-100
:parameters ()
:precondition
(and
(not (Flag261-))
(Flag261prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-101
:parameters ()
:precondition
(and
(not (Flag262-))
(Flag262prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-102
:parameters ()
:precondition
(and
(not (Flag263-))
(Flag263prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-103
:parameters ()
:precondition
(and
(not (Flag264-))
(Flag264prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-104
:parameters ()
:precondition
(and
(not (Flag265-))
(Flag265prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-105
:parameters ()
:precondition
(and
(not (Flag266-))
(Flag266prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-106
:parameters ()
:precondition
(and
(not (Flag267-))
(Flag267prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-107
:parameters ()
:precondition
(and
(not (Flag268-))
(Flag268prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-108
:parameters ()
:precondition
(and
(not (Flag269-))
(Flag269prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-109
:parameters ()
:precondition
(and
(not (Flag270-))
(Flag270prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-110
:parameters ()
:precondition
(and
(not (Flag271-))
(Flag271prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-111
:parameters ()
:precondition
(and
(not (Flag272-))
(Flag272prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-112
:parameters ()
:precondition
(and
(not (Flag273-))
(Flag273prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-113
:parameters ()
:precondition
(and
(not (Flag274-))
(Flag274prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-114
:parameters ()
:precondition
(and
(not (Flag275-))
(Flag275prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-115
:parameters ()
:precondition
(and
(not (Flag276-))
(Flag276prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-116
:parameters ()
:precondition
(and
(not (Flag277-))
(Flag277prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-117
:parameters ()
:precondition
(and
(not (Flag278-))
(Flag278prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-118
:parameters ()
:precondition
(and
(not (Flag279-))
(Flag279prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action Prim281Action-119
:parameters ()
:precondition
(and
(not (Flag280-))
(Flag280prime-)
)
:effect
(and
(Flag281prime-)
(not (Flag281-))
)

)
(:action ASSIGNMATERIALSAGENT-E
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(MATERIALSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-F)
(not (NOT-ELECTRONICENGINEER-F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-E)
(not (NOT-ELECTRONICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag281prime-)
)
:effect
(and
(when
(and
(Flag281-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-F)
(not (NOT-INFORMATICENGINEER-F))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-E)
(not (NOT-INFORMATICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag160prime-)
)
:effect
(and
(when
(and
(Flag160-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag280prime-))
(not (Flag279prime-))
(not (Flag278prime-))
(not (Flag277prime-))
(not (Flag276prime-))
(not (Flag275prime-))
(not (Flag274prime-))
(not (Flag273prime-))
(not (Flag272prime-))
(not (Flag271prime-))
(not (Flag270prime-))
(not (Flag269prime-))
(not (Flag268prime-))
(not (Flag267prime-))
(not (Flag266prime-))
(not (Flag265prime-))
(not (Flag264prime-))
(not (Flag263prime-))
(not (Flag262prime-))
(not (Flag261prime-))
(not (Flag260prime-))
(not (Flag259prime-))
(not (Flag258prime-))
(not (Flag257prime-))
(not (Flag256prime-))
(not (Flag255prime-))
(not (Flag254prime-))
(not (Flag253prime-))
(not (Flag252prime-))
(not (Flag251prime-))
(not (Flag250prime-))
(not (Flag249prime-))
(not (Flag248prime-))
(not (Flag247prime-))
(not (Flag246prime-))
(not (Flag245prime-))
(not (Flag244prime-))
(not (Flag243prime-))
(not (Flag242prime-))
(not (Flag241prime-))
(not (Flag240prime-))
(not (Flag239prime-))
(not (Flag238prime-))
(not (Flag237prime-))
(not (Flag236prime-))
(not (Flag235prime-))
(not (Flag234prime-))
(not (Flag233prime-))
(not (Flag232prime-))
(not (Flag231prime-))
(not (Flag230prime-))
(not (Flag229prime-))
(not (Flag228prime-))
(not (Flag227prime-))
(not (Flag226prime-))
(not (Flag225prime-))
(not (Flag224prime-))
(not (Flag223prime-))
(not (Flag222prime-))
(not (Flag221prime-))
(not (Flag220prime-))
(not (Flag219prime-))
(not (Flag218prime-))
(not (Flag217prime-))
(not (Flag216prime-))
(not (Flag215prime-))
(not (Flag214prime-))
(not (Flag213prime-))
(not (Flag212prime-))
(not (Flag211prime-))
(not (Flag210prime-))
(not (Flag209prime-))
(not (Flag208prime-))
(not (Flag207prime-))
(not (Flag206prime-))
(not (Flag205prime-))
(not (Flag204prime-))
(not (Flag203prime-))
(not (Flag202prime-))
(not (Flag201prime-))
(not (Flag200prime-))
(not (Flag199prime-))
(not (Flag198prime-))
(not (Flag197prime-))
(not (Flag196prime-))
(not (Flag195prime-))
(not (Flag194prime-))
(not (Flag193prime-))
(not (Flag192prime-))
(not (Flag191prime-))
(not (Flag190prime-))
(not (Flag189prime-))
(not (Flag188prime-))
(not (Flag187prime-))
(not (Flag186prime-))
(not (Flag185prime-))
(not (Flag184prime-))
(not (Flag183prime-))
(not (Flag182prime-))
(not (Flag181prime-))
(not (Flag180prime-))
(not (Flag179prime-))
(not (Flag178prime-))
(not (Flag177prime-))
(not (Flag176prime-))
(not (Flag175prime-))
(not (Flag174prime-))
(not (Flag173prime-))
(not (Flag172prime-))
(not (Flag171prime-))
(not (Flag170prime-))
(not (Flag169prime-))
(not (Flag168prime-))
(not (Flag167prime-))
(not (Flag166prime-))
(not (Flag165prime-))
(not (Flag164prime-))
(not (Flag163prime-))
(not (Flag162prime-))
(not (Flag161prime-))
(not (Flag159prime-))
(not (Flag158prime-))
(not (Flag157prime-))
(not (Flag156prime-))
(not (Flag155prime-))
(not (Flag154prime-))
(not (Flag153prime-))
(not (Flag152prime-))
(not (Flag151prime-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag90prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag37prime-))
(not (Flag281prime-))
(not (Flag160prime-))
(not (Flag38prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag30prime-))
(not (Flag29prime-))
(not (Flag28prime-))
(not (Flag27prime-))
(not (Flag26prime-))
(not (Flag25prime-))
(not (Flag24prime-))
(not (Flag23prime-))
(not (Flag22prime-))
(not (Flag21prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag31prime-))
(not (Flag32prime-))
)
)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim32Action-2
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag44Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag44Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag80Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag80Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag83Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag83Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag85Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag85Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag90Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag90Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Flag91Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag96Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag108Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag108Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag111Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag111Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag116Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag116Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag121Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag121Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag126Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag136Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag144Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag145Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag145Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag146Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag146Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag147Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag147Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag149Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag150Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag150Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag151Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag151Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Flag152Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag152Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag152-)
(Flag152prime-)
)

)
(:action Flag153Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag153Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag153Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag153-)
(Flag153prime-)
)

)
(:action Flag154Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag154Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag154Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag154-)
(Flag154prime-)
)

)
(:action Flag155Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag155Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag155Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag155-)
(Flag155prime-)
)

)
(:action Flag156Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag156Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag156-)
(Flag156prime-)
)

)
(:action Flag157Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag157Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag157-)
(Flag157prime-)
)

)
(:action Flag158Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag158Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag158-)
(Flag158prime-)
)

)
(:action Flag159Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-F)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Flag159Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag159-)
(Flag159prime-)
)

)
(:action Prim40Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim91Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim126Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim144Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim149Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
(:action Prim151Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim152Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag152prime-)
(not (Flag152-))
)

)
(:action Prim153Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag153prime-)
(not (Flag153-))
)

)
(:action Prim154Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag154prime-)
(not (Flag154-))
)

)
(:action Prim155Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag155prime-)
(not (Flag155-))
)

)
(:action Prim156Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag156prime-)
(not (Flag156-))
)

)
(:action Prim157Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag157prime-)
(not (Flag157-))
)

)
(:action Prim158Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag158prime-)
(not (Flag158-))
)

)
(:action Prim159Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-F))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag159prime-)
(not (Flag159-))
)

)
(:action Flag161Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag161Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag161-)
(Flag161prime-)
)

)
(:action Flag162Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag162Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag162-)
(Flag162prime-)
)

)
(:action Flag163Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag163Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag163-)
(Flag163prime-)
)

)
(:action Flag164Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag164Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag164-)
(Flag164prime-)
)

)
(:action Flag165Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag165Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag165-)
(Flag165prime-)
)

)
(:action Flag166Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag166Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag166-)
(Flag166prime-)
)

)
(:action Flag167Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag167Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag167-)
(Flag167prime-)
)

)
(:action Flag168Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag168Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag168-)
(Flag168prime-)
)

)
(:action Flag169Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag169Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag169-)
(Flag169prime-)
)

)
(:action Flag170Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag170Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag170-)
(Flag170prime-)
)

)
(:action Flag171Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag171Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag171Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag171-)
(Flag171prime-)
)

)
(:action Flag172Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag172Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag172Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag172-)
(Flag172prime-)
)

)
(:action Flag173Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag173Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag173-)
(Flag173prime-)
)

)
(:action Flag174Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag174Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag174Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag174-)
(Flag174prime-)
)

)
(:action Flag175Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag175Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag175-)
(Flag175prime-)
)

)
(:action Flag176Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag176Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag176-)
(Flag176prime-)
)

)
(:action Flag177Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag177Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag177-)
(Flag177prime-)
)

)
(:action Flag178Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag178Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag178-)
(Flag178prime-)
)

)
(:action Flag179Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag179Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag179-)
(Flag179prime-)
)

)
(:action Flag180Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag180Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag180-)
(Flag180prime-)
)

)
(:action Flag181Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag181Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag181-)
(Flag181prime-)
)

)
(:action Flag182Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag182Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag182-)
(Flag182prime-)
)

)
(:action Flag183Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag183Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag183-)
(Flag183prime-)
)

)
(:action Flag184Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag184Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag184-)
(Flag184prime-)
)

)
(:action Flag185Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag185Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag185-)
(Flag185prime-)
)

)
(:action Flag186Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag186Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag186Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag186-)
(Flag186prime-)
)

)
(:action Flag187Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag187Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag187-)
(Flag187prime-)
)

)
(:action Flag188Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag188Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag188Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag188-)
(Flag188prime-)
)

)
(:action Flag189Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag189Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag189-)
(Flag189prime-)
)

)
(:action Flag190Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag190Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag190Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag190-)
(Flag190prime-)
)

)
(:action Flag191Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag191Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag191-)
(Flag191prime-)
)

)
(:action Flag192Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag192Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag192Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag192-)
(Flag192prime-)
)

)
(:action Flag193Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag193Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag193-)
(Flag193prime-)
)

)
(:action Flag194Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag194Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag194-)
(Flag194prime-)
)

)
(:action Flag195Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag195Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag195-)
(Flag195prime-)
)

)
(:action Flag196Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag196Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag196Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag196-)
(Flag196prime-)
)

)
(:action Flag197Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag197Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag197Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag197-)
(Flag197prime-)
)

)
(:action Flag198Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag198Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag198-)
(Flag198prime-)
)

)
(:action Flag199Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag199Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag199Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag199-)
(Flag199prime-)
)

)
(:action Flag200Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag200Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag200-)
(Flag200prime-)
)

)
(:action Flag201Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag201Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag201Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag201-)
(Flag201prime-)
)

)
(:action Flag202Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag202Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag202-)
(Flag202prime-)
)

)
(:action Flag203Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag203Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag203-)
(Flag203prime-)
)

)
(:action Flag204Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag204Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag204-)
(Flag204prime-)
)

)
(:action Flag205Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag205Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag205-)
(Flag205prime-)
)

)
(:action Flag206Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag206Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag206-)
(Flag206prime-)
)

)
(:action Flag207Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag207Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag207-)
(Flag207prime-)
)

)
(:action Flag208Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag208Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag208-)
(Flag208prime-)
)

)
(:action Flag209Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag209Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag209-)
(Flag209prime-)
)

)
(:action Flag210Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag210Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag210-)
(Flag210prime-)
)

)
(:action Flag211Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag211Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag211-)
(Flag211prime-)
)

)
(:action Flag212Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag212Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag212-)
(Flag212prime-)
)

)
(:action Flag213Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag213Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag213-)
(Flag213prime-)
)

)
(:action Flag214Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag214Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag214-)
(Flag214prime-)
)

)
(:action Flag215Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag215Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag215-)
(Flag215prime-)
)

)
(:action Flag216Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag216Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag216-)
(Flag216prime-)
)

)
(:action Flag217Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag217Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag217-)
(Flag217prime-)
)

)
(:action Flag218Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag218Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag218-)
(Flag218prime-)
)

)
(:action Flag219Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag219Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag219-)
(Flag219prime-)
)

)
(:action Flag220Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag220Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag220-)
(Flag220prime-)
)

)
(:action Flag221Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag221Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag221-)
(Flag221prime-)
)

)
(:action Flag222Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag222Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag222-)
(Flag222prime-)
)

)
(:action Flag223Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag223Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag223-)
(Flag223prime-)
)

)
(:action Flag224Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag224Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag224-)
(Flag224prime-)
)

)
(:action Flag225Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag225Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag225-)
(Flag225prime-)
)

)
(:action Flag226Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag226Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag226Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag226-)
(Flag226prime-)
)

)
(:action Flag227Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag227Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag227Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag227-)
(Flag227prime-)
)

)
(:action Flag228Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag228Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag228-)
(Flag228prime-)
)

)
(:action Flag229Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag229Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag229Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag229-)
(Flag229prime-)
)

)
(:action Flag230Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag230Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag230-)
(Flag230prime-)
)

)
(:action Flag231Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag231Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag231-)
(Flag231prime-)
)

)
(:action Flag232Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag232Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag232-)
(Flag232prime-)
)

)
(:action Flag233Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag233Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag233Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag233-)
(Flag233prime-)
)

)
(:action Flag234Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag234Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag234-)
(Flag234prime-)
)

)
(:action Flag235Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag235Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag235-)
(Flag235prime-)
)

)
(:action Flag236Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag236Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag236Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag236-)
(Flag236prime-)
)

)
(:action Flag237Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag237Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag237-)
(Flag237prime-)
)

)
(:action Flag238Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag238Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag238-)
(Flag238prime-)
)

)
(:action Flag239Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag239Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag239-)
(Flag239prime-)
)

)
(:action Flag240Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag240Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag240-)
(Flag240prime-)
)

)
(:action Flag241Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag241Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag241-)
(Flag241prime-)
)

)
(:action Flag242Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag242Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag242-)
(Flag242prime-)
)

)
(:action Flag243Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag243Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag243-)
(Flag243prime-)
)

)
(:action Flag244Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag244Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag244-)
(Flag244prime-)
)

)
(:action Flag245Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag245Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag245-)
(Flag245prime-)
)

)
(:action Flag246Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag246Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag246-)
(Flag246prime-)
)

)
(:action Flag247Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag247Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag247-)
(Flag247prime-)
)

)
(:action Flag248Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag248Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag248-)
(Flag248prime-)
)

)
(:action Flag249Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag249Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag249-)
(Flag249prime-)
)

)
(:action Flag250Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag250Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag250-)
(Flag250prime-)
)

)
(:action Flag251Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag251Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag251-)
(Flag251prime-)
)

)
(:action Flag252Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag252Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag252-)
(Flag252prime-)
)

)
(:action Flag253Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag253Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag253-)
(Flag253prime-)
)

)
(:action Flag254Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag254Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag254-)
(Flag254prime-)
)

)
(:action Flag255Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag255Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag255-)
(Flag255prime-)
)

)
(:action Flag256Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag256Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag256-)
(Flag256prime-)
)

)
(:action Flag257Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag257Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag257-)
(Flag257prime-)
)

)
(:action Flag258Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag258Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag258-)
(Flag258prime-)
)

)
(:action Flag259Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag259Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag259-)
(Flag259prime-)
)

)
(:action Flag260Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag260Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag260-)
(Flag260prime-)
)

)
(:action Flag261Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag261Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag261-)
(Flag261prime-)
)

)
(:action Flag262Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag262Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag262-)
(Flag262prime-)
)

)
(:action Flag263Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag263Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag263-)
(Flag263prime-)
)

)
(:action Flag264Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag264Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag264-)
(Flag264prime-)
)

)
(:action Flag265Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag265Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag265-)
(Flag265prime-)
)

)
(:action Flag266Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag266Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag266-)
(Flag266prime-)
)

)
(:action Flag267Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag267Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag267-)
(Flag267prime-)
)

)
(:action Flag268Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag268Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag268-)
(Flag268prime-)
)

)
(:action Flag269Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag269Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag269-)
(Flag269prime-)
)

)
(:action Flag270Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag270Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag270-)
(Flag270prime-)
)

)
(:action Flag271Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag271Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag271-)
(Flag271prime-)
)

)
(:action Flag272Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag272Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag272-)
(Flag272prime-)
)

)
(:action Flag273Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag273Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag273-)
(Flag273prime-)
)

)
(:action Flag274Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag274Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag274-)
(Flag274prime-)
)

)
(:action Flag275Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag275Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag275-)
(Flag275prime-)
)

)
(:action Flag276Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag276Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag276-)
(Flag276prime-)
)

)
(:action Flag277Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag277Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag277-)
(Flag277prime-)
)

)
(:action Flag278Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag278Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag278-)
(Flag278prime-)
)

)
(:action Flag279Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag279Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag279-)
(Flag279prime-)
)

)
(:action Flag280Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-F)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Flag280Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag280-)
(Flag280prime-)
)

)
(:action Prim161Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag161prime-)
(not (Flag161-))
)

)
(:action Prim162Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag162prime-)
(not (Flag162-))
)

)
(:action Prim163Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag163prime-)
(not (Flag163-))
)

)
(:action Prim164Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag164prime-)
(not (Flag164-))
)

)
(:action Prim165Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag165prime-)
(not (Flag165-))
)

)
(:action Prim166Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag166prime-)
(not (Flag166-))
)

)
(:action Prim167Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag167prime-)
(not (Flag167-))
)

)
(:action Prim168Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag168prime-)
(not (Flag168-))
)

)
(:action Prim169Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag169prime-)
(not (Flag169-))
)

)
(:action Prim170Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag170prime-)
(not (Flag170-))
)

)
(:action Prim171Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag171prime-)
(not (Flag171-))
)

)
(:action Prim172Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag172prime-)
(not (Flag172-))
)

)
(:action Prim173Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag173prime-)
(not (Flag173-))
)

)
(:action Prim174Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag174prime-)
(not (Flag174-))
)

)
(:action Prim175Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag175prime-)
(not (Flag175-))
)

)
(:action Prim176Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag176prime-)
(not (Flag176-))
)

)
(:action Prim177Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag177prime-)
(not (Flag177-))
)

)
(:action Prim178Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag178prime-)
(not (Flag178-))
)

)
(:action Prim179Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag179prime-)
(not (Flag179-))
)

)
(:action Prim180Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag180prime-)
(not (Flag180-))
)

)
(:action Prim181Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag181prime-)
(not (Flag181-))
)

)
(:action Prim182Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag182prime-)
(not (Flag182-))
)

)
(:action Prim183Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag183prime-)
(not (Flag183-))
)

)
(:action Prim184Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag184prime-)
(not (Flag184-))
)

)
(:action Prim185Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag185prime-)
(not (Flag185-))
)

)
(:action Prim186Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag186prime-)
(not (Flag186-))
)

)
(:action Prim187Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag187prime-)
(not (Flag187-))
)

)
(:action Prim188Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag188prime-)
(not (Flag188-))
)

)
(:action Prim189Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag189prime-)
(not (Flag189-))
)

)
(:action Prim190Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag190prime-)
(not (Flag190-))
)

)
(:action Prim191Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag191prime-)
(not (Flag191-))
)

)
(:action Prim192Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag192prime-)
(not (Flag192-))
)

)
(:action Prim193Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag193prime-)
(not (Flag193-))
)

)
(:action Prim194Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag194prime-)
(not (Flag194-))
)

)
(:action Prim195Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag195prime-)
(not (Flag195-))
)

)
(:action Prim196Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag196prime-)
(not (Flag196-))
)

)
(:action Prim197Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag197prime-)
(not (Flag197-))
)

)
(:action Prim198Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag198prime-)
(not (Flag198-))
)

)
(:action Prim199Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag199prime-)
(not (Flag199-))
)

)
(:action Prim200Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag200prime-)
(not (Flag200-))
)

)
(:action Prim201Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag201prime-)
(not (Flag201-))
)

)
(:action Prim202Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag202prime-)
(not (Flag202-))
)

)
(:action Prim203Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag203prime-)
(not (Flag203-))
)

)
(:action Prim204Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag204prime-)
(not (Flag204-))
)

)
(:action Prim205Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag205prime-)
(not (Flag205-))
)

)
(:action Prim206Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag206prime-)
(not (Flag206-))
)

)
(:action Prim207Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag207prime-)
(not (Flag207-))
)

)
(:action Prim208Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag208prime-)
(not (Flag208-))
)

)
(:action Prim209Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag209prime-)
(not (Flag209-))
)

)
(:action Prim210Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag210prime-)
(not (Flag210-))
)

)
(:action Prim211Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag211prime-)
(not (Flag211-))
)

)
(:action Prim212Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag212prime-)
(not (Flag212-))
)

)
(:action Prim213Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag213prime-)
(not (Flag213-))
)

)
(:action Prim214Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag214prime-)
(not (Flag214-))
)

)
(:action Prim215Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag215prime-)
(not (Flag215-))
)

)
(:action Prim216Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag216prime-)
(not (Flag216-))
)

)
(:action Prim217Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag217prime-)
(not (Flag217-))
)

)
(:action Prim218Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag218prime-)
(not (Flag218-))
)

)
(:action Prim219Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag219prime-)
(not (Flag219-))
)

)
(:action Prim220Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag220prime-)
(not (Flag220-))
)

)
(:action Prim221Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag221prime-)
(not (Flag221-))
)

)
(:action Prim222Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag222prime-)
(not (Flag222-))
)

)
(:action Prim223Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag223prime-)
(not (Flag223-))
)

)
(:action Prim224Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag224prime-)
(not (Flag224-))
)

)
(:action Prim225Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag225prime-)
(not (Flag225-))
)

)
(:action Prim226Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag226prime-)
(not (Flag226-))
)

)
(:action Prim227Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag227prime-)
(not (Flag227-))
)

)
(:action Prim228Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag228prime-)
(not (Flag228-))
)

)
(:action Prim229Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag229prime-)
(not (Flag229-))
)

)
(:action Prim230Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag230prime-)
(not (Flag230-))
)

)
(:action Prim231Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag231prime-)
(not (Flag231-))
)

)
(:action Prim232Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag232prime-)
(not (Flag232-))
)

)
(:action Prim233Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag233prime-)
(not (Flag233-))
)

)
(:action Prim234Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag234prime-)
(not (Flag234-))
)

)
(:action Prim235Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag235prime-)
(not (Flag235-))
)

)
(:action Prim236Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag236prime-)
(not (Flag236-))
)

)
(:action Prim237Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag237prime-)
(not (Flag237-))
)

)
(:action Prim238Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag238prime-)
(not (Flag238-))
)

)
(:action Prim239Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag239prime-)
(not (Flag239-))
)

)
(:action Prim240Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag240prime-)
(not (Flag240-))
)

)
(:action Prim241Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag241prime-)
(not (Flag241-))
)

)
(:action Prim242Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag242prime-)
(not (Flag242-))
)

)
(:action Prim243Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag243prime-)
(not (Flag243-))
)

)
(:action Prim244Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag244prime-)
(not (Flag244-))
)

)
(:action Prim245Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag245prime-)
(not (Flag245-))
)

)
(:action Prim246Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag246prime-)
(not (Flag246-))
)

)
(:action Prim247Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag247prime-)
(not (Flag247-))
)

)
(:action Prim248Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag248prime-)
(not (Flag248-))
)

)
(:action Prim249Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag249prime-)
(not (Flag249-))
)

)
(:action Prim250Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag250prime-)
(not (Flag250-))
)

)
(:action Prim251Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag251prime-)
(not (Flag251-))
)

)
(:action Prim252Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag252prime-)
(not (Flag252-))
)

)
(:action Prim253Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag253prime-)
(not (Flag253-))
)

)
(:action Prim254Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag254prime-)
(not (Flag254-))
)

)
(:action Prim255Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag255prime-)
(not (Flag255-))
)

)
(:action Prim256Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-F))
)
:effect
(and
(Flag256prime-)
(not (Flag256-))
)

)
(:action Prim257Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag257prime-)
(not (Flag257-))
)

)
(:action Prim258Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag258prime-)
(not (Flag258-))
)

)
(:action Prim259Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag259prime-)
(not (Flag259-))
)

)
(:action Prim260Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag260prime-)
(not (Flag260-))
)

)
(:action Prim261Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag261prime-)
(not (Flag261-))
)

)
(:action Prim262Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag262prime-)
(not (Flag262-))
)

)
(:action Prim263Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag263prime-)
(not (Flag263-))
)

)
(:action Prim264Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag264prime-)
(not (Flag264-))
)

)
(:action Prim265Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag265prime-)
(not (Flag265-))
)

)
(:action Prim266Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag266prime-)
(not (Flag266-))
)

)
(:action Prim267Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag267prime-)
(not (Flag267-))
)

)
(:action Prim268Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag268prime-)
(not (Flag268-))
)

)
(:action Prim269Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag269prime-)
(not (Flag269-))
)

)
(:action Prim270Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag270prime-)
(not (Flag270-))
)

)
(:action Prim271Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag271prime-)
(not (Flag271-))
)

)
(:action Prim272Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag272prime-)
(not (Flag272-))
)

)
(:action Prim273Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag273prime-)
(not (Flag273-))
)

)
(:action Prim274Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag274prime-)
(not (Flag274-))
)

)
(:action Prim275Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag275prime-)
(not (Flag275-))
)

)
(:action Prim276Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag276prime-)
(not (Flag276-))
)

)
(:action Prim277Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag277prime-)
(not (Flag277-))
)

)
(:action Prim278Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag278prime-)
(not (Flag278-))
)

)
(:action Prim279Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag279prime-)
(not (Flag279-))
)

)
(:action Prim280Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-F))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag280prime-)
(not (Flag280-))
)

)
)
