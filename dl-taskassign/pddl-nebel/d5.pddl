(define (domain grounded-SIMPLE-ADL-TASKASSIGMENT)
(:requirements
:strips
)
(:predicates
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag23-)
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(ELECTRONICSAGENT-D)
(DESIGNAGENT-C)
(DESIGNAGENT-D)
(SPECIFICATIONSAGENT-C)
(SPECIFICATIONSAGENT-D)
(MATERIALSAGENT-C)
(MATERIALSAGENT-D)
(TASKAGENT-C)
(TASKAGENT-D)
(CODINGAGENT-C)
(CODINGAGENT-D)
(SOFTWAREAGENT-C)
(SOFTWAREAGENT-D)
(INFORMATICENGINEER-A)
(INFORMATICENGINEER-C)
(INFORMATICENGINEER-B)
(INFORMATICENGINEER-E)
(INFORMATICENGINEER-D)
(TESTINGAGENT-C)
(TESTINGAGENT-D)
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
(Flag151-)
(Flag90-)
(Flag28-)
(Flag26-)
(Flag24-)
(ERROR-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag21-)
(ELECTRONICSAGENT-A)
(ELECTRONICSAGENT-B)
(ELECTRONICSAGENT-E)
(DESIGNAGENT-A)
(DESIGNAGENT-B)
(DESIGNAGENT-E)
(SPECIFICATIONSAGENT-A)
(SPECIFICATIONSAGENT-B)
(SPECIFICATIONSAGENT-E)
(MATERIALSAGENT-A)
(MATERIALSAGENT-B)
(MATERIALSAGENT-E)
(TASKAGENT-A)
(TASKAGENT-B)
(TASKAGENT-E)
(CODINGAGENT-A)
(CODINGAGENT-B)
(CODINGAGENT-E)
(SOFTWAREAGENT-A)
(SOFTWAREAGENT-B)
(SOFTWAREAGENT-E)
(TESTINGAGENT-A)
(TESTINGAGENT-B)
(TESTINGAGENT-E)
(Flag22-)
(NOT-CHECKCONSISTENCY-)
(NOT-SOFTWAREAGENT-E)
(NOT-SOFTWAREAGENT-B)
(NOT-SOFTWAREAGENT-A)
(NOT-CODINGAGENT-E)
(NOT-CODINGAGENT-B)
(NOT-CODINGAGENT-A)
(NOT-SPECIFICATIONSAGENT-E)
(NOT-SPECIFICATIONSAGENT-B)
(NOT-SPECIFICATIONSAGENT-A)
(NOT-ELECTRONICSAGENT-E)
(NOT-ELECTRONICSAGENT-B)
(NOT-ELECTRONICSAGENT-A)
(NOT-ERROR-)
(NOT-ELECTRONICENGINEER-D)
(NOT-ELECTRONICENGINEER-E)
(NOT-ELECTRONICENGINEER-B)
(NOT-ELECTRONICENGINEER-C)
(NOT-ELECTRONICENGINEER-A)
(NOT-INFORMATICENGINEER-D)
(NOT-INFORMATICENGINEER-E)
(NOT-INFORMATICENGINEER-B)
(NOT-INFORMATICENGINEER-C)
(NOT-INFORMATICENGINEER-A)
(NOT-SOFTWAREAGENT-D)
(NOT-SOFTWAREAGENT-C)
(NOT-CODINGAGENT-D)
(NOT-CODINGAGENT-C)
(NOT-SPECIFICATIONSAGENT-D)
(NOT-SPECIFICATIONSAGENT-C)
(NOT-ELECTRONICSAGENT-D)
(NOT-ELECTRONICSAGENT-C)
(Flag150prime-)
(Flag149prime-)
(Flag148prime-)
(Flag147prime-)
(Flag146prime-)
(Flag145prime-)
(Flag144prime-)
(Flag143prime-)
(Flag142prime-)
(Flag141prime-)
(Flag140prime-)
(Flag139prime-)
(Flag138prime-)
(Flag137prime-)
(Flag136prime-)
(Flag135prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag131prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag126prime-)
(Flag125prime-)
(Flag124prime-)
(Flag123prime-)
(Flag122prime-)
(Flag121prime-)
(Flag120prime-)
(Flag119prime-)
(Flag118prime-)
(Flag117prime-)
(Flag116prime-)
(Flag115prime-)
(Flag114prime-)
(Flag113prime-)
(Flag112prime-)
(Flag111prime-)
(Flag110prime-)
(Flag109prime-)
(Flag108prime-)
(Flag107prime-)
(Flag106prime-)
(Flag105prime-)
(Flag104prime-)
(Flag103prime-)
(Flag102prime-)
(Flag101prime-)
(Flag100prime-)
(Flag99prime-)
(Flag98prime-)
(Flag97prime-)
(Flag96prime-)
(Flag95prime-)
(Flag94prime-)
(Flag93prime-)
(Flag92prime-)
(Flag91prime-)
(Flag89prime-)
(Flag88prime-)
(Flag87prime-)
(Flag86prime-)
(Flag85prime-)
(Flag84prime-)
(Flag83prime-)
(Flag82prime-)
(Flag81prime-)
(Flag80prime-)
(Flag79prime-)
(Flag78prime-)
(Flag77prime-)
(Flag76prime-)
(Flag75prime-)
(Flag74prime-)
(Flag73prime-)
(Flag72prime-)
(Flag71prime-)
(Flag70prime-)
(Flag69prime-)
(Flag68prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag64prime-)
(Flag63prime-)
(Flag62prime-)
(Flag61prime-)
(Flag60prime-)
(Flag59prime-)
(Flag58prime-)
(Flag57prime-)
(Flag56prime-)
(Flag55prime-)
(Flag54prime-)
(Flag53prime-)
(Flag52prime-)
(Flag51prime-)
(Flag50prime-)
(Flag49prime-)
(Flag48prime-)
(Flag47prime-)
(Flag46prime-)
(Flag45prime-)
(Flag44prime-)
(Flag43prime-)
(Flag42prime-)
(Flag41prime-)
(Flag40prime-)
(Flag39prime-)
(Flag38prime-)
(Flag37prime-)
(Flag36prime-)
(Flag35prime-)
(Flag34prime-)
(Flag33prime-)
(Flag32prime-)
(Flag31prime-)
(Flag30prime-)
(Flag23prime-)
(Flag151prime-)
(Flag90prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag20prime-)
(Flag19prime-)
(Flag18prime-)
(Flag17prime-)
(Flag16prime-)
(Flag15prime-)
(Flag14prime-)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag1prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag21prime-)
(Flag22prime-)
)
(:action Flag22Action
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Prim22Action-0
:parameters ()
:precondition
(and
(not (Flag21-))
(Flag21prime-)
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-8
:parameters ()
:precondition
(and
(TASKAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim24Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-E))
(not (SPECIFICATIONSAGENT-E))
(not (ELECTRONICSAGENT-E))
(not (CODINGAGENT-E))
(not (DESIGNAGENT-E))
(not (INFORMATICENGINEER-E))
(not (TESTINGAGENT-E))
(not (ELECTRONICENGINEER-E))
(not (TASKAGENT-E))
(not (SOFTWAREAGENT-E))
)
:effect
(and
(Flag24prime-)
(not (Flag24-))
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-8
:parameters ()
:precondition
(and
(TASKAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim26Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-B))
(not (SPECIFICATIONSAGENT-B))
(not (ELECTRONICSAGENT-B))
(not (CODINGAGENT-B))
(not (DESIGNAGENT-B))
(not (INFORMATICENGINEER-B))
(not (TESTINGAGENT-B))
(not (ELECTRONICENGINEER-B))
(not (TASKAGENT-B))
(not (SOFTWAREAGENT-B))
)
:effect
(and
(Flag26prime-)
(not (Flag26-))
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(MATERIALSAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(SPECIFICATIONSAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-2
:parameters ()
:precondition
(and
(ELECTRONICSAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-3
:parameters ()
:precondition
(and
(CODINGAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-4
:parameters ()
:precondition
(and
(DESIGNAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-6
:parameters ()
:precondition
(and
(TESTINGAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-8
:parameters ()
:precondition
(and
(TASKAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-9
:parameters ()
:precondition
(and
(SOFTWAREAGENT-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action
:parameters ()
:precondition
(and
(not (MATERIALSAGENT-A))
(not (SPECIFICATIONSAGENT-A))
(not (ELECTRONICSAGENT-A))
(not (CODINGAGENT-A))
(not (DESIGNAGENT-A))
(not (INFORMATICENGINEER-A))
(not (TESTINGAGENT-A))
(not (ELECTRONICENGINEER-A))
(not (TASKAGENT-A))
(not (SOFTWAREAGENT-A))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action ASSIGNTESTINGAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(TESTINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTESTINGAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(TESTINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTESTINGAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(TESTINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(SOFTWAREAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(SOFTWAREAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(SOFTWAREAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNCODINGAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(CODINGAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNCODINGAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(CODINGAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNCODINGAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(CODINGAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTASKAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(TASKAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTASKAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(TASKAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTASKAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(TASKAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNMATERIALSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(MATERIALSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNMATERIALSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(MATERIALSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNMATERIALSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(MATERIALSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(SPECIFICATIONSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNDESIGNAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(DESIGNAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNDESIGNAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(DESIGNAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNDESIGNAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(DESIGNAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-E
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(ELECTRONICSAGENT-E)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-B
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(ELECTRONICSAGENT-B)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-A
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(ELECTRONICSAGENT-A)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action Flag21Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-1
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-2
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-3
:parameters ()
:precondition
(and
(Flag4-)
(Flag4prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-4
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-5
:parameters ()
:precondition
(and
(Flag6-)
(Flag6prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-6
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-7
:parameters ()
:precondition
(and
(Flag8-)
(Flag8prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-8
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-9
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-10
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-11
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-12
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-13
:parameters ()
:precondition
(and
(Flag14-)
(Flag14prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-14
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-15
:parameters ()
:precondition
(and
(Flag16-)
(Flag16prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-16
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-17
:parameters ()
:precondition
(and
(Flag18-)
(Flag18prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-18
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag21Action-19
:parameters ()
:precondition
(and
(Flag20-)
(Flag20prime-)
)
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Prim21Action
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
(not (Flag2-))
(Flag2prime-)
(not (Flag3-))
(Flag3prime-)
(not (Flag4-))
(Flag4prime-)
(not (Flag5-))
(Flag5prime-)
(not (Flag6-))
(Flag6prime-)
(not (Flag7-))
(Flag7prime-)
(not (Flag8-))
(Flag8prime-)
(not (Flag9-))
(Flag9prime-)
(not (Flag10-))
(Flag10prime-)
(not (Flag11-))
(Flag11prime-)
(not (Flag12-))
(Flag12prime-)
(not (Flag13-))
(Flag13prime-)
(not (Flag14-))
(Flag14prime-)
(not (Flag15-))
(Flag15prime-)
(not (Flag16-))
(Flag16prime-)
(not (Flag17-))
(Flag17prime-)
(not (Flag18-))
(Flag18prime-)
(not (Flag19-))
(Flag19prime-)
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag21prime-)
(not (Flag21-))
)

)
(:action Flag25Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag24-)
(Flag24prime-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action-2
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Flag27Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag26-)
(Flag26prime-)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action-2
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Flag29Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag28-)
(Flag28prime-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action-2
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag1Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag8Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-C)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag14Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag15Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag16Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-D)
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag17Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag18Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-C)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag19Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag20Action
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
(ELECTRONICENGINEER-D)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Prim1Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim1Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim2Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim2Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim3Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim4Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim5Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim5Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim6Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim7Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim9Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim12Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim14Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim14Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag14prime-)
(not (Flag14-))
)

)
(:action Prim15Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim15Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag15prime-)
(not (Flag15-))
)

)
(:action Prim16Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim16Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag16prime-)
(not (Flag16-))
)

)
(:action Prim17Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-A))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim17Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag17prime-)
(not (Flag17-))
)

)
(:action Prim18Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-C))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim18Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag18prime-)
(not (Flag18-))
)

)
(:action Prim19Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-B))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim19Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag19prime-)
(not (Flag19-))
)

)
(:action Prim20Action-0
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-E))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action Prim20Action-1
:parameters ()
:precondition
(and
(not (ELECTRONICENGINEER-D))
)
:effect
(and
(Flag20prime-)
(not (Flag20-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(when
(and
(SPECIFICATIONSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-A)
(ELECTRONICSAGENT-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-C)
(ELECTRONICSAGENT-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-B)
(ELECTRONICSAGENT-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-E)
(ELECTRONICSAGENT-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(INFORMATICENGINEER-D)
(ELECTRONICSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-A)
(INFORMATICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-C)
(INFORMATICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-B)
(INFORMATICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-E)
(INFORMATICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICENGINEER-D)
(INFORMATICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-A)
(ELECTRONICENGINEER-A)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-C)
(ELECTRONICENGINEER-C)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-B)
(ELECTRONICENGINEER-B)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-E)
(ELECTRONICENGINEER-E)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-D)
(ELECTRONICENGINEER-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SPECIFICATIONSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(SOFTWAREAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(CODINGAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(when
(and
(ELECTRONICSAGENT-D)
)
(and
(ERROR-)
(not (NOT-ERROR-))
)
)
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action Flag24Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag24Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-E)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag26Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-B)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-5
:parameters ()
:precondition
(and
(INFORMATICENGINEER-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag28Action-7
:parameters ()
:precondition
(and
(ELECTRONICENGINEER-A)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag90Action
:parameters ()
:precondition
(and
(Flag30-)
(Flag30prime-)
(Flag31-)
(Flag31prime-)
(Flag32-)
(Flag32prime-)
(Flag33-)
(Flag33prime-)
(Flag34-)
(Flag34prime-)
(Flag35-)
(Flag35prime-)
(Flag36-)
(Flag36prime-)
(Flag37-)
(Flag37prime-)
(Flag38-)
(Flag38prime-)
(Flag39-)
(Flag39prime-)
(Flag40-)
(Flag40prime-)
(Flag41-)
(Flag41prime-)
(Flag42-)
(Flag42prime-)
(Flag43-)
(Flag43prime-)
(Flag44-)
(Flag44prime-)
(Flag45-)
(Flag45prime-)
(Flag46-)
(Flag46prime-)
(Flag47-)
(Flag47prime-)
(Flag48-)
(Flag48prime-)
(Flag49-)
(Flag49prime-)
(Flag50-)
(Flag50prime-)
(Flag51-)
(Flag51prime-)
(Flag52-)
(Flag52prime-)
(Flag53-)
(Flag53prime-)
(Flag54-)
(Flag54prime-)
(Flag55-)
(Flag55prime-)
(Flag56-)
(Flag56prime-)
(Flag57-)
(Flag57prime-)
(Flag58-)
(Flag58prime-)
(Flag59-)
(Flag59prime-)
(Flag60-)
(Flag60prime-)
(Flag61-)
(Flag61prime-)
(Flag62-)
(Flag62prime-)
(Flag63-)
(Flag63prime-)
(Flag64-)
(Flag64prime-)
(Flag65-)
(Flag65prime-)
(Flag66-)
(Flag66prime-)
(Flag67-)
(Flag67prime-)
(Flag68-)
(Flag68prime-)
(Flag69-)
(Flag69prime-)
(Flag70-)
(Flag70prime-)
(Flag71-)
(Flag71prime-)
(Flag72-)
(Flag72prime-)
(Flag73-)
(Flag73prime-)
(Flag74-)
(Flag74prime-)
(Flag75-)
(Flag75prime-)
(Flag76-)
(Flag76prime-)
(Flag77-)
(Flag77prime-)
(Flag78-)
(Flag78prime-)
(Flag79-)
(Flag79prime-)
(Flag80-)
(Flag80prime-)
(Flag81-)
(Flag81prime-)
(Flag82-)
(Flag82prime-)
(Flag83-)
(Flag83prime-)
(Flag84-)
(Flag84prime-)
(Flag85-)
(Flag85prime-)
(Flag86-)
(Flag86prime-)
(Flag87-)
(Flag87prime-)
(Flag88-)
(Flag88prime-)
(Flag89-)
(Flag89prime-)
)
:effect
(and
(Flag90-)
(Flag90prime-)
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (Flag31-))
(Flag31prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-2
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-3
:parameters ()
:precondition
(and
(not (Flag33-))
(Flag33prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-4
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-5
:parameters ()
:precondition
(and
(not (Flag35-))
(Flag35prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-6
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-7
:parameters ()
:precondition
(and
(not (Flag37-))
(Flag37prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-8
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-9
:parameters ()
:precondition
(and
(not (Flag39-))
(Flag39prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-10
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-11
:parameters ()
:precondition
(and
(not (Flag41-))
(Flag41prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-12
:parameters ()
:precondition
(and
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-13
:parameters ()
:precondition
(and
(not (Flag43-))
(Flag43prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-14
:parameters ()
:precondition
(and
(not (Flag44-))
(Flag44prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-15
:parameters ()
:precondition
(and
(not (Flag45-))
(Flag45prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-16
:parameters ()
:precondition
(and
(not (Flag46-))
(Flag46prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-17
:parameters ()
:precondition
(and
(not (Flag47-))
(Flag47prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-18
:parameters ()
:precondition
(and
(not (Flag48-))
(Flag48prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-19
:parameters ()
:precondition
(and
(not (Flag49-))
(Flag49prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-20
:parameters ()
:precondition
(and
(not (Flag50-))
(Flag50prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-21
:parameters ()
:precondition
(and
(not (Flag51-))
(Flag51prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-22
:parameters ()
:precondition
(and
(not (Flag52-))
(Flag52prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-23
:parameters ()
:precondition
(and
(not (Flag53-))
(Flag53prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-24
:parameters ()
:precondition
(and
(not (Flag54-))
(Flag54prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-25
:parameters ()
:precondition
(and
(not (Flag55-))
(Flag55prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-26
:parameters ()
:precondition
(and
(not (Flag56-))
(Flag56prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-27
:parameters ()
:precondition
(and
(not (Flag57-))
(Flag57prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-28
:parameters ()
:precondition
(and
(not (Flag58-))
(Flag58prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-29
:parameters ()
:precondition
(and
(not (Flag59-))
(Flag59prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-30
:parameters ()
:precondition
(and
(not (Flag60-))
(Flag60prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-31
:parameters ()
:precondition
(and
(not (Flag61-))
(Flag61prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-32
:parameters ()
:precondition
(and
(not (Flag62-))
(Flag62prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-33
:parameters ()
:precondition
(and
(not (Flag63-))
(Flag63prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-34
:parameters ()
:precondition
(and
(not (Flag64-))
(Flag64prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-35
:parameters ()
:precondition
(and
(not (Flag65-))
(Flag65prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-36
:parameters ()
:precondition
(and
(not (Flag66-))
(Flag66prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-37
:parameters ()
:precondition
(and
(not (Flag67-))
(Flag67prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-38
:parameters ()
:precondition
(and
(not (Flag68-))
(Flag68prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-39
:parameters ()
:precondition
(and
(not (Flag69-))
(Flag69prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-40
:parameters ()
:precondition
(and
(not (Flag70-))
(Flag70prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-41
:parameters ()
:precondition
(and
(not (Flag71-))
(Flag71prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-42
:parameters ()
:precondition
(and
(not (Flag72-))
(Flag72prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-43
:parameters ()
:precondition
(and
(not (Flag73-))
(Flag73prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-44
:parameters ()
:precondition
(and
(not (Flag74-))
(Flag74prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-45
:parameters ()
:precondition
(and
(not (Flag75-))
(Flag75prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-46
:parameters ()
:precondition
(and
(not (Flag76-))
(Flag76prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-47
:parameters ()
:precondition
(and
(not (Flag77-))
(Flag77prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-48
:parameters ()
:precondition
(and
(not (Flag78-))
(Flag78prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-49
:parameters ()
:precondition
(and
(not (Flag79-))
(Flag79prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-50
:parameters ()
:precondition
(and
(not (Flag80-))
(Flag80prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-51
:parameters ()
:precondition
(and
(not (Flag81-))
(Flag81prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-52
:parameters ()
:precondition
(and
(not (Flag82-))
(Flag82prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-53
:parameters ()
:precondition
(and
(not (Flag83-))
(Flag83prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-54
:parameters ()
:precondition
(and
(not (Flag84-))
(Flag84prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-55
:parameters ()
:precondition
(and
(not (Flag85-))
(Flag85prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-56
:parameters ()
:precondition
(and
(not (Flag86-))
(Flag86prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-57
:parameters ()
:precondition
(and
(not (Flag87-))
(Flag87prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-58
:parameters ()
:precondition
(and
(not (Flag88-))
(Flag88prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Prim90Action-59
:parameters ()
:precondition
(and
(not (Flag89-))
(Flag89prime-)
)
:effect
(and
(Flag90prime-)
(not (Flag90-))
)

)
(:action Flag151Action
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
(Flag92-)
(Flag92prime-)
(Flag93-)
(Flag93prime-)
(Flag94-)
(Flag94prime-)
(Flag95-)
(Flag95prime-)
(Flag96-)
(Flag96prime-)
(Flag97-)
(Flag97prime-)
(Flag98-)
(Flag98prime-)
(Flag99-)
(Flag99prime-)
(Flag100-)
(Flag100prime-)
(Flag101-)
(Flag101prime-)
(Flag102-)
(Flag102prime-)
(Flag103-)
(Flag103prime-)
(Flag104-)
(Flag104prime-)
(Flag105-)
(Flag105prime-)
(Flag106-)
(Flag106prime-)
(Flag107-)
(Flag107prime-)
(Flag108-)
(Flag108prime-)
(Flag109-)
(Flag109prime-)
(Flag110-)
(Flag110prime-)
(Flag111-)
(Flag111prime-)
(Flag112-)
(Flag112prime-)
(Flag113-)
(Flag113prime-)
(Flag114-)
(Flag114prime-)
(Flag115-)
(Flag115prime-)
(Flag116-)
(Flag116prime-)
(Flag117-)
(Flag117prime-)
(Flag118-)
(Flag118prime-)
(Flag119-)
(Flag119prime-)
(Flag120-)
(Flag120prime-)
(Flag121-)
(Flag121prime-)
(Flag122-)
(Flag122prime-)
(Flag123-)
(Flag123prime-)
(Flag124-)
(Flag124prime-)
(Flag125-)
(Flag125prime-)
(Flag126-)
(Flag126prime-)
(Flag127-)
(Flag127prime-)
(Flag128-)
(Flag128prime-)
(Flag129-)
(Flag129prime-)
(Flag130-)
(Flag130prime-)
(Flag131-)
(Flag131prime-)
(Flag132-)
(Flag132prime-)
(Flag133-)
(Flag133prime-)
(Flag134-)
(Flag134prime-)
(Flag135-)
(Flag135prime-)
(Flag136-)
(Flag136prime-)
(Flag137-)
(Flag137prime-)
(Flag138-)
(Flag138prime-)
(Flag139-)
(Flag139prime-)
(Flag140-)
(Flag140prime-)
(Flag141-)
(Flag141prime-)
(Flag142-)
(Flag142prime-)
(Flag143-)
(Flag143prime-)
(Flag144-)
(Flag144prime-)
(Flag145-)
(Flag145prime-)
(Flag146-)
(Flag146prime-)
(Flag147-)
(Flag147prime-)
(Flag148-)
(Flag148prime-)
(Flag149-)
(Flag149prime-)
(Flag150-)
(Flag150prime-)
)
:effect
(and
(Flag151-)
(Flag151prime-)
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (Flag91-))
(Flag91prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (Flag92-))
(Flag92prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-2
:parameters ()
:precondition
(and
(not (Flag93-))
(Flag93prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-3
:parameters ()
:precondition
(and
(not (Flag94-))
(Flag94prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-4
:parameters ()
:precondition
(and
(not (Flag95-))
(Flag95prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-5
:parameters ()
:precondition
(and
(not (Flag96-))
(Flag96prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-6
:parameters ()
:precondition
(and
(not (Flag97-))
(Flag97prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-7
:parameters ()
:precondition
(and
(not (Flag98-))
(Flag98prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-8
:parameters ()
:precondition
(and
(not (Flag99-))
(Flag99prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-9
:parameters ()
:precondition
(and
(not (Flag100-))
(Flag100prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-10
:parameters ()
:precondition
(and
(not (Flag101-))
(Flag101prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-11
:parameters ()
:precondition
(and
(not (Flag102-))
(Flag102prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-12
:parameters ()
:precondition
(and
(not (Flag103-))
(Flag103prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-13
:parameters ()
:precondition
(and
(not (Flag104-))
(Flag104prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-14
:parameters ()
:precondition
(and
(not (Flag105-))
(Flag105prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-15
:parameters ()
:precondition
(and
(not (Flag106-))
(Flag106prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-16
:parameters ()
:precondition
(and
(not (Flag107-))
(Flag107prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-17
:parameters ()
:precondition
(and
(not (Flag108-))
(Flag108prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-18
:parameters ()
:precondition
(and
(not (Flag109-))
(Flag109prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-19
:parameters ()
:precondition
(and
(not (Flag110-))
(Flag110prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-20
:parameters ()
:precondition
(and
(not (Flag111-))
(Flag111prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-21
:parameters ()
:precondition
(and
(not (Flag112-))
(Flag112prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-22
:parameters ()
:precondition
(and
(not (Flag113-))
(Flag113prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-23
:parameters ()
:precondition
(and
(not (Flag114-))
(Flag114prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-24
:parameters ()
:precondition
(and
(not (Flag115-))
(Flag115prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-25
:parameters ()
:precondition
(and
(not (Flag116-))
(Flag116prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-26
:parameters ()
:precondition
(and
(not (Flag117-))
(Flag117prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-27
:parameters ()
:precondition
(and
(not (Flag118-))
(Flag118prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-28
:parameters ()
:precondition
(and
(not (Flag119-))
(Flag119prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-29
:parameters ()
:precondition
(and
(not (Flag120-))
(Flag120prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-30
:parameters ()
:precondition
(and
(not (Flag121-))
(Flag121prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-31
:parameters ()
:precondition
(and
(not (Flag122-))
(Flag122prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-32
:parameters ()
:precondition
(and
(not (Flag123-))
(Flag123prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-33
:parameters ()
:precondition
(and
(not (Flag124-))
(Flag124prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-34
:parameters ()
:precondition
(and
(not (Flag125-))
(Flag125prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-35
:parameters ()
:precondition
(and
(not (Flag126-))
(Flag126prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-36
:parameters ()
:precondition
(and
(not (Flag127-))
(Flag127prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-37
:parameters ()
:precondition
(and
(not (Flag128-))
(Flag128prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-38
:parameters ()
:precondition
(and
(not (Flag129-))
(Flag129prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-39
:parameters ()
:precondition
(and
(not (Flag130-))
(Flag130prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-40
:parameters ()
:precondition
(and
(not (Flag131-))
(Flag131prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-41
:parameters ()
:precondition
(and
(not (Flag132-))
(Flag132prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-42
:parameters ()
:precondition
(and
(not (Flag133-))
(Flag133prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-43
:parameters ()
:precondition
(and
(not (Flag134-))
(Flag134prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-44
:parameters ()
:precondition
(and
(not (Flag135-))
(Flag135prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-45
:parameters ()
:precondition
(and
(not (Flag136-))
(Flag136prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-46
:parameters ()
:precondition
(and
(not (Flag137-))
(Flag137prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-47
:parameters ()
:precondition
(and
(not (Flag138-))
(Flag138prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-48
:parameters ()
:precondition
(and
(not (Flag139-))
(Flag139prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-49
:parameters ()
:precondition
(and
(not (Flag140-))
(Flag140prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-50
:parameters ()
:precondition
(and
(not (Flag141-))
(Flag141prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-51
:parameters ()
:precondition
(and
(not (Flag142-))
(Flag142prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-52
:parameters ()
:precondition
(and
(not (Flag143-))
(Flag143prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-53
:parameters ()
:precondition
(and
(not (Flag144-))
(Flag144prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-54
:parameters ()
:precondition
(and
(not (Flag145-))
(Flag145prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-55
:parameters ()
:precondition
(and
(not (Flag146-))
(Flag146prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-56
:parameters ()
:precondition
(and
(not (Flag147-))
(Flag147prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-57
:parameters ()
:precondition
(and
(not (Flag148-))
(Flag148prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-58
:parameters ()
:precondition
(and
(not (Flag149-))
(Flag149prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action Prim151Action-59
:parameters ()
:precondition
(and
(not (Flag150-))
(Flag150prime-)
)
:effect
(and
(Flag151prime-)
(not (Flag151-))
)

)
(:action HIREELECTRONICENG-D
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
(Flag151prime-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-D)
(not (NOT-ELECTRONICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREELECTRONICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag151prime-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-E)
(not (NOT-ELECTRONICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREELECTRONICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag151prime-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-B)
(not (NOT-ELECTRONICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREELECTRONICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag151prime-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-C)
(not (NOT-ELECTRONICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREELECTRONICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag151prime-)
)
:effect
(and
(when
(and
(Flag151-)
)
(and
(ELECTRONICENGINEER-A)
(not (NOT-ELECTRONICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTESTINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTESTINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TESTINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREINFORMATICENG-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-D)
(not (NOT-INFORMATICENGINEER-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREINFORMATICENG-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-E)
(not (NOT-INFORMATICENGINEER-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREINFORMATICENG-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-B)
(not (NOT-INFORMATICENGINEER-B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREINFORMATICENG-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-C)
(not (NOT-INFORMATICENGINEER-C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action HIREINFORMATICENG-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(Flag90prime-)
)
:effect
(and
(when
(and
(Flag90-)
)
(and
(INFORMATICENGINEER-A)
(not (NOT-INFORMATICENGINEER-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSOFTWAREAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SOFTWAREAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SOFTWAREAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNCODINGAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNCODINGAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(CODINGAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CODINGAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTASKAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNTASKAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(TASKAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNMATERIALSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNMATERIALSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(MATERIALSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNSPECIFICATIONSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(SPECIFICATIONSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-SPECIFICATIONSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNDESIGNAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNDESIGNAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(DESIGNAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-D)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action ASSIGNELECTRONICSAGENT-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(ELECTRONICSAGENT-C)
(CHECKCONSISTENCY-)
(not (NOT-ELECTRONICSAGENT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag150prime-))
(not (Flag149prime-))
(not (Flag148prime-))
(not (Flag147prime-))
(not (Flag146prime-))
(not (Flag145prime-))
(not (Flag144prime-))
(not (Flag143prime-))
(not (Flag142prime-))
(not (Flag141prime-))
(not (Flag140prime-))
(not (Flag139prime-))
(not (Flag138prime-))
(not (Flag137prime-))
(not (Flag136prime-))
(not (Flag135prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag131prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag126prime-))
(not (Flag125prime-))
(not (Flag124prime-))
(not (Flag123prime-))
(not (Flag122prime-))
(not (Flag121prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag118prime-))
(not (Flag117prime-))
(not (Flag116prime-))
(not (Flag115prime-))
(not (Flag114prime-))
(not (Flag113prime-))
(not (Flag112prime-))
(not (Flag111prime-))
(not (Flag110prime-))
(not (Flag109prime-))
(not (Flag108prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag105prime-))
(not (Flag104prime-))
(not (Flag103prime-))
(not (Flag102prime-))
(not (Flag101prime-))
(not (Flag100prime-))
(not (Flag99prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag94prime-))
(not (Flag93prime-))
(not (Flag92prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag88prime-))
(not (Flag87prime-))
(not (Flag86prime-))
(not (Flag85prime-))
(not (Flag84prime-))
(not (Flag83prime-))
(not (Flag82prime-))
(not (Flag81prime-))
(not (Flag80prime-))
(not (Flag79prime-))
(not (Flag78prime-))
(not (Flag77prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag74prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag71prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag68prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag61prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag58prime-))
(not (Flag57prime-))
(not (Flag56prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag52prime-))
(not (Flag51prime-))
(not (Flag50prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag47prime-))
(not (Flag46prime-))
(not (Flag45prime-))
(not (Flag44prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag41prime-))
(not (Flag40prime-))
(not (Flag39prime-))
(not (Flag38prime-))
(not (Flag37prime-))
(not (Flag36prime-))
(not (Flag35prime-))
(not (Flag34prime-))
(not (Flag33prime-))
(not (Flag32prime-))
(not (Flag31prime-))
(not (Flag30prime-))
(not (Flag23prime-))
(not (Flag151prime-))
(not (Flag90prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag20prime-))
(not (Flag19prime-))
(not (Flag18prime-))
(not (Flag17prime-))
(not (Flag16prime-))
(not (Flag15prime-))
(not (Flag14prime-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag21prime-))
(not (Flag22prime-))
)
)
(:action Prim22Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Prim22Action-2
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag22prime-)
(not (Flag22-))
)

)
(:action Flag23Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim23Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim23Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag23prime-)
(not (Flag23-))
)

)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag30Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag31Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag31Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag32Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag33Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag33Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag34Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag35Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag35Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag36Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag37Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag37Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag38Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag39Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag39Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag40Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag41Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag41Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Flag42Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag42Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Flag43Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag43Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Flag44Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag44Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag44Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Flag45Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag45Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag45-)
(Flag45prime-)
)

)
(:action Flag46Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag46Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Flag47Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag47Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag47-)
(Flag47prime-)
)

)
(:action Flag48Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag48Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Flag49Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag49Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Flag50Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag50Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Flag51Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag51Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag51-)
(Flag51prime-)
)

)
(:action Flag52Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag52Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Flag53Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag53Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Flag54Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag54Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Flag55Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag55Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Flag56Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag56Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Flag57Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag57Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Flag58Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag58Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Flag59Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag59Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Flag60Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag60Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Flag61Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag61Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Flag62Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag62Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Flag63Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag63Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Flag64Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag64Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Flag65Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag65Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Flag66Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag66Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Flag67Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag67Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Flag68Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag68Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag68-)
(Flag68prime-)
)

)
(:action Flag69Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag69Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Flag70Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag70Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Flag71Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag71Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag71-)
(Flag71prime-)
)

)
(:action Flag72Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag72Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Flag73Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag73Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Flag74Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag74Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Flag75Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag75Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Flag76Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag76Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Flag77Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag77Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Flag78Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag78Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Flag79Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag79Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Flag80Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag80Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag80Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Flag81Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag81Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Flag82Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag82Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Flag83Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag83Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag83Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag83-)
(Flag83prime-)
)

)
(:action Flag84Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag84Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Flag85Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag85Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag85Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag85-)
(Flag85prime-)
)

)
(:action Flag86Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag86Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag86-)
(Flag86prime-)
)

)
(:action Flag87Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag87Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-A)
)
:effect
(and
(Flag87-)
(Flag87prime-)
)

)
(:action Flag88Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag88Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-C)
)
:effect
(and
(Flag88-)
(Flag88prime-)
)

)
(:action Flag89Action-0
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-D)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-1
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-E)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Flag89Action-2
:parameters ()
:precondition
(and
(NOT-INFORMATICENGINEER-B)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Prim30Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag30prime-)
(not (Flag30-))
)

)
(:action Prim31Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim32Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag32prime-)
(not (Flag32-))
)

)
(:action Prim33Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim37Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag40prime-)
(not (Flag40-))
)

)
(:action Prim41Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag45prime-)
(not (Flag45-))
)

)
(:action Prim46Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag47prime-)
(not (Flag47-))
)

)
(:action Prim48Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag51prime-)
(not (Flag51-))
)

)
(:action Prim52Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag68prime-)
(not (Flag68-))
)

)
(:action Prim69Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim71Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag71prime-)
(not (Flag71-))
)

)
(:action Prim72Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-D))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim78Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-A))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-C))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag83prime-)
(not (Flag83-))
)

)
(:action Prim84Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag85prime-)
(not (Flag85-))
)

)
(:action Prim86Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-B))
(not (NOT-INFORMATICENGINEER-E))
)
:effect
(and
(Flag86prime-)
(not (Flag86-))
)

)
(:action Prim87Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-A))
)
:effect
(and
(Flag87prime-)
(not (Flag87-))
)

)
(:action Prim88Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-C))
)
:effect
(and
(Flag88prime-)
(not (Flag88-))
)

)
(:action Prim89Action
:parameters ()
:precondition
(and
(not (NOT-INFORMATICENGINEER-D))
(not (NOT-INFORMATICENGINEER-E))
(not (NOT-INFORMATICENGINEER-B))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Flag91Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag91Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Flag92Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag92Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Flag93Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag93Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Flag94Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag94Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Flag95Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag95Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Flag96Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag96Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Flag97Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag97Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Flag98Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag98Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Flag99Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag99Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Flag100Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag100Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Flag101Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag101Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Flag102Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag102Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Flag103Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag103Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag103-)
(Flag103prime-)
)

)
(:action Flag104Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag104Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Flag105Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag105Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Flag106Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag106Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Flag107Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag107Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Flag108Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag108Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag108Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Flag109Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag109Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Flag110Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag110Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Flag111Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag111Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag111Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Flag112Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag112Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Flag113Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag113Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Flag114Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag114Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag114-)
(Flag114prime-)
)

)
(:action Flag115Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag115Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Flag116Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag116Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag116Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Flag117Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag117Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Flag118Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag118Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Flag119Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag119Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Flag120Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag120Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Flag121Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag121Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag121Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Flag122Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag122Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag122-)
(Flag122prime-)
)

)
(:action Flag123Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag123Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag123-)
(Flag123prime-)
)

)
(:action Flag124Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag124Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag124-)
(Flag124prime-)
)

)
(:action Flag125Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag125Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag125-)
(Flag125prime-)
)

)
(:action Flag126Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag126Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Flag127Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag127Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Flag128Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag128Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Flag129Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag129Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Flag130Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag130Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Flag131Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag131Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag131-)
(Flag131prime-)
)

)
(:action Flag132Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag132Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Flag133Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag133Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Flag134Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag134Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Flag135Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag135Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Flag136Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag136Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Flag137Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag137Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Flag138Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag138Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Flag139Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag139Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Flag140Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag140Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag140-)
(Flag140prime-)
)

)
(:action Flag141Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag141Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag141-)
(Flag141prime-)
)

)
(:action Flag142Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag142Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag142-)
(Flag142prime-)
)

)
(:action Flag143Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag143Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag143-)
(Flag143prime-)
)

)
(:action Flag144Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag144Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag144-)
(Flag144prime-)
)

)
(:action Flag145Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag145Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag145Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag145-)
(Flag145prime-)
)

)
(:action Flag146Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag146Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag146Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag146-)
(Flag146prime-)
)

)
(:action Flag147Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag147Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag147Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag147-)
(Flag147prime-)
)

)
(:action Flag148Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-A)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag148Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag148-)
(Flag148prime-)
)

)
(:action Flag149Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-C)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag149Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag149-)
(Flag149prime-)
)

)
(:action Flag150Action-0
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-B)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag150Action-1
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-D)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Flag150Action-2
:parameters ()
:precondition
(and
(NOT-ELECTRONICENGINEER-E)
)
:effect
(and
(Flag150-)
(Flag150prime-)
)

)
(:action Prim91Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag103prime-)
(not (Flag103-))
)

)
(:action Prim104Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag114prime-)
(not (Flag114-))
)

)
(:action Prim115Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim121Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim122Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag122prime-)
(not (Flag122-))
)

)
(:action Prim123Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag123prime-)
(not (Flag123-))
)

)
(:action Prim124Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag124prime-)
(not (Flag124-))
)

)
(:action Prim125Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag125prime-)
(not (Flag125-))
)

)
(:action Prim126Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag131prime-)
(not (Flag131-))
)

)
(:action Prim132Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim140Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag140prime-)
(not (Flag140-))
)

)
(:action Prim141Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-A))
)
:effect
(and
(Flag141prime-)
(not (Flag141-))
)

)
(:action Prim142Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag142prime-)
(not (Flag142-))
)

)
(:action Prim143Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag143prime-)
(not (Flag143-))
)

)
(:action Prim144Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-C))
)
:effect
(and
(Flag144prime-)
(not (Flag144-))
)

)
(:action Prim145Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag145prime-)
(not (Flag145-))
)

)
(:action Prim146Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag146prime-)
(not (Flag146-))
)

)
(:action Prim147Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-E))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-B))
)
:effect
(and
(Flag147prime-)
(not (Flag147-))
)

)
(:action Prim148Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-A))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag148prime-)
(not (Flag148-))
)

)
(:action Prim149Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-C))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag149prime-)
(not (Flag149-))
)

)
(:action Prim150Action
:parameters ()
:precondition
(and
(not (NOT-ELECTRONICENGINEER-B))
(not (NOT-ELECTRONICENGINEER-D))
(not (NOT-ELECTRONICENGINEER-E))
)
:effect
(and
(Flag150prime-)
(not (Flag150-))
)

)
)
