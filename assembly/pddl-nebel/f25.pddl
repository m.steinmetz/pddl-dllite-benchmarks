(define (problem grounded-SIMPLE-ADL-ASSEM-X-25)
(:domain grounded-SIMPLE-ADL-ASSEMBLY)
(:init
(NOT-REMOVE-ORDER-WHATSIS-57-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-57-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-DOODAD-53-DOODAD-53-WHATSIS-57)
(NOT-REMOVE-ORDER-DOODAD-53-GIMCRACK-54-WHATSIS-57)
(NOT-REMOVE-ORDER-DOODAD-53-SOCKET-55-WHATSIS-57)
(NOT-REMOVE-ORDER-DOODAD-53-WIRE-56-WHATSIS-57)
(NOT-REMOVE-ORDER-GIMCRACK-54-DOODAD-53-WHATSIS-57)
(NOT-REMOVE-ORDER-GIMCRACK-54-GIMCRACK-54-WHATSIS-57)
(NOT-REMOVE-ORDER-GIMCRACK-54-SOCKET-55-WHATSIS-57)
(NOT-REMOVE-ORDER-GIMCRACK-54-WIRE-56-WHATSIS-57)
(NOT-REMOVE-ORDER-SOCKET-55-DOODAD-53-WHATSIS-57)
(NOT-REMOVE-ORDER-SOCKET-55-GIMCRACK-54-WHATSIS-57)
(NOT-REMOVE-ORDER-SOCKET-55-SOCKET-55-WHATSIS-57)
(NOT-REMOVE-ORDER-SOCKET-55-WIRE-56-WHATSIS-57)
(NOT-REMOVE-ORDER-WIRE-56-DOODAD-53-WHATSIS-57)
(NOT-REMOVE-ORDER-WIRE-56-GIMCRACK-54-WHATSIS-57)
(NOT-REMOVE-ORDER-WIRE-56-SOCKET-55-WHATSIS-57)
(NOT-REMOVE-ORDER-WIRE-56-WIRE-56-WHATSIS-57)
(NOT-REMOVE-ORDER-FASTENER-63-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-63-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-BRACKET-59-BRACKET-59-FASTENER-63)
(NOT-REMOVE-ORDER-BRACKET-59-CONNECTOR-60-FASTENER-63)
(NOT-REMOVE-ORDER-BRACKET-59-KLUDGE-61-FASTENER-63)
(NOT-REMOVE-ORDER-BRACKET-59-MOUNT-62-FASTENER-63)
(NOT-REMOVE-ORDER-BRACKET-59-FOOBAR-FASTENER-63)
(NOT-REMOVE-ORDER-CONNECTOR-60-BRACKET-59-FASTENER-63)
(NOT-REMOVE-ORDER-CONNECTOR-60-CONNECTOR-60-FASTENER-63)
(NOT-REMOVE-ORDER-CONNECTOR-60-KLUDGE-61-FASTENER-63)
(NOT-REMOVE-ORDER-CONNECTOR-60-MOUNT-62-FASTENER-63)
(NOT-REMOVE-ORDER-CONNECTOR-60-FOOBAR-FASTENER-63)
(NOT-REMOVE-ORDER-KLUDGE-61-BRACKET-59-FASTENER-63)
(NOT-REMOVE-ORDER-KLUDGE-61-CONNECTOR-60-FASTENER-63)
(NOT-REMOVE-ORDER-KLUDGE-61-KLUDGE-61-FASTENER-63)
(NOT-REMOVE-ORDER-KLUDGE-61-MOUNT-62-FASTENER-63)
(NOT-REMOVE-ORDER-KLUDGE-61-FOOBAR-FASTENER-63)
(NOT-REMOVE-ORDER-MOUNT-62-BRACKET-59-FASTENER-63)
(NOT-REMOVE-ORDER-MOUNT-62-CONNECTOR-60-FASTENER-63)
(NOT-REMOVE-ORDER-MOUNT-62-KLUDGE-61-FASTENER-63)
(NOT-REMOVE-ORDER-MOUNT-62-MOUNT-62-FASTENER-63)
(NOT-REMOVE-ORDER-MOUNT-62-FOOBAR-FASTENER-63)
(NOT-REMOVE-ORDER-WIDGET-22-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-22-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-64-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-COIL-64-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-COIL-64-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-COIL-64-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-COIL-64-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-COIL-64-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-DEVICE-17-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-FROB-18-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-VALVE-20-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-FOOBAR-21-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-TUBE-29-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-TUBE-29-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-CONTRAPTION-25-CONTRAPTION-25-TUBE-29)
(NOT-REMOVE-ORDER-UNIT-23-UNIT-23-CONTRAPTION-25)
(NOT-REMOVE-ORDER-PLUG-24-PLUG-24-UNIT-28)
(NOT-REMOVE-ORDER-PLUG-24-DOODAD-27-UNIT-28)
(NOT-REMOVE-ORDER-UNIT-28-UNIT-28-HACK)
(NOT-REMOVE-ORDER-UNIT-28-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-UNIT-28-WIRE-4-HACK)
(NOT-REMOVE-ORDER-UNIT-28-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-UNIT-28-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-UNIT-28-TUBE-HACK)
(NOT-REMOVE-ORDER-DOODAD-27-PLUG-24-UNIT-28)
(NOT-REMOVE-ORDER-DOODAD-27-DOODAD-27-UNIT-28)
(NOT-REMOVE-ORDER-GIMCRACK-12-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-GIMCRACK-12-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-30-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-WHATSIS-30-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-WHATSIS-30-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-WHATSIS-30-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-WHATSIS-30-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-WHATSIS-30-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-COIL-31-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-FOOBAR-32-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-SPROCKET-9-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-DEVICE-10-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-WHATSIS-30-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-COIL-31-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-FOOBAR-32-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-SPROCKET-9-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-DEVICE-10-GIMCRACK-12)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-11-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-REMOVE-ORDER-CONNECTOR-7-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-7-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-WIDGET-13-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-WIDGET-13-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-WIDGET-13-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-WIDGET-13-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-WIDGET-13-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-WIDGET-13-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-14-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-FROB-15-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-BRACKET-16-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-PLUG-5-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-WIDGET-13-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-HACK-14-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-FROB-15-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-BRACKET-16-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-PLUG-5-CONNECTOR-7)
(NOT-REMOVE-ORDER-SOCKET-6-SOCKET-6-CONNECTOR-7)
(NOT-REMOVE-ORDER-HACK-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-HACK-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-8-UNIT-28-HACK)
(NOT-REMOVE-ORDER-FASTENER-8-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-FASTENER-8-WIRE-4-HACK)
(NOT-REMOVE-ORDER-FASTENER-8-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-FASTENER-8-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-FASTENER-8-TUBE-HACK)
(NOT-REMOVE-ORDER-WIRE-4-UNIT-28-HACK)
(NOT-REMOVE-ORDER-WIRE-4-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-WIRE-4-WIRE-4-HACK)
(NOT-REMOVE-ORDER-WIRE-4-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-WIRE-4-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-WIRE-4-TUBE-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-UNIT-28-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-WIRE-4-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-KLUDGE-1-TUBE-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-UNIT-28-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-WIRE-4-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-MOUNT-2-TUBE-HACK)
(NOT-REMOVE-ORDER-TUBE-UNIT-28-HACK)
(NOT-REMOVE-ORDER-TUBE-FASTENER-8-HACK)
(NOT-REMOVE-ORDER-TUBE-WIRE-4-HACK)
(NOT-REMOVE-ORDER-TUBE-KLUDGE-1-HACK)
(NOT-REMOVE-ORDER-TUBE-MOUNT-2-HACK)
(NOT-REMOVE-ORDER-TUBE-TUBE-HACK)
(NOT-REMOVE-ORDER-VALVE-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-VALVE-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-CONNECTOR-CONNECTOR-VALVE)
(NOT-REMOVE-ORDER-CONNECTOR-DEVICE-VALVE)
(NOT-REMOVE-ORDER-SPROCKET-SPROCKET-CONNECTOR)
(NOT-REMOVE-ORDER-SPROCKET-CONTRAPTION-CONNECTOR)
(NOT-REMOVE-ORDER-CONTRAPTION-SPROCKET-CONNECTOR)
(NOT-REMOVE-ORDER-CONTRAPTION-CONTRAPTION-CONNECTOR)
(NOT-REMOVE-ORDER-DEVICE-CONNECTOR-VALVE)
(NOT-REMOVE-ORDER-DEVICE-DEVICE-VALVE)
(NOT-REMOVE-ORDER-THINGUMBOB-THINGUMBOB-DEVICE)
(NOT-REMOVE-ORDER-THINGUMBOB-WIDGET-DEVICE)
(NOT-REMOVE-ORDER-WIDGET-THINGUMBOB-DEVICE)
(NOT-REMOVE-ORDER-WIDGET-WIDGET-DEVICE)
(NOT-REMOVE-ORDER-WHATSIS-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-WHATSIS-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-FASTENER-FASTENER-WHATSIS)
(NOT-REMOVE-ORDER-FASTENER-PLUG-WHATSIS)
(NOT-REMOVE-ORDER-BRACKET-COIL-64-WIDGET-22)
(NOT-REMOVE-ORDER-BRACKET-DEVICE-17-WIDGET-22)
(NOT-REMOVE-ORDER-BRACKET-FROB-18-WIDGET-22)
(NOT-REMOVE-ORDER-BRACKET-VALVE-20-WIDGET-22)
(NOT-REMOVE-ORDER-BRACKET-FOOBAR-21-WIDGET-22)
(NOT-REMOVE-ORDER-BRACKET-BRACKET-WIDGET-22)
(NOT-REMOVE-ORDER-PLUG-FASTENER-WHATSIS)
(NOT-REMOVE-ORDER-PLUG-PLUG-WHATSIS)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-HOOZAWHATSIE-PLUG)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-UNIT-PLUG)
(NOT-REMOVE-ORDER-HOOZAWHATSIE-WIRE-PLUG)
(NOT-REMOVE-ORDER-UNIT-HOOZAWHATSIE-PLUG)
(NOT-REMOVE-ORDER-UNIT-UNIT-PLUG)
(NOT-REMOVE-ORDER-UNIT-WIRE-PLUG)
(NOT-REMOVE-ORDER-WIRE-HOOZAWHATSIE-PLUG)
(NOT-REMOVE-ORDER-WIRE-UNIT-PLUG)
(NOT-REMOVE-ORDER-WIRE-WIRE-PLUG)
(NOT-REMOVE-ORDER-COIL-WHATSIS-57-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-FASTENER-63-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-WIDGET-22-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-TUBE-29-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-GIMCRACK-12-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-CONNECTOR-7-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-HACK-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-VALVE-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-WHATSIS-GIMCRACK)
(NOT-REMOVE-ORDER-COIL-COIL-GIMCRACK)
(NOT-REMOVE-ORDER-KLUDGE-KLUDGE-COIL)
(NOT-REMOVE-ORDER-KLUDGE-SOCKET-COIL)
(NOT-REMOVE-ORDER-KLUDGE-DOODAD-COIL)
(NOT-REMOVE-ORDER-KLUDGE-MOUNT-COIL)
(NOT-REMOVE-ORDER-KLUDGE-FROB-COIL)
(NOT-REMOVE-ORDER-SOCKET-KLUDGE-COIL)
(NOT-REMOVE-ORDER-SOCKET-SOCKET-COIL)
(NOT-REMOVE-ORDER-SOCKET-DOODAD-COIL)
(NOT-REMOVE-ORDER-SOCKET-MOUNT-COIL)
(NOT-REMOVE-ORDER-SOCKET-FROB-COIL)
(NOT-REMOVE-ORDER-DOODAD-KLUDGE-COIL)
(NOT-REMOVE-ORDER-DOODAD-SOCKET-COIL)
(NOT-REMOVE-ORDER-DOODAD-DOODAD-COIL)
(NOT-REMOVE-ORDER-DOODAD-MOUNT-COIL)
(NOT-REMOVE-ORDER-DOODAD-FROB-COIL)
(NOT-REMOVE-ORDER-MOUNT-KLUDGE-COIL)
(NOT-REMOVE-ORDER-MOUNT-SOCKET-COIL)
(NOT-REMOVE-ORDER-MOUNT-DOODAD-COIL)
(NOT-REMOVE-ORDER-MOUNT-MOUNT-COIL)
(NOT-REMOVE-ORDER-MOUNT-FROB-COIL)
(NOT-REMOVE-ORDER-FROB-KLUDGE-COIL)
(NOT-REMOVE-ORDER-FROB-SOCKET-COIL)
(NOT-REMOVE-ORDER-FROB-DOODAD-COIL)
(NOT-REMOVE-ORDER-FROB-MOUNT-COIL)
(NOT-REMOVE-ORDER-FROB-FROB-COIL)
(NOT-REMOVE-ORDER-FOOBAR-BRACKET-59-FASTENER-63)
(NOT-REMOVE-ORDER-FOOBAR-CONNECTOR-60-FASTENER-63)
(NOT-REMOVE-ORDER-FOOBAR-KLUDGE-61-FASTENER-63)
(NOT-REMOVE-ORDER-FOOBAR-MOUNT-62-FASTENER-63)
(NOT-REMOVE-ORDER-FOOBAR-FOOBAR-FASTENER-63)
(NOT-INCORPORATED-WHATSIS-57-GIMCRACK)
(NOT-INCORPORATED-DOODAD-53-WHATSIS-57)
(NOT-INCORPORATED-GIMCRACK-54-WHATSIS-57)
(NOT-INCORPORATED-SOCKET-55-WHATSIS-57)
(NOT-INCORPORATED-WIRE-56-WHATSIS-57)
(NOT-INCORPORATED-FASTENER-63-GIMCRACK)
(NOT-INCORPORATED-BRACKET-59-FASTENER-63)
(NOT-INCORPORATED-CONNECTOR-60-FASTENER-63)
(NOT-INCORPORATED-KLUDGE-61-FASTENER-63)
(NOT-INCORPORATED-MOUNT-62-FASTENER-63)
(NOT-INCORPORATED-WIDGET-22-GIMCRACK)
(NOT-INCORPORATED-COIL-64-WIDGET-22)
(NOT-INCORPORATED-DEVICE-17-WIDGET-22)
(NOT-INCORPORATED-FROB-18-WIDGET-22)
(NOT-INCORPORATED-VALVE-20-WIDGET-22)
(NOT-INCORPORATED-FOOBAR-21-WIDGET-22)
(NOT-INCORPORATED-TUBE-29-GIMCRACK)
(NOT-INCORPORATED-CONTRAPTION-25-TUBE-29)
(NOT-INCORPORATED-UNIT-23-CONTRAPTION-25)
(NOT-INCORPORATED-PLUG-24-CONTRAPTION-25)
(NOT-INCORPORATED-PLUG-24-UNIT-28)
(NOT-INCORPORATED-UNIT-28-TUBE-29)
(NOT-INCORPORATED-UNIT-28-HACK)
(NOT-INCORPORATED-DOODAD-27-UNIT-28)
(NOT-INCORPORATED-GIMCRACK-12-GIMCRACK)
(NOT-INCORPORATED-WHATSIS-30-GIMCRACK-12)
(NOT-INCORPORATED-COIL-31-GIMCRACK-12)
(NOT-INCORPORATED-FOOBAR-32-GIMCRACK-12)
(NOT-INCORPORATED-SPROCKET-9-GIMCRACK-12)
(NOT-INCORPORATED-DEVICE-10-GIMCRACK-12)
(NOT-INCORPORATED-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-INCORPORATED-CONNECTOR-7-GIMCRACK)
(NOT-INCORPORATED-WIDGET-13-CONNECTOR-7)
(NOT-INCORPORATED-HACK-14-CONNECTOR-7)
(NOT-INCORPORATED-FROB-15-CONNECTOR-7)
(NOT-INCORPORATED-BRACKET-16-CONNECTOR-7)
(NOT-INCORPORATED-PLUG-5-CONNECTOR-7)
(NOT-INCORPORATED-SOCKET-6-CONNECTOR-7)
(NOT-INCORPORATED-HACK-GIMCRACK)
(NOT-INCORPORATED-FASTENER-8-HACK)
(NOT-INCORPORATED-WIRE-4-HACK)
(NOT-INCORPORATED-KLUDGE-1-HACK)
(NOT-INCORPORATED-MOUNT-2-HACK)
(NOT-INCORPORATED-TUBE-HACK)
(NOT-INCORPORATED-VALVE-GIMCRACK)
(NOT-INCORPORATED-CONNECTOR-VALVE)
(NOT-INCORPORATED-SPROCKET-CONNECTOR)
(NOT-INCORPORATED-CONTRAPTION-CONNECTOR)
(NOT-INCORPORATED-DEVICE-VALVE)
(NOT-INCORPORATED-THINGUMBOB-DEVICE)
(NOT-INCORPORATED-WIDGET-DEVICE)
(NOT-INCORPORATED-WHATSIS-GIMCRACK)
(NOT-INCORPORATED-FASTENER-WHATSIS)
(NOT-INCORPORATED-BRACKET-WIDGET-22)
(NOT-INCORPORATED-BRACKET-WHATSIS)
(NOT-INCORPORATED-PLUG-WHATSIS)
(NOT-INCORPORATED-HOOZAWHATSIE-PLUG)
(NOT-INCORPORATED-UNIT-PLUG)
(NOT-INCORPORATED-WIRE-PLUG)
(NOT-INCORPORATED-COIL-GIMCRACK)
(NOT-INCORPORATED-KLUDGE-COIL)
(NOT-INCORPORATED-SOCKET-COIL)
(NOT-INCORPORATED-DOODAD-COIL)
(NOT-INCORPORATED-MOUNT-COIL)
(NOT-INCORPORATED-FROB-COIL)
(NOT-INCORPORATED-FOOBAR-FASTENER-63)
(NOT-INCORPORATED-FOOBAR-COIL)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-57-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-DOODAD-53-DOODAD-53-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-DOODAD-53-GIMCRACK-54-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-DOODAD-53-SOCKET-55-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-DOODAD-53-WIRE-56-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-GIMCRACK-54-DOODAD-53-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-GIMCRACK-54-GIMCRACK-54-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-GIMCRACK-54-SOCKET-55-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-GIMCRACK-54-WIRE-56-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-SOCKET-55-DOODAD-53-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-SOCKET-55-GIMCRACK-54-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-SOCKET-55-SOCKET-55-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-SOCKET-55-WIRE-56-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-WIRE-56-DOODAD-53-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-WIRE-56-GIMCRACK-54-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-WIRE-56-SOCKET-55-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-WIRE-56-WIRE-56-WHATSIS-57)
(NOT-ASSEMBLE-ORDER-FASTENER-63-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-63-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-BRACKET-59-BRACKET-59-FASTENER-63)
(NOT-ASSEMBLE-ORDER-BRACKET-59-CONNECTOR-60-FASTENER-63)
(NOT-ASSEMBLE-ORDER-BRACKET-59-KLUDGE-61-FASTENER-63)
(NOT-ASSEMBLE-ORDER-BRACKET-59-MOUNT-62-FASTENER-63)
(NOT-ASSEMBLE-ORDER-BRACKET-59-FOOBAR-FASTENER-63)
(NOT-ASSEMBLE-ORDER-CONNECTOR-60-BRACKET-59-FASTENER-63)
(NOT-ASSEMBLE-ORDER-CONNECTOR-60-CONNECTOR-60-FASTENER-63)
(NOT-ASSEMBLE-ORDER-CONNECTOR-60-KLUDGE-61-FASTENER-63)
(NOT-ASSEMBLE-ORDER-CONNECTOR-60-MOUNT-62-FASTENER-63)
(NOT-ASSEMBLE-ORDER-CONNECTOR-60-FOOBAR-FASTENER-63)
(NOT-ASSEMBLE-ORDER-KLUDGE-61-BRACKET-59-FASTENER-63)
(NOT-ASSEMBLE-ORDER-KLUDGE-61-CONNECTOR-60-FASTENER-63)
(NOT-ASSEMBLE-ORDER-KLUDGE-61-KLUDGE-61-FASTENER-63)
(NOT-ASSEMBLE-ORDER-KLUDGE-61-MOUNT-62-FASTENER-63)
(NOT-ASSEMBLE-ORDER-KLUDGE-61-FOOBAR-FASTENER-63)
(NOT-ASSEMBLE-ORDER-MOUNT-62-BRACKET-59-FASTENER-63)
(NOT-ASSEMBLE-ORDER-MOUNT-62-CONNECTOR-60-FASTENER-63)
(NOT-ASSEMBLE-ORDER-MOUNT-62-KLUDGE-61-FASTENER-63)
(NOT-ASSEMBLE-ORDER-MOUNT-62-MOUNT-62-FASTENER-63)
(NOT-ASSEMBLE-ORDER-WIDGET-22-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-22-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-64-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-COIL-64-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-COIL-64-FROB-18-WIDGET-22)
(NOT-ASSEMBLE-ORDER-COIL-64-FOOBAR-21-WIDGET-22)
(NOT-ASSEMBLE-ORDER-COIL-64-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-DEVICE-17-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-DEVICE-17-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-DEVICE-17-VALVE-20-WIDGET-22)
(NOT-ASSEMBLE-ORDER-DEVICE-17-FOOBAR-21-WIDGET-22)
(NOT-ASSEMBLE-ORDER-DEVICE-17-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-FROB-18-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-VALVE-20-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-FOOBAR-21-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FROB-18-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-VALVE-20-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-VALVE-20-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-VALVE-20-VALVE-20-WIDGET-22)
(NOT-ASSEMBLE-ORDER-VALVE-20-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-FROB-18-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-VALVE-20-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-FOOBAR-21-WIDGET-22)
(NOT-ASSEMBLE-ORDER-FOOBAR-21-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-TUBE-29-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-TUBE-29-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONTRAPTION-25-CONTRAPTION-25-TUBE-29)
(NOT-ASSEMBLE-ORDER-UNIT-23-UNIT-23-CONTRAPTION-25)
(NOT-ASSEMBLE-ORDER-PLUG-24-PLUG-24-UNIT-28)
(NOT-ASSEMBLE-ORDER-PLUG-24-DOODAD-27-UNIT-28)
(NOT-ASSEMBLE-ORDER-UNIT-28-UNIT-28-HACK)
(NOT-ASSEMBLE-ORDER-UNIT-28-FASTENER-8-HACK)
(NOT-ASSEMBLE-ORDER-UNIT-28-WIRE-4-HACK)
(NOT-ASSEMBLE-ORDER-UNIT-28-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-UNIT-28-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-UNIT-28-TUBE-HACK)
(NOT-ASSEMBLE-ORDER-DOODAD-27-DOODAD-27-UNIT-28)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-GIMCRACK-12-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-30-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-WHATSIS-30-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-WHATSIS-30-FOOBAR-32-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-WHATSIS-30-SPROCKET-9-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-WHATSIS-30-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-FOOBAR-32-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-SPROCKET-9-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-DEVICE-10-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-COIL-31-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-FOOBAR-32-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-SPROCKET-9-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-DEVICE-10-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-FOOBAR-32-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-FOOBAR-32-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-SPROCKET-9-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-DEVICE-10-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-SPROCKET-9-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-DEVICE-10-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-DEVICE-10-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-DEVICE-10-SPROCKET-9-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-DEVICE-10-DEVICE-10-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-DEVICE-10-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-11-WHATSIS-30-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-11-COIL-31-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-11-FOOBAR-32-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-11-DEVICE-10-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-11-HOOZAWHATSIE-11-GIMCRACK-12)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-7-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WIDGET-13-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-WIDGET-13-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-WIDGET-13-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-WIDGET-13-SOCKET-6-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-BRACKET-16-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-PLUG-5-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-14-SOCKET-6-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-BRACKET-16-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-PLUG-5-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-FROB-15-SOCKET-6-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-BRACKET-16-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-PLUG-5-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-BRACKET-16-SOCKET-6-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-PLUG-5-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-PLUG-5-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-PLUG-5-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-PLUG-5-BRACKET-16-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-PLUG-5-PLUG-5-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-WIDGET-13-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-HACK-14-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-FROB-15-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-BRACKET-16-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-PLUG-5-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-SOCKET-6-SOCKET-6-CONNECTOR-7)
(NOT-ASSEMBLE-ORDER-HACK-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-HACK-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-8-UNIT-28-HACK)
(NOT-ASSEMBLE-ORDER-FASTENER-8-FASTENER-8-HACK)
(NOT-ASSEMBLE-ORDER-FASTENER-8-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-FASTENER-8-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-FASTENER-8-TUBE-HACK)
(NOT-ASSEMBLE-ORDER-WIRE-4-FASTENER-8-HACK)
(NOT-ASSEMBLE-ORDER-WIRE-4-WIRE-4-HACK)
(NOT-ASSEMBLE-ORDER-WIRE-4-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-WIRE-4-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-WIRE-4-TUBE-HACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-1-UNIT-28-HACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-1-FASTENER-8-HACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-1-WIRE-4-HACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-1-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-1-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-MOUNT-2-WIRE-4-HACK)
(NOT-ASSEMBLE-ORDER-MOUNT-2-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-MOUNT-2-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-MOUNT-2-TUBE-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-UNIT-28-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-FASTENER-8-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-WIRE-4-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-KLUDGE-1-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-MOUNT-2-HACK)
(NOT-ASSEMBLE-ORDER-TUBE-TUBE-HACK)
(NOT-ASSEMBLE-ORDER-VALVE-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-VALVE-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-CONNECTOR-CONNECTOR-VALVE)
(NOT-ASSEMBLE-ORDER-CONNECTOR-DEVICE-VALVE)
(NOT-ASSEMBLE-ORDER-SPROCKET-SPROCKET-CONNECTOR)
(NOT-ASSEMBLE-ORDER-CONTRAPTION-SPROCKET-CONNECTOR)
(NOT-ASSEMBLE-ORDER-CONTRAPTION-CONTRAPTION-CONNECTOR)
(NOT-ASSEMBLE-ORDER-DEVICE-CONNECTOR-VALVE)
(NOT-ASSEMBLE-ORDER-DEVICE-DEVICE-VALVE)
(NOT-ASSEMBLE-ORDER-THINGUMBOB-THINGUMBOB-DEVICE)
(NOT-ASSEMBLE-ORDER-THINGUMBOB-WIDGET-DEVICE)
(NOT-ASSEMBLE-ORDER-WIDGET-WIDGET-DEVICE)
(NOT-ASSEMBLE-ORDER-WHATSIS-WHATSIS-57-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-TUBE-29-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-GIMCRACK-12-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-WHATSIS-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-FASTENER-FASTENER-WHATSIS)
(NOT-ASSEMBLE-ORDER-FASTENER-PLUG-WHATSIS)
(NOT-ASSEMBLE-ORDER-BRACKET-COIL-64-WIDGET-22)
(NOT-ASSEMBLE-ORDER-BRACKET-DEVICE-17-WIDGET-22)
(NOT-ASSEMBLE-ORDER-BRACKET-FROB-18-WIDGET-22)
(NOT-ASSEMBLE-ORDER-BRACKET-VALVE-20-WIDGET-22)
(NOT-ASSEMBLE-ORDER-BRACKET-FOOBAR-21-WIDGET-22)
(NOT-ASSEMBLE-ORDER-BRACKET-BRACKET-WIDGET-22)
(NOT-ASSEMBLE-ORDER-PLUG-FASTENER-WHATSIS)
(NOT-ASSEMBLE-ORDER-PLUG-PLUG-WHATSIS)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-HOOZAWHATSIE-PLUG)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-UNIT-PLUG)
(NOT-ASSEMBLE-ORDER-HOOZAWHATSIE-WIRE-PLUG)
(NOT-ASSEMBLE-ORDER-UNIT-UNIT-PLUG)
(NOT-ASSEMBLE-ORDER-UNIT-WIRE-PLUG)
(NOT-ASSEMBLE-ORDER-WIRE-HOOZAWHATSIE-PLUG)
(NOT-ASSEMBLE-ORDER-WIRE-WIRE-PLUG)
(NOT-ASSEMBLE-ORDER-COIL-FASTENER-63-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-WIDGET-22-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-CONNECTOR-7-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-HACK-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-VALVE-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-WHATSIS-GIMCRACK)
(NOT-ASSEMBLE-ORDER-COIL-COIL-GIMCRACK)
(NOT-ASSEMBLE-ORDER-KLUDGE-KLUDGE-COIL)
(NOT-ASSEMBLE-ORDER-KLUDGE-SOCKET-COIL)
(NOT-ASSEMBLE-ORDER-KLUDGE-DOODAD-COIL)
(NOT-ASSEMBLE-ORDER-KLUDGE-MOUNT-COIL)
(NOT-ASSEMBLE-ORDER-KLUDGE-FROB-COIL)
(NOT-ASSEMBLE-ORDER-SOCKET-KLUDGE-COIL)
(NOT-ASSEMBLE-ORDER-SOCKET-SOCKET-COIL)
(NOT-ASSEMBLE-ORDER-SOCKET-DOODAD-COIL)
(NOT-ASSEMBLE-ORDER-SOCKET-MOUNT-COIL)
(NOT-ASSEMBLE-ORDER-SOCKET-FROB-COIL)
(NOT-ASSEMBLE-ORDER-DOODAD-KLUDGE-COIL)
(NOT-ASSEMBLE-ORDER-DOODAD-SOCKET-COIL)
(NOT-ASSEMBLE-ORDER-DOODAD-DOODAD-COIL)
(NOT-ASSEMBLE-ORDER-DOODAD-MOUNT-COIL)
(NOT-ASSEMBLE-ORDER-DOODAD-FROB-COIL)
(NOT-ASSEMBLE-ORDER-MOUNT-KLUDGE-COIL)
(NOT-ASSEMBLE-ORDER-MOUNT-SOCKET-COIL)
(NOT-ASSEMBLE-ORDER-MOUNT-DOODAD-COIL)
(NOT-ASSEMBLE-ORDER-MOUNT-MOUNT-COIL)
(NOT-ASSEMBLE-ORDER-MOUNT-FROB-COIL)
(NOT-ASSEMBLE-ORDER-FROB-KLUDGE-COIL)
(NOT-ASSEMBLE-ORDER-FROB-SOCKET-COIL)
(NOT-ASSEMBLE-ORDER-FROB-DOODAD-COIL)
(NOT-ASSEMBLE-ORDER-FROB-MOUNT-COIL)
(NOT-ASSEMBLE-ORDER-FROB-FROB-COIL)
(NOT-ASSEMBLE-ORDER-FOOBAR-BRACKET-59-FASTENER-63)
(NOT-ASSEMBLE-ORDER-FOOBAR-CONNECTOR-60-FASTENER-63)
(NOT-ASSEMBLE-ORDER-FOOBAR-KLUDGE-61-FASTENER-63)
(NOT-ASSEMBLE-ORDER-FOOBAR-MOUNT-62-FASTENER-63)
(NOT-ASSEMBLE-ORDER-FOOBAR-FOOBAR-FASTENER-63)
(REMOVE-ORDER-SOCKET-FOOBAR-COIL)
(ASSEMBLE-ORDER-FOOBAR-SOCKET-COIL)
(ASSEMBLE-ORDER-WIRE-UNIT-PLUG)
(ASSEMBLE-ORDER-UNIT-HOOZAWHATSIE-PLUG)
(ASSEMBLE-ORDER-PLUG-BRACKET-WHATSIS)
(REMOVE-ORDER-FASTENER-BRACKET-WHATSIS)
(ASSEMBLE-ORDER-BRACKET-FASTENER-WHATSIS)
(ASSEMBLE-ORDER-WIDGET-THINGUMBOB-DEVICE)
(ASSEMBLE-ORDER-SPROCKET-CONTRAPTION-CONNECTOR)
(ASSEMBLE-ORDER-MOUNT-2-FASTENER-8-HACK)
(ASSEMBLE-ORDER-MOUNT-2-UNIT-28-HACK)
(ASSEMBLE-ORDER-KLUDGE-1-TUBE-HACK)
(ASSEMBLE-ORDER-WIRE-4-UNIT-28-HACK)
(ASSEMBLE-ORDER-UNIT-28-CONTRAPTION-25-HACK)
(ASSEMBLE-ORDER-FASTENER-8-WIRE-4-HACK)
(ASSEMBLE-ORDER-PLUG-5-SOCKET-6-CONNECTOR-7)
(ASSEMBLE-ORDER-WIDGET-13-PLUG-5-CONNECTOR-7)
(ASSEMBLE-ORDER-WIDGET-13-BRACKET-16-CONNECTOR-7)
(ASSEMBLE-ORDER-HOOZAWHATSIE-11-SPROCKET-9-GIMCRACK-12)
(ASSEMBLE-ORDER-DEVICE-10-FOOBAR-32-GIMCRACK-12)
(ASSEMBLE-ORDER-WHATSIS-30-DEVICE-10-GIMCRACK-12)
(ASSEMBLE-ORDER-DOODAD-27-PLUG-24-UNIT-28)
(ASSEMBLE-ORDER-PLUG-24-UNIT-23-UNIT-28)
(REMOVE-ORDER-UNIT-23-PLUG-24-CONTRAPTION-25)
(ASSEMBLE-ORDER-PLUG-24-UNIT-23-CONTRAPTION-25)
(REMOVE-ORDER-CONTRAPTION-25-UNIT-28-TUBE-29)
(ASSEMBLE-ORDER-UNIT-28-CONTRAPTION-25-TUBE-29)
(ASSEMBLE-ORDER-VALVE-20-FROB-18-WIDGET-22)
(ASSEMBLE-ORDER-VALVE-20-FOOBAR-21-WIDGET-22)
(ASSEMBLE-ORDER-BRACKET-FASTENER-WIDGET-22)
(ASSEMBLE-ORDER-DEVICE-17-FROB-18-WIDGET-22)
(ASSEMBLE-ORDER-COIL-64-VALVE-20-WIDGET-22)
(ASSEMBLE-ORDER-MOUNT-62-FOOBAR-FASTENER-63)
(ASSEMBLE-ORDER-FOOBAR-SOCKET-FASTENER-63)
(ASSEMBLE-ORDER-COIL-TUBE-29-GIMCRACK)
(ASSEMBLE-ORDER-COIL-WHATSIS-57-GIMCRACK)
(ASSEMBLE-ORDER-COIL-GIMCRACK-12-GIMCRACK)
(ASSEMBLE-ORDER-GIMCRACK-12-VALVE-GIMCRACK)
(ASSEMBLE-ORDER-GIMCRACK-12-HACK-GIMCRACK)
(ASSEMBLE-ORDER-WIDGET-22-WHATSIS-57-GIMCRACK)
(ASSEMBLE-ORDER-WHATSIS-57-TUBE-29-GIMCRACK)
(AVAILABLE-SCALPEL)
(AVAILABLE-HAMMER)
(AVAILABLE-FOOBAR)
(AVAILABLE-FROB)
(AVAILABLE-MOUNT)
(AVAILABLE-DOODAD)
(AVAILABLE-SOCKET)
(AVAILABLE-KLUDGE)
(AVAILABLE-WIRE)
(AVAILABLE-UNIT)
(AVAILABLE-HOOZAWHATSIE)
(AVAILABLE-BRACKET)
(AVAILABLE-FASTENER)
(AVAILABLE-WIDGET)
(AVAILABLE-THINGUMBOB)
(AVAILABLE-CONTRAPTION)
(AVAILABLE-SPROCKET)
(AVAILABLE-TUBE)
(AVAILABLE-MOUNT-2)
(AVAILABLE-KLUDGE-1)
(AVAILABLE-WIRE-4)
(AVAILABLE-FASTENER-8)
(AVAILABLE-SOCKET-6)
(AVAILABLE-PLUG-5)
(AVAILABLE-BRACKET-16)
(AVAILABLE-FROB-15)
(AVAILABLE-HACK-14)
(AVAILABLE-WIDGET-13)
(AVAILABLE-HOOZAWHATSIE-11)
(AVAILABLE-DEVICE-10)
(AVAILABLE-SPROCKET-9)
(AVAILABLE-FOOBAR-32)
(AVAILABLE-COIL-31)
(AVAILABLE-WHATSIS-30)
(AVAILABLE-DOODAD-27)
(AVAILABLE-PLUG-24)
(AVAILABLE-UNIT-23)
(AVAILABLE-FOOBAR-21)
(AVAILABLE-VALVE-20)
(AVAILABLE-FROB-18)
(AVAILABLE-DEVICE-17)
(AVAILABLE-COIL-64)
(AVAILABLE-MOUNT-62)
(AVAILABLE-KLUDGE-61)
(AVAILABLE-CONNECTOR-60)
(AVAILABLE-BRACKET-59)
(AVAILABLE-WIRE-56)
(AVAILABLE-SOCKET-55)
(AVAILABLE-GIMCRACK-54)
(AVAILABLE-DOODAD-53)
)
(:goal
(and
(COMPLETE-GIMCRACK)
)
)
)
