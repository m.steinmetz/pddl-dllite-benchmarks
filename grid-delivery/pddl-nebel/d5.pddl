(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(HOLDING-O6)
(HOLDING-O3)
(Flag1-)
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(HOLDING-O9)
(HOLDING-O8)
(HOLDING-O7)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O9-P0-P0)
(OBJ-AT-O9-P0-P1)
(OBJ-AT-O9-P0-P2)
(OBJ-AT-O9-P0-P3)
(OBJ-AT-O9-P0-P4)
(OBJ-AT-O9-P0-P5)
(OBJ-AT-O9-P1-P0)
(OBJ-AT-O9-P1-P1)
(OBJ-AT-O9-P1-P2)
(OBJ-AT-O9-P1-P3)
(OBJ-AT-O9-P1-P4)
(OBJ-AT-O9-P1-P5)
(OBJ-AT-O9-P2-P0)
(OBJ-AT-O9-P2-P1)
(OBJ-AT-O9-P2-P2)
(OBJ-AT-O9-P2-P4)
(OBJ-AT-O9-P2-P5)
(OBJ-AT-O9-P3-P0)
(OBJ-AT-O9-P3-P1)
(OBJ-AT-O9-P3-P2)
(OBJ-AT-O9-P3-P3)
(OBJ-AT-O9-P3-P4)
(OBJ-AT-O9-P3-P5)
(OBJ-AT-O9-P4-P0)
(OBJ-AT-O9-P4-P1)
(OBJ-AT-O9-P4-P2)
(OBJ-AT-O9-P4-P3)
(OBJ-AT-O9-P4-P4)
(OBJ-AT-O9-P4-P5)
(OBJ-AT-O9-P5-P0)
(OBJ-AT-O9-P5-P1)
(OBJ-AT-O9-P5-P2)
(OBJ-AT-O9-P5-P3)
(OBJ-AT-O9-P5-P4)
(OBJ-AT-O9-P5-P5)
(OBJ-AT-O8-P0-P0)
(OBJ-AT-O8-P0-P1)
(OBJ-AT-O8-P0-P2)
(OBJ-AT-O8-P0-P3)
(OBJ-AT-O8-P0-P4)
(OBJ-AT-O8-P0-P5)
(OBJ-AT-O8-P1-P1)
(OBJ-AT-O8-P1-P2)
(OBJ-AT-O8-P1-P3)
(OBJ-AT-O8-P1-P4)
(OBJ-AT-O8-P1-P5)
(OBJ-AT-O8-P2-P0)
(OBJ-AT-O8-P2-P1)
(OBJ-AT-O8-P2-P2)
(OBJ-AT-O8-P2-P3)
(OBJ-AT-O8-P2-P4)
(OBJ-AT-O8-P2-P5)
(OBJ-AT-O8-P3-P0)
(OBJ-AT-O8-P3-P1)
(OBJ-AT-O8-P3-P2)
(OBJ-AT-O8-P3-P3)
(OBJ-AT-O8-P3-P4)
(OBJ-AT-O8-P3-P5)
(OBJ-AT-O8-P4-P0)
(OBJ-AT-O8-P4-P1)
(OBJ-AT-O8-P4-P2)
(OBJ-AT-O8-P4-P3)
(OBJ-AT-O8-P4-P4)
(OBJ-AT-O8-P4-P5)
(OBJ-AT-O8-P5-P0)
(OBJ-AT-O8-P5-P1)
(OBJ-AT-O8-P5-P2)
(OBJ-AT-O8-P5-P3)
(OBJ-AT-O8-P5-P4)
(OBJ-AT-O8-P5-P5)
(OBJ-AT-O7-P0-P0)
(OBJ-AT-O7-P0-P1)
(OBJ-AT-O7-P0-P2)
(OBJ-AT-O7-P0-P3)
(OBJ-AT-O7-P0-P4)
(OBJ-AT-O7-P0-P5)
(OBJ-AT-O7-P1-P0)
(OBJ-AT-O7-P1-P1)
(OBJ-AT-O7-P1-P2)
(OBJ-AT-O7-P1-P3)
(OBJ-AT-O7-P1-P4)
(OBJ-AT-O7-P1-P5)
(OBJ-AT-O7-P2-P0)
(OBJ-AT-O7-P2-P1)
(OBJ-AT-O7-P2-P2)
(OBJ-AT-O7-P2-P3)
(OBJ-AT-O7-P2-P4)
(OBJ-AT-O7-P2-P5)
(OBJ-AT-O7-P3-P0)
(OBJ-AT-O7-P3-P1)
(OBJ-AT-O7-P3-P2)
(OBJ-AT-O7-P3-P3)
(OBJ-AT-O7-P3-P4)
(OBJ-AT-O7-P3-P5)
(OBJ-AT-O7-P4-P0)
(OBJ-AT-O7-P4-P1)
(OBJ-AT-O7-P4-P2)
(OBJ-AT-O7-P4-P3)
(OBJ-AT-O7-P4-P4)
(OBJ-AT-O7-P4-P5)
(OBJ-AT-O7-P5-P0)
(OBJ-AT-O7-P5-P2)
(OBJ-AT-O7-P5-P3)
(OBJ-AT-O7-P5-P4)
(OBJ-AT-O7-P5-P5)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag10-)
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O7-P5-P1)
(OBJ-AT-O8-P1-P0)
(OBJ-AT-O9-P2-P3)
(ROBOT-AT-P0-P0)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O6-P0-P0)
(Flag1prime-)
(Flag9prime-)
(Flag8prime-)
(Flag7prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag10prime-)
)
(:action Flag10Action
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
(Flag2-)
(Flag2prime-)
(Flag3-)
(Flag3prime-)
(Flag4-)
(Flag4prime-)
(Flag5-)
(Flag5prime-)
(Flag6-)
(Flag6prime-)
(Flag7-)
(Flag7prime-)
(Flag8-)
(Flag8prime-)
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action-1
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-3
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-4
:parameters ()
:precondition
(and
(not (Flag5-))
(Flag5prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-5
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-6
:parameters ()
:precondition
(and
(not (Flag7-))
(Flag7prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-7
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-8
:parameters ()
:precondition
(and
(not (Flag9-))
(Flag9prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action Flag1Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag1Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag2Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P4-P2)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag3Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P1-P2)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag4Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P0-P1)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag5Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P2-P2)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag6Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P4-P4)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag7Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag7Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P2-P0)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag8Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P0-P3)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag9Action-0
:parameters ()
:precondition
(and
(OBJ-AT-O0-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-1
:parameters ()
:precondition
(and
(OBJ-AT-O1-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-2
:parameters ()
:precondition
(and
(OBJ-AT-O2-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-3
:parameters ()
:precondition
(and
(OBJ-AT-O3-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-4
:parameters ()
:precondition
(and
(OBJ-AT-O4-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-5
:parameters ()
:precondition
(and
(OBJ-AT-O5-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-6
:parameters ()
:precondition
(and
(OBJ-AT-O6-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-8
:parameters ()
:precondition
(and
(OBJ-AT-O8-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag9Action-9
:parameters ()
:precondition
(and
(OBJ-AT-O9-P0-P2)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim1Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P5-P1))
(not (OBJ-AT-O1-P5-P1))
(not (OBJ-AT-O2-P5-P1))
(not (OBJ-AT-O3-P5-P1))
(not (OBJ-AT-O4-P5-P1))
(not (OBJ-AT-O5-P5-P1))
(not (OBJ-AT-O6-P5-P1))
(not (OBJ-AT-O7-P5-P1))
(not (OBJ-AT-O8-P5-P1))
(not (OBJ-AT-O9-P5-P1))
)
:effect
(and
(Flag1prime-)
(not (Flag1-))
)

)
(:action Prim2Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P4-P2))
(not (OBJ-AT-O1-P4-P2))
(not (OBJ-AT-O2-P4-P2))
(not (OBJ-AT-O3-P4-P2))
(not (OBJ-AT-O4-P4-P2))
(not (OBJ-AT-O5-P4-P2))
(not (OBJ-AT-O6-P4-P2))
(not (OBJ-AT-O7-P4-P2))
(not (OBJ-AT-O8-P4-P2))
(not (OBJ-AT-O9-P4-P2))
)
:effect
(and
(Flag2prime-)
(not (Flag2-))
)

)
(:action Prim3Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P1-P2))
(not (OBJ-AT-O1-P1-P2))
(not (OBJ-AT-O2-P1-P2))
(not (OBJ-AT-O3-P1-P2))
(not (OBJ-AT-O4-P1-P2))
(not (OBJ-AT-O5-P1-P2))
(not (OBJ-AT-O6-P1-P2))
(not (OBJ-AT-O7-P1-P2))
(not (OBJ-AT-O8-P1-P2))
(not (OBJ-AT-O9-P1-P2))
)
:effect
(and
(Flag3prime-)
(not (Flag3-))
)

)
(:action Prim4Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P0-P1))
(not (OBJ-AT-O1-P0-P1))
(not (OBJ-AT-O2-P0-P1))
(not (OBJ-AT-O3-P0-P1))
(not (OBJ-AT-O4-P0-P1))
(not (OBJ-AT-O5-P0-P1))
(not (OBJ-AT-O6-P0-P1))
(not (OBJ-AT-O7-P0-P1))
(not (OBJ-AT-O8-P0-P1))
(not (OBJ-AT-O9-P0-P1))
)
:effect
(and
(Flag4prime-)
(not (Flag4-))
)

)
(:action Prim5Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P2-P2))
(not (OBJ-AT-O1-P2-P2))
(not (OBJ-AT-O2-P2-P2))
(not (OBJ-AT-O3-P2-P2))
(not (OBJ-AT-O4-P2-P2))
(not (OBJ-AT-O5-P2-P2))
(not (OBJ-AT-O6-P2-P2))
(not (OBJ-AT-O7-P2-P2))
(not (OBJ-AT-O8-P2-P2))
(not (OBJ-AT-O9-P2-P2))
)
:effect
(and
(Flag5prime-)
(not (Flag5-))
)

)
(:action Prim6Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P4-P4))
(not (OBJ-AT-O1-P4-P4))
(not (OBJ-AT-O2-P4-P4))
(not (OBJ-AT-O3-P4-P4))
(not (OBJ-AT-O4-P4-P4))
(not (OBJ-AT-O5-P4-P4))
(not (OBJ-AT-O6-P4-P4))
(not (OBJ-AT-O7-P4-P4))
(not (OBJ-AT-O8-P4-P4))
(not (OBJ-AT-O9-P4-P4))
)
:effect
(and
(Flag6prime-)
(not (Flag6-))
)

)
(:action Prim7Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P2-P0))
(not (OBJ-AT-O1-P2-P0))
(not (OBJ-AT-O2-P2-P0))
(not (OBJ-AT-O3-P2-P0))
(not (OBJ-AT-O4-P2-P0))
(not (OBJ-AT-O5-P2-P0))
(not (OBJ-AT-O6-P2-P0))
(not (OBJ-AT-O7-P2-P0))
(not (OBJ-AT-O8-P2-P0))
(not (OBJ-AT-O9-P2-P0))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim8Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P0-P3))
(not (OBJ-AT-O1-P0-P3))
(not (OBJ-AT-O2-P0-P3))
(not (OBJ-AT-O3-P0-P3))
(not (OBJ-AT-O4-P0-P3))
(not (OBJ-AT-O5-P0-P3))
(not (OBJ-AT-O6-P0-P3))
(not (OBJ-AT-O7-P0-P3))
(not (OBJ-AT-O8-P0-P3))
(not (OBJ-AT-O9-P0-P3))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim9Action
:parameters ()
:precondition
(and
(not (OBJ-AT-O0-P0-P2))
(not (OBJ-AT-O1-P0-P2))
(not (OBJ-AT-O2-P0-P2))
(not (OBJ-AT-O3-P0-P2))
(not (OBJ-AT-O4-P0-P2))
(not (OBJ-AT-O5-P0-P2))
(not (OBJ-AT-O6-P0-P2))
(not (OBJ-AT-O7-P0-P2))
(not (OBJ-AT-O8-P0-P2))
(not (OBJ-AT-O9-P0-P2))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O7-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O7-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O7-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O7-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O7-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O7-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O7-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O7-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O7-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O7-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O7-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O7-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O7-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O7-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O7-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O7-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O7-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O7-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O7-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O7-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O7-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O7-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O7-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O7-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O7-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O7-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O7-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O7-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O7-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O7-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O7-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O7-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O7-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O7-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O7-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O7-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O8-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O8-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O8-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O8-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O8-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O8-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O8-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O8-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O8-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O8-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O8-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O8-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O8-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O8-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O8-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O8-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O8-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O8-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O8-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O8-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O8-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O8-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O8-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O8-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O8-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O8-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O8-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O8-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O8-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O8-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O8-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O8-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O8-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O8-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O8-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O8-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O9-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O9-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O9-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O9-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O9-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O9-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O9-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O9-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O9-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O9-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O9-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O9-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O9-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O9-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O9-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O9-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O9-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O9-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O9-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O9-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O9-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O9-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O9-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O9-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O9-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O9-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O9-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O9-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O9-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O9-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O9-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O9-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O9-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O9-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O9-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O9-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action Flag1Action-7
:parameters ()
:precondition
(and
(OBJ-AT-O7-P5-P1)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
(not (Flag1prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag7prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag10prime-))
)
)
)
