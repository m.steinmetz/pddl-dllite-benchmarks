(define (domain grid)

(:types pos obj num - object)

(:predicates
    (holding ?o - obj)
    (robot-at ?x ?y - pos)
    (obj-at ?o - obj ?x ?y - pos)
    (adjacent ?x ?y - pos)
    (sum ?c0 ?c1 ?c2 - num)
    (weight ?o - obj ?c - num)
    (capacity ?c - num))

(:action move
    :parameters (?xcur ?ycur ?xnext ?ynext - pos)
    :precondition (and
        (robot-at ?xcur ?ycur)
        (or
            (and (= ?ycur ?ynext) (adjacent ?xcur ?xnext))
            (and (= ?xcur ?xnext) (adjacent ?ycur ?ynext))))
    :effect (and (not (robot-at ?xcur ?ycur)) (robot-at ?xnext ?ynext)))

(:action pickup
    :parameters (?o - obj ?x ?y - pos ?c0 ?c1 ?c2 - num)
    :precondition (and (robot-at ?x ?y) (obj-at ?o ?x ?y) (capacity ?c0) (weight ?o ?c1) (sum ?c1 ?c2 ?c0))
    :effect (and (not (obj-at ?o ?x ?y)) (holding ?o) (not (capacity ?c0)) (capacity ?c2)))

(:action drop
    :parameters (?o - obj ?x ?y - pos ?c0 ?c1 ?c2 - num)
    :precondition (and (robot-at ?x ?y) (holding ?o) (capacity ?c0) (weight ?o ?c1) (sum ?c0 ?c1 ?c2))
    :effect (and (obj-at ?o ?x ?y) (not (holding ?o)) (not (capacity ?c0)) (capacity ?c2)))

)
