(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(Flag10-)
(Flag1-)
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P0-P6)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P0-P7)
(ROBOT-AT-P1-P6)
(ROBOT-AT-P1-P7)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P2-P6)
(ROBOT-AT-P2-P7)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P3-P6)
(ROBOT-AT-P3-P7)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P4-P6)
(ROBOT-AT-P4-P7)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(ROBOT-AT-P5-P6)
(ROBOT-AT-P5-P7)
(ROBOT-AT-P6-P0)
(ROBOT-AT-P6-P1)
(ROBOT-AT-P6-P2)
(ROBOT-AT-P6-P3)
(ROBOT-AT-P6-P4)
(ROBOT-AT-P6-P5)
(ROBOT-AT-P6-P6)
(ROBOT-AT-P6-P7)
(ROBOT-AT-P7-P0)
(ROBOT-AT-P7-P1)
(ROBOT-AT-P7-P2)
(ROBOT-AT-P7-P3)
(ROBOT-AT-P7-P4)
(ROBOT-AT-P7-P5)
(ROBOT-AT-P7-P6)
(ROBOT-AT-P7-P7)
(HOLDING-O9)
(HOLDING-O8)
(HOLDING-O7)
(HOLDING-O6)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O3)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O9-P0-P0)
(OBJ-AT-O9-P0-P1)
(OBJ-AT-O9-P0-P2)
(OBJ-AT-O9-P0-P3)
(OBJ-AT-O9-P0-P4)
(OBJ-AT-O9-P0-P5)
(OBJ-AT-O9-P0-P6)
(OBJ-AT-O9-P0-P7)
(OBJ-AT-O9-P1-P0)
(OBJ-AT-O9-P1-P1)
(OBJ-AT-O9-P1-P2)
(OBJ-AT-O9-P1-P3)
(OBJ-AT-O9-P1-P4)
(OBJ-AT-O9-P1-P5)
(OBJ-AT-O9-P1-P6)
(OBJ-AT-O9-P1-P7)
(OBJ-AT-O9-P2-P0)
(OBJ-AT-O9-P2-P1)
(OBJ-AT-O9-P2-P2)
(OBJ-AT-O9-P2-P3)
(OBJ-AT-O9-P2-P4)
(OBJ-AT-O9-P2-P5)
(OBJ-AT-O9-P2-P6)
(OBJ-AT-O9-P2-P7)
(OBJ-AT-O9-P3-P0)
(OBJ-AT-O9-P3-P1)
(OBJ-AT-O9-P3-P2)
(OBJ-AT-O9-P3-P3)
(OBJ-AT-O9-P3-P4)
(OBJ-AT-O9-P3-P5)
(OBJ-AT-O9-P3-P6)
(OBJ-AT-O9-P3-P7)
(OBJ-AT-O9-P4-P0)
(OBJ-AT-O9-P4-P1)
(OBJ-AT-O9-P4-P2)
(OBJ-AT-O9-P4-P3)
(OBJ-AT-O9-P4-P4)
(OBJ-AT-O9-P4-P5)
(OBJ-AT-O9-P4-P6)
(OBJ-AT-O9-P4-P7)
(OBJ-AT-O9-P5-P0)
(OBJ-AT-O9-P5-P1)
(OBJ-AT-O9-P5-P2)
(OBJ-AT-O9-P5-P3)
(OBJ-AT-O9-P5-P4)
(OBJ-AT-O9-P5-P5)
(OBJ-AT-O9-P5-P6)
(OBJ-AT-O9-P6-P0)
(OBJ-AT-O9-P6-P1)
(OBJ-AT-O9-P6-P2)
(OBJ-AT-O9-P6-P3)
(OBJ-AT-O9-P6-P4)
(OBJ-AT-O9-P6-P5)
(OBJ-AT-O9-P6-P6)
(OBJ-AT-O9-P6-P7)
(OBJ-AT-O9-P7-P0)
(OBJ-AT-O9-P7-P1)
(OBJ-AT-O9-P7-P2)
(OBJ-AT-O9-P7-P3)
(OBJ-AT-O9-P7-P4)
(OBJ-AT-O9-P7-P5)
(OBJ-AT-O9-P7-P6)
(OBJ-AT-O9-P7-P7)
(OBJ-AT-O8-P0-P0)
(OBJ-AT-O8-P0-P1)
(OBJ-AT-O8-P0-P2)
(OBJ-AT-O8-P0-P3)
(OBJ-AT-O8-P0-P5)
(OBJ-AT-O8-P0-P6)
(OBJ-AT-O8-P0-P7)
(OBJ-AT-O8-P1-P0)
(OBJ-AT-O8-P1-P1)
(OBJ-AT-O8-P1-P2)
(OBJ-AT-O8-P1-P3)
(OBJ-AT-O8-P1-P4)
(OBJ-AT-O8-P1-P5)
(OBJ-AT-O8-P1-P6)
(OBJ-AT-O8-P1-P7)
(OBJ-AT-O8-P2-P0)
(OBJ-AT-O8-P2-P1)
(OBJ-AT-O8-P2-P2)
(OBJ-AT-O8-P2-P3)
(OBJ-AT-O8-P2-P4)
(OBJ-AT-O8-P2-P5)
(OBJ-AT-O8-P2-P6)
(OBJ-AT-O8-P2-P7)
(OBJ-AT-O8-P3-P0)
(OBJ-AT-O8-P3-P1)
(OBJ-AT-O8-P3-P2)
(OBJ-AT-O8-P3-P3)
(OBJ-AT-O8-P3-P4)
(OBJ-AT-O8-P3-P5)
(OBJ-AT-O8-P3-P6)
(OBJ-AT-O8-P3-P7)
(OBJ-AT-O8-P4-P0)
(OBJ-AT-O8-P4-P1)
(OBJ-AT-O8-P4-P2)
(OBJ-AT-O8-P4-P3)
(OBJ-AT-O8-P4-P4)
(OBJ-AT-O8-P4-P5)
(OBJ-AT-O8-P4-P6)
(OBJ-AT-O8-P4-P7)
(OBJ-AT-O8-P5-P0)
(OBJ-AT-O8-P5-P1)
(OBJ-AT-O8-P5-P2)
(OBJ-AT-O8-P5-P3)
(OBJ-AT-O8-P5-P4)
(OBJ-AT-O8-P5-P5)
(OBJ-AT-O8-P5-P6)
(OBJ-AT-O8-P5-P7)
(OBJ-AT-O8-P6-P0)
(OBJ-AT-O8-P6-P1)
(OBJ-AT-O8-P6-P2)
(OBJ-AT-O8-P6-P3)
(OBJ-AT-O8-P6-P4)
(OBJ-AT-O8-P6-P5)
(OBJ-AT-O8-P6-P6)
(OBJ-AT-O8-P6-P7)
(OBJ-AT-O8-P7-P0)
(OBJ-AT-O8-P7-P1)
(OBJ-AT-O8-P7-P2)
(OBJ-AT-O8-P7-P3)
(OBJ-AT-O8-P7-P4)
(OBJ-AT-O8-P7-P5)
(OBJ-AT-O8-P7-P6)
(OBJ-AT-O8-P7-P7)
(OBJ-AT-O7-P0-P0)
(OBJ-AT-O7-P0-P1)
(OBJ-AT-O7-P0-P2)
(OBJ-AT-O7-P0-P3)
(OBJ-AT-O7-P0-P4)
(OBJ-AT-O7-P0-P5)
(OBJ-AT-O7-P0-P6)
(OBJ-AT-O7-P0-P7)
(OBJ-AT-O7-P1-P0)
(OBJ-AT-O7-P1-P1)
(OBJ-AT-O7-P1-P2)
(OBJ-AT-O7-P1-P3)
(OBJ-AT-O7-P1-P4)
(OBJ-AT-O7-P1-P5)
(OBJ-AT-O7-P1-P6)
(OBJ-AT-O7-P1-P7)
(OBJ-AT-O7-P2-P0)
(OBJ-AT-O7-P2-P1)
(OBJ-AT-O7-P2-P2)
(OBJ-AT-O7-P2-P3)
(OBJ-AT-O7-P2-P4)
(OBJ-AT-O7-P2-P5)
(OBJ-AT-O7-P2-P6)
(OBJ-AT-O7-P2-P7)
(OBJ-AT-O7-P3-P0)
(OBJ-AT-O7-P3-P1)
(OBJ-AT-O7-P3-P2)
(OBJ-AT-O7-P3-P3)
(OBJ-AT-O7-P3-P4)
(OBJ-AT-O7-P3-P5)
(OBJ-AT-O7-P3-P6)
(OBJ-AT-O7-P3-P7)
(OBJ-AT-O7-P4-P0)
(OBJ-AT-O7-P4-P1)
(OBJ-AT-O7-P4-P2)
(OBJ-AT-O7-P4-P3)
(OBJ-AT-O7-P4-P4)
(OBJ-AT-O7-P4-P5)
(OBJ-AT-O7-P4-P6)
(OBJ-AT-O7-P5-P0)
(OBJ-AT-O7-P5-P1)
(OBJ-AT-O7-P5-P2)
(OBJ-AT-O7-P5-P3)
(OBJ-AT-O7-P5-P4)
(OBJ-AT-O7-P5-P5)
(OBJ-AT-O7-P5-P6)
(OBJ-AT-O7-P5-P7)
(OBJ-AT-O7-P6-P0)
(OBJ-AT-O7-P6-P1)
(OBJ-AT-O7-P6-P2)
(OBJ-AT-O7-P6-P3)
(OBJ-AT-O7-P6-P4)
(OBJ-AT-O7-P6-P5)
(OBJ-AT-O7-P6-P6)
(OBJ-AT-O7-P6-P7)
(OBJ-AT-O7-P7-P0)
(OBJ-AT-O7-P7-P1)
(OBJ-AT-O7-P7-P2)
(OBJ-AT-O7-P7-P3)
(OBJ-AT-O7-P7-P4)
(OBJ-AT-O7-P7-P5)
(OBJ-AT-O7-P7-P6)
(OBJ-AT-O7-P7-P7)
(OBJ-AT-O6-P0-P0)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P0-P6)
(OBJ-AT-O6-P0-P7)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P1-P6)
(OBJ-AT-O6-P1-P7)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P2-P6)
(OBJ-AT-O6-P2-P7)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P3-P6)
(OBJ-AT-O6-P3-P7)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P4-P6)
(OBJ-AT-O6-P4-P7)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O6-P5-P6)
(OBJ-AT-O6-P5-P7)
(OBJ-AT-O6-P6-P0)
(OBJ-AT-O6-P6-P1)
(OBJ-AT-O6-P6-P2)
(OBJ-AT-O6-P6-P3)
(OBJ-AT-O6-P6-P4)
(OBJ-AT-O6-P6-P5)
(OBJ-AT-O6-P6-P6)
(OBJ-AT-O6-P7-P0)
(OBJ-AT-O6-P7-P1)
(OBJ-AT-O6-P7-P2)
(OBJ-AT-O6-P7-P3)
(OBJ-AT-O6-P7-P4)
(OBJ-AT-O6-P7-P5)
(OBJ-AT-O6-P7-P6)
(OBJ-AT-O6-P7-P7)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O5-P0-P6)
(OBJ-AT-O5-P0-P7)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P1-P6)
(OBJ-AT-O5-P1-P7)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P2-P6)
(OBJ-AT-O5-P2-P7)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P3-P6)
(OBJ-AT-O5-P3-P7)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P4-P6)
(OBJ-AT-O5-P4-P7)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O5-P5-P6)
(OBJ-AT-O5-P5-P7)
(OBJ-AT-O5-P6-P0)
(OBJ-AT-O5-P6-P1)
(OBJ-AT-O5-P6-P2)
(OBJ-AT-O5-P6-P3)
(OBJ-AT-O5-P6-P4)
(OBJ-AT-O5-P6-P5)
(OBJ-AT-O5-P6-P6)
(OBJ-AT-O5-P6-P7)
(OBJ-AT-O5-P7-P0)
(OBJ-AT-O5-P7-P1)
(OBJ-AT-O5-P7-P2)
(OBJ-AT-O5-P7-P3)
(OBJ-AT-O5-P7-P4)
(OBJ-AT-O5-P7-P5)
(OBJ-AT-O5-P7-P6)
(OBJ-AT-O5-P7-P7)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O4-P0-P6)
(OBJ-AT-O4-P0-P7)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P1-P6)
(OBJ-AT-O4-P1-P7)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O4-P2-P6)
(OBJ-AT-O4-P2-P7)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P3-P6)
(OBJ-AT-O4-P3-P7)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P4-P6)
(OBJ-AT-O4-P4-P7)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O4-P5-P6)
(OBJ-AT-O4-P5-P7)
(OBJ-AT-O4-P6-P0)
(OBJ-AT-O4-P6-P1)
(OBJ-AT-O4-P6-P2)
(OBJ-AT-O4-P6-P3)
(OBJ-AT-O4-P6-P4)
(OBJ-AT-O4-P6-P5)
(OBJ-AT-O4-P6-P6)
(OBJ-AT-O4-P6-P7)
(OBJ-AT-O4-P7-P0)
(OBJ-AT-O4-P7-P1)
(OBJ-AT-O4-P7-P2)
(OBJ-AT-O4-P7-P3)
(OBJ-AT-O4-P7-P4)
(OBJ-AT-O4-P7-P5)
(OBJ-AT-O4-P7-P6)
(OBJ-AT-O4-P7-P7)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P0-P6)
(OBJ-AT-O3-P0-P7)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P1-P6)
(OBJ-AT-O3-P1-P7)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P2-P6)
(OBJ-AT-O3-P2-P7)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P3-P6)
(OBJ-AT-O3-P3-P7)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P4-P6)
(OBJ-AT-O3-P4-P7)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O3-P5-P6)
(OBJ-AT-O3-P5-P7)
(OBJ-AT-O3-P6-P0)
(OBJ-AT-O3-P6-P1)
(OBJ-AT-O3-P6-P2)
(OBJ-AT-O3-P6-P3)
(OBJ-AT-O3-P6-P4)
(OBJ-AT-O3-P6-P5)
(OBJ-AT-O3-P6-P6)
(OBJ-AT-O3-P6-P7)
(OBJ-AT-O3-P7-P0)
(OBJ-AT-O3-P7-P1)
(OBJ-AT-O3-P7-P2)
(OBJ-AT-O3-P7-P3)
(OBJ-AT-O3-P7-P4)
(OBJ-AT-O3-P7-P5)
(OBJ-AT-O3-P7-P6)
(OBJ-AT-O3-P7-P7)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P0-P6)
(OBJ-AT-O2-P0-P7)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P1-P6)
(OBJ-AT-O2-P1-P7)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P2-P6)
(OBJ-AT-O2-P2-P7)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P3-P6)
(OBJ-AT-O2-P3-P7)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P4-P6)
(OBJ-AT-O2-P4-P7)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O2-P5-P6)
(OBJ-AT-O2-P5-P7)
(OBJ-AT-O2-P6-P0)
(OBJ-AT-O2-P6-P1)
(OBJ-AT-O2-P6-P2)
(OBJ-AT-O2-P6-P3)
(OBJ-AT-O2-P6-P4)
(OBJ-AT-O2-P6-P5)
(OBJ-AT-O2-P6-P6)
(OBJ-AT-O2-P6-P7)
(OBJ-AT-O2-P7-P0)
(OBJ-AT-O2-P7-P2)
(OBJ-AT-O2-P7-P3)
(OBJ-AT-O2-P7-P4)
(OBJ-AT-O2-P7-P5)
(OBJ-AT-O2-P7-P6)
(OBJ-AT-O2-P7-P7)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P6)
(OBJ-AT-O1-P0-P7)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P1-P6)
(OBJ-AT-O1-P1-P7)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O1-P2-P6)
(OBJ-AT-O1-P2-P7)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P3-P6)
(OBJ-AT-O1-P3-P7)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P4-P6)
(OBJ-AT-O1-P4-P7)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O1-P5-P6)
(OBJ-AT-O1-P5-P7)
(OBJ-AT-O1-P6-P0)
(OBJ-AT-O1-P6-P1)
(OBJ-AT-O1-P6-P2)
(OBJ-AT-O1-P6-P3)
(OBJ-AT-O1-P6-P4)
(OBJ-AT-O1-P6-P5)
(OBJ-AT-O1-P6-P6)
(OBJ-AT-O1-P6-P7)
(OBJ-AT-O1-P7-P0)
(OBJ-AT-O1-P7-P1)
(OBJ-AT-O1-P7-P2)
(OBJ-AT-O1-P7-P3)
(OBJ-AT-O1-P7-P4)
(OBJ-AT-O1-P7-P5)
(OBJ-AT-O1-P7-P6)
(OBJ-AT-O1-P7-P7)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P0-P6)
(OBJ-AT-O0-P0-P7)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P1-P6)
(OBJ-AT-O0-P1-P7)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O0-P2-P6)
(OBJ-AT-O0-P2-P7)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P3-P6)
(OBJ-AT-O0-P3-P7)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P4-P6)
(OBJ-AT-O0-P4-P7)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(OBJ-AT-O0-P5-P6)
(OBJ-AT-O0-P5-P7)
(OBJ-AT-O0-P6-P0)
(OBJ-AT-O0-P6-P2)
(OBJ-AT-O0-P6-P3)
(OBJ-AT-O0-P6-P4)
(OBJ-AT-O0-P6-P5)
(OBJ-AT-O0-P6-P6)
(OBJ-AT-O0-P6-P7)
(OBJ-AT-O0-P7-P0)
(OBJ-AT-O0-P7-P1)
(OBJ-AT-O0-P7-P2)
(OBJ-AT-O0-P7-P3)
(OBJ-AT-O0-P7-P4)
(OBJ-AT-O0-P7-P5)
(OBJ-AT-O0-P7-P6)
(OBJ-AT-O0-P7-P7)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag11-)
(CAPACITY-C0)
(OBJ-AT-O0-P6-P1)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O2-P7-P1)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O6-P6-P7)
(OBJ-AT-O7-P4-P7)
(OBJ-AT-O8-P0-P4)
(OBJ-AT-O9-P5-P7)
(ROBOT-AT-P0-P0)
)
(:derived (Flag11-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)))

(:action PICKUP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P7))
)
)
(:action PICKUP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P6))
)
)
(:action PICKUP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P5))
)
)
(:action PICKUP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P4))
)
)
(:action PICKUP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P3))
)
)
(:action PICKUP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P2))
)
)
(:action PICKUP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P1))
)
)
(:action PICKUP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P0))
)
)
(:action PICKUP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P7))
)
)
(:action PICKUP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P6))
)
)
(:action PICKUP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P5))
)
)
(:action PICKUP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P4))
)
)
(:action PICKUP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P3))
)
)
(:action PICKUP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P2))
)
)
(:action PICKUP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P0))
)
)
(:action PICKUP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P7))
)
)
(:action PICKUP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P6))
)
)
(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
)
)
(:action PICKUP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P7))
)
)
(:action PICKUP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P6))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
)
)
(:action PICKUP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P7))
)
)
(:action PICKUP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P6))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
)
)
(:action PICKUP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P7))
)
)
(:action PICKUP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P6))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
)
)
(:action PICKUP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P7))
)
)
(:action PICKUP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P6))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
)
)
(:action PICKUP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P7))
)
)
(:action PICKUP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P6))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
)
)
(:action PICKUP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P7))
)
)
(:action PICKUP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P6))
)
)
(:action PICKUP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P5))
)
)
(:action PICKUP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P4))
)
)
(:action PICKUP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P3))
)
)
(:action PICKUP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P2))
)
)
(:action PICKUP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P1))
)
)
(:action PICKUP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P0))
)
)
(:action PICKUP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P7))
)
)
(:action PICKUP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P6))
)
)
(:action PICKUP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P5))
)
)
(:action PICKUP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P4))
)
)
(:action PICKUP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P3))
)
)
(:action PICKUP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P2))
)
)
(:action PICKUP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P1))
)
)
(:action PICKUP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P0))
)
)
(:action PICKUP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P7))
)
)
(:action PICKUP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P6))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
)
)
(:action PICKUP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P7))
)
)
(:action PICKUP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P6))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
)
)
(:action PICKUP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P7))
)
)
(:action PICKUP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P6))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
)
)
(:action PICKUP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P7))
)
)
(:action PICKUP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P6))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
)
)
(:action PICKUP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P7))
)
)
(:action PICKUP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P6))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
)
)
(:action PICKUP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P7))
)
)
(:action PICKUP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P6))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
)
)
(:action PICKUP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P7))
)
)
(:action PICKUP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P6))
)
)
(:action PICKUP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P5))
)
)
(:action PICKUP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P4))
)
)
(:action PICKUP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P3))
)
)
(:action PICKUP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P2))
)
)
(:action PICKUP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P0))
)
)
(:action PICKUP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P7))
)
)
(:action PICKUP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P6))
)
)
(:action PICKUP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P5))
)
)
(:action PICKUP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P4))
)
)
(:action PICKUP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P3))
)
)
(:action PICKUP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P2))
)
)
(:action PICKUP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P1))
)
)
(:action PICKUP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P0))
)
)
(:action PICKUP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P7))
)
)
(:action PICKUP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P6))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
)
)
(:action PICKUP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P7))
)
)
(:action PICKUP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P6))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
)
)
(:action PICKUP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P7))
)
)
(:action PICKUP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P6))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
)
)
(:action PICKUP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P7))
)
)
(:action PICKUP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P6))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
)
)
(:action PICKUP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P7))
)
)
(:action PICKUP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P6))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
)
)
(:action PICKUP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P7))
)
)
(:action PICKUP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P6))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
)
)
(:action PICKUP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P7))
)
)
(:action PICKUP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P6))
)
)
(:action PICKUP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P5))
)
)
(:action PICKUP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P4))
)
)
(:action PICKUP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P3))
)
)
(:action PICKUP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P2))
)
)
(:action PICKUP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P1))
)
)
(:action PICKUP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P0))
)
)
(:action PICKUP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P7))
)
)
(:action PICKUP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P6))
)
)
(:action PICKUP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P5))
)
)
(:action PICKUP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P4))
)
)
(:action PICKUP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P3))
)
)
(:action PICKUP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P2))
)
)
(:action PICKUP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P1))
)
)
(:action PICKUP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P0))
)
)
(:action PICKUP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P7))
)
)
(:action PICKUP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P6))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
)
)
(:action PICKUP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P7))
)
)
(:action PICKUP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P6))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
)
)
(:action PICKUP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P7))
)
)
(:action PICKUP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P6))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
)
)
(:action PICKUP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P7))
)
)
(:action PICKUP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P6))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
)
)
(:action PICKUP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P7))
)
)
(:action PICKUP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P6))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
)
)
(:action PICKUP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P7))
)
)
(:action PICKUP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P6))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
)
)
(:action PICKUP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P7))
)
)
(:action PICKUP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P6))
)
)
(:action PICKUP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P5))
)
)
(:action PICKUP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P4))
)
)
(:action PICKUP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P3))
)
)
(:action PICKUP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P2))
)
)
(:action PICKUP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P1))
)
)
(:action PICKUP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P0))
)
)
(:action PICKUP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P7))
)
)
(:action PICKUP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P6))
)
)
(:action PICKUP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P5))
)
)
(:action PICKUP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P4))
)
)
(:action PICKUP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P3))
)
)
(:action PICKUP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P2))
)
)
(:action PICKUP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P1))
)
)
(:action PICKUP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P0))
)
)
(:action PICKUP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P7))
)
)
(:action PICKUP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P6))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
)
)
(:action PICKUP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P7))
)
)
(:action PICKUP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P6))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
)
)
(:action PICKUP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P7))
)
)
(:action PICKUP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P6))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
)
)
(:action PICKUP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P7))
)
)
(:action PICKUP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P6))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
)
)
(:action PICKUP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P7))
)
)
(:action PICKUP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P6))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
)
)
(:action PICKUP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P7))
)
)
(:action PICKUP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P6))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
)
)
(:action PICKUP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P7))
)
)
(:action PICKUP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P6))
)
)
(:action PICKUP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P5))
)
)
(:action PICKUP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P4))
)
)
(:action PICKUP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P3))
)
)
(:action PICKUP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P2))
)
)
(:action PICKUP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P1))
)
)
(:action PICKUP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P0))
)
)
(:action PICKUP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P7))
)
)
(:action PICKUP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P6))
)
)
(:action PICKUP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P5))
)
)
(:action PICKUP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P4))
)
)
(:action PICKUP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P3))
)
)
(:action PICKUP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P2))
)
)
(:action PICKUP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P1))
)
)
(:action PICKUP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P0))
)
)
(:action PICKUP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P7))
)
)
(:action PICKUP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P6))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
)
)
(:action PICKUP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P7))
)
)
(:action PICKUP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P6))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
)
)
(:action PICKUP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P7))
)
)
(:action PICKUP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P6))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
)
)
(:action PICKUP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P7))
)
)
(:action PICKUP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P6))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
)
)
(:action PICKUP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P7))
)
)
(:action PICKUP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P6))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
)
)
(:action PICKUP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P7))
)
)
(:action PICKUP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P6))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
)
)
(:action PICKUP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P7))
)
)
(:action PICKUP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P6))
)
)
(:action PICKUP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P5))
)
)
(:action PICKUP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P4))
)
)
(:action PICKUP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P3))
)
)
(:action PICKUP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P2))
)
)
(:action PICKUP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P1))
)
)
(:action PICKUP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P0))
)
)
(:action PICKUP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P6))
)
)
(:action PICKUP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P5))
)
)
(:action PICKUP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P4))
)
)
(:action PICKUP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P3))
)
)
(:action PICKUP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P2))
)
)
(:action PICKUP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P1))
)
)
(:action PICKUP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P0))
)
)
(:action PICKUP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P7))
)
)
(:action PICKUP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P6))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
)
)
(:action PICKUP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P7))
)
)
(:action PICKUP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P6))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
)
)
(:action PICKUP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P7))
)
)
(:action PICKUP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P6))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
)
)
(:action PICKUP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P7))
)
)
(:action PICKUP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P6))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
)
)
(:action PICKUP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P7))
)
)
(:action PICKUP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P6))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
)
)
(:action PICKUP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P7))
)
)
(:action PICKUP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P6))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
)
)
(:action PICKUP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P7))
)
)
(:action PICKUP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P6))
)
)
(:action PICKUP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P5))
)
)
(:action PICKUP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P4))
)
)
(:action PICKUP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P3))
)
)
(:action PICKUP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P2))
)
)
(:action PICKUP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P1))
)
)
(:action PICKUP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P0))
)
)
(:action PICKUP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P7))
)
)
(:action PICKUP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P6))
)
)
(:action PICKUP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P5))
)
)
(:action PICKUP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P4))
)
)
(:action PICKUP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P3))
)
)
(:action PICKUP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P2))
)
)
(:action PICKUP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P1))
)
)
(:action PICKUP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P0))
)
)
(:action PICKUP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P7))
)
)
(:action PICKUP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P6))
)
)
(:action PICKUP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P5))
)
)
(:action PICKUP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P4))
)
)
(:action PICKUP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P3))
)
)
(:action PICKUP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P2))
)
)
(:action PICKUP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P1))
)
)
(:action PICKUP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P0))
)
)
(:action PICKUP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P6))
)
)
(:action PICKUP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P5))
)
)
(:action PICKUP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P4))
)
)
(:action PICKUP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P3))
)
)
(:action PICKUP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P2))
)
)
(:action PICKUP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P1))
)
)
(:action PICKUP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P0))
)
)
(:action PICKUP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P7))
)
)
(:action PICKUP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P6))
)
)
(:action PICKUP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P5))
)
)
(:action PICKUP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P4))
)
)
(:action PICKUP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P3))
)
)
(:action PICKUP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P2))
)
)
(:action PICKUP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P1))
)
)
(:action PICKUP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P0))
)
)
(:action PICKUP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P7))
)
)
(:action PICKUP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P6))
)
)
(:action PICKUP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P5))
)
)
(:action PICKUP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P4))
)
)
(:action PICKUP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P3))
)
)
(:action PICKUP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P2))
)
)
(:action PICKUP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P1))
)
)
(:action PICKUP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P0))
)
)
(:action PICKUP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P7))
)
)
(:action PICKUP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P6))
)
)
(:action PICKUP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P5))
)
)
(:action PICKUP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P4))
)
)
(:action PICKUP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P3))
)
)
(:action PICKUP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P2))
)
)
(:action PICKUP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P1))
)
)
(:action PICKUP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P0))
)
)
(:action PICKUP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P7))
)
)
(:action PICKUP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P6))
)
)
(:action PICKUP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P5))
)
)
(:action PICKUP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P4))
)
)
(:action PICKUP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P3))
)
)
(:action PICKUP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P2))
)
)
(:action PICKUP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P1))
)
)
(:action PICKUP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P0))
)
)
(:action PICKUP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P7))
)
)
(:action PICKUP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P6))
)
)
(:action PICKUP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P5))
)
)
(:action PICKUP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P4))
)
)
(:action PICKUP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P3))
)
)
(:action PICKUP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P2))
)
)
(:action PICKUP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P1))
)
)
(:action PICKUP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P0))
)
)
(:action PICKUP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P7))
)
)
(:action PICKUP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P6))
)
)
(:action PICKUP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P5))
)
)
(:action PICKUP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P4))
)
)
(:action PICKUP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P3))
)
)
(:action PICKUP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P2))
)
)
(:action PICKUP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P1))
)
)
(:action PICKUP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P0))
)
)
(:action PICKUP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P7))
)
)
(:action PICKUP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P6))
)
)
(:action PICKUP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P5))
)
)
(:action PICKUP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P4))
)
)
(:action PICKUP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P3))
)
)
(:action PICKUP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P2))
)
)
(:action PICKUP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P1))
)
)
(:action PICKUP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P0))
)
)
(:action PICKUP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P7))
)
)
(:action PICKUP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P6))
)
)
(:action PICKUP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P5))
)
)
(:action PICKUP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P4))
)
)
(:action PICKUP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P3))
)
)
(:action PICKUP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P2))
)
)
(:action PICKUP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P1))
)
)
(:action PICKUP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P0))
)
)
(:action PICKUP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P7))
)
)
(:action PICKUP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P6))
)
)
(:action PICKUP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P5))
)
)
(:action PICKUP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P4))
)
)
(:action PICKUP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P3))
)
)
(:action PICKUP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P2))
)
)
(:action PICKUP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P1))
)
)
(:action PICKUP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P0))
)
)
(:action PICKUP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P7))
)
)
(:action PICKUP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P6))
)
)
(:action PICKUP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P5))
)
)
(:action PICKUP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P4))
)
)
(:action PICKUP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P3))
)
)
(:action PICKUP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P2))
)
)
(:action PICKUP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P1))
)
)
(:action PICKUP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P0))
)
)
(:action PICKUP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P7))
)
)
(:action PICKUP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P6))
)
)
(:action PICKUP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P5))
)
)
(:action PICKUP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P4))
)
)
(:action PICKUP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P3))
)
)
(:action PICKUP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P2))
)
)
(:action PICKUP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P1))
)
)
(:action PICKUP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P0))
)
)
(:action PICKUP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P7))
)
)
(:action PICKUP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P6))
)
)
(:action PICKUP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P5))
)
)
(:action PICKUP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P3))
)
)
(:action PICKUP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P2))
)
)
(:action PICKUP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P1))
)
)
(:action PICKUP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P0))
)
)
(:action PICKUP-O9-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P7))
)
)
(:action PICKUP-O9-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P6))
)
)
(:action PICKUP-O9-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P5))
)
)
(:action PICKUP-O9-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P4))
)
)
(:action PICKUP-O9-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P3))
)
)
(:action PICKUP-O9-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P2))
)
)
(:action PICKUP-O9-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P1))
)
)
(:action PICKUP-O9-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P0))
)
)
(:action PICKUP-O9-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P7))
)
)
(:action PICKUP-O9-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P6))
)
)
(:action PICKUP-O9-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P5))
)
)
(:action PICKUP-O9-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P4))
)
)
(:action PICKUP-O9-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P3))
)
)
(:action PICKUP-O9-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P2))
)
)
(:action PICKUP-O9-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P1))
)
)
(:action PICKUP-O9-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P0))
)
)
(:action PICKUP-O9-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P6))
)
)
(:action PICKUP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P5))
)
)
(:action PICKUP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P4))
)
)
(:action PICKUP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P3))
)
)
(:action PICKUP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P2))
)
)
(:action PICKUP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P1))
)
)
(:action PICKUP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P0))
)
)
(:action PICKUP-O9-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P7))
)
)
(:action PICKUP-O9-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P6))
)
)
(:action PICKUP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P5))
)
)
(:action PICKUP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P4))
)
)
(:action PICKUP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P3))
)
)
(:action PICKUP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P2))
)
)
(:action PICKUP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P1))
)
)
(:action PICKUP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P0))
)
)
(:action PICKUP-O9-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P7))
)
)
(:action PICKUP-O9-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P6))
)
)
(:action PICKUP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P5))
)
)
(:action PICKUP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P4))
)
)
(:action PICKUP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P3))
)
)
(:action PICKUP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P2))
)
)
(:action PICKUP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P1))
)
)
(:action PICKUP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P0))
)
)
(:action PICKUP-O9-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P7))
)
)
(:action PICKUP-O9-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P6))
)
)
(:action PICKUP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P5))
)
)
(:action PICKUP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P4))
)
)
(:action PICKUP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P3))
)
)
(:action PICKUP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P2))
)
)
(:action PICKUP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P1))
)
)
(:action PICKUP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P0))
)
)
(:action PICKUP-O9-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P7))
)
)
(:action PICKUP-O9-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P6))
)
)
(:action PICKUP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P5))
)
)
(:action PICKUP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P4))
)
)
(:action PICKUP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P3))
)
)
(:action PICKUP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P2))
)
)
(:action PICKUP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P1))
)
)
(:action PICKUP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P0))
)
)
(:action PICKUP-O9-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P7))
)
)
(:action PICKUP-O9-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P6))
)
)
(:action PICKUP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P5))
)
)
(:action PICKUP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P4))
)
)
(:action PICKUP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P3))
)
)
(:action PICKUP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P2))
)
)
(:action PICKUP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P1))
)
)
(:action PICKUP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P0))
)
)
(:derived (Flag1-)(and (OBJ-AT-O1-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O2-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O3-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O4-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O5-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O6-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O7-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O8-P6-P1)))

(:derived (Flag1-)(and (OBJ-AT-O9-P6-P1)))

(:derived (Flag2-)(and (OBJ-AT-O0-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O1-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O2-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O3-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O4-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O5-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O6-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O7-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O8-P2-P0)))

(:derived (Flag2-)(and (OBJ-AT-O9-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O0-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O1-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O2-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O3-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O4-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O5-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O6-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O7-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O8-P5-P3)))

(:derived (Flag3-)(and (OBJ-AT-O9-P5-P3)))

(:derived (Flag4-)(and (OBJ-AT-O0-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O1-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O2-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O3-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O4-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O5-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O6-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O7-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O8-P2-P5)))

(:derived (Flag4-)(and (OBJ-AT-O9-P2-P5)))

(:derived (Flag5-)(and (OBJ-AT-O0-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O1-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O2-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O3-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O4-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O5-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O6-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O7-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O8-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O9-P0-P2)))

(:derived (Flag6-)(and (OBJ-AT-O0-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O1-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O2-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O3-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O4-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O5-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O6-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O7-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O8-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O9-P5-P5)))

(:derived (Flag7-)(and (OBJ-AT-O0-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O1-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O2-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O3-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O4-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O5-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O6-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O7-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O8-P0-P6)))

(:derived (Flag7-)(and (OBJ-AT-O9-P0-P6)))

(:derived (Flag8-)(and (OBJ-AT-O0-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O1-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O2-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O3-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O4-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O5-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O6-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O7-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O8-P4-P1)))

(:derived (Flag8-)(and (OBJ-AT-O9-P4-P1)))

(:derived (Flag9-)(and (OBJ-AT-O0-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O1-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O2-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O3-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O4-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O5-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O6-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O7-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O8-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O9-P1-P6)))

(:derived (Flag10-)(and (OBJ-AT-O0-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O1-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O2-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O3-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O4-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O6-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O7-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O9-P0-P4)))

(:action DROP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O0-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O0-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O0-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O0-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O0-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O0-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O0-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O0-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O0-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O0-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O0-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O0-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O0-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O0-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O0-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O0-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O0-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O0-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O0-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O0-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O0-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O0-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O0-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O0-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O0-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O0-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O0-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O0-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O1-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O1-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O1-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O1-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O1-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O1-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O1-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O1-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O1-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O1-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O1-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O1-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O1-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O1-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O1-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O1-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O1-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O1-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O1-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O1-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O1-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O1-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O1-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O1-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O1-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O1-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O1-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O1-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O2-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O2-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O2-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O2-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O2-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O2-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O2-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O2-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O2-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O2-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O2-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O2-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O2-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O2-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O2-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O2-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O2-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O2-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O2-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O2-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O2-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O2-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O2-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O2-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O2-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O2-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O2-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O2-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O3-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O3-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O3-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O3-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O3-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O3-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O3-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O3-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O3-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O3-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O3-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O3-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O3-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O3-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O3-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O3-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O3-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O3-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O3-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O3-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O3-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O3-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O3-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O3-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O3-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O3-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O3-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O3-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O4-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O4-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O4-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O4-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O4-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O4-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O4-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O4-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O4-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O4-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O4-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O4-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O4-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O4-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O4-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O4-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O4-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O4-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O4-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O4-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O4-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O4-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O4-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O4-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O4-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O4-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O4-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O4-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O5-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O5-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O5-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O5-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O5-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O5-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O5-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O5-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O5-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O5-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O5-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O5-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O5-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O5-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O5-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O5-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O5-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O5-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O5-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O5-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O5-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O5-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O5-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O5-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O5-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O5-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O5-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O5-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O6-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O6-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O6-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O6-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O6-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O6-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O6-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O6-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O6-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O6-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O6-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O6-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O6-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O6-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O6-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O6-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O6-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O6-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O6-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O6-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O6-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O6-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O6-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O6-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O6-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O6-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O6-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O6-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O7-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O7-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O7-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O7-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O7-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O7-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O7-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O7-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O7-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O7-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O7-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O7-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O7-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O7-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O7-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O7-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O7-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O7-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O7-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O7-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O7-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O7-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O7-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O7-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O7-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O7-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O7-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O7-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O7-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O7-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O7-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O7-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O7-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O7-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O7-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O7-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O7-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O7-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O7-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O7-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O7-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O7-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O7-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O7-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O7-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O7-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O7-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O7-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O7-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O7-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O7-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O7-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O7-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O7-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O7-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O7-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O7-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O7-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O7-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O7-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O7-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O7-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O7-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O7-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O8-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O8-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O8-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O8-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O8-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O8-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O8-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O8-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O8-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O8-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O8-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O8-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O8-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O8-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O8-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O8-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O8-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O8-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O8-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O8-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O8-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O8-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O8-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O8-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O8-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O8-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O8-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O8-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O8-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O8-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O8-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O8-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O8-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O8-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O8-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O8-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O8-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O8-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O8-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O8-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O8-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O8-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O8-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O8-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O8-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O8-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O8-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O8-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O8-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O8-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O8-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O8-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O8-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O8-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O8-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O8-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O8-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O8-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O8-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O8-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O8-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O8-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O8-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O8-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O9-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O9-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O9-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O9-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O9-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O9-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O9-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O9-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O9-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O9-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O9-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O9-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O9-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O9-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O9-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O9-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O9-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O9-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O9-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O9-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O9-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O9-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O9-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O9-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O9-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O9-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O9-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O9-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O9-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O9-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O9-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O9-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O9-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O9-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O9-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O9-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O9-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O9-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O9-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O9-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O9-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O9-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O9-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O9-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O9-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O9-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O9-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O9-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O9-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O9-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O9-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O9-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O9-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O9-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O9-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O9-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O9-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O9-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O9-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O9-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O9-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O9-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O9-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O9-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O9-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action PICKUP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P1))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
)
)
(:action PICKUP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P1))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
)
)
(:action PICKUP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P7))
)
)
(:action PICKUP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P7))
)
)
(:action PICKUP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P4))
)
)
(:action PICKUP-O9-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P7))
)
)
(:action MOVE-P7-P7-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P6-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P5-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P4-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P3-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P2-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P1-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P0-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P7-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P6-P7-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P6-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P5-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P4-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P3-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P2-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P1-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P0-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P5-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P4-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P3-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P2-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P1-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P0-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P7-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
)
)
(:derived (Flag1-)(and (OBJ-AT-O0-P6-P1)))

(:derived (Flag10-)(and (OBJ-AT-O5-P0-P4)))

(:derived (Flag10-)(and (OBJ-AT-O8-P0-P4)))

)
