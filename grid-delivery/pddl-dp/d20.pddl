(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(Flag276-)
(Flag184-)
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P0-P6)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P0-P7)
(ROBOT-AT-P1-P6)
(ROBOT-AT-P1-P7)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P2-P6)
(ROBOT-AT-P2-P7)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P3-P6)
(ROBOT-AT-P3-P7)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P4-P6)
(ROBOT-AT-P4-P7)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(ROBOT-AT-P5-P6)
(ROBOT-AT-P5-P7)
(ROBOT-AT-P6-P0)
(ROBOT-AT-P6-P1)
(ROBOT-AT-P6-P2)
(ROBOT-AT-P6-P3)
(ROBOT-AT-P6-P4)
(ROBOT-AT-P6-P5)
(ROBOT-AT-P6-P6)
(ROBOT-AT-P6-P7)
(ROBOT-AT-P7-P0)
(ROBOT-AT-P7-P1)
(ROBOT-AT-P7-P2)
(ROBOT-AT-P7-P3)
(ROBOT-AT-P7-P4)
(ROBOT-AT-P7-P5)
(ROBOT-AT-P7-P6)
(ROBOT-AT-P7-P7)
(HOLDING-O9)
(HOLDING-O8)
(HOLDING-O7)
(HOLDING-O6)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O3)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O9-P0-P0)
(OBJ-AT-O9-P0-P1)
(OBJ-AT-O9-P0-P2)
(OBJ-AT-O9-P0-P3)
(OBJ-AT-O9-P0-P4)
(OBJ-AT-O9-P0-P5)
(OBJ-AT-O9-P0-P6)
(OBJ-AT-O9-P0-P7)
(OBJ-AT-O9-P1-P0)
(OBJ-AT-O9-P1-P1)
(OBJ-AT-O9-P1-P2)
(OBJ-AT-O9-P1-P3)
(OBJ-AT-O9-P1-P4)
(OBJ-AT-O9-P1-P5)
(OBJ-AT-O9-P1-P6)
(OBJ-AT-O9-P1-P7)
(OBJ-AT-O9-P2-P0)
(OBJ-AT-O9-P2-P1)
(OBJ-AT-O9-P2-P2)
(OBJ-AT-O9-P2-P3)
(OBJ-AT-O9-P2-P4)
(OBJ-AT-O9-P2-P5)
(OBJ-AT-O9-P2-P6)
(OBJ-AT-O9-P2-P7)
(OBJ-AT-O9-P3-P0)
(OBJ-AT-O9-P3-P1)
(OBJ-AT-O9-P3-P2)
(OBJ-AT-O9-P3-P3)
(OBJ-AT-O9-P3-P4)
(OBJ-AT-O9-P3-P5)
(OBJ-AT-O9-P3-P6)
(OBJ-AT-O9-P3-P7)
(OBJ-AT-O9-P4-P0)
(OBJ-AT-O9-P4-P1)
(OBJ-AT-O9-P4-P2)
(OBJ-AT-O9-P4-P3)
(OBJ-AT-O9-P4-P4)
(OBJ-AT-O9-P4-P5)
(OBJ-AT-O9-P4-P6)
(OBJ-AT-O9-P4-P7)
(OBJ-AT-O9-P5-P0)
(OBJ-AT-O9-P5-P1)
(OBJ-AT-O9-P5-P2)
(OBJ-AT-O9-P5-P3)
(OBJ-AT-O9-P5-P4)
(OBJ-AT-O9-P5-P5)
(OBJ-AT-O9-P5-P6)
(OBJ-AT-O9-P5-P7)
(OBJ-AT-O9-P6-P0)
(OBJ-AT-O9-P6-P1)
(OBJ-AT-O9-P6-P2)
(OBJ-AT-O9-P6-P3)
(OBJ-AT-O9-P6-P4)
(OBJ-AT-O9-P6-P5)
(OBJ-AT-O9-P6-P6)
(OBJ-AT-O9-P6-P7)
(OBJ-AT-O9-P7-P0)
(OBJ-AT-O9-P7-P1)
(OBJ-AT-O9-P7-P2)
(OBJ-AT-O9-P7-P3)
(OBJ-AT-O9-P7-P5)
(OBJ-AT-O9-P7-P6)
(OBJ-AT-O9-P7-P7)
(OBJ-AT-O8-P0-P0)
(OBJ-AT-O8-P0-P1)
(OBJ-AT-O8-P0-P2)
(OBJ-AT-O8-P0-P3)
(OBJ-AT-O8-P0-P4)
(OBJ-AT-O8-P0-P5)
(OBJ-AT-O8-P0-P6)
(OBJ-AT-O8-P0-P7)
(OBJ-AT-O8-P1-P0)
(OBJ-AT-O8-P1-P1)
(OBJ-AT-O8-P1-P2)
(OBJ-AT-O8-P1-P3)
(OBJ-AT-O8-P1-P4)
(OBJ-AT-O8-P1-P5)
(OBJ-AT-O8-P1-P6)
(OBJ-AT-O8-P1-P7)
(OBJ-AT-O8-P2-P0)
(OBJ-AT-O8-P2-P1)
(OBJ-AT-O8-P2-P2)
(OBJ-AT-O8-P2-P3)
(OBJ-AT-O8-P2-P4)
(OBJ-AT-O8-P2-P5)
(OBJ-AT-O8-P2-P6)
(OBJ-AT-O8-P2-P7)
(OBJ-AT-O8-P3-P0)
(OBJ-AT-O8-P3-P1)
(OBJ-AT-O8-P3-P2)
(OBJ-AT-O8-P3-P3)
(OBJ-AT-O8-P3-P4)
(OBJ-AT-O8-P3-P5)
(OBJ-AT-O8-P3-P6)
(OBJ-AT-O8-P3-P7)
(OBJ-AT-O8-P4-P0)
(OBJ-AT-O8-P4-P1)
(OBJ-AT-O8-P4-P2)
(OBJ-AT-O8-P4-P3)
(OBJ-AT-O8-P4-P4)
(OBJ-AT-O8-P4-P5)
(OBJ-AT-O8-P4-P7)
(OBJ-AT-O8-P5-P0)
(OBJ-AT-O8-P5-P1)
(OBJ-AT-O8-P5-P2)
(OBJ-AT-O8-P5-P3)
(OBJ-AT-O8-P5-P4)
(OBJ-AT-O8-P5-P5)
(OBJ-AT-O8-P5-P6)
(OBJ-AT-O8-P5-P7)
(OBJ-AT-O8-P6-P0)
(OBJ-AT-O8-P6-P1)
(OBJ-AT-O8-P6-P2)
(OBJ-AT-O8-P6-P3)
(OBJ-AT-O8-P6-P4)
(OBJ-AT-O8-P6-P5)
(OBJ-AT-O8-P6-P6)
(OBJ-AT-O8-P6-P7)
(OBJ-AT-O8-P7-P0)
(OBJ-AT-O8-P7-P1)
(OBJ-AT-O8-P7-P2)
(OBJ-AT-O8-P7-P3)
(OBJ-AT-O8-P7-P4)
(OBJ-AT-O8-P7-P5)
(OBJ-AT-O8-P7-P6)
(OBJ-AT-O8-P7-P7)
(OBJ-AT-O7-P0-P0)
(OBJ-AT-O7-P0-P1)
(OBJ-AT-O7-P0-P2)
(OBJ-AT-O7-P0-P3)
(OBJ-AT-O7-P0-P4)
(OBJ-AT-O7-P0-P5)
(OBJ-AT-O7-P0-P6)
(OBJ-AT-O7-P0-P7)
(OBJ-AT-O7-P1-P0)
(OBJ-AT-O7-P1-P1)
(OBJ-AT-O7-P1-P2)
(OBJ-AT-O7-P1-P3)
(OBJ-AT-O7-P1-P4)
(OBJ-AT-O7-P1-P5)
(OBJ-AT-O7-P1-P6)
(OBJ-AT-O7-P1-P7)
(OBJ-AT-O7-P2-P1)
(OBJ-AT-O7-P2-P2)
(OBJ-AT-O7-P2-P3)
(OBJ-AT-O7-P2-P4)
(OBJ-AT-O7-P2-P5)
(OBJ-AT-O7-P2-P6)
(OBJ-AT-O7-P2-P7)
(OBJ-AT-O7-P3-P0)
(OBJ-AT-O7-P3-P1)
(OBJ-AT-O7-P3-P2)
(OBJ-AT-O7-P3-P3)
(OBJ-AT-O7-P3-P4)
(OBJ-AT-O7-P3-P5)
(OBJ-AT-O7-P3-P6)
(OBJ-AT-O7-P3-P7)
(OBJ-AT-O7-P4-P0)
(OBJ-AT-O7-P4-P1)
(OBJ-AT-O7-P4-P2)
(OBJ-AT-O7-P4-P3)
(OBJ-AT-O7-P4-P4)
(OBJ-AT-O7-P4-P5)
(OBJ-AT-O7-P4-P6)
(OBJ-AT-O7-P4-P7)
(OBJ-AT-O7-P5-P0)
(OBJ-AT-O7-P5-P1)
(OBJ-AT-O7-P5-P2)
(OBJ-AT-O7-P5-P3)
(OBJ-AT-O7-P5-P4)
(OBJ-AT-O7-P5-P5)
(OBJ-AT-O7-P5-P6)
(OBJ-AT-O7-P5-P7)
(OBJ-AT-O7-P6-P0)
(OBJ-AT-O7-P6-P1)
(OBJ-AT-O7-P6-P2)
(OBJ-AT-O7-P6-P3)
(OBJ-AT-O7-P6-P4)
(OBJ-AT-O7-P6-P5)
(OBJ-AT-O7-P6-P6)
(OBJ-AT-O7-P6-P7)
(OBJ-AT-O7-P7-P0)
(OBJ-AT-O7-P7-P1)
(OBJ-AT-O7-P7-P2)
(OBJ-AT-O7-P7-P3)
(OBJ-AT-O7-P7-P4)
(OBJ-AT-O7-P7-P5)
(OBJ-AT-O7-P7-P6)
(OBJ-AT-O7-P7-P7)
(OBJ-AT-O6-P0-P0)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P0-P6)
(OBJ-AT-O6-P0-P7)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P1-P6)
(OBJ-AT-O6-P1-P7)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P2-P6)
(OBJ-AT-O6-P2-P7)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P3-P6)
(OBJ-AT-O6-P3-P7)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P4-P6)
(OBJ-AT-O6-P4-P7)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O6-P5-P6)
(OBJ-AT-O6-P5-P7)
(OBJ-AT-O6-P6-P0)
(OBJ-AT-O6-P6-P1)
(OBJ-AT-O6-P6-P2)
(OBJ-AT-O6-P6-P3)
(OBJ-AT-O6-P6-P4)
(OBJ-AT-O6-P6-P5)
(OBJ-AT-O6-P6-P6)
(OBJ-AT-O6-P6-P7)
(OBJ-AT-O6-P7-P0)
(OBJ-AT-O6-P7-P1)
(OBJ-AT-O6-P7-P2)
(OBJ-AT-O6-P7-P3)
(OBJ-AT-O6-P7-P4)
(OBJ-AT-O6-P7-P5)
(OBJ-AT-O6-P7-P6)
(OBJ-AT-O6-P7-P7)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O5-P0-P6)
(OBJ-AT-O5-P0-P7)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P1-P6)
(OBJ-AT-O5-P1-P7)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P2-P6)
(OBJ-AT-O5-P2-P7)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P3-P6)
(OBJ-AT-O5-P3-P7)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P4-P6)
(OBJ-AT-O5-P4-P7)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O5-P5-P6)
(OBJ-AT-O5-P5-P7)
(OBJ-AT-O5-P6-P0)
(OBJ-AT-O5-P6-P1)
(OBJ-AT-O5-P6-P2)
(OBJ-AT-O5-P6-P3)
(OBJ-AT-O5-P6-P4)
(OBJ-AT-O5-P6-P5)
(OBJ-AT-O5-P6-P6)
(OBJ-AT-O5-P6-P7)
(OBJ-AT-O5-P7-P0)
(OBJ-AT-O5-P7-P1)
(OBJ-AT-O5-P7-P2)
(OBJ-AT-O5-P7-P3)
(OBJ-AT-O5-P7-P4)
(OBJ-AT-O5-P7-P5)
(OBJ-AT-O5-P7-P6)
(OBJ-AT-O5-P7-P7)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O4-P0-P6)
(OBJ-AT-O4-P0-P7)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P1-P6)
(OBJ-AT-O4-P1-P7)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O4-P2-P6)
(OBJ-AT-O4-P2-P7)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P3-P6)
(OBJ-AT-O4-P3-P7)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P4-P6)
(OBJ-AT-O4-P4-P7)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O4-P5-P6)
(OBJ-AT-O4-P6-P0)
(OBJ-AT-O4-P6-P1)
(OBJ-AT-O4-P6-P2)
(OBJ-AT-O4-P6-P3)
(OBJ-AT-O4-P6-P4)
(OBJ-AT-O4-P6-P5)
(OBJ-AT-O4-P6-P6)
(OBJ-AT-O4-P6-P7)
(OBJ-AT-O4-P7-P0)
(OBJ-AT-O4-P7-P1)
(OBJ-AT-O4-P7-P2)
(OBJ-AT-O4-P7-P3)
(OBJ-AT-O4-P7-P4)
(OBJ-AT-O4-P7-P5)
(OBJ-AT-O4-P7-P6)
(OBJ-AT-O4-P7-P7)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P0-P6)
(OBJ-AT-O3-P0-P7)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P1-P6)
(OBJ-AT-O3-P1-P7)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P2-P6)
(OBJ-AT-O3-P2-P7)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P3-P6)
(OBJ-AT-O3-P3-P7)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P4-P6)
(OBJ-AT-O3-P4-P7)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O3-P5-P6)
(OBJ-AT-O3-P5-P7)
(OBJ-AT-O3-P6-P0)
(OBJ-AT-O3-P6-P1)
(OBJ-AT-O3-P6-P2)
(OBJ-AT-O3-P6-P3)
(OBJ-AT-O3-P6-P4)
(OBJ-AT-O3-P6-P5)
(OBJ-AT-O3-P6-P6)
(OBJ-AT-O3-P6-P7)
(OBJ-AT-O3-P7-P0)
(OBJ-AT-O3-P7-P1)
(OBJ-AT-O3-P7-P2)
(OBJ-AT-O3-P7-P3)
(OBJ-AT-O3-P7-P4)
(OBJ-AT-O3-P7-P5)
(OBJ-AT-O3-P7-P6)
(OBJ-AT-O3-P7-P7)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P0-P6)
(OBJ-AT-O2-P0-P7)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P1-P7)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P2-P6)
(OBJ-AT-O2-P2-P7)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P3-P6)
(OBJ-AT-O2-P3-P7)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P4-P6)
(OBJ-AT-O2-P4-P7)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O2-P5-P6)
(OBJ-AT-O2-P5-P7)
(OBJ-AT-O2-P6-P0)
(OBJ-AT-O2-P6-P1)
(OBJ-AT-O2-P6-P2)
(OBJ-AT-O2-P6-P3)
(OBJ-AT-O2-P6-P4)
(OBJ-AT-O2-P6-P5)
(OBJ-AT-O2-P6-P6)
(OBJ-AT-O2-P6-P7)
(OBJ-AT-O2-P7-P0)
(OBJ-AT-O2-P7-P1)
(OBJ-AT-O2-P7-P2)
(OBJ-AT-O2-P7-P3)
(OBJ-AT-O2-P7-P4)
(OBJ-AT-O2-P7-P5)
(OBJ-AT-O2-P7-P6)
(OBJ-AT-O2-P7-P7)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O1-P0-P7)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P1-P6)
(OBJ-AT-O1-P1-P7)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O1-P2-P6)
(OBJ-AT-O1-P2-P7)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P3-P6)
(OBJ-AT-O1-P3-P7)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P4-P6)
(OBJ-AT-O1-P4-P7)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O1-P5-P6)
(OBJ-AT-O1-P5-P7)
(OBJ-AT-O1-P6-P0)
(OBJ-AT-O1-P6-P1)
(OBJ-AT-O1-P6-P2)
(OBJ-AT-O1-P6-P3)
(OBJ-AT-O1-P6-P4)
(OBJ-AT-O1-P6-P5)
(OBJ-AT-O1-P6-P6)
(OBJ-AT-O1-P6-P7)
(OBJ-AT-O1-P7-P0)
(OBJ-AT-O1-P7-P1)
(OBJ-AT-O1-P7-P2)
(OBJ-AT-O1-P7-P3)
(OBJ-AT-O1-P7-P4)
(OBJ-AT-O1-P7-P5)
(OBJ-AT-O1-P7-P6)
(OBJ-AT-O1-P7-P7)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P0-P6)
(OBJ-AT-O0-P0-P7)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P1-P6)
(OBJ-AT-O0-P1-P7)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O0-P2-P6)
(OBJ-AT-O0-P2-P7)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P3-P6)
(OBJ-AT-O0-P3-P7)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P4-P6)
(OBJ-AT-O0-P4-P7)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(OBJ-AT-O0-P5-P6)
(OBJ-AT-O0-P5-P7)
(OBJ-AT-O0-P6-P0)
(OBJ-AT-O0-P6-P1)
(OBJ-AT-O0-P6-P2)
(OBJ-AT-O0-P6-P3)
(OBJ-AT-O0-P6-P4)
(OBJ-AT-O0-P6-P5)
(OBJ-AT-O0-P6-P6)
(OBJ-AT-O0-P6-P7)
(OBJ-AT-O0-P7-P0)
(OBJ-AT-O0-P7-P1)
(OBJ-AT-O0-P7-P2)
(OBJ-AT-O0-P7-P3)
(OBJ-AT-O0-P7-P4)
(OBJ-AT-O0-P7-P5)
(OBJ-AT-O0-P7-P6)
(OBJ-AT-O0-P7-P7)
(Flag277-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag275-)
(Flag183-)
(Flag92-)
(Flag278-)
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O1-P0-P6)
(OBJ-AT-O2-P1-P6)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O4-P5-P7)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O7-P2-P0)
(OBJ-AT-O8-P4-P6)
(OBJ-AT-O9-P7-P4)
(ROBOT-AT-P0-P0)
)
(:derived (Flag278-)(and (Flag1-)(Flag92-)(Flag183-)(Flag184-)(Flag275-)(Flag276-)(Flag277-)))

(:derived (Flag92-)(and (Flag2-)))

(:derived (Flag92-)(and (Flag3-)))

(:derived (Flag92-)(and (Flag4-)))

(:derived (Flag92-)(and (Flag5-)))

(:derived (Flag92-)(and (Flag6-)))

(:derived (Flag92-)(and (Flag7-)))

(:derived (Flag92-)(and (Flag8-)))

(:derived (Flag92-)(and (Flag9-)))

(:derived (Flag92-)(and (Flag10-)))

(:derived (Flag92-)(and (Flag11-)))

(:derived (Flag92-)(and (Flag12-)))

(:derived (Flag92-)(and (Flag13-)))

(:derived (Flag92-)(and (Flag14-)))

(:derived (Flag92-)(and (Flag15-)))

(:derived (Flag92-)(and (Flag16-)))

(:derived (Flag92-)(and (Flag17-)))

(:derived (Flag92-)(and (Flag18-)))

(:derived (Flag92-)(and (Flag19-)))

(:derived (Flag92-)(and (Flag20-)))

(:derived (Flag92-)(and (Flag21-)))

(:derived (Flag92-)(and (Flag22-)))

(:derived (Flag92-)(and (Flag23-)))

(:derived (Flag92-)(and (Flag24-)))

(:derived (Flag92-)(and (Flag25-)))

(:derived (Flag92-)(and (Flag26-)))

(:derived (Flag92-)(and (Flag27-)))

(:derived (Flag92-)(and (Flag28-)))

(:derived (Flag92-)(and (Flag29-)))

(:derived (Flag92-)(and (Flag30-)))

(:derived (Flag92-)(and (Flag31-)))

(:derived (Flag92-)(and (Flag32-)))

(:derived (Flag92-)(and (Flag33-)))

(:derived (Flag92-)(and (Flag34-)))

(:derived (Flag92-)(and (Flag35-)))

(:derived (Flag92-)(and (Flag36-)))

(:derived (Flag92-)(and (Flag37-)))

(:derived (Flag92-)(and (Flag38-)))

(:derived (Flag92-)(and (Flag39-)))

(:derived (Flag92-)(and (Flag40-)))

(:derived (Flag92-)(and (Flag41-)))

(:derived (Flag92-)(and (Flag42-)))

(:derived (Flag92-)(and (Flag43-)))

(:derived (Flag92-)(and (Flag44-)))

(:derived (Flag92-)(and (Flag45-)))

(:derived (Flag92-)(and (Flag46-)))

(:derived (Flag92-)(and (Flag47-)))

(:derived (Flag92-)(and (Flag48-)))

(:derived (Flag92-)(and (Flag49-)))

(:derived (Flag92-)(and (Flag50-)))

(:derived (Flag92-)(and (Flag51-)))

(:derived (Flag92-)(and (Flag52-)))

(:derived (Flag92-)(and (Flag53-)))

(:derived (Flag92-)(and (Flag54-)))

(:derived (Flag92-)(and (Flag55-)))

(:derived (Flag92-)(and (Flag56-)))

(:derived (Flag92-)(and (Flag57-)))

(:derived (Flag92-)(and (Flag58-)))

(:derived (Flag92-)(and (Flag59-)))

(:derived (Flag92-)(and (Flag60-)))

(:derived (Flag92-)(and (Flag61-)))

(:derived (Flag92-)(and (Flag62-)))

(:derived (Flag92-)(and (Flag63-)))

(:derived (Flag92-)(and (Flag64-)))

(:derived (Flag92-)(and (Flag65-)))

(:derived (Flag92-)(and (Flag66-)))

(:derived (Flag92-)(and (Flag67-)))

(:derived (Flag92-)(and (Flag68-)))

(:derived (Flag92-)(and (Flag69-)))

(:derived (Flag92-)(and (Flag70-)))

(:derived (Flag92-)(and (Flag71-)))

(:derived (Flag92-)(and (Flag72-)))

(:derived (Flag92-)(and (Flag73-)))

(:derived (Flag92-)(and (Flag74-)))

(:derived (Flag92-)(and (Flag75-)))

(:derived (Flag92-)(and (Flag76-)))

(:derived (Flag92-)(and (Flag77-)))

(:derived (Flag92-)(and (Flag78-)))

(:derived (Flag92-)(and (Flag79-)))

(:derived (Flag92-)(and (Flag80-)))

(:derived (Flag92-)(and (Flag81-)))

(:derived (Flag92-)(and (Flag82-)))

(:derived (Flag92-)(and (Flag83-)))

(:derived (Flag92-)(and (Flag84-)))

(:derived (Flag92-)(and (Flag85-)))

(:derived (Flag92-)(and (Flag86-)))

(:derived (Flag92-)(and (Flag87-)))

(:derived (Flag92-)(and (Flag88-)))

(:derived (Flag92-)(and (Flag89-)))

(:derived (Flag92-)(and (Flag90-)))

(:derived (Flag92-)(and (Flag91-)))

(:derived (Flag183-)(and (Flag93-)))

(:derived (Flag183-)(and (Flag94-)))

(:derived (Flag183-)(and (Flag95-)))

(:derived (Flag183-)(and (Flag96-)))

(:derived (Flag183-)(and (Flag97-)))

(:derived (Flag183-)(and (Flag98-)))

(:derived (Flag183-)(and (Flag99-)))

(:derived (Flag183-)(and (Flag100-)))

(:derived (Flag183-)(and (Flag101-)))

(:derived (Flag183-)(and (Flag102-)))

(:derived (Flag183-)(and (Flag103-)))

(:derived (Flag183-)(and (Flag104-)))

(:derived (Flag183-)(and (Flag105-)))

(:derived (Flag183-)(and (Flag106-)))

(:derived (Flag183-)(and (Flag107-)))

(:derived (Flag183-)(and (Flag108-)))

(:derived (Flag183-)(and (Flag109-)))

(:derived (Flag183-)(and (Flag110-)))

(:derived (Flag183-)(and (Flag111-)))

(:derived (Flag183-)(and (Flag112-)))

(:derived (Flag183-)(and (Flag113-)))

(:derived (Flag183-)(and (Flag114-)))

(:derived (Flag183-)(and (Flag115-)))

(:derived (Flag183-)(and (Flag116-)))

(:derived (Flag183-)(and (Flag117-)))

(:derived (Flag183-)(and (Flag118-)))

(:derived (Flag183-)(and (Flag119-)))

(:derived (Flag183-)(and (Flag120-)))

(:derived (Flag183-)(and (Flag121-)))

(:derived (Flag183-)(and (Flag122-)))

(:derived (Flag183-)(and (Flag123-)))

(:derived (Flag183-)(and (Flag124-)))

(:derived (Flag183-)(and (Flag125-)))

(:derived (Flag183-)(and (Flag126-)))

(:derived (Flag183-)(and (Flag127-)))

(:derived (Flag183-)(and (Flag128-)))

(:derived (Flag183-)(and (Flag129-)))

(:derived (Flag183-)(and (Flag130-)))

(:derived (Flag183-)(and (Flag131-)))

(:derived (Flag183-)(and (Flag132-)))

(:derived (Flag183-)(and (Flag133-)))

(:derived (Flag183-)(and (Flag134-)))

(:derived (Flag183-)(and (Flag135-)))

(:derived (Flag183-)(and (Flag136-)))

(:derived (Flag183-)(and (Flag137-)))

(:derived (Flag183-)(and (Flag138-)))

(:derived (Flag183-)(and (Flag139-)))

(:derived (Flag183-)(and (Flag140-)))

(:derived (Flag183-)(and (Flag141-)))

(:derived (Flag183-)(and (Flag142-)))

(:derived (Flag183-)(and (Flag143-)))

(:derived (Flag183-)(and (Flag144-)))

(:derived (Flag183-)(and (Flag145-)))

(:derived (Flag183-)(and (Flag146-)))

(:derived (Flag183-)(and (Flag147-)))

(:derived (Flag183-)(and (Flag148-)))

(:derived (Flag183-)(and (Flag149-)))

(:derived (Flag183-)(and (Flag150-)))

(:derived (Flag183-)(and (Flag151-)))

(:derived (Flag183-)(and (Flag152-)))

(:derived (Flag183-)(and (Flag153-)))

(:derived (Flag183-)(and (Flag154-)))

(:derived (Flag183-)(and (Flag155-)))

(:derived (Flag183-)(and (Flag156-)))

(:derived (Flag183-)(and (Flag157-)))

(:derived (Flag183-)(and (Flag158-)))

(:derived (Flag183-)(and (Flag159-)))

(:derived (Flag183-)(and (Flag160-)))

(:derived (Flag183-)(and (Flag161-)))

(:derived (Flag183-)(and (Flag162-)))

(:derived (Flag183-)(and (Flag163-)))

(:derived (Flag183-)(and (Flag164-)))

(:derived (Flag183-)(and (Flag165-)))

(:derived (Flag183-)(and (Flag166-)))

(:derived (Flag183-)(and (Flag167-)))

(:derived (Flag183-)(and (Flag168-)))

(:derived (Flag183-)(and (Flag169-)))

(:derived (Flag183-)(and (Flag170-)))

(:derived (Flag183-)(and (Flag171-)))

(:derived (Flag183-)(and (Flag172-)))

(:derived (Flag183-)(and (Flag173-)))

(:derived (Flag183-)(and (Flag174-)))

(:derived (Flag183-)(and (Flag175-)))

(:derived (Flag183-)(and (Flag176-)))

(:derived (Flag183-)(and (Flag177-)))

(:derived (Flag183-)(and (Flag178-)))

(:derived (Flag183-)(and (Flag179-)))

(:derived (Flag183-)(and (Flag180-)))

(:derived (Flag183-)(and (Flag181-)))

(:derived (Flag183-)(and (Flag182-)))

(:derived (Flag275-)(and (Flag185-)))

(:derived (Flag275-)(and (Flag186-)))

(:derived (Flag275-)(and (Flag187-)))

(:derived (Flag275-)(and (Flag188-)))

(:derived (Flag275-)(and (Flag189-)))

(:derived (Flag275-)(and (Flag190-)))

(:derived (Flag275-)(and (Flag191-)))

(:derived (Flag275-)(and (Flag192-)))

(:derived (Flag275-)(and (Flag193-)))

(:derived (Flag275-)(and (Flag194-)))

(:derived (Flag275-)(and (Flag195-)))

(:derived (Flag275-)(and (Flag196-)))

(:derived (Flag275-)(and (Flag197-)))

(:derived (Flag275-)(and (Flag198-)))

(:derived (Flag275-)(and (Flag199-)))

(:derived (Flag275-)(and (Flag200-)))

(:derived (Flag275-)(and (Flag201-)))

(:derived (Flag275-)(and (Flag202-)))

(:derived (Flag275-)(and (Flag203-)))

(:derived (Flag275-)(and (Flag204-)))

(:derived (Flag275-)(and (Flag205-)))

(:derived (Flag275-)(and (Flag206-)))

(:derived (Flag275-)(and (Flag207-)))

(:derived (Flag275-)(and (Flag208-)))

(:derived (Flag275-)(and (Flag209-)))

(:derived (Flag275-)(and (Flag210-)))

(:derived (Flag275-)(and (Flag211-)))

(:derived (Flag275-)(and (Flag212-)))

(:derived (Flag275-)(and (Flag213-)))

(:derived (Flag275-)(and (Flag214-)))

(:derived (Flag275-)(and (Flag215-)))

(:derived (Flag275-)(and (Flag216-)))

(:derived (Flag275-)(and (Flag217-)))

(:derived (Flag275-)(and (Flag218-)))

(:derived (Flag275-)(and (Flag219-)))

(:derived (Flag275-)(and (Flag220-)))

(:derived (Flag275-)(and (Flag221-)))

(:derived (Flag275-)(and (Flag222-)))

(:derived (Flag275-)(and (Flag223-)))

(:derived (Flag275-)(and (Flag224-)))

(:derived (Flag275-)(and (Flag225-)))

(:derived (Flag275-)(and (Flag226-)))

(:derived (Flag275-)(and (Flag227-)))

(:derived (Flag275-)(and (Flag228-)))

(:derived (Flag275-)(and (Flag229-)))

(:derived (Flag275-)(and (Flag230-)))

(:derived (Flag275-)(and (Flag231-)))

(:derived (Flag275-)(and (Flag232-)))

(:derived (Flag275-)(and (Flag233-)))

(:derived (Flag275-)(and (Flag234-)))

(:derived (Flag275-)(and (Flag235-)))

(:derived (Flag275-)(and (Flag236-)))

(:derived (Flag275-)(and (Flag237-)))

(:derived (Flag275-)(and (Flag238-)))

(:derived (Flag275-)(and (Flag239-)))

(:derived (Flag275-)(and (Flag240-)))

(:derived (Flag275-)(and (Flag241-)))

(:derived (Flag275-)(and (Flag242-)))

(:derived (Flag275-)(and (Flag243-)))

(:derived (Flag275-)(and (Flag244-)))

(:derived (Flag275-)(and (Flag245-)))

(:derived (Flag275-)(and (Flag246-)))

(:derived (Flag275-)(and (Flag247-)))

(:derived (Flag275-)(and (Flag248-)))

(:derived (Flag275-)(and (Flag249-)))

(:derived (Flag275-)(and (Flag250-)))

(:derived (Flag275-)(and (Flag251-)))

(:derived (Flag275-)(and (Flag252-)))

(:derived (Flag275-)(and (Flag253-)))

(:derived (Flag275-)(and (Flag254-)))

(:derived (Flag275-)(and (Flag255-)))

(:derived (Flag275-)(and (Flag256-)))

(:derived (Flag275-)(and (Flag257-)))

(:derived (Flag275-)(and (Flag258-)))

(:derived (Flag275-)(and (Flag259-)))

(:derived (Flag275-)(and (Flag260-)))

(:derived (Flag275-)(and (Flag261-)))

(:derived (Flag275-)(and (Flag262-)))

(:derived (Flag275-)(and (Flag263-)))

(:derived (Flag275-)(and (Flag264-)))

(:derived (Flag275-)(and (Flag265-)))

(:derived (Flag275-)(and (Flag266-)))

(:derived (Flag275-)(and (Flag267-)))

(:derived (Flag275-)(and (Flag268-)))

(:derived (Flag275-)(and (Flag269-)))

(:derived (Flag275-)(and (Flag270-)))

(:derived (Flag275-)(and (Flag271-)))

(:derived (Flag275-)(and (Flag272-)))

(:derived (Flag275-)(and (Flag273-)))

(:derived (Flag275-)(and (Flag274-)))

(:action PICKUP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P7))
)
)
(:action PICKUP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P6))
)
)
(:action PICKUP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P5))
)
)
(:action PICKUP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P4))
)
)
(:action PICKUP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P3))
)
)
(:action PICKUP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P2))
)
)
(:action PICKUP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P1))
)
)
(:action PICKUP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P0))
)
)
(:action PICKUP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P7))
)
)
(:action PICKUP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P6))
)
)
(:action PICKUP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P5))
)
)
(:action PICKUP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P4))
)
)
(:action PICKUP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P3))
)
)
(:action PICKUP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P2))
)
)
(:action PICKUP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P1))
)
)
(:action PICKUP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P0))
)
)
(:action PICKUP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P7))
)
)
(:action PICKUP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P6))
)
)
(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
)
)
(:action PICKUP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P7))
)
)
(:action PICKUP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P6))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
)
)
(:action PICKUP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P7))
)
)
(:action PICKUP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P6))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
)
)
(:action PICKUP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P7))
)
)
(:action PICKUP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P6))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
)
)
(:action PICKUP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P7))
)
)
(:action PICKUP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P6))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
)
)
(:action PICKUP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P7))
)
)
(:action PICKUP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P6))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
)
)
(:action PICKUP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P7))
)
)
(:action PICKUP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P6))
)
)
(:action PICKUP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P5))
)
)
(:action PICKUP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P4))
)
)
(:action PICKUP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P3))
)
)
(:action PICKUP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P2))
)
)
(:action PICKUP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P1))
)
)
(:action PICKUP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P0))
)
)
(:action PICKUP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P7))
)
)
(:action PICKUP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P6))
)
)
(:action PICKUP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P5))
)
)
(:action PICKUP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P4))
)
)
(:action PICKUP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P3))
)
)
(:action PICKUP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P2))
)
)
(:action PICKUP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P1))
)
)
(:action PICKUP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P0))
)
)
(:action PICKUP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P7))
)
)
(:action PICKUP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P6))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
)
)
(:action PICKUP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P7))
)
)
(:action PICKUP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P6))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
)
)
(:action PICKUP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P7))
)
)
(:action PICKUP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P6))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
)
)
(:action PICKUP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P7))
)
)
(:action PICKUP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P6))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
)
)
(:action PICKUP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P7))
)
)
(:action PICKUP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P6))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
)
)
(:action PICKUP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P7))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
)
)
(:action PICKUP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P7))
)
)
(:action PICKUP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P6))
)
)
(:action PICKUP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P5))
)
)
(:action PICKUP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P4))
)
)
(:action PICKUP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P3))
)
)
(:action PICKUP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P2))
)
)
(:action PICKUP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P1))
)
)
(:action PICKUP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P0))
)
)
(:action PICKUP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P7))
)
)
(:action PICKUP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P6))
)
)
(:action PICKUP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P5))
)
)
(:action PICKUP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P4))
)
)
(:action PICKUP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P3))
)
)
(:action PICKUP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P2))
)
)
(:action PICKUP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P1))
)
)
(:action PICKUP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P0))
)
)
(:action PICKUP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P7))
)
)
(:action PICKUP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P6))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
)
)
(:action PICKUP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P7))
)
)
(:action PICKUP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P6))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
)
)
(:action PICKUP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P7))
)
)
(:action PICKUP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P6))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
)
)
(:action PICKUP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P7))
)
)
(:action PICKUP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P6))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
)
)
(:action PICKUP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P7))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
)
)
(:action PICKUP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P7))
)
)
(:action PICKUP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P6))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
)
)
(:action PICKUP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P7))
)
)
(:action PICKUP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P6))
)
)
(:action PICKUP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P5))
)
)
(:action PICKUP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P4))
)
)
(:action PICKUP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P3))
)
)
(:action PICKUP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P2))
)
)
(:action PICKUP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P1))
)
)
(:action PICKUP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P0))
)
)
(:action PICKUP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P7))
)
)
(:action PICKUP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P6))
)
)
(:action PICKUP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P5))
)
)
(:action PICKUP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P4))
)
)
(:action PICKUP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P3))
)
)
(:action PICKUP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P2))
)
)
(:action PICKUP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P1))
)
)
(:action PICKUP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P0))
)
)
(:action PICKUP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P7))
)
)
(:action PICKUP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P6))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
)
)
(:action PICKUP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P7))
)
)
(:action PICKUP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P6))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
)
)
(:action PICKUP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P7))
)
)
(:action PICKUP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P6))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
)
)
(:action PICKUP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P7))
)
)
(:action PICKUP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P6))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
)
)
(:action PICKUP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P7))
)
)
(:action PICKUP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P6))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
)
)
(:action PICKUP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P7))
)
)
(:action PICKUP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P6))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
)
)
(:action PICKUP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P7))
)
)
(:action PICKUP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P6))
)
)
(:action PICKUP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P5))
)
)
(:action PICKUP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P4))
)
)
(:action PICKUP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P3))
)
)
(:action PICKUP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P2))
)
)
(:action PICKUP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P1))
)
)
(:action PICKUP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P0))
)
)
(:action PICKUP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P7))
)
)
(:action PICKUP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P6))
)
)
(:action PICKUP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P5))
)
)
(:action PICKUP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P4))
)
)
(:action PICKUP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P3))
)
)
(:action PICKUP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P2))
)
)
(:action PICKUP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P1))
)
)
(:action PICKUP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P0))
)
)
(:action PICKUP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P6))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
)
)
(:action PICKUP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P7))
)
)
(:action PICKUP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P6))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
)
)
(:action PICKUP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P7))
)
)
(:action PICKUP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P6))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
)
)
(:action PICKUP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P7))
)
)
(:action PICKUP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P6))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
)
)
(:action PICKUP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P7))
)
)
(:action PICKUP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P6))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
)
)
(:action PICKUP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P7))
)
)
(:action PICKUP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P6))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
)
)
(:action PICKUP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P7))
)
)
(:action PICKUP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P6))
)
)
(:action PICKUP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P5))
)
)
(:action PICKUP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P4))
)
)
(:action PICKUP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P3))
)
)
(:action PICKUP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P2))
)
)
(:action PICKUP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P1))
)
)
(:action PICKUP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P0))
)
)
(:action PICKUP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P7))
)
)
(:action PICKUP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P6))
)
)
(:action PICKUP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P5))
)
)
(:action PICKUP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P4))
)
)
(:action PICKUP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P3))
)
)
(:action PICKUP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P2))
)
)
(:action PICKUP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P1))
)
)
(:action PICKUP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P0))
)
)
(:action PICKUP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P7))
)
)
(:action PICKUP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P6))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
)
)
(:action PICKUP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P7))
)
)
(:action PICKUP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P6))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
)
)
(:action PICKUP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P7))
)
)
(:action PICKUP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P6))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
)
)
(:action PICKUP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P7))
)
)
(:action PICKUP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P6))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
)
)
(:action PICKUP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P7))
)
)
(:action PICKUP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P6))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
)
)
(:action PICKUP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P7))
)
)
(:action PICKUP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P6))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
)
)
(:action PICKUP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P7))
)
)
(:action PICKUP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P6))
)
)
(:action PICKUP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P5))
)
)
(:action PICKUP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P4))
)
)
(:action PICKUP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P3))
)
)
(:action PICKUP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P2))
)
)
(:action PICKUP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P1))
)
)
(:action PICKUP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P0))
)
)
(:action PICKUP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P7))
)
)
(:action PICKUP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P6))
)
)
(:action PICKUP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P5))
)
)
(:action PICKUP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P4))
)
)
(:action PICKUP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P3))
)
)
(:action PICKUP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P2))
)
)
(:action PICKUP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P1))
)
)
(:action PICKUP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P0))
)
)
(:action PICKUP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P7))
)
)
(:action PICKUP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P6))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
)
)
(:action PICKUP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P7))
)
)
(:action PICKUP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P6))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
)
)
(:action PICKUP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P7))
)
)
(:action PICKUP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P6))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
)
)
(:action PICKUP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P7))
)
)
(:action PICKUP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P6))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
)
)
(:action PICKUP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P7))
)
)
(:action PICKUP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P6))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
)
)
(:action PICKUP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P7))
)
)
(:action PICKUP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P6))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
)
)
(:action PICKUP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P7))
)
)
(:action PICKUP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P6))
)
)
(:action PICKUP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P5))
)
)
(:action PICKUP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P4))
)
)
(:action PICKUP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P3))
)
)
(:action PICKUP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P2))
)
)
(:action PICKUP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P1))
)
)
(:action PICKUP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P0))
)
)
(:action PICKUP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P7))
)
)
(:action PICKUP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P6))
)
)
(:action PICKUP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P5))
)
)
(:action PICKUP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P4))
)
)
(:action PICKUP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P3))
)
)
(:action PICKUP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P2))
)
)
(:action PICKUP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P1))
)
)
(:action PICKUP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P0))
)
)
(:action PICKUP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P7))
)
)
(:action PICKUP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P6))
)
)
(:action PICKUP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P5))
)
)
(:action PICKUP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P4))
)
)
(:action PICKUP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P3))
)
)
(:action PICKUP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P2))
)
)
(:action PICKUP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P1))
)
)
(:action PICKUP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P0))
)
)
(:action PICKUP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P7))
)
)
(:action PICKUP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P6))
)
)
(:action PICKUP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P5))
)
)
(:action PICKUP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P4))
)
)
(:action PICKUP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P3))
)
)
(:action PICKUP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P2))
)
)
(:action PICKUP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P1))
)
)
(:action PICKUP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P0))
)
)
(:action PICKUP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P7))
)
)
(:action PICKUP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P6))
)
)
(:action PICKUP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P5))
)
)
(:action PICKUP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P4))
)
)
(:action PICKUP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P3))
)
)
(:action PICKUP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P2))
)
)
(:action PICKUP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P1))
)
)
(:action PICKUP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P0))
)
)
(:action PICKUP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P7))
)
)
(:action PICKUP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P6))
)
)
(:action PICKUP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P5))
)
)
(:action PICKUP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P4))
)
)
(:action PICKUP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P3))
)
)
(:action PICKUP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P2))
)
)
(:action PICKUP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P1))
)
)
(:action PICKUP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P7))
)
)
(:action PICKUP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P6))
)
)
(:action PICKUP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P5))
)
)
(:action PICKUP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P4))
)
)
(:action PICKUP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P3))
)
)
(:action PICKUP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P2))
)
)
(:action PICKUP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P1))
)
)
(:action PICKUP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P0))
)
)
(:action PICKUP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P7))
)
)
(:action PICKUP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P6))
)
)
(:action PICKUP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P5))
)
)
(:action PICKUP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P4))
)
)
(:action PICKUP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P3))
)
)
(:action PICKUP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P2))
)
)
(:action PICKUP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P1))
)
)
(:action PICKUP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P0))
)
)
(:action PICKUP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P7))
)
)
(:action PICKUP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P6))
)
)
(:action PICKUP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P5))
)
)
(:action PICKUP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P4))
)
)
(:action PICKUP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P3))
)
)
(:action PICKUP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P2))
)
)
(:action PICKUP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P1))
)
)
(:action PICKUP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P0))
)
)
(:action PICKUP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P7))
)
)
(:action PICKUP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P6))
)
)
(:action PICKUP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P5))
)
)
(:action PICKUP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P4))
)
)
(:action PICKUP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P3))
)
)
(:action PICKUP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P2))
)
)
(:action PICKUP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P1))
)
)
(:action PICKUP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P0))
)
)
(:action PICKUP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P7))
)
)
(:action PICKUP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P6))
)
)
(:action PICKUP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P5))
)
)
(:action PICKUP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P4))
)
)
(:action PICKUP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P3))
)
)
(:action PICKUP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P2))
)
)
(:action PICKUP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P1))
)
)
(:action PICKUP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P0))
)
)
(:action PICKUP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P7))
)
)
(:action PICKUP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P5))
)
)
(:action PICKUP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P4))
)
)
(:action PICKUP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P3))
)
)
(:action PICKUP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P2))
)
)
(:action PICKUP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P1))
)
)
(:action PICKUP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P0))
)
)
(:action PICKUP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P7))
)
)
(:action PICKUP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P6))
)
)
(:action PICKUP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P5))
)
)
(:action PICKUP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P4))
)
)
(:action PICKUP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P3))
)
)
(:action PICKUP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P2))
)
)
(:action PICKUP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P1))
)
)
(:action PICKUP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P0))
)
)
(:action PICKUP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P7))
)
)
(:action PICKUP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P6))
)
)
(:action PICKUP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P5))
)
)
(:action PICKUP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P4))
)
)
(:action PICKUP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P3))
)
)
(:action PICKUP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P2))
)
)
(:action PICKUP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P1))
)
)
(:action PICKUP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P0))
)
)
(:action PICKUP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P7))
)
)
(:action PICKUP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P6))
)
)
(:action PICKUP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P5))
)
)
(:action PICKUP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P4))
)
)
(:action PICKUP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P3))
)
)
(:action PICKUP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P2))
)
)
(:action PICKUP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P1))
)
)
(:action PICKUP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P0))
)
)
(:action PICKUP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P7))
)
)
(:action PICKUP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P6))
)
)
(:action PICKUP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P5))
)
)
(:action PICKUP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P4))
)
)
(:action PICKUP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P3))
)
)
(:action PICKUP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P2))
)
)
(:action PICKUP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P1))
)
)
(:action PICKUP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P0))
)
)
(:action PICKUP-O9-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P7))
)
)
(:action PICKUP-O9-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P6))
)
)
(:action PICKUP-O9-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P5))
)
)
(:action PICKUP-O9-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P3))
)
)
(:action PICKUP-O9-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P2))
)
)
(:action PICKUP-O9-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P1))
)
)
(:action PICKUP-O9-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P0))
)
)
(:action PICKUP-O9-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P7))
)
)
(:action PICKUP-O9-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P6))
)
)
(:action PICKUP-O9-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P5))
)
)
(:action PICKUP-O9-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P4))
)
)
(:action PICKUP-O9-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P3))
)
)
(:action PICKUP-O9-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P2))
)
)
(:action PICKUP-O9-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P1))
)
)
(:action PICKUP-O9-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P6-P0))
)
)
(:action PICKUP-O9-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P7))
)
)
(:action PICKUP-O9-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P6))
)
)
(:action PICKUP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P5))
)
)
(:action PICKUP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P4))
)
)
(:action PICKUP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P3))
)
)
(:action PICKUP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P2))
)
)
(:action PICKUP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P1))
)
)
(:action PICKUP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P5-P0))
)
)
(:action PICKUP-O9-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P7))
)
)
(:action PICKUP-O9-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P6))
)
)
(:action PICKUP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P5))
)
)
(:action PICKUP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P4))
)
)
(:action PICKUP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P3))
)
)
(:action PICKUP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P2))
)
)
(:action PICKUP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P1))
)
)
(:action PICKUP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P4-P0))
)
)
(:action PICKUP-O9-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P7))
)
)
(:action PICKUP-O9-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P6))
)
)
(:action PICKUP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P5))
)
)
(:action PICKUP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P4))
)
)
(:action PICKUP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P3))
)
)
(:action PICKUP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P2))
)
)
(:action PICKUP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P1))
)
)
(:action PICKUP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P3-P0))
)
)
(:action PICKUP-O9-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P7))
)
)
(:action PICKUP-O9-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P6))
)
)
(:action PICKUP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P5))
)
)
(:action PICKUP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P4))
)
)
(:action PICKUP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P3))
)
)
(:action PICKUP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P2))
)
)
(:action PICKUP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P1))
)
)
(:action PICKUP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P2-P0))
)
)
(:action PICKUP-O9-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P7))
)
)
(:action PICKUP-O9-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P6))
)
)
(:action PICKUP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P5))
)
)
(:action PICKUP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P4))
)
)
(:action PICKUP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P3))
)
)
(:action PICKUP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P2))
)
)
(:action PICKUP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P1))
)
)
(:action PICKUP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P1-P0))
)
)
(:action PICKUP-O9-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P7))
)
)
(:action PICKUP-O9-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P6))
)
)
(:action PICKUP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P5))
)
)
(:action PICKUP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P4))
)
)
(:action PICKUP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P3))
)
)
(:action PICKUP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P2))
)
)
(:action PICKUP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P1))
)
)
(:action PICKUP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P0-P0))
)
)
(:derived (Flag1-)(and (OBJ-AT-O0-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O1-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O2-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O3-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O4-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O5-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O6-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O7-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O8-P2-P5)))

(:derived (Flag1-)(and (OBJ-AT-O9-P2-P5)))

(:derived (Flag2-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag3-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag4-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag5-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag6-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag7-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag8-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag9-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag10-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag11-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag12-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag13-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag14-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag15-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag16-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag17-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag18-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag19-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag20-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag21-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag22-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag23-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag24-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag25-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag26-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag27-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag28-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag29-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag30-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag31-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag32-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag33-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag34-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag35-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag36-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag37-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag38-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag39-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag40-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag41-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag42-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag43-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag44-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag45-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag46-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag47-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag48-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag49-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag50-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag51-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag52-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag53-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag54-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag55-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag56-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag57-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag58-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag59-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag60-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag61-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag62-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag63-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag64-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag65-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag66-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag67-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag68-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag69-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag70-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag71-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag72-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag73-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag74-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag75-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag76-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag77-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag78-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag79-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag80-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag81-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag82-)(and (OBJ-AT-O9-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag83-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag84-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag85-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag86-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag87-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag88-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag89-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag90-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag91-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O9-P0-P2)))

(:derived (Flag93-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag94-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag95-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag96-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag97-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag98-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag99-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag100-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag101-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag102-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag103-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag104-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag105-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag106-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag107-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag108-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag109-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag110-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag111-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag112-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag113-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag114-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag115-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag116-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag117-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag118-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag119-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag120-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag121-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag122-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag123-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag124-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag125-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag126-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag127-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag128-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag129-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag130-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag131-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag132-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag133-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag134-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag135-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag136-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag137-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag138-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag139-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag140-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag141-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag142-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag143-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag144-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag145-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag146-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag147-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag148-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag149-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag150-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag151-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag152-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag153-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag154-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag155-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag156-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag157-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag158-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag159-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag160-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag161-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag162-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag163-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag164-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag165-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag166-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag167-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag168-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag169-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag170-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag171-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag172-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag173-)(and (OBJ-AT-O9-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag174-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag175-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag176-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag177-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag178-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag179-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag180-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag181-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag182-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O9-P5-P5)))

(:derived (Flag184-)(and (OBJ-AT-O0-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O2-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O3-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O4-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O5-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O6-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O7-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O8-P0-P6)))

(:derived (Flag184-)(and (OBJ-AT-O9-P0-P6)))

(:derived (Flag185-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag186-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag187-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag188-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag189-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag190-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag191-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag192-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag193-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag194-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag195-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag196-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag197-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag198-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag199-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag200-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag201-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag202-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag203-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag204-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag205-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag206-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag207-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag208-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag209-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag210-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag211-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag212-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag213-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag214-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag215-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag216-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag217-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag218-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag219-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag220-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag221-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag222-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag223-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag224-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag225-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag226-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag227-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag228-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag229-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag230-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag231-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag232-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag233-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag234-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag235-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag236-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag237-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag238-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag239-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag240-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag241-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag242-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag243-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag244-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag245-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag246-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag247-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag248-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag249-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag250-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag251-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag252-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag253-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag254-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag255-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag256-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag257-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag258-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag259-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag260-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag261-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag262-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag263-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag264-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag265-)(and (OBJ-AT-O9-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag266-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag267-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag268-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag269-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag270-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag271-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag272-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag273-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag274-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O9-P4-P1)))

(:derived (Flag276-)(and (OBJ-AT-O0-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O1-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O3-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O4-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O5-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O6-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O7-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O8-P1-P6)))

(:derived (Flag276-)(and (OBJ-AT-O9-P1-P6)))

(:derived (Flag277-)(and (OBJ-AT-O0-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O1-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O2-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O3-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O4-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O5-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O6-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O7-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O8-P0-P4)))

(:derived (Flag277-)(and (OBJ-AT-O9-P0-P4)))

(:action DROP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O0-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O0-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O0-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O0-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O0-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O0-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O0-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O0-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O0-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O0-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O0-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O0-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O0-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O0-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O0-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O0-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O0-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O0-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O0-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O0-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O0-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O0-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O0-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O0-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O0-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O0-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O0-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O0-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O1-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O1-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O1-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O1-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O1-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O1-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O1-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O1-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O1-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O1-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O1-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O1-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O1-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O1-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O1-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O1-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O1-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O1-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O1-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O1-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O1-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O1-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O1-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O1-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O1-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O1-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O1-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O1-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O2-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O2-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O2-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O2-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O2-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O2-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O2-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O2-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O2-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O2-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O2-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O2-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O2-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O2-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O2-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O2-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O2-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O2-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O2-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O2-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O2-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O2-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O2-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O2-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O2-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O2-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O2-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O2-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O3-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O3-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O3-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O3-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O3-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O3-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O3-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O3-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O3-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O3-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O3-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O3-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O3-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O3-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O3-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O3-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O3-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O3-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O3-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O3-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O3-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O3-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O3-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O3-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O3-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O3-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O3-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O3-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O4-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O4-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O4-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O4-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O4-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O4-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O4-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O4-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O4-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O4-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O4-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O4-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O4-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O4-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O4-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O4-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O4-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O4-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O4-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O4-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O4-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O4-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O4-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O4-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O4-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O4-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O4-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O4-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O5-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O5-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O5-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O5-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O5-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O5-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O5-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O5-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O5-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O5-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O5-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O5-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O5-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O5-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O5-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O5-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O5-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O5-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O5-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O5-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O5-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O5-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O5-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O5-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O5-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O5-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O5-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O5-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O6-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O6-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O6-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O6-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O6-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O6-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O6-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O6-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O6-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O6-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O6-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O6-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O6-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O6-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O6-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O6-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O6-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O6-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O6-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O6-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O6-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O6-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O6-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O6-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O6-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O6-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O6-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O6-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O7-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O7-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O7-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O7-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O7-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O7-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O7-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O7-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O7-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O7-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O7-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O7-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O7-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O7-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O7-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O7-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O7-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O7-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O7-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O7-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O7-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O7-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O7-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O7-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O7-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O7-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O7-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O7-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O7-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O7-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O7-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O7-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O7-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O7-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O7-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O7-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O7-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O7-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O7-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O7-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O7-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O7-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O7-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O7-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O7-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O7-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O7-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O7-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O7-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O7-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O7-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O7-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O7-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O7-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O7-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O7-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O7-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O7-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O7-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O7-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O7-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O7-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O7-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O7-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O8-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O8-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O8-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O8-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O8-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O8-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O8-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O8-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O8-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O8-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O8-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O8-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O8-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O8-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O8-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O8-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O8-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O8-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O8-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O8-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O8-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O8-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O8-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O8-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O8-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O8-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O8-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O8-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O8-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O8-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O8-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O8-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O8-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O8-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O8-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O8-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O8-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O8-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O8-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O8-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O8-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O8-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O8-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O8-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O8-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O8-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O8-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O8-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O8-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O8-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O8-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O8-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O8-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O8-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O8-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O8-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O8-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O8-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O8-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O8-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O8-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O8-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O8-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O8-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O9-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O9-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O9-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O9-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O9-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O9-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O9-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O9-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O9-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O9-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O9-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O9-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O9-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O9-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O9-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O9-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O9-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O9-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O9-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O9-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O9-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O9-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O9-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O9-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O9-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O9-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O9-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O9-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O9-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O9-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O9-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O9-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O9-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O9-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O9-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O9-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O9-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O9-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O9-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O9-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O9-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O9-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O9-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O9-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O9-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O9-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O9-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O9-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O9-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O9-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O9-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O9-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O9-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O9-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O9-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O9-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O9-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O9-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O9-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O9-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O9-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O9-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O9-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O9-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action DROP-O9-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O9)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O9-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O9))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
)
)
(:action PICKUP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P6))
)
)
(:action PICKUP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P6))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
)
)
(:action PICKUP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P7))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
)
)
(:action PICKUP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P0))
)
)
(:action PICKUP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P6))
)
)
(:action PICKUP-O9-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O9-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O9)
(CAPACITY-C0)
(not (OBJ-AT-O9-P7-P4))
)
)
(:action MOVE-P7-P7-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P6-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P5-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P4-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P3-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P2-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P1-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P0-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P7-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P6-P7-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P6-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P5-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P4-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P3-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P2-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P1-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P0-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P5-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P4-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P3-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P2-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P1-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P0-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P7-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
)
)
(:derived (Flag184-)(and (OBJ-AT-O1-P0-P6)))

(:derived (Flag276-)(and (OBJ-AT-O2-P1-P6)))

)
