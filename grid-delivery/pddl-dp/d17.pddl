(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P0-P6)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P0-P7)
(ROBOT-AT-P1-P6)
(ROBOT-AT-P1-P7)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P2-P6)
(ROBOT-AT-P2-P7)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P3-P6)
(ROBOT-AT-P3-P7)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P4-P6)
(ROBOT-AT-P4-P7)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(ROBOT-AT-P5-P6)
(ROBOT-AT-P5-P7)
(ROBOT-AT-P6-P0)
(ROBOT-AT-P6-P1)
(ROBOT-AT-P6-P2)
(ROBOT-AT-P6-P3)
(ROBOT-AT-P6-P4)
(ROBOT-AT-P6-P5)
(ROBOT-AT-P6-P6)
(ROBOT-AT-P6-P7)
(ROBOT-AT-P7-P0)
(ROBOT-AT-P7-P1)
(ROBOT-AT-P7-P2)
(ROBOT-AT-P7-P3)
(ROBOT-AT-P7-P4)
(ROBOT-AT-P7-P5)
(ROBOT-AT-P7-P6)
(ROBOT-AT-P7-P7)
(HOLDING-O6)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O3)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O6-P0-P0)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P0-P6)
(OBJ-AT-O6-P0-P7)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P1-P6)
(OBJ-AT-O6-P1-P7)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P2-P6)
(OBJ-AT-O6-P2-P7)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P3-P6)
(OBJ-AT-O6-P3-P7)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P4-P6)
(OBJ-AT-O6-P4-P7)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O6-P5-P6)
(OBJ-AT-O6-P5-P7)
(OBJ-AT-O6-P6-P0)
(OBJ-AT-O6-P6-P1)
(OBJ-AT-O6-P6-P2)
(OBJ-AT-O6-P6-P3)
(OBJ-AT-O6-P6-P4)
(OBJ-AT-O6-P6-P5)
(OBJ-AT-O6-P6-P6)
(OBJ-AT-O6-P6-P7)
(OBJ-AT-O6-P7-P0)
(OBJ-AT-O6-P7-P2)
(OBJ-AT-O6-P7-P3)
(OBJ-AT-O6-P7-P4)
(OBJ-AT-O6-P7-P5)
(OBJ-AT-O6-P7-P6)
(OBJ-AT-O6-P7-P7)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O5-P0-P6)
(OBJ-AT-O5-P0-P7)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P1-P6)
(OBJ-AT-O5-P1-P7)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P2-P6)
(OBJ-AT-O5-P2-P7)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P3-P6)
(OBJ-AT-O5-P3-P7)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P4-P6)
(OBJ-AT-O5-P4-P7)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O5-P5-P6)
(OBJ-AT-O5-P5-P7)
(OBJ-AT-O5-P6-P0)
(OBJ-AT-O5-P6-P1)
(OBJ-AT-O5-P6-P2)
(OBJ-AT-O5-P6-P3)
(OBJ-AT-O5-P6-P4)
(OBJ-AT-O5-P6-P5)
(OBJ-AT-O5-P6-P6)
(OBJ-AT-O5-P6-P7)
(OBJ-AT-O5-P7-P0)
(OBJ-AT-O5-P7-P1)
(OBJ-AT-O5-P7-P2)
(OBJ-AT-O5-P7-P3)
(OBJ-AT-O5-P7-P4)
(OBJ-AT-O5-P7-P5)
(OBJ-AT-O5-P7-P6)
(OBJ-AT-O5-P7-P7)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O4-P0-P6)
(OBJ-AT-O4-P0-P7)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P1-P6)
(OBJ-AT-O4-P1-P7)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O4-P2-P6)
(OBJ-AT-O4-P2-P7)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P3-P6)
(OBJ-AT-O4-P3-P7)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P4-P6)
(OBJ-AT-O4-P4-P7)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O4-P5-P6)
(OBJ-AT-O4-P5-P7)
(OBJ-AT-O4-P6-P0)
(OBJ-AT-O4-P6-P2)
(OBJ-AT-O4-P6-P3)
(OBJ-AT-O4-P6-P4)
(OBJ-AT-O4-P6-P5)
(OBJ-AT-O4-P6-P6)
(OBJ-AT-O4-P6-P7)
(OBJ-AT-O4-P7-P0)
(OBJ-AT-O4-P7-P1)
(OBJ-AT-O4-P7-P2)
(OBJ-AT-O4-P7-P3)
(OBJ-AT-O4-P7-P4)
(OBJ-AT-O4-P7-P5)
(OBJ-AT-O4-P7-P6)
(OBJ-AT-O4-P7-P7)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P0-P6)
(OBJ-AT-O3-P0-P7)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P1-P6)
(OBJ-AT-O3-P1-P7)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P2-P6)
(OBJ-AT-O3-P2-P7)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P3-P6)
(OBJ-AT-O3-P3-P7)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P4-P6)
(OBJ-AT-O3-P4-P7)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O3-P5-P6)
(OBJ-AT-O3-P5-P7)
(OBJ-AT-O3-P6-P0)
(OBJ-AT-O3-P6-P2)
(OBJ-AT-O3-P6-P3)
(OBJ-AT-O3-P6-P4)
(OBJ-AT-O3-P6-P5)
(OBJ-AT-O3-P6-P6)
(OBJ-AT-O3-P6-P7)
(OBJ-AT-O3-P7-P0)
(OBJ-AT-O3-P7-P1)
(OBJ-AT-O3-P7-P2)
(OBJ-AT-O3-P7-P3)
(OBJ-AT-O3-P7-P4)
(OBJ-AT-O3-P7-P5)
(OBJ-AT-O3-P7-P6)
(OBJ-AT-O3-P7-P7)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P0-P6)
(OBJ-AT-O2-P0-P7)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P1-P6)
(OBJ-AT-O2-P1-P7)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P2-P6)
(OBJ-AT-O2-P2-P7)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P3-P6)
(OBJ-AT-O2-P3-P7)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P4-P6)
(OBJ-AT-O2-P4-P7)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O2-P5-P6)
(OBJ-AT-O2-P5-P7)
(OBJ-AT-O2-P6-P0)
(OBJ-AT-O2-P6-P1)
(OBJ-AT-O2-P6-P2)
(OBJ-AT-O2-P6-P3)
(OBJ-AT-O2-P6-P4)
(OBJ-AT-O2-P6-P5)
(OBJ-AT-O2-P6-P6)
(OBJ-AT-O2-P6-P7)
(OBJ-AT-O2-P7-P0)
(OBJ-AT-O2-P7-P1)
(OBJ-AT-O2-P7-P2)
(OBJ-AT-O2-P7-P3)
(OBJ-AT-O2-P7-P4)
(OBJ-AT-O2-P7-P5)
(OBJ-AT-O2-P7-P6)
(OBJ-AT-O2-P7-P7)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O1-P0-P6)
(OBJ-AT-O1-P0-P7)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P1-P6)
(OBJ-AT-O1-P1-P7)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O1-P2-P6)
(OBJ-AT-O1-P2-P7)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P3-P6)
(OBJ-AT-O1-P3-P7)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P4-P6)
(OBJ-AT-O1-P4-P7)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O1-P5-P6)
(OBJ-AT-O1-P5-P7)
(OBJ-AT-O1-P6-P0)
(OBJ-AT-O1-P6-P1)
(OBJ-AT-O1-P6-P2)
(OBJ-AT-O1-P6-P3)
(OBJ-AT-O1-P6-P4)
(OBJ-AT-O1-P6-P5)
(OBJ-AT-O1-P6-P6)
(OBJ-AT-O1-P6-P7)
(OBJ-AT-O1-P7-P0)
(OBJ-AT-O1-P7-P1)
(OBJ-AT-O1-P7-P2)
(OBJ-AT-O1-P7-P3)
(OBJ-AT-O1-P7-P4)
(OBJ-AT-O1-P7-P5)
(OBJ-AT-O1-P7-P6)
(OBJ-AT-O1-P7-P7)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P0-P6)
(OBJ-AT-O0-P0-P7)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P1-P6)
(OBJ-AT-O0-P1-P7)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O0-P2-P6)
(OBJ-AT-O0-P2-P7)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P3-P6)
(OBJ-AT-O0-P3-P7)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P4-P6)
(OBJ-AT-O0-P4-P7)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(OBJ-AT-O0-P5-P6)
(OBJ-AT-O0-P5-P7)
(OBJ-AT-O0-P6-P0)
(OBJ-AT-O0-P6-P1)
(OBJ-AT-O0-P6-P2)
(OBJ-AT-O0-P6-P3)
(OBJ-AT-O0-P6-P4)
(OBJ-AT-O0-P6-P5)
(OBJ-AT-O0-P6-P6)
(OBJ-AT-O0-P6-P7)
(OBJ-AT-O0-P7-P0)
(OBJ-AT-O0-P7-P1)
(OBJ-AT-O0-P7-P2)
(OBJ-AT-O0-P7-P3)
(OBJ-AT-O0-P7-P4)
(OBJ-AT-O0-P7-P5)
(OBJ-AT-O0-P7-P6)
(OBJ-AT-O0-P7-P7)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag89-)
(Flag46-)
(Flag90-)
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O3-P6-P1)
(OBJ-AT-O4-P6-P1)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O6-P7-P1)
(ROBOT-AT-P0-P0)
)
(:derived (Flag90-)(and (Flag1-)(Flag2-)(Flag3-)(Flag46-)(Flag89-)))

(:derived (Flag46-)(and (Flag4-)))

(:derived (Flag46-)(and (Flag5-)))

(:derived (Flag46-)(and (Flag6-)))

(:derived (Flag46-)(and (Flag7-)))

(:derived (Flag46-)(and (Flag8-)))

(:derived (Flag46-)(and (Flag9-)))

(:derived (Flag46-)(and (Flag10-)))

(:derived (Flag46-)(and (Flag11-)))

(:derived (Flag46-)(and (Flag12-)))

(:derived (Flag46-)(and (Flag13-)))

(:derived (Flag46-)(and (Flag14-)))

(:derived (Flag46-)(and (Flag15-)))

(:derived (Flag46-)(and (Flag16-)))

(:derived (Flag46-)(and (Flag17-)))

(:derived (Flag46-)(and (Flag18-)))

(:derived (Flag46-)(and (Flag19-)))

(:derived (Flag46-)(and (Flag20-)))

(:derived (Flag46-)(and (Flag21-)))

(:derived (Flag46-)(and (Flag22-)))

(:derived (Flag46-)(and (Flag23-)))

(:derived (Flag46-)(and (Flag24-)))

(:derived (Flag46-)(and (Flag25-)))

(:derived (Flag46-)(and (Flag26-)))

(:derived (Flag46-)(and (Flag27-)))

(:derived (Flag46-)(and (Flag28-)))

(:derived (Flag46-)(and (Flag29-)))

(:derived (Flag46-)(and (Flag30-)))

(:derived (Flag46-)(and (Flag31-)))

(:derived (Flag46-)(and (Flag32-)))

(:derived (Flag46-)(and (Flag33-)))

(:derived (Flag46-)(and (Flag34-)))

(:derived (Flag46-)(and (Flag35-)))

(:derived (Flag46-)(and (Flag36-)))

(:derived (Flag46-)(and (Flag37-)))

(:derived (Flag46-)(and (Flag38-)))

(:derived (Flag46-)(and (Flag39-)))

(:derived (Flag46-)(and (Flag40-)))

(:derived (Flag46-)(and (Flag41-)))

(:derived (Flag46-)(and (Flag42-)))

(:derived (Flag46-)(and (Flag43-)))

(:derived (Flag46-)(and (Flag44-)))

(:derived (Flag46-)(and (Flag45-)))

(:derived (Flag89-)(and (Flag47-)))

(:derived (Flag89-)(and (Flag48-)))

(:derived (Flag89-)(and (Flag49-)))

(:derived (Flag89-)(and (Flag50-)))

(:derived (Flag89-)(and (Flag51-)))

(:derived (Flag89-)(and (Flag52-)))

(:derived (Flag89-)(and (Flag53-)))

(:derived (Flag89-)(and (Flag54-)))

(:derived (Flag89-)(and (Flag55-)))

(:derived (Flag89-)(and (Flag56-)))

(:derived (Flag89-)(and (Flag57-)))

(:derived (Flag89-)(and (Flag58-)))

(:derived (Flag89-)(and (Flag59-)))

(:derived (Flag89-)(and (Flag60-)))

(:derived (Flag89-)(and (Flag61-)))

(:derived (Flag89-)(and (Flag62-)))

(:derived (Flag89-)(and (Flag63-)))

(:derived (Flag89-)(and (Flag64-)))

(:derived (Flag89-)(and (Flag65-)))

(:derived (Flag89-)(and (Flag66-)))

(:derived (Flag89-)(and (Flag67-)))

(:derived (Flag89-)(and (Flag68-)))

(:derived (Flag89-)(and (Flag69-)))

(:derived (Flag89-)(and (Flag70-)))

(:derived (Flag89-)(and (Flag71-)))

(:derived (Flag89-)(and (Flag72-)))

(:derived (Flag89-)(and (Flag73-)))

(:derived (Flag89-)(and (Flag74-)))

(:derived (Flag89-)(and (Flag75-)))

(:derived (Flag89-)(and (Flag76-)))

(:derived (Flag89-)(and (Flag77-)))

(:derived (Flag89-)(and (Flag78-)))

(:derived (Flag89-)(and (Flag79-)))

(:derived (Flag89-)(and (Flag80-)))

(:derived (Flag89-)(and (Flag81-)))

(:derived (Flag89-)(and (Flag82-)))

(:derived (Flag89-)(and (Flag83-)))

(:derived (Flag89-)(and (Flag84-)))

(:derived (Flag89-)(and (Flag85-)))

(:derived (Flag89-)(and (Flag86-)))

(:derived (Flag89-)(and (Flag87-)))

(:derived (Flag89-)(and (Flag88-)))

(:action PICKUP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P7))
)
)
(:action PICKUP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P6))
)
)
(:action PICKUP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P5))
)
)
(:action PICKUP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P4))
)
)
(:action PICKUP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P3))
)
)
(:action PICKUP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P2))
)
)
(:action PICKUP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P1))
)
)
(:action PICKUP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P0))
)
)
(:action PICKUP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P7))
)
)
(:action PICKUP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P6))
)
)
(:action PICKUP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P5))
)
)
(:action PICKUP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P4))
)
)
(:action PICKUP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P3))
)
)
(:action PICKUP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P2))
)
)
(:action PICKUP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P1))
)
)
(:action PICKUP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P0))
)
)
(:action PICKUP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P7))
)
)
(:action PICKUP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P6))
)
)
(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
)
)
(:action PICKUP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P7))
)
)
(:action PICKUP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P6))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
)
)
(:action PICKUP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P7))
)
)
(:action PICKUP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P6))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
)
)
(:action PICKUP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P7))
)
)
(:action PICKUP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P6))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
)
)
(:action PICKUP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P7))
)
)
(:action PICKUP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P6))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
)
)
(:action PICKUP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P7))
)
)
(:action PICKUP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P6))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
)
)
(:action PICKUP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P7))
)
)
(:action PICKUP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P6))
)
)
(:action PICKUP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P5))
)
)
(:action PICKUP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P4))
)
)
(:action PICKUP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P3))
)
)
(:action PICKUP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P2))
)
)
(:action PICKUP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P1))
)
)
(:action PICKUP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P0))
)
)
(:action PICKUP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P7))
)
)
(:action PICKUP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P6))
)
)
(:action PICKUP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P5))
)
)
(:action PICKUP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P4))
)
)
(:action PICKUP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P3))
)
)
(:action PICKUP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P2))
)
)
(:action PICKUP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P1))
)
)
(:action PICKUP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P0))
)
)
(:action PICKUP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P7))
)
)
(:action PICKUP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P6))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
)
)
(:action PICKUP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P7))
)
)
(:action PICKUP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P6))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
)
)
(:action PICKUP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P7))
)
)
(:action PICKUP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P6))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
)
)
(:action PICKUP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P7))
)
)
(:action PICKUP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P6))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
)
)
(:action PICKUP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P7))
)
)
(:action PICKUP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P6))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
)
)
(:action PICKUP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P7))
)
)
(:action PICKUP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P6))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
)
)
(:action PICKUP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P7))
)
)
(:action PICKUP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P6))
)
)
(:action PICKUP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P5))
)
)
(:action PICKUP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P4))
)
)
(:action PICKUP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P3))
)
)
(:action PICKUP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P2))
)
)
(:action PICKUP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P1))
)
)
(:action PICKUP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P0))
)
)
(:action PICKUP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P7))
)
)
(:action PICKUP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P6))
)
)
(:action PICKUP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P5))
)
)
(:action PICKUP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P4))
)
)
(:action PICKUP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P3))
)
)
(:action PICKUP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P2))
)
)
(:action PICKUP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P1))
)
)
(:action PICKUP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P0))
)
)
(:action PICKUP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P7))
)
)
(:action PICKUP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P6))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
)
)
(:action PICKUP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P7))
)
)
(:action PICKUP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P6))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
)
)
(:action PICKUP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P7))
)
)
(:action PICKUP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P6))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
)
)
(:action PICKUP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P7))
)
)
(:action PICKUP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P6))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
)
)
(:action PICKUP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P7))
)
)
(:action PICKUP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P6))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
)
)
(:action PICKUP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P7))
)
)
(:action PICKUP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P6))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
)
)
(:action PICKUP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P7))
)
)
(:action PICKUP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P6))
)
)
(:action PICKUP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P5))
)
)
(:action PICKUP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P4))
)
)
(:action PICKUP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P3))
)
)
(:action PICKUP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P2))
)
)
(:action PICKUP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P1))
)
)
(:action PICKUP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P0))
)
)
(:action PICKUP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P7))
)
)
(:action PICKUP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P6))
)
)
(:action PICKUP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P5))
)
)
(:action PICKUP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P4))
)
)
(:action PICKUP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P3))
)
)
(:action PICKUP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P2))
)
)
(:action PICKUP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P0))
)
)
(:action PICKUP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P7))
)
)
(:action PICKUP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P6))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
)
)
(:action PICKUP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P7))
)
)
(:action PICKUP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P6))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
)
)
(:action PICKUP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P7))
)
)
(:action PICKUP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P6))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
)
)
(:action PICKUP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P7))
)
)
(:action PICKUP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P6))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
)
)
(:action PICKUP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P7))
)
)
(:action PICKUP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P6))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
)
)
(:action PICKUP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P7))
)
)
(:action PICKUP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P6))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
)
)
(:action PICKUP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P7))
)
)
(:action PICKUP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P6))
)
)
(:action PICKUP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P5))
)
)
(:action PICKUP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P4))
)
)
(:action PICKUP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P3))
)
)
(:action PICKUP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P2))
)
)
(:action PICKUP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P1))
)
)
(:action PICKUP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P0))
)
)
(:action PICKUP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P7))
)
)
(:action PICKUP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P6))
)
)
(:action PICKUP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P5))
)
)
(:action PICKUP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P4))
)
)
(:action PICKUP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P3))
)
)
(:action PICKUP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P2))
)
)
(:action PICKUP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P0))
)
)
(:action PICKUP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P7))
)
)
(:action PICKUP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P6))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
)
)
(:action PICKUP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P7))
)
)
(:action PICKUP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P6))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
)
)
(:action PICKUP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P7))
)
)
(:action PICKUP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P6))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
)
)
(:action PICKUP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P7))
)
)
(:action PICKUP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P6))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
)
)
(:action PICKUP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P7))
)
)
(:action PICKUP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P6))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
)
)
(:action PICKUP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P7))
)
)
(:action PICKUP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P6))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
)
)
(:action PICKUP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P7))
)
)
(:action PICKUP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P6))
)
)
(:action PICKUP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P5))
)
)
(:action PICKUP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P4))
)
)
(:action PICKUP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P3))
)
)
(:action PICKUP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P2))
)
)
(:action PICKUP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P1))
)
)
(:action PICKUP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P0))
)
)
(:action PICKUP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P7))
)
)
(:action PICKUP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P6))
)
)
(:action PICKUP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P5))
)
)
(:action PICKUP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P4))
)
)
(:action PICKUP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P3))
)
)
(:action PICKUP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P2))
)
)
(:action PICKUP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P1))
)
)
(:action PICKUP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P0))
)
)
(:action PICKUP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P7))
)
)
(:action PICKUP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P6))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
)
)
(:action PICKUP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P7))
)
)
(:action PICKUP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P6))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
)
)
(:action PICKUP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P7))
)
)
(:action PICKUP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P6))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
)
)
(:action PICKUP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P7))
)
)
(:action PICKUP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P6))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
)
)
(:action PICKUP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P7))
)
)
(:action PICKUP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P6))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
)
)
(:action PICKUP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P7))
)
)
(:action PICKUP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P6))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
)
)
(:action PICKUP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P7))
)
)
(:action PICKUP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P6))
)
)
(:action PICKUP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P5))
)
)
(:action PICKUP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P4))
)
)
(:action PICKUP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P3))
)
)
(:action PICKUP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P2))
)
)
(:action PICKUP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P0))
)
)
(:action PICKUP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P7))
)
)
(:action PICKUP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P6))
)
)
(:action PICKUP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P5))
)
)
(:action PICKUP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P4))
)
)
(:action PICKUP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P3))
)
)
(:action PICKUP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P2))
)
)
(:action PICKUP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P1))
)
)
(:action PICKUP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P0))
)
)
(:action PICKUP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P7))
)
)
(:action PICKUP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P6))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
)
)
(:action PICKUP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P7))
)
)
(:action PICKUP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P6))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
)
)
(:action PICKUP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P7))
)
)
(:action PICKUP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P6))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
)
)
(:action PICKUP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P7))
)
)
(:action PICKUP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P6))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
)
)
(:action PICKUP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P7))
)
)
(:action PICKUP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P6))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
)
)
(:action PICKUP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P7))
)
)
(:action PICKUP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P6))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
)
)
(:derived (Flag1-)(and (OBJ-AT-O0-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O1-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O2-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O3-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O4-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O5-P5-P5)))

(:derived (Flag1-)(and (OBJ-AT-O6-P5-P5)))

(:derived (Flag2-)(and (OBJ-AT-O0-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O1-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O2-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O3-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O4-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O5-P0-P6)))

(:derived (Flag2-)(and (OBJ-AT-O6-P0-P6)))

(:derived (Flag3-)(and (OBJ-AT-O0-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O1-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O2-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O3-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O4-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O5-P4-P1)))

(:derived (Flag3-)(and (OBJ-AT-O6-P4-P1)))

(:derived (Flag4-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag5-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag6-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag7-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag8-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag9-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag10-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag11-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag12-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag13-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag14-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag15-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag16-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag17-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag18-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag19-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag20-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag21-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag22-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag23-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag24-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag25-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag26-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag27-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag28-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag29-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag30-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag31-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag32-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag33-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag34-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag35-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag36-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag37-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag38-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag39-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag40-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag41-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag42-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag43-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag44-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag45-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag47-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag48-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag49-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag50-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag51-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag52-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O0-P0-P4)))

(:derived (Flag53-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag54-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag55-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag56-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag57-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag58-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O1-P0-P4)))

(:derived (Flag59-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag60-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag61-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag62-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag63-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag64-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O2-P0-P4)))

(:derived (Flag65-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag66-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag67-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag68-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag69-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag70-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O3-P0-P4)))

(:derived (Flag71-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag72-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag73-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag74-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag75-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag76-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O4-P0-P4)))

(:derived (Flag77-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag78-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag79-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag80-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag81-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag82-)(and (OBJ-AT-O6-P0-P4)(OBJ-AT-O5-P0-P4)))

(:derived (Flag83-)(and (OBJ-AT-O0-P0-P4)(OBJ-AT-O6-P0-P4)))

(:derived (Flag84-)(and (OBJ-AT-O1-P0-P4)(OBJ-AT-O6-P0-P4)))

(:derived (Flag85-)(and (OBJ-AT-O2-P0-P4)(OBJ-AT-O6-P0-P4)))

(:derived (Flag86-)(and (OBJ-AT-O3-P0-P4)(OBJ-AT-O6-P0-P4)))

(:derived (Flag87-)(and (OBJ-AT-O4-P0-P4)(OBJ-AT-O6-P0-P4)))

(:derived (Flag88-)(and (OBJ-AT-O5-P0-P4)(OBJ-AT-O6-P0-P4)))

(:action DROP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O0-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O0-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O0-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O0-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O0-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O0-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O0-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O0-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O0-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O0-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O0-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O0-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O0-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O0-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O0-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O0-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O0-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O0-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O0-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O0-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O0-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O0-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O0-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O0-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O0-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O0-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O0-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O0-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O1-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O1-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O1-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O1-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O1-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O1-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O1-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O1-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O1-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O1-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O1-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O1-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O1-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O1-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O1-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O1-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O1-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O1-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O1-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O1-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O1-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O1-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O1-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O1-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O1-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O1-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O1-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O1-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O2-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O2-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O2-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O2-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O2-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O2-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O2-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O2-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O2-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O2-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O2-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O2-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O2-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O2-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O2-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O2-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O2-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O2-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O2-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O2-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O2-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O2-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O2-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O2-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O2-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O2-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O2-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O2-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O3-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O3-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O3-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O3-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O3-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O3-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O3-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O3-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O3-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O3-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O3-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O3-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O3-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O3-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O3-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O3-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O3-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O3-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O3-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O3-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O3-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O3-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O3-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O3-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O3-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O3-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O3-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O3-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O4-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O4-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O4-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O4-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O4-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O4-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O4-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O4-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O4-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O4-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O4-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O4-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O4-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O4-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O4-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O4-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O4-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O4-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O4-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O4-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O4-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O4-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O4-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O4-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O4-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O4-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O4-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O4-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O5-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O5-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O5-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O5-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O5-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O5-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O5-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O5-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O5-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O5-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O5-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O5-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O5-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O5-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O5-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O5-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O5-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O5-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O5-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O5-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O5-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O5-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O5-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O5-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O5-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O5-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O5-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O5-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O6-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O6-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O6-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O6-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O6-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O6-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O6-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O6-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O6-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O6-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O6-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O6-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O6-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O6-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O6-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O6-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O6-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O6-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O6-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O6-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O6-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O6-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O6-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O6-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O6-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O6-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O6-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O6-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
)
)
(:action PICKUP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P1))
)
)
(:action PICKUP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P1))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
)
)
(:action PICKUP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P1))
)
)
(:action MOVE-P7-P7-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P6-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P5-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P4-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P3-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P2-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P1-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P0-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P7-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P6-P7-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P6-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P5-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P4-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P3-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P2-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P1-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P0-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P5-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P4-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P3-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P2-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P1-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P0-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P7-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
)
)
)
