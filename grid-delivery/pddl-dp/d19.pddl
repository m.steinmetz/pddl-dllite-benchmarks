(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(Flag222-)
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P0-P6)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P0-P7)
(ROBOT-AT-P1-P6)
(ROBOT-AT-P1-P7)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P2-P6)
(ROBOT-AT-P2-P7)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P3-P6)
(ROBOT-AT-P3-P7)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P4-P6)
(ROBOT-AT-P4-P7)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(ROBOT-AT-P5-P6)
(ROBOT-AT-P5-P7)
(ROBOT-AT-P6-P0)
(ROBOT-AT-P6-P1)
(ROBOT-AT-P6-P2)
(ROBOT-AT-P6-P3)
(ROBOT-AT-P6-P4)
(ROBOT-AT-P6-P5)
(ROBOT-AT-P6-P6)
(ROBOT-AT-P6-P7)
(ROBOT-AT-P7-P0)
(ROBOT-AT-P7-P1)
(ROBOT-AT-P7-P2)
(ROBOT-AT-P7-P3)
(ROBOT-AT-P7-P4)
(ROBOT-AT-P7-P5)
(ROBOT-AT-P7-P6)
(ROBOT-AT-P7-P7)
(HOLDING-O8)
(HOLDING-O7)
(HOLDING-O6)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O3)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O8-P0-P0)
(OBJ-AT-O8-P0-P1)
(OBJ-AT-O8-P0-P2)
(OBJ-AT-O8-P0-P3)
(OBJ-AT-O8-P0-P5)
(OBJ-AT-O8-P0-P6)
(OBJ-AT-O8-P0-P7)
(OBJ-AT-O8-P1-P0)
(OBJ-AT-O8-P1-P1)
(OBJ-AT-O8-P1-P2)
(OBJ-AT-O8-P1-P3)
(OBJ-AT-O8-P1-P4)
(OBJ-AT-O8-P1-P5)
(OBJ-AT-O8-P1-P6)
(OBJ-AT-O8-P1-P7)
(OBJ-AT-O8-P2-P0)
(OBJ-AT-O8-P2-P1)
(OBJ-AT-O8-P2-P2)
(OBJ-AT-O8-P2-P3)
(OBJ-AT-O8-P2-P4)
(OBJ-AT-O8-P2-P5)
(OBJ-AT-O8-P2-P6)
(OBJ-AT-O8-P2-P7)
(OBJ-AT-O8-P3-P0)
(OBJ-AT-O8-P3-P1)
(OBJ-AT-O8-P3-P2)
(OBJ-AT-O8-P3-P3)
(OBJ-AT-O8-P3-P4)
(OBJ-AT-O8-P3-P5)
(OBJ-AT-O8-P3-P6)
(OBJ-AT-O8-P3-P7)
(OBJ-AT-O8-P4-P0)
(OBJ-AT-O8-P4-P1)
(OBJ-AT-O8-P4-P2)
(OBJ-AT-O8-P4-P3)
(OBJ-AT-O8-P4-P4)
(OBJ-AT-O8-P4-P5)
(OBJ-AT-O8-P4-P6)
(OBJ-AT-O8-P4-P7)
(OBJ-AT-O8-P5-P0)
(OBJ-AT-O8-P5-P1)
(OBJ-AT-O8-P5-P2)
(OBJ-AT-O8-P5-P3)
(OBJ-AT-O8-P5-P4)
(OBJ-AT-O8-P5-P5)
(OBJ-AT-O8-P5-P6)
(OBJ-AT-O8-P5-P7)
(OBJ-AT-O8-P6-P0)
(OBJ-AT-O8-P6-P1)
(OBJ-AT-O8-P6-P2)
(OBJ-AT-O8-P6-P3)
(OBJ-AT-O8-P6-P4)
(OBJ-AT-O8-P6-P5)
(OBJ-AT-O8-P6-P6)
(OBJ-AT-O8-P6-P7)
(OBJ-AT-O8-P7-P0)
(OBJ-AT-O8-P7-P1)
(OBJ-AT-O8-P7-P2)
(OBJ-AT-O8-P7-P3)
(OBJ-AT-O8-P7-P4)
(OBJ-AT-O8-P7-P5)
(OBJ-AT-O8-P7-P6)
(OBJ-AT-O8-P7-P7)
(OBJ-AT-O7-P0-P0)
(OBJ-AT-O7-P0-P1)
(OBJ-AT-O7-P0-P2)
(OBJ-AT-O7-P0-P3)
(OBJ-AT-O7-P0-P4)
(OBJ-AT-O7-P0-P5)
(OBJ-AT-O7-P0-P6)
(OBJ-AT-O7-P0-P7)
(OBJ-AT-O7-P1-P0)
(OBJ-AT-O7-P1-P1)
(OBJ-AT-O7-P1-P2)
(OBJ-AT-O7-P1-P3)
(OBJ-AT-O7-P1-P4)
(OBJ-AT-O7-P1-P5)
(OBJ-AT-O7-P1-P6)
(OBJ-AT-O7-P1-P7)
(OBJ-AT-O7-P2-P0)
(OBJ-AT-O7-P2-P1)
(OBJ-AT-O7-P2-P3)
(OBJ-AT-O7-P2-P4)
(OBJ-AT-O7-P2-P5)
(OBJ-AT-O7-P2-P6)
(OBJ-AT-O7-P2-P7)
(OBJ-AT-O7-P3-P0)
(OBJ-AT-O7-P3-P1)
(OBJ-AT-O7-P3-P2)
(OBJ-AT-O7-P3-P3)
(OBJ-AT-O7-P3-P4)
(OBJ-AT-O7-P3-P5)
(OBJ-AT-O7-P3-P6)
(OBJ-AT-O7-P3-P7)
(OBJ-AT-O7-P4-P0)
(OBJ-AT-O7-P4-P1)
(OBJ-AT-O7-P4-P2)
(OBJ-AT-O7-P4-P3)
(OBJ-AT-O7-P4-P4)
(OBJ-AT-O7-P4-P5)
(OBJ-AT-O7-P4-P6)
(OBJ-AT-O7-P4-P7)
(OBJ-AT-O7-P5-P0)
(OBJ-AT-O7-P5-P1)
(OBJ-AT-O7-P5-P2)
(OBJ-AT-O7-P5-P3)
(OBJ-AT-O7-P5-P4)
(OBJ-AT-O7-P5-P5)
(OBJ-AT-O7-P5-P6)
(OBJ-AT-O7-P5-P7)
(OBJ-AT-O7-P6-P0)
(OBJ-AT-O7-P6-P1)
(OBJ-AT-O7-P6-P2)
(OBJ-AT-O7-P6-P3)
(OBJ-AT-O7-P6-P4)
(OBJ-AT-O7-P6-P5)
(OBJ-AT-O7-P6-P6)
(OBJ-AT-O7-P6-P7)
(OBJ-AT-O7-P7-P0)
(OBJ-AT-O7-P7-P1)
(OBJ-AT-O7-P7-P2)
(OBJ-AT-O7-P7-P3)
(OBJ-AT-O7-P7-P4)
(OBJ-AT-O7-P7-P5)
(OBJ-AT-O7-P7-P6)
(OBJ-AT-O7-P7-P7)
(OBJ-AT-O6-P0-P0)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P0-P6)
(OBJ-AT-O6-P0-P7)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P1-P6)
(OBJ-AT-O6-P1-P7)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P2-P6)
(OBJ-AT-O6-P2-P7)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P3-P6)
(OBJ-AT-O6-P3-P7)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P4-P6)
(OBJ-AT-O6-P4-P7)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O6-P5-P6)
(OBJ-AT-O6-P5-P7)
(OBJ-AT-O6-P6-P0)
(OBJ-AT-O6-P6-P1)
(OBJ-AT-O6-P6-P2)
(OBJ-AT-O6-P6-P3)
(OBJ-AT-O6-P6-P4)
(OBJ-AT-O6-P6-P5)
(OBJ-AT-O6-P6-P6)
(OBJ-AT-O6-P6-P7)
(OBJ-AT-O6-P7-P0)
(OBJ-AT-O6-P7-P1)
(OBJ-AT-O6-P7-P2)
(OBJ-AT-O6-P7-P3)
(OBJ-AT-O6-P7-P4)
(OBJ-AT-O6-P7-P5)
(OBJ-AT-O6-P7-P6)
(OBJ-AT-O6-P7-P7)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O5-P0-P6)
(OBJ-AT-O5-P0-P7)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P1-P6)
(OBJ-AT-O5-P1-P7)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P2-P6)
(OBJ-AT-O5-P2-P7)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P3-P6)
(OBJ-AT-O5-P3-P7)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P4-P6)
(OBJ-AT-O5-P4-P7)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O5-P5-P6)
(OBJ-AT-O5-P5-P7)
(OBJ-AT-O5-P6-P0)
(OBJ-AT-O5-P6-P1)
(OBJ-AT-O5-P6-P2)
(OBJ-AT-O5-P6-P3)
(OBJ-AT-O5-P6-P4)
(OBJ-AT-O5-P6-P5)
(OBJ-AT-O5-P6-P6)
(OBJ-AT-O5-P6-P7)
(OBJ-AT-O5-P7-P0)
(OBJ-AT-O5-P7-P2)
(OBJ-AT-O5-P7-P3)
(OBJ-AT-O5-P7-P4)
(OBJ-AT-O5-P7-P5)
(OBJ-AT-O5-P7-P6)
(OBJ-AT-O5-P7-P7)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O4-P0-P6)
(OBJ-AT-O4-P0-P7)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P1-P6)
(OBJ-AT-O4-P1-P7)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O4-P2-P6)
(OBJ-AT-O4-P2-P7)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P3-P6)
(OBJ-AT-O4-P3-P7)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P4-P6)
(OBJ-AT-O4-P4-P7)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O4-P5-P6)
(OBJ-AT-O4-P5-P7)
(OBJ-AT-O4-P6-P0)
(OBJ-AT-O4-P6-P1)
(OBJ-AT-O4-P6-P2)
(OBJ-AT-O4-P6-P3)
(OBJ-AT-O4-P6-P4)
(OBJ-AT-O4-P6-P5)
(OBJ-AT-O4-P6-P6)
(OBJ-AT-O4-P6-P7)
(OBJ-AT-O4-P7-P0)
(OBJ-AT-O4-P7-P1)
(OBJ-AT-O4-P7-P2)
(OBJ-AT-O4-P7-P3)
(OBJ-AT-O4-P7-P4)
(OBJ-AT-O4-P7-P5)
(OBJ-AT-O4-P7-P6)
(OBJ-AT-O4-P7-P7)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P0-P6)
(OBJ-AT-O3-P0-P7)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P1-P6)
(OBJ-AT-O3-P1-P7)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P2-P6)
(OBJ-AT-O3-P2-P7)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P3-P6)
(OBJ-AT-O3-P3-P7)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P4-P6)
(OBJ-AT-O3-P4-P7)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O3-P5-P6)
(OBJ-AT-O3-P5-P7)
(OBJ-AT-O3-P6-P0)
(OBJ-AT-O3-P6-P2)
(OBJ-AT-O3-P6-P3)
(OBJ-AT-O3-P6-P4)
(OBJ-AT-O3-P6-P5)
(OBJ-AT-O3-P6-P6)
(OBJ-AT-O3-P6-P7)
(OBJ-AT-O3-P7-P0)
(OBJ-AT-O3-P7-P1)
(OBJ-AT-O3-P7-P2)
(OBJ-AT-O3-P7-P3)
(OBJ-AT-O3-P7-P4)
(OBJ-AT-O3-P7-P5)
(OBJ-AT-O3-P7-P6)
(OBJ-AT-O3-P7-P7)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P0-P6)
(OBJ-AT-O2-P0-P7)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P1-P6)
(OBJ-AT-O2-P1-P7)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P2-P6)
(OBJ-AT-O2-P2-P7)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P3-P6)
(OBJ-AT-O2-P3-P7)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P4-P6)
(OBJ-AT-O2-P4-P7)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O2-P5-P6)
(OBJ-AT-O2-P5-P7)
(OBJ-AT-O2-P6-P0)
(OBJ-AT-O2-P6-P2)
(OBJ-AT-O2-P6-P3)
(OBJ-AT-O2-P6-P4)
(OBJ-AT-O2-P6-P5)
(OBJ-AT-O2-P6-P6)
(OBJ-AT-O2-P6-P7)
(OBJ-AT-O2-P7-P0)
(OBJ-AT-O2-P7-P1)
(OBJ-AT-O2-P7-P2)
(OBJ-AT-O2-P7-P3)
(OBJ-AT-O2-P7-P4)
(OBJ-AT-O2-P7-P5)
(OBJ-AT-O2-P7-P6)
(OBJ-AT-O2-P7-P7)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O1-P0-P6)
(OBJ-AT-O1-P0-P7)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P1-P6)
(OBJ-AT-O1-P1-P7)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O1-P2-P6)
(OBJ-AT-O1-P2-P7)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P3-P6)
(OBJ-AT-O1-P3-P7)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P4-P6)
(OBJ-AT-O1-P4-P7)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O1-P5-P6)
(OBJ-AT-O1-P5-P7)
(OBJ-AT-O1-P6-P0)
(OBJ-AT-O1-P6-P1)
(OBJ-AT-O1-P6-P2)
(OBJ-AT-O1-P6-P3)
(OBJ-AT-O1-P6-P4)
(OBJ-AT-O1-P6-P5)
(OBJ-AT-O1-P6-P6)
(OBJ-AT-O1-P6-P7)
(OBJ-AT-O1-P7-P0)
(OBJ-AT-O1-P7-P1)
(OBJ-AT-O1-P7-P2)
(OBJ-AT-O1-P7-P3)
(OBJ-AT-O1-P7-P4)
(OBJ-AT-O1-P7-P5)
(OBJ-AT-O1-P7-P6)
(OBJ-AT-O1-P7-P7)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P0-P6)
(OBJ-AT-O0-P0-P7)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P1-P6)
(OBJ-AT-O0-P1-P7)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O0-P2-P6)
(OBJ-AT-O0-P2-P7)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P3-P6)
(OBJ-AT-O0-P3-P7)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P4-P6)
(OBJ-AT-O0-P4-P7)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(OBJ-AT-O0-P5-P6)
(OBJ-AT-O0-P5-P7)
(OBJ-AT-O0-P6-P0)
(OBJ-AT-O0-P6-P1)
(OBJ-AT-O0-P6-P2)
(OBJ-AT-O0-P6-P3)
(OBJ-AT-O0-P6-P4)
(OBJ-AT-O0-P6-P5)
(OBJ-AT-O0-P6-P6)
(OBJ-AT-O0-P6-P7)
(OBJ-AT-O0-P7-P0)
(OBJ-AT-O0-P7-P1)
(OBJ-AT-O0-P7-P2)
(OBJ-AT-O0-P7-P3)
(OBJ-AT-O0-P7-P4)
(OBJ-AT-O0-P7-P5)
(OBJ-AT-O0-P7-P6)
(OBJ-AT-O0-P7-P7)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag221-)
(Flag148-)
(Flag74-)
(Flag223-)
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O2-P6-P1)
(OBJ-AT-O3-P6-P1)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O5-P7-P1)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O7-P2-P2)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P0)
)
(:derived (Flag223-)(and (Flag1-)(Flag74-)(Flag75-)(Flag148-)(Flag221-)(Flag222-)))

(:derived (Flag74-)(and (Flag2-)))

(:derived (Flag74-)(and (Flag3-)))

(:derived (Flag74-)(and (Flag4-)))

(:derived (Flag74-)(and (Flag5-)))

(:derived (Flag74-)(and (Flag6-)))

(:derived (Flag74-)(and (Flag7-)))

(:derived (Flag74-)(and (Flag8-)))

(:derived (Flag74-)(and (Flag9-)))

(:derived (Flag74-)(and (Flag10-)))

(:derived (Flag74-)(and (Flag11-)))

(:derived (Flag74-)(and (Flag12-)))

(:derived (Flag74-)(and (Flag13-)))

(:derived (Flag74-)(and (Flag14-)))

(:derived (Flag74-)(and (Flag15-)))

(:derived (Flag74-)(and (Flag16-)))

(:derived (Flag74-)(and (Flag17-)))

(:derived (Flag74-)(and (Flag18-)))

(:derived (Flag74-)(and (Flag19-)))

(:derived (Flag74-)(and (Flag20-)))

(:derived (Flag74-)(and (Flag21-)))

(:derived (Flag74-)(and (Flag22-)))

(:derived (Flag74-)(and (Flag23-)))

(:derived (Flag74-)(and (Flag24-)))

(:derived (Flag74-)(and (Flag25-)))

(:derived (Flag74-)(and (Flag26-)))

(:derived (Flag74-)(and (Flag27-)))

(:derived (Flag74-)(and (Flag28-)))

(:derived (Flag74-)(and (Flag29-)))

(:derived (Flag74-)(and (Flag30-)))

(:derived (Flag74-)(and (Flag31-)))

(:derived (Flag74-)(and (Flag32-)))

(:derived (Flag74-)(and (Flag33-)))

(:derived (Flag74-)(and (Flag34-)))

(:derived (Flag74-)(and (Flag35-)))

(:derived (Flag74-)(and (Flag36-)))

(:derived (Flag74-)(and (Flag37-)))

(:derived (Flag74-)(and (Flag38-)))

(:derived (Flag74-)(and (Flag39-)))

(:derived (Flag74-)(and (Flag40-)))

(:derived (Flag74-)(and (Flag41-)))

(:derived (Flag74-)(and (Flag42-)))

(:derived (Flag74-)(and (Flag43-)))

(:derived (Flag74-)(and (Flag44-)))

(:derived (Flag74-)(and (Flag45-)))

(:derived (Flag74-)(and (Flag46-)))

(:derived (Flag74-)(and (Flag47-)))

(:derived (Flag74-)(and (Flag48-)))

(:derived (Flag74-)(and (Flag49-)))

(:derived (Flag74-)(and (Flag50-)))

(:derived (Flag74-)(and (Flag51-)))

(:derived (Flag74-)(and (Flag52-)))

(:derived (Flag74-)(and (Flag53-)))

(:derived (Flag74-)(and (Flag54-)))

(:derived (Flag74-)(and (Flag55-)))

(:derived (Flag74-)(and (Flag56-)))

(:derived (Flag74-)(and (Flag57-)))

(:derived (Flag74-)(and (Flag58-)))

(:derived (Flag74-)(and (Flag59-)))

(:derived (Flag74-)(and (Flag60-)))

(:derived (Flag74-)(and (Flag61-)))

(:derived (Flag74-)(and (Flag62-)))

(:derived (Flag74-)(and (Flag63-)))

(:derived (Flag74-)(and (Flag64-)))

(:derived (Flag74-)(and (Flag65-)))

(:derived (Flag74-)(and (Flag66-)))

(:derived (Flag74-)(and (Flag67-)))

(:derived (Flag74-)(and (Flag68-)))

(:derived (Flag74-)(and (Flag69-)))

(:derived (Flag74-)(and (Flag70-)))

(:derived (Flag74-)(and (Flag71-)))

(:derived (Flag74-)(and (Flag72-)))

(:derived (Flag74-)(and (Flag73-)))

(:derived (Flag148-)(and (Flag76-)))

(:derived (Flag148-)(and (Flag77-)))

(:derived (Flag148-)(and (Flag78-)))

(:derived (Flag148-)(and (Flag79-)))

(:derived (Flag148-)(and (Flag80-)))

(:derived (Flag148-)(and (Flag81-)))

(:derived (Flag148-)(and (Flag82-)))

(:derived (Flag148-)(and (Flag83-)))

(:derived (Flag148-)(and (Flag84-)))

(:derived (Flag148-)(and (Flag85-)))

(:derived (Flag148-)(and (Flag86-)))

(:derived (Flag148-)(and (Flag87-)))

(:derived (Flag148-)(and (Flag88-)))

(:derived (Flag148-)(and (Flag89-)))

(:derived (Flag148-)(and (Flag90-)))

(:derived (Flag148-)(and (Flag91-)))

(:derived (Flag148-)(and (Flag92-)))

(:derived (Flag148-)(and (Flag93-)))

(:derived (Flag148-)(and (Flag94-)))

(:derived (Flag148-)(and (Flag95-)))

(:derived (Flag148-)(and (Flag96-)))

(:derived (Flag148-)(and (Flag97-)))

(:derived (Flag148-)(and (Flag98-)))

(:derived (Flag148-)(and (Flag99-)))

(:derived (Flag148-)(and (Flag100-)))

(:derived (Flag148-)(and (Flag101-)))

(:derived (Flag148-)(and (Flag102-)))

(:derived (Flag148-)(and (Flag103-)))

(:derived (Flag148-)(and (Flag104-)))

(:derived (Flag148-)(and (Flag105-)))

(:derived (Flag148-)(and (Flag106-)))

(:derived (Flag148-)(and (Flag107-)))

(:derived (Flag148-)(and (Flag108-)))

(:derived (Flag148-)(and (Flag109-)))

(:derived (Flag148-)(and (Flag110-)))

(:derived (Flag148-)(and (Flag111-)))

(:derived (Flag148-)(and (Flag112-)))

(:derived (Flag148-)(and (Flag113-)))

(:derived (Flag148-)(and (Flag114-)))

(:derived (Flag148-)(and (Flag115-)))

(:derived (Flag148-)(and (Flag116-)))

(:derived (Flag148-)(and (Flag117-)))

(:derived (Flag148-)(and (Flag118-)))

(:derived (Flag148-)(and (Flag119-)))

(:derived (Flag148-)(and (Flag120-)))

(:derived (Flag148-)(and (Flag121-)))

(:derived (Flag148-)(and (Flag122-)))

(:derived (Flag148-)(and (Flag123-)))

(:derived (Flag148-)(and (Flag124-)))

(:derived (Flag148-)(and (Flag125-)))

(:derived (Flag148-)(and (Flag126-)))

(:derived (Flag148-)(and (Flag127-)))

(:derived (Flag148-)(and (Flag128-)))

(:derived (Flag148-)(and (Flag129-)))

(:derived (Flag148-)(and (Flag130-)))

(:derived (Flag148-)(and (Flag131-)))

(:derived (Flag148-)(and (Flag132-)))

(:derived (Flag148-)(and (Flag133-)))

(:derived (Flag148-)(and (Flag134-)))

(:derived (Flag148-)(and (Flag135-)))

(:derived (Flag148-)(and (Flag136-)))

(:derived (Flag148-)(and (Flag137-)))

(:derived (Flag148-)(and (Flag138-)))

(:derived (Flag148-)(and (Flag139-)))

(:derived (Flag148-)(and (Flag140-)))

(:derived (Flag148-)(and (Flag141-)))

(:derived (Flag148-)(and (Flag142-)))

(:derived (Flag148-)(and (Flag143-)))

(:derived (Flag148-)(and (Flag144-)))

(:derived (Flag148-)(and (Flag145-)))

(:derived (Flag148-)(and (Flag146-)))

(:derived (Flag148-)(and (Flag147-)))

(:derived (Flag221-)(and (Flag149-)))

(:derived (Flag221-)(and (Flag150-)))

(:derived (Flag221-)(and (Flag151-)))

(:derived (Flag221-)(and (Flag152-)))

(:derived (Flag221-)(and (Flag153-)))

(:derived (Flag221-)(and (Flag154-)))

(:derived (Flag221-)(and (Flag155-)))

(:derived (Flag221-)(and (Flag156-)))

(:derived (Flag221-)(and (Flag157-)))

(:derived (Flag221-)(and (Flag158-)))

(:derived (Flag221-)(and (Flag159-)))

(:derived (Flag221-)(and (Flag160-)))

(:derived (Flag221-)(and (Flag161-)))

(:derived (Flag221-)(and (Flag162-)))

(:derived (Flag221-)(and (Flag163-)))

(:derived (Flag221-)(and (Flag164-)))

(:derived (Flag221-)(and (Flag165-)))

(:derived (Flag221-)(and (Flag166-)))

(:derived (Flag221-)(and (Flag167-)))

(:derived (Flag221-)(and (Flag168-)))

(:derived (Flag221-)(and (Flag169-)))

(:derived (Flag221-)(and (Flag170-)))

(:derived (Flag221-)(and (Flag171-)))

(:derived (Flag221-)(and (Flag172-)))

(:derived (Flag221-)(and (Flag173-)))

(:derived (Flag221-)(and (Flag174-)))

(:derived (Flag221-)(and (Flag175-)))

(:derived (Flag221-)(and (Flag176-)))

(:derived (Flag221-)(and (Flag177-)))

(:derived (Flag221-)(and (Flag178-)))

(:derived (Flag221-)(and (Flag179-)))

(:derived (Flag221-)(and (Flag180-)))

(:derived (Flag221-)(and (Flag181-)))

(:derived (Flag221-)(and (Flag182-)))

(:derived (Flag221-)(and (Flag183-)))

(:derived (Flag221-)(and (Flag184-)))

(:derived (Flag221-)(and (Flag185-)))

(:derived (Flag221-)(and (Flag186-)))

(:derived (Flag221-)(and (Flag187-)))

(:derived (Flag221-)(and (Flag188-)))

(:derived (Flag221-)(and (Flag189-)))

(:derived (Flag221-)(and (Flag190-)))

(:derived (Flag221-)(and (Flag191-)))

(:derived (Flag221-)(and (Flag192-)))

(:derived (Flag221-)(and (Flag193-)))

(:derived (Flag221-)(and (Flag194-)))

(:derived (Flag221-)(and (Flag195-)))

(:derived (Flag221-)(and (Flag196-)))

(:derived (Flag221-)(and (Flag197-)))

(:derived (Flag221-)(and (Flag198-)))

(:derived (Flag221-)(and (Flag199-)))

(:derived (Flag221-)(and (Flag200-)))

(:derived (Flag221-)(and (Flag201-)))

(:derived (Flag221-)(and (Flag202-)))

(:derived (Flag221-)(and (Flag203-)))

(:derived (Flag221-)(and (Flag204-)))

(:derived (Flag221-)(and (Flag205-)))

(:derived (Flag221-)(and (Flag206-)))

(:derived (Flag221-)(and (Flag207-)))

(:derived (Flag221-)(and (Flag208-)))

(:derived (Flag221-)(and (Flag209-)))

(:derived (Flag221-)(and (Flag210-)))

(:derived (Flag221-)(and (Flag211-)))

(:derived (Flag221-)(and (Flag212-)))

(:derived (Flag221-)(and (Flag213-)))

(:derived (Flag221-)(and (Flag214-)))

(:derived (Flag221-)(and (Flag215-)))

(:derived (Flag221-)(and (Flag216-)))

(:derived (Flag221-)(and (Flag217-)))

(:derived (Flag221-)(and (Flag218-)))

(:derived (Flag221-)(and (Flag219-)))

(:derived (Flag221-)(and (Flag220-)))

(:action PICKUP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P7))
)
)
(:action PICKUP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P6))
)
)
(:action PICKUP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P5))
)
)
(:action PICKUP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P4))
)
)
(:action PICKUP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P3))
)
)
(:action PICKUP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P2))
)
)
(:action PICKUP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P1))
)
)
(:action PICKUP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P7-P0))
)
)
(:action PICKUP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P7))
)
)
(:action PICKUP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P6))
)
)
(:action PICKUP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P5))
)
)
(:action PICKUP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P4))
)
)
(:action PICKUP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P3))
)
)
(:action PICKUP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P2))
)
)
(:action PICKUP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P1))
)
)
(:action PICKUP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P6-P0))
)
)
(:action PICKUP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P7))
)
)
(:action PICKUP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P6))
)
)
(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
)
)
(:action PICKUP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P7))
)
)
(:action PICKUP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P6))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
)
)
(:action PICKUP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P7))
)
)
(:action PICKUP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P6))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
)
)
(:action PICKUP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P7))
)
)
(:action PICKUP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P6))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
)
)
(:action PICKUP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P7))
)
)
(:action PICKUP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P6))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
)
)
(:action PICKUP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P7))
)
)
(:action PICKUP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P6))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
)
)
(:action PICKUP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P7))
)
)
(:action PICKUP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P6))
)
)
(:action PICKUP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P5))
)
)
(:action PICKUP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P4))
)
)
(:action PICKUP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P3))
)
)
(:action PICKUP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P2))
)
)
(:action PICKUP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P1))
)
)
(:action PICKUP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P7-P0))
)
)
(:action PICKUP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P7))
)
)
(:action PICKUP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P6))
)
)
(:action PICKUP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P5))
)
)
(:action PICKUP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P4))
)
)
(:action PICKUP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P3))
)
)
(:action PICKUP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P2))
)
)
(:action PICKUP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P1))
)
)
(:action PICKUP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P6-P0))
)
)
(:action PICKUP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P7))
)
)
(:action PICKUP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P6))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
)
)
(:action PICKUP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P7))
)
)
(:action PICKUP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P6))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
)
)
(:action PICKUP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P7))
)
)
(:action PICKUP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P6))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
)
)
(:action PICKUP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P7))
)
)
(:action PICKUP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P6))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
)
)
(:action PICKUP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P7))
)
)
(:action PICKUP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P6))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
)
)
(:action PICKUP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P7))
)
)
(:action PICKUP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P6))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
)
)
(:action PICKUP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P7))
)
)
(:action PICKUP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P6))
)
)
(:action PICKUP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P5))
)
)
(:action PICKUP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P4))
)
)
(:action PICKUP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P3))
)
)
(:action PICKUP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P2))
)
)
(:action PICKUP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P1))
)
)
(:action PICKUP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P7-P0))
)
)
(:action PICKUP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P7))
)
)
(:action PICKUP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P6))
)
)
(:action PICKUP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P5))
)
)
(:action PICKUP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P4))
)
)
(:action PICKUP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P3))
)
)
(:action PICKUP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P2))
)
)
(:action PICKUP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P0))
)
)
(:action PICKUP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P7))
)
)
(:action PICKUP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P6))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
)
)
(:action PICKUP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P7))
)
)
(:action PICKUP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P6))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
)
)
(:action PICKUP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P7))
)
)
(:action PICKUP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P6))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
)
)
(:action PICKUP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P7))
)
)
(:action PICKUP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P6))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
)
)
(:action PICKUP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P7))
)
)
(:action PICKUP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P6))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
)
)
(:action PICKUP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P7))
)
)
(:action PICKUP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P6))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
)
)
(:action PICKUP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P7))
)
)
(:action PICKUP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P6))
)
)
(:action PICKUP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P5))
)
)
(:action PICKUP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P4))
)
)
(:action PICKUP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P3))
)
)
(:action PICKUP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P2))
)
)
(:action PICKUP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P1))
)
)
(:action PICKUP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P7-P0))
)
)
(:action PICKUP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P7))
)
)
(:action PICKUP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P6))
)
)
(:action PICKUP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P5))
)
)
(:action PICKUP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P4))
)
)
(:action PICKUP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P3))
)
)
(:action PICKUP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P2))
)
)
(:action PICKUP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P0))
)
)
(:action PICKUP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P7))
)
)
(:action PICKUP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P6))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
)
)
(:action PICKUP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P7))
)
)
(:action PICKUP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P6))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
)
)
(:action PICKUP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P7))
)
)
(:action PICKUP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P6))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
)
)
(:action PICKUP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P7))
)
)
(:action PICKUP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P6))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
)
)
(:action PICKUP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P7))
)
)
(:action PICKUP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P6))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
)
)
(:action PICKUP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P7))
)
)
(:action PICKUP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P6))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
)
)
(:action PICKUP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P7))
)
)
(:action PICKUP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P6))
)
)
(:action PICKUP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P5))
)
)
(:action PICKUP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P4))
)
)
(:action PICKUP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P3))
)
)
(:action PICKUP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P2))
)
)
(:action PICKUP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P1))
)
)
(:action PICKUP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P7-P0))
)
)
(:action PICKUP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P7))
)
)
(:action PICKUP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P6))
)
)
(:action PICKUP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P5))
)
)
(:action PICKUP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P4))
)
)
(:action PICKUP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P3))
)
)
(:action PICKUP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P2))
)
)
(:action PICKUP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P1))
)
)
(:action PICKUP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P6-P0))
)
)
(:action PICKUP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P7))
)
)
(:action PICKUP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P6))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
)
)
(:action PICKUP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P7))
)
)
(:action PICKUP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P6))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
)
)
(:action PICKUP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P7))
)
)
(:action PICKUP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P6))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
)
)
(:action PICKUP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P7))
)
)
(:action PICKUP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P6))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
)
)
(:action PICKUP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P7))
)
)
(:action PICKUP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P6))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
)
)
(:action PICKUP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P7))
)
)
(:action PICKUP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P6))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
)
)
(:action PICKUP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P7))
)
)
(:action PICKUP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P6))
)
)
(:action PICKUP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P5))
)
)
(:action PICKUP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P4))
)
)
(:action PICKUP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P3))
)
)
(:action PICKUP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P2))
)
)
(:action PICKUP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P0))
)
)
(:action PICKUP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P7))
)
)
(:action PICKUP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P6))
)
)
(:action PICKUP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P5))
)
)
(:action PICKUP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P4))
)
)
(:action PICKUP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P3))
)
)
(:action PICKUP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P2))
)
)
(:action PICKUP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P1))
)
)
(:action PICKUP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P6-P0))
)
)
(:action PICKUP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P7))
)
)
(:action PICKUP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P6))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
)
)
(:action PICKUP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P7))
)
)
(:action PICKUP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P6))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
)
)
(:action PICKUP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P7))
)
)
(:action PICKUP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P6))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
)
)
(:action PICKUP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P7))
)
)
(:action PICKUP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P6))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
)
)
(:action PICKUP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P7))
)
)
(:action PICKUP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P6))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
)
)
(:action PICKUP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P7))
)
)
(:action PICKUP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P6))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
)
)
(:action PICKUP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P7))
)
)
(:action PICKUP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P6))
)
)
(:action PICKUP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P5))
)
)
(:action PICKUP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P4))
)
)
(:action PICKUP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P3))
)
)
(:action PICKUP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P2))
)
)
(:action PICKUP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P1))
)
)
(:action PICKUP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P7-P0))
)
)
(:action PICKUP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P7))
)
)
(:action PICKUP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P6))
)
)
(:action PICKUP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P5))
)
)
(:action PICKUP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P4))
)
)
(:action PICKUP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P3))
)
)
(:action PICKUP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P2))
)
)
(:action PICKUP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P1))
)
)
(:action PICKUP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P6-P0))
)
)
(:action PICKUP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P7))
)
)
(:action PICKUP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P6))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
)
)
(:action PICKUP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P7))
)
)
(:action PICKUP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P6))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
)
)
(:action PICKUP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P7))
)
)
(:action PICKUP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P6))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
)
)
(:action PICKUP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P7))
)
)
(:action PICKUP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P6))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
)
)
(:action PICKUP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P7))
)
)
(:action PICKUP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P6))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
)
)
(:action PICKUP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P7))
)
)
(:action PICKUP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P6))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
)
)
(:action PICKUP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P7))
)
)
(:action PICKUP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P6))
)
)
(:action PICKUP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P5))
)
)
(:action PICKUP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P4))
)
)
(:action PICKUP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P3))
)
)
(:action PICKUP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P2))
)
)
(:action PICKUP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P1))
)
)
(:action PICKUP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P7-P0))
)
)
(:action PICKUP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P7))
)
)
(:action PICKUP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P6))
)
)
(:action PICKUP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P5))
)
)
(:action PICKUP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P4))
)
)
(:action PICKUP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P3))
)
)
(:action PICKUP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P2))
)
)
(:action PICKUP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P1))
)
)
(:action PICKUP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P6-P0))
)
)
(:action PICKUP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P7))
)
)
(:action PICKUP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P6))
)
)
(:action PICKUP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P5))
)
)
(:action PICKUP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P4))
)
)
(:action PICKUP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P3))
)
)
(:action PICKUP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P2))
)
)
(:action PICKUP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P1))
)
)
(:action PICKUP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P0))
)
)
(:action PICKUP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P7))
)
)
(:action PICKUP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P6))
)
)
(:action PICKUP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P5))
)
)
(:action PICKUP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P4))
)
)
(:action PICKUP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P3))
)
)
(:action PICKUP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P2))
)
)
(:action PICKUP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P1))
)
)
(:action PICKUP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P0))
)
)
(:action PICKUP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P7))
)
)
(:action PICKUP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P6))
)
)
(:action PICKUP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P5))
)
)
(:action PICKUP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P4))
)
)
(:action PICKUP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P3))
)
)
(:action PICKUP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P2))
)
)
(:action PICKUP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P1))
)
)
(:action PICKUP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P0))
)
)
(:action PICKUP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P7))
)
)
(:action PICKUP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P6))
)
)
(:action PICKUP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P5))
)
)
(:action PICKUP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P4))
)
)
(:action PICKUP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P3))
)
)
(:action PICKUP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P1))
)
)
(:action PICKUP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P0))
)
)
(:action PICKUP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P7))
)
)
(:action PICKUP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P6))
)
)
(:action PICKUP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P5))
)
)
(:action PICKUP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P4))
)
)
(:action PICKUP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P3))
)
)
(:action PICKUP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P2))
)
)
(:action PICKUP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P1))
)
)
(:action PICKUP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P0))
)
)
(:action PICKUP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P7))
)
)
(:action PICKUP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P6))
)
)
(:action PICKUP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P5))
)
)
(:action PICKUP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P4))
)
)
(:action PICKUP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P3))
)
)
(:action PICKUP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P2))
)
)
(:action PICKUP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P1))
)
)
(:action PICKUP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P0))
)
)
(:action PICKUP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P7))
)
)
(:action PICKUP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P6))
)
)
(:action PICKUP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P5))
)
)
(:action PICKUP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P4))
)
)
(:action PICKUP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P3))
)
)
(:action PICKUP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P2))
)
)
(:action PICKUP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P1))
)
)
(:action PICKUP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P7-P0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P7-P0))
)
)
(:action PICKUP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P7))
)
)
(:action PICKUP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P6))
)
)
(:action PICKUP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P5))
)
)
(:action PICKUP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P4))
)
)
(:action PICKUP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P3))
)
)
(:action PICKUP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P2))
)
)
(:action PICKUP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P1))
)
)
(:action PICKUP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P6-P0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P6-P0))
)
)
(:action PICKUP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P7))
)
)
(:action PICKUP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P6))
)
)
(:action PICKUP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P5))
)
)
(:action PICKUP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P4))
)
)
(:action PICKUP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P3))
)
)
(:action PICKUP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P2))
)
)
(:action PICKUP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P1))
)
)
(:action PICKUP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P0))
)
)
(:action PICKUP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P7))
)
)
(:action PICKUP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P6))
)
)
(:action PICKUP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P5))
)
)
(:action PICKUP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P4))
)
)
(:action PICKUP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P3))
)
)
(:action PICKUP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P2))
)
)
(:action PICKUP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P1))
)
)
(:action PICKUP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P0))
)
)
(:action PICKUP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P7))
)
)
(:action PICKUP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P6))
)
)
(:action PICKUP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P5))
)
)
(:action PICKUP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P4))
)
)
(:action PICKUP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P3))
)
)
(:action PICKUP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P2))
)
)
(:action PICKUP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P1))
)
)
(:action PICKUP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P0))
)
)
(:action PICKUP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P7))
)
)
(:action PICKUP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P6))
)
)
(:action PICKUP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P5))
)
)
(:action PICKUP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P4))
)
)
(:action PICKUP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P3))
)
)
(:action PICKUP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P2))
)
)
(:action PICKUP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P1))
)
)
(:action PICKUP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P0))
)
)
(:action PICKUP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P7))
)
)
(:action PICKUP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P6))
)
)
(:action PICKUP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P5))
)
)
(:action PICKUP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P4))
)
)
(:action PICKUP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P3))
)
)
(:action PICKUP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P2))
)
)
(:action PICKUP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P1))
)
)
(:action PICKUP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P0))
)
)
(:action PICKUP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P7))
)
)
(:action PICKUP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P6))
)
)
(:action PICKUP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P5))
)
)
(:action PICKUP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P3))
)
)
(:action PICKUP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P2))
)
)
(:action PICKUP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P1))
)
)
(:action PICKUP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P0))
)
)
(:derived (Flag1-)(and (OBJ-AT-O0-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O1-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O2-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O3-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O4-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O5-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O6-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O7-P0-P2)))

(:derived (Flag1-)(and (OBJ-AT-O8-P0-P2)))

(:derived (Flag2-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag3-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag4-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag5-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag6-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag7-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag8-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag9-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O0-P5-P5)))

(:derived (Flag10-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag11-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag12-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag13-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag14-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag15-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag16-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag17-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O1-P5-P5)))

(:derived (Flag18-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag19-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag20-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag21-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag22-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag23-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag24-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag25-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O2-P5-P5)))

(:derived (Flag26-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag27-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag28-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag29-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag30-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag31-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag32-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag33-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O3-P5-P5)))

(:derived (Flag34-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag35-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag36-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag37-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag38-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag39-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag40-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag41-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O4-P5-P5)))

(:derived (Flag42-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag43-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag44-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag45-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag46-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag47-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag48-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag49-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O5-P5-P5)))

(:derived (Flag50-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag51-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag52-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag53-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag54-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag55-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag56-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag57-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O6-P5-P5)))

(:derived (Flag58-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag59-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag60-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag61-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag62-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag63-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag64-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag65-)(and (OBJ-AT-O8-P5-P5)(OBJ-AT-O7-P5-P5)))

(:derived (Flag66-)(and (OBJ-AT-O0-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag67-)(and (OBJ-AT-O1-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag68-)(and (OBJ-AT-O2-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag69-)(and (OBJ-AT-O3-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag70-)(and (OBJ-AT-O4-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag71-)(and (OBJ-AT-O5-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag72-)(and (OBJ-AT-O6-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag73-)(and (OBJ-AT-O7-P5-P5)(OBJ-AT-O8-P5-P5)))

(:derived (Flag75-)(and (OBJ-AT-O0-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O1-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O2-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O3-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O4-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O5-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O6-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O7-P0-P6)))

(:derived (Flag75-)(and (OBJ-AT-O8-P0-P6)))

(:derived (Flag76-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag77-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag78-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag79-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag80-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag81-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag82-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag83-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O0-P4-P1)))

(:derived (Flag84-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag85-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag86-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag87-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag88-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag89-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag90-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag91-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O1-P4-P1)))

(:derived (Flag92-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag93-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag94-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag95-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag96-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag97-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag98-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag99-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O2-P4-P1)))

(:derived (Flag100-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag101-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag102-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag103-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag104-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag105-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag106-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag107-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O3-P4-P1)))

(:derived (Flag108-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag109-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag110-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag111-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag112-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag113-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag114-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag115-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O4-P4-P1)))

(:derived (Flag116-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag117-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag118-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag119-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag120-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag121-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag122-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag123-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O5-P4-P1)))

(:derived (Flag124-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag125-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag126-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag127-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag128-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag129-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag130-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag131-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O6-P4-P1)))

(:derived (Flag132-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag133-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag134-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag135-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag136-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag137-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag138-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag139-)(and (OBJ-AT-O8-P4-P1)(OBJ-AT-O7-P4-P1)))

(:derived (Flag140-)(and (OBJ-AT-O0-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag141-)(and (OBJ-AT-O1-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag142-)(and (OBJ-AT-O2-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag143-)(and (OBJ-AT-O3-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag144-)(and (OBJ-AT-O4-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag145-)(and (OBJ-AT-O5-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag146-)(and (OBJ-AT-O6-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag147-)(and (OBJ-AT-O7-P4-P1)(OBJ-AT-O8-P4-P1)))

(:derived (Flag149-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag150-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag151-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag152-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag153-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag154-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag155-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag156-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O0-P1-P6)))

(:derived (Flag157-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag158-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag159-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag160-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag161-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag162-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag163-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag164-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O1-P1-P6)))

(:derived (Flag165-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag166-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag167-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag168-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag169-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag170-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag171-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag172-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O2-P1-P6)))

(:derived (Flag173-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag174-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag175-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag176-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag177-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag178-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag179-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag180-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O3-P1-P6)))

(:derived (Flag181-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag182-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag183-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag184-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag185-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag186-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag187-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag188-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O4-P1-P6)))

(:derived (Flag189-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag190-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag191-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag192-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag193-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag194-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag195-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag196-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O5-P1-P6)))

(:derived (Flag197-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag198-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag199-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag200-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag201-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag202-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag203-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag204-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O6-P1-P6)))

(:derived (Flag205-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag206-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag207-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag208-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag209-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag210-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag211-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag212-)(and (OBJ-AT-O8-P1-P6)(OBJ-AT-O7-P1-P6)))

(:derived (Flag213-)(and (OBJ-AT-O0-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag214-)(and (OBJ-AT-O1-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag215-)(and (OBJ-AT-O2-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag216-)(and (OBJ-AT-O3-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag217-)(and (OBJ-AT-O4-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag218-)(and (OBJ-AT-O5-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag219-)(and (OBJ-AT-O6-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag220-)(and (OBJ-AT-O7-P1-P6)(OBJ-AT-O8-P1-P6)))

(:derived (Flag222-)(and (OBJ-AT-O0-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O1-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O2-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O3-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O4-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O5-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O6-P0-P4)))

(:derived (Flag222-)(and (OBJ-AT-O7-P0-P4)))

(:action DROP-O0-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O0-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O0-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O0-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O0-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O0-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O0-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O0-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O0-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O0-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O0-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O0-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O0-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O0-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O0-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O0-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O0-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O0-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O0-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O0-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O0-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O0-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O0-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O0-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O0-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O0-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O0-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O0-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O0-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O1-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O1-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O1-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O1-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O1-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O1-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O1-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O1-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O1-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O1-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O1-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O1-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O1-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O1-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O1-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O1-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O1-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O1-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O1-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O1-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O1-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O1-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O1-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O1-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O1-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O1-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O1-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O1-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O1-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O2-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O2-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O2-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O2-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O2-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O2-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O2-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O2-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O2-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O2-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O2-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O2-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O2-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O2-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O2-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O2-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O2-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O2-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O2-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O2-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O2-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O2-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O2-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O2-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O2-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O2-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O2-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O2-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O2-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O3-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O3-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O3-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O3-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O3-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O3-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O3-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O3-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O3-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O3-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O3-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O3-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O3-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O3-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O3-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O3-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O3-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O3-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O3-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O3-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O3-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O3-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O3-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O3-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O3-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O3-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O3-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O3-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O3-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O4-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O4-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O4-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O4-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O4-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O4-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O4-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O4-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O4-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O4-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O4-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O4-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O4-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O4-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O4-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O4-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O4-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O4-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O4-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O4-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O4-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O4-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O4-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O4-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O4-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O4-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O4-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O4-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O4-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O5-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O5-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O5-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O5-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O5-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O5-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O5-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O5-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O5-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O5-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O5-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O5-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O5-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O5-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O5-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O5-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O5-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O5-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O5-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O5-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O5-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O5-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O5-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O5-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O5-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O5-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O5-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O5-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O5-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O6-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O6-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O6-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O6-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O6-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O6-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O6-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O6-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O6-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O6-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O6-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O6-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O6-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O6-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O6-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O6-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O6-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O6-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O6-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O6-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O6-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O6-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O6-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O6-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O6-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O6-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O6-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O6-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O6-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O7-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O7-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O7-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O7-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O7-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O7-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O7-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O7-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O7-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O7-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O7-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O7-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O7-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O7-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O7-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O7-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O7-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O7-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O7-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O7-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O7-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O7-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O7-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O7-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O7-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O7-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O7-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O7-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O7-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O7-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O7-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O7-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O7-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O7-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O7-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O7-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O7-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O7-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O7-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O7-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O7-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O7-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O7-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O7-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O7-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O7-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O7-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O7-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O7-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O7-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O7-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O7-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O7-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O7-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O7-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O7-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O7-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O7-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O7-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O7-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O7-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O7-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O7-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O7-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O7-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O8-P7-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P7)
)
:effect
(and
(OBJ-AT-O8-P7-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P6)
)
:effect
(and
(OBJ-AT-O8-P7-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P5)
)
:effect
(and
(OBJ-AT-O8-P7-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P4)
)
:effect
(and
(OBJ-AT-O8-P7-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P3)
)
:effect
(and
(OBJ-AT-O8-P7-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P2)
)
:effect
(and
(OBJ-AT-O8-P7-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P1)
)
:effect
(and
(OBJ-AT-O8-P7-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P7-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P7-P0)
)
:effect
(and
(OBJ-AT-O8-P7-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P7)
)
:effect
(and
(OBJ-AT-O8-P6-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P6)
)
:effect
(and
(OBJ-AT-O8-P6-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P5)
)
:effect
(and
(OBJ-AT-O8-P6-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P4)
)
:effect
(and
(OBJ-AT-O8-P6-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P3)
)
:effect
(and
(OBJ-AT-O8-P6-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P2)
)
:effect
(and
(OBJ-AT-O8-P6-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P1)
)
:effect
(and
(OBJ-AT-O8-P6-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P6-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P6-P0)
)
:effect
(and
(OBJ-AT-O8-P6-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P7)
)
:effect
(and
(OBJ-AT-O8-P5-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P6)
)
:effect
(and
(OBJ-AT-O8-P5-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O8-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O8-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O8-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O8-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O8-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O8-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P7)
)
:effect
(and
(OBJ-AT-O8-P4-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P6)
)
:effect
(and
(OBJ-AT-O8-P4-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O8-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O8-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O8-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O8-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O8-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O8-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P7)
)
:effect
(and
(OBJ-AT-O8-P3-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P6)
)
:effect
(and
(OBJ-AT-O8-P3-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O8-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O8-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O8-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O8-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O8-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O8-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P7)
)
:effect
(and
(OBJ-AT-O8-P2-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P6)
)
:effect
(and
(OBJ-AT-O8-P2-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O8-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O8-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O8-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O8-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O8-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O8-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P7)
)
:effect
(and
(OBJ-AT-O8-P1-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P6)
)
:effect
(and
(OBJ-AT-O8-P1-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O8-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O8-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O8-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O8-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O8-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O8-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P7-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P7)
)
:effect
(and
(OBJ-AT-O8-P0-P7)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P6-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P6)
)
:effect
(and
(OBJ-AT-O8-P0-P6)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O8-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O8-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O8-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O8-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O8-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O8-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
)
)
(:action PICKUP-O2-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P6-P1))
)
)
(:action PICKUP-O3-P6-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P6-P1)
(ROBOT-AT-P6-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P6-P1))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
)
)
(:action PICKUP-O5-P7-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P7-P1)
(ROBOT-AT-P7-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P7-P1))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
)
)
(:action PICKUP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P2))
)
)
(:action PICKUP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P4))
)
)
(:action MOVE-P7-P7-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P7-P7))
)
)
(:action MOVE-P7-P6-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P7-P6))
)
)
(:action MOVE-P7-P5-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P7-P5))
)
)
(:action MOVE-P7-P4-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P7-P4))
)
)
(:action MOVE-P7-P3-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P7-P3))
)
)
(:action MOVE-P7-P2-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P7-P2))
)
)
(:action MOVE-P7-P1-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P7-P1))
)
)
(:action MOVE-P7-P0-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P7-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P7-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P7-P0))
)
)
(:action MOVE-P6-P7-P7-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P7-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P6-P7))
)
)
(:action MOVE-P6-P6-P7-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P7-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P6-P6))
)
)
(:action MOVE-P6-P5-P7-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P7-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P6-P5))
)
)
(:action MOVE-P6-P4-P7-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P7-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P6-P4))
)
)
(:action MOVE-P6-P3-P7-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P7-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P6-P3))
)
)
(:action MOVE-P6-P2-P7-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P7-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P6-P2))
)
)
(:action MOVE-P6-P1-P7-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P7-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P6-P1))
)
)
(:action MOVE-P6-P0-P7-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P7-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P6-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P6-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P6-P0))
)
)
(:action MOVE-P5-P7-P6-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P6-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P5-P7))
)
)
(:action MOVE-P5-P6-P6-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P6-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P5-P6))
)
)
(:action MOVE-P5-P5-P6-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P6-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P4-P6-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P6-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P3-P6-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P6-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P2-P6-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P6-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P1-P6-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P6-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P0-P6-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P6-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P4-P7-P5-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P5-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P4-P7))
)
)
(:action MOVE-P4-P6-P5-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P5-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P4-P6))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P3-P7-P4-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P4-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P3-P7))
)
)
(:action MOVE-P3-P6-P4-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P4-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P3-P6))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P2-P7-P3-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P3-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P2-P7))
)
)
(:action MOVE-P2-P6-P3-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P3-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P2-P6))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P1-P7-P2-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P2-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P7-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P7)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P1-P7))
)
)
(:action MOVE-P1-P6-P2-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P2-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P6-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P6)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P1-P6))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P0-P7-P1-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P1-P7)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P7-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P7)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P7))
)
)
(:action MOVE-P0-P6-P1-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P1-P6)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P7
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P7)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P6-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P6)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P6))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P6
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P6)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
)
)
(:derived (Flag222-)(and (OBJ-AT-O8-P0-P4)))

)
