(define (domain grounded-SIMPLE-ADL-GRID)
(:requirements
:strips
)
(:predicates
(HOLDING-O8)
(ROBOT-AT-P0-P1)
(ROBOT-AT-P1-P0)
(ROBOT-AT-P0-P2)
(ROBOT-AT-P1-P1)
(ROBOT-AT-P0-P3)
(ROBOT-AT-P1-P2)
(ROBOT-AT-P0-P4)
(ROBOT-AT-P1-P3)
(ROBOT-AT-P0-P5)
(ROBOT-AT-P1-P4)
(ROBOT-AT-P1-P5)
(ROBOT-AT-P2-P0)
(ROBOT-AT-P2-P1)
(ROBOT-AT-P2-P2)
(ROBOT-AT-P2-P3)
(ROBOT-AT-P2-P4)
(ROBOT-AT-P2-P5)
(ROBOT-AT-P3-P0)
(ROBOT-AT-P3-P1)
(ROBOT-AT-P3-P2)
(ROBOT-AT-P3-P3)
(ROBOT-AT-P3-P4)
(ROBOT-AT-P3-P5)
(ROBOT-AT-P4-P0)
(ROBOT-AT-P4-P1)
(ROBOT-AT-P4-P2)
(ROBOT-AT-P4-P3)
(ROBOT-AT-P4-P4)
(ROBOT-AT-P4-P5)
(ROBOT-AT-P5-P0)
(ROBOT-AT-P5-P1)
(ROBOT-AT-P5-P2)
(ROBOT-AT-P5-P3)
(ROBOT-AT-P5-P4)
(ROBOT-AT-P5-P5)
(HOLDING-O7)
(HOLDING-O6)
(HOLDING-O5)
(HOLDING-O4)
(HOLDING-O3)
(HOLDING-O2)
(HOLDING-O1)
(HOLDING-O0)
(OBJ-AT-O8-P0-P1)
(OBJ-AT-O8-P0-P2)
(OBJ-AT-O8-P0-P3)
(OBJ-AT-O8-P0-P4)
(OBJ-AT-O8-P0-P5)
(OBJ-AT-O8-P1-P0)
(OBJ-AT-O8-P1-P1)
(OBJ-AT-O8-P1-P2)
(OBJ-AT-O8-P1-P3)
(OBJ-AT-O8-P1-P4)
(OBJ-AT-O8-P1-P5)
(OBJ-AT-O8-P2-P0)
(OBJ-AT-O8-P2-P1)
(OBJ-AT-O8-P2-P2)
(OBJ-AT-O8-P2-P3)
(OBJ-AT-O8-P2-P4)
(OBJ-AT-O8-P2-P5)
(OBJ-AT-O8-P3-P0)
(OBJ-AT-O8-P3-P1)
(OBJ-AT-O8-P3-P2)
(OBJ-AT-O8-P3-P3)
(OBJ-AT-O8-P3-P4)
(OBJ-AT-O8-P3-P5)
(OBJ-AT-O8-P4-P0)
(OBJ-AT-O8-P4-P1)
(OBJ-AT-O8-P4-P2)
(OBJ-AT-O8-P4-P3)
(OBJ-AT-O8-P4-P4)
(OBJ-AT-O8-P4-P5)
(OBJ-AT-O8-P5-P0)
(OBJ-AT-O8-P5-P1)
(OBJ-AT-O8-P5-P2)
(OBJ-AT-O8-P5-P3)
(OBJ-AT-O8-P5-P4)
(OBJ-AT-O8-P5-P5)
(OBJ-AT-O7-P0-P0)
(OBJ-AT-O7-P0-P1)
(OBJ-AT-O7-P0-P2)
(OBJ-AT-O7-P0-P3)
(OBJ-AT-O7-P0-P4)
(OBJ-AT-O7-P0-P5)
(OBJ-AT-O7-P1-P0)
(OBJ-AT-O7-P1-P1)
(OBJ-AT-O7-P1-P2)
(OBJ-AT-O7-P1-P3)
(OBJ-AT-O7-P1-P4)
(OBJ-AT-O7-P1-P5)
(OBJ-AT-O7-P2-P0)
(OBJ-AT-O7-P2-P1)
(OBJ-AT-O7-P2-P2)
(OBJ-AT-O7-P2-P3)
(OBJ-AT-O7-P2-P4)
(OBJ-AT-O7-P2-P5)
(OBJ-AT-O7-P3-P0)
(OBJ-AT-O7-P3-P1)
(OBJ-AT-O7-P3-P2)
(OBJ-AT-O7-P3-P3)
(OBJ-AT-O7-P3-P4)
(OBJ-AT-O7-P3-P5)
(OBJ-AT-O7-P4-P0)
(OBJ-AT-O7-P4-P1)
(OBJ-AT-O7-P4-P2)
(OBJ-AT-O7-P4-P3)
(OBJ-AT-O7-P4-P4)
(OBJ-AT-O7-P4-P5)
(OBJ-AT-O7-P5-P0)
(OBJ-AT-O7-P5-P1)
(OBJ-AT-O7-P5-P2)
(OBJ-AT-O7-P5-P4)
(OBJ-AT-O7-P5-P5)
(OBJ-AT-O6-P0-P0)
(OBJ-AT-O6-P0-P1)
(OBJ-AT-O6-P0-P3)
(OBJ-AT-O6-P0-P4)
(OBJ-AT-O6-P0-P5)
(OBJ-AT-O6-P1-P0)
(OBJ-AT-O6-P1-P1)
(OBJ-AT-O6-P1-P2)
(OBJ-AT-O6-P1-P3)
(OBJ-AT-O6-P1-P4)
(OBJ-AT-O6-P1-P5)
(OBJ-AT-O6-P2-P0)
(OBJ-AT-O6-P2-P1)
(OBJ-AT-O6-P2-P2)
(OBJ-AT-O6-P2-P3)
(OBJ-AT-O6-P2-P4)
(OBJ-AT-O6-P2-P5)
(OBJ-AT-O6-P3-P0)
(OBJ-AT-O6-P3-P1)
(OBJ-AT-O6-P3-P2)
(OBJ-AT-O6-P3-P3)
(OBJ-AT-O6-P3-P4)
(OBJ-AT-O6-P3-P5)
(OBJ-AT-O6-P4-P0)
(OBJ-AT-O6-P4-P1)
(OBJ-AT-O6-P4-P2)
(OBJ-AT-O6-P4-P3)
(OBJ-AT-O6-P4-P4)
(OBJ-AT-O6-P4-P5)
(OBJ-AT-O6-P5-P0)
(OBJ-AT-O6-P5-P1)
(OBJ-AT-O6-P5-P2)
(OBJ-AT-O6-P5-P3)
(OBJ-AT-O6-P5-P4)
(OBJ-AT-O6-P5-P5)
(OBJ-AT-O5-P0-P0)
(OBJ-AT-O5-P0-P1)
(OBJ-AT-O5-P0-P2)
(OBJ-AT-O5-P0-P3)
(OBJ-AT-O5-P0-P4)
(OBJ-AT-O5-P0-P5)
(OBJ-AT-O5-P1-P0)
(OBJ-AT-O5-P1-P1)
(OBJ-AT-O5-P1-P2)
(OBJ-AT-O5-P1-P3)
(OBJ-AT-O5-P1-P4)
(OBJ-AT-O5-P1-P5)
(OBJ-AT-O5-P2-P0)
(OBJ-AT-O5-P2-P1)
(OBJ-AT-O5-P2-P2)
(OBJ-AT-O5-P2-P3)
(OBJ-AT-O5-P2-P4)
(OBJ-AT-O5-P2-P5)
(OBJ-AT-O5-P3-P1)
(OBJ-AT-O5-P3-P2)
(OBJ-AT-O5-P3-P3)
(OBJ-AT-O5-P3-P4)
(OBJ-AT-O5-P3-P5)
(OBJ-AT-O5-P4-P0)
(OBJ-AT-O5-P4-P1)
(OBJ-AT-O5-P4-P2)
(OBJ-AT-O5-P4-P3)
(OBJ-AT-O5-P4-P4)
(OBJ-AT-O5-P4-P5)
(OBJ-AT-O5-P5-P0)
(OBJ-AT-O5-P5-P1)
(OBJ-AT-O5-P5-P2)
(OBJ-AT-O5-P5-P3)
(OBJ-AT-O5-P5-P4)
(OBJ-AT-O5-P5-P5)
(OBJ-AT-O4-P0-P0)
(OBJ-AT-O4-P0-P1)
(OBJ-AT-O4-P0-P2)
(OBJ-AT-O4-P0-P3)
(OBJ-AT-O4-P0-P5)
(OBJ-AT-O4-P1-P0)
(OBJ-AT-O4-P1-P1)
(OBJ-AT-O4-P1-P2)
(OBJ-AT-O4-P1-P3)
(OBJ-AT-O4-P1-P4)
(OBJ-AT-O4-P1-P5)
(OBJ-AT-O4-P2-P0)
(OBJ-AT-O4-P2-P1)
(OBJ-AT-O4-P2-P2)
(OBJ-AT-O4-P2-P3)
(OBJ-AT-O4-P2-P4)
(OBJ-AT-O4-P2-P5)
(OBJ-AT-O4-P3-P0)
(OBJ-AT-O4-P3-P1)
(OBJ-AT-O4-P3-P2)
(OBJ-AT-O4-P3-P3)
(OBJ-AT-O4-P3-P4)
(OBJ-AT-O4-P3-P5)
(OBJ-AT-O4-P4-P0)
(OBJ-AT-O4-P4-P1)
(OBJ-AT-O4-P4-P2)
(OBJ-AT-O4-P4-P3)
(OBJ-AT-O4-P4-P4)
(OBJ-AT-O4-P4-P5)
(OBJ-AT-O4-P5-P0)
(OBJ-AT-O4-P5-P1)
(OBJ-AT-O4-P5-P2)
(OBJ-AT-O4-P5-P3)
(OBJ-AT-O4-P5-P4)
(OBJ-AT-O4-P5-P5)
(OBJ-AT-O3-P0-P0)
(OBJ-AT-O3-P0-P1)
(OBJ-AT-O3-P0-P2)
(OBJ-AT-O3-P0-P4)
(OBJ-AT-O3-P0-P5)
(OBJ-AT-O3-P1-P0)
(OBJ-AT-O3-P1-P1)
(OBJ-AT-O3-P1-P2)
(OBJ-AT-O3-P1-P3)
(OBJ-AT-O3-P1-P4)
(OBJ-AT-O3-P1-P5)
(OBJ-AT-O3-P2-P0)
(OBJ-AT-O3-P2-P1)
(OBJ-AT-O3-P2-P2)
(OBJ-AT-O3-P2-P3)
(OBJ-AT-O3-P2-P4)
(OBJ-AT-O3-P2-P5)
(OBJ-AT-O3-P3-P0)
(OBJ-AT-O3-P3-P1)
(OBJ-AT-O3-P3-P2)
(OBJ-AT-O3-P3-P3)
(OBJ-AT-O3-P3-P4)
(OBJ-AT-O3-P3-P5)
(OBJ-AT-O3-P4-P0)
(OBJ-AT-O3-P4-P1)
(OBJ-AT-O3-P4-P2)
(OBJ-AT-O3-P4-P3)
(OBJ-AT-O3-P4-P4)
(OBJ-AT-O3-P4-P5)
(OBJ-AT-O3-P5-P0)
(OBJ-AT-O3-P5-P1)
(OBJ-AT-O3-P5-P2)
(OBJ-AT-O3-P5-P3)
(OBJ-AT-O3-P5-P4)
(OBJ-AT-O3-P5-P5)
(OBJ-AT-O2-P0-P0)
(OBJ-AT-O2-P0-P1)
(OBJ-AT-O2-P0-P2)
(OBJ-AT-O2-P0-P3)
(OBJ-AT-O2-P0-P4)
(OBJ-AT-O2-P0-P5)
(OBJ-AT-O2-P1-P0)
(OBJ-AT-O2-P1-P2)
(OBJ-AT-O2-P1-P3)
(OBJ-AT-O2-P1-P4)
(OBJ-AT-O2-P1-P5)
(OBJ-AT-O2-P2-P0)
(OBJ-AT-O2-P2-P1)
(OBJ-AT-O2-P2-P2)
(OBJ-AT-O2-P2-P3)
(OBJ-AT-O2-P2-P4)
(OBJ-AT-O2-P2-P5)
(OBJ-AT-O2-P3-P0)
(OBJ-AT-O2-P3-P1)
(OBJ-AT-O2-P3-P2)
(OBJ-AT-O2-P3-P3)
(OBJ-AT-O2-P3-P4)
(OBJ-AT-O2-P3-P5)
(OBJ-AT-O2-P4-P0)
(OBJ-AT-O2-P4-P1)
(OBJ-AT-O2-P4-P2)
(OBJ-AT-O2-P4-P3)
(OBJ-AT-O2-P4-P4)
(OBJ-AT-O2-P4-P5)
(OBJ-AT-O2-P5-P0)
(OBJ-AT-O2-P5-P1)
(OBJ-AT-O2-P5-P2)
(OBJ-AT-O2-P5-P3)
(OBJ-AT-O2-P5-P4)
(OBJ-AT-O2-P5-P5)
(OBJ-AT-O1-P0-P0)
(OBJ-AT-O1-P0-P1)
(OBJ-AT-O1-P0-P2)
(OBJ-AT-O1-P0-P3)
(OBJ-AT-O1-P0-P4)
(OBJ-AT-O1-P0-P5)
(OBJ-AT-O1-P1-P0)
(OBJ-AT-O1-P1-P1)
(OBJ-AT-O1-P1-P2)
(OBJ-AT-O1-P1-P3)
(OBJ-AT-O1-P1-P4)
(OBJ-AT-O1-P1-P5)
(OBJ-AT-O1-P2-P0)
(OBJ-AT-O1-P2-P1)
(OBJ-AT-O1-P2-P2)
(OBJ-AT-O1-P2-P3)
(OBJ-AT-O1-P2-P4)
(OBJ-AT-O1-P3-P0)
(OBJ-AT-O1-P3-P1)
(OBJ-AT-O1-P3-P2)
(OBJ-AT-O1-P3-P3)
(OBJ-AT-O1-P3-P4)
(OBJ-AT-O1-P3-P5)
(OBJ-AT-O1-P4-P0)
(OBJ-AT-O1-P4-P1)
(OBJ-AT-O1-P4-P2)
(OBJ-AT-O1-P4-P3)
(OBJ-AT-O1-P4-P4)
(OBJ-AT-O1-P4-P5)
(OBJ-AT-O1-P5-P0)
(OBJ-AT-O1-P5-P1)
(OBJ-AT-O1-P5-P2)
(OBJ-AT-O1-P5-P3)
(OBJ-AT-O1-P5-P4)
(OBJ-AT-O1-P5-P5)
(OBJ-AT-O0-P0-P0)
(OBJ-AT-O0-P0-P1)
(OBJ-AT-O0-P0-P2)
(OBJ-AT-O0-P0-P3)
(OBJ-AT-O0-P0-P4)
(OBJ-AT-O0-P0-P5)
(OBJ-AT-O0-P1-P0)
(OBJ-AT-O0-P1-P1)
(OBJ-AT-O0-P1-P2)
(OBJ-AT-O0-P1-P3)
(OBJ-AT-O0-P1-P4)
(OBJ-AT-O0-P1-P5)
(OBJ-AT-O0-P2-P0)
(OBJ-AT-O0-P2-P1)
(OBJ-AT-O0-P2-P2)
(OBJ-AT-O0-P2-P3)
(OBJ-AT-O0-P2-P5)
(OBJ-AT-O0-P3-P0)
(OBJ-AT-O0-P3-P1)
(OBJ-AT-O0-P3-P2)
(OBJ-AT-O0-P3-P3)
(OBJ-AT-O0-P3-P4)
(OBJ-AT-O0-P3-P5)
(OBJ-AT-O0-P4-P0)
(OBJ-AT-O0-P4-P1)
(OBJ-AT-O0-P4-P2)
(OBJ-AT-O0-P4-P3)
(OBJ-AT-O0-P4-P4)
(OBJ-AT-O0-P4-P5)
(OBJ-AT-O0-P5-P0)
(OBJ-AT-O0-P5-P1)
(OBJ-AT-O0-P5-P2)
(OBJ-AT-O0-P5-P3)
(OBJ-AT-O0-P5-P4)
(OBJ-AT-O0-P5-P5)
(Flag580-)
(Flag579-)
(Flag578-)
(Flag577-)
(Flag576-)
(Flag575-)
(Flag574-)
(Flag573-)
(Flag572-)
(Flag571-)
(Flag570-)
(Flag569-)
(Flag568-)
(Flag567-)
(Flag566-)
(Flag565-)
(Flag564-)
(Flag563-)
(Flag562-)
(Flag561-)
(Flag560-)
(Flag559-)
(Flag558-)
(Flag557-)
(Flag556-)
(Flag555-)
(Flag554-)
(Flag553-)
(Flag552-)
(Flag551-)
(Flag550-)
(Flag549-)
(Flag548-)
(Flag547-)
(Flag546-)
(Flag545-)
(Flag544-)
(Flag543-)
(Flag542-)
(Flag541-)
(Flag540-)
(Flag539-)
(Flag538-)
(Flag537-)
(Flag536-)
(Flag535-)
(Flag534-)
(Flag533-)
(Flag532-)
(Flag531-)
(Flag530-)
(Flag529-)
(Flag528-)
(Flag527-)
(Flag526-)
(Flag525-)
(Flag524-)
(Flag523-)
(Flag522-)
(Flag521-)
(Flag520-)
(Flag519-)
(Flag518-)
(Flag517-)
(Flag516-)
(Flag515-)
(Flag514-)
(Flag513-)
(Flag512-)
(Flag511-)
(Flag510-)
(Flag509-)
(Flag507-)
(Flag506-)
(Flag505-)
(Flag504-)
(Flag503-)
(Flag502-)
(Flag501-)
(Flag500-)
(Flag499-)
(Flag498-)
(Flag497-)
(Flag496-)
(Flag495-)
(Flag494-)
(Flag493-)
(Flag492-)
(Flag491-)
(Flag490-)
(Flag489-)
(Flag488-)
(Flag487-)
(Flag486-)
(Flag485-)
(Flag484-)
(Flag483-)
(Flag482-)
(Flag481-)
(Flag480-)
(Flag479-)
(Flag478-)
(Flag477-)
(Flag476-)
(Flag475-)
(Flag474-)
(Flag473-)
(Flag472-)
(Flag471-)
(Flag470-)
(Flag469-)
(Flag468-)
(Flag467-)
(Flag466-)
(Flag465-)
(Flag464-)
(Flag463-)
(Flag462-)
(Flag461-)
(Flag460-)
(Flag459-)
(Flag458-)
(Flag457-)
(Flag456-)
(Flag455-)
(Flag454-)
(Flag453-)
(Flag452-)
(Flag451-)
(Flag450-)
(Flag449-)
(Flag448-)
(Flag447-)
(Flag446-)
(Flag445-)
(Flag444-)
(Flag443-)
(Flag442-)
(Flag441-)
(Flag440-)
(Flag439-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag430-)
(Flag429-)
(Flag428-)
(Flag427-)
(Flag426-)
(Flag425-)
(Flag424-)
(Flag423-)
(Flag422-)
(Flag421-)
(Flag420-)
(Flag419-)
(Flag418-)
(Flag417-)
(Flag416-)
(Flag415-)
(Flag414-)
(Flag413-)
(Flag412-)
(Flag411-)
(Flag410-)
(Flag409-)
(Flag408-)
(Flag407-)
(Flag406-)
(Flag405-)
(Flag404-)
(Flag403-)
(Flag402-)
(Flag401-)
(Flag400-)
(Flag399-)
(Flag398-)
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag391-)
(Flag390-)
(Flag389-)
(Flag388-)
(Flag387-)
(Flag386-)
(Flag385-)
(Flag384-)
(Flag383-)
(Flag382-)
(Flag381-)
(Flag380-)
(Flag379-)
(Flag378-)
(Flag377-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag373-)
(Flag372-)
(Flag371-)
(Flag370-)
(Flag369-)
(Flag368-)
(Flag367-)
(Flag366-)
(Flag365-)
(Flag364-)
(Flag363-)
(Flag362-)
(Flag361-)
(Flag360-)
(Flag359-)
(Flag358-)
(Flag357-)
(Flag356-)
(Flag355-)
(Flag354-)
(Flag353-)
(Flag352-)
(Flag351-)
(Flag350-)
(Flag349-)
(Flag348-)
(Flag347-)
(Flag346-)
(Flag345-)
(Flag344-)
(Flag343-)
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag334-)
(Flag333-)
(Flag332-)
(Flag331-)
(Flag330-)
(Flag329-)
(Flag328-)
(Flag327-)
(Flag326-)
(Flag325-)
(Flag324-)
(Flag323-)
(Flag322-)
(Flag321-)
(Flag320-)
(Flag319-)
(Flag318-)
(Flag317-)
(Flag316-)
(Flag315-)
(Flag314-)
(Flag313-)
(Flag312-)
(Flag311-)
(Flag310-)
(Flag309-)
(Flag308-)
(Flag307-)
(Flag306-)
(Flag305-)
(Flag304-)
(Flag303-)
(Flag302-)
(Flag301-)
(Flag300-)
(Flag299-)
(Flag298-)
(Flag297-)
(Flag296-)
(Flag295-)
(Flag294-)
(Flag293-)
(Flag292-)
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag284-)
(Flag283-)
(Flag282-)
(Flag281-)
(Flag280-)
(Flag279-)
(Flag278-)
(Flag277-)
(Flag276-)
(Flag275-)
(Flag274-)
(Flag273-)
(Flag272-)
(Flag271-)
(Flag270-)
(Flag269-)
(Flag268-)
(Flag267-)
(Flag266-)
(Flag265-)
(Flag264-)
(Flag263-)
(Flag262-)
(Flag261-)
(Flag260-)
(Flag259-)
(Flag258-)
(Flag257-)
(Flag256-)
(Flag255-)
(Flag254-)
(Flag253-)
(Flag252-)
(Flag251-)
(Flag250-)
(Flag249-)
(Flag248-)
(Flag247-)
(Flag246-)
(Flag245-)
(Flag244-)
(Flag243-)
(Flag242-)
(Flag241-)
(Flag240-)
(Flag239-)
(Flag238-)
(Flag237-)
(Flag236-)
(Flag235-)
(Flag234-)
(Flag233-)
(Flag232-)
(Flag231-)
(Flag230-)
(Flag229-)
(Flag228-)
(Flag227-)
(Flag226-)
(Flag225-)
(Flag224-)
(Flag223-)
(Flag222-)
(Flag221-)
(Flag220-)
(Flag219-)
(Flag218-)
(Flag217-)
(Flag216-)
(Flag215-)
(Flag214-)
(Flag213-)
(Flag212-)
(Flag211-)
(Flag210-)
(Flag209-)
(Flag208-)
(Flag207-)
(Flag206-)
(Flag205-)
(Flag204-)
(Flag203-)
(Flag202-)
(Flag201-)
(Flag200-)
(Flag199-)
(Flag198-)
(Flag197-)
(Flag196-)
(Flag195-)
(Flag194-)
(Flag193-)
(Flag192-)
(Flag191-)
(Flag190-)
(Flag189-)
(Flag188-)
(Flag187-)
(Flag186-)
(Flag185-)
(Flag184-)
(Flag183-)
(Flag182-)
(Flag181-)
(Flag180-)
(Flag179-)
(Flag178-)
(Flag177-)
(Flag176-)
(Flag175-)
(Flag174-)
(Flag173-)
(Flag172-)
(Flag171-)
(Flag170-)
(Flag169-)
(Flag168-)
(Flag167-)
(Flag166-)
(Flag165-)
(Flag164-)
(Flag163-)
(Flag162-)
(Flag161-)
(Flag160-)
(Flag159-)
(Flag158-)
(Flag157-)
(Flag156-)
(Flag155-)
(Flag154-)
(Flag153-)
(Flag152-)
(Flag151-)
(Flag150-)
(Flag149-)
(Flag148-)
(Flag147-)
(Flag146-)
(Flag145-)
(Flag144-)
(Flag143-)
(Flag142-)
(Flag141-)
(Flag140-)
(Flag139-)
(Flag138-)
(Flag137-)
(Flag136-)
(Flag135-)
(Flag134-)
(Flag133-)
(Flag132-)
(Flag131-)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag126-)
(Flag125-)
(Flag124-)
(Flag123-)
(Flag122-)
(Flag121-)
(Flag120-)
(Flag119-)
(Flag118-)
(Flag117-)
(Flag116-)
(Flag115-)
(Flag114-)
(Flag113-)
(Flag112-)
(Flag111-)
(Flag110-)
(Flag109-)
(Flag108-)
(Flag107-)
(Flag106-)
(Flag105-)
(Flag104-)
(Flag103-)
(Flag102-)
(Flag101-)
(Flag100-)
(Flag99-)
(Flag98-)
(Flag97-)
(Flag96-)
(Flag95-)
(Flag94-)
(Flag93-)
(Flag92-)
(Flag91-)
(Flag90-)
(Flag89-)
(Flag88-)
(Flag87-)
(Flag86-)
(Flag85-)
(Flag84-)
(Flag83-)
(Flag82-)
(Flag81-)
(Flag80-)
(Flag79-)
(Flag78-)
(Flag77-)
(Flag76-)
(Flag75-)
(Flag74-)
(Flag73-)
(Flag72-)
(Flag71-)
(Flag70-)
(Flag69-)
(Flag68-)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag64-)
(Flag63-)
(Flag62-)
(Flag61-)
(Flag60-)
(Flag59-)
(Flag58-)
(Flag57-)
(Flag56-)
(Flag55-)
(Flag54-)
(Flag53-)
(Flag52-)
(Flag51-)
(Flag50-)
(Flag49-)
(Flag48-)
(Flag47-)
(Flag46-)
(Flag45-)
(Flag44-)
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag581-)
(Flag508-)
(Flag582-)
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(OBJ-AT-O1-P2-P5)
(OBJ-AT-O2-P1-P1)
(OBJ-AT-O3-P0-P3)
(OBJ-AT-O4-P0-P4)
(OBJ-AT-O5-P3-P0)
(OBJ-AT-O6-P0-P2)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P0-P0)
(OBJ-AT-O8-P0-P0)
)
(:derived (Flag582-)(and (Flag1-)(Flag2-)(Flag3-)(Flag508-)(Flag581-)))

(:derived (Flag508-)(and (Flag4-)))

(:derived (Flag508-)(and (Flag5-)))

(:derived (Flag508-)(and (Flag6-)))

(:derived (Flag508-)(and (Flag7-)))

(:derived (Flag508-)(and (Flag8-)))

(:derived (Flag508-)(and (Flag9-)))

(:derived (Flag508-)(and (Flag10-)))

(:derived (Flag508-)(and (Flag11-)))

(:derived (Flag508-)(and (Flag12-)))

(:derived (Flag508-)(and (Flag13-)))

(:derived (Flag508-)(and (Flag14-)))

(:derived (Flag508-)(and (Flag15-)))

(:derived (Flag508-)(and (Flag16-)))

(:derived (Flag508-)(and (Flag17-)))

(:derived (Flag508-)(and (Flag18-)))

(:derived (Flag508-)(and (Flag19-)))

(:derived (Flag508-)(and (Flag20-)))

(:derived (Flag508-)(and (Flag21-)))

(:derived (Flag508-)(and (Flag22-)))

(:derived (Flag508-)(and (Flag23-)))

(:derived (Flag508-)(and (Flag24-)))

(:derived (Flag508-)(and (Flag25-)))

(:derived (Flag508-)(and (Flag26-)))

(:derived (Flag508-)(and (Flag27-)))

(:derived (Flag508-)(and (Flag28-)))

(:derived (Flag508-)(and (Flag29-)))

(:derived (Flag508-)(and (Flag30-)))

(:derived (Flag508-)(and (Flag31-)))

(:derived (Flag508-)(and (Flag32-)))

(:derived (Flag508-)(and (Flag33-)))

(:derived (Flag508-)(and (Flag34-)))

(:derived (Flag508-)(and (Flag35-)))

(:derived (Flag508-)(and (Flag36-)))

(:derived (Flag508-)(and (Flag37-)))

(:derived (Flag508-)(and (Flag38-)))

(:derived (Flag508-)(and (Flag39-)))

(:derived (Flag508-)(and (Flag40-)))

(:derived (Flag508-)(and (Flag41-)))

(:derived (Flag508-)(and (Flag42-)))

(:derived (Flag508-)(and (Flag43-)))

(:derived (Flag508-)(and (Flag44-)))

(:derived (Flag508-)(and (Flag45-)))

(:derived (Flag508-)(and (Flag46-)))

(:derived (Flag508-)(and (Flag47-)))

(:derived (Flag508-)(and (Flag48-)))

(:derived (Flag508-)(and (Flag49-)))

(:derived (Flag508-)(and (Flag50-)))

(:derived (Flag508-)(and (Flag51-)))

(:derived (Flag508-)(and (Flag52-)))

(:derived (Flag508-)(and (Flag53-)))

(:derived (Flag508-)(and (Flag54-)))

(:derived (Flag508-)(and (Flag55-)))

(:derived (Flag508-)(and (Flag56-)))

(:derived (Flag508-)(and (Flag57-)))

(:derived (Flag508-)(and (Flag58-)))

(:derived (Flag508-)(and (Flag59-)))

(:derived (Flag508-)(and (Flag60-)))

(:derived (Flag508-)(and (Flag61-)))

(:derived (Flag508-)(and (Flag62-)))

(:derived (Flag508-)(and (Flag63-)))

(:derived (Flag508-)(and (Flag64-)))

(:derived (Flag508-)(and (Flag65-)))

(:derived (Flag508-)(and (Flag66-)))

(:derived (Flag508-)(and (Flag67-)))

(:derived (Flag508-)(and (Flag68-)))

(:derived (Flag508-)(and (Flag69-)))

(:derived (Flag508-)(and (Flag70-)))

(:derived (Flag508-)(and (Flag71-)))

(:derived (Flag508-)(and (Flag72-)))

(:derived (Flag508-)(and (Flag73-)))

(:derived (Flag508-)(and (Flag74-)))

(:derived (Flag508-)(and (Flag75-)))

(:derived (Flag508-)(and (Flag76-)))

(:derived (Flag508-)(and (Flag77-)))

(:derived (Flag508-)(and (Flag78-)))

(:derived (Flag508-)(and (Flag79-)))

(:derived (Flag508-)(and (Flag80-)))

(:derived (Flag508-)(and (Flag81-)))

(:derived (Flag508-)(and (Flag82-)))

(:derived (Flag508-)(and (Flag83-)))

(:derived (Flag508-)(and (Flag84-)))

(:derived (Flag508-)(and (Flag85-)))

(:derived (Flag508-)(and (Flag86-)))

(:derived (Flag508-)(and (Flag87-)))

(:derived (Flag508-)(and (Flag88-)))

(:derived (Flag508-)(and (Flag89-)))

(:derived (Flag508-)(and (Flag90-)))

(:derived (Flag508-)(and (Flag91-)))

(:derived (Flag508-)(and (Flag92-)))

(:derived (Flag508-)(and (Flag93-)))

(:derived (Flag508-)(and (Flag94-)))

(:derived (Flag508-)(and (Flag95-)))

(:derived (Flag508-)(and (Flag96-)))

(:derived (Flag508-)(and (Flag97-)))

(:derived (Flag508-)(and (Flag98-)))

(:derived (Flag508-)(and (Flag99-)))

(:derived (Flag508-)(and (Flag100-)))

(:derived (Flag508-)(and (Flag101-)))

(:derived (Flag508-)(and (Flag102-)))

(:derived (Flag508-)(and (Flag103-)))

(:derived (Flag508-)(and (Flag104-)))

(:derived (Flag508-)(and (Flag105-)))

(:derived (Flag508-)(and (Flag106-)))

(:derived (Flag508-)(and (Flag107-)))

(:derived (Flag508-)(and (Flag108-)))

(:derived (Flag508-)(and (Flag109-)))

(:derived (Flag508-)(and (Flag110-)))

(:derived (Flag508-)(and (Flag111-)))

(:derived (Flag508-)(and (Flag112-)))

(:derived (Flag508-)(and (Flag113-)))

(:derived (Flag508-)(and (Flag114-)))

(:derived (Flag508-)(and (Flag115-)))

(:derived (Flag508-)(and (Flag116-)))

(:derived (Flag508-)(and (Flag117-)))

(:derived (Flag508-)(and (Flag118-)))

(:derived (Flag508-)(and (Flag119-)))

(:derived (Flag508-)(and (Flag120-)))

(:derived (Flag508-)(and (Flag121-)))

(:derived (Flag508-)(and (Flag122-)))

(:derived (Flag508-)(and (Flag123-)))

(:derived (Flag508-)(and (Flag124-)))

(:derived (Flag508-)(and (Flag125-)))

(:derived (Flag508-)(and (Flag126-)))

(:derived (Flag508-)(and (Flag127-)))

(:derived (Flag508-)(and (Flag128-)))

(:derived (Flag508-)(and (Flag129-)))

(:derived (Flag508-)(and (Flag130-)))

(:derived (Flag508-)(and (Flag131-)))

(:derived (Flag508-)(and (Flag132-)))

(:derived (Flag508-)(and (Flag133-)))

(:derived (Flag508-)(and (Flag134-)))

(:derived (Flag508-)(and (Flag135-)))

(:derived (Flag508-)(and (Flag136-)))

(:derived (Flag508-)(and (Flag137-)))

(:derived (Flag508-)(and (Flag138-)))

(:derived (Flag508-)(and (Flag139-)))

(:derived (Flag508-)(and (Flag140-)))

(:derived (Flag508-)(and (Flag141-)))

(:derived (Flag508-)(and (Flag142-)))

(:derived (Flag508-)(and (Flag143-)))

(:derived (Flag508-)(and (Flag144-)))

(:derived (Flag508-)(and (Flag145-)))

(:derived (Flag508-)(and (Flag146-)))

(:derived (Flag508-)(and (Flag147-)))

(:derived (Flag508-)(and (Flag148-)))

(:derived (Flag508-)(and (Flag149-)))

(:derived (Flag508-)(and (Flag150-)))

(:derived (Flag508-)(and (Flag151-)))

(:derived (Flag508-)(and (Flag152-)))

(:derived (Flag508-)(and (Flag153-)))

(:derived (Flag508-)(and (Flag154-)))

(:derived (Flag508-)(and (Flag155-)))

(:derived (Flag508-)(and (Flag156-)))

(:derived (Flag508-)(and (Flag157-)))

(:derived (Flag508-)(and (Flag158-)))

(:derived (Flag508-)(and (Flag159-)))

(:derived (Flag508-)(and (Flag160-)))

(:derived (Flag508-)(and (Flag161-)))

(:derived (Flag508-)(and (Flag162-)))

(:derived (Flag508-)(and (Flag163-)))

(:derived (Flag508-)(and (Flag164-)))

(:derived (Flag508-)(and (Flag165-)))

(:derived (Flag508-)(and (Flag166-)))

(:derived (Flag508-)(and (Flag167-)))

(:derived (Flag508-)(and (Flag168-)))

(:derived (Flag508-)(and (Flag169-)))

(:derived (Flag508-)(and (Flag170-)))

(:derived (Flag508-)(and (Flag171-)))

(:derived (Flag508-)(and (Flag172-)))

(:derived (Flag508-)(and (Flag173-)))

(:derived (Flag508-)(and (Flag174-)))

(:derived (Flag508-)(and (Flag175-)))

(:derived (Flag508-)(and (Flag176-)))

(:derived (Flag508-)(and (Flag177-)))

(:derived (Flag508-)(and (Flag178-)))

(:derived (Flag508-)(and (Flag179-)))

(:derived (Flag508-)(and (Flag180-)))

(:derived (Flag508-)(and (Flag181-)))

(:derived (Flag508-)(and (Flag182-)))

(:derived (Flag508-)(and (Flag183-)))

(:derived (Flag508-)(and (Flag184-)))

(:derived (Flag508-)(and (Flag185-)))

(:derived (Flag508-)(and (Flag186-)))

(:derived (Flag508-)(and (Flag187-)))

(:derived (Flag508-)(and (Flag188-)))

(:derived (Flag508-)(and (Flag189-)))

(:derived (Flag508-)(and (Flag190-)))

(:derived (Flag508-)(and (Flag191-)))

(:derived (Flag508-)(and (Flag192-)))

(:derived (Flag508-)(and (Flag193-)))

(:derived (Flag508-)(and (Flag194-)))

(:derived (Flag508-)(and (Flag195-)))

(:derived (Flag508-)(and (Flag196-)))

(:derived (Flag508-)(and (Flag197-)))

(:derived (Flag508-)(and (Flag198-)))

(:derived (Flag508-)(and (Flag199-)))

(:derived (Flag508-)(and (Flag200-)))

(:derived (Flag508-)(and (Flag201-)))

(:derived (Flag508-)(and (Flag202-)))

(:derived (Flag508-)(and (Flag203-)))

(:derived (Flag508-)(and (Flag204-)))

(:derived (Flag508-)(and (Flag205-)))

(:derived (Flag508-)(and (Flag206-)))

(:derived (Flag508-)(and (Flag207-)))

(:derived (Flag508-)(and (Flag208-)))

(:derived (Flag508-)(and (Flag209-)))

(:derived (Flag508-)(and (Flag210-)))

(:derived (Flag508-)(and (Flag211-)))

(:derived (Flag508-)(and (Flag212-)))

(:derived (Flag508-)(and (Flag213-)))

(:derived (Flag508-)(and (Flag214-)))

(:derived (Flag508-)(and (Flag215-)))

(:derived (Flag508-)(and (Flag216-)))

(:derived (Flag508-)(and (Flag217-)))

(:derived (Flag508-)(and (Flag218-)))

(:derived (Flag508-)(and (Flag219-)))

(:derived (Flag508-)(and (Flag220-)))

(:derived (Flag508-)(and (Flag221-)))

(:derived (Flag508-)(and (Flag222-)))

(:derived (Flag508-)(and (Flag223-)))

(:derived (Flag508-)(and (Flag224-)))

(:derived (Flag508-)(and (Flag225-)))

(:derived (Flag508-)(and (Flag226-)))

(:derived (Flag508-)(and (Flag227-)))

(:derived (Flag508-)(and (Flag228-)))

(:derived (Flag508-)(and (Flag229-)))

(:derived (Flag508-)(and (Flag230-)))

(:derived (Flag508-)(and (Flag231-)))

(:derived (Flag508-)(and (Flag232-)))

(:derived (Flag508-)(and (Flag233-)))

(:derived (Flag508-)(and (Flag234-)))

(:derived (Flag508-)(and (Flag235-)))

(:derived (Flag508-)(and (Flag236-)))

(:derived (Flag508-)(and (Flag237-)))

(:derived (Flag508-)(and (Flag238-)))

(:derived (Flag508-)(and (Flag239-)))

(:derived (Flag508-)(and (Flag240-)))

(:derived (Flag508-)(and (Flag241-)))

(:derived (Flag508-)(and (Flag242-)))

(:derived (Flag508-)(and (Flag243-)))

(:derived (Flag508-)(and (Flag244-)))

(:derived (Flag508-)(and (Flag245-)))

(:derived (Flag508-)(and (Flag246-)))

(:derived (Flag508-)(and (Flag247-)))

(:derived (Flag508-)(and (Flag248-)))

(:derived (Flag508-)(and (Flag249-)))

(:derived (Flag508-)(and (Flag250-)))

(:derived (Flag508-)(and (Flag251-)))

(:derived (Flag508-)(and (Flag252-)))

(:derived (Flag508-)(and (Flag253-)))

(:derived (Flag508-)(and (Flag254-)))

(:derived (Flag508-)(and (Flag255-)))

(:derived (Flag508-)(and (Flag256-)))

(:derived (Flag508-)(and (Flag257-)))

(:derived (Flag508-)(and (Flag258-)))

(:derived (Flag508-)(and (Flag259-)))

(:derived (Flag508-)(and (Flag260-)))

(:derived (Flag508-)(and (Flag261-)))

(:derived (Flag508-)(and (Flag262-)))

(:derived (Flag508-)(and (Flag263-)))

(:derived (Flag508-)(and (Flag264-)))

(:derived (Flag508-)(and (Flag265-)))

(:derived (Flag508-)(and (Flag266-)))

(:derived (Flag508-)(and (Flag267-)))

(:derived (Flag508-)(and (Flag268-)))

(:derived (Flag508-)(and (Flag269-)))

(:derived (Flag508-)(and (Flag270-)))

(:derived (Flag508-)(and (Flag271-)))

(:derived (Flag508-)(and (Flag272-)))

(:derived (Flag508-)(and (Flag273-)))

(:derived (Flag508-)(and (Flag274-)))

(:derived (Flag508-)(and (Flag275-)))

(:derived (Flag508-)(and (Flag276-)))

(:derived (Flag508-)(and (Flag277-)))

(:derived (Flag508-)(and (Flag278-)))

(:derived (Flag508-)(and (Flag279-)))

(:derived (Flag508-)(and (Flag280-)))

(:derived (Flag508-)(and (Flag281-)))

(:derived (Flag508-)(and (Flag282-)))

(:derived (Flag508-)(and (Flag283-)))

(:derived (Flag508-)(and (Flag284-)))

(:derived (Flag508-)(and (Flag285-)))

(:derived (Flag508-)(and (Flag286-)))

(:derived (Flag508-)(and (Flag287-)))

(:derived (Flag508-)(and (Flag288-)))

(:derived (Flag508-)(and (Flag289-)))

(:derived (Flag508-)(and (Flag290-)))

(:derived (Flag508-)(and (Flag291-)))

(:derived (Flag508-)(and (Flag292-)))

(:derived (Flag508-)(and (Flag293-)))

(:derived (Flag508-)(and (Flag294-)))

(:derived (Flag508-)(and (Flag295-)))

(:derived (Flag508-)(and (Flag296-)))

(:derived (Flag508-)(and (Flag297-)))

(:derived (Flag508-)(and (Flag298-)))

(:derived (Flag508-)(and (Flag299-)))

(:derived (Flag508-)(and (Flag300-)))

(:derived (Flag508-)(and (Flag301-)))

(:derived (Flag508-)(and (Flag302-)))

(:derived (Flag508-)(and (Flag303-)))

(:derived (Flag508-)(and (Flag304-)))

(:derived (Flag508-)(and (Flag305-)))

(:derived (Flag508-)(and (Flag306-)))

(:derived (Flag508-)(and (Flag307-)))

(:derived (Flag508-)(and (Flag308-)))

(:derived (Flag508-)(and (Flag309-)))

(:derived (Flag508-)(and (Flag310-)))

(:derived (Flag508-)(and (Flag311-)))

(:derived (Flag508-)(and (Flag312-)))

(:derived (Flag508-)(and (Flag313-)))

(:derived (Flag508-)(and (Flag314-)))

(:derived (Flag508-)(and (Flag315-)))

(:derived (Flag508-)(and (Flag316-)))

(:derived (Flag508-)(and (Flag317-)))

(:derived (Flag508-)(and (Flag318-)))

(:derived (Flag508-)(and (Flag319-)))

(:derived (Flag508-)(and (Flag320-)))

(:derived (Flag508-)(and (Flag321-)))

(:derived (Flag508-)(and (Flag322-)))

(:derived (Flag508-)(and (Flag323-)))

(:derived (Flag508-)(and (Flag324-)))

(:derived (Flag508-)(and (Flag325-)))

(:derived (Flag508-)(and (Flag326-)))

(:derived (Flag508-)(and (Flag327-)))

(:derived (Flag508-)(and (Flag328-)))

(:derived (Flag508-)(and (Flag329-)))

(:derived (Flag508-)(and (Flag330-)))

(:derived (Flag508-)(and (Flag331-)))

(:derived (Flag508-)(and (Flag332-)))

(:derived (Flag508-)(and (Flag333-)))

(:derived (Flag508-)(and (Flag334-)))

(:derived (Flag508-)(and (Flag335-)))

(:derived (Flag508-)(and (Flag336-)))

(:derived (Flag508-)(and (Flag337-)))

(:derived (Flag508-)(and (Flag338-)))

(:derived (Flag508-)(and (Flag339-)))

(:derived (Flag508-)(and (Flag340-)))

(:derived (Flag508-)(and (Flag341-)))

(:derived (Flag508-)(and (Flag342-)))

(:derived (Flag508-)(and (Flag343-)))

(:derived (Flag508-)(and (Flag344-)))

(:derived (Flag508-)(and (Flag345-)))

(:derived (Flag508-)(and (Flag346-)))

(:derived (Flag508-)(and (Flag347-)))

(:derived (Flag508-)(and (Flag348-)))

(:derived (Flag508-)(and (Flag349-)))

(:derived (Flag508-)(and (Flag350-)))

(:derived (Flag508-)(and (Flag351-)))

(:derived (Flag508-)(and (Flag352-)))

(:derived (Flag508-)(and (Flag353-)))

(:derived (Flag508-)(and (Flag354-)))

(:derived (Flag508-)(and (Flag355-)))

(:derived (Flag508-)(and (Flag356-)))

(:derived (Flag508-)(and (Flag357-)))

(:derived (Flag508-)(and (Flag358-)))

(:derived (Flag508-)(and (Flag359-)))

(:derived (Flag508-)(and (Flag360-)))

(:derived (Flag508-)(and (Flag361-)))

(:derived (Flag508-)(and (Flag362-)))

(:derived (Flag508-)(and (Flag363-)))

(:derived (Flag508-)(and (Flag364-)))

(:derived (Flag508-)(and (Flag365-)))

(:derived (Flag508-)(and (Flag366-)))

(:derived (Flag508-)(and (Flag367-)))

(:derived (Flag508-)(and (Flag368-)))

(:derived (Flag508-)(and (Flag369-)))

(:derived (Flag508-)(and (Flag370-)))

(:derived (Flag508-)(and (Flag371-)))

(:derived (Flag508-)(and (Flag372-)))

(:derived (Flag508-)(and (Flag373-)))

(:derived (Flag508-)(and (Flag374-)))

(:derived (Flag508-)(and (Flag375-)))

(:derived (Flag508-)(and (Flag376-)))

(:derived (Flag508-)(and (Flag377-)))

(:derived (Flag508-)(and (Flag378-)))

(:derived (Flag508-)(and (Flag379-)))

(:derived (Flag508-)(and (Flag380-)))

(:derived (Flag508-)(and (Flag381-)))

(:derived (Flag508-)(and (Flag382-)))

(:derived (Flag508-)(and (Flag383-)))

(:derived (Flag508-)(and (Flag384-)))

(:derived (Flag508-)(and (Flag385-)))

(:derived (Flag508-)(and (Flag386-)))

(:derived (Flag508-)(and (Flag387-)))

(:derived (Flag508-)(and (Flag388-)))

(:derived (Flag508-)(and (Flag389-)))

(:derived (Flag508-)(and (Flag390-)))

(:derived (Flag508-)(and (Flag391-)))

(:derived (Flag508-)(and (Flag392-)))

(:derived (Flag508-)(and (Flag393-)))

(:derived (Flag508-)(and (Flag394-)))

(:derived (Flag508-)(and (Flag395-)))

(:derived (Flag508-)(and (Flag396-)))

(:derived (Flag508-)(and (Flag397-)))

(:derived (Flag508-)(and (Flag398-)))

(:derived (Flag508-)(and (Flag399-)))

(:derived (Flag508-)(and (Flag400-)))

(:derived (Flag508-)(and (Flag401-)))

(:derived (Flag508-)(and (Flag402-)))

(:derived (Flag508-)(and (Flag403-)))

(:derived (Flag508-)(and (Flag404-)))

(:derived (Flag508-)(and (Flag405-)))

(:derived (Flag508-)(and (Flag406-)))

(:derived (Flag508-)(and (Flag407-)))

(:derived (Flag508-)(and (Flag408-)))

(:derived (Flag508-)(and (Flag409-)))

(:derived (Flag508-)(and (Flag410-)))

(:derived (Flag508-)(and (Flag411-)))

(:derived (Flag508-)(and (Flag412-)))

(:derived (Flag508-)(and (Flag413-)))

(:derived (Flag508-)(and (Flag414-)))

(:derived (Flag508-)(and (Flag415-)))

(:derived (Flag508-)(and (Flag416-)))

(:derived (Flag508-)(and (Flag417-)))

(:derived (Flag508-)(and (Flag418-)))

(:derived (Flag508-)(and (Flag419-)))

(:derived (Flag508-)(and (Flag420-)))

(:derived (Flag508-)(and (Flag421-)))

(:derived (Flag508-)(and (Flag422-)))

(:derived (Flag508-)(and (Flag423-)))

(:derived (Flag508-)(and (Flag424-)))

(:derived (Flag508-)(and (Flag425-)))

(:derived (Flag508-)(and (Flag426-)))

(:derived (Flag508-)(and (Flag427-)))

(:derived (Flag508-)(and (Flag428-)))

(:derived (Flag508-)(and (Flag429-)))

(:derived (Flag508-)(and (Flag430-)))

(:derived (Flag508-)(and (Flag431-)))

(:derived (Flag508-)(and (Flag432-)))

(:derived (Flag508-)(and (Flag433-)))

(:derived (Flag508-)(and (Flag434-)))

(:derived (Flag508-)(and (Flag435-)))

(:derived (Flag508-)(and (Flag436-)))

(:derived (Flag508-)(and (Flag437-)))

(:derived (Flag508-)(and (Flag438-)))

(:derived (Flag508-)(and (Flag439-)))

(:derived (Flag508-)(and (Flag440-)))

(:derived (Flag508-)(and (Flag441-)))

(:derived (Flag508-)(and (Flag442-)))

(:derived (Flag508-)(and (Flag443-)))

(:derived (Flag508-)(and (Flag444-)))

(:derived (Flag508-)(and (Flag445-)))

(:derived (Flag508-)(and (Flag446-)))

(:derived (Flag508-)(and (Flag447-)))

(:derived (Flag508-)(and (Flag448-)))

(:derived (Flag508-)(and (Flag449-)))

(:derived (Flag508-)(and (Flag450-)))

(:derived (Flag508-)(and (Flag451-)))

(:derived (Flag508-)(and (Flag452-)))

(:derived (Flag508-)(and (Flag453-)))

(:derived (Flag508-)(and (Flag454-)))

(:derived (Flag508-)(and (Flag455-)))

(:derived (Flag508-)(and (Flag456-)))

(:derived (Flag508-)(and (Flag457-)))

(:derived (Flag508-)(and (Flag458-)))

(:derived (Flag508-)(and (Flag459-)))

(:derived (Flag508-)(and (Flag460-)))

(:derived (Flag508-)(and (Flag461-)))

(:derived (Flag508-)(and (Flag462-)))

(:derived (Flag508-)(and (Flag463-)))

(:derived (Flag508-)(and (Flag464-)))

(:derived (Flag508-)(and (Flag465-)))

(:derived (Flag508-)(and (Flag466-)))

(:derived (Flag508-)(and (Flag467-)))

(:derived (Flag508-)(and (Flag468-)))

(:derived (Flag508-)(and (Flag469-)))

(:derived (Flag508-)(and (Flag470-)))

(:derived (Flag508-)(and (Flag471-)))

(:derived (Flag508-)(and (Flag472-)))

(:derived (Flag508-)(and (Flag473-)))

(:derived (Flag508-)(and (Flag474-)))

(:derived (Flag508-)(and (Flag475-)))

(:derived (Flag508-)(and (Flag476-)))

(:derived (Flag508-)(and (Flag477-)))

(:derived (Flag508-)(and (Flag478-)))

(:derived (Flag508-)(and (Flag479-)))

(:derived (Flag508-)(and (Flag480-)))

(:derived (Flag508-)(and (Flag481-)))

(:derived (Flag508-)(and (Flag482-)))

(:derived (Flag508-)(and (Flag483-)))

(:derived (Flag508-)(and (Flag484-)))

(:derived (Flag508-)(and (Flag485-)))

(:derived (Flag508-)(and (Flag486-)))

(:derived (Flag508-)(and (Flag487-)))

(:derived (Flag508-)(and (Flag488-)))

(:derived (Flag508-)(and (Flag489-)))

(:derived (Flag508-)(and (Flag490-)))

(:derived (Flag508-)(and (Flag491-)))

(:derived (Flag508-)(and (Flag492-)))

(:derived (Flag508-)(and (Flag493-)))

(:derived (Flag508-)(and (Flag494-)))

(:derived (Flag508-)(and (Flag495-)))

(:derived (Flag508-)(and (Flag496-)))

(:derived (Flag508-)(and (Flag497-)))

(:derived (Flag508-)(and (Flag498-)))

(:derived (Flag508-)(and (Flag499-)))

(:derived (Flag508-)(and (Flag500-)))

(:derived (Flag508-)(and (Flag501-)))

(:derived (Flag508-)(and (Flag502-)))

(:derived (Flag508-)(and (Flag503-)))

(:derived (Flag508-)(and (Flag504-)))

(:derived (Flag508-)(and (Flag505-)))

(:derived (Flag508-)(and (Flag506-)))

(:derived (Flag508-)(and (Flag507-)))

(:derived (Flag581-)(and (Flag509-)))

(:derived (Flag581-)(and (Flag510-)))

(:derived (Flag581-)(and (Flag511-)))

(:derived (Flag581-)(and (Flag512-)))

(:derived (Flag581-)(and (Flag513-)))

(:derived (Flag581-)(and (Flag514-)))

(:derived (Flag581-)(and (Flag515-)))

(:derived (Flag581-)(and (Flag516-)))

(:derived (Flag581-)(and (Flag517-)))

(:derived (Flag581-)(and (Flag518-)))

(:derived (Flag581-)(and (Flag519-)))

(:derived (Flag581-)(and (Flag520-)))

(:derived (Flag581-)(and (Flag521-)))

(:derived (Flag581-)(and (Flag522-)))

(:derived (Flag581-)(and (Flag523-)))

(:derived (Flag581-)(and (Flag524-)))

(:derived (Flag581-)(and (Flag525-)))

(:derived (Flag581-)(and (Flag526-)))

(:derived (Flag581-)(and (Flag527-)))

(:derived (Flag581-)(and (Flag528-)))

(:derived (Flag581-)(and (Flag529-)))

(:derived (Flag581-)(and (Flag530-)))

(:derived (Flag581-)(and (Flag531-)))

(:derived (Flag581-)(and (Flag532-)))

(:derived (Flag581-)(and (Flag533-)))

(:derived (Flag581-)(and (Flag534-)))

(:derived (Flag581-)(and (Flag535-)))

(:derived (Flag581-)(and (Flag536-)))

(:derived (Flag581-)(and (Flag537-)))

(:derived (Flag581-)(and (Flag538-)))

(:derived (Flag581-)(and (Flag539-)))

(:derived (Flag581-)(and (Flag540-)))

(:derived (Flag581-)(and (Flag541-)))

(:derived (Flag581-)(and (Flag542-)))

(:derived (Flag581-)(and (Flag543-)))

(:derived (Flag581-)(and (Flag544-)))

(:derived (Flag581-)(and (Flag545-)))

(:derived (Flag581-)(and (Flag546-)))

(:derived (Flag581-)(and (Flag547-)))

(:derived (Flag581-)(and (Flag548-)))

(:derived (Flag581-)(and (Flag549-)))

(:derived (Flag581-)(and (Flag550-)))

(:derived (Flag581-)(and (Flag551-)))

(:derived (Flag581-)(and (Flag552-)))

(:derived (Flag581-)(and (Flag553-)))

(:derived (Flag581-)(and (Flag554-)))

(:derived (Flag581-)(and (Flag555-)))

(:derived (Flag581-)(and (Flag556-)))

(:derived (Flag581-)(and (Flag557-)))

(:derived (Flag581-)(and (Flag558-)))

(:derived (Flag581-)(and (Flag559-)))

(:derived (Flag581-)(and (Flag560-)))

(:derived (Flag581-)(and (Flag561-)))

(:derived (Flag581-)(and (Flag562-)))

(:derived (Flag581-)(and (Flag563-)))

(:derived (Flag581-)(and (Flag564-)))

(:derived (Flag581-)(and (Flag565-)))

(:derived (Flag581-)(and (Flag566-)))

(:derived (Flag581-)(and (Flag567-)))

(:derived (Flag581-)(and (Flag568-)))

(:derived (Flag581-)(and (Flag569-)))

(:derived (Flag581-)(and (Flag570-)))

(:derived (Flag581-)(and (Flag571-)))

(:derived (Flag581-)(and (Flag572-)))

(:derived (Flag581-)(and (Flag573-)))

(:derived (Flag581-)(and (Flag574-)))

(:derived (Flag581-)(and (Flag575-)))

(:derived (Flag581-)(and (Flag576-)))

(:derived (Flag581-)(and (Flag577-)))

(:derived (Flag581-)(and (Flag578-)))

(:derived (Flag581-)(and (Flag579-)))

(:derived (Flag581-)(and (Flag580-)))

(:action PICKUP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P5))
)
)
(:action PICKUP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P4))
)
)
(:action PICKUP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P3))
)
)
(:action PICKUP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P2))
)
)
(:action PICKUP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P1))
)
)
(:action PICKUP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P5-P0))
)
)
(:action PICKUP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P5))
)
)
(:action PICKUP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P4))
)
)
(:action PICKUP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P3))
)
)
(:action PICKUP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P2))
)
)
(:action PICKUP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P1))
)
)
(:action PICKUP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P4-P0))
)
)
(:action PICKUP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P5))
)
)
(:action PICKUP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P4))
)
)
(:action PICKUP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P3))
)
)
(:action PICKUP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P2))
)
)
(:action PICKUP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P1))
)
)
(:action PICKUP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P3-P0))
)
)
(:action PICKUP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P5))
)
)
(:action PICKUP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P3))
)
)
(:action PICKUP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P2))
)
)
(:action PICKUP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P1))
)
)
(:action PICKUP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P0))
)
)
(:action PICKUP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P5))
)
)
(:action PICKUP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P4))
)
)
(:action PICKUP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P3))
)
)
(:action PICKUP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P2))
)
)
(:action PICKUP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P1))
)
)
(:action PICKUP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P1-P0))
)
)
(:action PICKUP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P5))
)
)
(:action PICKUP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P4))
)
)
(:action PICKUP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P3))
)
)
(:action PICKUP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P2))
)
)
(:action PICKUP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P1))
)
)
(:action PICKUP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P0-P0))
)
)
(:action PICKUP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P5))
)
)
(:action PICKUP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P4))
)
)
(:action PICKUP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P3))
)
)
(:action PICKUP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P2))
)
)
(:action PICKUP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P1))
)
)
(:action PICKUP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P5-P0))
)
)
(:action PICKUP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P5))
)
)
(:action PICKUP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P4))
)
)
(:action PICKUP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P3))
)
)
(:action PICKUP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P2))
)
)
(:action PICKUP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P1))
)
)
(:action PICKUP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P4-P0))
)
)
(:action PICKUP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P5))
)
)
(:action PICKUP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P4))
)
)
(:action PICKUP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P3))
)
)
(:action PICKUP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P2))
)
)
(:action PICKUP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P1))
)
)
(:action PICKUP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P3-P0))
)
)
(:action PICKUP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P4))
)
)
(:action PICKUP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P3))
)
)
(:action PICKUP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P2))
)
)
(:action PICKUP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P1))
)
)
(:action PICKUP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P0))
)
)
(:action PICKUP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P5))
)
)
(:action PICKUP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P4))
)
)
(:action PICKUP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P3))
)
)
(:action PICKUP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P2))
)
)
(:action PICKUP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P1))
)
)
(:action PICKUP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P1-P0))
)
)
(:action PICKUP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P5))
)
)
(:action PICKUP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P4))
)
)
(:action PICKUP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P3))
)
)
(:action PICKUP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P2))
)
)
(:action PICKUP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P1))
)
)
(:action PICKUP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P0-P0))
)
)
(:action PICKUP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P5))
)
)
(:action PICKUP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P4))
)
)
(:action PICKUP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P3))
)
)
(:action PICKUP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P2))
)
)
(:action PICKUP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P1))
)
)
(:action PICKUP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P5-P0))
)
)
(:action PICKUP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P5))
)
)
(:action PICKUP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P4))
)
)
(:action PICKUP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P3))
)
)
(:action PICKUP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P2))
)
)
(:action PICKUP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P1))
)
)
(:action PICKUP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P4-P0))
)
)
(:action PICKUP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P5))
)
)
(:action PICKUP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P4))
)
)
(:action PICKUP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P3))
)
)
(:action PICKUP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P2))
)
)
(:action PICKUP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P1))
)
)
(:action PICKUP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P3-P0))
)
)
(:action PICKUP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P5))
)
)
(:action PICKUP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P4))
)
)
(:action PICKUP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P3))
)
)
(:action PICKUP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P2))
)
)
(:action PICKUP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P1))
)
)
(:action PICKUP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P2-P0))
)
)
(:action PICKUP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P5))
)
)
(:action PICKUP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P4))
)
)
(:action PICKUP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P3))
)
)
(:action PICKUP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P2))
)
)
(:action PICKUP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P0))
)
)
(:action PICKUP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P5))
)
)
(:action PICKUP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P4))
)
)
(:action PICKUP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P3))
)
)
(:action PICKUP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P2))
)
)
(:action PICKUP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P1))
)
)
(:action PICKUP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P0-P0))
)
)
(:action PICKUP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P5))
)
)
(:action PICKUP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P4))
)
)
(:action PICKUP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P3))
)
)
(:action PICKUP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P2))
)
)
(:action PICKUP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P1))
)
)
(:action PICKUP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P5-P0))
)
)
(:action PICKUP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P5))
)
)
(:action PICKUP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P4))
)
)
(:action PICKUP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P3))
)
)
(:action PICKUP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P2))
)
)
(:action PICKUP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P1))
)
)
(:action PICKUP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P4-P0))
)
)
(:action PICKUP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P5))
)
)
(:action PICKUP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P4))
)
)
(:action PICKUP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P3))
)
)
(:action PICKUP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P2))
)
)
(:action PICKUP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P1))
)
)
(:action PICKUP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P3-P0))
)
)
(:action PICKUP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P5))
)
)
(:action PICKUP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P4))
)
)
(:action PICKUP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P3))
)
)
(:action PICKUP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P2))
)
)
(:action PICKUP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P1))
)
)
(:action PICKUP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P2-P0))
)
)
(:action PICKUP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P5))
)
)
(:action PICKUP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P4))
)
)
(:action PICKUP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P3))
)
)
(:action PICKUP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P2))
)
)
(:action PICKUP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P1))
)
)
(:action PICKUP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P1-P0))
)
)
(:action PICKUP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P5))
)
)
(:action PICKUP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P4))
)
)
(:action PICKUP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P2))
)
)
(:action PICKUP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P1))
)
)
(:action PICKUP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P0))
)
)
(:action PICKUP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P5))
)
)
(:action PICKUP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P4))
)
)
(:action PICKUP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P3))
)
)
(:action PICKUP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P2))
)
)
(:action PICKUP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P1))
)
)
(:action PICKUP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P5-P0))
)
)
(:action PICKUP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P5))
)
)
(:action PICKUP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P4))
)
)
(:action PICKUP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P3))
)
)
(:action PICKUP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P2))
)
)
(:action PICKUP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P1))
)
)
(:action PICKUP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P4-P0))
)
)
(:action PICKUP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P5))
)
)
(:action PICKUP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P4))
)
)
(:action PICKUP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P3))
)
)
(:action PICKUP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P2))
)
)
(:action PICKUP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P1))
)
)
(:action PICKUP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P3-P0))
)
)
(:action PICKUP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P5))
)
)
(:action PICKUP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P4))
)
)
(:action PICKUP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P3))
)
)
(:action PICKUP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P2))
)
)
(:action PICKUP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P1))
)
)
(:action PICKUP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P2-P0))
)
)
(:action PICKUP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P5))
)
)
(:action PICKUP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P4))
)
)
(:action PICKUP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P3))
)
)
(:action PICKUP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P2))
)
)
(:action PICKUP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P1))
)
)
(:action PICKUP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P1-P0))
)
)
(:action PICKUP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P5))
)
)
(:action PICKUP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P3))
)
)
(:action PICKUP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P2))
)
)
(:action PICKUP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P1))
)
)
(:action PICKUP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P0))
)
)
(:action PICKUP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P5))
)
)
(:action PICKUP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P4))
)
)
(:action PICKUP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P3))
)
)
(:action PICKUP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P2))
)
)
(:action PICKUP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P1))
)
)
(:action PICKUP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P5-P0))
)
)
(:action PICKUP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P5))
)
)
(:action PICKUP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P4))
)
)
(:action PICKUP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P3))
)
)
(:action PICKUP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P2))
)
)
(:action PICKUP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P1))
)
)
(:action PICKUP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P4-P0))
)
)
(:action PICKUP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P5))
)
)
(:action PICKUP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P4))
)
)
(:action PICKUP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P3))
)
)
(:action PICKUP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P2))
)
)
(:action PICKUP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P1))
)
)
(:action PICKUP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P5))
)
)
(:action PICKUP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P4))
)
)
(:action PICKUP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P3))
)
)
(:action PICKUP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P2))
)
)
(:action PICKUP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P1))
)
)
(:action PICKUP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P2-P0))
)
)
(:action PICKUP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P5))
)
)
(:action PICKUP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P4))
)
)
(:action PICKUP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P3))
)
)
(:action PICKUP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P2))
)
)
(:action PICKUP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P1))
)
)
(:action PICKUP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P1-P0))
)
)
(:action PICKUP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P5))
)
)
(:action PICKUP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P4))
)
)
(:action PICKUP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P3))
)
)
(:action PICKUP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P2))
)
)
(:action PICKUP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P1))
)
)
(:action PICKUP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P0-P0))
)
)
(:action PICKUP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P5))
)
)
(:action PICKUP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P4))
)
)
(:action PICKUP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P3))
)
)
(:action PICKUP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P2))
)
)
(:action PICKUP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P1))
)
)
(:action PICKUP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P5-P0))
)
)
(:action PICKUP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P5))
)
)
(:action PICKUP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P4))
)
)
(:action PICKUP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P3))
)
)
(:action PICKUP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P2))
)
)
(:action PICKUP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P1))
)
)
(:action PICKUP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P4-P0))
)
)
(:action PICKUP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P5))
)
)
(:action PICKUP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P4))
)
)
(:action PICKUP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P3))
)
)
(:action PICKUP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P2))
)
)
(:action PICKUP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P1))
)
)
(:action PICKUP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P3-P0))
)
)
(:action PICKUP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P5))
)
)
(:action PICKUP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P4))
)
)
(:action PICKUP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P3))
)
)
(:action PICKUP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P2))
)
)
(:action PICKUP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P1))
)
)
(:action PICKUP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P2-P0))
)
)
(:action PICKUP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P5))
)
)
(:action PICKUP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P4))
)
)
(:action PICKUP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P3))
)
)
(:action PICKUP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P2))
)
)
(:action PICKUP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P1))
)
)
(:action PICKUP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P1-P0))
)
)
(:action PICKUP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P5))
)
)
(:action PICKUP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P4))
)
)
(:action PICKUP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P3))
)
)
(:action PICKUP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P1))
)
)
(:action PICKUP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P0))
)
)
(:action PICKUP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P5))
)
)
(:action PICKUP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P4))
)
)
(:action PICKUP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P2))
)
)
(:action PICKUP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P1))
)
)
(:action PICKUP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P0))
)
)
(:action PICKUP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P5))
)
)
(:action PICKUP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P4))
)
)
(:action PICKUP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P3))
)
)
(:action PICKUP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P2))
)
)
(:action PICKUP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P1))
)
)
(:action PICKUP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P4-P0))
)
)
(:action PICKUP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P5))
)
)
(:action PICKUP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P4))
)
)
(:action PICKUP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P3))
)
)
(:action PICKUP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P2))
)
)
(:action PICKUP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P1))
)
)
(:action PICKUP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P3-P0))
)
)
(:action PICKUP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P5))
)
)
(:action PICKUP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P4))
)
)
(:action PICKUP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P3))
)
)
(:action PICKUP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P2))
)
)
(:action PICKUP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P1))
)
)
(:action PICKUP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P2-P0))
)
)
(:action PICKUP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P5))
)
)
(:action PICKUP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P4))
)
)
(:action PICKUP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P3))
)
)
(:action PICKUP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P2))
)
)
(:action PICKUP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P1))
)
)
(:action PICKUP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P1-P0))
)
)
(:action PICKUP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P5))
)
)
(:action PICKUP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P4))
)
)
(:action PICKUP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P3))
)
)
(:action PICKUP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P2))
)
)
(:action PICKUP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P1))
)
)
(:action PICKUP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P0-P0))
)
)
(:action PICKUP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P5))
)
)
(:action PICKUP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P4))
)
)
(:action PICKUP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P3))
)
)
(:action PICKUP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P2))
)
)
(:action PICKUP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P1))
)
)
(:action PICKUP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P5-P0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P5-P0))
)
)
(:action PICKUP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P5))
)
)
(:action PICKUP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P4))
)
)
(:action PICKUP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P3))
)
)
(:action PICKUP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P2))
)
)
(:action PICKUP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P1))
)
)
(:action PICKUP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P4-P0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P4-P0))
)
)
(:action PICKUP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P5))
)
)
(:action PICKUP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P4))
)
)
(:action PICKUP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P3))
)
)
(:action PICKUP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P2))
)
)
(:action PICKUP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P1))
)
)
(:action PICKUP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P3-P0))
)
)
(:action PICKUP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P5))
)
)
(:action PICKUP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P4))
)
)
(:action PICKUP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P3))
)
)
(:action PICKUP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P2))
)
)
(:action PICKUP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P1))
)
)
(:action PICKUP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P2-P0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P2-P0))
)
)
(:action PICKUP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P5))
)
)
(:action PICKUP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P4))
)
)
(:action PICKUP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P3))
)
)
(:action PICKUP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P2))
)
)
(:action PICKUP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P1))
)
)
(:action PICKUP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P1-P0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P1-P0))
)
)
(:action PICKUP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P5))
)
)
(:action PICKUP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P4))
)
)
(:action PICKUP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P3))
)
)
(:action PICKUP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P2))
)
)
(:action PICKUP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P1))
)
)
(:derived (Flag1-)(and (OBJ-AT-O0-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O1-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O2-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O3-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O4-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O5-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O6-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O7-P2-P2)))

(:derived (Flag1-)(and (OBJ-AT-O8-P2-P2)))

(:derived (Flag2-)(and (OBJ-AT-O0-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O1-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O2-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O3-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O4-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O5-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O6-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O7-P4-P4)))

(:derived (Flag2-)(and (OBJ-AT-O8-P4-P4)))

(:derived (Flag3-)(and (OBJ-AT-O0-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O1-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O2-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O3-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O4-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O5-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O6-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O7-P2-P0)))

(:derived (Flag3-)(and (OBJ-AT-O8-P2-P0)))

(:derived (Flag4-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag5-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag6-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag7-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag8-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag9-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag10-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag11-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag12-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag13-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag14-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag15-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag16-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag17-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag18-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag19-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag20-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag21-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag22-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag23-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag24-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag25-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag26-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag27-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag28-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag29-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag30-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag31-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag32-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag33-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag34-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag35-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag36-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag37-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag38-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag39-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag40-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag41-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag42-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag43-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag44-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag45-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag46-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag47-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag48-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag49-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag50-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag51-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag52-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag53-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag54-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag55-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag56-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag57-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag58-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag59-)(and (OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag60-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag61-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag62-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag63-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag64-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag65-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag66-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag67-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag68-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag69-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag70-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag71-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag72-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag73-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag74-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag75-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag76-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag77-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag78-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag79-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag80-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag81-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag82-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag83-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag84-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag85-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag86-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag87-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag88-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag89-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag90-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag91-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag92-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag93-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag94-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag95-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag96-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag97-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag98-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag99-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag100-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag101-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag102-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag103-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag104-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag105-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag106-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag107-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag108-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag109-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag110-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag111-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag112-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag113-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag114-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag115-)(and (OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag116-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag117-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag118-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag119-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag120-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag121-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag122-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag123-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag124-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag125-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag126-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag127-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag128-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag129-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag130-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag131-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag132-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag133-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag134-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag135-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag136-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag137-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag138-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag139-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag140-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag141-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag142-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag143-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag144-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag145-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag146-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag147-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag148-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag149-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag150-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag151-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag152-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag153-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag154-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag155-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag156-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag157-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag158-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag159-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag160-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag161-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag162-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag163-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag164-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag165-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag166-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag167-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag168-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag169-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag170-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag171-)(and (OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag172-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag173-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag174-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag175-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag176-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag177-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag178-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag179-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag180-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag181-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag182-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag183-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag184-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag185-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag186-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag187-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag188-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag189-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag190-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag191-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag192-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag193-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag194-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag195-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag196-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag197-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag198-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag199-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag200-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag201-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag202-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag203-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag204-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag205-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag206-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag207-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag208-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag209-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag210-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag211-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag212-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag213-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag214-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag215-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag216-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag217-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag218-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag219-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag220-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag221-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag222-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag223-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag224-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag225-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag226-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag227-)(and (OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag228-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag229-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag230-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag231-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag232-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag233-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag234-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag235-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag236-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag237-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag238-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag239-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag240-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag241-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag242-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag243-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag244-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag245-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag246-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag247-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag248-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag249-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag250-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag251-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag252-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag253-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag254-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag255-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag256-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag257-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag258-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag259-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag260-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag261-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag262-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag263-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag264-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag265-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag266-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag267-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag268-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag269-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag270-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag271-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag272-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag273-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag274-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag275-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag276-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag277-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag278-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag279-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag280-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag281-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag282-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag283-)(and (OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag284-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag285-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag286-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag287-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag288-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag289-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag290-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag291-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag292-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag293-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag294-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag295-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag296-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag297-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag298-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag299-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag300-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag301-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag302-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag303-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag304-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag305-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag306-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag307-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag308-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag309-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag310-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag311-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag312-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag313-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag314-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag315-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag316-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag317-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag318-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag319-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag320-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag321-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag322-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag323-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag324-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag325-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag326-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag327-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag328-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag329-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag330-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag331-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag332-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag333-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag334-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag335-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag336-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag337-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag338-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag339-)(and (OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag340-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag341-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag342-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag343-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag344-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag345-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag346-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag347-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag348-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag349-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag350-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag351-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag352-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag353-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag354-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag355-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag356-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag357-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag358-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag359-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag360-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag361-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag362-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag363-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag364-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag365-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag366-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag367-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag368-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag369-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag370-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag371-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag372-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag373-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag374-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag375-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag376-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag377-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag378-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag379-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag380-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag381-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag382-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag383-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag384-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag385-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag386-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag387-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag388-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag389-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag390-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag391-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag392-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag393-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag394-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag395-)(and (OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag396-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag397-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag398-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag399-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag400-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag401-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag402-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag403-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag404-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag405-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag406-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag407-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag408-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag409-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag410-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag411-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag412-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag413-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag414-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag415-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag416-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag417-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag418-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag419-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag420-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag421-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag422-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag423-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag424-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag425-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag426-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag427-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag428-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag429-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag430-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag431-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag432-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag433-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag434-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag435-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag436-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag437-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag438-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag439-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag440-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag441-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag442-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag443-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag444-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O8-P0-P3)))

(:derived (Flag445-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag446-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag447-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag448-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag449-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag450-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag451-)(and (OBJ-AT-O7-P0-P3)(OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag452-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag453-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag454-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag455-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag456-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag457-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag458-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O0-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag459-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag460-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag461-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag462-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag463-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag464-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag465-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O1-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag466-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag467-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag468-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag469-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag470-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag471-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag472-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O2-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag473-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag474-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag475-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag476-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag477-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag478-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag479-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O3-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag480-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag481-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag482-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag483-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag484-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag485-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag486-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O4-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag487-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag488-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag489-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag490-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag491-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag492-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag493-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O5-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag494-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag495-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag496-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag497-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag498-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag499-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag500-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O6-P0-P3)(OBJ-AT-O7-P0-P3)))

(:derived (Flag501-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O0-P0-P3)))

(:derived (Flag502-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O1-P0-P3)))

(:derived (Flag503-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O2-P0-P3)))

(:derived (Flag504-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O3-P0-P3)))

(:derived (Flag505-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O4-P0-P3)))

(:derived (Flag506-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O5-P0-P3)))

(:derived (Flag507-)(and (OBJ-AT-O8-P0-P3)(OBJ-AT-O7-P0-P3)(OBJ-AT-O6-P0-P3)))

(:derived (Flag509-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag510-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag511-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag512-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag513-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag514-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag515-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag516-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O0-P0-P2)))

(:derived (Flag517-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag518-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag519-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag520-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag521-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag522-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag523-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag524-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O1-P0-P2)))

(:derived (Flag525-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag526-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag527-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag528-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag529-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag530-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag531-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag532-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O2-P0-P2)))

(:derived (Flag533-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag534-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag535-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag536-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag537-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag538-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag539-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag540-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O3-P0-P2)))

(:derived (Flag541-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag542-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag543-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag544-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag545-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag546-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag547-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag548-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O4-P0-P2)))

(:derived (Flag549-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag550-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag551-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag552-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag553-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag554-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag555-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag556-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O5-P0-P2)))

(:derived (Flag557-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag558-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag559-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag560-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag561-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag562-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag563-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag564-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O6-P0-P2)))

(:derived (Flag565-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag566-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag567-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag568-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag569-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag570-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag571-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag572-)(and (OBJ-AT-O8-P0-P2)(OBJ-AT-O7-P0-P2)))

(:derived (Flag573-)(and (OBJ-AT-O0-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag574-)(and (OBJ-AT-O1-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag575-)(and (OBJ-AT-O2-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag576-)(and (OBJ-AT-O3-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag577-)(and (OBJ-AT-O4-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag578-)(and (OBJ-AT-O5-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag579-)(and (OBJ-AT-O6-P0-P2)(OBJ-AT-O8-P0-P2)))

(:derived (Flag580-)(and (OBJ-AT-O7-P0-P2)(OBJ-AT-O8-P0-P2)))

(:action DROP-O0-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O0-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O0-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O0-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O0-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O0-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O0-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O0-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O0-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O0-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O0-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O0-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O0-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O0-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O0-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O0-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O0-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O0-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O0-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O0-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O0-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O0-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O0-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O0-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O0-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O0-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O0-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O0-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O0-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O0-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O0-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O0-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O0-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O0-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O0-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O0-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O0-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O0-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O0))
)
)
(:action DROP-O1-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O1-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O1-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O1-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O1-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O1-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O1-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O1-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O1-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O1-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O1-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O1-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O1-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O1-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O1-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O1-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O1-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O1-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O1-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O1-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O1-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O1-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O1-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O1-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O1-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O1-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O1-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O1-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O1-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O1-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O1-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O1-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O1-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O1-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O1-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O1-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O1-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O1)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O1-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O1))
)
)
(:action DROP-O2-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O2-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O2-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O2-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O2-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O2-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O2-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O2-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O2-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O2-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O2-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O2-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O2-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O2-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O2-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O2-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O2-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O2-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O2-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O2-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O2-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O2-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O2-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O2-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O2-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O2-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O2-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O2-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O2-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O2-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O2-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O2-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O2-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O2-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O2-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O2-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O2-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O2)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O2-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O2))
)
)
(:action DROP-O3-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O3-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O3-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O3-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O3-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O3-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O3-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O3-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O3-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O3-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O3-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O3-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O3-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O3-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O3-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O3-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O3-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O3-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O3-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O3-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O3-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O3-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O3-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O3-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O3-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O3-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O3-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O3-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O3-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O3-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O3-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O3-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O3-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O3-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O3-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O3-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O3-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O3)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O3-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O3))
)
)
(:action DROP-O4-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O4-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O4-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O4-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O4-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O4-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O4-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O4-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O4-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O4-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O4-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O4-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O4-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O4-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O4-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O4-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O4-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O4-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O4-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O4-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O4-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O4-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O4-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O4-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O4-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O4-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O4-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O4-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O4-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O4-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O4-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O4-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O4-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O4-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O4-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O4-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O4-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O4)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O4-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O4))
)
)
(:action DROP-O5-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O5-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O5-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O5-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O5-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O5-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O5-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O5-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O5-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O5-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O5-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O5-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O5-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O5-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O5-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O5-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O5-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O5-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O5-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O5-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O5-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O5-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O5-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O5-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O5-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O5-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O5-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O5-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O5-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O5-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O5-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O5-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O5-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O5-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O5-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O5-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O5-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O5)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O5-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O5))
)
)
(:action DROP-O6-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O6-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O6-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O6-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O6-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O6-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O6-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O6-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O6-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O6-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O6-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O6-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O6-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O6-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O6-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O6-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O6-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O6-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O6-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O6-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O6-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O6-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O6-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O6-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O6-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O6-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O6-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O6-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O6-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O6-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O6-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O6-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O6-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O6-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O6-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O6-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O6-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O6)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O6-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O6))
)
)
(:action DROP-O7-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O7-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O7-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O7-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O7-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O7-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O7-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O7-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O7-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O7-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O7-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O7-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O7-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O7-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O7-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O7-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O7-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O7-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O7-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O7-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O7-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O7-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O7-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O7-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O7-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O7-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O7-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O7-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O7-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O7-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O7-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O7-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O7-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O7-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O7-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O7-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O7-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O7)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O7-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O7))
)
)
(:action DROP-O8-P5-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P5)
)
:effect
(and
(OBJ-AT-O8-P5-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P4)
)
:effect
(and
(OBJ-AT-O8-P5-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P3)
)
:effect
(and
(OBJ-AT-O8-P5-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P2)
)
:effect
(and
(OBJ-AT-O8-P5-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P1)
)
:effect
(and
(OBJ-AT-O8-P5-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P5-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P5-P0)
)
:effect
(and
(OBJ-AT-O8-P5-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P5)
)
:effect
(and
(OBJ-AT-O8-P4-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P4)
)
:effect
(and
(OBJ-AT-O8-P4-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P3)
)
:effect
(and
(OBJ-AT-O8-P4-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P2)
)
:effect
(and
(OBJ-AT-O8-P4-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P1)
)
:effect
(and
(OBJ-AT-O8-P4-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P4-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P4-P0)
)
:effect
(and
(OBJ-AT-O8-P4-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P5)
)
:effect
(and
(OBJ-AT-O8-P3-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P4)
)
:effect
(and
(OBJ-AT-O8-P3-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P3)
)
:effect
(and
(OBJ-AT-O8-P3-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P2)
)
:effect
(and
(OBJ-AT-O8-P3-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P1)
)
:effect
(and
(OBJ-AT-O8-P3-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P3-P0)
)
:effect
(and
(OBJ-AT-O8-P3-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P5)
)
:effect
(and
(OBJ-AT-O8-P2-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P4)
)
:effect
(and
(OBJ-AT-O8-P2-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P3)
)
:effect
(and
(OBJ-AT-O8-P2-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P2)
)
:effect
(and
(OBJ-AT-O8-P2-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P1)
)
:effect
(and
(OBJ-AT-O8-P2-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P2-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P2-P0)
)
:effect
(and
(OBJ-AT-O8-P2-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P5)
)
:effect
(and
(OBJ-AT-O8-P1-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P4)
)
:effect
(and
(OBJ-AT-O8-P1-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P3)
)
:effect
(and
(OBJ-AT-O8-P1-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P2)
)
:effect
(and
(OBJ-AT-O8-P1-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P1)
)
:effect
(and
(OBJ-AT-O8-P1-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P1-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P1-P0)
)
:effect
(and
(OBJ-AT-O8-P1-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P5)
)
:effect
(and
(OBJ-AT-O8-P0-P5)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P4)
)
:effect
(and
(OBJ-AT-O8-P0-P4)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P3)
)
:effect
(and
(OBJ-AT-O8-P0-P3)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P2)
)
:effect
(and
(OBJ-AT-O8-P0-P2)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action DROP-O8-P0-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P1)
)
:effect
(and
(OBJ-AT-O8-P0-P1)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action PICKUP-O0-P2-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O0-P2-P4)
(ROBOT-AT-P2-P4)
)
:effect
(and
(HOLDING-O0)
(CAPACITY-C0)
(not (OBJ-AT-O0-P2-P4))
)
)
(:action PICKUP-O1-P2-P5-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O1-P2-P5)
(ROBOT-AT-P2-P5)
)
:effect
(and
(HOLDING-O1)
(CAPACITY-C0)
(not (OBJ-AT-O1-P2-P5))
)
)
(:action PICKUP-O2-P1-P1-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O2-P1-P1)
(ROBOT-AT-P1-P1)
)
:effect
(and
(HOLDING-O2)
(CAPACITY-C0)
(not (OBJ-AT-O2-P1-P1))
)
)
(:action PICKUP-O3-P0-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O3-P0-P3)
(ROBOT-AT-P0-P3)
)
:effect
(and
(HOLDING-O3)
(CAPACITY-C0)
(not (OBJ-AT-O3-P0-P3))
)
)
(:action PICKUP-O4-P0-P4-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O4-P0-P4)
(ROBOT-AT-P0-P4)
)
:effect
(and
(HOLDING-O4)
(CAPACITY-C0)
(not (OBJ-AT-O4-P0-P4))
)
)
(:action PICKUP-O5-P3-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O5-P3-P0)
(ROBOT-AT-P3-P0)
)
:effect
(and
(HOLDING-O5)
(CAPACITY-C0)
(not (OBJ-AT-O5-P3-P0))
)
)
(:action PICKUP-O6-P0-P2-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O6-P0-P2)
(ROBOT-AT-P0-P2)
)
:effect
(and
(HOLDING-O6)
(CAPACITY-C0)
(not (OBJ-AT-O6-P0-P2))
)
)
(:action PICKUP-O7-P5-P3-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O7-P5-P3)
(ROBOT-AT-P5-P3)
)
:effect
(and
(HOLDING-O7)
(CAPACITY-C0)
(not (OBJ-AT-O7-P5-P3))
)
)
(:action MOVE-P5-P5-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P5-P5))
)
)
(:action MOVE-P5-P4-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P5-P4))
)
)
(:action MOVE-P5-P3-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P5-P3))
)
)
(:action MOVE-P5-P2-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P5-P2))
)
)
(:action MOVE-P5-P1-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P5-P1))
)
)
(:action MOVE-P5-P0-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P5-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P5-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P5-P0))
)
)
(:action MOVE-P4-P5-P5-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P5-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P4-P5))
)
)
(:action MOVE-P4-P4-P5-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P5-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P4-P4))
)
)
(:action MOVE-P4-P3-P5-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P5-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P4-P3))
)
)
(:action MOVE-P4-P2-P5-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P5-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P4-P2))
)
)
(:action MOVE-P4-P1-P5-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P5-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P4-P1))
)
)
(:action MOVE-P4-P0-P5-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P5-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P4-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P4-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P4-P0))
)
)
(:action MOVE-P3-P5-P4-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P4-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P3-P5))
)
)
(:action MOVE-P3-P4-P4-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P4-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P3-P4))
)
)
(:action MOVE-P3-P3-P4-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P4-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P3-P3))
)
)
(:action MOVE-P3-P2-P4-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P4-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P3-P2))
)
)
(:action MOVE-P3-P1-P4-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P4-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P3-P1))
)
)
(:action MOVE-P3-P0-P4-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P4-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P3-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P3-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P3-P0))
)
)
(:action MOVE-P2-P5-P3-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P3-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P2-P5))
)
)
(:action MOVE-P2-P4-P3-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P3-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P2-P4))
)
)
(:action MOVE-P2-P3-P3-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P3-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P2-P3))
)
)
(:action MOVE-P2-P2-P3-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P3-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P2-P2))
)
)
(:action MOVE-P2-P1-P3-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P3-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P2-P1))
)
)
(:action MOVE-P2-P0-P3-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P3-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P2-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P2-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P2-P0))
)
)
(:action MOVE-P1-P5-P2-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P2-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P5-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P5)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P1-P5))
)
)
(:action MOVE-P1-P4-P2-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P2-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P4-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P4)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P1-P4))
)
)
(:action MOVE-P1-P3-P2-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P2-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P3-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P3)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P1-P3))
)
)
(:action MOVE-P1-P2-P2-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P2-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P2-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P2)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P1-P2))
)
)
(:action MOVE-P1-P1-P2-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P2-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P1-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P1)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P1-P1))
)
)
(:action MOVE-P1-P0-P2-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P2-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P1-P0-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P1-P0)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P1-P0))
)
)
(:action MOVE-P0-P5-P1-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P1-P5)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P5-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P5)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P5))
)
)
(:action MOVE-P0-P4-P1-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P1-P4)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P5
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P5)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P4-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P4)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P4))
)
)
(:action MOVE-P0-P3-P1-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P1-P3)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P4
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P4)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P3-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P3)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P3))
)
)
(:action MOVE-P0-P2-P1-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P1-P2)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P3
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P3)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P2-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P2)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P2))
)
)
(:action MOVE-P0-P1-P1-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P1-P1)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P2
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P2)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P1-P0-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P1)
)
:effect
(and
(ROBOT-AT-P0-P0)
(not (ROBOT-AT-P0-P1))
)
)
(:action MOVE-P0-P0-P1-P0
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P1-P0)
(not (ROBOT-AT-P0-P0))
)
)
(:action MOVE-P0-P0-P0-P1
:parameters ()
:precondition
(and
(ROBOT-AT-P0-P0)
)
:effect
(and
(ROBOT-AT-P0-P1)
(not (ROBOT-AT-P0-P0))
)
)
(:action DROP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(HOLDING-O8)
(ROBOT-AT-P0-P0)
)
:effect
(and
(OBJ-AT-O8-P0-P0)
(CAPACITY-C0)
(not (HOLDING-O8))
)
)
(:action PICKUP-O8-P0-P0-C0-C0-C0
:parameters ()
:precondition
(and
(CAPACITY-C0)
(OBJ-AT-O8-P0-P0)
(ROBOT-AT-P0-P0)
)
:effect
(and
(HOLDING-O8)
(CAPACITY-C0)
(not (OBJ-AT-O8-P0-P0))
)
)
)
