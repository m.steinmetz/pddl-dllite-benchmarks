(define (domain Wsmo2TPSA)
  (:requirements :ekab)

(:predicates 
	(order ?x)
	(requestedService ?x ?y)
	(service ?x)
	(contract ?x)
	(requiredService ?x ?y)
	(ack ?x)
	(confirmation ?x)
	(hasService ?x ?y)
	(hardwareAck ?x)
	(forService ?x ?y)
	(crmAck ?x)
	(courierAck ?x)
	(contractConfirmation ?x)
	(hasContract ?x ?y)
	(orderConfirmation ?x)
	(activation ?x)
	(serviceActivation ?x)
	(invoice ?x)
	(greater ?x ?y)
	(notFree ?x)
)

(:axioms
  (isA order (exists requestedService))
  (isA (exists (inverse requestedService)) service)
  (isA (exists (inverse requiredService)) service)
  (isA (exists (inverse hasService)) service)
  (isA (exists (inverse forService)) service)
  (isA contract (exists requiredService))
  (isA confirmation (exists hasService))
  (isA hardwareAck ack)
  (isA hardwareAck (exists forService))
  (isA crmAck ack)
  (isA courierAck ack)
  (isA contractConfirmation confirmation)
  (isA contractConfirmation (exists hasContract))
  (isA (exists (inverse hasContract)) contract)
  (isA orderConfirmation confirmation)
  (isA serviceActivation activation)
  (isA contract (not service))
  (isA order notFree)
  (isA (exists requestedService) notFree)
  (isA (exists (inverse requestedService)) notFree)
  (isA service notFree)
  (isA contract notFree)
  (isA (exists requiredService) notFree)
  (isA (exists (inverse requiredService)) notFree)
  (isA ack notFree)
  (isA confirmation notFree)
  (isA (exists hasService) notFree)
  (isA (exists (inverse hasService)) notFree)
  (isA hardwareAck notFree)
  (isA (exists forService) notFree)
  (isA (exists (inverse forService)) notFree)
  (isA crmAck notFree)
  (isA courierAck notFree)
  (isA contractConfirmation notFree)
  (isA (exists hasContract) notFree)
  (isA (exists (inverse hasContract)) notFree)
  (isA orderConfirmation notFree)
  (isA activation notFree)
  (isA serviceActivation notFree)
  (isA invoice notFree)
)

(:rule rule_billingActivation
:condition (mko (activation ?x))
:action billingActivation
)

(:rule rule_confirmOrder
:condition (and (mko (requestedService ?x ?y)) (mko (order ?x)) (mko (service ?y)))
:action confirmOrder
)

(:rule rule_contractSaving
:condition (and (mko (courierAck ?x)) (mko (contract ?y)))
:action contractSaving
)

(:rule rule_courierService
:condition (mko (contract ?x))
:action courierService
)

(:rule rule_prepareContract
:condition (and (mko (hasService ?x ?y)) (mko (orderConfirmation ?x)) (mko (service ?y)))
:action prepareContract
)

(:rule rule_prepareHardware
:condition (and (mko (hasService ?x ?y)) (mko (confirmation ?x)) (mko (service ?y)))
:action prepareHardware
)

(:rule rule_productActivation
:condition (and (mko (crmAck ?x)) (mko (hardwareAck ?y)))
:action productActivation
)

(:action billingActivation
  :parameters (?x ?y)
  :effects (
    :condition (:True)
    :add ((invoice ?y))
  )
)

(:action confirmOrder
  :parameters (?x ?y ?z)
  :effects (
    :condition (:True)
    :add ((hasService ?z ?y) (orderConfirmation ?z))
  )
)

(:action contractSaving
  :parameters (?x ?y ?z)
  :effects (
    :condition (:True)
    :add ((crmAck ?z))
  )
)

(:action courierService
  :parameters (?x ?y)
  :effects (
    :condition (:True)
    :add ((courierAck ?y))
  )
)

(:action prepareContract
  :parameters (?x ?y ?z)
  :effects (
    :condition (:True)
    :add ((requiredService ?z ?y) (contract ?z))
  )
)

(:action prepareHardware
  :parameters (?x ?y ?z)
  :effects (
    :condition (:True)
    :add ((forService ?z ?y) (hardwareAck ?z))
  )
)

(:action productActivation
  :parameters (?x ?y ?z)
  :effects (
    :condition (:True)
    :add ((serviceActivation ?z))
  )
)

)
