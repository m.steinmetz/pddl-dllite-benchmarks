(define (problem Wsmo2TPSA_problem )
  (:domain Wsmo2TPSA)
  (:objects
    aa ac ab ae ad ag af ai ah voipRequest aj am al ao an aq ap as ar au at aw av ay ax ca az bz ak bd be bf bg ba bb bc bl bm bn bo bh bi bj bk bt bu bv bw bp bq br bs bx by voip 
  )
  (:init
    (order voipRequest)
    (requestedService voipRequest voip)
    (service voip)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?x )
        (invoice ?x)
      )
    )
  )
)