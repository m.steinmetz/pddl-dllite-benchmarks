(define
  (domain Wsmo2TPSA)
  (:requirements :adl)
  (:predicates
    (confirmation ?x )
    (serviceActivation ?x )
    (orderConfirmation ?x )
    (service ?x )
    (ack ?x )
    (activation ?x )
    (contract ?x )
    (contractConfirmation ?x )
    (hardwareAck ?x )
    (courierAck ?x )
    (invoice ?x )
    (crmAck ?x )
    (order ?x )
    (requestedService ?x ?y )
    (hasService ?x ?y )
    (forService ?x ?y )
    (hasContract ?x ?y )
    (requiredService ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action prepareContract
    :parameters ( ?x ?y ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requestedService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (forService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (and
          (hasService ?x ?y)
          (service ?y)
          (orderConfirmation ?x)
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requiredService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (hasService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (requiredService ?z ?y)
          (contract ?z)
        )
      )
    )
  )
  (:action prepareHardware
    :parameters ( ?x ?y ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (forService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (contractConfirmation ?x)
            (hasService ?z_0 ?y)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (confirmation ?x)
            (forService ?z_0 ?y)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requestedService ?z_0 ?y)
            (contractConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requestedService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (and
          (hasService ?x ?y)
          (service ?y)
          (contractConfirmation ?x)
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requiredService ?z_0 ?y)
            (contractConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (requiredService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (hasService ?z_0 ?y)
            (orderConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (requestedService ?z_0 ?y)
            (hasService ?x ?y)
            (confirmation ?x)
          )
        )
        (and
          (hasService ?x ?y)
          (service ?y)
          (orderConfirmation ?x)
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (confirmation ?x)
            (requiredService ?z_0 ?y)
          )
        )
        (and
          (service ?y)
          (hasService ?x ?y)
          (confirmation ?x)
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (forService ?z_0 ?y)
            (contractConfirmation ?x)
          )
        )
        (exists (?z_0 )
          (and
            (hasService ?x ?y)
            (confirmation ?x)
            (hasService ?z_0 ?y)
          )
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (forService ?z ?y)
          (hardwareAck ?z)
        )
      )
    )
  )
  (:action contractSaving
    :parameters ( ?x ?y ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (exists (?z_0 )
          (and
            (hasContract ?z_0 ?y)
            (courierAck ?x)
          )
        )
        (and
          (contract ?y)
          (courierAck ?x)
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (crmAck ?z)
        )
      )
    )
  )
  (:action productActivation
    :parameters ( ?x ?y ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (crmAck ?x)
        (hardwareAck ?y)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (serviceActivation ?z)
        )
      )
    )
  )
  (:action courierService
    :parameters ( ?x ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (exists (?z_0 )
          (hasContract ?z_0 ?x)
        )
        (contract ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (courierAck ?y)
        )
      )
    )
  )
  (:action confirmOrder
    :parameters ( ?x ?y ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (exists (?z_0 )
          (and
            (requestedService ?x ?y)
            (forService ?z_0 ?y)
            (order ?x)
          )
        )
        (and
          (requestedService ?x ?y)
          (service ?y)
          (order ?x)
        )
        (exists (?z_0 )
          (and
            (requestedService ?x ?y)
            (hasService ?z_0 ?y)
            (order ?x)
          )
        )
        (exists (?z_0 )
          (and
            (requestedService ?x ?y)
            (requiredService ?z_0 ?y)
            (order ?x)
          )
        )
        (exists (?z_0 )
          (and
            (requestedService ?x ?y)
            (requestedService ?z_0 ?y)
            (order ?x)
          )
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (hasService ?z ?y)
          (orderConfirmation ?z)
        )
      )
    )
  )
  (:action billingActivation
    :parameters ( ?x ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (serviceActivation ?x)
        (activation ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (invoice ?y)
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (exists (?x_0 )
          (and
            (order ?x_0)
            (orderConfirmation ?x_0)
          )
        )
        (Error)
      )
    )
  )
)