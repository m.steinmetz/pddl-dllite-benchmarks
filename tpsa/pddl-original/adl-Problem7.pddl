(define (problem Wsmo2TPSA_problem )
  (:domain Wsmo2TPSA)
  (:objects
    a c b e d g f i h voipRequest j voip 
  )
  (:init
    (order voipRequest)
    (requestedService voipRequest voip)
    (service voip)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?x )
        (invoice ?x)
      )
    )
  )
)