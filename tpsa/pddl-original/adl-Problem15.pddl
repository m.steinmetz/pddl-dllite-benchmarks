(define (problem Wsmo2TPSA_problem )
  (:domain Wsmo2TPSA)
  (:objects
    aa ac ab ae ad ag af ai ah voipRequest aj am al ao an aq ap ar ak voip 
  )
  (:init
    (order voipRequest)
    (requestedService voipRequest voip)
    (service voip)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?x )
        (invoice ?x)
      )
    )
  )
)