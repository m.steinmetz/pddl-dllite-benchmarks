(define (problem Wsmo2TPSA_problem )
  (:domain Wsmo2TPSA)
  (:objects
    aa cj ac ab ae ad ag af ai ah voipRequest aj am al ao an aq ap as ar au at aw av ay ax ca az ci cc bz cb ak bd be bf bg ce ba bb bc bl bm bn bo bh bi bj bk bt bu bv bw bp bq br bs cd bx by voip cg ck cf ch 
  )
  (:init
    (order voipRequest)
    (requestedService voipRequest voip)
    (service voip)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (exists ( ?x )
        (invoice ?x)
      )
    )
  )
)