(define (problem grounded-SIMPLE-ADL-WSMO2TPSA_PROBLEM)
(:domain grounded-SIMPLE-ADL-WSMO2TPSA)
(:init
(NOT-HASSERVICE-A-VOIP)
(NOT-HASSERVICE-C-VOIP)
(NOT-HASSERVICE-B-VOIP)
(NOT-HASSERVICE-E-VOIP)
(NOT-HASSERVICE-D-VOIP)
(NOT-HASSERVICE-G-VOIP)
(NOT-HASSERVICE-F-VOIP)
(NOT-HASSERVICE-H-VOIP)
(NOT-HASSERVICE-VOIPREQUEST-VOIP)
(NOT-HASSERVICE-VOIP-VOIP)
(NOT-REQUIREDSERVICE-A-VOIP)
(NOT-REQUIREDSERVICE-C-VOIP)
(NOT-REQUIREDSERVICE-B-VOIP)
(NOT-REQUIREDSERVICE-E-VOIP)
(NOT-REQUIREDSERVICE-D-VOIP)
(NOT-REQUIREDSERVICE-G-VOIP)
(NOT-REQUIREDSERVICE-F-VOIP)
(NOT-REQUIREDSERVICE-H-VOIP)
(NOT-REQUIREDSERVICE-VOIPREQUEST-VOIP)
(NOT-REQUIREDSERVICE-VOIP-VOIP)
(NOT-FORSERVICE-A-VOIP)
(NOT-FORSERVICE-C-VOIP)
(NOT-FORSERVICE-B-VOIP)
(NOT-FORSERVICE-E-VOIP)
(NOT-FORSERVICE-D-VOIP)
(NOT-FORSERVICE-G-VOIP)
(NOT-FORSERVICE-F-VOIP)
(NOT-FORSERVICE-H-VOIP)
(NOT-FORSERVICE-VOIPREQUEST-VOIP)
(NOT-FORSERVICE-VOIP-VOIP)
(NOT-CONTRACT-A)
(NOT-CONTRACT-C)
(NOT-CONTRACT-B)
(NOT-CONTRACT-E)
(NOT-CONTRACT-D)
(NOT-CONTRACT-G)
(NOT-CONTRACT-F)
(NOT-CONTRACT-H)
(NOT-CONTRACT-VOIPREQUEST)
(NOT-CONTRACT-VOIP)
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
)
(:goal
(and
(Flag2-)
)
)
)
