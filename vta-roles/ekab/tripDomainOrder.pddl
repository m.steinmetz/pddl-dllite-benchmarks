(define (domain Wsmo2VTAT)
  (:requirements :ekab)

(:predicates 
	(invoice ?x ?y)
	(trip ?x)
	(flight ?x)
	(flightTicket ?x ?y)
	(hotelStay ?x)
	(carHotelBundleOption ?x ?y)
	(itinerary ?x ?y)
	(flightRequest ?x ?y)
	(hotelStayRequest ?x ?y)
	(hotelStayConfirmation ?x ?y)
	(carRentalRequest ?x ?y)
	(carRental ?x)
	(carRentalBooking ?x ?y)
	(airportShuttle ?x ?y)
	(directlyAfterObj ?x ?y)
	(notFree ?x)
)

(:axioms
  (isA (exists (inverse flightRequest)) flight)
  (isA (exists flightRequest) trip)
  (isA (exists flightTicket) trip)
  (isA (exists airportShuttle) trip)
  (isA (exists (inverse hotelStayRequest)) hotelStay)
  (isA (exists hotelStayRequest) trip)
  (isA (exists hotelStayConfirmation) trip)
  (isA (exists (inverse carRentalRequest)) carRental)
  (isA (exists carRentalRequest) trip)
  (isA (exists carRentalBooking) trip)
  (isA (exists carHotelBundleOption) trip)
  (isA flight (not hotelStay))
  (isA flight (not carRental))
  (isA hotelStay (not carRental))
)

(:rule rule_vtaShuttleService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y))
	(mko (flightTicket ?z ?x)))
:action vtaShuttleService
)

(:rule rule_vtaCarHotelBundlingService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?z)) 
	(mko (hotelStayConfirmation ?s ?x)) (mko (carRentalBooking ?s ?y)))
:action vtaCarHotelBundlingService
)

(:rule rule_vtaCarRentalService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) 
	(mko (carRental ?x)) (mko (carRentalRequest ?z ?x)))
:action vtaCarRentalService
)

(:rule rule_vtaHotelService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) 
	(mko (hotelStay ?x) ) (mko (hotelStayRequest ?z ?x)))
:action vtaHotelService
)

(:rule rule_vtaFlightService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) 
	(mko (flightRequest ?z ?x)) (mko (flight ?x)))
:action vtaFlightService
)

(:rule rule_vtaTripCombinationService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?z)) (mko (directlyAfterObj ?z ?t)) 
	(mko (airportShuttle ?s ?x)) (mko (carHotelBundleOption ?s ?y)))
:action vtaTripCombinationService
)

(:rule rule_vtaTripMakerService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) (mko (directlyAfterObj ?y ?z)) (mko (directlyAfterObj ?z ?t)) 	(mko (trip ?x)))
:action vtaTripMakerService
)

(:action vtaShuttleService
  :parameters (?x ?z ?q ?y)
  :effects (
    :condition (:True)
    :add ((airportShuttle ?z ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaCarHotelBundlingService
  :parameters (?s ?x ?y ?q ?z)
  :effects (
    :condition (:True)
    :add ((carHotelBundleOption ?s ?z) (notFree ?z))
    :delete ((notFree ?q))
  )
)

(:action vtaCarRentalService
  :parameters (?z ?x ?q ?y)
  :effects (
    :condition (:True)
    :add ((carRentalBooking ?z ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaHotelService
  :parameters (?z ?x ?q ?y)
  :effects (
    :condition (:True)
    :add ((hotelStayConfirmation ?z ?y)(notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaFlightService
  :parameters (?z ?x ?q ?y)
  :effects (
    :condition (:True)
    :add ((flightTicket ?z ?y)(notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaTripCombinationService
  :parameters (?s ?x ?y ?z ?t ?q )
  :effects (
    :condition (:True)
    :add ((itinerary ?s ?z) (invoice ?s ?t) (notFree ?t))
    :delete ((notFree ?q))
  )
)

(:action vtaTripMakerService
  :parameters (?x ?q ?y ?z ?t)
  :effects (
    :condition (:True)
    :add ((flightRequest ?x ?y) (hotelStayRequest ?x ?z) (carRentalRequest ?x ?t) (notFree ?t))
    :delete ((notFree ?q))
  )
)

)
