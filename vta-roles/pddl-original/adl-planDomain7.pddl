(define
  (domain Wsmo2VTAT)
  (:requirements :adl)
  (:predicates
    (hotelStay ?x )
    (notFree ?x )
    (flight ?x )
    (trip ?x )
    (carRental ?x )
    (invoice ?x ?y )
    (airportShuttle ?x ?y )
    (directlyAfterObj ?x ?y )
    (hotelStayRequest ?x ?y )
    (hotelStayConfirmation ?x ?y )
    (flightRequest ?x ?y )
    (carRentalBooking ?x ?y )
    (itinerary ?x ?y )
    (carHotelBundleOption ?x ?y )
    (flightTicket ?x ?y )
    (carRentalRequest ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action vtaCarRentalService
    :parameters ( ?z ?x ?q ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?q ?y)
        (notFree ?q)
        (or
          (carRental ?x)
          (exists (?z_0 )
            (carRentalRequest ?z_0 ?x)
          )
        )
        (carRentalRequest ?z ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (carRentalBooking ?z ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaShuttleService
    :parameters ( ?x ?z ?q ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (notFree ?q)
        (directlyAfterObj ?q ?y)
        (flightTicket ?z ?x)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (airportShuttle ?z ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaTripCombinationService
    :parameters ( ?s ?x ?y ?z ?t ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (notFree ?q)
        (carHotelBundleOption ?s ?y)
        (directlyAfterObj ?z ?t)
        (airportShuttle ?s ?x)
        (directlyAfterObj ?q ?z)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (itinerary ?s ?z)
          (invoice ?s ?t)
          (notFree ?t)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaFlightService
    :parameters ( ?z ?x ?q ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (or
          (exists (?z_0 )
            (flightRequest ?z_0 ?x)
          )
          (flight ?x)
        )
        (flightRequest ?z ?x)
        (notFree ?q)
        (directlyAfterObj ?q ?y)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (flightTicket ?z ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaCarHotelBundlingService
    :parameters ( ?s ?x ?y ?q ?z )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?q ?z)
        (notFree ?q)
        (hotelStayConfirmation ?s ?x)
        (carRentalBooking ?s ?y)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (carHotelBundleOption ?s ?z)
          (notFree ?z)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaTripMakerService
    :parameters ( ?x ?q ?y ?z ?t )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?y ?z)
        (directlyAfterObj ?z ?t)
        (directlyAfterObj ?q ?y)
        (notFree ?q)
        (or
          (trip ?x)
          (exists (?z_0 )
            (carRentalRequest ?x ?z_0)
          )
          (exists (?z_0 )
            (carRentalBooking ?x ?z_0)
          )
          (exists (?z_0 )
            (hotelStayConfirmation ?x ?z_0)
          )
          (exists (?z_0 )
            (hotelStayRequest ?x ?z_0)
          )
          (exists (?z_0 )
            (airportShuttle ?x ?z_0)
          )
          (exists (?z_0 )
            (flightRequest ?x ?z_0)
          )
          (exists (?z_0 )
            (carHotelBundleOption ?x ?z_0)
          )
          (exists (?z_0 )
            (flightTicket ?x ?z_0)
          )
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (flightRequest ?x ?y)
          (hotelStayRequest ?x ?z)
          (carRentalRequest ?x ?t)
          (notFree ?t)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaHotelService
    :parameters ( ?z ?x ?q ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?q ?y)
        (hotelStayRequest ?z ?x)
        (notFree ?q)
        (or
          (hotelStay ?x)
          (exists (?z_0 )
            (hotelStayRequest ?z_0 ?x)
          )
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (hotelStayConfirmation ?z ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_2 ?z_0 ?z_1 )
            (and
              (hotelStayRequest ?z_0 ?x_2)
              (flightRequest ?z_1 ?x_2)
            )
          )
          (exists (?x_2 )
            (and
              (hotelStay ?x_2)
              (flight ?x_2)
            )
          )
          (exists (?x_0 )
            (and
              (carRental ?x_0)
              (hotelStay ?x_0)
            )
          )
          (exists (?x_1 )
            (and
              (flight ?x_1)
              (carRental ?x_1)
            )
          )
          (exists (?x_1 ?z_0 )
            (and
              (carRental ?x_1)
              (flightRequest ?z_0 ?x_1)
            )
          )
          (exists (?x_1 ?z_0 ?z_1 )
            (and
              (carRentalRequest ?z_0 ?x_1)
              (flightRequest ?z_1 ?x_1)
            )
          )
          (exists (?x_1 ?z_0 )
            (and
              (carRentalRequest ?z_0 ?x_1)
              (flight ?x_1)
            )
          )
          (exists (?x_2 ?z_0 )
            (and
              (hotelStay ?x_2)
              (flightRequest ?z_0 ?x_2)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (carRentalRequest ?z_0 ?x_0)
              (hotelStay ?x_0)
            )
          )
          (exists (?x_2 ?z_0 ?z_1 )
            (and
              (flightRequest ?z_0 ?x_2)
              (hotelStayRequest ?z_1 ?x_2)
            )
          )
          (exists (?x_2 ?z_0 )
            (and
              (hotelStayRequest ?z_0 ?x_2)
              (flight ?x_2)
            )
          )
          (exists (?x_0 ?z_0 ?z_1 )
            (and
              (hotelStayRequest ?z_1 ?x_0)
              (carRentalRequest ?z_0 ?x_0)
            )
          )
          (exists (?x_0 ?z_0 )
            (and
              (carRental ?x_0)
              (hotelStayRequest ?z_0 ?x_0)
            )
          )
        )
        (Error)
      )
    )
  )
)