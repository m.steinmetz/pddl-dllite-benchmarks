This is a collection of benchmarks made for a KR submission ``Making DL-Lite Planning Practical’’ by Stefan Borgwardt, Jörg Hoffmann, Alisa Kovtunova and Marcel Steinmetz.

This collection consists of a total of 205 instances from varied sources, including existing ones as well as ones that we created. In particular, 125 instances are made for DL-Lite planning and expressed in eKAB. Each instance has three types of representation: original in pddl, pre-compiled with derived predicates and pre-compiled by a non-naive transformation suggested in 
	Nebel, B. 2000. On the compilability and expressive power of propositional planning formalisms. Journal of Artificial Intelligence Research 12:271–315.


© 2021. This work is licensed under a CC BY 4.0 license (see https://creativecommons.org/licenses/by/4.0/). 