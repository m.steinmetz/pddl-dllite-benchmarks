Here it is provided a verbose time evaluation of the our pre-compilation tool available in
https://gitlab.perspicuous-computing.science/a.kovtunova/moreflags2.git

The evaluation was done for FF and FD planners in three modes: original version, pre-compiled with derived predicates (“axioms”) and with non-naive transformation (“Nebel”).