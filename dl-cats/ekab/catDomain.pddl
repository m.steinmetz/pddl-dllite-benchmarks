(define (domain BTcat)
  (:requirements :ekab)

(:predicates 
	(cat ?x)
	(bomb ?x)
	(contains ?x ?y)
	(package ?x)
	(disarmed ?x)
	(object ?x)
)

(:axioms
  (isA	bomb (not cat))
  (isA cat object)
  (isA bomb object)
  (isA (exists contains) package)
  (isA package (exists contains))
  (isA (exists (inverse contains )) object)
  (funct contains)
)

(:rule rule1
:condition (mko (package ?x))
:action dunk
)

(:rule rule2
:condition (mko (package ?x))
:action let_the_cats_out
)

(:action dunk
  :parameters (?x ?y)
  :effects (
    :condition  (and 
            (mko (contains ?x ?y)) 
            (mko (bomb ?y))
         )
    :add ((disarmed ?x))
  )
)

(:action let_the_cats_out
  :parameters (?x ?y)
  :effects (
    :condition (and (mko (contains ?x ?y)) (mko (cat ?y)))
    :delete ((package ?x) (contains ?x ?y))
  )
)  
)

