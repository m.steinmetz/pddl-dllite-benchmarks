(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-B-L)
(NOT-CONTAINS-F-I)
(NOT-CONTAINS-H-M)
(NOT-CONTAINS-N-P)
(DISARMED-G)
(DISARMED-J)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag7-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
(CONTAINS-H-M)
(CONTAINS-F-I)
(CONTAINS-B-L)
(Flag13prime-)
(Flag12prime-)
(Flag11prime-)
(Flag10prime-)
(Flag9prime-)
(Flag8prime-)
(Flag6prime-)
(Flag5prime-)
(Flag4prime-)
(Flag3prime-)
(Flag2prime-)
(Flag1prime-)
(Flag7prime-)
)
(:action Flag7Action
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
(Flag2-)
(Flag2prime-)
(Flag3-)
(Flag3prime-)
(Flag4-)
(Flag4prime-)
(Flag5-)
(Flag5prime-)
(Flag6-)
(Flag6prime-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Prim7Action-0
:parameters ()
:precondition
(and
(not (Flag1-))
(Flag1prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-1
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-2
:parameters ()
:precondition
(and
(not (Flag3-))
(Flag3prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-3
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-4
:parameters ()
:precondition
(and
(not (Flag5-))
(Flag5prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Prim7Action-5
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag1Action-0
:parameters ()
:precondition
(and
(NOT-CONTAINS-B-L)
)
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(DISARMED-G)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag3Action-0
:parameters ()
:precondition
(and
(NOT-CONTAINS-F-I)
)
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(NOT-CONTAINS-H-M)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag5Action-1
:parameters ()
:precondition
(and
(DISARMED-J)
)
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(NOT-CONTAINS-N-P)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-P
:parameters ()
:precondition
(and
(Flag8-)
(Flag8prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-P
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-J)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-P
:parameters ()
:precondition
(and
(Flag10-)
(Flag10prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-P
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-P
:parameters ()
:precondition
(and
(Flag12-)
(Flag12prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-G)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-G-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-P
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action DUNK-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(when
(and
(CONTAINS-N-P)
)
(and
(NOT-CONTAINS-N-P)
(not (CONTAINS-N-P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(when
(and
(CONTAINS-H-M)
)
(and
(NOT-CONTAINS-H-M)
(not (CONTAINS-H-M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(when
(and
(CONTAINS-F-I)
)
(and
(NOT-CONTAINS-F-I)
(not (CONTAINS-F-I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-G-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(when
(and
(CONTAINS-B-L)
)
(and
(NOT-CONTAINS-B-L)
(not (CONTAINS-B-L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action LET_THE_CATS_OUT-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag13prime-))
(not (Flag12prime-))
(not (Flag11prime-))
(not (Flag10prime-))
(not (Flag9prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag5prime-))
(not (Flag4prime-))
(not (Flag3prime-))
(not (Flag2prime-))
(not (Flag1prime-))
(not (Flag7prime-))
)
)
(:action Prim7Action-7
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag7prime-)
(not (Flag7-))
)

)
(:action Flag8Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Prim8Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Prim8Action-2
:parameters ()
:precondition
(and
(not (CONTAINS-N-P))
)
:effect
(and
(Flag8prime-)
(not (Flag8-))
)

)
(:action Flag9Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Prim9Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag9prime-)
(not (Flag9-))
)

)
(:action Flag10Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Prim10Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Prim10Action-2
:parameters ()
:precondition
(and
(not (CONTAINS-H-M))
)
:effect
(and
(Flag10prime-)
(not (Flag10-))
)

)
(:action Flag11Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Prim11Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Prim11Action-2
:parameters ()
:precondition
(and
(not (CONTAINS-F-I))
)
:effect
(and
(Flag11prime-)
(not (Flag11-))
)

)
(:action Flag12Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Prim12Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag12prime-)
(not (Flag12-))
)

)
(:action Flag13Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Prim13Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
(:action Prim13Action-2
:parameters ()
:precondition
(and
(not (CONTAINS-B-L))
)
:effect
(and
(Flag13prime-)
(not (Flag13-))
)

)
)
