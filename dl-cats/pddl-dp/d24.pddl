(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag43-)
(Flag42-)
(Flag41-)
(Flag40-)
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-AA-BL)
(NOT-CONTAINS-AI-AS)
(NOT-CONTAINS-AK-AU)
(NOT-CONTAINS-AM-BQ)
(NOT-CONTAINS-AL-BJ)
(NOT-CONTAINS-AO-AP)
(NOT-CONTAINS-AW-AJ)
(NOT-CONTAINS-AV-BG)
(NOT-CONTAINS-AY-BE)
(NOT-CONTAINS-BD-AG)
(NOT-CONTAINS-BA-AT)
(NOT-CONTAINS-BC-BR)
(NOT-CONTAINS-BM-AB)
(NOT-CONTAINS-BP-BF)
(DISARMED-AD)
(DISARMED-AF)
(DISARMED-AN)
(DISARMED-AQ)
(DISARMED-AX)
(DISARMED-BI)
(DISARMED-BV)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag22-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
(CONTAINS-BM-AB)
(CONTAINS-BC-BR)
(CONTAINS-BA-AT)
(CONTAINS-BD-AG)
(CONTAINS-AY-BE)
(CONTAINS-AV-BG)
(CONTAINS-AW-AJ)
(CONTAINS-AO-AP)
(CONTAINS-AL-BJ)
(CONTAINS-AM-BQ)
(CONTAINS-AK-AU)
(CONTAINS-AI-AS)
(CONTAINS-AA-BL)
)
(:derived (Flag22-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)(Flag20-)(Flag21-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-AA-BL)))

(:derived (Flag2-)(and (DISARMED-AD)))

(:derived (Flag3-)(and (DISARMED-AF)))

(:derived (Flag4-)(and (NOT-CONTAINS-AI-AS)))

(:derived (Flag5-)(and (NOT-CONTAINS-AK-AU)))

(:derived (Flag6-)(and (NOT-CONTAINS-AM-BQ)))

(:derived (Flag7-)(and (NOT-CONTAINS-AL-BJ)))

(:derived (Flag8-)(and (NOT-CONTAINS-AO-AP)))

(:derived (Flag9-)(and (DISARMED-AN)))

(:derived (Flag10-)(and (DISARMED-AQ)))

(:derived (Flag11-)(and (NOT-CONTAINS-AW-AJ)))

(:derived (Flag12-)(and (NOT-CONTAINS-AV-BG)))

(:derived (Flag13-)(and (NOT-CONTAINS-AY-BE)))

(:derived (Flag14-)(and (DISARMED-AX)))

(:derived (Flag15-)(and (NOT-CONTAINS-BD-AG)))

(:derived (Flag16-)(and (NOT-CONTAINS-BA-AT)))

(:derived (Flag17-)(and (NOT-CONTAINS-BC-BR)))

(:derived (Flag18-)(and (NOT-CONTAINS-BM-AB)))

(:derived (Flag19-)(and (DISARMED-BI)))

(:derived (Flag20-)(and (DISARMED-BV)))

(:derived (Flag21-)(and (NOT-CONTAINS-BP-BF)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BS
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BS
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(DISARMED-BV)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BS
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-BI)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BS
:parameters ()
:precondition
(and
(Flag26-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BS
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BS
:parameters ()
:precondition
(and
(Flag28-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BS
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BS
:parameters ()
:precondition
(and
(Flag30-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AX)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BS
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BS
:parameters ()
:precondition
(and
(Flag32-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BS
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BS
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AQ)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BS
:parameters ()
:precondition
(and
(Flag35-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AN)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AN-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BS
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BS
:parameters ()
:precondition
(and
(Flag37-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BS
:parameters ()
:precondition
(and
(Flag38-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BS
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BS
:parameters ()
:precondition
(and
(Flag40-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BS
:parameters ()
:precondition
(and
(Flag41-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AF)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BS
:parameters ()
:precondition
(and
(Flag42-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AD)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BS
:parameters ()
:precondition
(and
(Flag43-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(when
(and
(CONTAINS-BP-BF)
)
(and
(NOT-CONTAINS-BP-BF)
(not (CONTAINS-BP-BF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BP-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(when
(and
(CONTAINS-BM-AB)
)
(and
(NOT-CONTAINS-BM-AB)
(not (CONTAINS-BM-AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BM-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(when
(and
(CONTAINS-BC-BR)
)
(and
(NOT-CONTAINS-BC-BR)
(not (CONTAINS-BC-BR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(when
(and
(CONTAINS-BA-AT)
)
(and
(NOT-CONTAINS-BA-AT)
(not (CONTAINS-BA-AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(when
(and
(CONTAINS-BD-AG)
)
(and
(NOT-CONTAINS-BD-AG)
(not (CONTAINS-BD-AG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(when
(and
(CONTAINS-AY-BE)
)
(and
(NOT-CONTAINS-AY-BE)
(not (CONTAINS-AY-BE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(when
(and
(CONTAINS-AV-BG)
)
(and
(NOT-CONTAINS-AV-BG)
(not (CONTAINS-AV-BG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(when
(and
(CONTAINS-AW-AJ)
)
(and
(NOT-CONTAINS-AW-AJ)
(not (CONTAINS-AW-AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AN-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(when
(and
(CONTAINS-AO-AP)
)
(and
(NOT-CONTAINS-AO-AP)
(not (CONTAINS-AO-AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(when
(and
(CONTAINS-AL-BJ)
)
(and
(NOT-CONTAINS-AL-BJ)
(not (CONTAINS-AL-BJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-BJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(when
(and
(CONTAINS-AM-BQ)
)
(and
(NOT-CONTAINS-AM-BQ)
(not (CONTAINS-AM-BQ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-BQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(when
(and
(CONTAINS-AK-AU)
)
(and
(NOT-CONTAINS-AK-AU)
(not (CONTAINS-AK-AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(when
(and
(CONTAINS-AI-AS)
)
(and
(NOT-CONTAINS-AI-AS)
(not (CONTAINS-AI-AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(when
(and
(CONTAINS-AA-BL)
)
(and
(NOT-CONTAINS-AA-BL)
(not (CONTAINS-AA-BL))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BP-BF)))

(:derived (Flag24-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag26-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BM-AB)))

(:derived (Flag27-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BC-BR)))

(:derived (Flag28-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BA-AT)))

(:derived (Flag29-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BD-AG)))

(:derived (Flag30-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag31-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AY-BE)))

(:derived (Flag32-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AV-BG)))

(:derived (Flag33-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AW-AJ)))

(:derived (Flag34-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag35-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag36-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AO-AP)))

(:derived (Flag37-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AL-BJ)))

(:derived (Flag38-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AM-BQ)))

(:derived (Flag39-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AK-AU)))

(:derived (Flag40-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AI-AS)))

(:derived (Flag41-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag42-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag43-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AA-BL)))

)
