(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(CHECKCONSISTENCY-)
(DISARMED-F)
(DISARMED-M)
(NOT-CONTAINS-K-H)
(NOT-CONTAINS-J-A)
(NOT-CONTAINS-L-G)
(NOT-CONTAINS-Q-N)
(NOT-CONTAINS-R-D)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag8-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
(CONTAINS-Q-N)
(CONTAINS-L-G)
(CONTAINS-J-A)
(CONTAINS-K-H)
)
(:derived (Flag8-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (DISARMED-F)))

(:derived (Flag2-)(and (NOT-CONTAINS-K-H)))

(:derived (Flag3-)(and (NOT-CONTAINS-J-A)))

(:derived (Flag4-)(and (DISARMED-M)))

(:derived (Flag5-)(and (NOT-CONTAINS-L-G)))

(:derived (Flag6-)(and (NOT-CONTAINS-Q-N)))

(:derived (Flag7-)(and (NOT-CONTAINS-R-D)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-R
:parameters ()
:precondition
(and
(Flag9-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(when
(and
(CONTAINS-R-D)
)
(and
(NOT-CONTAINS-R-D)
(not (CONTAINS-R-D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-R
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(when
(and
(CONTAINS-Q-N)
)
(and
(NOT-CONTAINS-Q-N)
(not (CONTAINS-Q-N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-R
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(when
(and
(CONTAINS-L-G)
)
(and
(NOT-CONTAINS-L-G)
(not (CONTAINS-L-G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-R
:parameters ()
:precondition
(and
(Flag12-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-M-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-R
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(when
(and
(CONTAINS-J-A)
)
(and
(NOT-CONTAINS-J-A)
(not (CONTAINS-J-A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-R
:parameters ()
:precondition
(and
(Flag14-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(when
(and
(CONTAINS-K-H)
)
(and
(NOT-CONTAINS-K-H)
(not (CONTAINS-K-H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-R
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-R-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-Q-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-M)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-M-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-J-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag9-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-R-D)))

(:derived (Flag10-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-Q-N)))

(:derived (Flag11-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-L-G)))

(:derived (Flag12-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag13-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-J-A)))

(:derived (Flag14-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-K-H)))

(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)))

)
