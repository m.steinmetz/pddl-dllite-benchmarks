(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-AC-BA)
(NOT-CONTAINS-AB-AF)
(NOT-CONTAINS-AI-AT)
(NOT-CONTAINS-AJ-AQ)
(NOT-CONTAINS-AL-AM)
(NOT-CONTAINS-AW-AA)
(NOT-CONTAINS-AY-AS)
(NOT-CONTAINS-BB-AD)
(DISARMED-AP)
(DISARMED-AR)
(DISARMED-AX)
(DISARMED-AZ)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag13-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
(CONTAINS-AY-AS)
(CONTAINS-AW-AA)
(CONTAINS-AL-AM)
(CONTAINS-AJ-AQ)
(CONTAINS-AI-AT)
(CONTAINS-AB-AF)
(CONTAINS-AC-BA)
)
(:derived (Flag13-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-AC-BA)))

(:derived (Flag2-)(and (NOT-CONTAINS-AB-AF)))

(:derived (Flag3-)(and (NOT-CONTAINS-AI-AT)))

(:derived (Flag4-)(and (NOT-CONTAINS-AJ-AQ)))

(:derived (Flag5-)(and (NOT-CONTAINS-AL-AM)))

(:derived (Flag6-)(and (DISARMED-AP)))

(:derived (Flag7-)(and (DISARMED-AR)))

(:derived (Flag8-)(and (NOT-CONTAINS-AW-AA)))

(:derived (Flag9-)(and (NOT-CONTAINS-AY-AS)))

(:derived (Flag10-)(and (DISARMED-AX)))

(:derived (Flag11-)(and (DISARMED-AZ)))

(:derived (Flag12-)(and (NOT-CONTAINS-BB-AD)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BB
:parameters ()
:precondition
(and
(Flag14-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-BB
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AZ)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AZ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BB
:parameters ()
:precondition
(and
(Flag16-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AX)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BB
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BB
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BB
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AR)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BB
:parameters ()
:precondition
(and
(Flag20-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AP)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BB
:parameters ()
:precondition
(and
(Flag21-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BB
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BB
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BB
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BB
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(when
(and
(CONTAINS-BB-AD)
)
(and
(NOT-CONTAINS-BB-AD)
(not (CONTAINS-BB-AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AZ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(when
(and
(CONTAINS-AY-AS)
)
(and
(NOT-CONTAINS-AY-AS)
(not (CONTAINS-AY-AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AW-AA)
)
:effect
(and
(when
(and
(CONTAINS-AW-AA)
)
(and
(NOT-CONTAINS-AW-AA)
(not (CONTAINS-AW-AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(when
(and
(CONTAINS-AL-AM)
)
(and
(NOT-CONTAINS-AL-AM)
(not (CONTAINS-AL-AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(when
(and
(CONTAINS-AJ-AQ)
)
(and
(NOT-CONTAINS-AJ-AQ)
(not (CONTAINS-AJ-AQ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(when
(and
(CONTAINS-AI-AT)
)
(and
(NOT-CONTAINS-AI-AT)
(not (CONTAINS-AI-AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(when
(and
(CONTAINS-AB-AF)
)
(and
(NOT-CONTAINS-AB-AF)
(not (CONTAINS-AB-AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AB-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(when
(and
(CONTAINS-AC-BA)
)
(and
(NOT-CONTAINS-AC-BA)
(not (CONTAINS-AC-BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag14-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BB-AD)))

(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag16-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AY-AS)))

(:derived (Flag18-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AW-AA)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag20-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag21-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AL-AM)))

(:derived (Flag22-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AJ-AQ)))

(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AI-AT)))

(:derived (Flag24-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AB-AF)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AC-BA)))

)
