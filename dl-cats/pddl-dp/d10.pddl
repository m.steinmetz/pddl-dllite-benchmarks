(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-C-N)
(NOT-CONTAINS-B-M)
(NOT-CONTAINS-H-J)
(NOT-CONTAINS-K-I)
(NOT-CONTAINS-L-S)
(NOT-CONTAINS-P-G)
(DISARMED-E)
(DISARMED-F)
(DISARMED-R)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag10-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
(CONTAINS-L-S)
(CONTAINS-K-I)
(CONTAINS-H-J)
(CONTAINS-B-M)
(CONTAINS-C-N)
)
(:derived (Flag10-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-C-N)))

(:derived (Flag2-)(and (NOT-CONTAINS-B-M)))

(:derived (Flag3-)(and (DISARMED-E)))

(:derived (Flag4-)(and (DISARMED-F)))

(:derived (Flag5-)(and (NOT-CONTAINS-H-J)))

(:derived (Flag6-)(and (NOT-CONTAINS-K-I)))

(:derived (Flag7-)(and (NOT-CONTAINS-L-S)))

(:derived (Flag8-)(and (NOT-CONTAINS-P-G)))

(:derived (Flag9-)(and (DISARMED-R)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action DUNK-R-T
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-R)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-T
:parameters ()
:precondition
(and
(Flag12-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-P-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-T
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-T
:parameters ()
:precondition
(and
(Flag14-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-K-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-T
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-T
:parameters ()
:precondition
(and
(Flag16-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-F)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-T
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(DISARMED-E)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-E-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-T
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-T
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(when
(and
(CONTAINS-P-G)
)
(and
(NOT-CONTAINS-P-G)
(not (CONTAINS-P-G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-P-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-P-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(when
(and
(CONTAINS-L-S)
)
(and
(NOT-CONTAINS-L-S)
(not (CONTAINS-L-S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(when
(and
(CONTAINS-K-I)
)
(and
(NOT-CONTAINS-K-I)
(not (CONTAINS-K-I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-K-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-K-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(when
(and
(CONTAINS-H-J)
)
(and
(NOT-CONTAINS-H-J)
(not (CONTAINS-H-J))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-E-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(when
(and
(CONTAINS-B-M)
)
(and
(NOT-CONTAINS-B-M)
(not (CONTAINS-B-M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(when
(and
(CONTAINS-C-N)
)
(and
(NOT-CONTAINS-C-N)
(not (CONTAINS-C-N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-C-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag11-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag12-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-P-G)))

(:derived (Flag13-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-L-S)))

(:derived (Flag14-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-K-I)))

(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-H-J)))

(:derived (Flag16-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag18-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-B-M)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-C-N)))

)
