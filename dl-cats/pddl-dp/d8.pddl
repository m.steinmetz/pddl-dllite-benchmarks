(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-B-L)
(NOT-CONTAINS-F-I)
(NOT-CONTAINS-H-M)
(NOT-CONTAINS-N-P)
(DISARMED-G)
(DISARMED-J)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag7-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
(CONTAINS-H-M)
(CONTAINS-F-I)
(CONTAINS-B-L)
)
(:derived (Flag7-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-B-L)))

(:derived (Flag2-)(and (DISARMED-G)))

(:derived (Flag3-)(and (NOT-CONTAINS-F-I)))

(:derived (Flag4-)(and (NOT-CONTAINS-H-M)))

(:derived (Flag5-)(and (DISARMED-J)))

(:derived (Flag6-)(and (NOT-CONTAINS-N-P)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action DUNK-N-P
:parameters ()
:precondition
(and
(Flag8-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-P
:parameters ()
:precondition
(and
(Flag9-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-J)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-P
:parameters ()
:precondition
(and
(Flag10-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-P
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-P
:parameters ()
:precondition
(and
(Flag12-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-G)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-G-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-P
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(when
(and
(CONTAINS-N-P)
)
(and
(NOT-CONTAINS-N-P)
(not (CONTAINS-N-P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-N-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-J-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(when
(and
(CONTAINS-H-M)
)
(and
(NOT-CONTAINS-H-M)
(not (CONTAINS-H-M))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-H-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-H-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(when
(and
(CONTAINS-F-I)
)
(and
(NOT-CONTAINS-F-I)
(not (CONTAINS-F-I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-F-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-F-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-G-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(when
(and
(CONTAINS-B-L)
)
(and
(NOT-CONTAINS-B-L)
(not (CONTAINS-B-L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag8-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-N-P)))

(:derived (Flag9-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag10-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-H-M)))

(:derived (Flag11-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-F-I)))

(:derived (Flag12-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag13-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-B-L)))

)
