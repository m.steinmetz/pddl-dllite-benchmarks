(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(CHECKCONSISTENCY-)
(DISARMED-C)
(DISARMED-Q)
(DISARMED-R)
(NOT-CONTAINS-A-V)
(NOT-CONTAINS-B-U)
(NOT-CONTAINS-D-P)
(NOT-CONTAINS-I-H)
(NOT-CONTAINS-L-E)
(NOT-CONTAINS-T-G)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag10-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
(CONTAINS-L-E)
(CONTAINS-I-H)
(CONTAINS-D-P)
(CONTAINS-B-U)
(CONTAINS-A-V)
)
(:derived (Flag10-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-A-V)))

(:derived (Flag2-)(and (DISARMED-C)))

(:derived (Flag3-)(and (NOT-CONTAINS-B-U)))

(:derived (Flag4-)(and (NOT-CONTAINS-D-P)))

(:derived (Flag5-)(and (NOT-CONTAINS-I-H)))

(:derived (Flag6-)(and (NOT-CONTAINS-L-E)))

(:derived (Flag7-)(and (DISARMED-Q)))

(:derived (Flag8-)(and (DISARMED-R)))

(:derived (Flag9-)(and (NOT-CONTAINS-T-G)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-V
:parameters ()
:precondition
(and
(Flag11-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(when
(and
(CONTAINS-T-G)
)
(and
(NOT-CONTAINS-T-G)
(not (CONTAINS-T-G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-T-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-V
:parameters ()
:precondition
(and
(Flag12-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-V
:parameters ()
:precondition
(and
(Flag13-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-V
:parameters ()
:precondition
(and
(Flag14-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(when
(and
(CONTAINS-L-E)
)
(and
(NOT-CONTAINS-L-E)
(not (CONTAINS-L-E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-V
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(when
(and
(CONTAINS-I-H)
)
(and
(NOT-CONTAINS-I-H)
(not (CONTAINS-I-H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-I-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-V
:parameters ()
:precondition
(and
(Flag16-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(when
(and
(CONTAINS-D-P)
)
(and
(NOT-CONTAINS-D-P)
(not (CONTAINS-D-P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-D-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-V
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(when
(and
(CONTAINS-B-U)
)
(and
(NOT-CONTAINS-B-U)
(not (CONTAINS-B-U))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-V
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-V
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(when
(and
(CONTAINS-A-V)
)
(and
(NOT-CONTAINS-A-V)
(not (CONTAINS-A-V))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-A-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-T-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-T-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-R)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-Q)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-L-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-I-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-I-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-D-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-D-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-B-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-C)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-A-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-A-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag11-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-T-G)))

(:derived (Flag12-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag13-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag14-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-L-E)))

(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-I-H)))

(:derived (Flag16-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-D-P)))

(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-B-U)))

(:derived (Flag18-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-A-V)))

)
