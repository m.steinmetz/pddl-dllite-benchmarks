(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(CHECKCONSISTENCY-)
(NOT-CONTAINS-AC-AS)
(NOT-CONTAINS-AE-AN)
(NOT-CONTAINS-AG-AO)
(NOT-CONTAINS-AK-AA)
(NOT-CONTAINS-AM-AR)
(NOT-CONTAINS-AQ-AD)
(NOT-CONTAINS-AU-BF)
(NOT-CONTAINS-BD-BH)
(NOT-CONTAINS-BA-AX)
(NOT-CONTAINS-BC-AW)
(DISARMED-AB)
(DISARMED-AF)
(DISARMED-AI)
(DISARMED-AY)
(DISARMED-BB)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag16-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
(CONTAINS-BA-AX)
(CONTAINS-BD-BH)
(CONTAINS-AU-BF)
(CONTAINS-AQ-AD)
(CONTAINS-AM-AR)
(CONTAINS-AK-AA)
(CONTAINS-AG-AO)
(CONTAINS-AE-AN)
(CONTAINS-AC-AS)
)
(:derived (Flag16-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-AC-AS)))

(:derived (Flag2-)(and (DISARMED-AB)))

(:derived (Flag3-)(and (NOT-CONTAINS-AE-AN)))

(:derived (Flag4-)(and (NOT-CONTAINS-AG-AO)))

(:derived (Flag5-)(and (DISARMED-AF)))

(:derived (Flag6-)(and (DISARMED-AI)))

(:derived (Flag7-)(and (NOT-CONTAINS-AK-AA)))

(:derived (Flag8-)(and (NOT-CONTAINS-AM-AR)))

(:derived (Flag9-)(and (NOT-CONTAINS-AQ-AD)))

(:derived (Flag10-)(and (NOT-CONTAINS-AU-BF)))

(:derived (Flag11-)(and (DISARMED-AY)))

(:derived (Flag12-)(and (NOT-CONTAINS-BD-BH)))

(:derived (Flag13-)(and (NOT-CONTAINS-BA-AX)))

(:derived (Flag14-)(and (DISARMED-BB)))

(:derived (Flag15-)(and (NOT-CONTAINS-BC-AW)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BH
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BH
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-BB)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BH
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BH
:parameters ()
:precondition
(and
(Flag20-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BH
:parameters ()
:precondition
(and
(Flag21-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AY)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BH
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BH
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BH
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BH
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BH
:parameters ()
:precondition
(and
(Flag26-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AI)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BH
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AF)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BH
:parameters ()
:precondition
(and
(Flag28-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BH
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AE-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BH
:parameters ()
:precondition
(and
(Flag30-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AB)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BH
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(when
(and
(CONTAINS-BC-AW)
)
(and
(NOT-CONTAINS-BC-AW)
(not (CONTAINS-BC-AW))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BC-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(when
(and
(CONTAINS-BA-AX)
)
(and
(NOT-CONTAINS-BA-AX)
(not (CONTAINS-BA-AX))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(when
(and
(CONTAINS-BD-BH)
)
(and
(NOT-CONTAINS-BD-BH)
(not (CONTAINS-BD-BH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BD-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(when
(and
(CONTAINS-AU-BF)
)
(and
(NOT-CONTAINS-AU-BF)
(not (CONTAINS-AU-BF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(when
(and
(CONTAINS-AQ-AD)
)
(and
(NOT-CONTAINS-AQ-AD)
(not (CONTAINS-AQ-AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AQ-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(when
(and
(CONTAINS-AM-AR)
)
(and
(NOT-CONTAINS-AM-AR)
(not (CONTAINS-AM-AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AM-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AM-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AK-AA)
)
:effect
(and
(when
(and
(CONTAINS-AK-AA)
)
(and
(NOT-CONTAINS-AK-AA)
(not (CONTAINS-AK-AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(when
(and
(CONTAINS-AG-AO)
)
(and
(NOT-CONTAINS-AG-AO)
(not (CONTAINS-AG-AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(when
(and
(CONTAINS-AE-AN)
)
(and
(NOT-CONTAINS-AE-AN)
(not (CONTAINS-AE-AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AE-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AE-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(when
(and
(CONTAINS-AC-AS)
)
(and
(NOT-CONTAINS-AC-AS)
(not (CONTAINS-AC-AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AC-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BC-AW)))

(:derived (Flag18-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BA-AX)))

(:derived (Flag20-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BD-BH)))

(:derived (Flag21-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag22-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AU-BF)))

(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AQ-AD)))

(:derived (Flag24-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AM-AR)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AK-AA)))

(:derived (Flag26-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag27-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag28-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AG-AO)))

(:derived (Flag29-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AE-AN)))

(:derived (Flag30-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag31-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AC-AS)))

)
