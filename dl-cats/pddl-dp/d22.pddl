(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag39-)
(Flag38-)
(Flag37-)
(Flag36-)
(Flag35-)
(Flag34-)
(Flag33-)
(Flag32-)
(Flag31-)
(Flag30-)
(Flag29-)
(Flag28-)
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(CHECKCONSISTENCY-)
(DISARMED-AC)
(DISARMED-AQ)
(DISARMED-AR)
(DISARMED-AW)
(DISARMED-AV)
(DISARMED-BP)
(NOT-CONTAINS-AD-AK)
(NOT-CONTAINS-AG-AM)
(NOT-CONTAINS-AF-AT)
(NOT-CONTAINS-AI-AO)
(NOT-CONTAINS-AH-BM)
(NOT-CONTAINS-AJ-BR)
(NOT-CONTAINS-AP-AZ)
(NOT-CONTAINS-AU-BC)
(NOT-CONTAINS-AX-AB)
(NOT-CONTAINS-BA-BG)
(NOT-CONTAINS-BN-BD)
(NOT-CONTAINS-BH-AN)
(NOT-CONTAINS-BQ-BE)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(Flag14-)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag20-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
(CONTAINS-BH-AN)
(CONTAINS-BN-BD)
(CONTAINS-BA-BG)
(CONTAINS-AX-AB)
(CONTAINS-AU-BC)
(CONTAINS-AP-AZ)
(CONTAINS-AJ-BR)
(CONTAINS-AH-BM)
(CONTAINS-AI-AO)
(CONTAINS-AF-AT)
(CONTAINS-AG-AM)
(CONTAINS-AD-AK)
)
(:derived (Flag20-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(Flag14-)(Flag15-)(Flag16-)(Flag17-)(Flag18-)(Flag19-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (DISARMED-AC)))

(:derived (Flag2-)(and (NOT-CONTAINS-AD-AK)))

(:derived (Flag3-)(and (NOT-CONTAINS-AG-AM)))

(:derived (Flag4-)(and (NOT-CONTAINS-AF-AT)))

(:derived (Flag5-)(and (NOT-CONTAINS-AI-AO)))

(:derived (Flag6-)(and (NOT-CONTAINS-AH-BM)))

(:derived (Flag7-)(and (NOT-CONTAINS-AJ-BR)))

(:derived (Flag8-)(and (DISARMED-AQ)))

(:derived (Flag9-)(and (NOT-CONTAINS-AP-AZ)))

(:derived (Flag10-)(and (DISARMED-AR)))

(:derived (Flag11-)(and (NOT-CONTAINS-AU-BC)))

(:derived (Flag12-)(and (DISARMED-AW)))

(:derived (Flag13-)(and (DISARMED-AV)))

(:derived (Flag14-)(and (NOT-CONTAINS-AX-AB)))

(:derived (Flag15-)(and (NOT-CONTAINS-BA-BG)))

(:derived (Flag16-)(and (NOT-CONTAINS-BN-BD)))

(:derived (Flag17-)(and (NOT-CONTAINS-BH-AN)))

(:derived (Flag18-)(and (DISARMED-BP)))

(:derived (Flag19-)(and (NOT-CONTAINS-BQ-BE)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BR
:parameters ()
:precondition
(and
(Flag21-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(when
(and
(CONTAINS-BQ-BE)
)
(and
(NOT-CONTAINS-BQ-BE)
(not (CONTAINS-BQ-BE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BR
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BR
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(when
(and
(CONTAINS-BH-AN)
)
(and
(NOT-CONTAINS-BH-AN)
(not (CONTAINS-BH-AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BR
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(when
(and
(CONTAINS-BN-BD)
)
(and
(NOT-CONTAINS-BN-BD)
(not (CONTAINS-BN-BD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BN-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BR
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(when
(and
(CONTAINS-BA-BG)
)
(and
(NOT-CONTAINS-BA-BG)
(not (CONTAINS-BA-BG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BR
:parameters ()
:precondition
(and
(Flag26-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(when
(and
(CONTAINS-AX-AB)
)
(and
(NOT-CONTAINS-AX-AB)
(not (CONTAINS-AX-AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BR
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BR
:parameters ()
:precondition
(and
(Flag28-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BR
:parameters ()
:precondition
(and
(Flag29-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(when
(and
(CONTAINS-AU-BC)
)
(and
(NOT-CONTAINS-AU-BC)
(not (CONTAINS-AU-BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BR
:parameters ()
:precondition
(and
(Flag30-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BR
:parameters ()
:precondition
(and
(Flag31-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(when
(and
(CONTAINS-AP-AZ)
)
(and
(NOT-CONTAINS-AP-AZ)
(not (CONTAINS-AP-AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BR
:parameters ()
:precondition
(and
(Flag32-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BR
:parameters ()
:precondition
(and
(Flag33-)
)
:effect
(and
(when
(and
(CONTAINS-AJ-BR)
)
(and
(NOT-CONTAINS-AJ-BR)
(not (CONTAINS-AJ-BR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BR
:parameters ()
:precondition
(and
(Flag34-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(when
(and
(CONTAINS-AH-BM)
)
(and
(NOT-CONTAINS-AH-BM)
(not (CONTAINS-AH-BM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BR
:parameters ()
:precondition
(and
(Flag35-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(when
(and
(CONTAINS-AI-AO)
)
(and
(NOT-CONTAINS-AI-AO)
(not (CONTAINS-AI-AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BR
:parameters ()
:precondition
(and
(Flag36-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(when
(and
(CONTAINS-AF-AT)
)
(and
(NOT-CONTAINS-AF-AT)
(not (CONTAINS-AF-AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BR
:parameters ()
:precondition
(and
(Flag37-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(when
(and
(CONTAINS-AG-AM)
)
(and
(NOT-CONTAINS-AG-AM)
(not (CONTAINS-AG-AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BR
:parameters ()
:precondition
(and
(Flag38-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(when
(and
(CONTAINS-AD-AK)
)
(and
(NOT-CONTAINS-AD-AK)
(not (CONTAINS-AD-AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BR
:parameters ()
:precondition
(and
(Flag39-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BQ-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-BP)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BH-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BN-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BN-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BA-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AV)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AW)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AU-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AR)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AP-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AQ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AQ)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AJ-BR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AH-BM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AI-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AF-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AD-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AC)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag21-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BQ-BE)))

(:derived (Flag22-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BH-AN)))

(:derived (Flag24-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BN-BD)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BA-BG)))

(:derived (Flag26-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AX-AB)))

(:derived (Flag27-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag28-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag29-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AU-BC)))

(:derived (Flag30-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag31-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AP-AZ)))

(:derived (Flag32-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag33-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AJ-BR)))

(:derived (Flag34-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AH-BM)))

(:derived (Flag35-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AI-AO)))

(:derived (Flag36-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AF-AT)))

(:derived (Flag37-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AG-AM)))

(:derived (Flag38-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AD-AK)))

(:derived (Flag39-)(and (NOT-CHECKCONSISTENCY-)))

)
