(define (domain grounded-SIMPLE-ADL-BTCAT)
(:requirements
:strips
)
(:predicates
(Flag27-)
(Flag26-)
(Flag25-)
(Flag24-)
(Flag23-)
(Flag22-)
(Flag21-)
(Flag20-)
(Flag19-)
(Flag18-)
(Flag17-)
(Flag16-)
(Flag15-)
(CHECKCONSISTENCY-)
(DISARMED-AD)
(DISARMED-AK)
(DISARMED-AW)
(DISARMED-BA)
(NOT-CONTAINS-AA-BC)
(NOT-CONTAINS-AG-AU)
(NOT-CONTAINS-AL-AR)
(NOT-CONTAINS-AO-AC)
(NOT-CONTAINS-AS-AE)
(NOT-CONTAINS-AV-AH)
(NOT-CONTAINS-AY-AI)
(NOT-CONTAINS-AX-AP)
(NOT-CONTAINS-BB-AJ)
(Flag13-)
(Flag12-)
(Flag11-)
(Flag10-)
(Flag9-)
(Flag8-)
(Flag7-)
(Flag6-)
(Flag5-)
(Flag4-)
(Flag3-)
(Flag2-)
(Flag1-)
(Flag14-)
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
(CONTAINS-AX-AP)
(CONTAINS-AY-AI)
(CONTAINS-AV-AH)
(CONTAINS-AS-AE)
(CONTAINS-AO-AC)
(CONTAINS-AL-AR)
(CONTAINS-AG-AU)
(CONTAINS-AA-BC)
)
(:derived (Flag14-)(and (Flag1-)(Flag2-)(Flag3-)(Flag4-)(Flag5-)(Flag6-)(Flag7-)(Flag8-)(Flag9-)(Flag10-)(Flag11-)(Flag12-)(Flag13-)(NOT-CHECKCONSISTENCY-)))

(:derived (Flag1-)(and (NOT-CONTAINS-AA-BC)))

(:derived (Flag2-)(and (DISARMED-AD)))

(:derived (Flag3-)(and (NOT-CONTAINS-AG-AU)))

(:derived (Flag4-)(and (DISARMED-AK)))

(:derived (Flag5-)(and (NOT-CONTAINS-AL-AR)))

(:derived (Flag6-)(and (NOT-CONTAINS-AO-AC)))

(:derived (Flag7-)(and (NOT-CONTAINS-AS-AE)))

(:derived (Flag8-)(and (DISARMED-AW)))

(:derived (Flag9-)(and (NOT-CONTAINS-AV-AH)))

(:derived (Flag10-)(and (NOT-CONTAINS-AY-AI)))

(:derived (Flag11-)(and (NOT-CONTAINS-AX-AP)))

(:derived (Flag12-)(and (DISARMED-BA)))

(:derived (Flag13-)(and (NOT-CONTAINS-BB-AJ)))

(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BC
:parameters ()
:precondition
(and
(Flag15-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(when
(and
(CONTAINS-BB-AJ)
)
(and
(NOT-CONTAINS-BB-AJ)
(not (CONTAINS-BB-AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BC
:parameters ()
:precondition
(and
(Flag16-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BC
:parameters ()
:precondition
(and
(Flag17-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(when
(and
(CONTAINS-AX-AP)
)
(and
(NOT-CONTAINS-AX-AP)
(not (CONTAINS-AX-AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BC
:parameters ()
:precondition
(and
(Flag18-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(when
(and
(CONTAINS-AY-AI)
)
(and
(NOT-CONTAINS-AY-AI)
(not (CONTAINS-AY-AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BC
:parameters ()
:precondition
(and
(Flag19-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(when
(and
(CONTAINS-AV-AH)
)
(and
(NOT-CONTAINS-AV-AH)
(not (CONTAINS-AV-AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BC
:parameters ()
:precondition
(and
(Flag20-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-BC
:parameters ()
:precondition
(and
(Flag21-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(when
(and
(CONTAINS-AS-AE)
)
(and
(NOT-CONTAINS-AS-AE)
(not (CONTAINS-AS-AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AS-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BC
:parameters ()
:precondition
(and
(Flag22-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(when
(and
(CONTAINS-AO-AC)
)
(and
(NOT-CONTAINS-AO-AC)
(not (CONTAINS-AO-AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AO-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BC
:parameters ()
:precondition
(and
(Flag23-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(when
(and
(CONTAINS-AL-AR)
)
(and
(NOT-CONTAINS-AL-AR)
(not (CONTAINS-AL-AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BC
:parameters ()
:precondition
(and
(Flag24-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BC
:parameters ()
:precondition
(and
(Flag25-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(when
(and
(CONTAINS-AG-AU)
)
(and
(NOT-CONTAINS-AG-AU)
(not (CONTAINS-AG-AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BC
:parameters ()
:precondition
(and
(Flag26-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BC
:parameters ()
:precondition
(and
(Flag27-)
)
:effect
(and
(when
(and
(CONTAINS-AA-BC)
)
(and
(NOT-CONTAINS-AA-BC)
(not (CONTAINS-AA-BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action LET_THE_CATS_OUT-AA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-BB-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-BA)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AX-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AX-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AY-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AV-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AV-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AW)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AS-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AS-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AO-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AO-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AL-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AK)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AG-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(DISARMED-AD)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:action DUNK-AA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(CONTAINS-AA-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
)
)
(:derived (Flag15-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-BB-AJ)))

(:derived (Flag16-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag17-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AX-AP)))

(:derived (Flag18-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AY-AI)))

(:derived (Flag19-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AV-AH)))

(:derived (Flag20-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag21-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AS-AE)))

(:derived (Flag22-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AO-AC)))

(:derived (Flag23-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AL-AR)))

(:derived (Flag24-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag25-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AG-AU)))

(:derived (Flag26-)(and (NOT-CHECKCONSISTENCY-)))

(:derived (Flag27-)(and (NOT-CHECKCONSISTENCY-)(CONTAINS-AA-BC)))

)
