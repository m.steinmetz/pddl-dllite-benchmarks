(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j l 
  )
  (:init
    (cat l)
    (contains h i)
    (cat f)
    (cat a)
    (bomb i)
    (contains j f)
    (contains c l)
    (contains b a)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
            (not
              (disarmed ?x)
            )
          )
        )
      )
    )
  )
)