(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    aa ac ab ae ad ag af ai ah ak aj am al ao an aq ap as ar au at aw av ay ax az 
  )
  (:init
    (bomb av)
    (cat az)
    (contains ax ab)
    (contains af az)
    (cat au)
    (cat ar)
    (bomb as)
    (contains al am)
    (contains ap ao)
    (cat ao)
    (cat am)
    (contains aq av)
    (contains ay ad)
    (cat ah)
    (contains ak as)
    (contains aw au)
    (contains ac ah)
    (cat ad)
    (contains an ar)
    (bomb ab)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (not
              (disarmed ?x)
            )
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
          )
        )
      )
    )
  )
)