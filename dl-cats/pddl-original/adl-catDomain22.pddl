(define
  (domain BTcat)
  (:requirements :adl)
  (:predicates
    (package ?x )
    (bomb ?x )
    (disarmed ?x )
    (object ?x )
    (cat ?x )
    (contains ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action dunk
    :parameters ( ?x ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (package ?x)
        (exists (?z_0 )
          (contains ?x ?z_0)
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (and
          (bomb ?y)
          (contains ?x ?y)
        )
        (and
          (disarmed ?x)
        )
      )
    )
  )
  (:action let_the_cats_out
    :parameters ( ?x ?y )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (or
        (package ?x)
        (exists (?z_0 )
          (contains ?x ?z_0)
        )
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (and
          (cat ?y)
          (contains ?x ?y)
        )
        (and
          (not (package ?x))
          (not (contains ?x ?y))
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?y_2 ?y_1 ?x_1 )
            (and
              (contains ?x_1 ?y_2)
              (contains ?x_1 ?y_1)
              (not (?y_1 ?y_2))
            )
          )
          (exists (?x_0 )
            (and
              (bomb ?x_0)
              (cat ?x_0)
            )
          )
        )
        (Error)
      )
    )
  )
)