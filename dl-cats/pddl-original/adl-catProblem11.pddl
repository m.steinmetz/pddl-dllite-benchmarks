(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l o n q p s r u t v 
  )
  (:init
    (contains a v)
    (contains q m)
    (cat h)
    (contains i h)
    (cat e)
    (contains b u)
    (cat g)
    (contains t g)
    (contains l e)
    (bomb k)
    (bomb m)
    (contains d p)
    (contains c k)
    (contains r f)
    (cat u)
    (cat v)
    (cat p)
    (bomb f)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
            (not
              (disarmed ?x)
            )
          )
        )
      )
    )
  )
)