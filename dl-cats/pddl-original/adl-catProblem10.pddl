(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l o n q p s r t 
  )
  (:init
    (cat m)
    (cat n)
    (contains h j)
    (cat i)
    (cat j)
    (contains f d)
    (cat g)
    (bomb t)
    (contains r a)
    (contains k i)
    (contains c n)
    (contains e t)
    (contains b m)
    (bomb a)
    (contains p g)
    (bomb d)
    (contains l s)
    (cat s)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (not
              (disarmed ?x)
            )
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
          )
        )
      )
    )
  )
)