(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l o n q p s r u t w v x 
  )
  (:init
    (cat n)
    (contains t q)
    (contains g f)
    (cat j)
    (cat k)
    (bomb q)
    (cat f)
    (contains a k)
    (bomb w)
    (contains o w)
    (contains u r)
    (contains e n)
    (contains l c)
    (contains m j)
    (contains h p)
    (bomb c)
    (contains x v)
    (cat p)
    (cat v)
    (cat r)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
            (not
              (disarmed ?x)
            )
          )
        )
      )
    )
  )
)