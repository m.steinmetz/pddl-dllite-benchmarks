(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l o n p 
  )
  (:init
    (cat l)
    (cat m)
    (contains h m)
    (cat i)
    (bomb e)
    (contains f i)
    (contains n p)
    (contains g o)
    (contains j e)
    (contains b l)
    (bomb o)
    (cat p)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
            (not
              (disarmed ?x)
            )
          )
        )
      )
    )
  )
)