(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l n 
  )
  (:init
    (cat l)
    (cat m)
    (contains j l)
    (cat d)
    (cat f)
    (contains i b)
    (contains g m)
    (bomb h)
    (contains c f)
    (bomb b)
    (contains k h)
    (contains e d)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (not
              (disarmed ?x)
            )
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
          )
        )
      )
    )
  )
)