(define (problem BTcat_problem )
  (:domain BTcat)
  (:objects
    a c b e d g f i h k j m l o n q p r 
  )
  (:init
    (cat n)
    (contains r d)
    (cat h)
    (contains j a)
    (cat d)
    (cat g)
    (contains f o)
    (cat a)
    (bomb i)
    (contains q n)
    (bomb o)
    (contains l g)
    (contains m i)
    (contains k h)
  )
  (:goal
    (and
      (not (CheckConsistency))
      (not (Error))
      (not
        (exists ( ?x )
          (and
            (or
              (package ?x)
              (exists (?z_0 )
                (contains ?x ?z_0)
              )
            )
            (not
              (disarmed ?x)
            )
          )
        )
      )
    )
  )
)