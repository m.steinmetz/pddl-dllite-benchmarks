(define (domain Wsmo2VTAT)
  (:requirements :ekab)

(:predicates 
	(invoice ?x)
	(trip ?x)
	(flight ?x)
	(flightTicket ?x)
	(hotelStay ?x)
	(carHotelBundleOption ?x)
	(itinerary ?x)
	(flightRequest ?x)
	(hotelStayRequest ?x)
	(hotelStayConfirmation ?x)
	(carRentalRequest ?x)
	(carRental ?x)
	(carRentalBooking ?x)
	(airportShuttle ?x)
	(directlyAfterObj ?x ?y)
	(notFree ?x)
)

(:axioms
  (isA flightRequest flight)
  (isA hotelStayRequest hotelStay)
  (isA carRentalRequest carRental)
  (isA flightRequest (not hotelStayRequest))
  (isA flightRequest (not carRentalRequest))
  (isA hotelStayRequest (not carRentalRequest))
)

(:rule rule_vtaShuttleService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y))
	(mko (flightTicket ?x)))
:action vtaShuttleService
)

(:rule rule_vtaCarHotelBundlingService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?z))
	(mko (hotelStayConfirmation ?x)) (mko (carRentalBooking ?y)))
:action vtaCarHotelBundlingService
)

(:rule rule_vtaCarRentalService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) 
	(mko (carRental ?x)))
:action vtaCarRentalService
)

(:rule rule_vtaHotelService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y))
	(mko (hotelStay ?x) ))
:action vtaHotelService
)

(:rule rule_vtaFlightService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y))
	(mko (flight ?x)))
:action vtaFlightService
)

(:rule rule_vtaTripCombinationService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?z)) (mko (directlyAfterObj ?z ?t))
	(mko (airportShuttle ?x)) (mko (carHotelBundleOption ?y)))
:action vtaTripCombinationService
)

(:rule rule_vtaTripMakerService
:condition (and (mko (notFree ?q)) (mko (directlyAfterObj ?q ?y)) (mko (directlyAfterObj ?y ?z)) (mko (directlyAfterObj ?z ?t)) 
	(mko (trip ?x)))
:action vtaTripMakerService
)

(:action vtaShuttleService
  :parameters (?x ?y ?q)
  :effects (
    :condition (:True)
    :add ((airportShuttle ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaCarHotelBundlingService
  :parameters (?x ?y ?z ?q)
  :effects (
    :condition (:True)
    :add ((carHotelBundleOption ?z) (notFree ?z))
    :delete ((notFree ?q))
  )
)

(:action vtaCarRentalService
  :parameters (?x ?y ?q)
  :effects (
    :condition (:True)
    :add ((carRentalBooking ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaHotelService
  :parameters (?x ?y ?q)
  :effects (
    :condition (:True)
    :add ((hotelStayConfirmation ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaFlightService
  :parameters (?x ?y ?q)
  :effects (
    :condition (:True)
    :add ((flightTicket ?y) (notFree ?y))
    :delete ((notFree ?q))
  )
)

(:action vtaTripCombinationService
  :parameters (?x ?y ?z ?t ?q)
  :effects (
    :condition (:True)
    :add ((itinerary ?z) (invoice ?t) (notFree ?t))
    :delete ((notFree ?q))
  )
)

(:action vtaTripMakerService
  :parameters (?x ?y ?z ?t ?q)
  :effects (
    :condition (:True)
    :add ((flightRequest ?y) (hotelStayRequest ?z) (carRentalRequest ?t) (notFree ?t))
    :delete ((notFree ?q))
  )
)

)
