(define
  (domain Wsmo2VTAT)
  (:requirements :adl)
  (:predicates
    (notFree ?x )
    (hotelStay ?x )
    (itinerary ?x )
    (airportShuttle ?x )
    (hotelStayRequest ?x )
    (hotelStayConfirmation ?x )
    (carRental ?x )
    (flightRequest ?x )
    (carRentalBooking ?x )
    (invoice ?x )
    (carHotelBundleOption ?x )
    (flight ?x )
    (flightTicket ?x )
    (trip ?x )
    (carRentalRequest ?x )
    (directlyAfterObj ?x ?y )
    (CheckConsistency)
    (Error)
  )
  (:action vtaShuttleService
    :parameters ( ?x ?y ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (flightTicket ?x)
        (directlyAfterObj ?q ?y)
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (airportShuttle ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaTripMakerService
    :parameters ( ?x ?y ?z ?t ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?y ?z)
        (directlyAfterObj ?q ?y)
        (trip ?x)
        (directlyAfterObj ?z ?t)
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (flightRequest ?y)
          (hotelStayRequest ?z)
          (carRentalRequest ?t)
          (notFree ?t)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaTripCombinationService
    :parameters ( ?x ?y ?z ?t ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (carHotelBundleOption ?y)
        (directlyAfterObj ?z ?t)
        (airportShuttle ?x)
        (notFree ?q)
        (directlyAfterObj ?q ?z)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (itinerary ?z)
          (invoice ?t)
          (notFree ?t)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaCarRentalService
    :parameters ( ?x ?y ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (or
          (carRental ?x)
          (carRentalRequest ?x)
        )
        (directlyAfterObj ?q ?y)
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (carRentalBooking ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaFlightService
    :parameters ( ?x ?y ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (or
          (flight ?x)
          (flightRequest ?x)
        )
        (directlyAfterObj ?q ?y)
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (flightTicket ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaCarHotelBundlingService
    :parameters ( ?x ?y ?z ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (carRentalBooking ?y)
        (hotelStayConfirmation ?x)
        (directlyAfterObj ?q ?z)
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (carHotelBundleOption ?z)
          (notFree ?z)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action vtaHotelService
    :parameters ( ?x ?y ?q )
    :precondition ( and
      (not (CheckConsistency))
      (not (Error))
      (and
        (directlyAfterObj ?q ?y)
        (or
          (hotelStay ?x)
          (hotelStayRequest ?x)
        )
        (notFree ?q)
      )
    )
    :effect ( and
      (CheckConsistency)
      (when
        (= aa aa)
        (and
          (hotelStayConfirmation ?y)
          (notFree ?y)
          (not (notFree ?q))
        )
      )
    )
  )
  (:action CheckConsistencyAction
    :parameters ( )
    :precondition ( and
      (CheckConsistency)
      (not (Error))
    )
    :effect ( and
      (not (CheckConsistency))
      (when
        (or
          (exists (?x_0 )
            (and
              (hotelStayRequest ?x_0)
              (carRentalRequest ?x_0)
            )
          )
          (exists (?x_2 )
            (and
              (hotelStayRequest ?x_2)
              (flightRequest ?x_2)
            )
          )
          (exists (?x_1 )
            (and
              (flightRequest ?x_1)
              (carRentalRequest ?x_1)
            )
          )
        )
        (Error)
      )
    )
  )
)