(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag745-)
(Flag744-)
(Flag743-)
(Flag742-)
(Flag741-)
(Flag740-)
(Flag739-)
(Flag738-)
(Flag737-)
(Flag736-)
(Flag735-)
(Flag734-)
(Flag115-)
(NOT-LIFT_AT-BC)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(LIFT_AT-BD)
(ERROR-)
(Flag39-)
(Flag37-)
(Flag35-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AC)
(BOARDED-PASS_BA)
(Flag119-)
(Flag116-)
(LIFT_AT-AY)
(LIFT_AT-AX)
(LIFT_AT-AW)
(Flag40-)
(Flag38-)
(Flag36-)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_AI)
(BOARDED-PASS_AE)
(SERVED-PASS_BF)
(BOARDED-PASS_BE)
(Flag126-)
(Flag117-)
(Flag108-)
(LIFT_AT-AV)
(LIFT_AT-AU)
(LIFT_AT-AT)
(LIFT_AT-AS)
(LIFT_AT-AR)
(BOARDED-PASS_AG)
(BOARDED-PASS_AC)
(SERVED-PASS_BE)
(BOARDED-PASS_BB)
(SERVED-PASS_BA)
(Flag130-)
(Flag129-)
(Flag128-)
(Flag127-)
(Flag118-)
(LIFT_AT-AP)
(SERVED-PASS_AO)
(SERVED-PASS_AF)
(BOARDED-PASS_AD)
(SERVED-PASS_AQ)
(Flag109-)
(LIFT_AT-AN)
(LIFT_AT-AM)
(LIFT_AT-AL)
(LIFT_AT-AK)
(BOARDED-PASS_AF)
(SERVED-PASS_AG)
(SERVED-PASS_AD)
(SERVED-PASS_AE)
(Flag136-)
(Flag134-)
(Flag133-)
(Flag132-)
(LIFT_AT-AJ)
(BOARDED-PASS_AI)
(BOARDED-PASS_AQ)
(Flag135-)
(LIFT_AT-AH)
(BOARDED-PASS_AO)
(BOARDED-PASS_BF)
(SERVED-PASS_BB)
(Flag137-)
(LIFT_AT-AB)
(LIFT_AT-AA)
(Flag41-)
(Flag139-)
(Flag138-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-AB)
(NOT-LIFT_AT-AH)
(NOT-LIFT_AT-AA)
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BF)
(NOT-BOARDED-PASS_AO)
(NOT-LIFT_AT-AJ)
(NOT-BOARDED-PASS_AQ)
(NOT-BOARDED-PASS_AI)
(NOT-LIFT_AT-AK)
(NOT-SERVED-PASS_AE)
(NOT-SERVED-PASS_AD)
(NOT-SERVED-PASS_AG)
(NOT-BOARDED-PASS_AF)
(NOT-LIFT_AT-AL)
(NOT-LIFT_AT-AM)
(NOT-LIFT_AT-AN)
(NOT-LIFT_AT-AP)
(NOT-SERVED-PASS_AQ)
(NOT-BOARDED-PASS_AD)
(NOT-SERVED-PASS_AF)
(NOT-SERVED-PASS_AO)
(NOT-LIFT_AT-AR)
(NOT-SERVED-PASS_BA)
(NOT-BOARDED-PASS_BB)
(NOT-SERVED-PASS_BE)
(NOT-BOARDED-PASS_AC)
(NOT-BOARDED-PASS_AG)
(NOT-LIFT_AT-AS)
(NOT-LIFT_AT-AT)
(NOT-LIFT_AT-AU)
(NOT-LIFT_AT-AV)
(NOT-LIFT_AT-AW)
(NOT-BOARDED-PASS_BE)
(NOT-SERVED-PASS_BF)
(NOT-BOARDED-PASS_AE)
(NOT-SERVED-PASS_AI)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AY)
(NOT-LIFT_AT-AZ)
(NOT-BOARDED-PASS_BA)
(NOT-SERVED-PASS_AC)
(NOT-ERROR-)
(LIFT_AT-BC)
(NOT-LIFT_AT-BD)
(Flag745prime-)
(Flag744prime-)
(Flag743prime-)
(Flag742prime-)
(Flag741prime-)
(Flag740prime-)
(Flag739prime-)
(Flag738prime-)
(Flag737prime-)
(Flag736prime-)
(Flag735prime-)
(Flag734prime-)
(Flag115prime-)
(Flag39prime-)
(Flag37prime-)
(Flag35prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag119prime-)
(Flag116prime-)
(Flag40prime-)
(Flag38prime-)
(Flag36prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag126prime-)
(Flag117prime-)
(Flag108prime-)
(Flag130prime-)
(Flag129prime-)
(Flag128prime-)
(Flag127prime-)
(Flag118prime-)
(Flag109prime-)
(Flag136prime-)
(Flag134prime-)
(Flag133prime-)
(Flag132prime-)
(Flag135prime-)
(Flag137prime-)
(Flag41prime-)
(Flag139prime-)
(Flag138prime-)
)
(:action STOP-PASS_AQ-AB
:parameters ()
:precondition
(and
(Flag138-)
(Flag138prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AA
:parameters ()
:precondition
(and
(Flag139-)
(Flag139prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim73Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim74Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim106Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim107Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Flag138Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(Flag138-)
(Flag138prime-)
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Flag139Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(Flag139-)
(Flag139prime-)
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim171Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim172Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim204Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim205Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim237Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim238Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim270Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim271Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim303Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim304Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim336Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim337Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim369Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim370Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim402Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim403Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim435Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim436Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim468Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim469Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim501Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim502Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim534Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim535Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim567Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim568Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim600Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim601Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim633Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim634Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim666Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim667Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim699Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim700Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim732Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim733Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AH
:parameters ()
:precondition
(and
(Flag137-)
(Flag137prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Flag41Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(SERVED-PASS_AO)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(SERVED-PASS_AI)
(SERVED-PASS_AF)
(SERVED-PASS_AG)
(SERVED-PASS_AD)
(SERVED-PASS_AE)
(SERVED-PASS_AC)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(Flag32-)
(Flag32prime-)
(Flag34-)
(Flag34prime-)
(Flag36-)
(Flag36prime-)
(SERVED-PASS_BF)
(SERVED-PASS_BE)
(SERVED-PASS_BB)
(SERVED-PASS_BA)
(SERVED-PASS_AQ)
(Flag38-)
(Flag38prime-)
(Flag40-)
(Flag40prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim41Action-27
:parameters ()
:precondition
(and
(not (SERVED-PASS_BB))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AA-AB
:parameters ()
:precondition
(and
(LIFT_AT-AA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AA)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AA))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AB-AH
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AB-AA
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AH-AB
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim72Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim105Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Flag137Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(Flag137-)
(Flag137prime-)
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim170Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim203Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim236Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim269Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim302Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim335Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim368Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim401Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim434Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim467Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim500Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim533Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim566Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim599Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim632Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim665Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim698Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim731Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AJ
:parameters ()
:precondition
(and
(Flag135-)
(Flag135prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(when
(and
(BOARDED-PASS_BB)
)
(and
(SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(not (NOT-SERVED-PASS_BB))
(not (BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
(Flag741prime-)
)
:effect
(and
(when
(and
(Flag741-)
)
(and
(BOARDED-PASS_BF)
(not (NOT-BOARDED-PASS_BF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
(Flag734prime-)
)
:effect
(and
(when
(and
(Flag734-)
)
(and
(BOARDED-PASS_AO)
(not (NOT-BOARDED-PASS_AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AH-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AJ-AH
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim70Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim103Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Flag135Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(Flag135-)
(Flag135prime-)
)

)
(:action Prim135Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim168Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim201Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim234Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim267Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim300Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim333Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim366Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim399Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim432Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim465Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim498Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim531Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim564Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim597Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim630Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim663Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim696Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim729Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AN
:parameters ()
:precondition
(and
(Flag132-)
(Flag132prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AL
:parameters ()
:precondition
(and
(Flag133-)
(Flag133prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AM
:parameters ()
:precondition
(and
(Flag134-)
(Flag134prime-)
(Flag745prime-)
)
:effect
(and
(when
(and
(Flag745-)
)
(and
(BOARDED-PASS_AQ)
(not (NOT-BOARDED-PASS_AQ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AK
:parameters ()
:precondition
(and
(Flag136-)
(Flag136prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
(Flag735prime-)
)
:effect
(and
(when
(and
(Flag735-)
)
(and
(BOARDED-PASS_AI)
(not (NOT-BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim41Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_AG))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_AD))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_AE))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AJ-AK
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AK-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim67Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim68Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim69Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim71Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim100Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim101Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim102Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim104Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Flag132Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(Flag132-)
(Flag132prime-)
)

)
(:action Prim132Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Flag133Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(Flag133-)
(Flag133prime-)
)

)
(:action Prim133Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Flag134Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(Flag134-)
(Flag134prime-)
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Flag136Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(Flag136-)
(Flag136prime-)
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim165Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim166Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim167Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim169Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim198Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim199Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim200Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim202Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim231Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim232Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim233Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim235Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim264Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim265Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim266Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim268Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim297Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim298Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim299Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim301Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim330Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim331Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim332Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim334Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim363Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim364Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim365Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim367Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim396Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim397Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim398Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim400Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim429Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim430Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim431Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim433Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim462Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim463Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim464Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim466Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim495Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim496Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim497Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim499Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim528Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim529Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim530Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim532Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim561Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim562Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim563Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim565Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim594Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim595Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim596Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim598Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim627Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim628Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim629Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim631Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim660Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim661Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim662Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim664Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim693Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim694Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim695Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim697Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim726Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim727Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim728Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim730Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AP
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(when
(and
(BOARDED-PASS_AE)
)
(and
(SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
(not (NOT-SERVED-PASS_AE))
(not (BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(when
(and
(BOARDED-PASS_AD)
)
(and
(SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
(not (NOT-SERVED-PASS_AD))
(not (BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(when
(and
(BOARDED-PASS_AG)
)
(and
(SERVED-PASS_AG)
(NOT-BOARDED-PASS_AG)
(not (NOT-SERVED-PASS_AG))
(not (BOARDED-PASS_AG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
(Flag736prime-)
)
:effect
(and
(when
(and
(Flag736-)
)
(and
(BOARDED-PASS_AF)
(not (NOT-BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim41Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_AO))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_AF))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-29
:parameters ()
:precondition
(and
(not (SERVED-PASS_AQ))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AK-AL
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AL-AM
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AM-AN
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AN-AP
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AL-AK
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AM-AL
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AN-AM
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AP-AN
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim44Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim77Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Prim109Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim142Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim175Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim208Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim241Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim274Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim307Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim340Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim373Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim406Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim439Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim472Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim505Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim538Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim571Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim604Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim637Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim670Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim703Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AV
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AT
:parameters ()
:precondition
(and
(Flag127-)
(Flag127prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AU
:parameters ()
:precondition
(and
(Flag128-)
(Flag128prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AR
:parameters ()
:precondition
(and
(Flag129-)
(Flag129prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AS
:parameters ()
:precondition
(and
(Flag130-)
(Flag130prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_AQ)
)
(and
(SERVED-PASS_AQ)
(NOT-BOARDED-PASS_AQ)
(not (NOT-SERVED-PASS_AQ))
(not (BOARDED-PASS_AQ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
(Flag738prime-)
)
:effect
(and
(when
(and
(Flag738-)
)
(and
(BOARDED-PASS_AD)
(not (NOT-BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(when
(and
(BOARDED-PASS_AF)
)
(and
(SERVED-PASS_AF)
(NOT-BOARDED-PASS_AF)
(not (NOT-SERVED-PASS_AF))
(not (BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(when
(and
(BOARDED-PASS_AO)
)
(and
(SERVED-PASS_AO)
(NOT-BOARDED-PASS_AO)
(not (NOT-SERVED-PASS_AO))
(not (BOARDED-PASS_AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim41Action-26
:parameters ()
:precondition
(and
(not (SERVED-PASS_BE))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-28
:parameters ()
:precondition
(and
(not (SERVED-PASS_BA))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AP-AR
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AR-AP
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim53Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim62Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim63Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim64Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim65Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim86Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim95Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim96Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim97Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim98Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Flag127Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag127-)
(Flag127prime-)
)

)
(:action Prim127Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Flag128Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(Flag128-)
(Flag128prime-)
)

)
(:action Prim128Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Flag129Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(Flag129-)
(Flag129prime-)
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Flag130Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(Flag130-)
(Flag130prime-)
)

)
(:action Prim130Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim151Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim161Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim162Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim163Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim184Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim193Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim194Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim195Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim196Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim217Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim226Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim227Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim228Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim229Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim250Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim259Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim260Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim261Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim262Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim283Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim292Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim293Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim294Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim295Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim316Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim325Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim326Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim327Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim328Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim349Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim358Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim359Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim360Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim361Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim382Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim391Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim392Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim393Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim394Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim415Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim424Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim425Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim426Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim427Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim448Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim457Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim458Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim459Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim460Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim481Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim490Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim491Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim492Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim493Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim514Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim523Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim524Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim525Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim526Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim547Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim556Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim557Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim558Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim559Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim580Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim589Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim590Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim591Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim592Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim613Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim622Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim623Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim624Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim625Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim646Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim655Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim656Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim657Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim658Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim679Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim688Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim689Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim690Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim691Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim712Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim721Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim722Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim723Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim724Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-AX
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AY
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AW
:parameters ()
:precondition
(and
(Flag126-)
(Flag126prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(BOARDED-PASS_BA)
)
(and
(SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
(not (NOT-SERVED-PASS_BA))
(not (BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
(Flag743prime-)
)
:effect
(and
(when
(and
(Flag743-)
)
(and
(BOARDED-PASS_BB)
(not (NOT-BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(when
(and
(BOARDED-PASS_BE)
)
(and
(SERVED-PASS_BE)
(NOT-BOARDED-PASS_BE)
(not (NOT-SERVED-PASS_BE))
(not (BOARDED-PASS_BE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
(Flag740prime-)
)
:effect
(and
(when
(and
(Flag740-)
)
(and
(BOARDED-PASS_AC)
(not (NOT-BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
(Flag737prime-)
)
:effect
(and
(when
(and
(Flag737-)
)
(and
(BOARDED-PASS_AG)
(not (NOT-BOARDED-PASS_AG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-4
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-5
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-6
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-7
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-9
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-10
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-11
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-12
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-13
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_AI))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-20
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-21
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-22
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-23
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-24
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-25
:parameters ()
:precondition
(and
(not (SERVED-PASS_BF))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-30
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-31
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AR-AS
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AS-AT
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AT-AU
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AU-AV
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AS-AR
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AT-AS
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AU-AT
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AV-AU
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim43Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim52Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim61Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim76Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim85Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim94Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Flag126Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag126-)
(Flag126prime-)
)

)
(:action Prim126Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim141Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim150Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim159Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim174Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim183Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim192Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim207Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim216Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim225Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim240Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim249Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim258Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim273Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim282Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim291Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim306Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim315Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim324Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim339Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim348Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim357Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim372Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim381Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim390Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim405Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim414Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim423Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim438Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim447Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim456Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim471Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim480Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim489Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim504Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim513Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim522Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim537Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim546Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim555Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim570Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim579Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim588Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim603Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim612Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim621Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim636Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim645Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim654Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim669Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim678Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim687Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim702Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim711Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim720Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-BD
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AQ-AZ
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
(Flag742prime-)
)
:effect
(and
(when
(and
(Flag742-)
)
(and
(BOARDED-PASS_BE)
(not (NOT-BOARDED-PASS_BE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(BOARDED-PASS_BF)
)
(and
(SERVED-PASS_BF)
(NOT-BOARDED-PASS_BF)
(not (NOT-SERVED-PASS_BF))
(not (BOARDED-PASS_BF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
(Flag739prime-)
)
:effect
(and
(when
(and
(Flag739-)
)
(and
(BOARDED-PASS_AE)
(not (NOT-BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(when
(and
(BOARDED-PASS_AI)
)
(and
(SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
(not (NOT-SERVED-PASS_AI))
(not (BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag40Action-0
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Prim41Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AC))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action MOVEUP-AW-AX
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AX-AY
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AY-AZ
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AX-AW
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AY-AX
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-AZ-AY
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-BD-BC
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-BC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-BC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim51Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim54Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim84Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim87Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim149Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim152Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim182Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim185Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim215Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim218Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim248Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim251Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim281Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim284Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim314Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim317Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim347Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim350Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim380Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim383Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim413Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim416Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim446Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim449Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim479Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim482Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim512Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim515Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim545Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim548Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim578Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim581Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim611Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim614Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim644Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim647Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim677Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim680Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim710Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim713Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action STOP-PASS_AQ-BC
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
(Flag744prime-)
)
:effect
(and
(when
(and
(Flag744-)
)
(and
(BOARDED-PASS_BA)
(not (NOT-BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_BF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(when
(and
(BOARDED-PASS_AC)
)
(and
(SERVED-PASS_AC)
(NOT-BOARDED-PASS_AC)
(not (NOT-SERVED-PASS_AC))
(not (BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action STOP-PASS_AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action
:parameters ()
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag37Action
:parameters ()
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag39Action
:parameters ()
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim41Action-32
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-33
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-AZ-BC
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-BC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-BC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEUP-BC-BD
:parameters ()
:precondition
(and
(LIFT_AT-BC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BC)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BC))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action MOVEDOWN-BC-AZ
:parameters ()
:precondition
(and
(LIFT_AT-BC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BC)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BC))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag745prime-))
(not (Flag744prime-))
(not (Flag743prime-))
(not (Flag742prime-))
(not (Flag741prime-))
(not (Flag740prime-))
(not (Flag739prime-))
(not (Flag738prime-))
(not (Flag737prime-))
(not (Flag736prime-))
(not (Flag735prime-))
(not (Flag734prime-))
(not (Flag115prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag119prime-))
(not (Flag116prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag126prime-))
(not (Flag117prime-))
(not (Flag108prime-))
(not (Flag130prime-))
(not (Flag129prime-))
(not (Flag128prime-))
(not (Flag127prime-))
(not (Flag118prime-))
(not (Flag109prime-))
(not (Flag136prime-))
(not (Flag134prime-))
(not (Flag133prime-))
(not (Flag132prime-))
(not (Flag135prime-))
(not (Flag137prime-))
(not (Flag41prime-))
(not (Flag139prime-))
(not (Flag138prime-))
)
)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag126prime-)
(not (Flag126-))
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag127prime-)
(not (Flag127-))
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag128prime-)
(not (Flag128-))
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag129prime-)
(not (Flag129-))
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag130prime-)
(not (Flag130-))
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag132prime-)
(not (Flag132-))
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag133prime-)
(not (Flag133-))
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag134prime-)
(not (Flag134-))
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag135prime-)
(not (Flag135-))
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag136prime-)
(not (Flag136-))
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag137prime-)
(not (Flag137-))
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag138prime-)
(not (Flag138-))
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag139prime-)
(not (Flag139-))
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim533Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim533Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim551Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim551Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim564Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim564Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim568Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim568Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim570Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim570Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim571Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim571Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim572Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim572Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim573Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim573Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim574Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim574Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim575Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim576Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim576Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim577Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim577Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim577Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim578Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim578Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim579Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim579Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim580Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim580Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim581Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim581Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim582Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim582Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim583Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim583Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim584Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim584Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim585Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim585Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim586Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim586Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim587Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim588Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim588Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim589Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim589Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim590Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim591Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim591Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim592Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim592Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim593Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim593Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim594Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim594Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim595Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim595Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim596Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim596Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim597Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim597Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim598Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim598Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim599Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim599Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim600Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim600Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim601Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim601Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim603Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim603Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim604Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim604Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim605Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim606Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim606Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim607Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim607Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim608Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim608Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim609Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim609Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim610Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim610Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim610Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim611Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim611Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim612Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim612Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim613Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim613Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim614Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim614Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim615Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim615Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim616Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim616Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim617Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim617Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim618Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim618Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim619Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim619Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim620Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim620Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim621Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim621Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim622Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim623Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim623Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim624Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim624Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim625Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim625Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim626Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim626Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim627Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim627Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim628Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim628Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim629Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim629Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim630Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim630Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim631Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim631Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim632Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim632Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim633Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim633Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim634Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim634Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim636Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim636Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim637Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim637Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim638Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim638Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim639Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim639Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim640Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim640Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim641Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim641Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim642Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim642Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim643Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim643Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim643Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim644Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim644Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim645Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim645Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim646Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim646Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim647Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim647Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim648Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim648Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim649Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim649Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim650Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim650Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim651Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim651Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim652Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim652Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim653Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim653Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim654Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim654Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim655Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim655Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim656Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim656Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim657Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim657Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim658Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim658Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim659Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim659Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim660Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim660Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim661Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim661Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim662Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim662Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim663Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim663Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim664Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim664Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim665Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim665Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim666Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim666Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim667Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim667Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim669Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim669Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim670Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim670Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim671Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim671Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim672Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim672Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim673Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim673Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim674Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim674Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim675Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim675Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim676Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim676Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim676Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim677Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim677Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim678Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim678Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim679Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim679Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim680Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim680Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim681Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim681Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim682Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim682Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim683Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim683Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim684Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim684Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim685Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim685Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim686Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim686Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim687Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim687Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim688Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim688Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim689Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim689Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim690Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim690Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim691Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim691Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim692Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim692Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim693Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim693Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim694Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim694Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim695Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim695Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim696Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim696Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim697Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim697Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim698Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim698Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim699Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim699Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim700Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim700Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim702Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim702Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim703Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim703Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim704Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim704Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim705Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim705Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim706Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim706Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim707Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim707Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim708Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim708Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim709Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim709Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim709Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim710Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim710Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim711Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim711Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim712Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim712Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim713Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim713Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim714Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim714Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim715Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim715Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim716Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim716Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim717Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim717Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim718Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim718Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim719Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim719Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim720Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim720Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim721Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim721Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim722Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim722Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim723Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim723Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim724Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim724Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim725Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim725Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim726Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim726Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim727Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim727Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim728Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim728Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim729Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim729Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim730Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim730Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim731Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim731Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim732Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim732Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim733Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim733Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag734Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AO)
(NOT-BOARDED-PASS_AO)
)
:effect
(and
(Flag734-)
(Flag734prime-)
)

)
(:action Prim734Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AO))
)
:effect
(and
(Flag734prime-)
(not (Flag734-))
)

)
(:action Prim734Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AO))
)
:effect
(and
(Flag734prime-)
(not (Flag734-))
)

)
(:action Flag735Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
)
:effect
(and
(Flag735-)
(Flag735prime-)
)

)
(:action Prim735Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AI))
)
:effect
(and
(Flag735prime-)
(not (Flag735-))
)

)
(:action Prim735Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AI))
)
:effect
(and
(Flag735prime-)
(not (Flag735-))
)

)
(:action Flag736Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AF)
(NOT-BOARDED-PASS_AF)
)
:effect
(and
(Flag736-)
(Flag736prime-)
)

)
(:action Prim736Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AF))
)
:effect
(and
(Flag736prime-)
(not (Flag736-))
)

)
(:action Prim736Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AF))
)
:effect
(and
(Flag736prime-)
(not (Flag736-))
)

)
(:action Flag737Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AG)
(NOT-BOARDED-PASS_AG)
)
:effect
(and
(Flag737-)
(Flag737prime-)
)

)
(:action Prim737Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AG))
)
:effect
(and
(Flag737prime-)
(not (Flag737-))
)

)
(:action Prim737Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AG))
)
:effect
(and
(Flag737prime-)
(not (Flag737-))
)

)
(:action Flag738Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
)
:effect
(and
(Flag738-)
(Flag738prime-)
)

)
(:action Prim738Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AD))
)
:effect
(and
(Flag738prime-)
(not (Flag738-))
)

)
(:action Prim738Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AD))
)
:effect
(and
(Flag738prime-)
(not (Flag738-))
)

)
(:action Flag739Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
)
:effect
(and
(Flag739-)
(Flag739prime-)
)

)
(:action Prim739Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AE))
)
:effect
(and
(Flag739prime-)
(not (Flag739-))
)

)
(:action Prim739Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AE))
)
:effect
(and
(Flag739prime-)
(not (Flag739-))
)

)
(:action Flag740Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AC)
(NOT-BOARDED-PASS_AC)
)
:effect
(and
(Flag740-)
(Flag740prime-)
)

)
(:action Prim740Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AC))
)
:effect
(and
(Flag740prime-)
(not (Flag740-))
)

)
(:action Prim740Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AC))
)
:effect
(and
(Flag740prime-)
(not (Flag740-))
)

)
(:action Flag741Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BF)
(NOT-BOARDED-PASS_BF)
)
:effect
(and
(Flag741-)
(Flag741prime-)
)

)
(:action Prim741Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BF))
)
:effect
(and
(Flag741prime-)
(not (Flag741-))
)

)
(:action Prim741Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BF))
)
:effect
(and
(Flag741prime-)
(not (Flag741-))
)

)
(:action Flag742Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BE)
(NOT-BOARDED-PASS_BE)
)
:effect
(and
(Flag742-)
(Flag742prime-)
)

)
(:action Prim742Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BE))
)
:effect
(and
(Flag742prime-)
(not (Flag742-))
)

)
(:action Prim742Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BE))
)
:effect
(and
(Flag742prime-)
(not (Flag742-))
)

)
(:action Flag743Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
)
:effect
(and
(Flag743-)
(Flag743prime-)
)

)
(:action Prim743Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BB))
)
:effect
(and
(Flag743prime-)
(not (Flag743-))
)

)
(:action Prim743Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BB))
)
:effect
(and
(Flag743prime-)
(not (Flag743-))
)

)
(:action Flag744Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
)
:effect
(and
(Flag744-)
(Flag744prime-)
)

)
(:action Prim744Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BA))
)
:effect
(and
(Flag744prime-)
(not (Flag744-))
)

)
(:action Prim744Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BA))
)
:effect
(and
(Flag744prime-)
(not (Flag744-))
)

)
(:action Flag745Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AQ)
(NOT-BOARDED-PASS_AQ)
)
:effect
(and
(Flag745-)
(Flag745prime-)
)

)
(:action Prim745Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AQ))
)
:effect
(and
(Flag745prime-)
(not (Flag745-))
)

)
(:action Prim745Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AQ))
)
:effect
(and
(Flag745prime-)
(not (Flag745-))
)

)
)
