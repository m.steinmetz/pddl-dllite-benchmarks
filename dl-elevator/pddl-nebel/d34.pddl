(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag825-)
(Flag824-)
(Flag823-)
(Flag822-)
(Flag821-)
(Flag820-)
(Flag819-)
(Flag818-)
(Flag817-)
(Flag816-)
(Flag815-)
(Flag814-)
(Flag813-)
(Flag62-)
(NOT-LIFT_AT-AY)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(LIFT_AT-AZ)
(LIFT_AT-BB)
(ERROR-)
(Flag41-)
(Flag39-)
(Flag37-)
(Flag35-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AS)
(Flag61-)
(Flag56-)
(Flag50-)
(LIFT_AT-AW)
(LIFT_AT-BD)
(LIFT_AT-BE)
(Flag42-)
(Flag40-)
(Flag38-)
(Flag36-)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_AP)
(SERVED-PASS_AO)
(SERVED-PASS_AI)
(BOARDED-PASS_AE)
(BOARDED-PASS_AB)
(BOARDED-PASS_AU)
(BOARDED-PASS_AN)
(Flag64-)
(Flag54-)
(Flag53-)
(LIFT_AT-AV)
(LIFT_AT-AT)
(LIFT_AT-AR)
(LIFT_AT-BF)
(LIFT_AT-BG)
(BOARDED-PASS_AI)
(SERVED-PASS_AA)
(BOARDED-PASS_BC)
(SERVED-PASS_BC)
(SERVED-PASS_BA)
(SERVED-PASS_AN)
(Flag67-)
(Flag66-)
(Flag65-)
(Flag55-)
(Flag52-)
(LIFT_AT-AQ)
(LIFT_AT-AL)
(LIFT_AT-BH)
(SERVED-PASS_AP)
(SERVED-PASS_AB)
(SERVED-PASS_AC)
(BOARDED-PASS_AC)
(BOARDED-PASS_AM)
(Flag69-)
(Flag49-)
(Flag48-)
(LIFT_AT-AK)
(Flag73-)
(LIFT_AT-AJ)
(Flag72-)
(LIFT_AT-AH)
(BOARDED-PASS_BA)
(SERVED-PASS_AM)
(Flag74-)
(LIFT_AT-AG)
(LIFT_AT-AF)
(BOARDED-PASS_AS)
(BOARDED-PASS_AO)
(BOARDED-PASS_AA)
(SERVED-PASS_AU)
(Flag76-)
(Flag75-)
(LIFT_AT-AD)
(SERVED-PASS_AE)
(Flag77-)
(Flag43-)
(NOT-CHECKCONSISTENCY-)
(NOT-SERVED-PASS_AE)
(NOT-LIFT_AT-AF)
(NOT-LIFT_AT-AD)
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AA)
(NOT-BOARDED-PASS_AO)
(NOT-BOARDED-PASS_AS)
(NOT-LIFT_AT-AG)
(NOT-LIFT_AT-AH)
(NOT-SERVED-PASS_AM)
(NOT-BOARDED-PASS_BA)
(NOT-LIFT_AT-AJ)
(NOT-LIFT_AT-AK)
(NOT-LIFT_AT-AL)
(NOT-LIFT_AT-BG)
(NOT-BOARDED-PASS_AM)
(NOT-BOARDED-PASS_AC)
(NOT-SERVED-PASS_AC)
(NOT-SERVED-PASS_AB)
(NOT-SERVED-PASS_AP)
(NOT-LIFT_AT-AQ)
(NOT-LIFT_AT-BH)
(NOT-LIFT_AT-AR)
(NOT-LIFT_AT-BF)
(NOT-LIFT_AT-BE)
(NOT-SERVED-PASS_AN)
(NOT-SERVED-PASS_BA)
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AI)
(NOT-LIFT_AT-AT)
(NOT-LIFT_AT-AV)
(NOT-LIFT_AT-AW)
(NOT-LIFT_AT-BD)
(NOT-LIFT_AT-BB)
(NOT-BOARDED-PASS_AN)
(NOT-BOARDED-PASS_AU)
(NOT-BOARDED-PASS_AB)
(NOT-BOARDED-PASS_AE)
(NOT-SERVED-PASS_AI)
(NOT-SERVED-PASS_AO)
(NOT-BOARDED-PASS_AP)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AZ)
(NOT-SERVED-PASS_AS)
(NOT-ERROR-)
(LIFT_AT-AY)
(Flag825prime-)
(Flag824prime-)
(Flag823prime-)
(Flag822prime-)
(Flag821prime-)
(Flag820prime-)
(Flag819prime-)
(Flag818prime-)
(Flag817prime-)
(Flag816prime-)
(Flag815prime-)
(Flag814prime-)
(Flag813prime-)
(Flag62prime-)
(Flag41prime-)
(Flag39prime-)
(Flag37prime-)
(Flag35prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag61prime-)
(Flag56prime-)
(Flag50prime-)
(Flag42prime-)
(Flag40prime-)
(Flag38prime-)
(Flag36prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag64prime-)
(Flag54prime-)
(Flag53prime-)
(Flag67prime-)
(Flag66prime-)
(Flag65prime-)
(Flag55prime-)
(Flag52prime-)
(Flag69prime-)
(Flag49prime-)
(Flag48prime-)
(Flag73prime-)
(Flag72prime-)
(Flag74prime-)
(Flag76prime-)
(Flag75prime-)
(Flag77prime-)
(Flag43prime-)
)
(:action STOP-PASS_AM-AD
:parameters ()
:precondition
(and
(Flag77-)
(Flag77prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag43Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(SERVED-PASS_AS)
(SERVED-PASS_AP)
(Flag14-)
(Flag14prime-)
(SERVED-PASS_AO)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(SERVED-PASS_AI)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(SERVED-PASS_AE)
(SERVED-PASS_AB)
(SERVED-PASS_AC)
(SERVED-PASS_AA)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(Flag32-)
(Flag32prime-)
(Flag34-)
(Flag34prime-)
(Flag36-)
(Flag36prime-)
(SERVED-PASS_AU)
(Flag38-)
(Flag38prime-)
(Flag40-)
(Flag40prime-)
(Flag42-)
(Flag42prime-)
(SERVED-PASS_BC)
(SERVED-PASS_BA)
(SERVED-PASS_AN)
(SERVED-PASS_AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Prim43Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_AE))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Flag77Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(Flag77-)
(Flag77prime-)
)

)
(:action Prim77Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim112Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim147Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim182Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim217Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim252Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim287Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim322Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim357Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim392Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim427Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim462Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim497Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim532Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim567Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim602Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim637Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim672Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim707Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim742Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim777Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim812Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AF
:parameters ()
:precondition
(and
(Flag75-)
(Flag75prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AG
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(when
(and
(BOARDED-PASS_AE)
)
(and
(SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
(not (NOT-SERVED-PASS_AE))
(not (BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim43Action-26
:parameters ()
:precondition
(and
(not (SERVED-PASS_AU))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-AD-AF
:parameters ()
:precondition
(and
(LIFT_AT-AD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AD)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AD))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AF-AD
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag75Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(Flag75-)
(Flag75prime-)
)

)
(:action Prim75Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim110Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim111Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim145Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim146Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim180Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim181Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim215Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim216Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim250Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim251Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim285Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim286Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim320Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim321Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim355Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim356Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim390Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim391Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim425Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim426Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim460Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim461Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim495Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim496Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim530Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim531Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim565Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim566Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim600Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim601Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim635Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim636Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim670Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim671Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim705Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim706Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim740Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim741Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim775Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim776Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim810Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim811Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AH
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(when
(and
(BOARDED-PASS_AU)
)
(and
(SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
(not (NOT-SERVED-PASS_AU))
(not (BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag820prime-)
)
:effect
(and
(when
(and
(Flag820-)
)
(and
(BOARDED-PASS_AA)
(not (NOT-BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag815prime-)
)
:effect
(and
(when
(and
(Flag815-)
)
(and
(BOARDED-PASS_AO)
(not (NOT-BOARDED-PASS_AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
(Flag813prime-)
)
:effect
(and
(when
(and
(Flag813-)
)
(and
(BOARDED-PASS_AS)
(not (NOT-BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim43Action-33
:parameters ()
:precondition
(and
(not (SERVED-PASS_AM))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-AF-AG
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AG-AH
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AG-AF
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AH-AG
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag74Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim109Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim144Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim179Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim214Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim249Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim284Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim319Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim354Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim389Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim424Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim459Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim494Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim529Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim564Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim599Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim634Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim669Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim704Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim739Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim774Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim809Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AJ
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_AM)
)
(and
(SERVED-PASS_AM)
(NOT-BOARDED-PASS_AM)
(not (NOT-SERVED-PASS_AM))
(not (BOARDED-PASS_AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
(Flag823prime-)
)
:effect
(and
(when
(and
(Flag823-)
)
(and
(BOARDED-PASS_BA)
(not (NOT-BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AH-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AJ-AH
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag72Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Prim72Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim107Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim142Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim177Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim212Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim247Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim282Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim317Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim352Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim387Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim422Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim457Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim492Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim527Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim562Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim597Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim632Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim667Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim702Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim737Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim772Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim807Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AK
:parameters ()
:precondition
(and
(Flag73-)
(Flag73prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AJ-AK
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AK-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag73Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(Flag73-)
(Flag73prime-)
)

)
(:action Prim73Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim108Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim143Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim178Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim213Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim248Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim283Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim318Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim353Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim388Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim423Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim458Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim493Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim528Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim563Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim598Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim633Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim668Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim703Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim738Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim773Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim808Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AL
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-BH
:parameters ()
:precondition
(and
(Flag49-)
(Flag49prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AQ
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim43Action-7
:parameters ()
:precondition
(and
(not (SERVED-PASS_AP))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_AB))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AC))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-AK-AL
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AL-AK
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BH-BG
:parameters ()
:precondition
(and
(LIFT_AT-BH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BH)
(LIFT_AT-BG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BH))
(not (NOT-LIFT_AT-BG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag48Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Prim48Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Flag49Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(Flag49-)
(Flag49prime-)
)

)
(:action Prim49Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim83Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim84Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim104Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim118Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim119Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim139Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim153Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim154Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim174Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim188Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim189Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim209Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim223Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim224Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim244Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim258Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim259Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim279Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim293Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim294Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim314Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim328Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim329Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim349Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim363Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim364Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim384Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim398Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim399Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim419Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim433Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim434Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim454Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim468Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim469Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim489Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim503Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim504Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim524Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim538Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim539Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim559Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim573Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim574Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim594Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim608Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim609Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim629Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim643Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim644Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim664Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim678Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim679Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim699Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim713Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim714Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim734Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim748Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim749Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim769Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim783Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim784Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BH))
)
:effect
(and
)

)
(:action Prim804Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action STOP-PASS_AM-BF
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AV
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AT
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-BG
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AR
:parameters ()
:precondition
(and
(Flag67-)
(Flag67prime-)
(Flag825prime-)
)
:effect
(and
(when
(and
(Flag825-)
)
(and
(BOARDED-PASS_AM)
(not (NOT-BOARDED-PASS_AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
(Flag819prime-)
)
:effect
(and
(when
(and
(Flag819-)
)
(and
(BOARDED-PASS_AC)
(not (NOT-BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AC)
)
(and
(SERVED-PASS_AC)
(NOT-BOARDED-PASS_AC)
(not (NOT-SERVED-PASS_AC))
(not (BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(when
(and
(BOARDED-PASS_AB)
)
(and
(SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
(not (NOT-SERVED-PASS_AB))
(not (BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AP)
)
(and
(SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
(not (NOT-SERVED-PASS_AP))
(not (BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim43Action-20
:parameters ()
:precondition
(and
(not (SERVED-PASS_AA))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-30
:parameters ()
:precondition
(and
(not (SERVED-PASS_BC))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-31
:parameters ()
:precondition
(and
(not (SERVED-PASS_BA))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-32
:parameters ()
:precondition
(and
(not (SERVED-PASS_AN))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-AL-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-BG-BH
:parameters ()
:precondition
(and
(LIFT_AT-BG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BG)
(LIFT_AT-BH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BG))
(not (NOT-LIFT_AT-BH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AQ-AR
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AQ-AL
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BG-BF
:parameters ()
:precondition
(and
(LIFT_AT-BG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BG)
(LIFT_AT-BF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BG))
(not (NOT-LIFT_AT-BF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AR-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BF-BE
:parameters ()
:precondition
(and
(LIFT_AT-BF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BF)
(LIFT_AT-BE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BF))
(not (NOT-LIFT_AT-BE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag52Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim52Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Flag67Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(Flag67-)
(Flag67prime-)
)

)
(:action Prim67Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim87Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim90Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim100Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim101Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim102Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim122Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim125Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim135Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim136Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim137Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim157Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim170Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim171Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim172Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim192Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim195Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim205Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim206Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim207Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim227Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim230Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim240Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim241Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim242Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim262Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim265Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim275Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim276Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim277Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim297Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim300Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim310Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim311Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim312Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim332Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim335Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim345Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim346Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim347Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim367Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim370Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim380Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim381Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim382Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim402Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim405Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim415Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim416Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim417Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim437Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim440Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim450Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim451Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim452Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim472Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim475Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim485Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim486Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim487Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim507Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim510Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim520Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim521Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim522Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim542Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim545Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim555Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim556Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim557Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim577Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim580Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim590Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim591Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim592Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim612Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim615Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim625Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim626Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim627Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim647Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim650Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim660Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim661Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim662Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim682Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim685Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim695Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim696Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim697Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim717Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim720Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim730Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim731Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim732Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim752Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim755Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim765Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim766Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim767Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim787Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BF))
)
:effect
(and
)

)
(:action Prim790Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim800Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim801Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BG))
)
:effect
(and
)

)
(:action Prim802Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action STOP-PASS_AM-BE
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-BD
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AW
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(when
(and
(BOARDED-PASS_AN)
)
(and
(SERVED-PASS_AN)
(NOT-BOARDED-PASS_AN)
(not (NOT-SERVED-PASS_AN))
(not (BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(when
(and
(BOARDED-PASS_BA)
)
(and
(SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
(not (NOT-SERVED-PASS_BA))
(not (BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(when
(and
(BOARDED-PASS_BC)
)
(and
(SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
(not (NOT-SERVED-PASS_BC))
(not (BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
(Flag822prime-)
)
:effect
(and
(when
(and
(Flag822-)
)
(and
(BOARDED-PASS_BC)
(not (NOT-BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(when
(and
(BOARDED-PASS_AA)
)
(and
(SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
(not (NOT-SERVED-PASS_AA))
(not (BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
(Flag816prime-)
)
:effect
(and
(when
(and
(Flag816-)
)
(and
(BOARDED-PASS_AI)
(not (NOT-BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-4
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-5
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-8
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-9
:parameters ()
:precondition
(and
(not (SERVED-PASS_AO))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-10
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-11
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-12
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-13
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_AI))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-15
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-16
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-21
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-22
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-23
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-24
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-25
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-27
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-28
:parameters ()
:precondition
(and
(not (Flag40-))
(Flag40prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-29
:parameters ()
:precondition
(and
(not (Flag42-))
(Flag42prime-)
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-AR-AT
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AT-AV
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-BF-BG
:parameters ()
:precondition
(and
(LIFT_AT-BF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BF)
(LIFT_AT-BG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BF))
(not (NOT-LIFT_AT-BG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-BE-BF
:parameters ()
:precondition
(and
(LIFT_AT-BE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BE)
(LIFT_AT-BF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BE))
(not (NOT-LIFT_AT-BF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AT-AR
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AV-AT
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BE-BD
:parameters ()
:precondition
(and
(LIFT_AT-BE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BE)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BE))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BD-BB
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-BB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-BB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag53Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim88Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim89Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim99Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim123Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim124Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim134Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim158Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim159Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim169Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim193Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim194Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim204Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim228Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim229Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim239Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim263Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim264Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim274Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim298Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim299Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim309Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim333Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim334Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim344Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim368Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim369Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim379Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim403Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim404Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim414Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim438Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim439Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim449Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim473Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim474Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim484Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim508Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim509Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim519Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim543Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim544Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim554Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim578Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim579Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim589Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim613Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim614Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim624Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim648Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim649Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim659Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim683Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim684Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim694Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim718Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim719Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim729Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim753Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim754Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim764Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim788Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim789Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim799Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action STOP-PASS_AM-BB
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AZ
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AM-AX
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
(Flag824prime-)
)
:effect
(and
(when
(and
(Flag824-)
)
(and
(BOARDED-PASS_AN)
(not (NOT-BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
(Flag821prime-)
)
:effect
(and
(when
(and
(Flag821-)
)
(and
(BOARDED-PASS_AU)
(not (NOT-BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
(Flag818prime-)
)
:effect
(and
(when
(and
(Flag818-)
)
(and
(BOARDED-PASS_AB)
(not (NOT-BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
(Flag817prime-)
)
:effect
(and
(when
(and
(Flag817-)
)
(and
(BOARDED-PASS_AE)
(not (NOT-BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(when
(and
(BOARDED-PASS_AI)
)
(and
(SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
(not (NOT-SERVED-PASS_AI))
(not (BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(when
(and
(BOARDED-PASS_AO)
)
(and
(SERVED-PASS_AO)
(NOT-BOARDED-PASS_AO)
(not (NOT-SERVED-PASS_AO))
(not (BOARDED-PASS_AO))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
(Flag814prime-)
)
:effect
(and
(when
(and
(Flag814-)
)
(and
(BOARDED-PASS_AP)
(not (NOT-BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag38Action-1
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Flag40Action-1
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(Flag40-)
(Flag40prime-)
)

)
(:action Flag42Action-1
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim43Action-6
:parameters ()
:precondition
(and
(not (SERVED-PASS_AS))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action MOVEUP-BD-BE
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-BE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-BE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AW-AX
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-BB-BD
:parameters ()
:precondition
(and
(LIFT_AT-BB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BB)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BB))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-BB-AZ
:parameters ()
:precondition
(and
(LIFT_AT-BB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BB)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BB))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AZ-AY
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AX-AW
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag50Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Prim50Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim85Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim91Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim96Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim120Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim126Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim131Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim155Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim161Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim166Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim190Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim196Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim201Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim225Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim231Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim236Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim260Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim266Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim271Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim295Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim301Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim306Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim330Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim336Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim341Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim365Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim371Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim376Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim400Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim406Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim411Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim435Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim441Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim446Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim470Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim476Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim481Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim505Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim511Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim516Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim540Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim546Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim551Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim575Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim581Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim586Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim610Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim616Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim621Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim645Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim651Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim656Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim680Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim686Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim691Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim715Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim721Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim726Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim750Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim756Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim761Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim785Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim791Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim796Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action STOP-PASS_AM-AY
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AN-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_BC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AA-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AC-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AE-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AO-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action STOP-PASS_AS-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(when
(and
(BOARDED-PASS_AS)
)
(and
(SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(not (NOT-SERVED-PASS_AS))
(not (BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action
:parameters ()
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag37Action
:parameters ()
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Flag39Action
:parameters ()
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Flag41Action
:parameters ()
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim43Action-34
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-35
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AZ-BB
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-BB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-BB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AX-AY
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEUP-AY-AZ
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action MOVEDOWN-AY-AX
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag825prime-))
(not (Flag824prime-))
(not (Flag823prime-))
(not (Flag822prime-))
(not (Flag821prime-))
(not (Flag820prime-))
(not (Flag819prime-))
(not (Flag818prime-))
(not (Flag817prime-))
(not (Flag816prime-))
(not (Flag815prime-))
(not (Flag814prime-))
(not (Flag813prime-))
(not (Flag62prime-))
(not (Flag41prime-))
(not (Flag39prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag61prime-))
(not (Flag56prime-))
(not (Flag50prime-))
(not (Flag42prime-))
(not (Flag40prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag64prime-))
(not (Flag54prime-))
(not (Flag53prime-))
(not (Flag67prime-))
(not (Flag66prime-))
(not (Flag65prime-))
(not (Flag55prime-))
(not (Flag52prime-))
(not (Flag69prime-))
(not (Flag49prime-))
(not (Flag48prime-))
(not (Flag73prime-))
(not (Flag72prime-))
(not (Flag74prime-))
(not (Flag76prime-))
(not (Flag75prime-))
(not (Flag77prime-))
(not (Flag43prime-))
)
)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag49prime-)
(not (Flag49-))
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag67prime-)
(not (Flag67-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag73prime-)
(not (Flag73-))
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag75prime-)
(not (Flag75-))
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag77prime-)
(not (Flag77-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim551Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim551Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim564Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim564Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim569Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim569Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim570Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim570Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim571Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim571Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim572Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim572Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim573Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim573Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim574Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim574Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim575Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim576Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim576Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim577Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim577Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim578Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim578Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim579Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim579Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim580Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim580Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim581Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim581Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim582Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim582Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim583Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim583Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim584Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim584Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim585Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim585Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim586Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim586Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim587Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim588Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim588Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim589Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim589Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim590Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim591Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim591Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim592Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim592Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim593Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim593Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim594Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim594Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim595Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim595Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim596Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim596Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim597Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim597Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim598Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim598Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim599Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim599Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim600Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim600Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim601Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim601Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim602Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim602Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim604Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim604Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim605Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim606Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim606Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim607Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim607Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim608Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim608Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim609Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim609Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim610Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim610Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim611Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim611Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim612Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim612Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim613Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim613Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim614Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim614Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim615Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim615Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim616Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim616Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim617Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim617Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim618Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim618Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim619Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim619Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim620Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim620Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim621Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim621Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim622Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim623Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim623Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim624Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim624Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim625Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim625Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim626Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim626Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim627Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim627Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim628Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim628Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim629Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim629Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim630Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim630Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim631Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim631Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim632Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim632Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim633Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim633Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim634Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim634Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim635Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim635Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim636Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim636Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim637Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim637Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim639Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim639Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim640Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim640Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim641Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim641Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim642Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim642Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim643Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim643Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim644Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim644Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim645Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim645Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim646Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim646Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim647Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim647Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim648Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim648Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim649Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim649Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim650Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim650Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim651Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim651Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim652Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim652Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim653Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim653Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim654Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim654Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim655Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim655Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim656Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim656Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim657Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim657Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim657Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim658Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim658Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim659Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim659Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim660Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim660Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim661Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim661Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim662Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim662Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim663Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim663Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim664Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim664Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim665Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim665Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim666Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim666Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim667Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim667Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim668Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim668Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim669Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim669Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim670Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim670Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim671Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim671Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim672Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim672Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim674Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim674Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim675Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim675Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim676Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim676Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim677Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim677Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim678Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim678Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim679Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim679Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim680Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim680Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim681Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim681Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim682Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim682Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim683Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim683Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim684Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim684Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim685Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim685Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim686Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim686Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim687Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim687Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim688Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim688Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim689Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim689Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim690Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim690Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim691Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim691Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim692Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim692Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim692Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim693Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim693Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim694Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim694Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim695Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim695Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim696Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim696Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim697Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim697Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim698Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim698Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim699Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim699Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim700Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim700Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim701Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim701Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim702Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim702Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim703Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim703Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim704Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim704Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim705Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim705Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim706Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim706Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim707Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim707Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim709Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim709Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim710Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim710Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim711Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim711Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim712Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim712Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim713Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim713Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim714Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim714Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim715Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim715Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim716Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim716Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim717Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim717Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim718Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim718Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim719Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim719Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim720Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim720Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim721Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim721Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim722Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim722Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim723Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim723Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim724Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim724Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim725Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim725Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim726Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim726Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim727Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim727Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim727Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim728Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim728Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim729Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim729Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim730Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim730Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim731Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim731Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim732Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim732Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim733Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim733Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim734Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim734Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim735Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim735Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim736Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim736Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim737Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim737Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim738Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim738Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim739Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim739Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim740Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim740Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim741Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim741Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim742Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim742Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim744Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim744Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim745Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim745Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim746Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim746Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim747Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim747Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim748Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim748Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim749Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim749Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim750Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim750Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim751Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim751Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim752Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim752Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim753Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim753Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim754Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim754Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim755Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim755Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim756Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim756Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim757Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim757Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim758Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim758Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim759Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim759Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim760Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim760Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim761Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim761Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim762Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim762Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim762Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim763Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim763Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim764Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim764Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim765Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim765Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim766Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim766Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim767Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim767Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim768Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim768Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim769Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim769Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim770Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim770Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim771Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim771Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim772Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim772Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim773Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim773Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim774Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim774Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim775Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim775Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim776Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim776Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim777Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim777Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim779Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim779Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim780Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim780Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim781Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim781Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim782Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim782Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim783Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim783Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim784Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim784Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim785Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim785Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim786Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim786Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim787Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim787Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim788Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim788Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim789Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim789Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim790Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim790Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim791Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim791Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim792Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim792Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim793Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim793Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim794Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim794Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim795Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim795Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim796Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim796Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim797Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim797Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim797Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim798Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim798Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim799Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim799Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim800Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim800Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim801Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim801Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim802Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim802Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim803Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim803Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim804Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim804Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim805Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim805Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim806Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim806Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim807Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim807Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim808Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim808Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim809Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim809Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim810Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim810Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim811Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim811Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim812Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim812Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag813Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
)
:effect
(and
(Flag813-)
(Flag813prime-)
)

)
(:action Prim813Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AS))
)
:effect
(and
(Flag813prime-)
(not (Flag813-))
)

)
(:action Prim813Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AS))
)
:effect
(and
(Flag813prime-)
(not (Flag813-))
)

)
(:action Flag814Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
)
:effect
(and
(Flag814-)
(Flag814prime-)
)

)
(:action Prim814Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AP))
)
:effect
(and
(Flag814prime-)
(not (Flag814-))
)

)
(:action Prim814Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AP))
)
:effect
(and
(Flag814prime-)
(not (Flag814-))
)

)
(:action Flag815Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AO)
(NOT-BOARDED-PASS_AO)
)
:effect
(and
(Flag815-)
(Flag815prime-)
)

)
(:action Prim815Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AO))
)
:effect
(and
(Flag815prime-)
(not (Flag815-))
)

)
(:action Prim815Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AO))
)
:effect
(and
(Flag815prime-)
(not (Flag815-))
)

)
(:action Flag816Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
)
:effect
(and
(Flag816-)
(Flag816prime-)
)

)
(:action Prim816Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AI))
)
:effect
(and
(Flag816prime-)
(not (Flag816-))
)

)
(:action Prim816Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AI))
)
:effect
(and
(Flag816prime-)
(not (Flag816-))
)

)
(:action Flag817Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
)
:effect
(and
(Flag817-)
(Flag817prime-)
)

)
(:action Prim817Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AE))
)
:effect
(and
(Flag817prime-)
(not (Flag817-))
)

)
(:action Prim817Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AE))
)
:effect
(and
(Flag817prime-)
(not (Flag817-))
)

)
(:action Flag818Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
)
:effect
(and
(Flag818-)
(Flag818prime-)
)

)
(:action Prim818Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AB))
)
:effect
(and
(Flag818prime-)
(not (Flag818-))
)

)
(:action Prim818Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AB))
)
:effect
(and
(Flag818prime-)
(not (Flag818-))
)

)
(:action Flag819Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AC)
(NOT-BOARDED-PASS_AC)
)
:effect
(and
(Flag819-)
(Flag819prime-)
)

)
(:action Prim819Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AC))
)
:effect
(and
(Flag819prime-)
(not (Flag819-))
)

)
(:action Prim819Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AC))
)
:effect
(and
(Flag819prime-)
(not (Flag819-))
)

)
(:action Flag820Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
)
:effect
(and
(Flag820-)
(Flag820prime-)
)

)
(:action Prim820Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AA))
)
:effect
(and
(Flag820prime-)
(not (Flag820-))
)

)
(:action Prim820Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AA))
)
:effect
(and
(Flag820prime-)
(not (Flag820-))
)

)
(:action Flag821Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
)
:effect
(and
(Flag821-)
(Flag821prime-)
)

)
(:action Prim821Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AU))
)
:effect
(and
(Flag821prime-)
(not (Flag821-))
)

)
(:action Prim821Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AU))
)
:effect
(and
(Flag821prime-)
(not (Flag821-))
)

)
(:action Flag822Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
)
:effect
(and
(Flag822-)
(Flag822prime-)
)

)
(:action Prim822Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BC))
)
:effect
(and
(Flag822prime-)
(not (Flag822-))
)

)
(:action Prim822Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BC))
)
:effect
(and
(Flag822prime-)
(not (Flag822-))
)

)
(:action Flag823Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
)
:effect
(and
(Flag823-)
(Flag823prime-)
)

)
(:action Prim823Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BA))
)
:effect
(and
(Flag823prime-)
(not (Flag823-))
)

)
(:action Prim823Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BA))
)
:effect
(and
(Flag823prime-)
(not (Flag823-))
)

)
(:action Flag824Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AN)
(NOT-BOARDED-PASS_AN)
)
:effect
(and
(Flag824-)
(Flag824prime-)
)

)
(:action Prim824Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AN))
)
:effect
(and
(Flag824prime-)
(not (Flag824-))
)

)
(:action Prim824Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AN))
)
:effect
(and
(Flag824prime-)
(not (Flag824-))
)

)
(:action Flag825Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AM)
(NOT-BOARDED-PASS_AM)
)
:effect
(and
(Flag825-)
(Flag825prime-)
)

)
(:action Prim825Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AM))
)
:effect
(and
(Flag825prime-)
(not (Flag825-))
)

)
(:action Prim825Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AM))
)
:effect
(and
(Flag825prime-)
(not (Flag825-))
)

)
)
