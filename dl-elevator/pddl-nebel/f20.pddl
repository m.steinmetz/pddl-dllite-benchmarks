(define (problem grounded-SIMPLE-ADL-ELEVATORS_PROBLEM)
(:domain grounded-SIMPLE-ADL-ELEVATORS)
(:init
(NOT-LIFT_AT-A)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-P)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-D)
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(NOT-BOARDED-PASS_Q)
(NOT-BOARDED-PASS_R)
(NOT-BOARDED-PASS_S)
(NOT-BOARDED-PASS_C)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_M)
(NOT-BOARDED-PASS_N)
(NOT-BOARDED-PASS_H)
(NOT-SERVED-PASS_Q)
(NOT-SERVED-PASS_R)
(NOT-SERVED-PASS_S)
(NOT-SERVED-PASS_C)
(NOT-SERVED-PASS_L)
(NOT-SERVED-PASS_M)
(NOT-SERVED-PASS_N)
(NOT-SERVED-PASS_H)
(LIFT_AT-E)
)
(:goal
(and
(Flag25-)
)
)
)
