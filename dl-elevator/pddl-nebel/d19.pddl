(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag291-)
(Flag290-)
(Flag289-)
(Flag288-)
(Flag287-)
(Flag286-)
(Flag285-)
(Flag37-)
(NOT-LIFT_AT-M)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(LIFT_AT-H)
(LIFT_AT-N)
(ERROR-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_S)
(BOARDED-PASS_A)
(SERVED-PASS_A)
(BOARDED-PASS_C)
(SERVED-PASS_L)
(BOARDED-PASS_L)
(SERVED-PASS_I)
(BOARDED-PASS_I)
(Flag39-)
(Flag35-)
(Flag29-)
(LIFT_AT-G)
(LIFT_AT-F)
(LIFT_AT-O)
(LIFT_AT-P)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_R)
(SERVED-PASS_R)
(BOARDED-PASS_S)
(Flag38-)
(Flag36-)
(Flag33-)
(Flag28-)
(LIFT_AT-E)
(LIFT_AT-D)
(LIFT_AT-Q)
(SERVED-PASS_C)
(SERVED-PASS_K)
(Flag43-)
(Flag42-)
(Flag34-)
(LIFT_AT-B)
(Flag25-)
(BOARDED-PASS_K)
(Flag44-)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_K)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-P)
(NOT-SERVED-PASS_K)
(NOT-SERVED-PASS_C)
(NOT-LIFT_AT-E)
(NOT-LIFT_AT-Q)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-O)
(NOT-LIFT_AT-N)
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-H)
(NOT-SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(NOT-SERVED-PASS_S)
(NOT-ERROR-)
(NOT-LIFT_AT-J)
(LIFT_AT-M)
(Flag291prime-)
(Flag290prime-)
(Flag289prime-)
(Flag288prime-)
(Flag287prime-)
(Flag286prime-)
(Flag285prime-)
(Flag37prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag39prime-)
(Flag35prime-)
(Flag29prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag38prime-)
(Flag36prime-)
(Flag33prime-)
(Flag28prime-)
(Flag43prime-)
(Flag42prime-)
(Flag34prime-)
(Flag25prime-)
(Flag44prime-)
)
(:action STOP-PASS_K-B
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag44Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim124Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim244Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim264Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action STOP-PASS_K-Q
:parameters ()
:precondition
(and
(Flag34-)
(Flag34prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-D
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
(Flag291prime-)
)
:effect
(and
(when
(and
(Flag291-)
)
(and
(BOARDED-PASS_K)
(not (NOT-BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-E
:parameters ()
:precondition
(and
(Flag43-)
(Flag43prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag25Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(SERVED-PASS_R)
(SERVED-PASS_S)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(SERVED-PASS_A)
(SERVED-PASS_C)
(SERVED-PASS_L)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(SERVED-PASS_I)
(SERVED-PASS_K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim25Action-13
:parameters ()
:precondition
(and
(not (SERVED-PASS_C))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_K))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEUP-B-D
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-D-B
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-Q-P
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag34Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim34Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Flag42Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim42Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag43Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(Flag43-)
(Flag43prime-)
)

)
(:action Prim43Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim83Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim94Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim103Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim122Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim142Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim143Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim163Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim174Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim183Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim194Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim203Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim214Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim222Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim223Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim234Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim242Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim243Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim254Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim262Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim263Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim282Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim283Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action STOP-PASS_K-P
:parameters ()
:precondition
(and
(Flag28-)
(Flag28prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-G
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-O
:parameters ()
:precondition
(and
(Flag36-)
(Flag36prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-F
:parameters ()
:precondition
(and
(Flag38-)
(Flag38prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_K)
)
(and
(SERVED-PASS_K)
(NOT-BOARDED-PASS_K)
(not (NOT-SERVED-PASS_K))
(not (BOARDED-PASS_K))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(when
(and
(BOARDED-PASS_C)
)
(and
(SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(not (NOT-SERVED-PASS_C))
(not (BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Prim25Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_R))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-5
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-6
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-7
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-8
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-9
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-10
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-11
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-15
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-16
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-P-Q
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-P-O
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-O-N
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag28Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim28Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Flag33Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim33Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Flag36Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim36Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag38Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action Prim38Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim48Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim73Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim78Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim88Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim93Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim128Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim133Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim158Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim168Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim173Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim178Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim188Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim193Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim196Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim198Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim213Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim216Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim218Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim233Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim238Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim248Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim256Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim258Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim268Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim273Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim278Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action STOP-PASS_K-N
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-J
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_K-H
:parameters ()
:precondition
(and
(Flag39-)
(Flag39prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag286prime-)
)
:effect
(and
(when
(and
(Flag286-)
)
(and
(BOARDED-PASS_S)
(not (NOT-BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
(Flag285prime-)
)
:effect
(and
(when
(and
(Flag285-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Prim25Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_S))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-12
:parameters ()
:precondition
(and
(not (SERVED-PASS_A))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_L))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_I))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-O-P
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-N-O
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-N-M
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag29Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Flag35Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Flag39Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim49Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim75Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim99Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim109Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim135Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim159Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim169Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim175Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim179Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim189Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim195Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim199Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim209Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim215Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim219Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim229Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim235Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim239Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim259Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim269Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim279Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action STOP-PASS_K-M
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
(Flag290prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_I)
)
(and
(SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(not (NOT-SERVED-PASS_I))
(not (BOARDED-PASS_I))
)
)
(when
(and
(Flag290-)
)
(and
(BOARDED-PASS_I)
(not (NOT-BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_I-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
(Flag289prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(when
(and
(Flag289-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
(Flag288prime-)
)
:effect
(and
(when
(and
(Flag288-)
)
(and
(BOARDED-PASS_C)
(not (NOT-BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(when
(and
(BOARDED-PASS_A)
)
(and
(SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(not (NOT-SERVED-PASS_A))
(not (BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
(Flag287prime-)
)
:effect
(and
(when
(and
(Flag287-)
)
(and
(BOARDED-PASS_A)
(not (NOT-BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_A-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_S)
)
(and
(SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(not (NOT-SERVED-PASS_S))
(not (BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_S-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action STOP-PASS_R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Prim25Action-19
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action Prim25Action-20
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag25prime-)
(not (Flag25-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-H-J
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-M-N
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEUP-J-M
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-J-H
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action MOVEDOWN-M-J
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag291prime-))
(not (Flag290prime-))
(not (Flag289prime-))
(not (Flag288prime-))
(not (Flag287prime-))
(not (Flag286prime-))
(not (Flag285prime-))
(not (Flag37prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag39prime-))
(not (Flag35prime-))
(not (Flag29prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag33prime-))
(not (Flag28prime-))
(not (Flag43prime-))
(not (Flag42prime-))
(not (Flag34prime-))
(not (Flag25prime-))
(not (Flag44prime-))
)
)
(:action Prim26Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim26Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim28Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim28Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag28prime-)
(not (Flag28-))
)

)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag33prime-)
(not (Flag33-))
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag34prime-)
(not (Flag34-))
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag36prime-)
(not (Flag36-))
)

)
(:action Flag37Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag38prime-)
(not (Flag38-))
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag43prime-)
(not (Flag43-))
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag285Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_R)
)
:effect
(and
(Flag285-)
(Flag285prime-)
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_R))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_R))
)
:effect
(and
(Flag285prime-)
(not (Flag285-))
)

)
(:action Flag286Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_S)
)
:effect
(and
(Flag286-)
(Flag286prime-)
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_S))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_S))
)
:effect
(and
(Flag286prime-)
(not (Flag286-))
)

)
(:action Flag287Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_A)
(NOT-SERVED-PASS_A)
)
:effect
(and
(Flag287-)
(Flag287prime-)
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_A))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_A))
)
:effect
(and
(Flag287prime-)
(not (Flag287-))
)

)
(:action Flag288Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_C)
)
:effect
(and
(Flag288-)
(Flag288prime-)
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_C))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_C))
)
:effect
(and
(Flag288prime-)
(not (Flag288-))
)

)
(:action Flag289Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_L)
)
:effect
(and
(Flag289-)
(Flag289prime-)
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_L))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_L))
)
:effect
(and
(Flag289prime-)
(not (Flag289-))
)

)
(:action Flag290Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_I)
)
:effect
(and
(Flag290-)
(Flag290prime-)
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_I))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_I))
)
:effect
(and
(Flag290prime-)
(not (Flag290-))
)

)
(:action Flag291Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_K)
(NOT-SERVED-PASS_K)
)
:effect
(and
(Flag291-)
(Flag291prime-)
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_K))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_K))
)
:effect
(and
(Flag291prime-)
(not (Flag291-))
)

)
)
