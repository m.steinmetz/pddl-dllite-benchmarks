(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag617-)
(Flag616-)
(Flag615-)
(Flag614-)
(Flag613-)
(Flag612-)
(Flag611-)
(Flag610-)
(Flag609-)
(Flag608-)
(Flag607-)
(Flag65-)
(NOT-LIFT_AT-AC)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(LIFT_AT-AE)
(ERROR-)
(Flag35-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AJ)
(BOARDED-PASS_AD)
(BOARDED-PASS_AB)
(Flag66-)
(Flag64-)
(LIFT_AT-AG)
(LIFT_AT-AM)
(LIFT_AT-AN)
(Flag36-)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_AI)
(SERVED-PASS_AD)
(SERVED-PASS_AL)
(BOARDED-PASS_AL)
(Flag62-)
(Flag60-)
(Flag58-)
(LIFT_AT-AO)
(LIFT_AT-AQ)
(BOARDED-PASS_AH)
(BOARDED-PASS_AF)
(SERVED-PASS_BB)
(BOARDED-PASS_BB)
(Flag59-)
(Flag57-)
(LIFT_AT-AR)
(LIFT_AT-AS)
(LIFT_AT-AT)
(SERVED-PASS_AU)
(BOARDED-PASS_AJ)
(SERVED-PASS_AK)
(BOARDED-PASS_AK)
(Flag55-)
(Flag54-)
(Flag52-)
(LIFT_AT-AV)
(BOARDED-PASS_AU)
(SERVED-PASS_AB)
(Flag44-)
(LIFT_AT-AW)
(LIFT_AT-AX)
(LIFT_AT-AY)
(SERVED-PASS_AH)
(SERVED-PASS_AP)
(Flag63-)
(Flag56-)
(Flag48-)
(LIFT_AT-AZ)
(LIFT_AT-BA)
(SERVED-PASS_AI)
(SERVED-PASS_AF)
(Flag46-)
(Flag42-)
(LIFT_AT-BC)
(Flag37-)
(BOARDED-PASS_AP)
(Flag41-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-BA)
(NOT-BOARDED-PASS_AP)
(NOT-LIFT_AT-BC)
(NOT-LIFT_AT-AZ)
(NOT-LIFT_AT-AY)
(NOT-SERVED-PASS_AF)
(NOT-SERVED-PASS_AI)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AW)
(NOT-LIFT_AT-AV)
(NOT-SERVED-PASS_AP)
(NOT-SERVED-PASS_AH)
(NOT-LIFT_AT-AT)
(NOT-SERVED-PASS_AB)
(NOT-BOARDED-PASS_AU)
(NOT-LIFT_AT-AS)
(NOT-LIFT_AT-AR)
(NOT-LIFT_AT-AQ)
(NOT-BOARDED-PASS_AK)
(NOT-SERVED-PASS_AK)
(NOT-BOARDED-PASS_AJ)
(NOT-SERVED-PASS_AU)
(NOT-LIFT_AT-AO)
(NOT-LIFT_AT-AN)
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(NOT-BOARDED-PASS_AF)
(NOT-BOARDED-PASS_AH)
(NOT-LIFT_AT-AM)
(NOT-LIFT_AT-AG)
(NOT-LIFT_AT-AE)
(NOT-SERVED-PASS_AL)
(NOT-BOARDED-PASS_AL)
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AI)
(NOT-BOARDED-PASS_AB)
(NOT-BOARDED-PASS_AD)
(NOT-SERVED-PASS_AJ)
(NOT-ERROR-)
(LIFT_AT-AC)
(NOT-LIFT_AT-AA)
(Flag617prime-)
(Flag616prime-)
(Flag615prime-)
(Flag614prime-)
(Flag613prime-)
(Flag612prime-)
(Flag611prime-)
(Flag610prime-)
(Flag609prime-)
(Flag608prime-)
(Flag607prime-)
(Flag65prime-)
(Flag35prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag66prime-)
(Flag64prime-)
(Flag36prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag62prime-)
(Flag60prime-)
(Flag58prime-)
(Flag59prime-)
(Flag57prime-)
(Flag55prime-)
(Flag54prime-)
(Flag52prime-)
(Flag44prime-)
(Flag63prime-)
(Flag56prime-)
(Flag48prime-)
(Flag46prime-)
(Flag42prime-)
(Flag37prime-)
(Flag41prime-)
)
(:action STOP-PASS_BB-BC
:parameters ()
:precondition
(and
(Flag41-)
(Flag41prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-BC-BA
:parameters ()
:precondition
(and
(LIFT_AT-BC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BC)
(LIFT_AT-BA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BC))
(not (NOT-LIFT_AT-BA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag41Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(Flag41-)
(Flag41prime-)
)

)
(:action Prim41Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim71Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim131Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim161Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim191Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim221Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim251Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim311Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim341Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim371Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim401Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim431Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim461Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim491Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim521Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim551Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action Prim581Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BC))
)
:effect
(and
)

)
(:action STOP-PASS_BB-BA
:parameters ()
:precondition
(and
(Flag42-)
(Flag42prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AZ
:parameters ()
:precondition
(and
(Flag46-)
(Flag46prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
(Flag615prime-)
)
:effect
(and
(when
(and
(Flag615-)
)
(and
(BOARDED-PASS_AP)
(not (NOT-BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-BC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag37Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(SERVED-PASS_AU)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(SERVED-PASS_AJ)
(Flag26-)
(Flag26prime-)
(SERVED-PASS_AH)
(SERVED-PASS_AI)
(SERVED-PASS_AF)
(Flag28-)
(Flag28prime-)
(SERVED-PASS_AD)
(Flag30-)
(Flag30prime-)
(SERVED-PASS_AB)
(Flag32-)
(Flag32prime-)
(SERVED-PASS_AK)
(Flag34-)
(Flag34prime-)
(Flag36-)
(Flag36prime-)
(SERVED-PASS_AP)
(SERVED-PASS_AL)
(SERVED-PASS_BB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim37Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_AI))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_AF))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-BA-BC
:parameters ()
:precondition
(and
(LIFT_AT-BA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BA)
(LIFT_AT-BC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BA))
(not (NOT-LIFT_AT-BC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-BA-AZ
:parameters ()
:precondition
(and
(LIFT_AT-BA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BA)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BA))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AZ-AY
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag42Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(Flag42-)
(Flag42prime-)
)

)
(:action Prim42Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Flag46Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(Flag46-)
(Flag46prime-)
)

)
(:action Prim46Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim72Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim106Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim132Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim166Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim192Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim196Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim222Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim226Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim252Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim256Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim282Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim286Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim312Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim316Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim342Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim346Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim372Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim376Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim402Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim406Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim432Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim436Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim462Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim466Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim492Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim496Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim522Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim526Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim552Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim556Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim582Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim586Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AX
:parameters ()
:precondition
(and
(Flag48-)
(Flag48prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AY
:parameters ()
:precondition
(and
(Flag56-)
(Flag56prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AW
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AF)
)
(and
(SERVED-PASS_AF)
(NOT-BOARDED-PASS_AF)
(not (NOT-SERVED-PASS_AF))
(not (BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AI)
)
(and
(SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
(not (NOT-SERVED-PASS_AI))
(not (BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim37Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_AH))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-26
:parameters ()
:precondition
(and
(not (SERVED-PASS_AP))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AZ-BA
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-BA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-BA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AY-AZ
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AY-AX
:parameters ()
:precondition
(and
(LIFT_AT-AY)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AY)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AY))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AX-AW
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag48Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag48-)
(Flag48prime-)
)

)
(:action Prim48Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Flag56Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(Flag56-)
(Flag56prime-)
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim78Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim86Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim93Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim146Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim168Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim183Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim198Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim213Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim243Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim258Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim266Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim273Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim288Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim296Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim303Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim318Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim326Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim333Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim348Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim356Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim363Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim378Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim386Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim393Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim408Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim416Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim423Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim438Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim446Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim453Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim468Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim476Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim483Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim498Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim506Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim513Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim528Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim536Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim543Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim558Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim566Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim573Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim588Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim596Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AY))
)
:effect
(and
)

)
(:action Prim603Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AV
:parameters ()
:precondition
(and
(Flag44-)
(Flag44prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(when
(and
(BOARDED-PASS_AP)
)
(and
(SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
(not (NOT-SERVED-PASS_AP))
(not (BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(BOARDED-PASS_AH)
)
(and
(SERVED-PASS_AH)
(NOT-BOARDED-PASS_AH)
(not (NOT-SERVED-PASS_AH))
(not (BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AY
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AY)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim37Action-21
:parameters ()
:precondition
(and
(not (SERVED-PASS_AB))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AX-AY
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AY)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AY))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AW-AX
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AV-AT
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag44Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag44-)
(Flag44prime-)
)

)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim194Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim254Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim314Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim344Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim374Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim404Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim434Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim464Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim494Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim524Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim554Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim584Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AT
:parameters ()
:precondition
(and
(Flag52-)
(Flag52prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AR
:parameters ()
:precondition
(and
(Flag54-)
(Flag54prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AS
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(BOARDED-PASS_AB)
)
(and
(SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
(not (NOT-SERVED-PASS_AB))
(not (BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
(Flag607prime-)
)
:effect
(and
(when
(and
(Flag607-)
)
(and
(BOARDED-PASS_AU)
(not (NOT-BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim37Action-5
:parameters ()
:precondition
(and
(not (SERVED-PASS_AU))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-23
:parameters ()
:precondition
(and
(not (SERVED-PASS_AK))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AT-AV
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AT-AS
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AS-AR
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AR-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag52Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag52-)
(Flag52prime-)
)

)
(:action Prim52Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Flag54Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(Flag54-)
(Flag54prime-)
)

)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim112Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim142Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim145Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim172Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim174Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim175Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim232Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim234Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim235Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim262Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim264Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim265Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim292Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim294Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim295Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim322Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim324Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim325Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim352Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim354Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim355Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim382Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim384Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim385Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim412Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim414Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim415Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim442Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim444Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim445Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim472Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim474Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim475Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim502Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim504Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim505Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim532Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim534Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim535Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim562Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim564Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim565Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim592Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim594Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim595Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AQ
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AO
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
(Flag614prime-)
)
:effect
(and
(when
(and
(Flag614-)
)
(and
(BOARDED-PASS_AK)
(not (NOT-BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(when
(and
(BOARDED-PASS_AK)
)
(and
(SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
(not (NOT-SERVED-PASS_AK))
(not (BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
(Flag608prime-)
)
:effect
(and
(when
(and
(Flag608-)
)
(and
(BOARDED-PASS_AJ)
(not (NOT-BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(when
(and
(BOARDED-PASS_AU)
)
(and
(SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
(not (NOT-SERVED-PASS_AU))
(not (BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim37Action-28
:parameters ()
:precondition
(and
(not (SERVED-PASS_BB))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AS-AT
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AR-AS
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AQ-AR
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AQ-AO
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AO-AN
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag57Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim87Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim147Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim179Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim209Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim237Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim239Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim267Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim269Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim297Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim299Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim327Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim329Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim357Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim359Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim387Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim389Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim417Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim419Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim447Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim449Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim477Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim479Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim507Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim509Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim537Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim539Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim567Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim569Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim597Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim599Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AN
:parameters ()
:precondition
(and
(Flag58-)
(Flag58prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AM
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AG
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
(Flag617prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_BB)
)
(and
(SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(not (NOT-SERVED-PASS_BB))
(not (BOARDED-PASS_BB))
)
)
(when
(and
(Flag617-)
)
(and
(BOARDED-PASS_BB)
(not (NOT-BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
(Flag611prime-)
)
:effect
(and
(when
(and
(Flag611-)
)
(and
(BOARDED-PASS_AF)
(not (NOT-BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag609prime-)
)
:effect
(and
(when
(and
(Flag609-)
)
(and
(BOARDED-PASS_AH)
(not (NOT-BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-3
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-4
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-6
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-7
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-8
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-9
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-10
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-11
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-12
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-14
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-18
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AD))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-20
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-22
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-24
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-25
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-27
:parameters ()
:precondition
(and
(not (SERVED-PASS_AL))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AO-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AN-AO
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AN-AM
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AM-AG
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AG-AE
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag58Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(Flag58-)
(Flag58prime-)
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim88Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim90Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim92Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim122Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim150Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim152Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim178Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim210Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim212Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim238Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim240Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim242Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim268Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim270Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim272Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim298Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim300Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim302Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim328Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim330Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim332Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim358Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim360Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim362Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim388Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim390Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim392Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim418Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim420Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim422Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim448Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim450Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim452Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim478Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim480Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim482Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim508Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim510Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim512Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim538Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim540Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim542Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim568Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim570Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim572Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim598Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim600Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim602Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AE
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_BB-AA
:parameters ()
:precondition
(and
(Flag66-)
(Flag66prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
(Flag616prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_AL)
)
(and
(SERVED-PASS_AL)
(NOT-BOARDED-PASS_AL)
(not (NOT-SERVED-PASS_AL))
(not (BOARDED-PASS_AL))
)
)
(when
(and
(Flag616-)
)
(and
(BOARDED-PASS_AL)
(not (NOT-BOARDED-PASS_AL))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(when
(and
(BOARDED-PASS_AD)
)
(and
(SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
(not (NOT-SERVED-PASS_AD))
(not (BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag610prime-)
)
:effect
(and
(when
(and
(Flag610-)
)
(and
(BOARDED-PASS_AI)
(not (NOT-BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag36Action-1
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Prim37Action-13
:parameters ()
:precondition
(and
(not (SERVED-PASS_AJ))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action MOVEUP-AM-AN
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AG-AM
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AE-AG
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AE-AC
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag64Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Flag66Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(Flag66-)
(Flag66prime-)
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim94Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim124Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim126Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim186Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim214Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim216Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim244Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim246Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim304Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim306Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim334Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim336Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim364Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim366Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim394Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim396Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim424Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim426Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim454Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim456Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim484Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim486Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim514Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim516Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim544Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim546Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim574Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim576Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim604Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim606Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AC
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AL-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
(Flag613prime-)
)
:effect
(and
(when
(and
(Flag613-)
)
(and
(BOARDED-PASS_AB)
(not (NOT-BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
(Flag612prime-)
)
:effect
(and
(when
(and
(Flag612-)
)
(and
(BOARDED-PASS_AD)
(not (NOT-BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(when
(and
(BOARDED-PASS_AJ)
)
(and
(SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AJ)
(not (NOT-SERVED-PASS_AJ))
(not (BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action STOP-PASS_AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action
:parameters ()
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim37Action-29
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action Prim37Action-30
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag37prime-)
(not (Flag37-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AC-AE
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEUP-AA-AC
:parameters ()
:precondition
(and
(LIFT_AT-AA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AA)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AA))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action MOVEDOWN-AC-AA
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag617prime-))
(not (Flag616prime-))
(not (Flag615prime-))
(not (Flag614prime-))
(not (Flag613prime-))
(not (Flag612prime-))
(not (Flag611prime-))
(not (Flag610prime-))
(not (Flag609prime-))
(not (Flag608prime-))
(not (Flag607prime-))
(not (Flag65prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag66prime-))
(not (Flag64prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag62prime-))
(not (Flag60prime-))
(not (Flag58prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag55prime-))
(not (Flag54prime-))
(not (Flag52prime-))
(not (Flag44prime-))
(not (Flag63prime-))
(not (Flag56prime-))
(not (Flag48prime-))
(not (Flag46prime-))
(not (Flag42prime-))
(not (Flag37prime-))
(not (Flag41prime-))
)
)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag41prime-)
(not (Flag41-))
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag42prime-)
(not (Flag42-))
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag44prime-)
(not (Flag44-))
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag46prime-)
(not (Flag46-))
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag48prime-)
(not (Flag48-))
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag52prime-)
(not (Flag52-))
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag54prime-)
(not (Flag54-))
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag56prime-)
(not (Flag56-))
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag58prime-)
(not (Flag58-))
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag66prime-)
(not (Flag66-))
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim533Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim533Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim551Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim551Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim564Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim564Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim568Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim568Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim569Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim569Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim570Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim570Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim571Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim571Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim572Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim572Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim573Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim573Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim574Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim574Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim575Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim576Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim576Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim578Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim578Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim579Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim579Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim580Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim580Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim581Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim581Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim582Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim582Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim583Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim583Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim584Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim584Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim585Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim585Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim586Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim586Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim587Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim588Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim588Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim589Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim589Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim590Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim591Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim591Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim592Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim592Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim593Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim593Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim594Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim594Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim595Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim595Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim596Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim596Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim597Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim597Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim598Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim598Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim599Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim599Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim600Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim600Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim601Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim601Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim602Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim602Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim603Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim603Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim604Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim604Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim605Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim606Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim606Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag607Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
)
:effect
(and
(Flag607-)
(Flag607prime-)
)

)
(:action Prim607Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AU))
)
:effect
(and
(Flag607prime-)
(not (Flag607-))
)

)
(:action Prim607Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AU))
)
:effect
(and
(Flag607prime-)
(not (Flag607-))
)

)
(:action Flag608Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AJ)
)
:effect
(and
(Flag608-)
(Flag608prime-)
)

)
(:action Prim608Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AJ))
)
:effect
(and
(Flag608prime-)
(not (Flag608-))
)

)
(:action Prim608Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AJ))
)
:effect
(and
(Flag608prime-)
(not (Flag608-))
)

)
(:action Flag609Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AH)
(NOT-BOARDED-PASS_AH)
)
:effect
(and
(Flag609-)
(Flag609prime-)
)

)
(:action Prim609Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AH))
)
:effect
(and
(Flag609prime-)
(not (Flag609-))
)

)
(:action Prim609Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AH))
)
:effect
(and
(Flag609prime-)
(not (Flag609-))
)

)
(:action Flag610Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
)
:effect
(and
(Flag610-)
(Flag610prime-)
)

)
(:action Prim610Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AI))
)
:effect
(and
(Flag610prime-)
(not (Flag610-))
)

)
(:action Prim610Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AI))
)
:effect
(and
(Flag610prime-)
(not (Flag610-))
)

)
(:action Flag611Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AF)
(NOT-BOARDED-PASS_AF)
)
:effect
(and
(Flag611-)
(Flag611prime-)
)

)
(:action Prim611Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AF))
)
:effect
(and
(Flag611prime-)
(not (Flag611-))
)

)
(:action Prim611Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AF))
)
:effect
(and
(Flag611prime-)
(not (Flag611-))
)

)
(:action Flag612Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
)
:effect
(and
(Flag612-)
(Flag612prime-)
)

)
(:action Prim612Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AD))
)
:effect
(and
(Flag612prime-)
(not (Flag612-))
)

)
(:action Prim612Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AD))
)
:effect
(and
(Flag612prime-)
(not (Flag612-))
)

)
(:action Flag613Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
)
:effect
(and
(Flag613-)
(Flag613prime-)
)

)
(:action Prim613Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AB))
)
:effect
(and
(Flag613prime-)
(not (Flag613-))
)

)
(:action Prim613Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AB))
)
:effect
(and
(Flag613prime-)
(not (Flag613-))
)

)
(:action Flag614Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
)
:effect
(and
(Flag614-)
(Flag614prime-)
)

)
(:action Prim614Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AK))
)
:effect
(and
(Flag614prime-)
(not (Flag614-))
)

)
(:action Prim614Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AK))
)
:effect
(and
(Flag614prime-)
(not (Flag614-))
)

)
(:action Flag615Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
)
:effect
(and
(Flag615-)
(Flag615prime-)
)

)
(:action Prim615Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AP))
)
:effect
(and
(Flag615prime-)
(not (Flag615-))
)

)
(:action Prim615Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AP))
)
:effect
(and
(Flag615prime-)
(not (Flag615-))
)

)
(:action Flag616Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AL)
(NOT-BOARDED-PASS_AL)
)
:effect
(and
(Flag616-)
(Flag616prime-)
)

)
(:action Prim616Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AL))
)
:effect
(and
(Flag616prime-)
(not (Flag616-))
)

)
(:action Prim616Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AL))
)
:effect
(and
(Flag616prime-)
(not (Flag616-))
)

)
(:action Flag617Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
)
:effect
(and
(Flag617-)
(Flag617prime-)
)

)
(:action Prim617Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BB))
)
:effect
(and
(Flag617prime-)
(not (Flag617-))
)

)
(:action Prim617Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BB))
)
:effect
(and
(Flag617prime-)
(not (Flag617-))
)

)
)
