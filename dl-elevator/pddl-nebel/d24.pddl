(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag439-)
(Flag438-)
(Flag437-)
(Flag436-)
(Flag435-)
(Flag434-)
(Flag433-)
(Flag432-)
(Flag431-)
(Flag411-)
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(LIFT_AT-M)
(LIFT_AT-T)
(LIFT_AT-U)
(LIFT_AT-V)
(ERROR-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_R)
(SERVED-PASS_B)
(BOARDED-PASS_B)
(SERVED-PASS_N)
(BOARDED-PASS_N)
(Flag413-)
(Flag412-)
(Flag410-)
(Flag409-)
(Flag407-)
(LIFT_AT-K)
(LIFT_AT-J)
(LIFT_AT-W)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_R)
(SERVED-PASS_L)
(SERVED-PASS_I)
(Flag415-)
(Flag414-)
(Flag408-)
(LIFT_AT-H)
(Flag416-)
(LIFT_AT-G)
(LIFT_AT-F)
(BOARDED-PASS_Q)
(BOARDED-PASS_X)
(SERVED-PASS_X)
(BOARDED-PASS_C)
(SERVED-PASS_C)
(Flag418-)
(Flag417-)
(LIFT_AT-E)
(LIFT_AT-D)
(SERVED-PASS_Q)
(SERVED-PASS_S)
(BOARDED-PASS_S)
(BOARDED-PASS_I)
(Flag420-)
(Flag419-)
(LIFT_AT-A)
(Flag31-)
(BOARDED-PASS_L)
(Flag421-)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_L)
(NOT-LIFT_AT-D)
(NOT-LIFT_AT-A)
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_Q)
(NOT-LIFT_AT-E)
(NOT-LIFT_AT-F)
(NOT-SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_X)
(NOT-BOARDED-PASS_X)
(NOT-BOARDED-PASS_Q)
(NOT-LIFT_AT-G)
(NOT-LIFT_AT-H)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-V)
(NOT-SERVED-PASS_I)
(NOT-SERVED-PASS_L)
(NOT-SERVED-PASS_R)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-W)
(NOT-LIFT_AT-M)
(NOT-LIFT_AT-U)
(NOT-LIFT_AT-T)
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
(NOT-BOARDED-PASS_B)
(NOT-SERVED-PASS_B)
(NOT-BOARDED-PASS_R)
(NOT-ERROR-)
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(Flag439prime-)
(Flag438prime-)
(Flag437prime-)
(Flag436prime-)
(Flag435prime-)
(Flag434prime-)
(Flag433prime-)
(Flag432prime-)
(Flag431prime-)
(Flag411prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag413prime-)
(Flag412prime-)
(Flag410prime-)
(Flag409prime-)
(Flag407prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag415prime-)
(Flag414prime-)
(Flag408prime-)
(Flag416prime-)
(Flag418prime-)
(Flag417prime-)
(Flag420prime-)
(Flag419prime-)
(Flag31prime-)
(Flag421prime-)
)
(:action STOP-PASS_I-A
:parameters ()
:precondition
(and
(Flag421-)
(Flag421prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim47Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim72Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim97Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim122Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim147Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim172Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim197Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim222Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim247Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim272Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim297Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim322Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim347Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim372Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim397Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Flag421Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(Flag421-)
(Flag421prime-)
)

)
(:action Prim421Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action STOP-PASS_I-D
:parameters ()
:precondition
(and
(Flag419-)
(Flag419prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-E
:parameters ()
:precondition
(and
(Flag420-)
(Flag420prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
(Flag437prime-)
)
:effect
(and
(when
(and
(Flag437-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Flag31Action
:parameters ()
:precondition
(and
(SERVED-PASS_Q)
(SERVED-PASS_R)
(SERVED-PASS_S)
(SERVED-PASS_X)
(SERVED-PASS_B)
(SERVED-PASS_C)
(SERVED-PASS_L)
(SERVED-PASS_N)
(SERVED-PASS_I)
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (SERVED-PASS_Q))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-2
:parameters ()
:precondition
(and
(not (SERVED-PASS_S))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action MOVEUP-A-D
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-D-A
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim45Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim46Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim70Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim71Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim95Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim96Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim120Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim121Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim145Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim146Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim170Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim171Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim195Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim196Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim220Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim221Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim245Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim246Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim270Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim271Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim295Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim296Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim320Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim321Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim345Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim346Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim370Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim371Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim395Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim396Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Flag419Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(Flag419-)
(Flag419prime-)
)

)
(:action Prim419Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Flag420Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(Flag420-)
(Flag420prime-)
)

)
(:action Prim420Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action STOP-PASS_I-F
:parameters ()
:precondition
(and
(Flag417-)
(Flag417prime-)
(Flag439prime-)
)
:effect
(and
(when
(and
(Flag439-)
)
(and
(BOARDED-PASS_I)
(not (NOT-BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-G
:parameters ()
:precondition
(and
(Flag418-)
(Flag418prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
(Flag433prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_S)
)
(and
(SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(not (NOT-SERVED-PASS_S))
(not (BOARDED-PASS_S))
)
)
(when
(and
(Flag433-)
)
(and
(BOARDED-PASS_S)
(not (NOT-BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(when
(and
(BOARDED-PASS_Q)
)
(and
(SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
(not (NOT-SERVED-PASS_Q))
(not (BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim31Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_X))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-5
:parameters ()
:precondition
(and
(not (SERVED-PASS_C))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action MOVEUP-D-E
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-E-D
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim43Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim44Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim68Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim69Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim93Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim94Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim118Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim119Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim143Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim144Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim168Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim169Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim193Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim194Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim218Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim219Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim243Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim244Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim268Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim269Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim293Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim294Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim318Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim319Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim343Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim344Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim368Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim369Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Prim393Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim394Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
)

)
(:action Flag417Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag417-)
(Flag417prime-)
)

)
(:action Prim417Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Flag418Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(Flag418-)
(Flag418prime-)
)

)
(:action Prim418Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-G))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action STOP-PASS_I-H
:parameters ()
:precondition
(and
(Flag416-)
(Flag416prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(BOARDED-PASS_C)
)
(and
(SERVED-PASS_C)
(NOT-BOARDED-PASS_C)
(not (NOT-SERVED-PASS_C))
(not (BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
(Flag436prime-)
)
:effect
(and
(when
(and
(Flag436-)
)
(and
(BOARDED-PASS_C)
(not (NOT-BOARDED-PASS_C))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(when
(and
(BOARDED-PASS_X)
)
(and
(SERVED-PASS_X)
(NOT-BOARDED-PASS_X)
(not (NOT-SERVED-PASS_X))
(not (BOARDED-PASS_X))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
(Flag434prime-)
)
:effect
(and
(when
(and
(Flag434-)
)
(and
(BOARDED-PASS_X)
(not (NOT-BOARDED-PASS_X))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-G
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-G)
(Flag431prime-)
)
:effect
(and
(when
(and
(Flag431-)
)
(and
(BOARDED-PASS_Q)
(not (NOT-BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-F-G
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-G-H
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-G-F
:parameters ()
:precondition
(and
(LIFT_AT-G)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-G)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-G))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-H-G
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-G)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-G))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim42Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim67Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim92Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim117Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim142Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim167Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim192Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim217Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim242Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim267Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim292Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim317Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim342Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim367Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim392Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Flag416Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(Flag416-)
(Flag416prime-)
)

)
(:action Prim416Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action STOP-PASS_I-W
:parameters ()
:precondition
(and
(Flag408-)
(Flag408prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-J
:parameters ()
:precondition
(and
(Flag414-)
(Flag414prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-K
:parameters ()
:precondition
(and
(Flag415-)
(Flag415prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (SERVED-PASS_R))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-6
:parameters ()
:precondition
(and
(not (SERVED-PASS_L))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_I))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-9
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-10
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-11
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-12
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-13
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-14
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-15
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-16
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-17
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-18
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-19
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-20
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-21
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-22
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-23
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action MOVEUP-H-J
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-J-H
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-W-V
:parameters ()
:precondition
(and
(LIFT_AT-W)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-W)
(LIFT_AT-V)
(CHECKCONSISTENCY-)
(not (LIFT_AT-W))
(not (NOT-LIFT_AT-V))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim34Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim40Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim41Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim59Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim65Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim66Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim84Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim90Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim91Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim109Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim115Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim116Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim134Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim140Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim141Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim159Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim165Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim166Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim184Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim190Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim191Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim209Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim215Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim216Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim234Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim240Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim241Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim259Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim265Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim266Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim284Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim290Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim291Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim309Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim315Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim316Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim334Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim340Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim341Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim359Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim365Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim366Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim384Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim390Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim391Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Flag408Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(Flag408-)
(Flag408prime-)
)

)
(:action Prim408Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Flag414Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag414-)
(Flag414prime-)
)

)
(:action Prim414Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Flag415Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(Flag415-)
(Flag415prime-)
)

)
(:action Prim415Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action STOP-PASS_I-V
:parameters ()
:precondition
(and
(Flag407-)
(Flag407prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-T
:parameters ()
:precondition
(and
(Flag409-)
(Flag409prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_I)
)
(and
(SERVED-PASS_I)
(NOT-BOARDED-PASS_I)
(not (NOT-SERVED-PASS_I))
(not (BOARDED-PASS_I))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-U
:parameters ()
:precondition
(and
(Flag410-)
(Flag410prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-O
:parameters ()
:precondition
(and
(Flag412-)
(Flag412prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_I-M
:parameters ()
:precondition
(and
(Flag413-)
(Flag413prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Prim31Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_B))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-7
:parameters ()
:precondition
(and
(not (SERVED-PASS_N))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-V-W
:parameters ()
:precondition
(and
(LIFT_AT-V)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-V)
(LIFT_AT-W)
(CHECKCONSISTENCY-)
(not (LIFT_AT-V))
(not (NOT-LIFT_AT-W))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-K-M
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-V-U
:parameters ()
:precondition
(and
(LIFT_AT-V)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-V)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-V))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-U-T
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-M-K
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-T-P
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim33Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim35Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim36Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim38Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim39Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim58Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim60Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim61Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim63Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim64Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim83Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim85Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim86Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim88Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim89Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim108Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim110Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim111Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim113Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim114Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim133Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim135Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim136Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim138Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim139Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim158Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim161Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim163Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim164Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim183Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim185Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim186Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim188Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim189Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim208Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim210Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim211Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim213Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim214Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim233Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim235Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim236Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim238Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim239Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim258Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim260Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim261Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim263Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim264Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim283Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim285Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim286Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim288Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim289Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim308Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim310Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim311Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim313Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim314Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim333Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim335Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim336Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim338Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim339Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim358Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim360Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim361Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim363Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim364Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim383Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
)

)
(:action Prim385Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim386Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim388Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
)

)
(:action Prim389Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Flag407Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(Flag407-)
(Flag407prime-)
)

)
(:action Prim407Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-V))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Flag409Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(Flag409-)
(Flag409prime-)
)

)
(:action Prim409Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Flag410Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(Flag410-)
(Flag410prime-)
)

)
(:action Prim410Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Flag412Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(Flag412-)
(Flag412prime-)
)

)
(:action Prim412Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-O))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Flag413Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(Flag413-)
(Flag413prime-)
)

)
(:action Prim413Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action STOP-PASS_I-P
:parameters ()
:precondition
(and
(Flag411-)
(Flag411prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
(Flag438prime-)
)
:effect
(and
(when
(and
(Flag438-)
)
(and
(BOARDED-PASS_N)
(not (NOT-BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(when
(and
(BOARDED-PASS_N)
)
(and
(SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(not (NOT-SERVED-PASS_N))
(not (BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_C-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag435prime-)
)
:effect
(and
(when
(and
(Flag435-)
)
(and
(BOARDED-PASS_B)
(not (NOT-BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_B)
)
(and
(SERVED-PASS_B)
(NOT-BOARDED-PASS_B)
(not (NOT-SERVED-PASS_B))
(not (BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_X-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_S-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
(Flag432prime-)
)
:effect
(and
(when
(and
(Flag432-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-V
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-V)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-O
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-O)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action STOP-PASS_Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim31Action-24
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action Prim31Action-25
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag31prime-)
(not (Flag31-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-U-V
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-V)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-V))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-T-U
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-M-O
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-O-P
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEUP-P-T
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-O-M
:parameters ()
:precondition
(and
(LIFT_AT-O)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-O)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-O))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action MOVEDOWN-P-O
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-O)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-O))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag439prime-))
(not (Flag438prime-))
(not (Flag437prime-))
(not (Flag436prime-))
(not (Flag435prime-))
(not (Flag434prime-))
(not (Flag433prime-))
(not (Flag432prime-))
(not (Flag431prime-))
(not (Flag411prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag413prime-))
(not (Flag412prime-))
(not (Flag410prime-))
(not (Flag409prime-))
(not (Flag407prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag415prime-))
(not (Flag414prime-))
(not (Flag408prime-))
(not (Flag416prime-))
(not (Flag418prime-))
(not (Flag417prime-))
(not (Flag420prime-))
(not (Flag419prime-))
(not (Flag31prime-))
(not (Flag421prime-))
)
)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim37Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag407prime-)
(not (Flag407-))
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag408prime-)
(not (Flag408-))
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag409prime-)
(not (Flag409-))
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag410prime-)
(not (Flag410-))
)

)
(:action Flag411Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(Flag411-)
(Flag411prime-)
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim411Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
(Flag411prime-)
(not (Flag411-))
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag412prime-)
(not (Flag412-))
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag413prime-)
(not (Flag413-))
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag414prime-)
(not (Flag414-))
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag415prime-)
(not (Flag415-))
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag416prime-)
(not (Flag416-))
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag417prime-)
(not (Flag417-))
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag418prime-)
(not (Flag418-))
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag419prime-)
(not (Flag419-))
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag420prime-)
(not (Flag420-))
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag421prime-)
(not (Flag421-))
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag431Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_Q)
(NOT-SERVED-PASS_Q)
)
:effect
(and
(Flag431-)
(Flag431prime-)
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_Q))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_Q))
)
:effect
(and
(Flag431prime-)
(not (Flag431-))
)

)
(:action Flag432Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_R)
)
:effect
(and
(Flag432-)
(Flag432prime-)
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_R))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_R))
)
:effect
(and
(Flag432prime-)
(not (Flag432-))
)

)
(:action Flag433Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_S)
)
:effect
(and
(Flag433-)
(Flag433prime-)
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_S))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_S))
)
:effect
(and
(Flag433prime-)
(not (Flag433-))
)

)
(:action Flag434Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_X)
(NOT-SERVED-PASS_X)
)
:effect
(and
(Flag434-)
(Flag434prime-)
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_X))
)
:effect
(and
(Flag434prime-)
(not (Flag434-))
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_X))
)
:effect
(and
(Flag434prime-)
(not (Flag434-))
)

)
(:action Flag435Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_B)
(NOT-SERVED-PASS_B)
)
:effect
(and
(Flag435-)
(Flag435prime-)
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_B))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_B))
)
:effect
(and
(Flag435prime-)
(not (Flag435-))
)

)
(:action Flag436Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_C)
(NOT-SERVED-PASS_C)
)
:effect
(and
(Flag436-)
(Flag436prime-)
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_C))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_C))
)
:effect
(and
(Flag436prime-)
(not (Flag436-))
)

)
(:action Flag437Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_L)
)
:effect
(and
(Flag437-)
(Flag437prime-)
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_L))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_L))
)
:effect
(and
(Flag437prime-)
(not (Flag437-))
)

)
(:action Flag438Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
)
:effect
(and
(Flag438-)
(Flag438prime-)
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_N))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_N))
)
:effect
(and
(Flag438prime-)
(not (Flag438-))
)

)
(:action Flag439Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_I)
(NOT-SERVED-PASS_I)
)
:effect
(and
(Flag439-)
(Flag439prime-)
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_I))
)
:effect
(and
(Flag439prime-)
(not (Flag439-))
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_I))
)
:effect
(and
(Flag439prime-)
(not (Flag439-))
)

)
)
