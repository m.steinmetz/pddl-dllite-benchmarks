(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag397-)
(Flag396-)
(Flag395-)
(Flag394-)
(Flag393-)
(Flag392-)
(Flag391-)
(Flag390-)
(Flag389-)
(Flag377-)
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(LIFT_AT-H)
(LIFT_AT-I)
(LIFT_AT-J)
(LIFT_AT-C)
(LIFT_AT-A)
(ERROR-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_T)
(BOARDED-PASS_V)
(SERVED-PASS_V)
(SERVED-PASS_G)
(BOARDED-PASS_G)
(SERVED-PASS_B)
(SERVED-PASS_L)
(BOARDED-PASS_L)
(BOARDED-PASS_N)
(SERVED-PASS_N)
(Flag379-)
(Flag378-)
(Flag376-)
(Flag375-)
(Flag374-)
(Flag372-)
(LIFT_AT-K)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_S)
(BOARDED-PASS_O)
(Flag373-)
(LIFT_AT-M)
(LIFT_AT-P)
(BOARDED-PASS_B)
(Flag371-)
(Flag369-)
(LIFT_AT-Q)
(LIFT_AT-R)
(SERVED-PASS_T)
(BOARDED-PASS_S)
(SERVED-PASS_D)
(BOARDED-PASS_D)
(Flag370-)
(Flag368-)
(LIFT_AT-U)
(LIFT_AT-W)
(SERVED-PASS_O)
(Flag367-)
(Flag366-)
(Flag29-)
(NOT-CHECKCONSISTENCY-)
(NOT-SERVED-PASS_O)
(NOT-LIFT_AT-U)
(NOT-LIFT_AT-R)
(NOT-LIFT_AT-W)
(NOT-SERVED-PASS_D)
(NOT-BOARDED-PASS_D)
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_T)
(NOT-LIFT_AT-Q)
(NOT-LIFT_AT-P)
(NOT-BOARDED-PASS_B)
(NOT-LIFT_AT-M)
(NOT-LIFT_AT-K)
(NOT-BOARDED-PASS_O)
(NOT-SERVED-PASS_S)
(NOT-LIFT_AT-J)
(NOT-LIFT_AT-C)
(NOT-SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_B)
(NOT-BOARDED-PASS_G)
(NOT-SERVED-PASS_G)
(NOT-SERVED-PASS_V)
(NOT-BOARDED-PASS_V)
(NOT-BOARDED-PASS_T)
(NOT-ERROR-)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-H)
(NOT-LIFT_AT-A)
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(Flag397prime-)
(Flag396prime-)
(Flag395prime-)
(Flag394prime-)
(Flag393prime-)
(Flag392prime-)
(Flag391prime-)
(Flag390prime-)
(Flag389prime-)
(Flag377prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag379prime-)
(Flag378prime-)
(Flag376prime-)
(Flag375prime-)
(Flag374prime-)
(Flag372prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag373prime-)
(Flag371prime-)
(Flag369prime-)
(Flag370prime-)
(Flag368prime-)
(Flag367prime-)
(Flag366prime-)
(Flag29prime-)
)
(:action STOP-PASS_O-W
:parameters ()
:precondition
(and
(Flag366-)
(Flag366prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-U
:parameters ()
:precondition
(and
(Flag367-)
(Flag367prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Flag29Action
:parameters ()
:precondition
(and
(SERVED-PASS_T)
(SERVED-PASS_V)
(SERVED-PASS_S)
(SERVED-PASS_D)
(SERVED-PASS_G)
(SERVED-PASS_B)
(SERVED-PASS_L)
(SERVED-PASS_N)
(SERVED-PASS_O)
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(Flag28-)
(Flag28prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Prim29Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_O))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim31Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim32Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim80Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim103Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim127Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim128Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim151Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim152Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim175Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim199Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim223Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim247Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim248Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim271Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim272Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim295Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim296Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim319Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim320Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim343Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
)

)
(:action Prim344Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Flag366Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(Flag366-)
(Flag366prime-)
)

)
(:action Prim366Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-W))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Flag367Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(Flag367-)
(Flag367prime-)
)

)
(:action Prim367Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action STOP-PASS_O-R
:parameters ()
:precondition
(and
(Flag368-)
(Flag368prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-Q
:parameters ()
:precondition
(and
(Flag370-)
(Flag370prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-W
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-W)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (SERVED-PASS_T))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_D))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action MOVEDOWN-W-U
:parameters ()
:precondition
(and
(LIFT_AT-W)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-W)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-W))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-U-R
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-R)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-R))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-U-W
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-W)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-W))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-R-U
:parameters ()
:precondition
(and
(LIFT_AT-R)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-R)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-R))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim33Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim83Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim105Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim107Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim131Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim179Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim201Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim203Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim225Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim227Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim251Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim273Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim297Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim299Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim321Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim323Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Prim345Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
)

)
(:action Prim347Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
)

)
(:action Flag368Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(Flag368-)
(Flag368prime-)
)

)
(:action Prim368Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-R))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Flag370Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(Flag370-)
(Flag370prime-)
)

)
(:action Prim370Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-Q))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action STOP-PASS_O-P
:parameters ()
:precondition
(and
(Flag369-)
(Flag369prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-M
:parameters ()
:precondition
(and
(Flag371-)
(Flag371prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
(Flag392prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_D)
)
(and
(SERVED-PASS_D)
(NOT-BOARDED-PASS_D)
(not (NOT-SERVED-PASS_D))
(not (BOARDED-PASS_D))
)
)
(when
(and
(Flag392-)
)
(and
(BOARDED-PASS_D)
(not (NOT-BOARDED-PASS_D))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
(Flag391prime-)
)
:effect
(and
(when
(and
(Flag391-)
)
(and
(BOARDED-PASS_S)
(not (NOT-BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-R
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-R)
)
:effect
(and
(when
(and
(BOARDED-PASS_T)
)
(and
(SERVED-PASS_T)
(NOT-BOARDED-PASS_T)
(not (NOT-SERVED-PASS_T))
(not (BOARDED-PASS_T))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-Q
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-Q)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-R-Q
:parameters ()
:precondition
(and
(LIFT_AT-R)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-R)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-R))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-Q-P
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-Q-R
:parameters ()
:precondition
(and
(LIFT_AT-Q)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-Q)
(LIFT_AT-R)
(CHECKCONSISTENCY-)
(not (LIFT_AT-Q))
(not (NOT-LIFT_AT-R))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-P-Q
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-Q)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-Q))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim34Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim36Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim106Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim130Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim132Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim178Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim226Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim250Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim252Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim298Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim300Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim322Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim324Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim346Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
)

)
(:action Prim348Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Flag369Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(Flag369-)
(Flag369prime-)
)

)
(:action Prim369Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-P))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Flag371Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(Flag371-)
(Flag371prime-)
)

)
(:action Prim371Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action STOP-PASS_O-K
:parameters ()
:precondition
(and
(Flag373-)
(Flag373prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
(Flag394prime-)
)
:effect
(and
(when
(and
(Flag394-)
)
(and
(BOARDED-PASS_B)
(not (NOT-BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-P
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-P)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim29Action-2
:parameters ()
:precondition
(and
(not (SERVED-PASS_S))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-9
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-10
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-11
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-12
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-13
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-14
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-15
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-16
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-17
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-18
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-19
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-20
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-21
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-22
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action MOVEDOWN-P-M
:parameters ()
:precondition
(and
(LIFT_AT-P)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-P)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-P))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-M-K
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-M-P
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-P)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-P))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-K-M
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim38Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim86Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim110Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim158Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim230Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim254Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim278Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim302Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim326Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim350Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Flag373Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(Flag373-)
(Flag373prime-)
)

)
(:action Prim373Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action STOP-PASS_O-J
:parameters ()
:precondition
(and
(Flag372-)
(Flag372prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-H
:parameters ()
:precondition
(and
(Flag374-)
(Flag374prime-)
(Flag397prime-)
)
:effect
(and
(when
(and
(Flag397-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-I
:parameters ()
:precondition
(and
(Flag375-)
(Flag375prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-F
:parameters ()
:precondition
(and
(Flag376-)
(Flag376prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-C
:parameters ()
:precondition
(and
(Flag378-)
(Flag378prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_O-A
:parameters ()
:precondition
(and
(Flag379-)
(Flag379prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(when
(and
(BOARDED-PASS_S)
)
(and
(SERVED-PASS_S)
(NOT-BOARDED-PASS_S)
(not (NOT-SERVED-PASS_S))
(not (BOARDED-PASS_S))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (SERVED-PASS_V))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_G))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-5
:parameters ()
:precondition
(and
(not (SERVED-PASS_B))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-6
:parameters ()
:precondition
(and
(not (SERVED-PASS_L))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-7
:parameters ()
:precondition
(and
(not (SERVED-PASS_N))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-A-C
:parameters ()
:precondition
(and
(LIFT_AT-A)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-A)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-A))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-C-E
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim40Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim41Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim43Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim44Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim67Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim87Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim88Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim91Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim92Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim109Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim111Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim112Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim133Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim135Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim140Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim157Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim159Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim160Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim161Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim163Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim181Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim183Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim184Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim187Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim188Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim209Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim211Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim212Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim229Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim231Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim232Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim233Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim235Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim256Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim257Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim259Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim260Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim277Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim279Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim280Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim283Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim301Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim303Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim304Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim305Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim307Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim308Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim325Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim327Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim328Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim329Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim331Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim332Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Prim349Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim351Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
)

)
(:action Prim352Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim353Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim355Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim356Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
)

)
(:action Flag372Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag372-)
(Flag372prime-)
)

)
(:action Prim372Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Flag374Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(Flag374-)
(Flag374prime-)
)

)
(:action Prim374Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-H))
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Flag375Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(Flag375-)
(Flag375prime-)
)

)
(:action Prim375Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Flag376Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag376-)
(Flag376prime-)
)

)
(:action Prim376Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Flag378Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(Flag378-)
(Flag378prime-)
)

)
(:action Prim378Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Flag379Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(Flag379-)
(Flag379prime-)
)

)
(:action Prim379Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-A))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action STOP-PASS_O-E
:parameters ()
:precondition
(and
(Flag377-)
(Flag377prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(when
(and
(BOARDED-PASS_N)
)
(and
(SERVED-PASS_N)
(NOT-BOARDED-PASS_N)
(not (NOT-SERVED-PASS_N))
(not (BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
(Flag396prime-)
)
:effect
(and
(when
(and
(Flag396-)
)
(and
(BOARDED-PASS_N)
(not (NOT-BOARDED-PASS_N))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_N-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_L-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
(Flag395prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_L)
)
(and
(SERVED-PASS_L)
(NOT-BOARDED-PASS_L)
(not (NOT-SERVED-PASS_L))
(not (BOARDED-PASS_L))
)
)
(when
(and
(Flag395-)
)
(and
(BOARDED-PASS_L)
(not (NOT-BOARDED-PASS_L))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_B-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(when
(and
(BOARDED-PASS_B)
)
(and
(SERVED-PASS_B)
(NOT-BOARDED-PASS_B)
(not (NOT-SERVED-PASS_B))
(not (BOARDED-PASS_B))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
(Flag393prime-)
)
:effect
(and
(when
(and
(Flag393-)
)
(and
(BOARDED-PASS_G)
(not (NOT-BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(when
(and
(BOARDED-PASS_G)
)
(and
(SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
(not (NOT-SERVED-PASS_G))
(not (BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_G-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_D-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_S-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_V)
)
(and
(SERVED-PASS_V)
(NOT-BOARDED-PASS_V)
(not (NOT-SERVED-PASS_V))
(not (BOARDED-PASS_V))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
(Flag390prime-)
)
:effect
(and
(when
(and
(Flag390-)
)
(and
(BOARDED-PASS_V)
(not (NOT-BOARDED-PASS_V))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_V-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-H
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-H)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-E
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action STOP-PASS_T-A
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-A)
(Flag389prime-)
)
:effect
(and
(when
(and
(Flag389-)
)
(and
(BOARDED-PASS_T)
(not (NOT-BOARDED-PASS_T))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim29Action-23
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action Prim29Action-24
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag29prime-)
(not (Flag29-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-I-H
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-C-A
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-A)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-A))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-H-F
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-F-E
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-E)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-E))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEDOWN-E-C
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-H-I
:parameters ()
:precondition
(and
(LIFT_AT-H)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-H)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-H))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-F-H
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-H)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-H))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action MOVEUP-E-F
:parameters ()
:precondition
(and
(LIFT_AT-E)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-E)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-E))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag397prime-))
(not (Flag396prime-))
(not (Flag395prime-))
(not (Flag394prime-))
(not (Flag393prime-))
(not (Flag392prime-))
(not (Flag391prime-))
(not (Flag390prime-))
(not (Flag389prime-))
(not (Flag377prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag379prime-))
(not (Flag378prime-))
(not (Flag376prime-))
(not (Flag375prime-))
(not (Flag374prime-))
(not (Flag372prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag373prime-))
(not (Flag371prime-))
(not (Flag369prime-))
(not (Flag370prime-))
(not (Flag368prime-))
(not (Flag367prime-))
(not (Flag366prime-))
(not (Flag29prime-))
)
)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag366prime-)
(not (Flag366-))
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag367prime-)
(not (Flag367-))
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag368prime-)
(not (Flag368-))
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag369prime-)
(not (Flag369-))
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag370prime-)
(not (Flag370-))
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag371prime-)
(not (Flag371-))
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag372prime-)
(not (Flag372-))
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag373prime-)
(not (Flag373-))
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag374prime-)
(not (Flag374-))
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag375prime-)
(not (Flag375-))
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag376prime-)
(not (Flag376-))
)

)
(:action Flag377Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-E)
)
:effect
(and
(Flag377-)
(Flag377prime-)
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim377Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-E))
)
:effect
(and
(Flag377prime-)
(not (Flag377-))
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag378prime-)
(not (Flag378-))
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag379prime-)
(not (Flag379-))
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag389Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_T)
(NOT-SERVED-PASS_T)
)
:effect
(and
(Flag389-)
(Flag389prime-)
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_T))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_T))
)
:effect
(and
(Flag389prime-)
(not (Flag389-))
)

)
(:action Flag390Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_V)
(NOT-SERVED-PASS_V)
)
:effect
(and
(Flag390-)
(Flag390prime-)
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_V))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_V))
)
:effect
(and
(Flag390prime-)
(not (Flag390-))
)

)
(:action Flag391Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_S)
(NOT-SERVED-PASS_S)
)
:effect
(and
(Flag391-)
(Flag391prime-)
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_S))
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_S))
)
:effect
(and
(Flag391prime-)
(not (Flag391-))
)

)
(:action Flag392Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_D)
(NOT-SERVED-PASS_D)
)
:effect
(and
(Flag392-)
(Flag392prime-)
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_D))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_D))
)
:effect
(and
(Flag392prime-)
(not (Flag392-))
)

)
(:action Flag393Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_G)
(NOT-SERVED-PASS_G)
)
:effect
(and
(Flag393-)
(Flag393prime-)
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_G))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_G))
)
:effect
(and
(Flag393prime-)
(not (Flag393-))
)

)
(:action Flag394Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_B)
(NOT-SERVED-PASS_B)
)
:effect
(and
(Flag394-)
(Flag394prime-)
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_B))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_B))
)
:effect
(and
(Flag394prime-)
(not (Flag394-))
)

)
(:action Flag395Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_L)
(NOT-SERVED-PASS_L)
)
:effect
(and
(Flag395-)
(Flag395prime-)
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_L))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_L))
)
:effect
(and
(Flag395prime-)
(not (Flag395-))
)

)
(:action Flag396Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_N)
(NOT-SERVED-PASS_N)
)
:effect
(and
(Flag396-)
(Flag396prime-)
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_N))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_N))
)
:effect
(and
(Flag396prime-)
(not (Flag396-))
)

)
(:action Flag397Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_O)
(NOT-SERVED-PASS_O)
)
:effect
(and
(Flag397-)
(Flag397prime-)
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_O))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_O))
)
:effect
(and
(Flag397prime-)
(not (Flag397-))
)

)
)
