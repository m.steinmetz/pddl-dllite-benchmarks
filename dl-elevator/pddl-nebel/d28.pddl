(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag567-)
(Flag566-)
(Flag565-)
(Flag564-)
(Flag563-)
(Flag562-)
(Flag561-)
(Flag560-)
(Flag559-)
(Flag558-)
(Flag557-)
(Flag121-)
(NOT-LIFT_AT-AA)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(LIFT_AT-AC)
(LIFT_AT-AE)
(ERROR-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_AY)
(BOARDED-PASS_AT)
(SERVED-PASS_AT)
(SERVED-PASS_AP)
(BOARDED-PASS_AJ)
(BOARDED-PASS_AK)
(BOARDED-PASS_AD)
(Flag120-)
(Flag119-)
(Flag104-)
(LIFT_AT-AF)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_AR)
(Flag101-)
(LIFT_AT-AG)
(LIFT_AT-AH)
(LIFT_AT-AI)
(SERVED-PASS_AW)
(SERVED-PASS_AR)
(BOARDED-PASS_AS)
(SERVED-PASS_AS)
(SERVED-PASS_AK)
(Flag117-)
(Flag115-)
(Flag100-)
(LIFT_AT-AL)
(LIFT_AT-AM)
(LIFT_AT-AN)
(BOARDED-PASS_AZ)
(BOARDED-PASS_BB)
(Flag109-)
(Flag98-)
(Flag97-)
(LIFT_AT-AO)
(LIFT_AT-AQ)
(SERVED-PASS_AD)
(Flag108-)
(Flag102-)
(LIFT_AT-AU)
(LIFT_AT-AV)
(SERVED-PASS_AZ)
(BOARDED-PASS_AY)
(BOARDED-PASS_AP)
(SERVED-PASS_BB)
(Flag105-)
(Flag94-)
(LIFT_AT-AX)
(LIFT_AT-BA)
(BOARDED-PASS_AW)
(SERVED-PASS_AJ)
(Flag99-)
(Flag95-)
(Flag35-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AV)
(NOT-SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AW)
(NOT-LIFT_AT-BA)
(NOT-LIFT_AT-AU)
(NOT-LIFT_AT-AQ)
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_AP)
(NOT-BOARDED-PASS_AY)
(NOT-SERVED-PASS_AZ)
(NOT-LIFT_AT-AO)
(NOT-LIFT_AT-AN)
(NOT-SERVED-PASS_AD)
(NOT-LIFT_AT-AM)
(NOT-LIFT_AT-AL)
(NOT-LIFT_AT-AI)
(NOT-BOARDED-PASS_BB)
(NOT-BOARDED-PASS_AZ)
(NOT-LIFT_AT-AH)
(NOT-LIFT_AT-AG)
(NOT-LIFT_AT-AF)
(NOT-SERVED-PASS_AK)
(NOT-SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(NOT-SERVED-PASS_AR)
(NOT-SERVED-PASS_AW)
(NOT-LIFT_AT-AE)
(NOT-BOARDED-PASS_AR)
(NOT-LIFT_AT-AC)
(NOT-LIFT_AT-AB)
(NOT-BOARDED-PASS_AD)
(NOT-BOARDED-PASS_AK)
(NOT-BOARDED-PASS_AJ)
(NOT-SERVED-PASS_AP)
(NOT-SERVED-PASS_AT)
(NOT-BOARDED-PASS_AT)
(NOT-SERVED-PASS_AY)
(NOT-ERROR-)
(LIFT_AT-AA)
(Flag567prime-)
(Flag566prime-)
(Flag565prime-)
(Flag564prime-)
(Flag563prime-)
(Flag562prime-)
(Flag561prime-)
(Flag560prime-)
(Flag559prime-)
(Flag558prime-)
(Flag557prime-)
(Flag121prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag120prime-)
(Flag119prime-)
(Flag104prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag101prime-)
(Flag117prime-)
(Flag115prime-)
(Flag100prime-)
(Flag109prime-)
(Flag98prime-)
(Flag97prime-)
(Flag108prime-)
(Flag102prime-)
(Flag105prime-)
(Flag94prime-)
(Flag99prime-)
(Flag95prime-)
(Flag35prime-)
)
(:action STOP-PASS_BB-AX
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-BA
:parameters ()
:precondition
(and
(Flag99-)
(Flag99prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Flag35Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(SERVED-PASS_AZ)
(Flag8-)
(Flag8prime-)
(SERVED-PASS_AY)
(Flag10-)
(Flag10prime-)
(SERVED-PASS_AW)
(SERVED-PASS_AT)
(SERVED-PASS_AR)
(SERVED-PASS_AS)
(SERVED-PASS_AP)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(SERVED-PASS_AJ)
(SERVED-PASS_AK)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(SERVED-PASS_AD)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(SERVED-PASS_BB)
(Flag32-)
(Flag32prime-)
(Flag34-)
(Flag34prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_AJ))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEDOWN-BA-AX
:parameters ()
:precondition
(and
(LIFT_AT-BA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BA)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BA))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AX-AV
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim38Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim42Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim67Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim71Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag99Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(Flag99-)
(Flag99prime-)
)

)
(:action Prim99Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim124Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim128Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim153Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim157Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim182Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim186Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim211Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim215Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim240Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim244Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim269Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim273Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim298Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim302Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim327Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim331Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim356Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim360Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim385Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim389Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim414Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim418Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim443Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim447Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim472Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim476Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim501Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim505Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim530Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim534Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AU
:parameters ()
:precondition
(and
(Flag94-)
(Flag94prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AV
:parameters ()
:precondition
(and
(Flag105-)
(Flag105prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(BOARDED-PASS_AJ)
)
(and
(SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AJ)
(not (NOT-SERVED-PASS_AJ))
(not (BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
(Flag559prime-)
)
:effect
(and
(when
(and
(Flag559-)
)
(and
(BOARDED-PASS_AW)
(not (NOT-BOARDED-PASS_AW))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_AZ))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-25
:parameters ()
:precondition
(and
(not (SERVED-PASS_BB))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AX-BA
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-BA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-BA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AV-AX
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AV-AU
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AU-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim37Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim48Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim66Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim77Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Flag94Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(Flag94-)
(Flag94prime-)
)

)
(:action Prim94Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Flag105Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag105-)
(Flag105prime-)
)

)
(:action Prim105Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim123Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim134Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim152Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim163Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim181Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim192Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim210Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim221Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim239Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim250Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim268Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim279Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim297Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim308Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim326Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim337Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim355Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim366Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim384Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim395Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim413Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim424Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim442Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim453Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim471Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim482Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim500Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim511Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim529Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AU))
)
:effect
(and
)

)
(:action Prim540Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AO
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AQ
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_BB)
)
(and
(SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
(not (NOT-SERVED-PASS_BB))
(not (BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
(Flag563prime-)
)
:effect
(and
(when
(and
(Flag563-)
)
(and
(BOARDED-PASS_AP)
(not (NOT-BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
(Flag558prime-)
)
:effect
(and
(when
(and
(Flag558-)
)
(and
(BOARDED-PASS_AY)
(not (NOT-BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AU
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AU)
)
:effect
(and
(when
(and
(BOARDED-PASS_AZ)
)
(and
(SERVED-PASS_AZ)
(NOT-BOARDED-PASS_AZ)
(not (NOT-SERVED-PASS_AZ))
(not (BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_AD))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AU-AV
:parameters ()
:precondition
(and
(LIFT_AT-AU)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AU)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AU))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AQ-AU
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AU)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AU))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AQ-AO
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AO-AN
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim45Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim51Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim74Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim80Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim131Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim137Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim166Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim189Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim195Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim218Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim224Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim247Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim253Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim276Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim282Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim305Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim311Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim334Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim340Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim363Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim369Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim392Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim398Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim421Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim427Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim450Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim456Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim479Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim485Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim508Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim514Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim537Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim543Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AL
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AM
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AN
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(when
(and
(BOARDED-PASS_AD)
)
(and
(SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
(not (NOT-SERVED-PASS_AD))
(not (BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AO-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AN-AO
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AN-AM
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AM-AL
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AL-AI
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim40Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim41Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim52Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim69Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim70Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim81Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Prim109Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim126Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim127Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim138Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim155Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim156Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim167Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim184Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim185Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim196Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim213Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim214Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim225Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim242Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim243Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim254Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim271Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim272Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim283Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim300Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim301Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim312Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim329Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim330Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim341Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim358Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim359Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim370Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim387Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim388Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim399Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim416Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim417Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim428Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim445Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim446Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim457Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim474Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim475Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim486Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim503Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim504Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim515Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim532Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim533Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim544Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AH
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AI
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
(Flag567prime-)
)
:effect
(and
(when
(and
(Flag567-)
)
(and
(BOARDED-PASS_BB)
(not (NOT-BOARDED-PASS_BB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AG
:parameters ()
:precondition
(and
(Flag117-)
(Flag117prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
(Flag557prime-)
)
:effect
(and
(when
(and
(Flag557-)
)
(and
(BOARDED-PASS_AZ)
(not (NOT-BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-7
:parameters ()
:precondition
(and
(not (SERVED-PASS_AW))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-9
:parameters ()
:precondition
(and
(not (SERVED-PASS_AR))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-10
:parameters ()
:precondition
(and
(not (SERVED-PASS_AS))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_AK))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AM-AN
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AL-AM
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AI-AL
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AI-AH
:parameters ()
:precondition
(and
(LIFT_AT-AI)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AI)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AI))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AH-AG
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AG-AF
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim43Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim58Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim60Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim72Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim87Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim89Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Prim100Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Flag117Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(Flag117-)
(Flag117prime-)
)

)
(:action Prim117Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim129Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim144Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim146Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim158Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim173Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim175Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim187Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim202Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim204Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim216Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim231Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim233Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim245Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim260Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim262Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim274Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim289Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim291Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim303Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim318Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim320Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim332Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim347Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim349Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim361Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim376Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim378Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim390Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim405Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim407Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim419Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim434Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim436Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim448Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim463Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim465Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim477Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim492Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim494Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim506Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim521Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim523Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim535Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AH))
)
:effect
(and
)

)
(:action Prim550Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AI))
)
:effect
(and
)

)
(:action Prim552Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AF
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(when
(and
(BOARDED-PASS_AK)
)
(and
(SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
(not (NOT-SERVED-PASS_AK))
(not (BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(when
(and
(BOARDED-PASS_AS)
)
(and
(SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
(not (NOT-SERVED-PASS_AS))
(not (BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag562prime-)
)
:effect
(and
(when
(and
(Flag562-)
)
(and
(BOARDED-PASS_AS)
(not (NOT-BOARDED-PASS_AS))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(when
(and
(BOARDED-PASS_AR)
)
(and
(SERVED-PASS_AR)
(NOT-BOARDED-PASS_AR)
(not (NOT-SERVED-PASS_AR))
(not (BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(when
(and
(BOARDED-PASS_AW)
)
(and
(SERVED-PASS_AW)
(NOT-BOARDED-PASS_AW)
(not (NOT-SERVED-PASS_AW))
(not (BOARDED-PASS_AW))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AH
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AH)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AI
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AI)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-4
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-6
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-12
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-13
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-16
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-17
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-19
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-20
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-21
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-22
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-23
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-24
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-26
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-27
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AH-AI
:parameters ()
:precondition
(and
(LIFT_AT-AH)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AH)
(LIFT_AT-AI)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AH))
(not (NOT-LIFT_AT-AI))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AG-AH
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AH)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AH))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AF-AG
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AF-AE
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim44Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim73Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim130Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim159Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim188Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim217Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim246Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim275Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim304Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim333Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim362Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim391Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim420Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim449Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim478Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim507Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim536Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AE
:parameters ()
:precondition
(and
(Flag104-)
(Flag104prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AB
:parameters ()
:precondition
(and
(Flag119-)
(Flag119prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_BB-AC
:parameters ()
:precondition
(and
(Flag120-)
(Flag120prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
(Flag561prime-)
)
:effect
(and
(when
(and
(Flag561-)
)
(and
(BOARDED-PASS_AR)
(not (NOT-BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Flag2Action-1
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-1
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-1
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-1
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-1
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-1
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-1
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-1
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-1
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-1
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-1
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-1
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-1
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-1
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-1
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-1
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-1
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Prim35Action-5
:parameters ()
:precondition
(and
(not (SERVED-PASS_AY))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_AT))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-11
:parameters ()
:precondition
(and
(not (SERVED-PASS_AP))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AE-AF
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AE-AC
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AC-AB
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AB-AA
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim47Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim62Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim63Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim76Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim91Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim92Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Flag104Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(Flag104-)
(Flag104prime-)
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Flag119Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(Flag119-)
(Flag119prime-)
)

)
(:action Prim119Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Flag120Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(Flag120-)
(Flag120prime-)
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim133Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim148Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim149Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim162Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim177Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim178Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim191Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim206Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim207Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim220Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim235Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim236Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim249Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim264Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim265Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim278Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim293Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim294Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim307Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim322Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim323Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim336Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim351Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim352Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim365Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim380Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim381Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim394Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim409Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim410Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim423Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim438Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim439Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim452Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim467Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim468Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim481Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim496Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim497Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim510Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim525Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim526Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim539Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim554Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim555Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action STOP-PASS_BB-AA
:parameters ()
:precondition
(and
(Flag121-)
(Flag121prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
(Flag566prime-)
)
:effect
(and
(when
(and
(Flag566-)
)
(and
(BOARDED-PASS_AD)
(not (NOT-BOARDED-PASS_AD))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AD-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
(Flag565prime-)
)
:effect
(and
(when
(and
(Flag565-)
)
(and
(BOARDED-PASS_AK)
(not (NOT-BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
(Flag564prime-)
)
:effect
(and
(when
(and
(Flag564-)
)
(and
(BOARDED-PASS_AJ)
(not (NOT-BOARDED-PASS_AJ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AJ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(when
(and
(BOARDED-PASS_AP)
)
(and
(SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
(not (NOT-SERVED-PASS_AP))
(not (BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AP-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AS-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AR-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(when
(and
(BOARDED-PASS_AT)
)
(and
(SERVED-PASS_AT)
(NOT-BOARDED-PASS_AT)
(not (NOT-SERVED-PASS_AT))
(not (BOARDED-PASS_AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AT-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
(Flag560prime-)
)
:effect
(and
(when
(and
(Flag560-)
)
(and
(BOARDED-PASS_AT)
(not (NOT-BOARDED-PASS_AT))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AW-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(when
(and
(BOARDED-PASS_AY)
)
(and
(SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
(not (NOT-SERVED-PASS_AY))
(not (BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim35Action-28
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-29
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AC-AE
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AB-AC
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AA-AB
:parameters ()
:precondition
(and
(LIFT_AT-AA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AA)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AA))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag567prime-))
(not (Flag566prime-))
(not (Flag565prime-))
(not (Flag564prime-))
(not (Flag563prime-))
(not (Flag562prime-))
(not (Flag561prime-))
(not (Flag560prime-))
(not (Flag559prime-))
(not (Flag558prime-))
(not (Flag557prime-))
(not (Flag121prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag120prime-))
(not (Flag119prime-))
(not (Flag104prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag101prime-))
(not (Flag117prime-))
(not (Flag115prime-))
(not (Flag100prime-))
(not (Flag109prime-))
(not (Flag98prime-))
(not (Flag97prime-))
(not (Flag108prime-))
(not (Flag102prime-))
(not (Flag105prime-))
(not (Flag94prime-))
(not (Flag99prime-))
(not (Flag95prime-))
(not (Flag35prime-))
)
)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim93Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag94prime-)
(not (Flag94-))
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag99prime-)
(not (Flag99-))
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag104prime-)
(not (Flag104-))
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag105prime-)
(not (Flag105-))
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag117prime-)
(not (Flag117-))
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag119prime-)
(not (Flag119-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag120prime-)
(not (Flag120-))
)

)
(:action Flag121Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(Flag121-)
(Flag121prime-)
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim121Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
(Flag121prime-)
(not (Flag121-))
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim455Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim455Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim533Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim533Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim551Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim551Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Flag557Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AZ)
(NOT-BOARDED-PASS_AZ)
)
:effect
(and
(Flag557-)
(Flag557prime-)
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AZ))
)
:effect
(and
(Flag557prime-)
(not (Flag557-))
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AZ))
)
:effect
(and
(Flag557prime-)
(not (Flag557-))
)

)
(:action Flag558Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
)
:effect
(and
(Flag558-)
(Flag558prime-)
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AY))
)
:effect
(and
(Flag558prime-)
(not (Flag558-))
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AY))
)
:effect
(and
(Flag558prime-)
(not (Flag558-))
)

)
(:action Flag559Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AW)
(NOT-BOARDED-PASS_AW)
)
:effect
(and
(Flag559-)
(Flag559prime-)
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AW))
)
:effect
(and
(Flag559prime-)
(not (Flag559-))
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AW))
)
:effect
(and
(Flag559prime-)
(not (Flag559-))
)

)
(:action Flag560Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AT)
(NOT-BOARDED-PASS_AT)
)
:effect
(and
(Flag560-)
(Flag560prime-)
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AT))
)
:effect
(and
(Flag560prime-)
(not (Flag560-))
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AT))
)
:effect
(and
(Flag560prime-)
(not (Flag560-))
)

)
(:action Flag561Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AR)
(NOT-BOARDED-PASS_AR)
)
:effect
(and
(Flag561-)
(Flag561prime-)
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AR))
)
:effect
(and
(Flag561prime-)
(not (Flag561-))
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AR))
)
:effect
(and
(Flag561prime-)
(not (Flag561-))
)

)
(:action Flag562Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AS)
(NOT-BOARDED-PASS_AS)
)
:effect
(and
(Flag562-)
(Flag562prime-)
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AS))
)
:effect
(and
(Flag562prime-)
(not (Flag562-))
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AS))
)
:effect
(and
(Flag562prime-)
(not (Flag562-))
)

)
(:action Flag563Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
)
:effect
(and
(Flag563-)
(Flag563prime-)
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AP))
)
:effect
(and
(Flag563prime-)
(not (Flag563-))
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AP))
)
:effect
(and
(Flag563prime-)
(not (Flag563-))
)

)
(:action Flag564Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AJ)
(NOT-BOARDED-PASS_AJ)
)
:effect
(and
(Flag564-)
(Flag564prime-)
)

)
(:action Prim564Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AJ))
)
:effect
(and
(Flag564prime-)
(not (Flag564-))
)

)
(:action Prim564Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AJ))
)
:effect
(and
(Flag564prime-)
(not (Flag564-))
)

)
(:action Flag565Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
)
:effect
(and
(Flag565-)
(Flag565prime-)
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AK))
)
:effect
(and
(Flag565prime-)
(not (Flag565-))
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AK))
)
:effect
(and
(Flag565prime-)
(not (Flag565-))
)

)
(:action Flag566Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AD)
(NOT-BOARDED-PASS_AD)
)
:effect
(and
(Flag566-)
(Flag566prime-)
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AD))
)
:effect
(and
(Flag566prime-)
(not (Flag566-))
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AD))
)
:effect
(and
(Flag566prime-)
(not (Flag566-))
)

)
(:action Flag567Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BB)
(NOT-BOARDED-PASS_BB)
)
:effect
(and
(Flag567-)
(Flag567prime-)
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BB))
)
:effect
(and
(Flag567prime-)
(not (Flag567-))
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BB))
)
:effect
(and
(Flag567prime-)
(not (Flag567-))
)

)
)
