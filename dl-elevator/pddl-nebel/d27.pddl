(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag548-)
(Flag547-)
(Flag546-)
(Flag545-)
(Flag544-)
(Flag543-)
(Flag542-)
(Flag541-)
(Flag540-)
(Flag539-)
(Flag115-)
(NOT-LIFT_AT-AG)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(LIFT_AT-AJ)
(LIFT_AT-AL)
(ERROR-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(BOARDED-PASS_AI)
(Flag113-)
(Flag111-)
(Flag101-)
(LIFT_AT-AD)
(LIFT_AT-AA)
(LIFT_AT-AM)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_AI)
(SERVED-PASS_AB)
(Flag118-)
(Flag116-)
(Flag112-)
(LIFT_AT-AO)
(LIFT_AT-AP)
(BOARDED-PASS_AZ)
(BOARDED-PASS_AH)
(BOARDED-PASS_AB)
(SERVED-PASS_AC)
(BOARDED-PASS_AU)
(Flag110-)
(Flag108-)
(LIFT_AT-AQ)
(BOARDED-PASS_AK)
(SERVED-PASS_AF)
(SERVED-PASS_AN)
(Flag109-)
(LIFT_AT-AR)
(LIFT_AT-AS)
(LIFT_AT-AT)
(SERVED-PASS_AZ)
(BOARDED-PASS_AY)
(SERVED-PASS_AK)
(BOARDED-PASS_AF)
(SERVED-PASS_AU)
(Flag107-)
(Flag106-)
(Flag92-)
(LIFT_AT-AV)
(SERVED-PASS_AH)
(Flag98-)
(LIFT_AT-AW)
(LIFT_AT-AX)
(LIFT_AT-BA)
(SERVED-PASS_AY)
(BOARDED-PASS_AC)
(BOARDED-PASS_AN)
(Flag96-)
(Flag95-)
(Flag93-)
(Flag35-)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-AX)
(NOT-LIFT_AT-AW)
(NOT-LIFT_AT-AV)
(NOT-BOARDED-PASS_AN)
(NOT-BOARDED-PASS_AC)
(NOT-SERVED-PASS_AY)
(NOT-LIFT_AT-BA)
(NOT-LIFT_AT-AT)
(NOT-SERVED-PASS_AH)
(NOT-LIFT_AT-AS)
(NOT-LIFT_AT-AR)
(NOT-LIFT_AT-AQ)
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AF)
(NOT-SERVED-PASS_AK)
(NOT-BOARDED-PASS_AY)
(NOT-SERVED-PASS_AZ)
(NOT-LIFT_AT-AP)
(NOT-SERVED-PASS_AN)
(NOT-SERVED-PASS_AF)
(NOT-BOARDED-PASS_AK)
(NOT-LIFT_AT-AO)
(NOT-LIFT_AT-AM)
(NOT-BOARDED-PASS_AU)
(NOT-SERVED-PASS_AC)
(NOT-BOARDED-PASS_AB)
(NOT-BOARDED-PASS_AH)
(NOT-BOARDED-PASS_AZ)
(NOT-LIFT_AT-AL)
(NOT-SERVED-PASS_AB)
(NOT-SERVED-PASS_AI)
(NOT-LIFT_AT-AD)
(NOT-LIFT_AT-AE)
(NOT-LIFT_AT-AJ)
(NOT-LIFT_AT-AA)
(NOT-BOARDED-PASS_AI)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag548prime-)
(Flag547prime-)
(Flag546prime-)
(Flag545prime-)
(Flag544prime-)
(Flag543prime-)
(Flag542prime-)
(Flag541prime-)
(Flag540prime-)
(Flag539prime-)
(Flag115prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag113prime-)
(Flag111prime-)
(Flag101prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag118prime-)
(Flag116prime-)
(Flag112prime-)
(Flag110prime-)
(Flag108prime-)
(Flag109prime-)
(Flag107prime-)
(Flag106prime-)
(Flag92prime-)
(Flag98prime-)
(Flag96prime-)
(Flag95prime-)
(Flag93prime-)
(Flag35prime-)
)
(:action STOP-PASS_AN-AX
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AW
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-BA
:parameters ()
:precondition
(and
(Flag96-)
(Flag96prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Flag35Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(SERVED-PASS_AZ)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(SERVED-PASS_AY)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(Flag20-)
(Flag20prime-)
(Flag22-)
(Flag22prime-)
(SERVED-PASS_AK)
(SERVED-PASS_AH)
(SERVED-PASS_AI)
(SERVED-PASS_AF)
(Flag24-)
(Flag24prime-)
(SERVED-PASS_AB)
(SERVED-PASS_AC)
(Flag26-)
(Flag26prime-)
(SERVED-PASS_AU)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(SERVED-PASS_AN)
(Flag32-)
(Flag32prime-)
(Flag34-)
(Flag34prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Prim35Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_AY))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEDOWN-BA-AX
:parameters ()
:precondition
(and
(LIFT_AT-BA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BA)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BA))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AX-AW
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim38Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim40Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim41Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim68Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Prim93Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag96Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(Flag96-)
(Flag96prime-)
)

)
(:action Prim96Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim121Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim124Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim149Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim151Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim152Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim179Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim205Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim233Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim235Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim261Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim263Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim264Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim289Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim291Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim292Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim317Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim319Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim320Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim345Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim347Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim348Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim373Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim375Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim376Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim401Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim403Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim404Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim429Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim431Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim432Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim457Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim459Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim460Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim485Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim487Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim488Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action Prim513Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim515Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim516Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BA))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AV
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
(Flag548prime-)
)
:effect
(and
(when
(and
(Flag548-)
)
(and
(BOARDED-PASS_AN)
(not (NOT-BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
(Flag546prime-)
)
:effect
(and
(when
(and
(Flag546-)
)
(and
(BOARDED-PASS_AC)
(not (NOT-BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(when
(and
(BOARDED-PASS_AY)
)
(and
(SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
(not (NOT-SERVED-PASS_AY))
(not (BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-BA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_AH))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AX-BA
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-BA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-BA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AW-AX
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AV-AT
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim43Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim71Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim126Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim154Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim182Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim210Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim238Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim266Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim294Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim322Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim350Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim378Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim406Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim434Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim462Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim490Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim518Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AT
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AR
:parameters ()
:precondition
(and
(Flag106-)
(Flag106prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AS
:parameters ()
:precondition
(and
(Flag107-)
(Flag107prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(BOARDED-PASS_AH)
)
(and
(SERVED-PASS_AH)
(NOT-BOARDED-PASS_AH)
(not (NOT-SERVED-PASS_AH))
(not (BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (SERVED-PASS_AZ))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-13
:parameters ()
:precondition
(and
(not (SERVED-PASS_AK))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-21
:parameters ()
:precondition
(and
(not (SERVED-PASS_AU))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AT-AV
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AT-AS
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AS-AR
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AR-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim37Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim51Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim52Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim80Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Prim92Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Flag106Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(Flag106-)
(Flag106prime-)
)

)
(:action Prim106Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Flag107Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(Flag107-)
(Flag107prime-)
)

)
(:action Prim107Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim120Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim135Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim163Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim190Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim191Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim218Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim219Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim232Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim246Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim247Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim260Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim275Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim288Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim302Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim303Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim316Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim330Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim331Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim344Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim358Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim359Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim372Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim386Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim387Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim400Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim414Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim415Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim428Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim442Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim443Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim456Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim470Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim471Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim484Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim498Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim499Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim512Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim526Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AR))
)
:effect
(and
)

)
(:action Prim527Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AQ
:parameters ()
:precondition
(and
(Flag109-)
(Flag109prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(when
(and
(BOARDED-PASS_AU)
)
(and
(SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
(not (NOT-SERVED-PASS_AU))
(not (BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
(Flag544prime-)
)
:effect
(and
(when
(and
(Flag544-)
)
(and
(BOARDED-PASS_AF)
(not (NOT-BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(when
(and
(BOARDED-PASS_AK)
)
(and
(SERVED-PASS_AK)
(NOT-BOARDED-PASS_AK)
(not (NOT-SERVED-PASS_AK))
(not (BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
(Flag540prime-)
)
:effect
(and
(when
(and
(Flag540-)
)
(and
(BOARDED-PASS_AY)
(not (NOT-BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(when
(and
(BOARDED-PASS_AZ)
)
(and
(SERVED-PASS_AZ)
(NOT-BOARDED-PASS_AZ)
(not (NOT-SERVED-PASS_AZ))
(not (BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AR
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AR)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_AF))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-24
:parameters ()
:precondition
(and
(not (SERVED-PASS_AN))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AS-AT
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AR-AS
:parameters ()
:precondition
(and
(LIFT_AT-AR)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AR)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AR))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AQ-AR
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AR)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AR))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AQ-AP
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim54Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Flag109Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag109-)
(Flag109prime-)
)

)
(:action Prim109Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim137Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim165Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim193Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim221Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim277Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim305Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim333Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim361Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim389Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim417Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim445Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim473Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim501Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim529Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AP
:parameters ()
:precondition
(and
(Flag108-)
(Flag108prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AO
:parameters ()
:precondition
(and
(Flag110-)
(Flag110prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_AN)
)
(and
(SERVED-PASS_AN)
(NOT-BOARDED-PASS_AN)
(not (NOT-SERVED-PASS_AN))
(not (BOARDED-PASS_AN))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AF)
)
(and
(SERVED-PASS_AF)
(NOT-BOARDED-PASS_AF)
(not (NOT-SERVED-PASS_AF))
(not (BOARDED-PASS_AF))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag541prime-)
)
:effect
(and
(when
(and
(Flag541-)
)
(and
(BOARDED-PASS_AK)
(not (NOT-BOARDED-PASS_AK))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AC))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AP-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AP-AO
:parameters ()
:precondition
(and
(LIFT_AT-AP)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AP)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AP))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AO-AM
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim83Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Flag108Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(Flag108-)
(Flag108prime-)
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Flag110Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(Flag110-)
(Flag110prime-)
)

)
(:action Prim110Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim166Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim192Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim194Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim220Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim222Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim248Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim250Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim278Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim304Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim306Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim332Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim334Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim360Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim362Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim388Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim390Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim416Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim418Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim444Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim446Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim472Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim474Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim500Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim502Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim528Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AP))
)
:effect
(and
)

)
(:action Prim530Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AM
:parameters ()
:precondition
(and
(Flag112-)
(Flag112prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AD
:parameters ()
:precondition
(and
(Flag116-)
(Flag116prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AA
:parameters ()
:precondition
(and
(Flag118-)
(Flag118prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
(Flag547prime-)
)
:effect
(and
(when
(and
(Flag547-)
)
(and
(BOARDED-PASS_AU)
(not (NOT-BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(when
(and
(BOARDED-PASS_AC)
)
(and
(SERVED-PASS_AC)
(NOT-BOARDED-PASS_AC)
(not (NOT-SERVED-PASS_AC))
(not (BOARDED-PASS_AC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
(Flag545prime-)
)
:effect
(and
(when
(and
(Flag545-)
)
(and
(BOARDED-PASS_AB)
(not (NOT-BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
(Flag542prime-)
)
:effect
(and
(when
(and
(Flag542-)
)
(and
(BOARDED-PASS_AH)
(not (NOT-BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AP
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AP)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
(Flag539prime-)
)
:effect
(and
(when
(and
(Flag539-)
)
(and
(BOARDED-PASS_AZ)
(not (NOT-BOARDED-PASS_AZ))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-2
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-3
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-5
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-6
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-7
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-8
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-9
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-10
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-11
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-12
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_AI))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-17
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_AB))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-20
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-22
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-23
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-25
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-26
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action MOVEUP-AO-AP
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AP)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AP))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AM-AO
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AM-AL
:parameters ()
:precondition
(and
(LIFT_AT-AM)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AM)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AM))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim85Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim91Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Flag112Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(Flag112-)
(Flag112prime-)
)

)
(:action Prim112Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Flag116Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(Flag116-)
(Flag116prime-)
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Flag118Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(Flag118-)
(Flag118prime-)
)

)
(:action Prim118Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim140Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim146Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim168Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim172Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim174Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim196Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim224Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim230Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim252Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim256Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim258Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim280Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim286Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim308Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim312Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim314Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim336Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim340Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim342Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim364Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim368Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim370Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim392Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim396Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim398Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim420Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim424Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim426Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim448Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim452Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim454Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim476Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim480Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim482Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim504Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim508Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim510Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action Prim532Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AM))
)
:effect
(and
)

)
(:action Prim536Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim538Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AA))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AE
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AL
:parameters ()
:precondition
(and
(Flag111-)
(Flag111prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AN-AJ
:parameters ()
:precondition
(and
(Flag113-)
(Flag113prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(when
(and
(BOARDED-PASS_AB)
)
(and
(SERVED-PASS_AB)
(NOT-BOARDED-PASS_AB)
(not (NOT-SERVED-PASS_AB))
(not (BOARDED-PASS_AB))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(when
(and
(BOARDED-PASS_AI)
)
(and
(SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
(not (NOT-SERVED-PASS_AI))
(not (BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AM
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AM)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AA
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AA)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action MOVEUP-AL-AM
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AM)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AM))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AA-AD
:parameters ()
:precondition
(and
(LIFT_AT-AA)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AA)
(LIFT_AT-AD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AA))
(not (NOT-LIFT_AT-AD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AD-AE
:parameters ()
:precondition
(and
(LIFT_AT-AD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AD)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AD))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AL-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AD-AA
:parameters ()
:precondition
(and
(LIFT_AT-AD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AD)
(LIFT_AT-AA)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AD))
(not (NOT-LIFT_AT-AA))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AJ-AG
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AE-AD
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim46Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim56Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim86Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Flag111Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(Flag111-)
(Flag111prime-)
)

)
(:action Prim111Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Flag113Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(Flag113-)
(Flag113prime-)
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim139Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim141Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim157Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim167Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim169Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim195Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim197Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim213Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim223Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim225Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim241Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim251Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim269Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim279Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim297Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim307Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim309Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim325Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim335Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim337Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim353Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim363Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim365Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim381Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim391Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim393Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim409Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim419Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim421Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim437Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim447Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim449Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim465Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim475Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim477Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim493Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim503Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim505Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim521Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AE))
)
:effect
(and
)

)
(:action Prim531Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim533Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action STOP-PASS_AN-AG
:parameters ()
:precondition
(and
(Flag115-)
(Flag115prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AU-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AC-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AB-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AF-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AI-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
(Flag543prime-)
)
:effect
(and
(when
(and
(Flag543-)
)
(and
(BOARDED-PASS_AI)
(not (NOT-BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AH-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AK-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AY-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action STOP-PASS_AZ-AG
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Prim35Action-27
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action Prim35Action-28
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag35prime-)
(not (Flag35-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AJ-AL
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AE-AG
:parameters ()
:precondition
(and
(LIFT_AT-AE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AE)
(LIFT_AT-AG)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AE))
(not (NOT-LIFT_AT-AG))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEUP-AG-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action MOVEDOWN-AG-AE
:parameters ()
:precondition
(and
(LIFT_AT-AG)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AG)
(LIFT_AT-AE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AG))
(not (NOT-LIFT_AT-AE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag548prime-))
(not (Flag547prime-))
(not (Flag546prime-))
(not (Flag545prime-))
(not (Flag544prime-))
(not (Flag543prime-))
(not (Flag542prime-))
(not (Flag541prime-))
(not (Flag540prime-))
(not (Flag539prime-))
(not (Flag115prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag113prime-))
(not (Flag111prime-))
(not (Flag101prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag118prime-))
(not (Flag116prime-))
(not (Flag112prime-))
(not (Flag110prime-))
(not (Flag108prime-))
(not (Flag109prime-))
(not (Flag107prime-))
(not (Flag106prime-))
(not (Flag92prime-))
(not (Flag98prime-))
(not (Flag96prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag35prime-))
)
)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag96prime-)
(not (Flag96-))
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag106prime-)
(not (Flag106-))
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag107prime-)
(not (Flag107-))
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag108prime-)
(not (Flag108-))
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag109prime-)
(not (Flag109-))
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag110prime-)
(not (Flag110-))
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag111prime-)
(not (Flag111-))
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag112prime-)
(not (Flag112-))
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag113prime-)
(not (Flag113-))
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag115Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AG)
)
:effect
(and
(Flag115-)
(Flag115prime-)
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim115Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
(Flag115prime-)
(not (Flag115-))
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag116prime-)
(not (Flag116-))
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag118prime-)
(not (Flag118-))
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim359Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim359Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim367Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim391Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim391Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim423Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim423Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim487Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim487Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim519Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim519Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim533Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim533Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AG))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag539Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AZ)
(NOT-SERVED-PASS_AZ)
)
:effect
(and
(Flag539-)
(Flag539prime-)
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AZ))
)
:effect
(and
(Flag539prime-)
(not (Flag539-))
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AZ))
)
:effect
(and
(Flag539prime-)
(not (Flag539-))
)

)
(:action Flag540Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AY)
(NOT-SERVED-PASS_AY)
)
:effect
(and
(Flag540-)
(Flag540prime-)
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AY))
)
:effect
(and
(Flag540prime-)
(not (Flag540-))
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AY))
)
:effect
(and
(Flag540prime-)
(not (Flag540-))
)

)
(:action Flag541Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AK)
(NOT-SERVED-PASS_AK)
)
:effect
(and
(Flag541-)
(Flag541prime-)
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AK))
)
:effect
(and
(Flag541prime-)
(not (Flag541-))
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AK))
)
:effect
(and
(Flag541prime-)
(not (Flag541-))
)

)
(:action Flag542Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AH)
(NOT-SERVED-PASS_AH)
)
:effect
(and
(Flag542-)
(Flag542prime-)
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AH))
)
:effect
(and
(Flag542prime-)
(not (Flag542-))
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AH))
)
:effect
(and
(Flag542prime-)
(not (Flag542-))
)

)
(:action Flag543Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AI)
(NOT-SERVED-PASS_AI)
)
:effect
(and
(Flag543-)
(Flag543prime-)
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AI))
)
:effect
(and
(Flag543prime-)
(not (Flag543-))
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AI))
)
:effect
(and
(Flag543prime-)
(not (Flag543-))
)

)
(:action Flag544Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AF)
(NOT-SERVED-PASS_AF)
)
:effect
(and
(Flag544-)
(Flag544prime-)
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AF))
)
:effect
(and
(Flag544prime-)
(not (Flag544-))
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AF))
)
:effect
(and
(Flag544prime-)
(not (Flag544-))
)

)
(:action Flag545Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AB)
(NOT-SERVED-PASS_AB)
)
:effect
(and
(Flag545-)
(Flag545prime-)
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AB))
)
:effect
(and
(Flag545prime-)
(not (Flag545-))
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AB))
)
:effect
(and
(Flag545prime-)
(not (Flag545-))
)

)
(:action Flag546Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AC)
(NOT-SERVED-PASS_AC)
)
:effect
(and
(Flag546-)
(Flag546prime-)
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AC))
)
:effect
(and
(Flag546prime-)
(not (Flag546-))
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AC))
)
:effect
(and
(Flag546prime-)
(not (Flag546-))
)

)
(:action Flag547Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AU)
(NOT-SERVED-PASS_AU)
)
:effect
(and
(Flag547-)
(Flag547prime-)
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AU))
)
:effect
(and
(Flag547prime-)
(not (Flag547-))
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AU))
)
:effect
(and
(Flag547prime-)
(not (Flag547-))
)

)
(:action Flag548Action
:parameters ()
:precondition
(and
(NOT-BOARDED-PASS_AN)
(NOT-SERVED-PASS_AN)
)
:effect
(and
(Flag548-)
(Flag548prime-)
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AN))
)
:effect
(and
(Flag548prime-)
(not (Flag548-))
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AN))
)
:effect
(and
(Flag548prime-)
(not (Flag548-))
)

)
)
