(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag342-)
(Flag341-)
(Flag340-)
(Flag339-)
(Flag338-)
(Flag337-)
(Flag336-)
(Flag335-)
(Flag61-)
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(LIFT_AT-S)
(LIFT_AT-L)
(ERROR-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(SERVED-PASS_R)
(SERVED-PASS_E)
(BOARDED-PASS_E)
(BOARDED-PASS_O)
(Flag60-)
(Flag59-)
(Flag57-)
(LIFT_AT-T)
(LIFT_AT-U)
(LIFT_AT-K)
(LIFT_AT-J)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(BOARDED-PASS_P)
(SERVED-PASS_Q)
(BOARDED-PASS_G)
(SERVED-PASS_G)
(BOARDED-PASS_H)
(Flag63-)
(Flag62-)
(Flag55-)
(Flag53-)
(LIFT_AT-I)
(LIFT_AT-F)
(LIFT_AT-D)
(SERVED-PASS_P)
(BOARDED-PASS_R)
(BOARDED-PASS_A)
(SERVED-PASS_O)
(Flag65-)
(Flag64-)
(Flag50-)
(LIFT_AT-C)
(LIFT_AT-B)
(BOARDED-PASS_Q)
(SERVED-PASS_A)
(Flag70-)
(Flag69-)
(SERVED-PASS_H)
(Flag27-)
(NOT-SERVED-PASS_H)
(NOT-CHECKCONSISTENCY-)
(NOT-LIFT_AT-C)
(NOT-LIFT_AT-D)
(NOT-SERVED-PASS_A)
(NOT-BOARDED-PASS_Q)
(NOT-LIFT_AT-B)
(NOT-LIFT_AT-F)
(NOT-LIFT_AT-I)
(NOT-LIFT_AT-J)
(NOT-SERVED-PASS_O)
(NOT-BOARDED-PASS_A)
(NOT-BOARDED-PASS_R)
(NOT-SERVED-PASS_P)
(NOT-LIFT_AT-K)
(NOT-LIFT_AT-L)
(NOT-BOARDED-PASS_H)
(NOT-SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
(NOT-SERVED-PASS_Q)
(NOT-BOARDED-PASS_P)
(NOT-LIFT_AT-T)
(NOT-LIFT_AT-S)
(NOT-LIFT_AT-U)
(NOT-BOARDED-PASS_O)
(NOT-BOARDED-PASS_E)
(NOT-SERVED-PASS_E)
(NOT-SERVED-PASS_R)
(NOT-ERROR-)
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(Flag342prime-)
(Flag341prime-)
(Flag340prime-)
(Flag339prime-)
(Flag338prime-)
(Flag337prime-)
(Flag336prime-)
(Flag335prime-)
(Flag61prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag60prime-)
(Flag59prime-)
(Flag57prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag63prime-)
(Flag62prime-)
(Flag55prime-)
(Flag53prime-)
(Flag65prime-)
(Flag64prime-)
(Flag50prime-)
(Flag70prime-)
(Flag69prime-)
(Flag27prime-)
)
(:action Flag27Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(SERVED-PASS_P)
(SERVED-PASS_Q)
(SERVED-PASS_R)
(Flag6-)
(Flag6prime-)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(Flag12-)
(Flag12prime-)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(SERVED-PASS_E)
(Flag20-)
(Flag20prime-)
(SERVED-PASS_G)
(Flag22-)
(Flag22prime-)
(SERVED-PASS_A)
(Flag24-)
(Flag24prime-)
(SERVED-PASS_O)
(SERVED-PASS_H)
(Flag26-)
(Flag26prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Prim27Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_H))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action STOP-PASS_H-B
:parameters ()
:precondition
(and
(Flag69-)
(Flag69prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_H)
)
(and
(SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
(not (NOT-SERVED-PASS_H))
(not (BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-C
:parameters ()
:precondition
(and
(Flag70-)
(Flag70prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim27Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_A))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action MOVEUP-B-C
:parameters ()
:precondition
(and
(LIFT_AT-B)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-B)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-B))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-C-D
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim48Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim49Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Flag69Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(Flag69-)
(Flag69prime-)
)

)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Flag70Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(Flag70-)
(Flag70prime-)
)

)
(:action Prim70Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim91Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim92Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim113Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim114Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim135Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim136Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim157Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim158Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim179Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim180Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim201Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim202Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim223Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim224Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim245Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim246Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim267Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim268Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim289Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim290Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim311Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim312Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action Prim333Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-B))
)
:effect
(and
)

)
(:action Prim334Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-C))
)
:effect
(and
)

)
(:action STOP-PASS_H-D
:parameters ()
:precondition
(and
(Flag50-)
(Flag50prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-I
:parameters ()
:precondition
(and
(Flag64-)
(Flag64prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-F
:parameters ()
:precondition
(and
(Flag65-)
(Flag65prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(when
(and
(BOARDED-PASS_A)
)
(and
(SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
(not (NOT-SERVED-PASS_A))
(not (BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
(Flag336prime-)
)
:effect
(and
(when
(and
(Flag336-)
)
(and
(BOARDED-PASS_Q)
(not (NOT-BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-B
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-B)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-C
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-C)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim27Action-2
:parameters ()
:precondition
(and
(not (SERVED-PASS_P))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-18
:parameters ()
:precondition
(and
(not (SERVED-PASS_O))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action MOVEDOWN-C-B
:parameters ()
:precondition
(and
(LIFT_AT-C)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-C)
(LIFT_AT-B)
(CHECKCONSISTENCY-)
(not (LIFT_AT-C))
(not (NOT-LIFT_AT-B))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-D-C
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-C)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-C))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-D-F
:parameters ()
:precondition
(and
(LIFT_AT-D)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-D)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-D))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-F-I
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-I-J
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim29Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim43Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim44Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Flag50Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(Flag50-)
(Flag50prime-)
)

)
(:action Prim50Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Flag64Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(Flag64-)
(Flag64prime-)
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Flag65Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(Flag65-)
(Flag65prime-)
)

)
(:action Prim65Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim72Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim86Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim87Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim94Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim108Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim109Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim116Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim130Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim131Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim138Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim152Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim153Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim160Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim174Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim175Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim182Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim196Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim197Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim204Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim218Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim219Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim226Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim240Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim241Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim248Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim262Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim263Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim270Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim284Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim285Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim292Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim306Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim307Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action Prim314Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-D))
)
:effect
(and
)

)
(:action Prim328Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-I))
)
:effect
(and
)

)
(:action Prim329Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-F))
)
:effect
(and
)

)
(:action STOP-PASS_H-T
:parameters ()
:precondition
(and
(Flag53-)
(Flag53prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-U
:parameters ()
:precondition
(and
(Flag55-)
(Flag55prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-J
:parameters ()
:precondition
(and
(Flag62-)
(Flag62prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-K
:parameters ()
:precondition
(and
(Flag63-)
(Flag63prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_O)
)
(and
(SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
(not (NOT-SERVED-PASS_O))
(not (BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
(Flag340prime-)
)
:effect
(and
(when
(and
(Flag340-)
)
(and
(BOARDED-PASS_A)
(not (NOT-BOARDED-PASS_A))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
(Flag337prime-)
)
:effect
(and
(when
(and
(Flag337-)
)
(and
(BOARDED-PASS_R)
(not (NOT-BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-D
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-D)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-I
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-I)
)
:effect
(and
(when
(and
(BOARDED-PASS_P)
)
(and
(SERVED-PASS_P)
(NOT-BOARDED-PASS_P)
(not (NOT-SERVED-PASS_P))
(not (BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-F
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-F)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim27Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_Q))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-5
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-6
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-7
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-8
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-9
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-10
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-11
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-13
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_G))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-15
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-17
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-20
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action MOVEDOWN-F-D
:parameters ()
:precondition
(and
(LIFT_AT-F)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-F)
(LIFT_AT-D)
(CHECKCONSISTENCY-)
(not (LIFT_AT-F))
(not (NOT-LIFT_AT-D))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-I-F
:parameters ()
:precondition
(and
(LIFT_AT-I)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-I)
(LIFT_AT-F)
(CHECKCONSISTENCY-)
(not (LIFT_AT-I))
(not (NOT-LIFT_AT-F))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-J-I
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-I)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-I))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-J-K
:parameters ()
:precondition
(and
(LIFT_AT-J)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-J)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-J))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-K-L
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim32Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim34Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim41Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim42Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Flag53Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(Flag53-)
(Flag53prime-)
)

)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Flag55Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(Flag55-)
(Flag55prime-)
)

)
(:action Prim55Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Flag62Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(Flag62-)
(Flag62prime-)
)

)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Flag63Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(Flag63-)
(Flag63prime-)
)

)
(:action Prim63Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim75Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim77Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim84Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim85Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim97Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim99Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim106Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim107Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim119Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim121Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim128Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim129Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim141Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim143Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim150Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim151Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim163Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim165Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim172Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim173Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim185Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim187Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim194Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim195Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim207Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim209Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim216Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim217Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim229Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim231Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim238Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim239Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim251Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim253Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim260Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim261Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim273Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim275Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim282Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim283Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim295Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim297Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim304Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim305Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action Prim317Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-T))
)
:effect
(and
)

)
(:action Prim319Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-U))
)
:effect
(and
)

)
(:action Prim326Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-J))
)
:effect
(and
)

)
(:action Prim327Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-K))
)
:effect
(and
)

)
(:action STOP-PASS_H-S
:parameters ()
:precondition
(and
(Flag57-)
(Flag57prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-N
:parameters ()
:precondition
(and
(Flag59-)
(Flag59prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_H-L
:parameters ()
:precondition
(and
(Flag60-)
(Flag60prime-)
(Flag342prime-)
)
:effect
(and
(when
(and
(Flag342-)
)
(and
(BOARDED-PASS_H)
(not (NOT-BOARDED-PASS_H))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(when
(and
(BOARDED-PASS_G)
)
(and
(SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
(not (NOT-SERVED-PASS_G))
(not (BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
(Flag339prime-)
)
:effect
(and
(when
(and
(Flag339-)
)
(and
(BOARDED-PASS_G)
(not (NOT-BOARDED-PASS_G))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(when
(and
(BOARDED-PASS_Q)
)
(and
(SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
(not (NOT-SERVED-PASS_Q))
(not (BOARDED-PASS_Q))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-T
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-T)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-U
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-U)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-J
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-J)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-K
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-K)
(Flag335prime-)
)
:effect
(and
(when
(and
(Flag335-)
)
(and
(BOARDED-PASS_P)
(not (NOT-BOARDED-PASS_P))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Prim27Action-4
:parameters ()
:precondition
(and
(not (SERVED-PASS_R))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-12
:parameters ()
:precondition
(and
(not (SERVED-PASS_E))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action MOVEDOWN-K-J
:parameters ()
:precondition
(and
(LIFT_AT-K)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-K)
(LIFT_AT-J)
(CHECKCONSISTENCY-)
(not (LIFT_AT-K))
(not (NOT-LIFT_AT-J))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-U-T
:parameters ()
:precondition
(and
(LIFT_AT-U)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-U)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-U))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-T-S
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-L-K
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-K)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-K))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-L-M
:parameters ()
:precondition
(and
(LIFT_AT-L)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-L)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-L))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-T-U
:parameters ()
:precondition
(and
(LIFT_AT-T)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-T)
(LIFT_AT-U)
(CHECKCONSISTENCY-)
(not (LIFT_AT-T))
(not (NOT-LIFT_AT-U))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-S-T
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-T)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-T))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim36Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim38Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim39Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Flag57Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(Flag57-)
(Flag57prime-)
)

)
(:action Prim57Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Flag59Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(Flag59-)
(Flag59prime-)
)

)
(:action Prim59Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Flag60Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(Flag60-)
(Flag60prime-)
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim79Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim81Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim82Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim101Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim103Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim104Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim123Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim125Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim126Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim145Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim147Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim148Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim167Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim169Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim170Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim189Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim191Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim192Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim211Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim213Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim214Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim233Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim235Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim236Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim255Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim257Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim258Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim277Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim279Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim280Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim299Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim301Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim302Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action Prim321Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-S))
)
:effect
(and
)

)
(:action Prim323Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-N))
)
:effect
(and
)

)
(:action Prim324Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-L))
)
:effect
(and
)

)
(:action STOP-PASS_H-M
:parameters ()
:precondition
(and
(Flag61-)
(Flag61prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
(Flag341prime-)
)
:effect
(and
(when
(and
(Flag341-)
)
(and
(BOARDED-PASS_O)
(not (NOT-BOARDED-PASS_O))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_O-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_A-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_G-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
(Flag338prime-)
)
:effect
(and
(when
(and
(Flag338-)
)
(and
(BOARDED-PASS_E)
(not (NOT-BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_E-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_E)
)
(and
(SERVED-PASS_E)
(NOT-BOARDED-PASS_E)
(not (NOT-SERVED-PASS_E))
(not (BOARDED-PASS_E))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_R-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(when
(and
(BOARDED-PASS_R)
)
(and
(SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
(not (NOT-SERVED-PASS_R))
(not (BOARDED-PASS_R))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_Q-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-S
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-S)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-N
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-N)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-L
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-L)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action STOP-PASS_P-M
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Prim27Action-21
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action Prim27Action-22
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag27prime-)
(not (Flag27-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-S-N
:parameters ()
:precondition
(and
(LIFT_AT-S)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-S)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-S))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-M-L
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-L)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-L))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEDOWN-N-M
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-M)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-M))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-N-S
:parameters ()
:precondition
(and
(LIFT_AT-N)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-N)
(LIFT_AT-S)
(CHECKCONSISTENCY-)
(not (LIFT_AT-N))
(not (NOT-LIFT_AT-S))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action MOVEUP-M-N
:parameters ()
:precondition
(and
(LIFT_AT-M)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-M)
(LIFT_AT-N)
(CHECKCONSISTENCY-)
(not (LIFT_AT-M))
(not (NOT-LIFT_AT-N))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag342prime-))
(not (Flag341prime-))
(not (Flag340prime-))
(not (Flag339prime-))
(not (Flag338prime-))
(not (Flag337prime-))
(not (Flag336prime-))
(not (Flag335prime-))
(not (Flag61prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag60prime-))
(not (Flag59prime-))
(not (Flag57prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag63prime-))
(not (Flag62prime-))
(not (Flag55prime-))
(not (Flag53prime-))
(not (Flag65prime-))
(not (Flag64prime-))
(not (Flag50prime-))
(not (Flag70prime-))
(not (Flag69prime-))
(not (Flag27prime-))
)
)
(:action Prim29Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim29Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim30Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim30Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim31Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim31Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim32Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim32Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim33Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim33Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim34Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim34Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim35Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim35Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim36Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim36Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim37Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim37Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim38Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim38Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim40Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim40Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag50prime-)
(not (Flag50-))
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag53prime-)
(not (Flag53-))
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag55prime-)
(not (Flag55-))
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag57prime-)
(not (Flag57-))
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag59prime-)
(not (Flag59-))
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag60prime-)
(not (Flag60-))
)

)
(:action Flag61Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-M)
)
:effect
(and
(Flag61-)
(Flag61prime-)
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
(Flag61prime-)
(not (Flag61-))
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag62prime-)
(not (Flag62-))
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag63prime-)
(not (Flag63-))
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag64prime-)
(not (Flag64-))
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag65prime-)
(not (Flag65-))
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag69prime-)
(not (Flag69-))
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag70prime-)
(not (Flag70-))
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim83Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim103Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim103Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim135Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim135Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim167Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim167Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim199Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim199Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim231Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim231Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim263Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim263Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim295Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim295Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim303Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-3
:parameters ()
:precondition
(and
(not (LIFT_AT-M))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim327Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim327Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag335Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_P)
(NOT-BOARDED-PASS_P)
)
:effect
(and
(Flag335-)
(Flag335prime-)
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_P))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_P))
)
:effect
(and
(Flag335prime-)
(not (Flag335-))
)

)
(:action Flag336Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_Q)
(NOT-BOARDED-PASS_Q)
)
:effect
(and
(Flag336-)
(Flag336prime-)
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_Q))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_Q))
)
:effect
(and
(Flag336prime-)
(not (Flag336-))
)

)
(:action Flag337Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_R)
(NOT-BOARDED-PASS_R)
)
:effect
(and
(Flag337-)
(Flag337prime-)
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_R))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_R))
)
:effect
(and
(Flag337prime-)
(not (Flag337-))
)

)
(:action Flag338Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_E)
(NOT-BOARDED-PASS_E)
)
:effect
(and
(Flag338-)
(Flag338prime-)
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_E))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_E))
)
:effect
(and
(Flag338prime-)
(not (Flag338-))
)

)
(:action Flag339Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_G)
(NOT-BOARDED-PASS_G)
)
:effect
(and
(Flag339-)
(Flag339prime-)
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_G))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_G))
)
:effect
(and
(Flag339prime-)
(not (Flag339-))
)

)
(:action Flag340Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_A)
(NOT-BOARDED-PASS_A)
)
:effect
(and
(Flag340-)
(Flag340prime-)
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_A))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_A))
)
:effect
(and
(Flag340prime-)
(not (Flag340-))
)

)
(:action Flag341Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_O)
(NOT-BOARDED-PASS_O)
)
:effect
(and
(Flag341-)
(Flag341prime-)
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_O))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_O))
)
:effect
(and
(Flag341prime-)
(not (Flag341-))
)

)
(:action Flag342Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_H)
(NOT-BOARDED-PASS_H)
)
:effect
(and
(Flag342-)
(Flag342prime-)
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_H))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_H))
)
:effect
(and
(Flag342prime-)
(not (Flag342-))
)

)
)
