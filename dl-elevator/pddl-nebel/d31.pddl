(define (domain grounded-SIMPLE-ADL-ELEVATORS)
(:requirements
:strips
)
(:predicates
(Flag690-)
(Flag689-)
(Flag688-)
(Flag687-)
(Flag686-)
(Flag685-)
(Flag684-)
(Flag683-)
(Flag682-)
(Flag681-)
(Flag680-)
(Flag679-)
(Flag78-)
(NOT-LIFT_AT-BE)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(ERROR-)
(Flag37-)
(Flag35-)
(Flag33-)
(Flag31-)
(Flag29-)
(Flag27-)
(Flag25-)
(Flag23-)
(Flag21-)
(Flag19-)
(Flag17-)
(Flag15-)
(Flag13-)
(Flag11-)
(Flag9-)
(Flag7-)
(Flag5-)
(Flag3-)
(Flag1-)
(Flag79-)
(LIFT_AT-BB)
(LIFT_AT-AZ)
(Flag38-)
(Flag36-)
(Flag34-)
(Flag32-)
(Flag30-)
(Flag28-)
(Flag26-)
(Flag24-)
(Flag22-)
(Flag20-)
(Flag18-)
(Flag16-)
(Flag14-)
(Flag12-)
(Flag10-)
(Flag8-)
(Flag6-)
(Flag4-)
(Flag2-)
(SERVED-PASS_AR)
(SERVED-PASS_AG)
(BOARDED-PASS_AU)
(SERVED-PASS_BC)
(Flag82-)
(Flag76-)
(LIFT_AT-AX)
(SERVED-PASS_AH)
(Flag72-)
(LIFT_AT-AW)
(SERVED-PASS_AI)
(Flag84-)
(LIFT_AT-AV)
(LIFT_AT-AT)
(LIFT_AT-AS)
(BOARDED-PASS_AY)
(BOARDED-PASS_AI)
(SERVED-PASS_AE)
(SERVED-PASS_AU)
(SERVED-PASS_BA)
(Flag91-)
(Flag89-)
(Flag80-)
(LIFT_AT-AQ)
(LIFT_AT-AO)
(SERVED-PASS_AM)
(BOARDED-PASS_AG)
(Flag92-)
(Flag74-)
(LIFT_AT-AN)
(LIFT_AT-AL)
(LIFT_AT-AK)
(BOARDED-PASS_AE)
(BOARDED-PASS_AA)
(BOARDED-PASS_BC)
(Flag98-)
(Flag95-)
(Flag93-)
(LIFT_AT-AJ)
(BOARDED-PASS_AM)
(Flag97-)
(LIFT_AT-AF)
(SERVED-PASS_AY)
(BOARDED-PASS_BA)
(Flag81-)
(LIFT_AT-AD)
(LIFT_AT-AC)
(SERVED-PASS_AP)
(BOARDED-PASS_AP)
(BOARDED-PASS_AH)
(SERVED-PASS_AA)
(Flag102-)
(Flag100-)
(LIFT_AT-AB)
(Flag39-)
(BOARDED-PASS_AR)
(Flag101-)
(NOT-CHECKCONSISTENCY-)
(NOT-BOARDED-PASS_AR)
(NOT-LIFT_AT-AC)
(NOT-LIFT_AT-AB)
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AH)
(NOT-BOARDED-PASS_AP)
(NOT-SERVED-PASS_AP)
(NOT-LIFT_AT-AD)
(NOT-LIFT_AT-AF)
(NOT-BOARDED-PASS_BA)
(NOT-SERVED-PASS_AY)
(NOT-LIFT_AT-AJ)
(NOT-BOARDED-PASS_AM)
(NOT-LIFT_AT-AK)
(NOT-BOARDED-PASS_BC)
(NOT-BOARDED-PASS_AA)
(NOT-BOARDED-PASS_AE)
(NOT-LIFT_AT-AL)
(NOT-LIFT_AT-AN)
(NOT-LIFT_AT-AO)
(NOT-BOARDED-PASS_AG)
(NOT-SERVED-PASS_AM)
(NOT-LIFT_AT-AQ)
(NOT-LIFT_AT-AS)
(NOT-SERVED-PASS_BA)
(NOT-SERVED-PASS_AU)
(NOT-SERVED-PASS_AE)
(NOT-BOARDED-PASS_AI)
(NOT-BOARDED-PASS_AY)
(NOT-LIFT_AT-AT)
(NOT-LIFT_AT-AV)
(NOT-LIFT_AT-AW)
(NOT-SERVED-PASS_AI)
(NOT-LIFT_AT-AX)
(NOT-SERVED-PASS_AH)
(NOT-LIFT_AT-AZ)
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_AU)
(NOT-SERVED-PASS_AG)
(NOT-SERVED-PASS_AR)
(NOT-LIFT_AT-BB)
(NOT-LIFT_AT-BD)
(NOT-ERROR-)
(LIFT_AT-BE)
(Flag690prime-)
(Flag689prime-)
(Flag688prime-)
(Flag687prime-)
(Flag686prime-)
(Flag685prime-)
(Flag684prime-)
(Flag683prime-)
(Flag682prime-)
(Flag681prime-)
(Flag680prime-)
(Flag679prime-)
(Flag78prime-)
(Flag37prime-)
(Flag35prime-)
(Flag33prime-)
(Flag31prime-)
(Flag29prime-)
(Flag27prime-)
(Flag25prime-)
(Flag23prime-)
(Flag21prime-)
(Flag19prime-)
(Flag17prime-)
(Flag15prime-)
(Flag13prime-)
(Flag11prime-)
(Flag9prime-)
(Flag7prime-)
(Flag5prime-)
(Flag3prime-)
(Flag1prime-)
(Flag79prime-)
(Flag38prime-)
(Flag36prime-)
(Flag34prime-)
(Flag32prime-)
(Flag30prime-)
(Flag28prime-)
(Flag26prime-)
(Flag24prime-)
(Flag22prime-)
(Flag20prime-)
(Flag18prime-)
(Flag16prime-)
(Flag14prime-)
(Flag12prime-)
(Flag10prime-)
(Flag8prime-)
(Flag6prime-)
(Flag4prime-)
(Flag2prime-)
(Flag82prime-)
(Flag76prime-)
(Flag72prime-)
(Flag84prime-)
(Flag91prime-)
(Flag89prime-)
(Flag80prime-)
(Flag92prime-)
(Flag74prime-)
(Flag98prime-)
(Flag95prime-)
(Flag93prime-)
(Flag97prime-)
(Flag81prime-)
(Flag102prime-)
(Flag100prime-)
(Flag39prime-)
(Flag101prime-)
)
(:action STOP-PASS_BA-AB
:parameters ()
:precondition
(and
(Flag101-)
(Flag101prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim70Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Flag101Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(Flag101-)
(Flag101prime-)
)

)
(:action Prim101Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim133Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim165Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim197Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim229Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim261Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim293Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim325Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim357Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim389Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim421Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim453Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim485Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim517Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim549Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim581Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim613Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim645Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action Prim677Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AB))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AD
:parameters ()
:precondition
(and
(Flag100-)
(Flag100prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AC
:parameters ()
:precondition
(and
(Flag102-)
(Flag102prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
(Flag680prime-)
)
:effect
(and
(when
(and
(Flag680-)
)
(and
(BOARDED-PASS_AR)
(not (NOT-BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Flag39Action
:parameters ()
:precondition
(and
(Flag2-)
(Flag2prime-)
(Flag4-)
(Flag4prime-)
(Flag6-)
(Flag6prime-)
(SERVED-PASS_AY)
(Flag8-)
(Flag8prime-)
(Flag10-)
(Flag10prime-)
(SERVED-PASS_AR)
(Flag12-)
(Flag12prime-)
(SERVED-PASS_AP)
(Flag14-)
(Flag14prime-)
(Flag16-)
(Flag16prime-)
(Flag18-)
(Flag18prime-)
(SERVED-PASS_AM)
(Flag20-)
(Flag20prime-)
(SERVED-PASS_AH)
(SERVED-PASS_AI)
(SERVED-PASS_AG)
(SERVED-PASS_AE)
(Flag22-)
(Flag22prime-)
(SERVED-PASS_AA)
(Flag24-)
(Flag24prime-)
(Flag26-)
(Flag26prime-)
(Flag28-)
(Flag28prime-)
(Flag30-)
(Flag30prime-)
(Flag32-)
(Flag32prime-)
(SERVED-PASS_AU)
(Flag34-)
(Flag34prime-)
(SERVED-PASS_BC)
(Flag36-)
(Flag36prime-)
(SERVED-PASS_BA)
(Flag38-)
(Flag38prime-)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(Flag39-)
(Flag39prime-)
)

)
(:action Prim39Action-8
:parameters ()
:precondition
(and
(not (SERVED-PASS_AP))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-19
:parameters ()
:precondition
(and
(not (SERVED-PASS_AA))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AB-AC
:parameters ()
:precondition
(and
(LIFT_AT-AB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AB)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AB))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AC-AB
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim69Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim71Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Flag100Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(Flag100-)
(Flag100prime-)
)

)
(:action Prim100Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Flag102Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(Flag102-)
(Flag102prime-)
)

)
(:action Prim102Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim132Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim134Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim164Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim166Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim196Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim198Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim228Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim230Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim260Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim262Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim292Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim294Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim324Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim326Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim356Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim358Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim388Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim390Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim420Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim422Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim452Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim454Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim484Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim486Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim516Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim518Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim548Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim550Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim580Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim582Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim612Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim614Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim644Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim646Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action Prim676Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AD))
)
:effect
(and
)

)
(:action Prim678Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AC))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AF
:parameters ()
:precondition
(and
(Flag81-)
(Flag81prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(when
(and
(BOARDED-PASS_AA)
)
(and
(SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
(not (NOT-SERVED-PASS_AA))
(not (BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
(Flag683prime-)
)
:effect
(and
(when
(and
(Flag683-)
)
(and
(BOARDED-PASS_AH)
(not (NOT-BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
(Flag681prime-)
)
:effect
(and
(when
(and
(Flag681-)
)
(and
(BOARDED-PASS_AP)
(not (NOT-BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(when
(and
(BOARDED-PASS_AP)
)
(and
(SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
(not (NOT-SERVED-PASS_AP))
(not (BOARDED-PASS_AP))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AC
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AC)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-3
:parameters ()
:precondition
(and
(not (SERVED-PASS_AY))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AC-AD
:parameters ()
:precondition
(and
(LIFT_AT-AC)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AC)
(LIFT_AT-AD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AC))
(not (NOT-LIFT_AT-AD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AD-AF
:parameters ()
:precondition
(and
(LIFT_AT-AD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AD)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AD))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AD-AC
:parameters ()
:precondition
(and
(LIFT_AT-AD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AD)
(LIFT_AT-AC)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AD))
(not (NOT-LIFT_AT-AC))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AF-AD
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim50Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Flag81Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(Flag81-)
(Flag81prime-)
)

)
(:action Prim81Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim113Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim145Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim177Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim209Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim241Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim273Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim305Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim337Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim369Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim401Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim433Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim465Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim497Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim529Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim561Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim593Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim625Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action Prim657Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AF))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AJ
:parameters ()
:precondition
(and
(Flag97-)
(Flag97prime-)
(Flag690prime-)
)
:effect
(and
(when
(and
(Flag690-)
)
(and
(BOARDED-PASS_BA)
(not (NOT-BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AF
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AF)
)
:effect
(and
(when
(and
(BOARDED-PASS_AY)
)
(and
(SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
(not (NOT-SERVED-PASS_AY))
(not (BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AF-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AF)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AF)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AF))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AJ-AF
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AF)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AF))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim66Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Flag97Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(Flag97-)
(Flag97prime-)
)

)
(:action Prim97Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim129Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim161Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim193Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim225Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim257Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim289Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim321Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim353Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim385Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim417Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim449Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim481Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim513Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim545Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim577Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim609Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim641Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action Prim673Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AJ))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AN
:parameters ()
:precondition
(and
(Flag93-)
(Flag93prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AL
:parameters ()
:precondition
(and
(Flag95-)
(Flag95prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AK
:parameters ()
:precondition
(and
(Flag98-)
(Flag98prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
(Flag682prime-)
)
:effect
(and
(when
(and
(Flag682-)
)
(and
(BOARDED-PASS_AM)
(not (NOT-BOARDED-PASS_AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AJ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AJ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AJ-AK
:parameters ()
:precondition
(and
(LIFT_AT-AJ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AJ)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AJ))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AK-AJ
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AJ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AJ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim62Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim64Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim67Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Flag93Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(Flag93-)
(Flag93prime-)
)

)
(:action Prim93Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Flag95Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(Flag95-)
(Flag95prime-)
)

)
(:action Prim95Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Flag98Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(Flag98-)
(Flag98prime-)
)

)
(:action Prim98Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim125Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim127Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim130Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim157Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim159Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim162Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim189Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim191Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim194Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim221Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim223Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim226Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim253Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim255Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim258Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim285Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim287Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim290Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim317Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim319Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim322Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim349Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim351Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim354Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim381Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim383Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim386Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim413Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim415Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim418Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim445Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim447Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim450Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim477Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim479Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim482Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim509Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim511Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim514Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim541Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim543Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim546Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim573Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim575Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim578Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim605Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim607Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim610Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim637Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim639Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim642Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action Prim669Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AN))
)
:effect
(and
)

)
(:action Prim671Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AL))
)
:effect
(and
)

)
(:action Prim674Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AK))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AO
:parameters ()
:precondition
(and
(Flag74-)
(Flag74prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AQ
:parameters ()
:precondition
(and
(Flag92-)
(Flag92prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
(Flag689prime-)
)
:effect
(and
(when
(and
(Flag689-)
)
(and
(BOARDED-PASS_BC)
(not (NOT-BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
(Flag687prime-)
)
:effect
(and
(when
(and
(Flag687-)
)
(and
(BOARDED-PASS_AA)
(not (NOT-BOARDED-PASS_AA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
(Flag686prime-)
)
:effect
(and
(when
(and
(Flag686-)
)
(and
(BOARDED-PASS_AE)
(not (NOT-BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AN
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AN)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AL
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AL)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AK
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AK)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-12
:parameters ()
:precondition
(and
(not (SERVED-PASS_AM))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AK-AL
:parameters ()
:precondition
(and
(LIFT_AT-AK)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AK)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AK))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AL-AN
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AN-AO
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AL-AK
:parameters ()
:precondition
(and
(LIFT_AT-AL)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AL)
(LIFT_AT-AK)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AL))
(not (NOT-LIFT_AT-AK))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AN-AL
:parameters ()
:precondition
(and
(LIFT_AT-AN)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AN)
(LIFT_AT-AL)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AN))
(not (NOT-LIFT_AT-AL))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AO-AN
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AN)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AN))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim43Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim61Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Flag74Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(Flag74-)
(Flag74prime-)
)

)
(:action Prim74Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Flag92Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(Flag92-)
(Flag92prime-)
)

)
(:action Prim92Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim106Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim124Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim138Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim156Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim170Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim188Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim202Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim220Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim234Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim252Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim266Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim284Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim298Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim316Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim330Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim348Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim362Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim380Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim394Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim412Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim426Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim444Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim458Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim476Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim490Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim508Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim522Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim540Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim554Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim572Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim586Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim604Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim618Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim636Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action Prim650Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AO))
)
:effect
(and
)

)
(:action Prim668Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AQ))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AV
:parameters ()
:precondition
(and
(Flag80-)
(Flag80prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AT
:parameters ()
:precondition
(and
(Flag89-)
(Flag89prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AS
:parameters ()
:precondition
(and
(Flag91-)
(Flag91prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
(Flag685prime-)
)
:effect
(and
(when
(and
(Flag685-)
)
(and
(BOARDED-PASS_AG)
(not (NOT-BOARDED-PASS_AG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(when
(and
(BOARDED-PASS_AM)
)
(and
(SERVED-PASS_AM)
(NOT-BOARDED-PASS_AM)
(not (NOT-SERVED-PASS_AM))
(not (BOARDED-PASS_AM))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AO
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AO)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AQ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AQ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-17
:parameters ()
:precondition
(and
(not (SERVED-PASS_AE))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-25
:parameters ()
:precondition
(and
(not (SERVED-PASS_AU))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-29
:parameters ()
:precondition
(and
(not (SERVED-PASS_BA))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AO-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AO)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AO)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AO))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AQ-AS
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AQ-AO
:parameters ()
:precondition
(and
(LIFT_AT-AQ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AQ)
(LIFT_AT-AO)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AQ))
(not (NOT-LIFT_AT-AO))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AS-AQ
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AQ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AQ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim49Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim58Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim60Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Flag80Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(Flag80-)
(Flag80prime-)
)

)
(:action Prim80Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Flag89Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(Flag89-)
(Flag89prime-)
)

)
(:action Prim89Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Flag91Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(Flag91-)
(Flag91prime-)
)

)
(:action Prim91Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim112Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim121Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim123Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim144Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim153Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim155Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim176Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim185Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim187Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim208Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim217Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim219Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim240Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim249Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim251Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim272Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim281Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim283Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim304Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim313Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim315Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim336Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim345Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim347Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim368Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim377Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim379Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim400Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim409Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim411Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim432Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim441Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim443Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim464Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim473Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim475Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim496Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim505Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim507Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim528Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim537Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim539Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim560Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim569Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim571Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim592Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim601Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim603Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim624Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim633Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim635Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action Prim656Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AV))
)
:effect
(and
)

)
(:action Prim665Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AT))
)
:effect
(and
)

)
(:action Prim667Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AS))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AW
:parameters ()
:precondition
(and
(Flag84-)
(Flag84prime-)
)
:effect
(and
(when
(and
(BOARDED-PASS_BA)
)
(and
(SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
(not (NOT-SERVED-PASS_BA))
(not (BOARDED-PASS_BA))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(when
(and
(BOARDED-PASS_AU)
)
(and
(SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
(not (NOT-SERVED-PASS_AU))
(not (BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(when
(and
(BOARDED-PASS_AE)
)
(and
(SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
(not (NOT-SERVED-PASS_AE))
(not (BOARDED-PASS_AE))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
(Flag684prime-)
)
:effect
(and
(when
(and
(Flag684-)
)
(and
(BOARDED-PASS_AI)
(not (NOT-BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AV
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AV)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AT
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AT)
(Flag679prime-)
)
:effect
(and
(when
(and
(Flag679-)
)
(and
(BOARDED-PASS_AY)
(not (NOT-BOARDED-PASS_AY))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AS
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AS)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-15
:parameters ()
:precondition
(and
(not (SERVED-PASS_AI))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AS-AT
:parameters ()
:precondition
(and
(LIFT_AT-AS)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AS)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AS))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AT-AV
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-AV-AW
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AT-AS
:parameters ()
:precondition
(and
(LIFT_AT-AT)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AT)
(LIFT_AT-AS)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AT))
(not (NOT-LIFT_AT-AS))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AV-AT
:parameters ()
:precondition
(and
(LIFT_AT-AV)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AV)
(LIFT_AT-AT)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AV))
(not (NOT-LIFT_AT-AT))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AW-AV
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AV)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AV))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim53Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Flag84Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(Flag84-)
(Flag84prime-)
)

)
(:action Prim84Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim116Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim148Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim180Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim212Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim244Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim276Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim308Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim340Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim372Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim404Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim436Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim468Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim500Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim532Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim564Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim596Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim628Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action Prim660Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AW))
)
:effect
(and
)

)
(:action STOP-PASS_BA-AX
:parameters ()
:precondition
(and
(Flag72-)
(Flag72prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(when
(and
(BOARDED-PASS_AI)
)
(and
(SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
(not (NOT-SERVED-PASS_AI))
(not (BOARDED-PASS_AI))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AW
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AW)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-14
:parameters ()
:precondition
(and
(not (SERVED-PASS_AH))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AW-AX
:parameters ()
:precondition
(and
(LIFT_AT-AW)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AW)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AW))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AX-AW
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AW)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AW))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim41Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Flag72Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(Flag72-)
(Flag72prime-)
)

)
(:action Prim72Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim104Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim136Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim168Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim200Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim232Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim264Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim296Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim328Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim360Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim392Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim424Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim456Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim488Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim520Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim552Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim584Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim616Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action Prim648Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AX))
)
:effect
(and
)

)
(:action STOP-PASS_BA-BB
:parameters ()
:precondition
(and
(Flag76-)
(Flag76prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BA-AZ
:parameters ()
:precondition
(and
(Flag82-)
(Flag82prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(when
(and
(BOARDED-PASS_AH)
)
(and
(SERVED-PASS_AH)
(NOT-BOARDED-PASS_AH)
(not (NOT-SERVED-PASS_AH))
(not (BOARDED-PASS_AH))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AX
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AX)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim39Action-0
:parameters ()
:precondition
(and
(not (Flag2-))
(Flag2prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-1
:parameters ()
:precondition
(and
(not (Flag4-))
(Flag4prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-2
:parameters ()
:precondition
(and
(not (Flag6-))
(Flag6prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-4
:parameters ()
:precondition
(and
(not (Flag8-))
(Flag8prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-5
:parameters ()
:precondition
(and
(not (Flag10-))
(Flag10prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-6
:parameters ()
:precondition
(and
(not (SERVED-PASS_AR))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-7
:parameters ()
:precondition
(and
(not (Flag12-))
(Flag12prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-9
:parameters ()
:precondition
(and
(not (Flag14-))
(Flag14prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-10
:parameters ()
:precondition
(and
(not (Flag16-))
(Flag16prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-11
:parameters ()
:precondition
(and
(not (Flag18-))
(Flag18prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-13
:parameters ()
:precondition
(and
(not (Flag20-))
(Flag20prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-16
:parameters ()
:precondition
(and
(not (SERVED-PASS_AG))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-18
:parameters ()
:precondition
(and
(not (Flag22-))
(Flag22prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-20
:parameters ()
:precondition
(and
(not (Flag24-))
(Flag24prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-21
:parameters ()
:precondition
(and
(not (Flag26-))
(Flag26prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-22
:parameters ()
:precondition
(and
(not (Flag28-))
(Flag28prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-23
:parameters ()
:precondition
(and
(not (Flag30-))
(Flag30prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-24
:parameters ()
:precondition
(and
(not (Flag32-))
(Flag32prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-26
:parameters ()
:precondition
(and
(not (Flag34-))
(Flag34prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-27
:parameters ()
:precondition
(and
(not (SERVED-PASS_BC))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-28
:parameters ()
:precondition
(and
(not (Flag36-))
(Flag36prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-30
:parameters ()
:precondition
(and
(not (Flag38-))
(Flag38prime-)
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action MOVEUP-AX-AZ
:parameters ()
:precondition
(and
(LIFT_AT-AX)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AX)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AX))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-AZ-AX
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-AX)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-AX))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim45Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim51Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Flag76Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(Flag76-)
(Flag76prime-)
)

)
(:action Prim76Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Flag82Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(Flag82-)
(Flag82prime-)
)

)
(:action Prim82Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim108Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim114Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim140Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim146Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim172Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim178Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim204Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim210Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim236Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim242Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim268Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim274Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim300Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim306Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim332Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim338Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim364Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim370Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim396Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim402Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim428Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim434Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim460Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim466Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim492Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim498Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim524Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim530Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim556Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim562Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim588Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim594Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim620Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim626Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action Prim652Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BB))
)
:effect
(and
)

)
(:action Prim658Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-AZ))
)
:effect
(and
)

)
(:action STOP-PASS_BA-BD
:parameters ()
:precondition
(and
(Flag79-)
(Flag79prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(when
(and
(BOARDED-PASS_BC)
)
(and
(SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
(not (NOT-SERVED-PASS_BC))
(not (BOARDED-PASS_BC))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
(Flag688prime-)
)
:effect
(and
(when
(and
(Flag688-)
)
(and
(BOARDED-PASS_AU)
(not (NOT-BOARDED-PASS_AU))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AG)
)
(and
(SERVED-PASS_AG)
(NOT-BOARDED-PASS_AG)
(not (NOT-SERVED-PASS_AG))
(not (BOARDED-PASS_AG))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(when
(and
(BOARDED-PASS_AR)
)
(and
(SERVED-PASS_AR)
(NOT-BOARDED-PASS_AR)
(not (NOT-SERVED-PASS_AR))
(not (BOARDED-PASS_AR))
)
)
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-BB
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BB)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-AZ
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-AZ)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Flag2Action-0
:parameters ()
:precondition
(and
(Flag1-)
(Flag1prime-)
)
:effect
(and
(Flag2-)
(Flag2prime-)
)

)
(:action Flag4Action-0
:parameters ()
:precondition
(and
(Flag3-)
(Flag3prime-)
)
:effect
(and
(Flag4-)
(Flag4prime-)
)

)
(:action Flag6Action-0
:parameters ()
:precondition
(and
(Flag5-)
(Flag5prime-)
)
:effect
(and
(Flag6-)
(Flag6prime-)
)

)
(:action Flag8Action-0
:parameters ()
:precondition
(and
(Flag7-)
(Flag7prime-)
)
:effect
(and
(Flag8-)
(Flag8prime-)
)

)
(:action Flag10Action-0
:parameters ()
:precondition
(and
(Flag9-)
(Flag9prime-)
)
:effect
(and
(Flag10-)
(Flag10prime-)
)

)
(:action Flag12Action-0
:parameters ()
:precondition
(and
(Flag11-)
(Flag11prime-)
)
:effect
(and
(Flag12-)
(Flag12prime-)
)

)
(:action Flag14Action-0
:parameters ()
:precondition
(and
(Flag13-)
(Flag13prime-)
)
:effect
(and
(Flag14-)
(Flag14prime-)
)

)
(:action Flag16Action-0
:parameters ()
:precondition
(and
(Flag15-)
(Flag15prime-)
)
:effect
(and
(Flag16-)
(Flag16prime-)
)

)
(:action Flag18Action-0
:parameters ()
:precondition
(and
(Flag17-)
(Flag17prime-)
)
:effect
(and
(Flag18-)
(Flag18prime-)
)

)
(:action Flag20Action-0
:parameters ()
:precondition
(and
(Flag19-)
(Flag19prime-)
)
:effect
(and
(Flag20-)
(Flag20prime-)
)

)
(:action Flag22Action-0
:parameters ()
:precondition
(and
(Flag21-)
(Flag21prime-)
)
:effect
(and
(Flag22-)
(Flag22prime-)
)

)
(:action Flag24Action-0
:parameters ()
:precondition
(and
(Flag23-)
(Flag23prime-)
)
:effect
(and
(Flag24-)
(Flag24prime-)
)

)
(:action Flag26Action-0
:parameters ()
:precondition
(and
(Flag25-)
(Flag25prime-)
)
:effect
(and
(Flag26-)
(Flag26prime-)
)

)
(:action Flag28Action-0
:parameters ()
:precondition
(and
(Flag27-)
(Flag27prime-)
)
:effect
(and
(Flag28-)
(Flag28prime-)
)

)
(:action Flag30Action-0
:parameters ()
:precondition
(and
(Flag29-)
(Flag29prime-)
)
:effect
(and
(Flag30-)
(Flag30prime-)
)

)
(:action Flag32Action-0
:parameters ()
:precondition
(and
(Flag31-)
(Flag31prime-)
)
:effect
(and
(Flag32-)
(Flag32prime-)
)

)
(:action Flag34Action-0
:parameters ()
:precondition
(and
(Flag33-)
(Flag33prime-)
)
:effect
(and
(Flag34-)
(Flag34prime-)
)

)
(:action Flag36Action-0
:parameters ()
:precondition
(and
(Flag35-)
(Flag35prime-)
)
:effect
(and
(Flag36-)
(Flag36prime-)
)

)
(:action Flag38Action-0
:parameters ()
:precondition
(and
(Flag37-)
(Flag37prime-)
)
:effect
(and
(Flag38-)
(Flag38prime-)
)

)
(:action MOVEUP-AZ-BB
:parameters ()
:precondition
(and
(LIFT_AT-AZ)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-AZ)
(LIFT_AT-BB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-AZ))
(not (NOT-LIFT_AT-BB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-BB-BD
:parameters ()
:precondition
(and
(LIFT_AT-BB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BB)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BB))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-BB-AZ
:parameters ()
:precondition
(and
(LIFT_AT-BB)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BB)
(LIFT_AT-AZ)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BB))
(not (NOT-LIFT_AT-AZ))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-BD-BB
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-BB)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-BB))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim48Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Flag79Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(Flag79-)
(Flag79prime-)
)

)
(:action Prim79Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim111Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim143Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim175Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim207Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim239Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim271Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim303Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim335Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim367Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim399Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim431Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim463Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim495Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim527Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim559Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim591Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim623Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action Prim655Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BD))
)
:effect
(and
)

)
(:action STOP-PASS_BA-BE
:parameters ()
:precondition
(and
(Flag78-)
(Flag78prime-)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_BC-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AU-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AA-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AE-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AG-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AI-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AH-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AM-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AP-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AR-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-BE
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action STOP-PASS_AY-BD
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BD)
)
:effect
(and
(CHECKCONSISTENCY-)
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Flag1Action
:parameters ()
:effect
(and
(Flag1-)
(Flag1prime-)
)

)
(:action Flag3Action
:parameters ()
:effect
(and
(Flag3-)
(Flag3prime-)
)

)
(:action Flag5Action
:parameters ()
:effect
(and
(Flag5-)
(Flag5prime-)
)

)
(:action Flag7Action
:parameters ()
:effect
(and
(Flag7-)
(Flag7prime-)
)

)
(:action Flag9Action
:parameters ()
:effect
(and
(Flag9-)
(Flag9prime-)
)

)
(:action Flag11Action
:parameters ()
:effect
(and
(Flag11-)
(Flag11prime-)
)

)
(:action Flag13Action
:parameters ()
:effect
(and
(Flag13-)
(Flag13prime-)
)

)
(:action Flag15Action
:parameters ()
:effect
(and
(Flag15-)
(Flag15prime-)
)

)
(:action Flag17Action
:parameters ()
:effect
(and
(Flag17-)
(Flag17prime-)
)

)
(:action Flag19Action
:parameters ()
:effect
(and
(Flag19-)
(Flag19prime-)
)

)
(:action Flag21Action
:parameters ()
:effect
(and
(Flag21-)
(Flag21prime-)
)

)
(:action Flag23Action
:parameters ()
:effect
(and
(Flag23-)
(Flag23prime-)
)

)
(:action Flag25Action
:parameters ()
:effect
(and
(Flag25-)
(Flag25prime-)
)

)
(:action Flag27Action
:parameters ()
:effect
(and
(Flag27-)
(Flag27prime-)
)

)
(:action Flag29Action
:parameters ()
:effect
(and
(Flag29-)
(Flag29prime-)
)

)
(:action Flag31Action
:parameters ()
:effect
(and
(Flag31-)
(Flag31prime-)
)

)
(:action Flag33Action
:parameters ()
:effect
(and
(Flag33-)
(Flag33prime-)
)

)
(:action Flag35Action
:parameters ()
:effect
(and
(Flag35-)
(Flag35prime-)
)

)
(:action Flag37Action
:parameters ()
:effect
(and
(Flag37-)
(Flag37prime-)
)

)
(:action Prim39Action-31
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action Prim39Action-32
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag39prime-)
(not (Flag39-))
)

)
(:action CHECKCONSISTENCYACTION
:parameters ()
:precondition
(and
(NOT-ERROR-)
(CHECKCONSISTENCY-)
)
:effect
(and
(NOT-CHECKCONSISTENCY-)
(not (CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEUP-BD-BE
:parameters ()
:precondition
(and
(LIFT_AT-BD)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BD)
(LIFT_AT-BE)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BD))
(not (NOT-LIFT_AT-BE))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action MOVEDOWN-BE-BD
:parameters ()
:precondition
(and
(LIFT_AT-BE)
(NOT-ERROR-)
(NOT-CHECKCONSISTENCY-)
)
:effect
(and
(NOT-LIFT_AT-BE)
(LIFT_AT-BD)
(CHECKCONSISTENCY-)
(not (LIFT_AT-BE))
(not (NOT-LIFT_AT-BD))
(not (NOT-CHECKCONSISTENCY-))
(not (Flag690prime-))
(not (Flag689prime-))
(not (Flag688prime-))
(not (Flag687prime-))
(not (Flag686prime-))
(not (Flag685prime-))
(not (Flag684prime-))
(not (Flag683prime-))
(not (Flag682prime-))
(not (Flag681prime-))
(not (Flag680prime-))
(not (Flag679prime-))
(not (Flag78prime-))
(not (Flag37prime-))
(not (Flag35prime-))
(not (Flag33prime-))
(not (Flag31prime-))
(not (Flag29prime-))
(not (Flag27prime-))
(not (Flag25prime-))
(not (Flag23prime-))
(not (Flag21prime-))
(not (Flag19prime-))
(not (Flag17prime-))
(not (Flag15prime-))
(not (Flag13prime-))
(not (Flag11prime-))
(not (Flag9prime-))
(not (Flag7prime-))
(not (Flag5prime-))
(not (Flag3prime-))
(not (Flag1prime-))
(not (Flag79prime-))
(not (Flag38prime-))
(not (Flag36prime-))
(not (Flag34prime-))
(not (Flag32prime-))
(not (Flag30prime-))
(not (Flag28prime-))
(not (Flag26prime-))
(not (Flag24prime-))
(not (Flag22prime-))
(not (Flag20prime-))
(not (Flag18prime-))
(not (Flag16prime-))
(not (Flag14prime-))
(not (Flag12prime-))
(not (Flag10prime-))
(not (Flag8prime-))
(not (Flag6prime-))
(not (Flag4prime-))
(not (Flag2prime-))
(not (Flag82prime-))
(not (Flag76prime-))
(not (Flag72prime-))
(not (Flag84prime-))
(not (Flag91prime-))
(not (Flag89prime-))
(not (Flag80prime-))
(not (Flag92prime-))
(not (Flag74prime-))
(not (Flag98prime-))
(not (Flag95prime-))
(not (Flag93prime-))
(not (Flag97prime-))
(not (Flag81prime-))
(not (Flag102prime-))
(not (Flag100prime-))
(not (Flag39prime-))
(not (Flag101prime-))
)
)
(:action Prim41Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim41Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim42Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim42Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim43Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim43Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim44Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim44Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim45Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim45Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim46Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim46Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim47Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim47Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim48Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim48Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim49Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim49Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim50Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim50Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim51Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim51Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim52Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim52Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim53Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim53Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim54Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim54Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim55Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim55Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim56Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim56Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim57Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim57Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim58Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim58Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim59Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim59Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim60Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim60Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim61Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim61Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim62Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim62Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim63Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim63Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim64Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim64Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim65Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim65Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim66Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim66Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim67Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim67Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim68Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim68Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim69Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim69Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim70Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim70Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim71Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim71Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim72Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim72Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag72prime-)
(not (Flag72-))
)

)
(:action Prim73Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim73Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim74Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim74Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag74prime-)
(not (Flag74-))
)

)
(:action Prim75Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim75Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim76Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim76Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag76prime-)
(not (Flag76-))
)

)
(:action Prim77Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim77Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag78Action
:parameters ()
:precondition
(and
(NOT-CHECKCONSISTENCY-)
(NOT-ERROR-)
(LIFT_AT-BE)
)
:effect
(and
(Flag78-)
(Flag78prime-)
)

)
(:action Prim78Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim78Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
(Flag78prime-)
(not (Flag78-))
)

)
(:action Prim79Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim79Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag79prime-)
(not (Flag79-))
)

)
(:action Prim80Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim80Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag80prime-)
(not (Flag80-))
)

)
(:action Prim81Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim81Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag81prime-)
(not (Flag81-))
)

)
(:action Prim82Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim82Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag82prime-)
(not (Flag82-))
)

)
(:action Prim83Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim83Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim84Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim84Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag84prime-)
(not (Flag84-))
)

)
(:action Prim85Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim85Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim86Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim86Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim87Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim87Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim88Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim88Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim89Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim89Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag89prime-)
(not (Flag89-))
)

)
(:action Prim90Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim90Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim91Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim91Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag91prime-)
(not (Flag91-))
)

)
(:action Prim92Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim92Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag92prime-)
(not (Flag92-))
)

)
(:action Prim93Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim93Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag93prime-)
(not (Flag93-))
)

)
(:action Prim94Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim94Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim95Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim95Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag95prime-)
(not (Flag95-))
)

)
(:action Prim96Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim96Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim97Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim97Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag97prime-)
(not (Flag97-))
)

)
(:action Prim98Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim98Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag98prime-)
(not (Flag98-))
)

)
(:action Prim99Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim99Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim100Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim100Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag100prime-)
(not (Flag100-))
)

)
(:action Prim101Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim101Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag101prime-)
(not (Flag101-))
)

)
(:action Prim102Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim102Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
(Flag102prime-)
(not (Flag102-))
)

)
(:action Prim104Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim104Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim105Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim105Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim106Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim106Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim107Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim107Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim108Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim108Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim109Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim109Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim110Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim110Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim111Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim111Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim112Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim112Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim113Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim113Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim114Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim114Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim115Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim115Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim116Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim116Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim117Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim117Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim118Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim118Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim119Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim119Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim120Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim120Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim121Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim121Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim122Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim122Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim123Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim123Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim124Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim124Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim125Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim125Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim126Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim126Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim127Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim127Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim128Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim128Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim129Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim129Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim130Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim130Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim131Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim131Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim132Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim132Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim133Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim133Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim134Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim134Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim136Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim136Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim137Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim137Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim138Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim138Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim139Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim139Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim140Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim140Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim141Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim141Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim142Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim142Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim143Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim143Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim144Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim144Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim145Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim145Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim146Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim146Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim147Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim147Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim148Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim148Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim149Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim149Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim150Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim150Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim151Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim151Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim152Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim152Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim153Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim153Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim154Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim154Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim155Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim155Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim156Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim156Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim157Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim157Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim158Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim158Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim159Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim159Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim160Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim160Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim161Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim161Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim162Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim162Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim163Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim163Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim164Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim164Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim165Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim165Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim166Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim166Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim168Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim168Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim169Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim169Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim170Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim170Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim171Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim171Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim172Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim172Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim173Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim173Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim174Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim174Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim175Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim175Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim176Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim176Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim177Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim177Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim178Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim178Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim179Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim179Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim180Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim180Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim181Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim181Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim182Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim182Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim183Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim183Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim184Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim184Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim185Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim185Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim186Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim186Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim187Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim187Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim188Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim188Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim189Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim189Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim190Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim190Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim191Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim191Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim192Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim192Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim193Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim193Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim194Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim194Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim195Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim195Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim196Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim196Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim197Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim197Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim198Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim198Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim200Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim200Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim201Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim201Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim202Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim202Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim203Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim203Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim204Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim204Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim205Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim205Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim206Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim206Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim207Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim207Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim208Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim208Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim209Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim209Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim210Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim210Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim211Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim211Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim212Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim212Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim213Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim213Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim214Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim214Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim215Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim215Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim216Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim216Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim217Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim217Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim218Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim218Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim219Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim219Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim220Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim220Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim221Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim221Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim222Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim222Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim223Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim223Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim224Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim224Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim225Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim225Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim226Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim226Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim227Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim227Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim228Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim228Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim229Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim229Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim230Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim230Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim232Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim232Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim233Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim233Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim234Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim234Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim235Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim235Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim236Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim236Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim237Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim237Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim238Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim238Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim239Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim239Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim240Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim240Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim241Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim241Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim242Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim242Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim243Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim243Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim244Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim244Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim245Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim245Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim246Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim246Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim247Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim247Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim248Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim248Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim249Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim249Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim250Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim250Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim251Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim251Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim252Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim252Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim253Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim253Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim254Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim254Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim255Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim255Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim256Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim256Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim257Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim257Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim258Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim258Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim259Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim259Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim260Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim260Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim261Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim261Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim262Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim262Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim264Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim264Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim265Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim265Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim266Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim266Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim267Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim267Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim268Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim268Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim269Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim269Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim270Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim270Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim271Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim271Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim272Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim272Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim273Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim273Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim274Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim274Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim275Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim275Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim276Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim276Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim277Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim277Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim278Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim278Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim279Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim279Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim280Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim280Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim281Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim281Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim282Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim282Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim283Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim283Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim284Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim284Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim285Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim285Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim286Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim286Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim287Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim287Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim288Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim288Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim289Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim289Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim290Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim290Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim291Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim291Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim292Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim292Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim293Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim293Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim294Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim294Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim296Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim296Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim297Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim297Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim298Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim298Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim299Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim299Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim300Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim300Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim301Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim301Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim302Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim302Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim303Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim303Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim304Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim304Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim305Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim305Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim306Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim306Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim307Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim307Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim308Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim308Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim309Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim309Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim310Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim310Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim311Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim311Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim312Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim312Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim313Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim313Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim314Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim314Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim315Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim315Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim316Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim316Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim317Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim317Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim318Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim318Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim319Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim319Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim320Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim320Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim321Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim321Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim322Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim322Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim323Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim323Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim324Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim324Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim325Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim325Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim326Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim326Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim328Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim328Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim329Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim329Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim330Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim330Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim331Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim331Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim332Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim332Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim333Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim333Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim334Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim334Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim335Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim335Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim336Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim336Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim337Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim337Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim338Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim338Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim339Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim339Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim340Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim340Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim341Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim341Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim342Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim342Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim343Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim343Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim344Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim344Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim345Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim345Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim346Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim346Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim347Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim347Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim348Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim348Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim349Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim349Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim350Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim350Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim351Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim351Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim352Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim352Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim353Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim353Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim354Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim354Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim355Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim355Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim356Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim356Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim357Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim357Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim358Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim358Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim360Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim360Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim361Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim361Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim362Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim362Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim363Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim363Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim364Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim364Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim365Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim365Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim366Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim366Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim367Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim367Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim368Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim368Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim369Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim369Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim370Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim370Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim371Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim371Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim372Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim372Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim373Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim373Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim374Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim374Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim375Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim375Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim376Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim376Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim377Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim377Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim378Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim378Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim379Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim379Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim380Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim380Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim381Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim381Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim382Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim382Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim383Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim383Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim384Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim384Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim385Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim385Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim386Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim386Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim387Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim387Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim388Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim388Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim389Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim389Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim390Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim390Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim392Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim392Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim393Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim393Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim394Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim394Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim395Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim395Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim396Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim396Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim397Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim397Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim398Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim398Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim399Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim399Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim400Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim400Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim401Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim401Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim402Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim402Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim403Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim403Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim404Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim404Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim405Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim405Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim406Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim406Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim407Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim407Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim408Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim408Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim409Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim409Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim410Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim410Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim411Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim411Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim412Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim412Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim413Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim413Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim414Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim414Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim415Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim415Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim416Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim416Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim417Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim417Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim418Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim418Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim419Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim419Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim420Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim420Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim421Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim421Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim422Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim422Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim424Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim424Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim425Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim425Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim426Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim426Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim427Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim427Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim428Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim428Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim429Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim429Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim430Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim430Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim431Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim431Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim432Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim432Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim433Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim433Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim434Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim434Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim435Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim435Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim436Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim436Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim437Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim437Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim438Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim438Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim439Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim439Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim440Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim440Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim441Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim441Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim442Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim442Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim443Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim443Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim444Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim444Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim445Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim445Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim446Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim446Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim447Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim447Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim448Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim448Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim449Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim449Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim450Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim450Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim451Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim451Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim452Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim452Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim453Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim453Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim454Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim454Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim456Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim456Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim457Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim457Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim458Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim458Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim459Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim459Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim460Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim460Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim461Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim461Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim462Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim462Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim463Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim463Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim464Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim464Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim465Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim465Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim466Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim466Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim467Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim467Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim468Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim468Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim469Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim469Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim470Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim470Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim471Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim471Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim472Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim472Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim473Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim473Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim474Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim474Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim475Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim475Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim476Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim476Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim477Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim477Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim478Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim478Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim479Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim479Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim480Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim480Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim481Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim481Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim482Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim482Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim483Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim483Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim484Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim484Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim485Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim485Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim486Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim486Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim488Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim488Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim489Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim489Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim490Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim490Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim491Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim491Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim492Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim492Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim493Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim493Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim494Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim494Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim495Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim495Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim496Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim496Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim497Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim497Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim498Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim498Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim499Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim499Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim500Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim500Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim501Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim501Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim502Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim502Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim503Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim503Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim504Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim504Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim505Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim505Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim506Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim506Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim507Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim507Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim508Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim508Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim509Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim509Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim510Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim510Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim511Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim511Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim512Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim512Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim513Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim513Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim514Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim514Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim515Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim515Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim516Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim516Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim517Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim517Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim518Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim518Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim520Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim520Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim521Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim521Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim522Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim522Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim523Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim523Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim524Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim524Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim525Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim525Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim526Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim526Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim527Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim527Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim528Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim528Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim529Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim529Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim530Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim530Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim531Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim531Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim532Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim532Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim533Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim533Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim534Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim534Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim535Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim535Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim536Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim536Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim537Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim537Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim538Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim538Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim539Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim539Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim540Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim540Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim541Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim541Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim542Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim542Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim543Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim543Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim544Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim544Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim545Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim545Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim546Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim546Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim547Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim547Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim548Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim548Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim549Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim549Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim550Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim550Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim552Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim552Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim553Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim553Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim554Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim554Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim555Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim555Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim556Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim556Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim557Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim557Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim558Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim558Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim559Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim559Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim560Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim560Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim561Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim561Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim562Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim562Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim563Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim563Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim564Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim564Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim565Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim565Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim566Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim566Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim567Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim567Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim568Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim568Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim569Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim569Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim570Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim570Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim571Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim571Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim572Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim572Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim573Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim573Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim574Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim574Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim575Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim575Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim576Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim576Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim577Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim577Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim578Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim578Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim579Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim579Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim580Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim580Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim581Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim581Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim582Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim582Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim584Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim584Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim585Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim585Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim586Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim586Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim587Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim587Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim588Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim588Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim589Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim589Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim590Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim590Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim591Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim591Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim592Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim592Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim593Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim593Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim594Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim594Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim595Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim595Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim596Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim596Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim597Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim597Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim598Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim598Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim599Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim599Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim600Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim600Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim601Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim601Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim602Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim602Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim603Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim603Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim604Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim604Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim605Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim605Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim606Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim606Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim607Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim607Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim608Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim608Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim609Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim609Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim610Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim610Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim611Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim611Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim612Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim612Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim613Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim613Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim614Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim614Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim616Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim616Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim617Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim617Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim618Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim618Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim619Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim619Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim620Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim620Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim621Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim621Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim622Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim622Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim623Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim623Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim624Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim624Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim625Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim625Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim626Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim626Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim627Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim627Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim628Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim628Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim629Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim629Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim630Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim630Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim631Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim631Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim632Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim632Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim633Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim633Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim634Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim634Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim635Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim635Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim636Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim636Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim637Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim637Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim638Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim638Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim639Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim639Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim640Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim640Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim641Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim641Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim642Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim642Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim643Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim643Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim644Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim644Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim645Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim645Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim646Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim646Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim648Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim648Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim649Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim649Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim650Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim650Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim651Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim651Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim652Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim652Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim653Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim653Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim654Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim654Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim654Action-2
:parameters ()
:precondition
(and
(not (LIFT_AT-BE))
)
:effect
(and
)

)
(:action Prim655Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim655Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim656Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim656Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim657Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim657Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim658Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim658Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim659Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim659Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim660Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim660Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim661Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim661Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim662Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim662Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim663Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim663Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim664Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim664Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim665Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim665Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim666Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim666Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim667Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim667Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim668Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim668Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim669Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim669Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim670Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim670Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim671Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim671Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim672Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim672Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim673Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim673Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim674Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim674Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim675Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim675Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim676Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim676Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim677Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim677Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Prim678Action-0
:parameters ()
:precondition
(and
(not (NOT-CHECKCONSISTENCY-))
)
:effect
(and
)

)
(:action Prim678Action-1
:parameters ()
:precondition
(and
(not (NOT-ERROR-))
)
:effect
(and
)

)
(:action Flag679Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AY)
(NOT-BOARDED-PASS_AY)
)
:effect
(and
(Flag679-)
(Flag679prime-)
)

)
(:action Prim679Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AY))
)
:effect
(and
(Flag679prime-)
(not (Flag679-))
)

)
(:action Prim679Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AY))
)
:effect
(and
(Flag679prime-)
(not (Flag679-))
)

)
(:action Flag680Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AR)
(NOT-BOARDED-PASS_AR)
)
:effect
(and
(Flag680-)
(Flag680prime-)
)

)
(:action Prim680Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AR))
)
:effect
(and
(Flag680prime-)
(not (Flag680-))
)

)
(:action Prim680Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AR))
)
:effect
(and
(Flag680prime-)
(not (Flag680-))
)

)
(:action Flag681Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AP)
(NOT-BOARDED-PASS_AP)
)
:effect
(and
(Flag681-)
(Flag681prime-)
)

)
(:action Prim681Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AP))
)
:effect
(and
(Flag681prime-)
(not (Flag681-))
)

)
(:action Prim681Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AP))
)
:effect
(and
(Flag681prime-)
(not (Flag681-))
)

)
(:action Flag682Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AM)
(NOT-BOARDED-PASS_AM)
)
:effect
(and
(Flag682-)
(Flag682prime-)
)

)
(:action Prim682Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AM))
)
:effect
(and
(Flag682prime-)
(not (Flag682-))
)

)
(:action Prim682Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AM))
)
:effect
(and
(Flag682prime-)
(not (Flag682-))
)

)
(:action Flag683Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AH)
(NOT-BOARDED-PASS_AH)
)
:effect
(and
(Flag683-)
(Flag683prime-)
)

)
(:action Prim683Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AH))
)
:effect
(and
(Flag683prime-)
(not (Flag683-))
)

)
(:action Prim683Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AH))
)
:effect
(and
(Flag683prime-)
(not (Flag683-))
)

)
(:action Flag684Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AI)
(NOT-BOARDED-PASS_AI)
)
:effect
(and
(Flag684-)
(Flag684prime-)
)

)
(:action Prim684Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AI))
)
:effect
(and
(Flag684prime-)
(not (Flag684-))
)

)
(:action Prim684Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AI))
)
:effect
(and
(Flag684prime-)
(not (Flag684-))
)

)
(:action Flag685Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AG)
(NOT-BOARDED-PASS_AG)
)
:effect
(and
(Flag685-)
(Flag685prime-)
)

)
(:action Prim685Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AG))
)
:effect
(and
(Flag685prime-)
(not (Flag685-))
)

)
(:action Prim685Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AG))
)
:effect
(and
(Flag685prime-)
(not (Flag685-))
)

)
(:action Flag686Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AE)
(NOT-BOARDED-PASS_AE)
)
:effect
(and
(Flag686-)
(Flag686prime-)
)

)
(:action Prim686Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AE))
)
:effect
(and
(Flag686prime-)
(not (Flag686-))
)

)
(:action Prim686Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AE))
)
:effect
(and
(Flag686prime-)
(not (Flag686-))
)

)
(:action Flag687Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AA)
(NOT-BOARDED-PASS_AA)
)
:effect
(and
(Flag687-)
(Flag687prime-)
)

)
(:action Prim687Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AA))
)
:effect
(and
(Flag687prime-)
(not (Flag687-))
)

)
(:action Prim687Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AA))
)
:effect
(and
(Flag687prime-)
(not (Flag687-))
)

)
(:action Flag688Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_AU)
(NOT-BOARDED-PASS_AU)
)
:effect
(and
(Flag688-)
(Flag688prime-)
)

)
(:action Prim688Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_AU))
)
:effect
(and
(Flag688prime-)
(not (Flag688-))
)

)
(:action Prim688Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_AU))
)
:effect
(and
(Flag688prime-)
(not (Flag688-))
)

)
(:action Flag689Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BC)
(NOT-BOARDED-PASS_BC)
)
:effect
(and
(Flag689-)
(Flag689prime-)
)

)
(:action Prim689Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BC))
)
:effect
(and
(Flag689prime-)
(not (Flag689-))
)

)
(:action Prim689Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BC))
)
:effect
(and
(Flag689prime-)
(not (Flag689-))
)

)
(:action Flag690Action
:parameters ()
:precondition
(and
(NOT-SERVED-PASS_BA)
(NOT-BOARDED-PASS_BA)
)
:effect
(and
(Flag690-)
(Flag690prime-)
)

)
(:action Prim690Action-0
:parameters ()
:precondition
(and
(not (NOT-SERVED-PASS_BA))
)
:effect
(and
(Flag690prime-)
(not (Flag690-))
)

)
(:action Prim690Action-1
:parameters ()
:precondition
(and
(not (NOT-BOARDED-PASS_BA))
)
:effect
(and
(Flag690prime-)
(not (Flag690-))
)

)
)
